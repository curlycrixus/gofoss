[![Netlify Status](https://api.netlify.com/api/v1/badges/d510ab06-d1d4-437b-a057-d53cc3388c2d/deploy-status)](https://app.netlify.com/sites/gofossnet/deploys)

<img src="https://img.shields.io/liberapay/goal/gofoss.svg?logo=liberapay">

# gofoss.net

<img src="https://gofoss.net/assets/img/gofoss_favicon.png" alt="gofoss.net" width="150px"></img>

This is the source code of [gofoss.net](https://gofoss.net/), the ultimate guide to adopting free and open source software, and using privacy-friendly services.

The aim of this website is to make privacy respecting, free and open source software accessible to all.

## Learn how to...

* ... [safely browse the Internet](https://gofoss.net/intro-browse-freely/)
* ... [keep your conversations private](https://gofoss.net/intro-speak-freely/)
* ... [protect your data](https://gofoss.net/intro-store-safely/)
* ... [unlock your computer's full potential](https://gofoss.net/intro-free-your-computer/)
* ... [stay mobile and free](https://gofoss.net/intro-free-your-phone/)
* ... [own your cloud](https://gofoss.net/intro-free-your-cloud/)
* ... [avoid filter bubbles, surveillance & censorship](https://gofoss.net/nothing-to-hide/)

## Why should you bother?

* Large tech firms harvest user data to [maximize their profits](https://en.wikipedia.org/wiki/Surveillance_capitalism/)
* Irrespective of fundamental rights, such as freedom & privacy
* This gives rise to mass surveillance & polarizes the political debate
* It also drives uniformity of thought & facilitates censorship
* Free and open source software can help addressing those issues

## How can you contribute?

* This is a volunteer-run & non-profit project
* It is truly free and open source
* No ads, no tracking, no sponsors, no affiliates, no paywall
* Help us to improve the content, add new features & translate
* Read the [contribution guidelines](https://gofoss.net/contributing/) to learn about all the ways to get involved
* Or click on the button below to help us funding operating & development costs

<center>

<a href="https://liberapay.com/GeorgJerska/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

</center>

## Screenshots

![gofoss.net screenshots](docs/assets/img/screenshot_gofoss.png)

## License

Released with ❤️under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt).

Please also refer to the [privacy statement, legal disclaimer and license terms](https://gofoss.net/disclaimer/).


Copyright (c) 2020-2022 Georg Jerska <gofoss@protonmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

A full copy of the GNU Affero General Public License version 3 is available at <https://www.gnu.org/licenses/agpl-3.0.txt>.

This program incorporates work covered by the following copyright and
permission notice:

    Copyright (c) 2016-2022 Martin Donath <martin.donath@squidfunk.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
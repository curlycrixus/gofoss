---
template: main.html
title: Comment auto-héberger des services en nuage
description: Fediverse. Fournisseurs alternatifs de services en nuage. Auto-héberger. Serveur Ubuntu. LAMP. Sécuriser votre serveur. SSH. Pare-feu. NTP. Antivirus. OpenVPN. Pi-Hole.
---

# Rejoignez le Fediverse, changez de fournisseur de services en nuage & auto-hébergez

<div align="center">
<img src="../../assets/img/server.png" alt="Libérez votre nuage" width="400px"></img>
</div>

Bravo si vous avez êtes arrivés jusqu'ici ! Peut-être que vous avez déjà [changé de navigateur](https://gofoss.net/fr/firefox/) et de [fournisseur d'e-mail](https://gofoss.net/fr/encrypted-emails/). [Sauvegardé](https://gofoss.net/fr/backups/) et [chiffré vos données](https://gofoss.net/fr/encrypted-files/). Changé pour [Linux](https://gofoss.net/fr/ubuntu/), [CalyxOS](https://gofoss.net/fr/calyxos/) ou bien [LineageOS](https://gofoss.net/fr/lineageos/). Et installé des [applications FOSS](https://gofoss.net/fr/foss-apps/). Ce sont des étapes significatives vers plus de vie privée en ligne !

Mais ce n'est pas fini. Vos données continueront d'être récoltées tant que vous utiliserez des services en nuage de Google, Apple, Facebook, Twitter, Amazon ou Microsoft. Pour reprendre un contrôle total sur vos données, vous n'avez en définitive que trois options:

* [rejoignez le Fediverse](https://gofoss.net/fr/fediverse/)
* [choisissez des fournisseurs de service en nuage dignes de confiance](https://gofoss.net/fr/cloud-providers/)
* [auto-hébergez vos services en nuage](https://gofoss.net/fr/ubuntu-server/)


<br>

<center> <img src="../../assets/img/separator_mastodon.svg" alt="Fediverse" width="150px"></img> </center>

**Option 1** — rejoignez le Fediverse : dans ce chapitre, nous allons nous plonger dans le Fediverse, une alternative libre et décentralisée aux médias sociaux commerciaux. Rejoignez l'une des différentes communautés pour entrer en contact avec des gens, communiquer, discuter, ou partager vos photos, vidéos et musiques.

<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Fournisseurs de services en nuage" width="150px"></img> </center>

**Option 2** — choisissez des fournisseurs de service en nuage dignes de confiance : dans ce chapitre, nous allons recommander des services en nuage respectueux de la vie privée pour synchroniser et partager des fichiers, collaborer sur des documents, planifier des réunions, organiser des discussions en ligne, diffuser des vidéos, et ainsi de suite. Que vous suiviez ces recommandations ou vos propres préférences, assurez-vous que vos fournisseurs de solutions en nuage appliquent des politiques strictes de respect de la vie privée, et qu'ils reposent sur des logiciels libres et *open-source*.

<br>

<center> <img src="../../assets/img/separator_duckdns.svg" alt="Auto-hébergement" width="150px"></img> </center>

**Option 3** — auto-hébergez vos services en nuage : dans ces chapitres, nous allons vous expliquer comment configurer un serveur chez vous et le protéger raisonnablement d'[accès non autorisés](https://gofoss.net/fr/server-hardening-basics/) ou d'[attaques malveillantes](https://gofoss.net/fr/server-hardening-advanced/). Nous allons expliquer en outre comment [héberger vos propres services en nuage](https://gofoss.net/fr/secure-domain/), comme de la [synchronisation de fichiers](https://gofoss.net/fr/cloud-storage/), des [galeries de photos](https://gofoss.net/fr/photo-gallery/), de la [gestion de contacts/calendriers/tâches](https://gofoss.net/fr/contacts.calendars-tasks/) ou de la [diffusion de media](https://gofoss.net/fr/media-streaming/). Nous vous montrerons également [comment sauvegarder les données de votre serveur](https://gofoss.net/fr/server-backups/).


<br>

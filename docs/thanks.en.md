---
template: main.html
title: Round of applause
description: Thanks to our close ones. Thanks to privacy, FOSS & self-hosted communities. Thanks to dedicated coders. Thanks to open-source artists.
---

# Thanks to the FOSS community

<div align="center">
<img src="../../assets/img/thanks.png" alt="Thanks" width="600px"></img>
</div>

<br>

Thanks to our close ones for their patience and support.

Thanks to the privacy, FOSS and self-hosting community:

* [/r/degoogle](https://teddit.net/r/degoogle/)
* [/r/privacy](https://teddit.net/r/privacy/)
* [/r/fossdroid](https://teddit.net/r/fossdroid/)
* [/r/selfhosted](https://teddit.net/r/selfhosted/)
* [degoogle (Josh Moore)](https://degoogle.jmoore.dev/)
* [itsfoss](https://itsfoss.com/)
* [framasoft](https://framasoft.org/en/)
* [chatons](https://www.chatons.org/)
* [disroot](https://disroot.org/en)
* [digitalcourage](https://digitalcourage.de/en)
* [riseup](https://riseup.net/)
* [privacytools](https://www.privacytools.io/)
* [systemausfall](https://systemausfall.org/)
* [picasoft](https://picasoft.net/)
* [letsdebugit](https://www.letsdebug.it/)
* [free software directory](https://directory.fsf.org/wiki/Main_Page)
* [open source software directory](https://opensourcesoftwaredirectory.com)

Thanks to all dedicated coders out there building free, open source software:

* [firefox](https://www.mozilla.org/en-US/firefox/)
* [ublock origin](https://en.wikipedia.org/wiki/UBlock_Origin)
* [searx](https://searx.me/)
* [tor](https://www.torproject.org/)
* [protonvpn](https://protonvpn.com/)
* [mullvad](https://mullvad.net/en/)
* [riseupvpn](https://riseup.net/en/vpn)
* [calyxvpn](https://calyx.net/)
* [protonmail](https://protonmail.com/)
* [tutanota](https://tutanota.com/)
* [thunderbird](https://www.thunderbird.net/en-US/)
* [signal](https://signal.org/en/)
* [element](https://element.io/)
* [jami](https://jami.net/)
* [briar](https://briarproject.org/)
* [keepass](https://keepass.info/)
* [andotp](https://github.com/andOTP/andOTP/)
* [freefilesync](https://freefilesync.org/)
* [veracrypt](https://www.veracrypt.fr/)
* [cryptomator](https://cryptomator.org/)
* [f-droid](https://f-droid.org/)
* [aurora store](https://auroraoss.com/)
* [lineageos](https://www.lineageos.org/), [microg](https://microg.org/) & [lineageos for microg](https://lineage.microg.org/)
* [calyxos](https://calyxos.org/)
* [exodus privacy](https://exodus-privacy.eu.org/en/)
* [linux](https://www.linux.org/)
* [ubuntu](https://ubuntu.com/)
* [linux mint](https://linuxmint.com)
* [gnome](https://www.gnome.org/)
* [virtualbox](https://www.virtualbox.org/)
* [etherpad](https://etherpad.org/)
* [cryptpad](https://cryptpad.fr/)
* [seafile](https://www.seafile.com/en/home/)
* [nextcloud](https://nextcloud.com/)
* [onlyoffice](https://www.onlyoffice.com/en/)
* [studs](https://sourcesup.cru.fr/projects/studs/)
* [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)
* [jitsi](https://meet.jit.si/)
* [mattermost](https://mattermost.com/)
* [xmpp](https://conversejs.org/)
* [matrix](https://matrix.org/)
* [sympa](https://www.sympa.org/)
* [diaspora](https://diasporafoundation.org/)
* [friendica](https://friendi.ca/)
* [gnu social](https://gnusocial.network/)
* [mastodon](https://joinmastodon.org/)
* [nitter](https://nitter.net/)
* [write freely](https://writefreely.org/)
* [mobilizon](https://joinmobilizon.org/en/)
* [lemmy](https://join-lemmy.org/)
* [aether](https://getaether.net/)
* [teddit](https://teddit.net/)
* [peertube](https://joinpeertube.org/)
* [invidious](https://invidious.io/)
* [pixelfed](https://pixelfed.org/)
* [bibliogram](https://bibliogram.art/)
* [lutim](https://github.com/ldidry/lutim/)
* [funkwhale](https://funkwhale.audio/)
* [bookwyrm](https://bookwyrm.social/)
* [openstreetmaps](https://www.openstreetmap.org/)
* [umap](https://github.com/umap-project/umap/)
* [gogocarto](https://gogocarto.fr/projects)
* [libre translate](https://libretranslate.com/)
* [simply translate](https://translate.metalune.xyz/)
* [lstu](https://lstu.fr/)
* [polr](https://github.com/cydrobolt/polr/)
* [hastebin](https://www.toptal.com/developers/hastebin/)
* [privatebin](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Instances-Directory)
* [ghostbin](https://ghostbin.com/)
* [libera pay](https://liberapay.com/)
* [bitcoin](https://bitcoin.org/en/)
* [ethereum](https://ethereum.org/en/)
* [litecoin](https://litecoin.org/)
* [linux malware detect](https://www.rfxn.com/projects/linux-malware-detect/)
* [clamav](https://www.clamav.net/)
* [duckdns](https://www.duckdns.org/)
* [pihole](https://pi-hole.net/)
* [openvpn](https://openvpn.net/)
* [letsencrypt](https://letsencrypt.org/)
* [dehydrated](https://dehydrated.io/)
* [radicale](https://radicale.org/)
* [davx5](https://www.davx5.com/)
* [piwigo](https://piwigo.org/)
* [jellyfin](https://jellyfin.org/)
* [rsnapshot](https://rsnapshot.org/)
* [material for mkdocs](https://squidfunk.github.io/mkdocs-material/)
* [echarts](https://github.com/apache/echarts)
* [senfcall](https://senfcall.de/)

Thanks to the whole [Framalang team](https://framablog.org/framalang/) for the amazing job on the French translation: Aliénor, CLC, Côme, Frabrice, goofy, Guestr, jums, Marius, susy, Sylvain R. and many more.

Thanks to open-source artists:

* Homepage background released by Gam-Ol under the [Pixabay License](https://pixabay.com/service/license/)
* Illustrations inspired by [freepik](https://www.freepik.com/) and [undraw](https://undraw.co/)
* Icons respectfully borrowed from [ameixa](https://gitlab.com/xphnx/ameixa/)
* Logos and screenshots for Firefox, Friendica, Mastodon, Funkwhale, etc. kindly provided by [Wikimedia](https://commons.wikimedia.org/wiki/Main_Page)
* Blender animations inspired by [duck3d](https://www.ducky3d.com/)
* [Contra Chrome](https://contrachrome.com/) is a great web comic explaining how Google’s browser has become a threat to user privacy
* Photos in the *Origins* chapter are distributed under the [Unsplash License](https://unsplash.com/license), thanks go to Alexander Popov, Gabriel Perez, Sandro Katalina, Roman Denisenko, Andrea De Santis, Denys Nevozhai, Hello I'm Nik, Ray Zhuang, Matthew Henry, Markus Spiske, Gabriel Heinzer, Clark Tibbs, Alexandre Debiève, cheng feng, Taylor Vick, Adi Goldstein
* Geek humour served to you by [xkcd](https://xkcd.com/)
* Retro music powered by [Aries The Producer](https://ariestheproducer.com/) and [nihilore](http://www.nihilore.com/)

<br>
---
template: main.html
title: Comment créer des sauvegardes de votre serveur
description: Faire des sauvegardes de votre serveur. Logiciel de sauvegarde de serveur. Qu'est-ce que rsnapshot ? Comment installer rsnapshot ? Rsnapshot Ubuntu. Restauration de rsnapshot.
---

# Planifiez des sauvegardes de votre serveur avec rsnapshot

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises."

<center>
<img align="center" src="../../assets/img/server_2.png" alt="Sauvegardes de serveur" width="600px"></img>
</center>

[rsnapshot](https://rsnapshot.org/) est un outil permettant de planifier des sauvegardes de votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/). Le logiciel est libre et open source, fiable et économe en espace de stockage. Cela est dû au fait que rsnapshot effectue des sauvegardes incrémentales : après une sauvegarde initiale complète, seuls les fichiers modifiés sont sauvegardés.

<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Installation de rsnapshot" width="150px"></img> </center>

## Installation

En cas de problème, il est souhaitable de pouvoir rétablir le serveur dans un état antérieur. Pour cela, installez rsnapshot sur votre serveur en exécutant la commande suivante: `sudo apt install rsnapshot`.


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Configuration de rsnapshot" width="150px"></img> </center>

## Configuration

Après avoir installé rsnapshot, vous pouvez procéder à un certain nombre de réglages tels que l'emplacement du répertoire de sauvegarde, les dépendances externes, les intervalles de sauvegarde, la collecte de données, les fichiers à sauvegarder, etc. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Répertoire de sauvegarde

    Ouvrez le fichier de configuration de rsnapshot :

    ```bash
    sudo vi /etc/rsnapshot.conf
    ```

    Ajoutez ou modifiez les lignes suivantes pour définir l'emplacement où rsnapshot doit stocker les sauvegardes du serveur. Remplacez `$PATH` par le chemin vers le répertoire de sauvegarde souhaité. Celui-ci peut être situé sur le disque dur du serveur, un disque externe connecté au serveur ou un emplacement à distance. Activé également le paramètre `no_create_root` pour éviter que rsnapshot crée automatiquement un dossier de sauvegarde :

    ```bash
    snapshot_root	$PATH
    no_create_root	1
    ```

    ### Dependencies

    Dans le même fichier de configuration `/etc/rsnapshot.conf`, dé-commentez les lignes suivantes pour indiquer les dépendances externes à rsnapshot :

    ```bash
    cmd_rsync           /usr/bin/rsync
    cmd_du              /usr/bin/du
    cmd_rsnapshot_diff	/usr/bin/rsnapshot-diff
    ```

    Assurez-vous que rsnapshot pointe vers les bons binaires. Pour cela, exécutez les commandes suivantes et vérifiez que tous les chemins sont corrects :

    ```bash
    whereis rsync
    whereis du
    whereis rsnapshot-diff
    ```

    ### Intervalles de sauvegarde

    Les paramètres ci-dessous, situés dans le fichier `/etc/rsnapshot.conf`, définissent les intervalles de sauvegarde ainsi que le nombre de sauvegardes à conserver. Rsnapshot prend en charge des sauvegardes horaires, quotidiennes, hebdomadaires et mensuelles. Dans cet exemple, nous garderons les 3 dernières sauvegardes quotidiennes, les 7 dernières sauvegardes hebdomadaires et les 2 dernières sauvegardes mensuelles. Ajustez ces fréquences en fonction de votre propre stratégie de sauvegarde :

    ```bash
    retain	daily	3
    retain	weekly	6
    retain	monthly	2
    ```

    ### Collecte de donnée

    Vous pouvez également activer la collecte de donnée, par exemple à des fins de débogage. Pour cela, créez un fichier journal :

    ```bash
    sudo touch /var/log/rsnapshot.log
    ```

    Activez la collecte de donnée dans le fichier de configuration `/etc/rsnapshot.conf`:

    ```bash
    verbose     5
    loglevel    5
    logfile     /var/log/rsnapshot.log
    ```


    ### Fichiers à sauvegarder

    Décidez quels fichiers du serveur doivent être sauvegardés. Vous pouvez spécifier un ou plusieurs répertoires de votre choix. Vous pouvez également exclure certains sous-répertoires en utilisant la fonction `exclude`. Veillez à utiliser une nouvelle ligne pour chaque répertoire, et d'ajouter un `/` à la fin de chaque chemin de répertoire. Dans cet exemple, nous allons sauvegarder le système de fichiers « root » complet `/`, en excluant les répertoires `tmp`, `proc`, `mnt` et `sys` :

    ```bash
    backup	/	.	exclude=tmp,exclude=proc,exclude=mnt,exclude=sys
    ```


    ### Vérifications

    Vérifiez la syntaxe du fichier de configuration (le terminal devrait afficher `Syntax OK`) :

    ```bash
    sudo rsnapshot configtest
    ```

    Enfin, testez vos stratégies de sauvegarde horaire, quotidienne, hebdomadaire et mensuelle :

    ```bash
    sudo rsnapshot -t hourly
    sudo rsnapshot -t daily
    sudo rsnapshot -t weekly
    sudo rsnapshot -t monthly
    ```


<br>

<center> <img src="../../assets/img/separator_alarm.svg" alt="Programmer des sauvegardes avec rsnapshot" width="150px"></img> </center>

## Sauvegardes programmées

Planifiez des sauvegardes automatiques à certains intervalles en suivant les instructions ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Exécutez la stratégie de sauvegarde la plus courte pour créer une sauvegarde initiale complète. Dans notre exemple, il s'agit de la sauvegarde quotidienne (à ajuster en conséquence) :

    ```bash
    sudo rsnapshot daily
    ```

    Ouvrez le fichier de configuration `cron` pour planifier les sauvegardes à certains intervalles :

    ```bash
    sudo vi /etc/cron.d/rsnapshot
    ```

    Dans cet exemple, nous allons programmer des sauvegardes quotidiennes à 1h00 du matin, des sauvegardes hebdomadaires chaque lundi à 18h30 et des sauvegardes mensuelles le 2ème jour de chaque mois à 7h00. Programmez vos propres horaires selon vos besoins, le [guru crontab](https://crontab.guru/) peut vous faciliter la tâche :

    ```bash
    #0 */4 * * *    root    /usr/bin/rsnapshot hourly
    0 1  * * *      root    /usr/bin/rsnapshot daily
    30 18 * * 1     root    /usr/bin/rsnapshot weekly
    0 7 2 * *       root    /usr/bin/rsnapshot monthly
    ```


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Rétablir des sauvegardes avec rsnapshot" width="150px"></img> </center>

## Rétablir des sauvegardes

Si quelque chose devait tourner mal, vous pouvez essayer de rétablir votre serveur à un état antérieur à partir d'une des sauvegardes rsnapshot. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    La commande ci-dessous liste toutes les sauvegardes existantes. Remplacez `$PATH` par le chemin vers le répertoire de sauvegarde défini précédemment :

    ```bash
    ls -l $PATH
    ```

    Selon votre stratégie de sauvegarde, le terminal devrait afficher quelque chose comme :

    ```bash
    /path/to/rsnapshot/backup/folder/daily.0
    /path/to/rsnapshot/backup/folder/daily.1
    /path/to/rsnapshot/backup/folder/daily.2
    /path/to/rsnapshot/backup/folder/weekly.0
    /path/to/rsnapshot/backup/folder/weekly.1
    /path/to/rsnapshot/backup/folder/weekly.2
    /path/to/rsnapshot/backup/folder/weekly.3
    /path/to/rsnapshot/backup/folder/weekly.4
    /path/to/rsnapshot/backup/folder/weekly.5
    /path/to/rsnapshot/backup/folder/monthly.0
    /path/to/rsnapshot/backup/folder/monthly.1
    ```

    La commande suivante restaure le serveur à un état antérieur :

    ```bash
    sudo rsync -avr $SOURCE_PATH $DESTINATION_PATH
    ```

    Assurez-vous de fournir les bons cheminements `$SOURCE_PATH` (= le répertoire de sauvegarde qui est sur le point d'être rétabli) et `$DESTINATION_PATH` (= le répertoire cible sur le serveur dans lequel la sauvegarde doit être rétablie). Par exemple, la commande suivante rétablira la sauvegarde hebdomadaire de l'ensemble du système de fichiers d'il y a 3 semaines à la racine `/` du serveur :

    ```bash
    sudo rsync -avr /path/to/rsnapshot/backup/folder/weekly.3/ /
    ```

!!! warning "Attention ! Tu risques de te faire pincer très fort ! "

    Faites preuve d'une extrême prudence lorsque vous rétablissez une sauvegarde. Elle peut effacer irrévocablement des fichiers et entraîner des pertes de données.

<br>
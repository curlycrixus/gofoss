---
template: main.html
title: So verschlüsselt Ihr Eure Dateien
description: Daten-Verschlüsselung. Ist Veracrypt sicher? Wie benutzt man Veracrypt? Ist Cryptomator sicher? Wie benutzt man Cryptomator? Cryptomator oder Veracrypt? Veracrypt oder Truecrypt?
---

# Verschlüsselt Eure Daten mit VeraCrypt & Cryptomator

!!! level "Letzte Aktualisierung: März 2022. Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/cryptomator_veracrypt.png" alt="Daten-Verschlüsselung" width="400px"></img>
</div>

Was ist Dateiverschlüsselung? Es ist eine Technik mit der Ihr Eure Daten und [Sicherungskopien](https://gofoss.net/de/backups/) unlesbar machen könnt, selbst im Falle eines unbefugten Zugriffs. Es gibt eine große Auswahl an kostenloser Software zur Dateiverschlüsselung. In diesem Kapitel widmen wir uns VeraCrypt und Cryptomator, die zu den sichersten Verschlüsselungsprogrammen gehören. VeraCrypt ist das Werkzeug Eurer Wahl, wenn Ihr Dateien in großen Containern auf einem lokalen Gerät verschlüsseln wollt, ohne Informationen über Dateistruktur, -anzahl oder -größe preiszugeben. Cryptomator wiederum eignet sich bestens zur Verschlüsselung von Cloud-Dateien, da jede Datei einzeln verschlüsselt wird. Einziger Nachteil ist, dass Cryptomator Metainformationen wie Zeitstempel, Anzahl oder Größe der Dateien preisgibt.

<br>

<center> <img src="../../assets/img/separator_veracrypt.svg" alt="VeraCrypt" width="150px"></img> </center>

## VeraCrypt

[VeraCrypt](https://www.veracrypt.fr/en/) ist ein freies und [quelloffenes](https://veracrypt.fr/code/) Programm zur Verschlüsselung von Dateien oder ganzen Dateisystemen. Es handelt sich um den Nachfolger von [TrueCrypt](https://en.wikipedia.org/wiki/TrueCrypt) und wurde erstmals im Jahr 2013 veröffentlicht. Untenstehend findet Ihr eine ausführliche Anleitung um VeraCrypt zu installieren (zum Zeitpunkt der Verfassung dieses Artikels wurde noch keine VeraCrypt Android-Version veröffentlicht).

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        Ladet das aktuelle [VeraCrypt-Installationsprogramm für Windows](https://www.veracrypt.fr/en/Downloads.html) herunter und folgt den Anweisungen auf dem Bildschirm.


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        MacOS benötigt die Begleit-App OSXFUSE, um VeraCrypt korrekt auszuführen. Ladet das aktuelle [Installationsprogramm](https://github.com/osxfuse/osxfuse/releases/) herunter, öffnet die heruntergeladene `.dmg`-Datei und folget den Anweisungen auf dem Bildschirm. Stellt dabei sicher, dass Ihr den `MacFUSE Compatibility Layer` auswählt.

        Ladet anschließend das aktuelle [VeraCrypt-Installationsprogramm für macOS](https://www.veracrypt.fr/en/Downloads.html) herunter und folgt den Anweisungen auf dem Bildschirm. Für einen noch bequemeren Zugriff könnt Ihr den Anwendungsordner öffnen und das VeraCrypt-Symbol in das Andockmenü verschieben.


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Ladet das aktuelle [VeraCrypt-Generic-Installationsprogramm für Linux](https://www.veracrypt.fr/en/Downloads.html) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `veracrypt-X.XX-UpdateX-setup.tar.bz2`. Im Rahmen dieses Tutorials gehen wir davon aus, dass die Datei in den Ordner `/home/gofoss/Downloads` heruntergeladen wurde und im Ordner `/home/gofoss/veracrypt` installiert werden soll. Stellt sicher, dass Ihr diese Dateipfade entsprechend Euren eigenen Einstellungen anpasst. Öffnet ein Terminal und führt die folgenden Befehle aus:

        ```bash
        mkdir /home/gofoss/veracrypt
        cd /home/gofoss/Downloads
        tar -xvf veracrypt-X.XX-UpdateX-setup.tar.bz2 -C /home/gofoss/veracrypt
        ```

        Startet das Installationsprogramm und folgt den Anweisungen auf dem Bildschirm:

        ```bash
        cd /home/gofoss/veracrypt
        ./veracrypt-1.XX-UpdateX-setup-gui-x64
        ```

        Führt das Skript `/usr/bin/veracrypt-uninstall.sh` aus, falls Ihr VeraCrypt deinstallieren wollt.


### Dateien mit VeraCrypt verschlüsseln

Sobald VeraCrypt installiert ist, könnt Ihr verschlüsselte Container erstellen und so Eure Dateien schützen. *Hinweis*: die deutsche Benutzeroberfläche ist derzeit nur für Windows verfügbar. Benutzer von Linux und macOS müssen weiterhin mit der englischen Oberfläche arbeiten.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Schritte | Anweisungen |
    | :------: | ------ |
    |Volumen erstellen |Startet VeraCrypt und klickt auf `Volume erstellen ‣ Eine verschlüsselten Containerdatei erstellen ‣ Weiter`. |
    |Volume-Typ |Wählt `Standard VeraCrypt-Volume ‣ Weiter`. |
    |Volume-Speicherort |Klickt auf `Datei...` und legt den Speicherort für den VeraCrypt-Container fest. Klickt dann auf `Speichern ‣ Weiter`. |
    |Verschlüsselungseinstellungen |Wählt `AES` als Verschlüsselungsalgorithmus und `SHA-512` als Hash-Algorithmus und klickt auf `Weiter`. <br><br>*Hinweis*: VeraCrypt unterstützt eine Reihe von Verschlüsselungsalgorithmen, darunter AES, Serpent, Twofish, Camellia und Kuznyechik, sowie verschiedene Kombinationen von kaskadierten Algorithmen. VeraCrypt unterstützt außerdem vier Hash-Funktionen: SHA-512, Whirlpool, SHA-256 und Streebog. |
    |Volume-Größe |Legt die Dateigröße Eures VeraCrypt-Containers fest. Klickt dann auf `Weiter`. |
    |Volume-Kennwort |Legt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) für Euren VeraCrypt-Container fest. Dies ist der Schlüssel zu Euren Daten. <br><br>**Hinweis**: VeraCrypt bietet zusätzliche Optionen, wie z. B. die Aktivierung von Schlüsseldateien oder einem sogenannten *Personal Iterations Multiplier (PIM)*. |
    |Volume-Format |Wählt ein Dateisystem, zum Beispiel `FAT`. Klickt dann auf `Weiter`. |
    |Volume-Format |Bewegt Eure Maus mindestens 30 Sekunden lang so zufällig wie möglich, idealerweise so lange, bis der Balken der Zufallsanzeige das Maximum erreicht hat. Klickt dann auf `Formatieren`, um den Container zu erstellen. Je nach Größe und verfügbarer Rechenleistung kann dies ziemlich LANGE dauern. Macht Euch eine Tasse Kaffee oder Mittagspause. Und zieht auf keinen Fall den Stecker... |
    |Beenden |Klickt auf `OK ‣ Beenden` sobald das Volumen erstellt wurde. |

    </center>

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/6d68b947-67ee-402b-bdcd-9d292f792f57" frameborder="0" allowfullscreen></iframe>

    </center>


### Mit VeraCrypt auf verschlüsselte Dateien zugreifen

Der Zugriff auf mit VeraCrypt verschlüsselte Dateien ist nur mit dem richtigen Passwort möglich.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Schritte | Anweisungen |
    | ------ | ------ |
    |Laufwerk auswählen |Öffnet die VeraCrypt-Anwendung und wählt einen freien Laufwerkbuchstaben, in den der VeraCrypt-Container eingebunden werden soll. |
    |Container einbinden |Klickt auf `Datei...` und wählt den verschlüsselten Container, den Ihr öffnen wollt. Klickt dann auf `Öffnen` und `Einbinden`. |
    |Kennwort eingeben |Gebt das Kennwort für den VeraCrypt-Container ein und klickt auf `OK`. Gebt dann auf Anfrage Euer Administrator-Kennwort ein. |
    |Container öffnen |Der Container wird als virtuelles Laufwerk eingebunden, das sich wie jedes beliebige Laufwerk auf Eurem Computer verhält. Ruft das virtuelle Laufwerk auf und erstellt, öffnet, ändert, speichert, kopiert oder löscht nach belieben Dateien. |
    |Container aushängen |Schließt den Container sobald Ihr mit Eurer Arbeit fertig seid, indem Ihr im Hauptfenster von VeraCrypt auf `Trennen` klickt. |

    </center>

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/1eeb5668-4800-4326-9da1-1b20d8a53edb" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_cryptomator.svg" alt="Cryptomator" width="150px"></img> </center>

## Cryptomator

[Cryptomator](https://cryptomator.org/de/) ist ein [quelloffenes](https://github.com/cryptomator/) Programm, das Eure Cloud-Daten verschlüsselt. Zugriff auf die verschlüsselten Daten ist nur mit dem richtigen Passwort möglich. Dies verringert das Risiko, dass Cloud-Anbieter oder Dritte sich Zugriff auf Eure Online-Daten verschaffen. Untenstehend findet Ihr eine ausführliche Anleitung um Cryptomator zu installieren.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        Ladet das aktuelle [Cryptomator-Installationsprogramm für Windows](https://cryptomator.org/de/downloads/win/thanks/) herunter und folgt den Anweisungen auf dem Bildschirm.


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        Ladet das aktuelle [Cryptomator-Installationsprogramm für macOS](https://cryptomator.org/de/downloads/mac/thanks/) herunter, öffnet die heruntergeladene `.dmg`-Datei und verschiebtdas Cryptomator-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff könnt Ihr  den Anwendungsordner öffnen und das Cryptomator-Symbol in das Andockmenü verschieben.


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Öffnet ein Terminal, fügt die richtige Paketquelle hinzu und installiert Cryptomator:

        ```bash
        sudo add-apt-repository ppa:sebastian-stenzel/cryptomator
        sudo apt update
        sudo apt install cryptomator -y
        ```

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Erwerbt die mobile Cryptomator-App auf [Googles Play Store](https://play.google.com/store/apps/details?id=org.cryptomator&hl=de), auf [Cryptomators Webseite](https://cryptomator.org/de/android/) oder auf dem [Aurora Store](https://auroraoss.com/de/).


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        Erwerbt die mobile Cryptomator-App auf dem [App Store](https://apps.apple.com/de/app/cryptomator/id953086535/).


### Dateien mit Cryptomator verschlüsseln

=== "Desktop-Clients"

    Sobald Cryptomator auf Eurem Desktop-Computer installiert ist, könnt Ihr sogenannte verschlüsselte Tresore erstellen und so Eure Cloud-Dateien schützen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Anweisungen |
        | :------: | ------ |
        |Neuen Tresor erstellen |Startet Cryptomator und wählt `+ Tresor hinzufügen ‣ Neuen Tresor erstellen`. |
        |Namen wählen |Gebt dem Tresor einen Namen und klickt anschließend auf `Weiter`. |
        |Speicherort wählen |Wählt den Ort, an dem die verschlüsselten Dateien gespeichert werden sollen. Es sollte sich um ein Verzeichnis handeln, das mit der Cloud synchronisiert wird. Klickt dann auf `Öffnen ‣ Weiter`. |
        |Kennwort wählen |Legt ein [sicheres, individuelles Kennwort](https://gofoss.net/de/passwords/) für Euren Cryptomator-Tresor fest. Dies ist der Schlüssel zu Euren Daten.|
        |Wiederherstellungsschlüssel erstellen |Optional könnt Ihr ebenfalls einen Wiederherstellungsschlüssel erstellen. Dieser Schlüssel wird im Falle eines Passwortverlustes benötigt. Er sollte daher sicher aufbewahrt werden. |
        |Tresor erstellen |Klickt auf `Tresor erstellen`. |

        </center>

        *Hinweis*: Ihr könnt ebenfalls einen bestehenden Tresor zum Desktop-Client hinzufügen. Startet hierzu Cryptomator, wählt `+ Tresor hinzufügen ‣ Existierenden Tresor öffnen`, ruft den Ordner auf, der den Tresor enthält und wählt die Datei `masterkey.cryptomator`.

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        <center>

        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/dd51ffbc-d347-4319-8543-a63e21fd3023" frameborder="0" allowfullscreen></iframe>

        </center>


=== "Mobile Clients"

    Sobald Cryptomator auf Eurem mobilen Gerät installiert ist, könnt Ihr sogenannte verschlüsselte Tresore erstellen und so Eure Cloud-Dateien schützen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Anweisungen |
        | :------: | ------ |
        |Neuen Tresor erstellen |Startet Cryptomator und wählt `+ Neuen Tresor erstellen`. |
        |Cloud-Anbieter wählen |Wählt einen Cloud-Anbieter aus, bei dem Eure verschlüsselten Dateien gespeichert werden sollen. Falls erforderlich, meldet Euch beim Cloud-Anbieter an.|
        |Namen wählen |Gebt dem Tresor einen Namen und klickt anschließend auf `Weiter`. |
        |Speicherort wählen |Wählt den Ort, an dem die verschlüsselten Dateien gespeichert werden sollen. Klickt auf `Hier einfügen`. |
        |Kennwort wählen | Legt ein [sicheres, individuelles Kennwort](https://gofoss.net/de/passwords/) für Euren Cryptomator-Tresor fest. Dies ist der Schlüssel zu Euren Daten. Klickt abschließend auf `Fertig`. |

        </center>

        *Hinweis*: Ihr könnt ebenfalls einen bestehenden Tresor zur mobilen App hinzufügen. Startet hierzu die Cryptomator-App, wählt `+ Existierenden Tresor öffnen`, wählt Euren Cloud-Anbieter, ruft den Ordner auf, der den Tresor enthält und wählt die Datei `masterkey.cryptomator`.


### Mit Cryptomator auf verschlüsselte Dateien zugreifen

=== "Desktop-Clients"

    Der Zugriff auf mit Cryptomator verschlüsselte Dateien ist nur mit dem richtigen Passwort möglich.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Anweisungen |
        | :------: | ------ |
        |Tresor entsperren |Öffnet die Cryptomator-Anwendung, wählt einen bestehenden Tresor und klickt auf `Entsperren...`. Gebt Euer Passwort ein und klickt auf `Entsperren`. |
        |Tresor einbinden |Klickt auf `Laufwerk anzeigen`. Der Tresor wird als virtuelles Laufwerk eingebunden und verhält sich wie jedes beliebige Laufwerk auf Eurem Computer. Erstellt, öffnet, ändert, speichert, kopiert oder löscht Eure Dateien nach belieben. |
        |Tresor sperren |Schließt den Tresor sobald Ihr mit Eurer Arbeit fertig seid, indem Ihr im Hauptfenster von Cryptomator auf `Sperren` klickt. |

        </center>

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/e7937c30-f60c-40f1-abdb-798400e5cbcb" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Mobile Clients"

    Der Zugriff auf mit Cryptomator verschlüsselte Dateien ist nur mit dem richtigen Passwort möglich.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Anweisungen |
        | :------: | ------ |
        |Tresor entsperren |Öffnet die Cryptomator-App, wählt einen bestehenden Tresor, gebt Euer Kennwort ein und klickt auf `Entsperren`. |
        |Tresor sperren|Erstellt, öffnet, ändert, speichert, kopiert oder löscht Eure Dateien nach belieben. Schließt den Tresor sobald Ihr mit Eurer Arbeit fertig seid, indem Ihr im Hauptfenster von Cryptomator auf `Sperren` klickt. |

        </center>

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Cryptomator & Veracrypt Unterstützung" width="150px"></img> </center>

## Unterstützung

Für weitere Details und Fragen könnt Ihr:

* die [VeraCrypt-Dokumentation](https://veracrypt.fr/en/Documentation.html) lesen, oder die [VeraCrypt-Gemeinschaft](https://sourceforge.net/p/veracrypt/discussion/) um Hilfe bitten.

* die [Cryptomator-Dokumentation](https://docs.cryptomator.org/en/latest/) lesen oder die [Cryptomator-Gemeinschaft](https://community.cryptomator.org/) um Hilfe bitten.

<br>

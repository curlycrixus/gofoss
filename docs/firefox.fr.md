---
template: main.html
title: Comment sécuriser Firefox
description: Firefox vs Chrome. Est-ce que Firefox est plus sécurisé que Google ? Comment installer Firefox ? Firefox sous Android ? Qu'est-ce qu'un Firefox renforcé ?
---

# Firefox, un navigateur libre et open source

!!! level "Dernière mise à jour: mai 2022. Destiné aux débutants et utilisateurs expérimentés. Certaines compétences techniques peuvent être requises."


<center>
<img align="center" src="../../assets/img/firefox_logo.png" alt="Le navigateur Mozilla Firefox" width ="150px"></img>
</center>

[Firefox](https://www.mozilla.org/fr/firefox/new/) est le navigateur préféré lorsqu'on parle de protection de donnée, de sécurité, et d'utilisabilité. Firefox est sorti pour la première fois en 2004 grâce à la communauté Mozilla. Le navigateur est libre et open source, bien que la version Android comprenne des bibliothèques propriétaires, comme Google Analytics. Firefox est hautement configurable, bloque les cookies de pistage intersites par défaut, et tourne sans accrocs sur pratiquement n'importe quel appareil. Des instructions d'installation sont disponibles ci-dessous.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        Téléchargez et lancez l'[installateur Firefox](https://www.mozilla.org/fr/firefox/windows/).


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        Téléchargez l'[image disque Firefox](https://www.mozilla.org/fr/firefox/mac/), ouvrez-la et glissez l'icône Firefox sur le répertoire Application. Pour un accès plus simple, ouvrez le répertoire Applications et glissez l'icône Firefox sur votre dock.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), ouvrez un terminal avec le raccourci `Ctrl+Alt+T` ou bien cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Puis exécutez la commande suivante :

        ```bash
        sudo apt install firefox
        ```

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        Téléchargez Firefox depuis l'[App Store](https://play.google.com/store/apps/details?id=org.mozilla.firefox&hl=fr&gl=FR), ou visitez la [page de téléchargement de Firefox](https://www.mozilla.org/fr/firefox/browsers/mobile/) depuis votre mobile Android. Il existe également un moyen de télécharger et mettre à jour Firefox sans utiliser de compte Google: [Aurora Store](https://auroraoss.com/) ! Nous allons vous expliquer comment utiliser des magasins d'application alternatifs dans [un chapitre ultérieur](https://gofoss.net/fr/foss-apps/).


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        Téléchargez Firefox depuis l'[App Store](https://apps.apple.com/fr/app/firefox-private-safe-browser/id989804926/).

<div style="margin-top:-20px">
</div>

??? warning "Mozilla, le chevalier (pas si) blanc à la merci de Google"

    Dans un paysage dominé par Google, Firefox semble actuellement être le seul navigateur indépendant qui mérite d'être mentionné. C'est pourquoi nous recommandons d'utiliser Firefox comme une alternative libre et open source aux navigateurs basés sur Chrome. Toutefois, nous partageons également certaines critiques concernant Mozilla. Notamment au sujet de choix techniques discutables, comme l'intégration du logiciel « Pocket », [partiellement propriétaire](https://github.com/Pocket/extension-save-to-pocket/issues/75), ou l'activation par défaut de [contenus sponsorisés](https://support.mozilla.org/fr/kb/personnaliser-parametres-firefox-suggest) et de [télémétrie](https://support.mozilla.org/fr/kb/telemetrie-clientid). Fort heureusement, ces paramètres peuvent être ajustés, comme expliqué sur cette page.

    Des préoccupations plus générales concernent le modèle de financement de la *Mozilla Corporation*. C'est la société responsable du développement, de la distribution et de la promotion de Firefox, Thunderbird ainsi que d'autres applications. Par ailleurs, la *Mozilla Corporation* est une entité commerciale qui dépend de l'organisation à but non lucratif appelée *Mozilla Foundation*. Il se trouve que cette même Mozilla Corporation tire l'essentiel de ses revenus d'un accord avec... Google, son principal rival ! Datant de 2006, cet étrange partenariat a été prolongé jusqu'en 2023, et assure [400 millions d'euros](https://www.zdnet.com/article/sources-mozilla-extends-its-google-search-deal/) de revenus annuels en désignant Google comme moteur de recherche par défaut de Firefox. S'il est légitime pour Mozilla de rechercher des flux de valeur qui soutiennent son développement, certains qualifient cette relation commerciale de conflit d'intérêts. On peut en effet se demander si Google ne paye pas Mozilla pour contenir la concurrence et éviter les autorités de la concurrence. Un renard ne mord pas la main qui le nourrit...


??? question "Est-ce que d'autres navigateurs respectent la vie privée ?"

    Il y en a, listés dans le tableau ci-dessous. Gardez à l'esprit que nous recommandons de ne pas utiliser Chrome ou les navigateurs basés sur [Chromium](https://www.chromium.org/) en raison du respect de la vie privée. La bande dessinée de Leah Elliott [Contra Chrome](https://contrachrome.com/) résume parfaitement pourquoi le navigateur de Google est devenu une menace pour la vie privée des utilisatrices et utilisateurs:

    * Chrome ainsi que les navigateurs basés sur Chromium tournent sur du code fondamentalement contrôlé par Google, et renforcent le monopole de Google
    * Chrome, mais aussi d'autres navigateurs basés sur Chromium, sont la porte d'entrée de l'écosystème de Google, notamment GMail, YouTube, Google Photos, etc.
    * Chrome ainsi que les services de Google collectent systématiquement les informations privées des utilisateurs, la plupart du temps sans leur consentement, pour les vendre au plus offrant


    <div class="table-noscroll">

    <center>

    | | [Tor](https://gofoss.net/fr/tor/) | [Librewolf](https://librewolf-community.gitlab.io/) | [FOSS Browser](https://github.com/scoute-dich/browser/) | [Mull](https://github.com/Divested-Mobile/mull/) | [Fennec F-Droid](https://f-droid.org/fr/packages/org.mozilla.fennec_fdroid/) | [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium/) | [Brave](https://brave.com) | [Bromite](https://www.bromite.org/) |
    |------|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
    | Libre & ouvert | <span style="color:orange">✔</span>^1^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^1^ | <span style="color:green">✔</span> |
    | Appli bureau | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Appli Android | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Isolement de données | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^2^ | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^2^ | <span style="color:orange">✔</span>^2^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Isolement de processus | <span style="color:orange">✔</span>^3^ | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Protection empreintes numériques | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ |
    | Filtre de contenu | <span style="color:green">✔</span>^5^ | <span style="color:green">✔</span>^5^ | <span style="color:orange">✔</span>^6^ | <span style="color:green">✔</span>^5^ | <span style="color:green">✔</span>^5^ | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^6^ |
    | Moteur de recherche personnalisé | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Mises à jour régulières | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Absence de télémétrie & pisteurs | <span style="color:red">✗</span>^7^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span>^8^ | <span style="color:green">✔</span>^9^ | <span style="color:green">✔</span> | <span style="color:red">✗</span>^10^ | <span style="color:green">✔</span> |

    </center>

    </div>

    1. *Contient des bibliothèques propriétaires.*

    2. *Protection basique contre le pistage.*

    3. *Uniquement disponible sur ordinateur de bureau. La version Android n'isole pas les sites Web dans leur propre processus afin de limiter l'exposition aux failles de sécurité : ni l'[isolation du processus par site](https://wiki.mozilla.org/Project_Fission) ni le [« Sandboxing »](https://bugzilla.mozilla.org/show_bug.cgi?id=1565196) ne sont mis en œuvre.*

    4. *Protection de base des empreintes digitales.*

    5. *Compatible avec uBlock Origin.*

    6. *Filtre de contenu basique.*

    7. *Tor Browser pour Android contient [3 pisteurs (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/fr/reports/org.torproject.torbrowser/latest/)*

    8. *Exodus Privacy rapporte [2 pisteurs (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/fr/reports/us.spotco.fennec_dos/latest/) pour Mull. Cependant, selon l'équipe de développement, il s'agit d'un [faux positif](https://forum.f-droid.org/t/classyshark3exydus-found-five-trackers-inside-tor-browser/13453/20) : les bibliothèques en question ont été remplacées par des « stubs », c'est-à-dire des bouts de code qui ne font rien.*

    9. *Exodus Privacy rapporte [2 pisteurs (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/fr/reports/org.mozilla.fennec_fdroid/latest/) pour Fennec F-Droid. Cependant, selon l'équipe de développement, il s'agit d'un [faux positif](https://forum.f-droid.org/t/welcome-a-new-fennec-f-droid/11113) : les bibliothèques en question ont été remplacées par des « stubs », c'est-à-dire des bouts de code qui ne font rien.*

    10. *Brave est maintenu par une société à but lucratif, soutenue par un investisseur en capital risque. Contient un système publicitaire (optionnel), et a fait face à des critiques dans le passé pour l'ajout de liens affiliés dont il tirait un profit financier. Transmet par défaut des données télémétriques à un service d'analyse, cela peut être désactivé.*


<br>

<center> <img src="../../assets/img/separator_ublockorigin.svg" alt="uBlock Origin" width="150px"></img> </center>

## Extensions pour protéger vos données

=== "Windows, macOS & Linux"

    Ajoutez [uBlock Origin](https://fr.wikipedia.org/wiki/UBlock_Origin) à votre nouvelle installation de Firefox. C'est un filtre de contenus libre et open source, qui fait très bien son travail ! [Allez sur le store Mozilla](https://addons.mozilla.org/fr/firefox/addon/ublock-origin/) et cliquez sur le bouton `Ajouter à Firefox`. De nombreux paramètres vous permettent  de protéger vos données, comme le détaillent les instructions ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

        * Cliquez sur l'icône uBlock Origin dans la barre d'outils de Firefox
        * Assurez-vous que uBlock Origin est activé, le gros bouton doit être bleu
        * Cliquez sur le bouton Tableau de bord
        * Ouvrez l'onglet `Listes de filtres`
        * Sélectionnez les cases du tableau ci-dessous, et cliquez sur `Appliquer`

        <center>

        | Section | Cases à cocher |
        | ------ | ------ |
        | Publicités | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Confidentialité | ☑ AdGuard Tracking Protection <br> ☑ EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Nuisances | ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br> ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>


=== "Android"

    Ajoutez [HTTPS Everywhere](https://www.eff.org/https-everywhere/) à votre nouvelle installation de Firefox. Il s'agit d'une extension qui active le « Hypertext Transfer Protocol Secure » ([HTTPS](https://fr.wikipedia.org/wiki/HyperText_Transfer_Protocol_Secure)) par défaut, c'est-à-dire un type de chiffrement qui sécurise le trafic sur le Web. Allez dans `Menu ‣ Extensions et thèmes` puis cliquez sur le `+` près de HTTPS Everywhere.

    Vous pouvez également ajouter [uBlock Origin](https://fr.wikipedia.org/wiki/UBlock_Origin), un filtre de contenus libre et open source, qui fait très bien son travail ! Allez dans `Menu ‣ Extensions et thèmes` puis cliquez sur le `+` près de uBlock Origin. Divers paramètres vous permettent d'augmenter le degré de confidentialité, comme détaillé ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        * Allez dans `Menu ‣ Modules complémentaires ‣ uBlock Origin ‣ Paramètres`
        * Ouvrez l'onglet `Listes de filtres`
        * Sélectionnez les cases à cocher du tableau ci-dessous et cliquez sur `Appliquez les changements`

        <center>

        | Section | Cases à cocher |
        | ------ | ------ |
        | Publicités | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Confidentialité | ☑ AdGuard Tracking Protection <br> ☑ EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Nuisances | ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br> ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>

<div style="margin-top:-20px">
</div>

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ae911b43-b4bd-4059-8447-891e82bd2367" frameborder="0" allowfullscreen></iframe>

    </center>

??? info "Dites m'en plus sur les extensions concernant la vie privée"

    Utilisez les extensions avec **parcimonie**. Elles facilitent les [empreintes digitales d'appareil ](https://fr.wikipedia.org/wiki/Empreinte_digitale_d%27appareil), une pratique utilisée pour collecter des informations, suivre les habitudes de navigation, et afficher des publicités ciblées. Plus vous avez d'extensions, plus votre empreinte est unique, et plus large est votre surface d'attaque. Vous voulez savoir à quel point il est facile de vous identifier et suivre sur la base de votre navigateur ? Rendez-vous sur [Couvrez vos traces de l'EFF](https://coveryourtracks.eff.org/).

    Soyez **prudent** avec les extensions. Certaines pourraient endommager l'affichage de sites web. Ajoutez vos extensions une à une et désactivez-les si vous constatez des impacts négatifs. Il peut s'avérer délicat de garder un équilibre entre utilisabilité et protection de vos données.

    Enfin, quelques suggestions pour celles et ceux qui utilisent les **réseaux sociaux**. Ne cochez pas les cases dans la section « Nuisances » de la liste des filtres de uBlock Origin si vous utilisez les boutons de partage de Facebook, Twitter et autres.


    | Extension | Description |
    | ------ | ------ |
    | [Clear URLs](https://addons.mozilla.org/fr/firefox/addon/clearurls/) | Supprime les traqueurs des URL. |
    | [Cookie autodelete](https://addons.mozilla.org/fr/firefox/addon/cookie-autodelete/) | Supprime automatiquement les cookies inutilisés à la fermeture des onglets. |
    | [I don't care about cookies](https://addons.mozilla.org/fr/firefox/addon/i-dont-care-about-cookies/) | Débarrassez-vous des avertissements concernant les cookies. |
    | [Miner block](https://addons.mozilla.org/fr/firefox/addon/minerblock-origin/) | Bloque les mineurs de cryptomonnaies. |
    | [Cloud firewall](https://addons.mozilla.org/fr/firefox/addon/cloud-firewall/) | Bloque les connexions aux services cloud de Google, Amazon, Facebook, Microsoft, Apple et Cloudflare. |
    | [CSS exfil protection](https://addons.mozilla.org/fr/firefox/addon/css-exfil-protection/) | Protège votre navigateur contre le vol des données par des pages web qui utilisent des CSS. |
    | [Disconnect](https://addons.mozilla.org/fr/firefox/addon/disconnect/) | Affiche et bloque les traqueurs sur le Web. |
    | [Noscript](https://noscript.net/) | Autorise seulement les sites web de confiance à exécuter du JavaScript, du Java, du Flash et autres modules. |
    | [HTTPS everywhere](https://addons.mozilla.org/fr/firefox/addon/https-everywhere/) | Chiffre le trafic web, rend la navigation plus sûre. |
    | [Privacy badger](https://addons.mozilla.org/fr/firefox/addon/privacy-badger17/) | Empêche les annonceurs publicitaires et les traqueurs tiers de vous espionner secrètement. |
    | [Decentraleyes](https://addons.mozilla.org/fr/firefox/addon/decentraleyes/) | Protège du pistage lié aux diffuseurs de contenus « gratuits », centralisés. |
    | [Terms of service; didn't read](https://addons.mozilla.org/fr/firefox/addon/terms-of-service-didnt-read/) | Vous informe instantanément sur les CGUs et politiques de données des sites, avec les notes et sommaires du projet. |


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Sécuriser Firefox" width="150px"></img> </center>

## Paramètres vie privée & sécurité

=== "Windows, macOS & Linux"

    Ouvrez un nouvel onglet dans Firefox. Enlevez tout ce qui peut encombrer les onglets vides, comme les `Top Sites` ou les `Mises en avant`. Ensuite, tapez `about:preferences` dans la barre d'adresse pour accéder aux paramètres de confidentialité et de sécurité de Firefox. Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

        *Attention* : Appliquez ces réglages avec précaution, certains pouvant endommager l'affichage de sites web. Ajoutez les nouveaux réglages progressivement, et désactivez-les si vous constatez des impacts négatifs.

        <center>

        | Menu | Paramètres |
        | ------ | ------ |
        | Général | Dans la section langue, décochez la case `Vérifier l'orthographe pendant la saisie` |
        | Général | Dans la section `Navigation`, décochez les deux cases `Recommander des extensions en cours de navigation` et `Recommander des fonctionnalités en cours de navigation`. |
        | Accueil | Dans la section `Contenu de la page d’accueil de Firefox`, décochez les cases `Raccourcis`, `Activités récentes` et `Brèves`. |
        | Recherche | Dans la section `Suggestions de recherche`, décochez la case `Afficher les suggestions de recherche`. |
        | Recherche | Dans la section `Raccourcis de recherche`, supprimez Google, Bing, Amazon et Ebay. |
        | Recherche | Allez sur la page [Disroot Searx](https://search.disroot.org/) et ajoutez-le comme moteur de recherche par défaut en faisant un clic droit ou en cliquant sur le menu `trois points` dans la barre d'adresse. <br><br> *Remarque* : vous trouverez d'autres suggestions de moteurs de recherche respectueux de la vie privée à la fin de cette section. |
        | Recherche | Retournez à la section `Moteur de recherche par défaut` des paramètres de Firefox et choisissez Disroot SearX. |
        | Vie privée et sécurité | La section `Amélioration de la protection contre le pistage` contient des réglages pour empêcher des tierces parties de vous suivre sur différents sites Web. Choisissez entre le paramétrage `Standard` ou `Strict`. Un paramétrage plus strict va bloquer plus de pisteurs et de publicités, mais a plus de chances de dégrader l'affichage de certains sites. |
        | Vie privée et sécurité | Dans la section `Protection contre le pistage`, sélectionnez également `Toujours` pour `Envoyer aux sites web un signal « Ne pas me pister » `.|
        | Vie privée et sécurité | Dans la section `Cookies et Données de sites`, sélectionnez `Effacer les cookies et les données des sites lorsque vous quittez Firefox`. Puis cliquez sur `Effacer les données` et effacez tous les cookies et toutes les données stockées par Firefox. |
        | Vie privée et sécurité | Dans la section `Identifiants et mots de passe`, décochez `Proposer de sauver les login et les mots de passe pour les sites web`. |
        | Vie privée et sécurité | Dans la section `Historique`, choisissez `Utiliser des paramètres personnalisés pour l'historique`. <br><br> Décochez la case `Se souvenir des historiques de navigation et de téléchargement`, et `Se souvenir des historiques de recherche et des formulaires`.<br><br> À la place, cochez la case `Supprimer l'historique à la fermeture de Firefox`. Puis cliquez sur `Effacer l'historique`, et effacez toutes les données stockées par Firefox. <br><br> *Remarque* : ceci est un contournement, car pour d'étranges raisons `Ne jamais se souvenir de l'historique` casse de nombreux ajouts. |
        | Vie privée et sécurité | Dans la section `Collecte de données par Firefox et utilisation`, décochez toutes les entrées. |
        | Vie privée et sécurité | Dans la section `Securité`, décochez l'entrée `Bloquer les contenus dangereux ou trompeurs`. Si ce paramètre permet de repérer les tentatives d'hameçonnage et les logiciels malveillants, il le fait en [établissant une connexion avec les serveurs de Google](https://support.mozilla.org/fr/kb/comment-fonctionne-protection-contre-hame%C3%A7onnage-et-logiciels-malveillants). |
        | Vie privée et sécurité | Dans la section `HTTPS-Only Mode`, sélectionnez `Activer le mode HTTPS-Only dans toutes les fenêtres`. Cela active le protocole de transfert Hypertexte sécurisé ([HTTPS](https://fr.wikipedia.org/wiki/HTTPS)) par défaut, une forme de chiffrement qui protège le trafic de navigation. Un cadenas (vert) devrait apparaître dans la barre d'adresse de Firefox à chaque fois que vous naviguez sur un site web. |
        | Vie privée et sécurité | Dans la section `Barre d'adresse`, désélectionnez `Suggestions contextuelles` et `Inclure des suggestions sponsorisées`. |
        | General | (Optionnel) Cochez la case `Toujours vérifier que Firefox est mon navigateur par défaut`, et cliquez sur `Définir par défaut`. |

        </center>


=== "Android"

    Démarrez Firefox. Sur l'écran de bienvenue, descendez et cliquez sur le bouton `Commencer la navigation`. Enlevez tout ce qui peut encombrer les onglets vides, comme `Google` ou `Meilleurs articles`. Ensuite, naviguez vers `Menu ‣ Configuration` et ajustez les paramètre de vie privée et de sécurité de Firefox. Vous trouverez des instructions détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        *Attention* : Appliquez ces réglages avec précaution, certains pouvant endommager l'affichage de sites web. Ajoutez les nouveaux réglages progressivement, et désactivez-les si vous constatez des impacts négatifs.

        <center>

        | Menu | Paramètres |
        | ------ | ------ |
        | Recherche | Dans la section `Moteur de recherche par défaut`, supprimez Google, Bing, Amazon, Qwant et eBay. |
        | Recherche | Dans la section `Moteur de recherche par défaut`, cliquez sur `+ Ajoutez un moteur de recherche`. Dans `Autre`, ajoutez ceci: <br><br> • `Nom`: `Disroot SearX`<br><br> • `Adresse de recherche à utiliser`: `https://search.disroot.org/search?q=%s` <br><br> Puis cliquez sur `🗸` pour appliquer vos changements. <br><br>*Remarque*: Vous trouverez plus de propositions sur les moteurs de recherche respectueux de la vie privée à la fin de cette section.  |
        | Recherche | Retournez dans la section `Moteur de recherche par défaut`, et sélectionnez `Disroot SearX`. |
        | Recherche | Dans la section `Barre d'adresse`, désactivez les options `Compléter les URLs`, `Affichez les suggestions du presse-papiers`, `Rechercher dans l'historique de recherche` et `Afficher les suggestions de recherche`. |
        | Personnalisation | Dans la section `Home`, désactivez `Affichez les sites les plus visités`. |
        | Logins et mots de passe | Changez l'option `Sauver les logins et les mots de passe` à `Ne jamais sauver`. |
        | Logins et mots de passe | Désactivez `Remplissage automatique`. |
        | Amélioration de la protection contre le pistage | Assurez-vous que l'option `Amélioration de la protection contre le pistage` est cochée pour empêcher des tierces parties de vous suivre sur différents sites Web. Choisissez entre le paramétrage `Standard` ou `Strict`. Un paramétrage plus strict va bloquer plus de pisteurs et de publicités, mais a plus de chances de dégrader l'affichage de certains sites. |
        | Effacer les données de navigation en quittant | Activez l'option `Effacer les données de navigation en quittant`. Si vous voulez que les onglets ouverts soient restaurés après la fermeture de Firefox, décochez l'option `Ouvrir les onglets`. |
        | Collecte de données | Désactivez `Utilisation et données techniques`, `Données marketing` ainsi que `Études`. |
        | Général | (Facultatif) Activez `Définir comme navigateur par défaut`. |

        </center>

<div style="margin-top: -20px;">
</div>

??? tip "Montrez-moi une vidéo récapitulative (3min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7e4abf0d-c96b-4455-bb44-6951e1b0619c" frameborder="0" allowfullscreen></iframe>

    </center>


??? info "Dites m'en plus sur les moteurs de recherche respectant la vie privée"

    <center>

    | Moteur de recherche | Description |
    | ------ | ------ |
    | [Searx](https://asciimoo.github.io/searx/) | Méta-moteur de recherche open source, qui agrège anonymement les résultats de plusieurs moteurs. [Plusieurs instances sont disponibles en ligne](https://searx.space), par exemple [Disroot](https://search.disroot.org/). Peut aussi être auto-hébergé. |
    | [Duckduckgo](https://duckduckgo.com/) | Méta-moteur basé aux États-Unis, qui agrège principalement les résultats de Bing et Yahoo. |
    | [Ecosia](https://www.ecosia.org/) | Méta-moteur de recherche allemand, qui fournit principalement des résultats de Bing et plante des arbres. |
    | [Swisscows](https://swisscows.com/) | Méta-moteur de recherche suisse, qui fournit principalement des résultats de Bing. |
    | [Mojeek](https://www.mojeek.com/) | Moteur de recherche basé au Royaume-Uni. |
    | [Metager](https://metager.de/) | Méta-moteur de recherche open source allemand. |
    | [Qwant](https://www.qwant.com/) | Méta-moteur de recherche français, financé par un investisseur en capital risque (la grande entreprise multimédia Axel-Springer). |
    | [Startpage](https://www.startpage.com/) | Méta-moteur de recherche hollandais, qui fournit principalement des résultats de Google. System 1, régie publicitaire, en est actionnaire depuis octobre 2019. |

    </center>


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Firefox sécurisé" width="150px"></img> </center>

## User.js

Firefox offre une grande diversité de paramètres pour gérer votre vie privée et sécurité. Sur les ordinateurs de bureau, ils peuvent être accédés en tapant `about:config` dans la barre d'adresse, et en confirmant l'avertissement de sécurité. Étant donné le grand nombre d'options, cela peut rapidement devenir fastidieux. Un moyen plus simple est d'installer un fichier `user.js`. Ce petit fichier JavaScript contient un certain nombre de paramètres prédéfinis, qui seront chargés chaque fois que vous utilisez Firefox. Vous trouverez plus de détails sur l'utilisation du `user.js` ci-dessous !

??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

    *Attention* : utilisez les fichiers `user.js` avec précaution. Plus le niveau de protection de données est élevé, plus vous risquez d'endommager l'affichage de certains sites web !

    Sauvegardez votre configuration actuelle, qui est stockée dans un fichier nommé `pref.js` :

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Étape 1 | Saisissez `about:support` dans la barre d'adresse de Firefox. |
    | Étape 2 | Allez dans `Paramètres de base de l’application`. |
    | Étape 3 | Cliquez sur `Ouvrir le dossier correspondant`. |
    | Étape 4 | Sauvegardez une copie du fichier `pref.js`. |

    </center>

    Téléchargez maintenant le modèle `user.js` de votre choix. Vous trouverez davantage de modèles disponibles à la fin de cette section. Placez le fichier `user.js` téléchargé dans le même répertoire que le fichier `pref.js`. Selon votre navigateur et votre système d'exploitation, le répertoire peut se trouver dans les emplacements suivants :

    <center>

    | Système d'exploitation | Emplacement |
    | ------ | ------ |
    | Windows | `%APPDATA%\Mozilla\Firefox\Profiles\XXXXXXXX.your_profile_name\user.js` |
    | macOS | `~/Library/Application Support/Firefox/Profiles/XXXXXXXX.your_profile_name` |
    | Linux (Ubuntu) | `~/.mozilla/firefox/XXXXXXXX.default-release/user.js` |

    </center>

    Si à un moment quelconque vous devez revenir en arrière pour retrouver les paramètres initiaux, il vous suffit de restaurer le fichier `pref.js` et de supprimer le fichier `user.js`.


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4b9255d5-3d9f-4319-bd3a-8214a396df18" frameborder="0" allowfullscreen></iframe>

    </center>


??? question "Où puis-je trouver des modèles `user.js` ?"

    * [Arkenfox](https://github.com/arkenfox/user.js) procure un bon équilibre entre vie privée et utilisabilité. Pour en savoir plus consultez la [page wiki](https://github.com/arkenfox/user.js/wiki)
    * [Pyllyukko](https://github.com/pyllyukko/user.js/)
    * [Relaxed Pyllyukko](https://github.com/pyllyukko/user.js/tree/relaxed/)
    * [Privacy Handbuch minimal configuration (en allemand)]( https://www.privacy-handbuch.de/download/minimal/user.js)
    * [Privacy Handbuch moderate configuration (en allemand)]( https://www.privacy-handbuch.de/download/moderat/user.js)
    * [Privacy Handbuch strict configuration (en allemand)]( https://www.privacy-handbuch.de/download/streng/user.js)
    * [Un outil pour créer votre fichier user.js personnel](https://ffprofile.com/)
    * [Des explications sur les paramètres courants du fichier user.js](http://kb.mozillazine.org/About:config_Entries/)


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Firefox" width="150px"></img> </center>

## Assistance

Si vous avez des questions ou désirez davantage de détails, consultez:

* le [site d'assistance et de documentation de Firefox](https://support.mozilla.org/fr/products/firefox/get-started) ou demandez de l'aide à la [communauté Firefox](https://support.mozilla.org/fr/questions/new).

* la [documentation officielle d'uBlock Origin](https://github.com/gorhill/uBlock/wiki/) ou l'un des [tutoriels en ligne](https://www.maketecheasier.com/ultimate-ublock-origin-superusers-guide/) si vous souhaitez utiliser des fonctionnalités avancées.


<center>
<img align="center" src="https://imgs.xkcd.com/comics/perspective.png" alt="Firefox"></img>
</center>

<br>

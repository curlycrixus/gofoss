---
template: main.html
title: Per Anhalter durchs Fediverse
description: Wie funktioniert das Fediverse? Mastodon vs Twitter. PeerTube vs YouTube. PixelFed vs Instagram. Friendica vs Facebook. Lemmy vs Reddit. Funkwhale vs Spotify.
---

# Per Anhalter durchs Fediverse

!!! level "Letzte Aktualisierung: Mai 2022. Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/fediverse.png" alt="Fediverse" width="400px"></img>
</div>

Das Föderierte Universum, kurz Fediverse, ist eine freie und quelloffene Alternative zu kommerziellen Social-Media-Plattformen.

Das Fediverse besteht aus verschiedenen miteinander verbundenen sozialen Netzwerken und Mikro-Blogging-Diensten, in denen die NutzerInnen frei kommunizieren und Bilder, Videos, Musik und vieles mehr austauschen können. Zum Zeitpunkt der Abfassung dieses Textes [zählte das Fediverse über 5 Millionen NutzerInnen](https://the-federation.info/). Das Fediverse ist:

* **Dezentral** – Statt eines einzigen Unternehmens bieten viele verschiedene [alternative Anbieter](https://gofoss.net/de/cloud-providers/) Zugang zum Fediverse. Jeder kann einen Server oder eine sogenannte Instanz betreiben, um einer Gemeinschaft Dienste anzubieten. Das ist so, als könnte jeglicher NutzerIn oder jedwede Organisation einen eigenen Twitter-, Facebook-, YouTube- oder Instagram-Server betreiben und Freunde einladen.
* **Föderiert** – Dank des [ActivityPub Protokolls](https://activitypub.rocks/) können NutzerInnen verschiedener Dienste miteinander kommunizieren. Das ist so, als ob Twitter-, Facebook-, Instagram- oder YouTube-NutzerInnen frei miteinander interagieren könnten.
* **Nicht böse** – Das Fediverse ist nicht darauf optimiert, ständig die Aufmerksamkeit seiner NutzerInnen zu erhaschen, Profile zu erstellen und so viel Reklame und bezahlte Inhalte wie nur möglich zu servieren. Also [ganz das Gegenteil](https://quitsocialmedia.club/why) zu Twitter, Facebook, YouTube, Instagram, Reddit und Co.
* **Menschennah** – Im Fediverse gibt es keine Filter, die entscheiden was NutzerInnen in ihren Zeitleisten zu sehen bekommen, und was zensiert wird. Keine Algorithmen, die den NutzerInnen kontroverse Inhalte aufzwingen, um sie auf Trab zu halten. Keine dunklen Muster, die das Verhalten der NutzerInnen beeinflussen. Auch darin unterscheidet sich das Fediverse von den Datenkraken.
* **Befähigend** – NutzerInnen werden nicht dazu genötigt, Ihre Urheberrechte aufzugeben. Es gibt keine willkürlichen und undurchsichtigen Nutzungsbedingungen oder Moderationsrichtlinien. Keine digitalen Mauern zwischen sozialen Netzwerken. Im Gegensatz zu kommerziellen Plattformen, die von den Technologie-Giganten betrieben werden.


<br>


<center> <img src="../../assets/img/separator_mastodon.svg" alt="Mastodon" width="150px"></img> </center>

## Mastodon

<img src="../../assets/img/fediverse_mastodon.jpg" alt="Mastodon" width="300px" align="left"></img> [Mastodon](https://joinmastodon.org/) ist die Fediverse-Alternative zu Twitter. Es ist ein Micro-Blogging-Dienst, bei dem NutzerInnen anderen NutzerInnen folgen, 500-Zeichen-Nachrichten teilen (sogenannte *Toots*), Inhalte teilen (sogenannte *Boosts*) und so weiter. Mastodon wird seit 2016 entwickelt, ist völlig [quelloffen](https://github.com/mastodon/mastodon) und kann selbst gehostet werden. Zum Zeitpunkt der Abfassung dieses Textes zählte Mastodon fast 2 Millionen NutzerInnen.

<br>

??? tip "Legt los mit Mastodon!"

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Benutzerkonto erstellen |Jeder Mastodon-Benutzer erhält eine eindeutige Kennung, die einer E-Mail-Adresse ziemlich ähnlich sieht: `@benutzername@instanz.social`. Ihr könnt jederzeit eine neue Mastodon-Kennung bei einer beliebigen Instanz Eurer Wahl erstellen, oder sogar Eure eigene Mastodon-Instanz hosten. Eine Auswahl aktiver Instanzen findet Ihr hier: <br>• [Mastodon Gemeinschaften](https://joinmastodon.org/communities) <br>• [Mastodon Instanz Liste](https://instances.social/) |
    |Anmelden |Meldet Euch über Mastodons Weboberfläche oder einen der Mastodon-Clients bei Eurer Instanz an. Ihr habt nun Zugriff auf Eure Zeitleisten, in denen Beiträge von Nutzern Eurer Instanz oder anderer Instanzen angezeigt werden. |
    |Eine Nachricht posten |• Weboberfläche: verwendet das Nachrichtenfeld in der linken Leiste <br>• Client-Apps: klickt auf das `Stift`-Symbol <br>• Verfasst Euren Toot <br>• Fügt optional Links, Mediendateien, Erwähnungen, Emojis, Umfragen oder Inhaltswarnungen hinzu <br>• Legt fest, wer Euren Beitrag sehen kann |
    |Datenschutz-Einstellungen |• `Öffentliche` Nachrichten sind für alle sichtbar <br>• `Ungelistete` Nachrichten sind lediglich sichtbar für Eure Follower, im Beitrag erwähnte BenutzerInnen oder BenutzerInnen, die Euer Profil aufsuchen <br>• `Follower-exklusive` Nachrichten sind ausschließlich für Eure Follower oder im Beitrag erwähnte BenutzerInnen sichtbar <br>• `Private` Nachrichten sind nur für die im Beitrag erwähnten Benutzer sichtbar <br>• Unabhängig von den Datenschutzeinstellungen können Serveradministratoren alle Nachrichten einsehen, Inhalte werden nicht verschlüsselt |
    |Inhaltswarnungen |Heikle Inhalte können ausgeblendet werden, solange NutzerInnen nicht auf einen gekennzeichneten Link klicken. Dies kann nützlich sein, wenn Ihr heikle Inhalte mit Eurem Publikum teilen wollt |
    |Hashtags |Ihr könnt Hashtags `#` in Eure Toots einfügen, oder auf Wörter mit einem vorangestellten Hashtag klicken, um ähnliche Inhalte aufzuspüren. |
    |Clients |[Pinafore](https://pinafore.social/) und [Halcyon](https://www.halcyon.social) sind "Twitter-ähnliche" Web-Clients. Darüber hinaus verfügt Mastodon über verschiedenste [Mobile- und Desktop-Clients](https://joinmastodon.org/apps). Funktioniert auch auf degoogelten Handys mit [CalyxOS](https://gofoss.net/de/calyxos/) oder [LineageOS](https://gofoss.net/de/lineageos/). |


<br>


<center> <img src="../../assets/img/separator_peertube.svg" alt="PeerTube" width="150px"></img> </center>

## PeerTube

<img src="../../assets/img/fediverse_peertube.png" alt="Peertube" width="300px" align="left"></img> [Peertube](https://joinpeertube.org/de) ist die Fediverse-Alternative zu YouTube, Vimeo oder Dailymotion. Es handelt sich um eine Medien-Streaming-Plattform, auf der NutzerInnen Videos ansehen, "liken" oder hochladen können. PeerTube wird seit 2015 entwickelt, ist völlig [quelloffen](https://github.com/Chocobozzz/PeerTube) und kann selbst gehostet werden. Zum Zeitpunkt der Abfassung dieses Textes zählte PeerTube fast 200.000 NutzerInnen.

<br>

??? tip "Legt los mit PeerTube!"

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Benutzerkonto erstellen |Erstellt ein neues PeerTube-Konto bei einer beliebigen Instanz Eurer Wahl, oder hostet Eure eigene PeerTube-Instanz. Eine Auswahl aktiver Instanzen findet Ihr hier: <br>• [PeerTube instances](https://instances.joinpeertube.org/instances) <br>• [FediDB](https://fedidb.org/network/?s=peertube) |
    |Anmelden |Meldet Euch über PeerTubes Weboberfläche oder einen der PeerTube-Clients bei Eurer Instanz an. Ihr habt nun Zugriff zu Eurem Profil sowie unzähligen Videos. |
    |Videos ansehen |Die Benutzeroberfläche ist ziemlich intuitiv: Videos abspielen und anhalten, die Lautstärke einstellen, die Videoqualität und -geschwindigkeit ändern, Untertitel einblenden, in den Vollbildmodus wechseln, Videos herunterladen und teilen, usw. |
    |Videos veröffentlichen |<br>• Tippt auf die Schaltfläche `Veröffentlichen` <br>• Ladet eine Videodatei hoch, entweder von Euren Geräten, über eine URL oder über einen Torrent-Link <br>• Videos von YouTube, Vimeo, Dailymotion und so weiter können problemlos importiert werden, gebt hierzu einfach die entsprechende URL in der Rubrik `Importieren über eine URL` an <br>• PeerTube unterschützt verschiedene Formate: mp4, ogv, webm, flv, mov, avi, mkv, mp3, ogg & flac <br>• Fügt Information zu Euren Uploads hinzu: Titel, Tags, Beschreibungen, Kategorien, Lizenzen, usw. <br>• Wählt einen Kanal <br>• Passt die Datenschutz-Einstellungen an <br>• Klickt auf die `Veröffentlichen`-Schaltfläche <br>• Klickt auf die `Teilen`-Schaltfläche, um Freunde und Kontakte über das Video zu informieren <br>• Um das Video in Euren Blog oder Eure Webseite einzubetten, klickt auf `Teilen ‣ Einbetten` und kopiert den Link |
    |Kanäle |PeerTube bietet sogenannte "Kanäle" an, um Videos nach Themen zu gruppieren. Verwaltet Eure Kanäle unter `Meine Bibliothek ‣ Kanäle`. |
    |Playlisten |PeerTube bietet ebenfalls sogenannte "Playlisten" an, um Video-Sammlungen zu erstellen. Playlisten können privat gehalten oder mit anderen NutzerInnen geteilt werden. Verwaltet Eure Playlisten unter `Meine Bibliothek ‣ Playlists` |
    |Sich abonnieren |Klickt auf die `Abonnieren`-Schaltfläche, um Kanälen oder Playlists anderer NutzerInnen zu folgen. Ihr könnt Euch mit Eurem lokalen PeerTube-Konto, anderen Fediverse-Konten (z.B. Mastodon) oder über RSS abonnieren. |
    |Videos suchen |Gebt Stichworte in die Suchleiste ein, um nach Videos, Kanälen oder Playlisten zu suchen. Je nach den Einstellungen Eurer PeerTube-Instanz werden Ergebnisse verschiedener Instanzen, oder aber lediglich Eurer Instanz angezeigt. Alternativ könnt Ihr auch eine Suchmaschine wie [Sepia Search](https://search.joinpeertube.org/de/) nutzen. |
    |Datenschutz-Einstellungen |• `Öffentliche` Videos sind für alle sichtbar <br>• `Interne` Videos sind nur für NutzerInnen Eurer Instanz sichtbar <br>• `Ungelistete` Videos sind lediglich für NutzerInnen mit dem privaten Link sichtbar <br>• `Private` Videos sind nur für Euch sichtbar |
    |Weitere Einstellungen |In der Rubrik `Mein Konto ‣ Einstellungen` könnt Ihr Euer Profil aktualisieren, Passwörter ändern, den verfügbaren Speicherplatz einsehen, den Inhaltsschutz festlegen, die Sprache auswählen, Benachrichtigungen verwalten, Euer Konto löschen und vieles mehr. |
    |Clients |Es sind verschiedene [Mobile oder Desktop-Clients für PeerTube](https://docs.joinpeertube.org/use-third-party-application) verfügbar. |


<br>


<center> <img src="../../assets/img/separator_pixelfed.svg" alt="Pixelfed" width="150px"></img> </center>

## PixelFed

<img src="../../assets/img/fediverse_pixelfed.png" alt="Pixelfed" width="200px" align="left"></img> [PixelFed](https://pixelfed.org/) ist die Fediverse-Alternative zu Instagram. Es handelt sich um ein soziales Netzwerk, in dem NutzerInnen Bilder und Videos teilen, "liken" und kommentieren. PixelFed wird seit 2018 entwickelt, ist völlig [quelloffen](https://github.com/pixelfed/pixelfed) und kann selbst gehostet werden. Zum Zeitpunkt der Abfassung dieses Textes zählte PixelFed über 50.000 NutzerInnen.


<br>

??? tip "Legt los mit PixelFed!"

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Benutzerkonto erstellen |Erstellt ein neues PixelFed-Konto bei einer beliebigen Instanz Eurer Wahl, oder hostet Eure eigene PixelFed-Instanz. Hier eine [Auswahl aktiver PixelFed-Instanzen](https://fedidb.org/software/pixelfed). |
    |Anmelden |• Meldet Euch über PixelFeds Weboberfläche oder einen der PixelFed-Clients bei Eurer Instanz an <br>• Klickt auf `Menü ‣ Einstellungen ‣ Konto` um Profilfoto, Namen, Passwort usw. anzupassen |
    |Zeitleisten |• Klickt auf `Menü ‣ My Feed`, um Posts anderer NutzerInnen anzuzeigen, denen Ihr folgt <br>• Klickt auf `Menü ‣ Public Feed`, um Posts anderer NutzerInnen Eurer Instanz anzuzeigen <br>• Klickt auf `Menü ‣ Network Feed` um Posts anderer Instanzen anzuzeigen |
    |Bilder und Videos veröffentlichen |• Klickt auf die `New`-Schaltfläche, um ein oder mehrere Fotos oder Videos hochzuladen <br>• PixelFed unterstützt verschiedene Formate: jpeg, gif, png, mp4 or webp <br>• Optional könnt Ihr Beschreibungen, Inhaltswarnungen, Tags, Lizenzen oder Standorte hinzufügen <br>• Legt fest, wer Eure Bilder sehen kann <br>• `Öffentliche` Bilder und Videos sind für alle sichtbar <br>• `Ungelistete` Bilder und Videos sind für Eure Followers sowie auf Eurem Profil sichtbar <br>• `Follower-exklusive` Bilder und Videos sind nur für Eure Follower sichtbar |
    |Leute und Inhalte entdecken |Klickt auf `Menü ‣ Entdecken`, um aktuelle Themen und empfohlene Inhalte zu entdecken. |
    |Direktnachrichten |Klickt auf die `Direkt`-Schaltfläche, um anderen NutzerInnen Direktnachrichten zu schicken. Diese können Fotos, Videos, Posts, Text und so weiter enthalten. |
    |Hashtags |Ihr könnt Hashtags `#` in Eure eigenen Posts einfügen oder Hashtags anderer NutzerInnen folgen, um informiert zu bleiben. |
    |Stories |Klickt auf `Neu ‣ Neue Story`, um während 24 Stunden besondere Momente mit Euren Followern zu teilen. |
    |Sammlungen |Klickt auf `Neu ‣ Neue Sammlung` um Posts in eine Sammlung zu gruppieren. |
    |Datenschutz-Einstellungen |• Klickt auf `Menü ‣ Einstellungen ‣ Datenschutz` <br>• Markiert `Privates Konto`, um den Zugriff auf Eure Fotos und Videos auf zugelassene Follower zu beschränken <br>• Markiert `Aus der Suchmaschinenindizierung ausschließen` um Eure Fotos und Videos vor Suchmaschinen zu verbergen |
    |Zwei-Faktor-Authentifizierung (2FA) |• Klickt auf `Menü ‣ Einstellungen ‣ Sicherheit` <br>• Gebt Euer Passwort ein <br>• Klickt auf `Zwei-Faktor-Authentifizierung aktivieren` <br>• Scannt den QR-Code mit Eurer mobilen 2FA-App, z.B. mit [andOTP](https://gofoss.net/de/passwords/#2fa-mobile-clients) <br>• Gebt den 6-stelligen Code in PixelFeds Eingabemaske ein <br>• [Bewahrt die Backup-Tokens sicher auf](https://gofoss.net/de/passwords/#keepass), die PixelFed bereitstellt. Diese erlauben es Euch, den Zugang zu Eurem Konto auch ohne Telefon wiederherzustellen |
    |Clients |[PixelDroid](https://f-droid.org/de/packages/org.pixeldroid.app/) ist ein mobiler Android-Client. |

<br>


<center> <img src="../../assets/img/separator_lemmy.svg" alt="Lemmy" width="150px"></img> </center>

## Lemmy

<img src="../../assets/img/fediverse_lemmy.webp" alt="Lemmy" width="300px" align="left"></img> [Lemmy](https://join-lemmy.org/) ist die Fediverse-Alternative zu Reddit. Es handelt sich um ein soziales Netzwerk, in dem NutzerInnen Gemeinschaften beitreten und Status-Updates und Bilder posten, "liken" oder kommentieren. Lemmy wird seit 2019 entwickelt, ist völlig [quelloffen](https://github.com/LemmyNet/lemmy) und kann selbst gehostet werden. Zum Zeitpunkt der Abfassung dieses Textes zählte Lemmy über 20.000 NutzerInnen.

<br>

??? tip "Legt los mit Lemmy!"

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Benutzerkonto erstellen |Erstellt ein neues Lemmy-Konto bei einer beliebigen Instanz Eurer Wahl, oder hostet Eure eigene Lemmy-Instanz. Hier eine Auswahl aktiver Instanzen: <br>• [Lemmy-Server](https://join-lemmy.org/instances) <br>• [FediDB](https://fedidb.org/software/lemmy) |
    |Anmelden |Meldet Euch über Lemmys Weboberfläche oder einen der Lemmy-Clients bei Eurer Instanz an. |
    |Abonnieren |• Klickt auf `Menü ‣ Communitys` <br>• Abonniert Euch bei Gemeinschaften, die Euch interessieren |
    |Beiträge veröffentlichen |• Klickt auf `Menü ‣ Beitrag erstellen` <br>• Fügt eine URL, ein Bild, einen Titel oder Text hinzu <br>• Wählt eine Community <br>• Fügt optional eine Inhaltswarnung bei <br>• Klickt auf `Erstellen` |
    |Andere Einstellungen |In der Rubrik `Menü ‣ Benutzername ‣ Einstellungen` könnt Ihr Euer Profil aktualisieren, Passwörter ändern, Inhaltswarnungen anpassen, Euer Konto löschen und so fort. |
    |Clients |Es sind verschiedene [Mobile oder Desktop-Clients für Lemmy](https://join-lemmy.org/apps) verfügbar. |

<br>


<center> <img src="../../assets/img/separator_friendica.svg" alt="Friendica" width="150px"></img> </center>

## Friendica

<img src="../../assets/img/fediverse_friendica.png" alt="Mastodon" width="300px" align="left"></img> [Friendica](https://friendi.ca/) ist die Fediverse-Alternative zu Facebook. Es handelt sich um ein soziales Netzwerk, in dem sich NutzerInnen in Foren anmelden, um Links und Diskussionen zu posten, zu kommentieren und darüber abzustimmen. Friendica wird seit 2010 entwickelt, ist völlig [quelloffen](https://github.com/friendica/friendica) und kann selbst gehostet werden. Zum Zeitpunkt der Abfassung dieses Textes zählte Friendica über 10.000 NutzerInnen.

<br>

??? tip "Legt los mit Friendica!"

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Benutzerkonto erstellen |Erstellt ein neues Friendica-Konto bei einer beliebigen Instanz Eurer Wahl, oder hostet Eure eigene Friendica-Instanz. Hier eine Auswahl aktiver Instanzen: <br>• [Friendica Verzeichnis](https://dir.friendica.social/servers) <br>• [FediDB](https://fedidb.org/network/?s=friendica) |
    |Anmelden |Meldet Euch über Friendicas Weboberfläche oder einen der Friendica-Clients bei Eurer Instanz an. Ihr habt nun Zugriff auf Eure Profilseite, Statusmeldungen sowie Beiträge. |
    |Status teilen |• Klickt auf das `Stift & Papier`-Symbol oder die `Teilen`-Schaltfläche <br>• Verfasst ein Statusupdate <br>• Fügt optional Links, Mediendateien oder Ortsangaben bei <br>• Legt fest, wer Euren Post sehen kann |
    |Netzwerk Rubrik |Hier könnt Ihr Beiträge von Freunden oder Gruppen verfolgen, kommentieren oder "liken". |
    |Sich mit Leuten vernetzen |Sucht nach anderen NutzerInnen in [Friendicas Nutzerverzeichnis](http://dir.friendica.social/). Friendica macht Euch ebenfalls Freundschaftsvorschläge. |
    |Gruppen, Foren & Community-Seiten |Wenn Ihr noch niemanden auf Friendica kennt, könnt Ihr Euch verschiedenen Gruppen, Foren oder Community-Seiten anschließen. So entdeckt Ihr vielleicht Leute mit gleichen Interessen und könnt Euch vernetzen. |
    |Schnittstellen |Je nach Einstellung Eurer Instanz könnt Ihr ggf. [Diensten außerhalb von Friendica interagieren](https://forum.friendi.ca/help/Connectors), wie z.B. Twitter, Diaspora, GNU Social oder RSS-Feeds. |
    |Clients |Es sind verschiedene [Mobile oder Desktop-Clients für Friendica](https://friendi.ca/resources/mobile-clients/) verfügbar. |


<br>


<center> <img src="../../assets/img/separator_funkwhale.svg" alt="Funkwhale" width="150px"></img> </center>

## Funkwhale

<img src="../../assets/img/fediverse_funkwhale.png" alt="Funkwhale" width="300px" align="left"></img> [Funkwhale](https://funkwhale.audio/) ist die Fediverse-Alternative zu YouTube, Spotify oder SoundCloud. Es handelt sich um ein soziales Netzwerk, in dem NutzerInnen Musik, Podcasts und Radiosendungen anhören, "liken" oder hochladen. Funkwhale wird seit 2015 entwickelt, ist völlig [quelloffen](https://dev.funkwhale.audio/funkwhale/funkwhale) und kann selbst gehostet werden. Zum Zeitpunkt der Abfassung dieses Textes zählte Funkwhale fast 5.000 NutzerInnen.

<br>

??? tip "Legt los mit Funkwhale!"

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Taggen |Bevor Ihr mit Funkwhale loslegt, solltet Ihr sicherstellen, dass Eure Musiksammlung sorgfältig getaggt ist. Sorgfältiges Taggen erleichtert es Funkwhale, nützliche Informationen wie Albencover oder Künstlerdaten anzuzeigen. [Picard](https://picard.musicbrainz.org/) ist ein plattformübergreifender Musik-Tagger, der Euch bei der Verwaltung Eurer Musiksammlung helfen kann. |
    |Benutzerkonto erstellen |Erstellt ein neues Funkwhale-Konto bei einer beliebigen Instanz Eurer Wahl, oder hostet Eure eigene Funkwhale-Instanz. Hier eine Auswahl aktiver Instanzen:<br>• [Funkwhale-Server](https://network.funkwhale.audio/dashboards/d/overview/funkwhale-network-overview?orgId=1&refresh=2h) <br>• [FediDB](https://fedidb.org/network/?s=funkwhale) |
    |Inhalte hochladen |• Meldet Euch über Funkwhales Weboberfläche oder einen der Funkwhale-Clients bei Eurer Instanz an <br>• Klickt auf das `Hochladen`-Symbol <br>• Wählt einen Kanal (um Eure eigenen Musik zu veröffentlichen) oder eine Mediathek (für öffentliche und/oder private Musiksammlungen) |
    |Kanäle |Funkwhale bietet sogenannte "Kanäle" an, die es Euch ermöglichen Eure eigens produzierte Musik zu veröffentlichen und zu verwalten. Kanäle können von verschiedenen Funkwhale-Instanzen aus verfolgt werden, sowie von anderen Fediverse-Diensten (z.B. Mastodon). Klickt einfach auf `Abonnieren` um Kanälen anderer NutzerInnen zu folgen. Um Eure eigenen Kanäle zu verwalten, kickt auf `Hochladen ‣ Veröffentliche deine Arbeit in einem Kanal ‣ Loslegen`. |
    |Mediathek |Funkwhale bietet ebenfalls sogenannte "Mediatheken" an, die es Euch erlauben öffentliche sowie persönliche Musiksammlungen zu verwalten. Verwaltet Eure Mediatheken unter `Hochladen ‣ Lade Inhalte von Drittanbietern in eine Mediathek hoch ‣ Loslegen`. Mediatheken verfügen über verschieden Datenschutz-Einstellungen: <br>• `Öffentliche` Mediatheken sind für alle sichtbar <br>• `Lokale` Mediatheken sind für NutzerInnen Eurer Instanz sichtbar <br>•  `Private` Mediatheken sind nur für Euch sichtbar <br>• Unabhängig von den Datenschutz-Einstellungen können Links zu Mediatheken mit bestimmten NutzerInnen geteilt werden, um Ihnen Zugang zu gewähren <br><br> **Hinweis**: Beachtet, dass Ihr stets über entsprechende Urheberrechte verfügen solltet, insbesondere bei Verwendung öffentlicher oder lokaler Mediatheken. |
    |Playlisten |Funkwhale bietet ebenfalls sogenannte Wiedergabelisten an, die sowohl Eure eigene Musik als auch Musik anderer NutzerInnen beinhalten können. Verwaltet Eure Playlisten unter `Menü ‣ Wiedergabelisten`. |
    |Clients |Es sind verschiedene [Mobile oder Desktop-Clients für Funkwhale](https://funkwhale.audio/de/apps/) verfügbar. |

<br>


<center> <img src="../../assets/img/separator_permissions.svg" alt="Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten finden Ihr in:

* [Mastodons Dokumentation](https://docs.joinmastodon.org/) oder [Forum](https://discourse.joinmastodon.org/)
* [PeerTubes Dokumentation](https://docs.joinpeertube.org/), [FAQ](https://joinpeertube.org/faq) oder [Forum](https://framacolibri.org/c/peertube/38)
* [PixelFeds Dokumentation](https://docs.pixelfed.org/) oder [Hilfeseite](https://pixelfed.social/site/help)
* [Friendicas Dokumentation](https://friendi.ca/resources/use-it/) oder [Hilfeseite](https://forum.friendi.ca/help/)
* [Lemmys Dokumentation](https://join-lemmy.org/docs/en/index.html)
* [Funkwhales Dokumentation](https://docs.funkwhale.audio/) oder [Forum](https://forum.funkwhale.audio/)

<center>
<img align="center" src="https://imgs.xkcd.com/comics/social_media.png" alt="Fediverse"></img>
</center>

<br>
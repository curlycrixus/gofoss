---
template: main.html
title: L'équipe du projet
description: gofoss.net est un projet géré par des bénévoles et à but non lucratif. Rencontrez l'équipe. Georg Jerska. Lenina Helmholtz. Tom Parsons.
---

# L'équipe du projet gofoss.net

## Georg Jerska

<img src="../../assets/img/georg.png" alt="Georg" width="150px" align="left"></img> Georg travaille dans les arts du spectacle. Il a passé une partie de sa carrière en Allemagne, et s'intéresse particulièrement à la protection de la vie privée des citoyens.

<br><br>

## Lenina Helmholtz

<img src="../../assets/img/lenina.png" alt="Georg" width="150px" align="left"></img> Lenina a un parcours technique et donne des cours à l'université. Elle est toujours curieuse de découvrir de nouveaux outils du logiciel libre.

<br><br>

## Tom Parsons

<img src="../../assets/img/tom.png" alt="Georg" width="150px" align="left"></img> Tom est très social et croit en un Internet meilleur. Il espère contribuer au bon fonctionnement du projet.

<br><br>
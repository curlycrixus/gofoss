---
template: main.html
title: Jellyfin | Self-host Your Home Media Server
description: Looking for a FOSS alternative to Plex media server? Learn how to setup a Linux media server in this Jellyfin review.
---

# Jellyfin, a Self-Hosted Home Media Server

!!! level "Last updated: May 2022. For advanced users. Solid tech skills required."

<center>
<img align="center" src="../../assets/img/jellyfin.png" alt="Jellyfin vs Plex" width="550px"></img>
</center>

<br>

Looking for a FOSS alternative to Plex media server? [Jellyfin](https://jellyfin.org/) is a self-hosted digital media solution for your [Ubuntu server](https://gofoss.net/ubuntu-server/). Enjoy movies, TV shows and music on your media streaming devices. Jellyfin is free, open source, fast and beautiful. And it supports multiple users, Live TV, metadata management, customisation & plugins, various clients, and more.

<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Jellyfin install" width="150px"></img> </center>

## How to install Jellyfin

Jellyfin's installation process is pretty straight forward. More details below.

??? tip "Show me the step-by-step guide"

    Log into the server, then run the following commands to add the repository to the apt source list and install the software:

    ```bash
    sudo apt install apt-transport-https
    sudo apt-get install software-properties-common
    sudo add-apt-repository universe
    curl -fsSL https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/debian-jellyfin.gpg
    sudo echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/ubuntu $( lsb_release -c -s ) main" | sudo tee /etc/apt/sources.list.d/jellyfin.list
    sudo apt update
    sudo apt install jellyfin
    ```


??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/65b649cf-9b0b-4448-8823-0162e51fd9f0" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Temporary workaround for Ubuntu 22.04"

    At the time of writing, Jellyfin can't be installed from Ubuntu 22.04 repositories using the instructions outlined above. This is mainly due to the fact that Jellyfin 10.7.7 depends on `libssl1`, which is [no longer supported by Ubuntu 22.04](https://github.com/jellyfin/jellyfin/pull/7648). Instead, install [Jellyfin 10.8.0 Beta 3](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.0-beta3) as a temporary workaround.

    Enable the Universe repositories:

    ```bash
    sudo add-apt-repository universe
    sudo apt update
    ```

    Download the Jellyfin `.deb` packages:

    ```bash
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/10.8.0-beta3/web/jellyfin-web_10.8.0~beta3_all.deb
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/10.8.0-beta3/server/jellyfin-server_10.8.0~beta3_amd64.deb
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/ffmpeg/jellyfin-ffmpeg5_5.0.1-4-jammy_amd64.deb
    ```

    Install the required dependencies:

    ```bash
    sudo apt install at libsqlite3-0 libfontconfig1 libfreetype6
    ```

    Install the downloaded `.deb` packages:


    ```bash
    sudo dpkg -i jellyfin-*.deb
    ```

    Fix missing dependencies:

    ```bash
    sudo apt -f install
    ```

    Restart the Jellyfin service and make sure the status is `Active`:

    ```bash
    sudo systemctl restart jellyfin
    sudo systemctl status jellyfin
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Jellyfin Ubuntu" width="150px"></img> </center>

## Setting up the home media server

After successfully installing Jellyfin, we're going to configure a number of settings such as the auto start on boot, user accounts, language preferences, and so on. More details below.

??? tip "Show me the step-by-step guide"

    ### Auto start

    Let's make sure Jellyfin automatically starts every time the server boots:

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable jellyfin
    ```

    Reboot the server:

    ```bash
    sudo reboot
    ```

    Make sure Jellyfin is up and running (the status should be "Active"):

    ```bash
    sudo systemctl status jellyfin
    ```

    ### Initial Jellyfin setup

    We're going to access Jellyfin's web interface to perform the initial setup. For this, we need to temporarily open port `8096` on the firewall:

    ```bash
    sudo ufw allow 8096/tcp
    ```

    Now browse to `http://192.168.1.100:8096` (adjust accordingly) and follow the initial setup wizard:

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Preferred display language |Select your preferred language, for example English. |
    | Username |Provide a username for the Jellyfin administrator. For the purpose of this tutorial, we'll call this administrator `jellyfinadmin`. Of course, you can choose any name, just make sure to adjust the commands accordingly.  |
    | Password |Provide a [strong, unique password](https://gofoss.net/passwords/) for the Jellyfin administrator. |
    | Add Media Libraries |You can add music, movies or TV shows to Jellyfin now, or at any later time from the dashboard. Media should be stored on the file system: the server's storage, an external drive connected to the server or a  network storage device directly mounted to the OS.|
    | Select preferred Metadata Language |Select your preferred language, for example English. |
    | Allow remote connections to this Jellyfin media server |No. |

    </center>

    Once the initial setup is done, close the port `8096`:

    ```
    sudo ufw delete allow 8096/tcp
    ```


??? tip "Show me the 2-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/006c7186-00ea-4c0e-b804-6a7b8629f3e0" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Jellyfin remote access" width="150px"></img> </center>

## Web interface

We are going to set up an Apache Virtual Host as a Reverse Proxy to access Jellyfin's web interface. Read on below for more details on how to set this up.

??? tip "Show me the step-by-step guide"

    Create an Apache configuration file:

    ```bash
    sudo vi /etc/apache2/sites-available/mymedia.gofoss.duckdns.org.conf
    ```

    Add the following content and make sure to adjust the settings to your own setup, such as domain names (`mymedia.gofoss.duckdns.org`), path to SSL keys, IP addresses and so on:

    ```bash
    <VirtualHost *:80>

      ServerName mymedia.gofoss.duckdns.org
      ServerAlias www.mymedia.gofoss.duckdns.org
      Redirect permanent / https://mymedia.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

      ServerName  mymedia.gofoss.duckdns.org
      ServerAlias www.mymedia.gofoss.duckdns.org
      ServerSignature Off

      SecRuleEngine Off
      SSLEngine On
      SSLCertificateFile  /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
      SSLCertificateKeyFile /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
      DocumentRoot /var/www

      <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
      </Location>

      ProxyPreserveHost On
      ProxyPass "/socket" "ws://127.0.0.1:8096/socket"
      ProxyPassReverse "/socket" "ws://127.0.0.1:8096/socket"
      ProxyPass "/" "http://127.0.0.1:8096/"
      ProxyPassReverse "/" "http://127.0.0.1:8096/"

      ErrorLog ${APACHE_LOG_DIR}/mymedia.gofoss.duckdns.org-error.log
      CustomLog ${APACHE_LOG_DIR}/mymedia.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Once the content is added, save and close the file (`:wq!`).

    Note how we enable SSL encryption for Jellyfin with the instruction `SSLEngine On`, and use the SSL certificate `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` as well as the private SSL key `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, which were created earlier on.

    Also note how we disabled ModSecurity in the Apache configuration file with the instruction `SecRuleEngine Off`, as Jellyfin and ModSecurity don't play well together.

    Next, enable the Apache Virtual Host and reload Apache:

    ```bash
    sudo a2ensite mymedia.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configure Pi-Hole to resolve the Jellyfin server address. Browse to `https://mypihole.gofoss.duckdns.org` and log into Pi-Hole's web interface (adjust accordingly). Navigate to the menu entry `Local DNS Records` and add the following domain/IP combination (adjust accordingly):

    ```bash
    DOMAIN:      mymedia.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    You can now browse to [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) and log in as `jellyfinadmin` (adjust accordingly) or any valid Jellyfin user.


??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/252127f0-8931-4a5e-a987-7f6502e55322" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Jellyfin app" width="150px"></img> </center>

## Multimedia applications

Jellyfin supports various clients for Desktop, TV or mobile devices. Choose your preferred ones and follow install instructions provided on [Jellyfin's website](https://jellyfin.org/clients/).


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="What is a media server" width="150px"></img> </center>

## Add users

Jellyfin differenciates between [two user types](https://jellyfin.org/docs/general/server/users/index.html):

* **Administrators** have full access and can add, edit and remove media as well as users. In addition, administrators can maintain and update Jellyfin. In our example, the administrator `jellyfinadmin` was created during the installation process. Other administrators can be added.

* **Users** have limited access to Jellyfin. Features are individually configurable for each user.

??? tip "Show me the step-by-step guide"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Jellyfin login | Browse to [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) and log in as administrator, e.g. `jellyfinadmin` (adjust accordingly). |
    | Settings | Navigate to `Menu ‣ Dashboard ‣ Users ‣ Add User`. |
    | Add user | Provide a user name, as well as a [strong, unique password](https://gofoss.net/passwords/). Then click on `Save`. Also define which libraries this user can access. |
    | Define user rights | Optionally define further settings, such as:<br>• Administrator rights <br>• Live TV access <br>• Transcoding <br>• Restricted Internet speed <br>• Permission to delete media <br>• Remote control <br>• Account locking <br>• Library & device access <br>• Parental control <br>• Pin code|

    </center>


??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ba6374ef-d524-43e7-97eb-d1bc81d6f6cb" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Admins & users need a VPN access"

    All users must be connected to the server via [VPN](https://gofoss.net/secure-domain/) to access Jellyfin.


<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Best home media server" width="150px"></img> </center>

## Subtitles

Jellyfin supports both [embedded and external subtitles](https://jellyfin.org/docs/general/server/media/external-files.html). The Open Subtitle plugin enables Jellyfin to download subtitles automatically. Instructions on how to add external subtitles to Jellyfin are outlined below.

??? tip "Show me the step-by-step guide"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Login | Browse to [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) and log in as administrator, e.g. `jellyfinadmin` (adjust accordingly). |
    | Install Jellyfin plugins | Navigate to `Menu ‣ Dashboard ‣ Plugins ‣ Catalog ‣ Open Subtitles` and click on `Install`. |
    | Restart | Navigate to `Menu ‣ Dashboard` and click on `Restart`. |
    | Configure plugin | The Open Subtitle plugin doesn't require an account to download subtitles. Optionally, create an account on [https://www.opensubtitles.org](https://www.opensubtitles.org/en/search/subs). Then navigate to `Menu ‣ Dashboard ‣ Plugins ‣ Open Subtitles ‣ Settings` and provide your Open Subtitle user credentials. |
    | Settings | Navigate to `Menu ‣ Libraries ‣ Movies or TV Shows ‣ Manage Library` and define further settings in the section `Subtitle Downloads`:<br><br>• Download languages <br>• Subtitle downloader <br>• Filter subtitles based on file name & audio <br>• Location to store subtitles |
    | Download subtitles | Jellyfin automatically schedules a task to regularly download missing subtitles. To force the download, navigate to `Menu ‣ Dashboard ‣ Scheduled Tasks` and click on `Download missing subtitles`. |

    </center>


??? tip "Show me the 3-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/3d82e2cb-9bd7-41b4-866b-a1db979b584a" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Best Linux media server" width="150px"></img> </center>

## Upgrade

Upgrades to new Jellyfin releases are automatically handled by Ubuntu server's package manager. To perform a manual upgrade, follow the instructions below.

??? tip "Show me the step-by-step guide"

    Log into the server and run the following command:

    ```bash
    sudo apt update && sudo apt upgrade
    ```

    Then restart Jellyfin and make sure it's up and running (the status should be "Active"):

    ```bash
    sudo systemctl restart jellyfin
    sudo systemctl status jellyfin
    ```

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="How to set up a media server" width="150px"></img> </center>

## Support

For further details, refer to [Jellyfin's documentation](https://jellyfin.org/docs/) or request support from the [Jellyfin community](https://jellyfin.org/docs/general/getting-help.html).

<br>
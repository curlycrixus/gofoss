---
template: main.html
title: So erstellt Ihr sichere, individuelle Passwörter
description: Passwortstärke. Passwort-Generator. Diceware Wortliste. Zwei-Faktor-Authentifizierung. Passwort-Manager. Keepass. Wurde mein Passwort gehackt?
---

# Wählt sichere Passwörter und Zwei-Faktor-Authentifizierung

!!! level "Letzte Aktualisierung: März 2022. Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/password.png" alt="Diceware" width ="450px"></img>
</center>

Habt Ihr schon einmal ein Passwort-Leck erlebt? Dann seid Ihr hier richtig: dies ist ein Leitfaden zur Erstellung sicherer, individueller und einprägsamer Passwörter, um Eure Konten, Geräte und [verschlüsselten Dateien](https://gofoss.net/de/encrypted-files/) zu schützen. Im Kapitel werden Empfehlungen zur Länge von Passwörtern sowie Open-Source-Passwortmanager wie Keepass XC, Keepass DX oder Strongbox vorgestellt. Außerdem erfahrt Ihr, wie Ihr Zwei-Faktor-Authentifizierung aktivieren könnt.

## Diceware

[Diceware](https://theworld.com/~reinhold/diceware.html) ist eine beliebte Methode, um sichere und individuelle Passwörter zu erstellen, die man sich dennoch leicht merken kann. Dazu braucht Ihr lediglich einen Würfel, ein Stift und ein Blatt Papier. Wenn Ihr dann ein aus 7 Wörtern bestehendes Passwort wählt, gilt dieses nach dem heutigen Stand der Technik als praktisch unknackbar. Untenstehend findet Ihr mehr zum Thema.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Schritte | Anweisungen |
    | :------: | ------ |
    | 1 |Wählt Euch eine Diceware-Liste aus. Zum Beispiel die [Originalliste](https://theworld.com/~reinhold/dicewarewordlist.pdf), oder die [Liste der Electronic Frontier Foundation](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt). Es gibt noch viele weitere Listen in verschiedenen Sprachen zur Auswahl, unter anderem auf [deutsch](https://theworld.com/~reinhold/diceware_german.txt). |
    | 2 |Würfelt 5 Mal und schreibt Euch die Zahlen auf. |
    | 3 |Schlagt das entsprechende Wort in der Wörterliste nach und schreibt es Euch auf. |
    | 4 |Wiederholt die vorherigen Schritte, bis Ihr mindestens 6 Wörter habt. Empfohlen sind sogar 7 Wörter, um eine Entropie von 90,3 Bit zu erzielen. Laut [Dicewares FAQ](https://theworld.com/~reinhold/dicewarefaq.html#howlong/) ist ein solches Passwort heutzutage durch keine bekannte Technologie zu knacken, könnte jedoch bis 2030 durch größere Organisationen geknackt werden. Acht Wörter hingegen gelten bis 2050 als völlig sicher. |
    | 5 |Die Kombination aus diesen Wörtern ist Euer sicheres Passwort. Achtet darauf, die Wörter durch ein Leerzeichen zu trennen. |

    </center>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe src="https://archive.org/embed/how-to-make-a-super-secure-password" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Quelle: [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


??? question "Wurde mein Konto gehackt?"

    | Wurdet Ihr gehackt? | Beschreibung |
    | ------ | ------ |
    | [Have I Been Pwned](https://haveibeenpwned.com/) | Umgekehrte Suchmaschine, die Eure E-Mail-Adresse oder Euer Passwort mit einer riesigen Datenbank mit gestohlenen Daten vergleicht. |
    | [Dehashed](https://www.dehashed.com/) | Sucht nach IP-Adressen, E-Mails, Benutzernamen, Namen, Telefonnummern usw, um Einblicke in Datenbank-Sicherheitslücken und Datenlecks zu erhalten. |


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Keepass" width="150px"></img> </center>

## Keepass

[Keepass](https://keepass.info/) ist ein freier und quelloffener Passwortmanager, der auf fast allen Plattformen verfügbar ist. Keepass speichert Eure Passwörter in einer verschlüsselten Datenbank, die ihrerseits durch ein Master-Passwort geschützt ist. Natürlich solltet Ihr dieses Master-Passwort **niemals verlegen**!

Wir empfehlen außerdem, die Datenbank Eures Passwortmanagers offline zu aufzubewahren. Speichert sie lokal auf Euren Geräten, und bewahrt zwei Remote-Kopien zur [Sicherung](https://gofoss.net/de/backups/) auf.


### Keepass-Clients

=== "Android"

    [Keepass DX](https://www.keepassdx.com/) ist ein freier und [quelloffener](https://github.com/Kunzisoft/KeePassDX) Passwortmanager für Android. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Ladet die App aus [Googles Play Store](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free&hl=de_DE/), [F-Droid](https://www.f-droid.org/de/packages/com.kunzisoft.keepass.libre/) oder [Aurora Store](https://auroraoss.com/de/) herunter. Sie [enthält 0 Tracker und benötigt 6 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/com.kunzisoft.keepass.libre/latest/).


=== "iOS"

    Zum Zeitpunkt der Erstellung dieses Texts war keine freie Version von Keepass DX für iOS verfügbar. [Strongbox](https://github.com/strongbox-password-safe/Strongbox/) ist ein sicherer und quelloffener Keepass-Client. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        Ladet die Strongbox-App aus dem [App Store](https://apps.apple.com/de/app/strongbox-keepass-pwsafe/id897283731/) herunter.


=== "Windows"

    [KeePass XC](https://keepassxc.org/) ist ein plattformübergreifender, gemeinschaftlich verwalteter, freier und [quelloffener](https://github.com/keepassxreboot/keepassxc/) Passwortmanager. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        [Ladet den Windows-Installer herunter](https://keepassxc.org/download/#windows/), doppelklickt auf die `.msi`-Datei und folgt dem Installationsassistenten.


=== "macOS"

    [KeePass XC](https://keepassxc.org/) ist ein plattformübergreifender, gemeinschaftlich verwalteter, freier und [quelloffener](https://github.com/keepassxreboot/keepassxc/) Passwortmanager. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        [Ladet den macOS-Installer herunter](https://keepassxc.org/download/#mac/). Die Keepass-Anwendung sollte von selbst starten und ein neues Volumen mounten. Falls nicht, öffnet die heruntergeladene `.dmg`-Datei und zieht das Keepass-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das Keepass-Symbol in das Andockmenü.


=== "Linux (Ubuntu)"

    [KeePass XC](https://keepassxc.org/) ist ein plattformübergreifender, gemeinschaftlich verwalteter, freier und [quelloffener](https://github.com/keepassxreboot/keepassxc/) Passwortmanager. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`. Führt die folgenden Befehle aus, um KeePassXC zu installieren:

        ```bash
        sudo add-apt-repository ppa:phoerious/keepassxc
        sudo apt update
        sudo apt install keepassxc
        ```

<div style="    margin-top: -20px;">
</div>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe src="https://archive.org/embed/using-password-managers-to-stay-safe-online" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Quelle: [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>

<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Zwei-Faktor-Authentifizierung" width="150px"></img> </center>

## Zwei-Faktor-Authentifizierung

[Zwei-Faktor-Authentifizierung](https://de.wikipedia.org/wiki/Hilfe:Zwei-Faktor-Authentifizierung) (2FA) bietet eine zusätzliche Sicherheitsebene. Sie erfordert mehr als nur ein Passwort, um auf Dienste oder Konten zuzugreifen. Zum Beispiel einen einmalig verwendbaren Verifizierungscode, der per SMS gesendet wird, oder aber von einer Authentifizierungs-App oder einem Schlüssel generiert wird.

Zwar wird die Zwei-Faktor-Authentifizierung allgemein als sicherheitsfördernd angesehen, bietet aber auch zusätzliche Angriffsflächen für Cyberattacken wie [Phishing](https://de.wikipedia.org/wiki/Phishing), [Identitätsdiebstahl](https://de.wikipedia.org/wiki/SIM-Swapping) (SIM-Swapping) oder [SMS-Hijacking](https://de.wikipedia.org/wiki/Signalling_System_7) (SS7-Attacken). Zwei-Faktor-Authentifizierung ist für den Durchschnittsnutzer außerdem weniger bequem.

Alles in allem würden wir zur Zwei-Faktor-Authentifizierung raten. Entscheidet je nach [Bedrohungsmodell](https://www.eff.org/document/surveillance-self-defense-threat-modeling/) selbst, ob die Zwei-Faktor-Authentifizierung Euch zusätzliche Vorteile bringt. Falls Ihr Euch dafür entscheidet, solltet Ihr nicht vergessen die von einigen Diensten angebotenen Backup-Codes aufzubewahren. Sie können Lebensretter sein, wenn Ihr den Zugriff auf Euer Telefon oder Authentifizierungsprogramm verliert.


### 2FA-Clients

=== "Android"

    [AndOTP](https://github.com/andOTP/andOTP/) ist ein freier und quelloffener Zwei-Faktor-Authentifikator für Android. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Ladet die App aus [Googles Play Store](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp&hl=en&gl=DE/), [F-Droid](https://f-droid.org/de/packages/org.shadowice.flocke.andotp/) oder [Aurora Store](https://auroraoss.com/de/) herunter. AndOTP [enthält 0 Tracker und benötigt 1 Berechtigung](https://reports.exodus-privacy.eu.org/de/reports/org.shadowice.flocke.andotp/latest/).

=== "iOS"

    [Tofu](https://www.tofuauth.com/) ist ein freier und quelloffener Zwei-Faktor-Authentifikator für iOS. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        Ladet die Tofu-App aus dem [App Store](https://apps.apple.com/de/app/tofu-authenticator/id1082229305) herunter.


=== "Windows"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) ist eine plattformübergreifende und quelloffene Authentifizierungs-App. Sie erfordert einen physischen Hardwareschlüssel. Untenstehend findet Ihr weitere Informationen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        [Ladet den Windows-Installer herunter](https://www.yubico.com/products/yubico-authenticator/) und folgt dem Installationsassistenten.


=== "macOS"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) ist eine plattformübergreifende und quelloffene Authentifizierungs-App. Sie erfordert einen physischen Hardwareschlüssel. Untenstehend findet Ihr weitere Informationen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        [Ladet den macOS-Installer herunter](https://www.yubico.com/products/yubico-authenticator/). Die Yubico-Anwendung sollte von selbst starten und ein neues Volumen mounten. Falls nicht, öffnet die heruntergeladene `.dmg`-Datei und zieht das Yubico-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das Yubico-Symbol in das Andockmenü.

=== "Linux (Ubuntu)"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) ist eine plattformübergreifende und quelloffene Authentifizierungs-App. Sie erfordert einen physischen Hardwareschlüssel. Untenstehend findet Ihr weitere Informationen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`. Führt die folgenden Befehle aus, um die Yubico Authentifizierungs-App zu installieren:

        ```bash
        sudo add-apt-repository ppa:yubico/stable
        sudo apt update
        sudo apt-get install yubioath-desktop
        ```

<div align="center">
<img src="https://imgs.xkcd.com/comics/password_strength.png" width="550px" alt="Passwortstärke"></img>
</div>

<br>

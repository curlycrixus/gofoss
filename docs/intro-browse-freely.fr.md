---
template: main.html
title: Naviguez de façon privée avec Firefox, Tor et VPN
description: Firefox vs Chrome. Firefox est-il plus sûr que Google ? Comment installer Firefox ? Utiliser Firefox pour Android ? Qu'est-ce qu'un Firefox sécurisé ?
---

# La toile est vaste et infinie

<center>
<html>
<embed src="../../assets/echarts/browser_stats.html" style="width: 100%; height:520px">
</html>
</center>

Dans ce chapitre, nous allons expliquer comment naviguer Internet de manière plus respectueuse pour votre vie privée:

* avec [Firefox](https://gofoss.net/fr/firefox/)
* avec [Tor Browser](https://gofoss.net/fr/tor/)
* avec un [VPN](https://gofoss.net/fr/vpn/)

Le navigateur est la porte d'entrée sur l'internet. Il est devenu si omniprésent dans notre vie quotidienne qu'on en néglige l'importance.

La plupart d'entre nous s'en remettent à Google Chrome pour naviguer sur la toile vaste et infinie. D'autres sur Microsoft Edge ou sur Safari d'Apple. Tous ces navigateurs ont un piètre palmarès en matière de respect de la vie privée. Les réglages par défaut et les « fonctionnalités » sont étudiés pour récolter nos données : pages consultées, contenus affichés, historiques de recherche, noms et mots de passe utilisateur, localisation, enregistrements vocaux, achats, et ainsi de suite.

Il existe des palliatifs pour Chrome, Edge et Safari. Car en définitive, ces navigateurs sont conçus pour scruter les comportements en ligne des utilisateurs, afin d'en tirer profit.

<center>
<a href="https://contrachrome.com/"><img align="center" src="../../assets/img/contrachrome_en.png" alt="Contra Chrome"></img></a>
</center>

<br>

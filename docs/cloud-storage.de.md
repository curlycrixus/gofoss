---
template: main.html
title: So hosten Ihr Eure Cloud-Speicher selbst
description: Hostet Eure Cloud-Dienste selbst. Seafile installieren. Was ist Seafile? Seafile vs. Nextcloud? Seafile vs. Syncthing? Wie installiert man Seafile? Ist Seafile sicher?
---

# Seafile, eine selbst gehostete Cloud-Speicherlösung

!!! level "Letzte Aktualisierung: Mai 2022.Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/seafile_picture.png" alt="Seafile installieren" width="550px"></img>
</center>

<br>

[Seafile](https://www.seafile.com/en/home/) ist eine Dateisynchronisationslösung, die Ihr selbst auf Eurem [Ubuntu-Server](https://gofoss.net/de/ubuntu-server/) hosten könnt. Wenngleich der Funktionsumfang überschaubar bleibt, zeichnet sich Seafile vor allem durch eine hocheffiziente, geräteübergreifende und, soweit gewünscht, verschlüsselte Dateisynchronisation aus. Seafile ist frei, quelloffen, schnell und zuverlässig. Durch die Verwendung von Delta-Sync synchronisiert Seafile nur aktualisierte oder geänderte Dateiabschnitte, nicht die ganze Dateie. Client-Anwendungen sind für zahlreiche Plattformen verfügbar, einschließlich mobiler Geräte.


??? question "Und was ist mit Nextcloud oder Syncthing?"

    [Nextcloud](https://nextcloud.com/de/) ist eine beliebte, selbst gehostete und funktionsreiche Dateisynchronisationslösung. Sie ist frei, quelloffen und bietet viele zusätzliche Dienste wie Kontaktverwaltung, Videokonferenzen, Webmail, Kalender und so weiter. Nachteilig ist, dass die Dateisynchronisierung vergleichsweise langsam ist.

    [Syncthing](https://syncthing.net/) ist eine dezentrale, freie und quelloffene Dateisynchronisationslösung. Im Gegensatz zu Seafile und Nextcloud ist kein zentraler Server bzw. keine zentrale Datenbank erforderlich. Nachteilig ist, dass die Dateisynchronisierung vergleichsweise langsam ist und dass, je nach Netzwerkkonfiguration, Schwierigkeiten mit der Firewall auftreten können.


??? question "Warum nicht einfach die üblichen Cloud-Speicher nutzen?"

    Klar, wenn Ihr kommerziellen Cloud-Anbietern Eure privaten Daten anvertrauen wollt. Ihr solltet jedoch bedenken, dass Cloud-Speicherlösungen wie iCloud, GDrive, Dropbox oder OneDrive trotz aller Behauptungen auf Eure Daten zugreifen oder diese Dritten zur Verfügung stellen können. Tatsächlich besitzen die Cloud-Anbieter Kopien der Schlüssel, die Eure Privatsphäre schützen.

    <center>

    | Cloud-Anbieter | Datenschutzrichtlinien |
    | ------ | ------ |
    | [Apple iCloud](https://support.apple.com/de-de/HT202303) |“Bei der Verwendung von "Nachrichten" in iCloud wird auch Ende-zu-Ende-Verschlüsselung eingesetzt. Wenn du "iCloud-Backup" aktiviert hast, enthält deine Backup eine Kopie des Schlüssels, der deine Nachrichten schützt. Dadurch wird sichergestellt, dass du deine Nachrichten wiederherstellen kannst, wenn du den Zugriff auf den iCloud-Schlüsselbund und deine vertrauenswürdigen Geräte verloren hast.“ |
    | [Google Drive & WhatsApp](https://faq.whatsapp.com/android/chats/about-google-drive-backups?lang=de) |“Mediendateien und Nachrichten sind nicht durch die WhatsApp Ende-zu-Ende-Verschlüsselung geschützt, wenn sie auf Google Drive gespeichert sind.“ |
    |[Dropbox](https://www.dropbox.com/privacy#terms) |“Um diese und andere Funktionen bereitzustellen, greift Dropbox auf Ihre Daten zu, speichert und scannt sie. Sie geben uns die Erlaubnis, diese Dinge zu tun, und diese Erlaubnis gilt auch für unsere Partner und vertrauenswürdige Dritte, mit denen wir zusammenarbeiten.” |
    | [Microsoft OneDrive](https://privacy.microsoft.com/de-de/privacystatement/) |“Wenn Sie OneDrive verwenden, erfassen wir Daten über Ihre Nutzung des Dienstes sowie über den von Ihnen gespeicherten Inhalt, um den Dienst bereitzustellen, zu verbessern und zu schützen. Beispiele umfassen sowohl die Indizierung Ihrer OneDrive-Dokumente, so dass Sie diese später durchsuchen können als auch die Verwendung von Ortsinformationen, um Ihnen die Suche nach Fotos auf Basis der Orte, wo das Foto aufgenommen wurde, zu erleichtern.” |

    </center>


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Seafile MySQL" width="150px"></img> </center>

## Vorbereitung der Datenbank

Seafile kann mit verschiedenen Datenbanken betrieben werden, darunter MySQL, SQLite, PostgreSQL und so weiter. In diesem Tutorial werden wir mit MySQL arbeiten, um die benötigten Datenbanken zu generieren. Untenstehend eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Meldet Euch auf dem Server an und legt einen Systembenutzer an, der Seafile ausführen soll. Für die Zwecke dieses Tutorials nennen wir diesen Systembenutzer `seafileadmin`. Natürlich könnt Ihr jeden beliebigen Namen wählen, stellt nur sicher, dass Ihr die Befehle entsprechend anpasst:

    ```bash
    sudo adduser --disabled-login seafileadmin
    ```

    Meldet Euch mit dem folgenden Befehl bei MySQL als Root-Benutzer an:

    ```bash
    sudo mysql -u root -p
    ```

    Führt den folgenden Befehl aus, um den MySQL-Benutzer `seafileadmin` zu erstellen (passt den Benutzernamen entsprechend an). Stellt sicher, dass Ihr die Zeichenfolge `StrongPassword` durch ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) ersetzt:

    ```bash
    CREATE USER 'seafileadmin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'StrongPassword';
    ```

    Erzeugt als Nächstes die von Seafile benötigten Datenbanken und gewährt die richtigen Zugriffsrechte:

    ```bash
    CREATE DATABASE ccnet;
    CREATE DATABASE seafile;
    CREATE DATABASE seahub;
    GRANT ALL ON ccnet.* TO 'seafileadmin'@'localhost';
    GRANT ALL ON seafile.* TO 'seafileadmin'@'localhost';
    GRANT ALL ON seahub.* TO 'seafileadmin'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;
    ```

    Meldet Euch wieder als Benutzer `seafileadmin` bei MySQL an (passt den Benutzernamen entsprechend an):

    ```bash
    sudo mysql -u seafileadmin -p
    ```

    Stellt sicher, dass alle Datenbanken korrekt angelegt wurden:

    ```bash
    SHOW DATABASES;
    ```

    Die Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | ccnet              |
    | seafile            |
    | seahub             |
    +--------------------+
    4 rows in set (0.02 sec)
    ```

    Verlasst MySQL:

    ```bash
    EXIT;
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/0a9f7981-aff4-4c63-be92-60e4fb111c51" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Fehler bei der Erstellung von Seafile admin"

    Mehrere BenutzerInnen berichten von Problemen beim Versuch, Seafile auf einem Ubuntu Server zu installieren ("Error during creation of Seafile admin"). Das Problem scheint mit MySQL 8 zusammenzuhängen, welches `caching_sha2_password` als Standard-Authentifizierungs-Plugin verwendet. Die Lösung besteht darin, den MySQL-Benutzer `seafileadmin` mit dem Authentifizierungs-Plugin `mysql_native_password` anzulegen, wie im obigen Abschnitt beschrieben.

<br>

<center> <img src="../../assets/img/separator_seafile.svg" alt="Seafile Installation" width="150px"></img> </center>

## Seafile-Installation

Folgt den untenstehenden Anweisungen, um alle [Abhängigkeiten](https://manual.seafile.com/deploy/using_mysql/#requirements) zu prüfen und Seafile auf Eurem Ubuntu 22.04 Server zu installieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Voraussetzungen

    Python ist zur Ausführung von Seafile erforderlich, installiert folgende Pakete auf dem Server:

    ```bash
    sudo apt install python3 python3-{setuptools,pip} libmysqlclient-dev memcached libmemcached-dev libffi-dev
    ```

    Installiert anschließend weitere Pakete als Benutzer `seafileadmin` (passt den Benutzernamen entsprechend an):

    ```bash
    sudo -H -u seafileadmin pip3 install --user django==3.2.* Pillow pylibmc captcha jinja2 sqlalchemy==1.4.3 django-pylibmc django-simple-captcha python3-ldap mysqlclient pycryptodome==3.12.0
    ```

    ### Installationsskript

    [Ermittelt die neueste Version von Seafile Server für Linux](https://www.seafile.com/en/download/). Zum Zeitpunkt der Abfassung dieses Textes handelt es sich um die Version 9.0.5. Ladet das Paket herunter und entpackt es mit den folgenden Befehlen (passt die Versionsnummer entsprechend an):

    ```bash
    sudo wget -4 https://download.seadrive.org/seafile-server_9.0.5_x86-64.tar.gz
    sudo tar -xvf seafile-server_*
    sudo mkdir /var/www/seafile
    sudo mkdir /var/www/seafile/installed
    sudo mv seafile-server-* /var/www/seafile
    sudo mv seafile-server_* /var/www/seafile/installed
    ```

    !!! warning "Übergangslösung für Ubuntu 22.04"

        Zum Zeitpunkt des Verfassens dieser Zeilen ist Seafile 9.0.5 [inkompatibel mit Version `1.15.0` des `cffi`-Moduls](https://forum.seafile.com/t/seafile-community-edition-9-0-4-is-ready/15988/7), das mit Ubuntu 22.04 ausgeliefert wird. Aus diesem Grund wurde es im vorherigen Schritt nicht zusammen mit den anderen Python-Modulen installiert. Stattdessen könnt Ihr folgende [Übergangslösung](https://forum.seafile.com/t/seafile-community-edition-9-0-5-is-ready/16388/14) anwenden. Nachdem Ihr die richtigen Zugriffsrechte für den `seafileadmin`-Nutzer festgelegt habt (passt den Namen entsprechend an) könnt Ihr Version `1.14.6` des `cffi`-Moduls im `thirdpart`-Verzeichnis installieren:

        ```bash
        sudo chown -R seafileadmin:seafileadmin /var/www/seafile
        sudo chmod -R 755 /var/www/seafile
        sudo -H -u seafileadmin pip3 install --force-reinstall --upgrade --target /var/www/seafile/seafile-server-9.0.5/seahub/thirdpart cffi==1.14.6
        ```

    Führt das Installationsskript aus:

    ```bash
    sudo bash /var/www/seafile/seafile-server-9.0.5/setup-seafile-mysql.sh
    ```

    Folgt den Anweisungen auf dem Bildschirm:

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Server Name | Wählt einen Namen für den Seafile-Server. Im Rahmen dieses Tutorials wählen wir `gofoss_seafile`, passt den Namen entsprechend an. |
    | Domain | Gebt den vom Cloud-Speicher verwendeten Domain-Namen an. Im Rahmen dieses Tutorials wählen wir `https://myfiles.gofoss.duckdns.org`, passt die Domäne entsprechend an. Es handelt sich um dieselbe Adresse wie für die Web-Oberfläche, die wir später einrichten werden. |
    | Port | Wählt den von Seafile verwendeten TCP-Port. Der Standardport ist `8082`. |
    | Databases | Verwendet die bestehenden ccnet/seafile/seahub-Datenbanken, die zuvor erstellt wurden. |
    | Host of MySQL server | Der Standardwert ist `localhost`. |
    | Port of MySQL server | Der Standardwert ist `3306`. |
    | MySQL user | Gebt den Namen des MySQL-Benutzers an. In unserem Beispiel ist das `seafileadmin`, passt den Benutzernamen entsprechend an. |
    | Password of MySQL user | Gebt das Passwort des MySQL-Benutzers an. |
    | ccnet database name | Gebt den Namen der ccnet-Datenbank an. In diesem Fall ist das `ccnet`. |
    | seafile database name | Gebt den Namen der seafile-Datenbank an. In diesem Fall ist das `seafile`. |
    | seahub database name | Gebt den Namen der seahub-Datenbank an. In diesem Fall ist das `seahub`. |

    </center>

    Nach erfolgreicher Installation sollte das Terminal in etwa folgende Ausgabe anzeigen:

    ```bash
    -----------------------------------------------------------------
    Your seafile server configuration has been finished successfully.
    -----------------------------------------------------------------

    run seafile server:     ./seafile.sh { start | stop | restart }
    run seahub  server:     ./seahub.sh  { start <port> | stop | restart <port> }

    -----------------------------------------------------------------
    If you are behind a firewall, remember to allow input/output of these tcp ports:
    -----------------------------------------------------------------

    port of seafile fileserver:   8082
    port of seahub:               8000

    When problems occur, refer to https://download.seafile.com/published/seafile-manual/home.md for information.
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8a1189e-ce82-47ee-8a70-1e5eb9ab8462" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Seafile Konfiguration" width="150px"></img> </center>

## Konfiguration

Nach der erfolgreichen Installation von Seafile sind eine Reihe von Einstellungen vorzunehmen: Spracheinstellungen, Berechtigungen, Administratorkonto, Autostart beim Booten, usw. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Sprache

    Ändert die Spracheinstellungen. In diesem Beispiel werden wir Deutsch verwenden:

    ```bash
    echo "export LC_ALL=de_DE.UTF-8" >>~/.bashrc
    echo "export LANG=de_DE.UTF-8" >>~/.bashrc
    echo "export LANGUAGE=de_DE.UTF-8" >>~/.bashrc
    source ~/.bashrc
    ```

    Stellt mit dem folgenden Befehl sicher, dass die Einstellungen korrekt übernommen wurden:

    ```bash
    locale
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    LANG=de_DE.UTF-8
    LANGUAGE=de_DE.UTF-8
    LC_CTYPE="de_DE.UTF-8"
    LC_NUMERIC="de_DE.UTF-8"
    LC_TIME="de_DE.UTF-8"
    LC_COLLATE="de_DE.UTF-8"
    LC_MONETARY="de_DE.UTF-8"
    LC_MESSAGES="de_DE.UTF-8"
    LC_PAPER="de_DE.UTF-8"
    LC_NAME="de_DE.UTF-8"
    LC_ADDRESS="de_DE.UTF-8"
    LC_TELEPHONE="de_DE.UTF-8"
    LC_MEASUREMENT="de_DE.UTF-8"
    LC_IDENTIFICATION="de_DE.UTF-8"
    LC_ALL=de_DE.UTF-8
    ```

    Ihr könnt natürlich jegliche andere Sprache wählen. Prüft, welche Sprachen (auf Englisch *Locales*) der Server derzeit unterstützt:

    ```bash
    locale -a
    ```

    Solltet Ihr weitere Sprachen hinzufügen wollen, kommentiert einfach die erforderlichen Zeilen in der folgenden Konfigurationsdatei aus (z. B. `es_ES.UTF-8` für Spanisch, `fr_FR.UTF-8` für Französisch, `nl_NL.UTF-8` für Niederländisch, usw.):

    ```bash
    sudo vi /etc/locale.gen
    ```

    Übernehmt die Änderungen:

    ```bash
    sudo locale-gen
    ```

    Führt nun die im obigen Abschnitt beschriebenen Befehle aus und wendet dabei Eure bevorzugten Spracheinstellungen an.


    ### Administratorkonto

    Legt die richtigen Zugrifssrechte fest und wechselt in das Verzeichnis `/var/www/seafile`:

    ```bash
    sudo chown -R seafileadmin:seafileadmin /var/www/seafile
    sudo chmod -R 755 /var/www/seafile
    sudo chmod 750 /home/seafileadmin
    cd /var/www/seafile
    ```

    Startet Seafile:

    ```bash
    sudo -H -u seafileadmin bash -C '/var/www/seafile/seafile-server-latest/seafile.sh' start
    ```

    Startet Seahub:

    ```bash
    sudo -H -u seafileadmin bash -C '/var/www/seafile/seafile-server-latest/seahub.sh' start
    ```

    Beim ersten Start von Seahub wird die Einrichtung eines Administratorkontos verlangt. Für den Zweck dieses Tutorials geben wir die Administrator-E-Mail-Adresse `seafileadmin@gofoss.net` an. Natürlich kann jegliche geeignete E-Mail-Adresse verwendet werden. Wenn Ihr dazu aufgefordert werdet, gebt ebenfalls ein [sicheres, individuelles Kennwort](https://gofoss.net/de/passwords/) ein.


    ### Autostart

    Stellt sicher, dass Seafile bei jedem Hochfahren des Servers automatisch gestartet wird. Erstellt eine erste Konfigurationsdatei:

    ```bash
    sudo vi /etc/systemd/system/seafile.service
    ```

    Gebt den folgenden Inhalt ein:

    ```bash
    [Unit]
    Description=Seafile
    After= mysql.service network.target

    [Service]
    Type=forking
    ExecStart=/var/www/seafile/seafile-server-latest/seafile.sh start
    ExecStop=/var/www/seafile/seafile-server-latest/seafile.sh stop
    User=seafileadmin

    [Install]
    WantedBy=multi-user.target
    ```

    Speichert und schließt die Datei (`:wq!`). Erstellt nun eine zweite Konfigurationsdatei:

    ```bash
    sudo vi /etc/systemd/system/seahub.service
    ```

    Gebt den folgenden Inhalt ein:

    ```bash
    [Unit]
    Description=Seahub
    After= mysql.service network.target seafile.service

    [Service]
    Type=forking
    Environment="LC_ALL=en_US.UTF-8"
    ExecStart=/var/www/seafile/seafile-server-latest/seahub.sh start
    ExecStop=/var/www/seafile/seafile-server-latest/seahub.sh stop
    User=seafileadmin

    [Install]
    WantedBy=multi-user.target
    ```

    Speichert schließt die Datei (`:wq!`).

    Stellt sicher, dass Seafile bei jedem Boot-Vorgang automatisch gestartet wird:

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable seafile
    sudo systemctl enable seahub
    ```

    Startet den Server neu:

    ```bash
    sudo reboot
    ```

    Vergewissert Euch, dass Seafile betriebsbereit ist (der Status sollte `Active` sein):

    ```bash
    sudo systemctl status seafile
    sudo systemctl status seahub
    ```


    ### Nachbereitung

    Der Cloud-Speicher ist von einer Domain Eurer Wahl aus zugänglich. Im Rahmen dieses Tutorials wählen wir `https://myfiles.gofoss.duckdns.org`. Natürlich ist jegliche andere Adresse möglich. Achtet nur darauf, dass Ihr die Befehle entsprechend anpasst. Öffnet die erste Konfigurationsdatei:

    ```bash
    sudo vi /var/www/seafile/conf/seahub_settings.py
    ```

    Vergewissert Euch, dass die `SERVICE__URL` auf die richtige Adresse verweist:

    ```bash
    SERVICE_URL = "https://myfiles.gofoss.duckdns.org/"
    ```

    Fügt anschließend die folgenden Zeilen am Ende der Datei hinzu (passt die Zeitzone sowie die FILE_SERVER_ROOT URL entsprechend Eurer eigenen Konfiguration an):

    ```bash
    TIME_ZONE = 'Europe/Budapest'
    SESSION_EXPIRE_AT_BROWSER_CLOSE = True
    LOGIN_ATTEMPT_LIMIT = 2
    FILE_SERVER_ROOT = 'https://myfiles.gofoss.duckdns.org/seafhttp'
    ```

    Speichert schließt die Datei (`:wq!`).


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4812ac9b-29e3-4941-9681-1332e1b3047d" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Seafile Weboberfläche" width="150px"></img> </center>

## Web-Oberfläche

Als nächstes richten wir einen Apache Virtual Host als Reverse Proxy ein, um auf die Seafile-Weboberfläche zuzugreifen. Untenstehend eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Erstellt eine Apache-Konfigurationsdatei:

    ```bash
    sudo vi /etc/apache2/sites-available/myfiles.gofoss.duckdns.org.conf
    ```

    Fügt den folgenden Inhalt hinzu und passt die Einstellungen an Eure eigene Konfiguration an, z. B. den Domain-Namen (`myfiles.gofoss.duckdns.org`), den Pfad zu den SSL-Schlüsseln, die IP-Adressen und so weiter:

    ```bash
    <VirtualHost *:80>

        ServerName              myfiles.gofoss.duckdns.org
        ServerAlias             www.myfiles.gofoss.duckdns.org
        Redirect permanent /    https://myfiles.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              myfiles.gofoss.duckdns.org
        ServerAlias             www.myfiles.gofoss.duckdns.org
        ServerSignature         Off

        SecRuleEngine           Off
        SSLEngine               On
        SSLProxyEngine          On
        SSLProxyCheckPeerCN     Off
        SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
        SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
        DocumentRoot            /var/www/seafile

        <Location />
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        Alias /media  /var/www/seafile/seafile-server-latest/seahub/media

        RewriteEngine On

        <Location /media>
        Require all granted
        </Location>

        # seafile fileserver
        ProxyPass /seafhttp http://127.0.0.1:8082
        ProxyPassReverse /seafhttp http://127.0.0.1:8082

        # seahub
        SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
        ProxyPreserveHost On
        ProxyPass / http://127.0.0.1:8000/
        ProxyPassReverse / http://127.0.0.1:8000/

        ErrorLog ${APACHE_LOG_DIR}/myfiles.gofoss.duckdns.org-error.log
        CustomLog ${APACHE_LOG_DIR}/myfiles.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Speichert und schließt die Datei (`:wq!`).

    Beachtet, dass die SSL-Verschlüsselung für Seafile mit der Anweisung `SSLEngine On` aktiviert und das zuvor erstellte SSL-Zertifikat `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` sowie der private SSL-Schlüssel `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem` verwendet wurden.

    Beachtet ebenfalls, dass ModSecurity in der Apache-Konfigurationsdatei mit der Anweisung `SecRuleEngine Off` deaktiviert wurde, da Seafile und ModSecurity nicht gut miteinander harmonieren.

    Aktiviert als nächstes den Apache Virtual Host und ladet Apache neu:

    ```bash
    sudo a2ensite myfiles.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Konfiguriert Pi-Hole, um die lokale Adresse von Seafile nutzen zu können. Ruft dazu `https://mypihole.gofoss.duckdns.org` auf und meldet Euch über die Pi-Hole-Weboberfläche an (passt die URL entsprechend an). Öffnet den Menüeintrag `Locale DNS Records` und fügt die folgende Domain/IP-Kombination hinzu (passt wiederum die URL sowie IP entsprechend an):

    ```bash
    DOMAIN:      myfiles.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Ruft [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) auf und meldet Euch als `seafileadmin@gofoss.net` an (passt den Benutzernamen entsprechend an). Nehmt anschließend folgende Einstellungen vor:

    <center>

    | Einstellungen | Beschreibung |
    | ------ | ------ |
    | System Admin ‣ Settings | Ändert das Feld `SERVICE_URL` in `https://myfiles.gofoss.duckdns.org` um und klickt auf `Save`. |
    | System Admin ‣ Settings | Ändert das Feld `FILE_SERVER_ROOT` in `https://myfiles.gofoss.duckdns.org/seafhttp` um und klickt auf `Save`. |

    </center>


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/59faee72-c4e9-491f-8bb7-4a2cbfb1fbb4" frameborder="0" allowfullscreen></iframe>
    </center>


<br>


<center> <img src="../../assets/img/separator_fdroid.svg" alt="Seafile Clients" width="150px"></img> </center>

## Clients

### Seahub

Seahub ist nicht wirklich ein Client, sondern Seafiles Web-Oberfläche. BenutzerInnen können sich über einen Browser anmelden und die Ordnerstruktur erkunden, Dateien hoch- und herunterladen, bearbeiten, mit anderen teilen und so weiter. Seahub zeigt nativ Dateiformate von Microsoft und LibreOffice an, sowie Videos, Bilder, PDF- & Textdateien, und vieles mehr. Über Seahub vorgenommene Änderungen werden auf allen synchronisierten Geräten gespiegelt.


### Synchronisations-Client

Dieser Client synchronisiert ausgewählte Dateien und Ordner zwischen Geräten. Er ist am besten für Geräte mit ausreichender Speicherkapazität geeignet.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Installation | Ladet die neueste Version von Seafiles [Desktop Sync Client für Windows](https://www.seafile.com/en/download/) herunter und installiert die Software. <br><br><center> <img src="../../assets/img/seafile_desktop_client_1.png" alt="Seafile Desktop Sync" width="150px"></img> </center> |
        | Cloud-Ordner | Startet den Seafile-Client und wählt einen lokalen Ordner, der mit der Cloud synchronisiert werden soll. <br><center> <img src="../../assets/img/seafile_desktop_client_2.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Anmeldung | Gebt den Servernamen sowie die Benutzeranmeldedaten an. Bestätigt gegebenenfalls auch die Zwei-Faktor-Authentifizierung. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Installation | Ladet die neueste Version von Seafiles [Desktop Sync Client für macOS](https://www.seafile.com/en/download/) herunter und installiert die Software. <br><br><center> <img src="../../assets/img/seafile_desktop_client_1.png" alt="Seafile Desktop Sync" width="150px"></img> </center> |
        | Cloud-Ordner | Startet den Seafile-Client und wählt einen lokalen Ordner, der mit der Cloud synchronisiert werden soll. <br><center> <img src="../../assets/img/seafile_desktop_client_2a.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Anmeldung | Gebt den Servernamen sowie die Benutzeranmeldedaten an. Bestätigt gegebenenfalls auch die Zwei-Faktor-Authentifizierung. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Signierschlüssel | Öffnet ein Terminal und ladet den Signierschlüssel herunter: <br>`sudo wget https://linux-clients.seafile.com/seafile.asc -O /usr/share/keyrings/seafile-keyring.asc` |
        | Paketquelle | Fügt die Paketquelle zum Archiv für Ubuntu 22.04 (Jammy Jellyfish) hinzu:<br> `sudo bash -c "echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/seafile-keyring.asc] https://linux-clients.seafile.com/seafile-deb/jammy/ stable main' > /etc/apt/sources.list.d/seafile.list"`<br><br> Aktualisiert anschließend den lokalen apt-Cache: `sudo apt update` |
        | Installation | Installiert den Seafile Synching Client: `sudo apt install -y seafile-gui` |
        | Cloud-Ordner | Startet den Seafile-Client und wählt einen lokalen Ordner, der mit der Cloud synchronisiert werden soll. <br><center> <img src="../../assets/img/seafile_desktop_client_2b.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Anmeldung | Gebt den Servernamen sowie die Benutzeranmeldedaten an. Bestätigt gegebenenfalls auch die Zwei-Faktor-Authentifizierung. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


### SeaDrive

Der SeaDrive Client funktioniert wie ein Netzlaufwerk. BenutzerInnen können ohne vorherige Synchronisation auf Server-Dateien zugreifen. Dateien können auch zur Offline-Nutzung zwischengespeichert werden. SeaDrive ist bestens dazu geeignet, die Kapazität von Geräten mit begrenztem Speicherplatz zu erweitern.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Installation | Ladet die neueste Version des [SeaDrive Clients für Windows](https://www.seafile.com/en/download/) herunter und installiert die Software. <br><br><center> <img src="../../assets/img/seadrive_client_1.png" alt="Seadrive" width="300px"></img> </center> |
        | Virtuelles Laufwerk | Startet den SeaDrive-Client und wählt einen Laufwerksbuchstaben für das virtuelle Laufwerk. Standardmäßig wird `S:` verwendet. |
        | Anmeldung | Gebt den Servernamen sowie die Benutzeranmeldedaten an. Bestätigt gegebenenfalls auch die Zwei-Faktor-Authentifizierung.  <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Datei-Liste | SeaDrive ruft eine Liste aller auf dem Server gespeicherten Dateien und Ordner ab, ohne diese tatsächlich herunterzuladen. Eine Benachrichtigung wird angezeigt sobald der Vorgang abgeschlossen ist. Nun kann man über den Windows-Explorer auf die Cloud-Dateien zugreifen, als wären diese auf einer normalen Festplatte gespeichert. <br><center> <img src="../../assets/img/seadrive_client_2.png" alt="Seadrive" width="300px"></img> </center>|

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Installation | [Download & install the latest release of the SeaDrive Client for macOS](https://www.seafile.com/en/download/). <br><br><center> <img src="../../assets/img/seadrive_client_1.png" alt="Seadrive" width="300px"></img> </center> |
        | Virtuelles Laufwerk | Startet den SeaDrive-Client und wählt einen Laufwerksbuchstaben für das virtuelle Laufwerk. Standardmäßig wird `S:` verwendet. |
        | Anmeldung | Gebt den Servernamen sowie die Benutzeranmeldedaten an. Bestätigt gegebenenfalls auch die Zwei-Faktor-Authentifizierung. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Datei-Liste | SeaDrive ruft eine Liste aller auf dem Server gespeicherten Dateien und Ordner ab, ohne diese tatsächlich herunterzuladen. Eine Benachrichtigung wird angezeigt sobald der Vorgang abgeschlossen ist. Nun kann man auf die Cloud-Dateien zugreifen, als wären diese auf einer normalen Festplatte gespeichert. <br><center> <img src="../../assets/img/seadrive_client_2.png" alt="Seadrive" width="300px"></img> </center>|

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Signierschlüssel | Öffnet ein Terminal und ladet den Signierschlüssel herunter:<br> `sudo wget https://linux-clients.seafile.com/seafile.asc -O /usr/share/keyrings/seafile-keyring.asc` |
        | Paketquelle | Fügt die Paketquelle zum Archiv für Ubuntu 22.04 (Jammy Jellyfish) hinzu:<br> `sudo bash -c "echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/seafile-keyring.asc] https://linux-clients.seafile.com/seadrive-deb/jammy/ stable main' > /etc/apt/sources.list.d/seadrive.list"`<br><br> Aktualisiert anschließend den lokalen apt-Cache: `sudo apt update` |
        | Installation | Installiert den SeaDrive Client: `sudo apt install -y seadrive-gui` |
        | Anmeldung | Startet den SeaDrive Client und gebt den Servernamen sowie die Benutzeranmeldedaten an. Bestätigt gegebenenfalls auch die Zwei-Faktor-Authentifizierung.  Nach dem Einloggen wird das virtuelle Laufwerk hier eingebunden: `~/SeaDrive`.<br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


### Mobile Clients

Seafile ist auf Smartphones und Tabletts verfügbar. BenutzerInnen können die Ordnerstruktur erkunden, Dateien hoch- und herunterladen und bearbeiten. Um Speicherplatz zu sparen, werden nur verwendete Dateien zwischengespeichert, statt alle Dateien vollständig zu synchronisieren. Die mobilen Apps von Seafile enthalten auch eine Funktion zur automatischen Sicherung von Fotos auf dem Server. Untenstehend findet Ihr weitere Details zum Thema

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Öffnet F-Droid auf Eurem Smartphone und installiert die [Seafile Anwendung](https://f-droid.org/de/packages/com.seafile.seadroid2/). Alternativ könnt Ihr Seafile auch über den [Aurora Store](https://auroraoss.com/de/) oder [Googles Play Store](https://play.google.com/store/apps/details?id=com.seafile.seadroid2&hl=de&gl=DE) installieren.

        <center>

        | Einstellungen | Beschreibung |
        | ------ | ------ |
        | Server | Gebt die Adresse des Seafile-Servers an, z. B.  [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) (passt die Adresse entsprechend an). |
        | Email | Gebt die E-Mail-Adresse des Seafile-Benutzers an, z. B. `seafileuser@gofoss.net` (passt die Adresse entsprechend an). |
        | Passwort | Gebt das Kennwort des Seafile-Benutzers an. |
        | Token | Falls die Zwei-Faktor-Authentifizierung aktiviert ist, gebt ebenfalls den von Eurem Smartphone (z.b. andOTP) generierten Token ein. |

        </center>

=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        Installiert Seafile über den [App Store](https://apps.apple.com/cn/app/seafile-pro/id639202512?l=de).

        <center>

        | Einstellungen | Beschreibung |
        | ------ | ------ |
        | Server | Gebt die Adresse des Seafile-Servers an, z. B. [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) (passt die Adresse entsprechend an). |
        | Email | Gebt die E-Mail-Adresse des Seafile-Benutzers an, z. B. `seafileuser@gofoss.net` (passt die Adresse entsprechend an). |
        | Passwort | Gebt das Kennwort des Seafile-Benutzers an. |
        | Token | Falls die Zwei-Faktor-Authentifizierung aktiviert ist, gebt ebenfalls den von Eurem Smartphone generierten Token ein. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Seafile BenutzerInnen anlegen" width="150px"></img> </center>

## BenutzerInnen anlegen

Seafile unterscheidet zwischen zwei Benutzertypen:

* **Administratoren** haben volle Zugriffsrechte. Sie können Dateien sowie BenutzerInnen hinzufügen, bearbeiten und entfernen. Darüber hinaus können Administratoren die Seafile-Weboberfläche verwalten und aktualisieren. In unserem Beispiel wurde der Administrator `seafileadmin` bei der Installation des Seafile-Servers angelegt.

* **BenutzerInnen** können von Administratoren hinzugefügt werden, haben aber nur begrenzte Zugriffsrechte. Sie können Dateien und Ordner nur dann verwalten, wenn sie über die richtigen Zugriffsrechte verfügen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Schritt 1 | Ruft [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) auf und meldet Euch als Administrator an, z.B. `seafileadmin@gofoss.net` (passt den Benutzernamen entsprechend an). |
    | Schritt 2 | Ruft das Menü `System Admin ‣ Users ‣ Add user` auf. |
    | Schritt 3 | Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Klickt dann auf `Submit`. |

    </center>


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    Der Systemadministrator meldet sich über die Seahub-Weboberfläche an, um die Benutzer Georg, Tom und Lenina anzulegen.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/d6db71f4-a793-472a-ba6a-e2095c6f8e47" frameborder="0" allowfullscreen></iframe>
    </center>

??? warning "Administratoren & BenutzerInnen benötigen einen VPN-Zugang"

    BenutzerInnen müssen über [VPN](https://gofoss.net/de/secure-domain/) mit dem Server verbunden sein, um auf Seafile zugreifen zu können.



<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Seafile Zwei-Faktor-Authentifizierung" width="150px"></img> </center>

## Zwei-Faktor-Authentifizierung

Die Zwei-Faktor-Authentifizierung (2FA) ist eine optionale Funktion zur weiteren Erhöhung der Sicherheit. Wenn diese aktiviert ist, erfordert die Anmeldung bei Seafile ein Passwort sowie einen 6-stelligen Code, der von einer App auf dem Telefon des Benutzers generiert wird. Wir empfehlen die Verwendung von [andOTP](https://f-droid.org/de/packages/org.shadowice.flocke.andotp/), welches über F-Droid installiert werden kann.


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Zunächst muss der Seafile-Administrator 2FA aktivieren:

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Schritt 1 | Meldet Euch über die Seafile-Web-Oberfläche [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) als `seafileadmin@gofoss.net` an (passt den Benutzernamen entsprechend an). |
    | Schritt 2 | Ruft das Menü `System Admin ‣ Settings ‣ Passwords` auf. |
    | Schritt 3 | Aktiviert die Zwei-Faktor-Authentifizierung für alle BenutzerInnen. |

    </center>

    Als nächstes muss jeder Benutzer individuell die 2FA konfigurieren. Beginnt mit dem Seafile-Administrator und wiederholt die Einstellung für jedes später eingerichtete Benutzerkonto:

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Schritt 1 | Meldet Euch über Seafiles Web-Oberfläche [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) an (passt die Adresse entsprechend an). |
    | Schritt 2 | Ruft das Menü `Settings` auf. |
    | Schritt 3 | Aktiviert die Zwei-Faktor-Authentifizierung. |
    | Schritt 4 | Öffnet andOTP auf dem Telefon des Benutzers. |
    | Schritt 5 | Scannt den angezeigten QR-Code mit andOTP. |
    | Schritt 6 | Gebt den von andOTP generierten 6-stelligen Code in Seafiles Web-Oberfläche ein. |
    | Schritt 7 | Bewahrt die von Seafile bereitgestellte Liste von `Backup-Tokens` sicher auf. Diese Tokens ermöglichen es, den Zugriff auf Seafile ohne andOTP wiederherzustellen. |

    </center>


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/1c615577-9a2b-4c7e-aa50-9a8d575abc98" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Seafile Bibliotheken, Dateien & Verzeichnisse hinzufügen" width="150px"></img> </center>

## Bibliotheken, Dateien & Verzeichnisse hinzufügen

=== "Seahub"

    Seahub ermöglicht es BenutzerInnen, über die Web-Oberfläche neue Bibliotheken, Dateien und Ordner zu erstellen. BenutzerInnen können auch Dateien und Ordner in bestehende Bibliotheken hochladen.

    ??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

        Georg loggt sich in die Seahub-Web-Oberfläche ein, um eine Bibliothek mit Bildern von seinem Wanderausflug mit Lenina zu erstellen. Tom loggt sich ebenfalls in die Seahub-Web-Oberfläche ein, um George Orwells Aufsatz über "Politik und die englische Sprache" hochzuladen.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7e5847ea-881b-49fa-a1ab-1c16223ea366" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Synchronisations-Client"

    BenutzerInnen können mit Hilfe von Seafiles Sync-Client neue Bibliotheken erstellen oder Dateien und Ordner in bestehende Bibliotheken hochladen. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Eine neue Bibliothek erstellen | Öffnet den Seafile Sync Client, klickt auf `Select` und ruft den lokalen Ordner auf, den Ihr mit dem Server synchronisieren möchtet. Alternativ könnt Ihr den lokalen Ordner auch per Drag & Drop in den dazu vorgesehenen Bereich schieben. Gebt als Nächstes einen Namen für die neue Bibliothek an und legt fest, ob diese mit einem Kennwort verschlüsselt werden soll oder nicht. <br><br> <center> <img src="../../assets/img/seafile_desktop_client_5b.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Dateien & Ordner hinzufügen | Verwendet den Dateimanager Eures Geräts. Ruft den synchronisierten Ordner auf und fügt Dateien und Ordner hinzu. Die Änderungen werden mit dem Seafile-Server sowie mit allen verbundenen Geräten synchronisiert. |

        </center>


=== "SeaDrive"

    SeaDrive funktioniert wie ein Netzlaufwerk. BenutzerInnen können direkt über den Dateimanager des Geräts neue Bibliotheken, Dateien und Ordner erstellen oder vorhandene hinzufügen. Weitere Einzelheiten findet Ihr in Seafiles Benutzerhandbuch, das die Besonderheiten von [SeaDrive Version 1](https://help.seafile.com/drive_client/using_drive_client/) sowie [SeaDrive Version 2](https://help.seafile.com/drive_client/drive_client_2.0_for_windows_10/) beschreibt.



<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Seafile Daten synchronisieren" width="150px"></img> </center>

## Daten synchronisieren

=== "Synchronisations-Client"

    Seafile arbeitet mit sogenannten Bibliotheken, die die eigentlichen Dateien und Ordner enthalten. Der Sync-Client sorgt dafür, dass jede lokale Änderung an einer Bibliothek auf den Seafile-Server und andere verbundene Geräte gespiegelt wird und umgekehrt. Untenstehend wird genauer beschrieben, wie Ihr Bibliotheken synchronisieren könnt.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Bibliothek synchronisieren | Öffnet den Sync-Client auf Eurem Gerät und klickt mit der rechten Maustaste auf die Bibliothek. Wählt dann den Menüeintrag `Sync this library`. Alle Dateien und Ordner werden vom Server auf das lokale Gerät heruntergeladen. Von nun an wird jede Änderung an lokalen Dateien auf den Server gespiegelt und umgekehrt.<br><center> <img src="../../assets/img/seafile_desktop_client_4.png" alt="Seafile Desktop Sync" width="200px"></img> </center> |
        | Bibliothek entsynchronisieren | Klickt mit der rechten Maustaste auf die Bibliothek und wählt den Menüeintrag `Unsync`, um die Synchronisierung einer Bibliothek zu beenden. Änderungen an lokalen Dateien werden nun nicht mehr zwischen Server und Geräten gespiegelt. |
        | Cloud-Datei-Browser | Der Sync-Client bietet einen sogenannten Cloud-Datei-Browser. Mit diesem können BenutzerInnen ohne vorherige Synchronisierung direkt auf Server-Dateien zugreifen und diese abändern. Klickt dazu mit der rechten Maustaste auf eine (unsynchronisierte) Bibliothek und wählt den Menüeintrag `Open Cloud File Browser`. <br><br><center> <img src="../../assets/img/seafile_desktop_client_6.png" alt="Seafile Desktop Sync" width="250px"></img> </center> |
        | Unterordner synchronisieren | Um das Synchronisieren großer Bibliotheken zu vermeiden, ist es möglich nur bestimmte Unterordner zu spiegeln. Klickt dazu mit der rechten Maustaste auf die Bibliothek, wählt den Menüeintrag `Open Cloud File Browser`, klickt mit der rechten Maustaste auf den Unterordner und wählt den Menüeintrag `Sync this folder`. <br><br><center> <img src="../../assets/img/seafile_desktop_client_7.png" alt="Seafile Desktop Sync" width="250px"></img> </center> |

        </center>


    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        Georg verwendet den Seafile Sync-Client, um seine Wanderbilder mit seinem Arbeitsplatzrechner zu synchronisieren. Er greift dann auf die lokalen Kopien seiner Bilder zu und verschiebt diese in einen neu erstellten Ordner mit dem Namen *holidays*. Diese lokalen Änderungen werden sofort auf den Seafile-Server gespiegelt, ebenso wie auf alle anderen verbundenen Geräte. Nachdem Georg sich wieder in die Seahub-Web-Oberfläche einloggt hat, erscheint der neue Ordner *holidays*.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/9e254a58-7a44-4687-b521-897731db9983" frameborder="0" allowfullscreen></iframe>
        </center>


=== "SeaDrive"

    Der SeaDrive-Client funktioniert wie ein Netzlaufwerk. BenutzerInnen können direkt auf Dateien zugreifen, die auf dem Seafile-Server gespeichert sind, ohne vorherige Synchronisation. Untenstehend eine ausführliche Anleitung zur Verwendung von SeaDrive.

    ??? info "Mehr Informationen über den Synchronisationsstatus"

        Im Windows Explorer werden verschiedene Symbole angezeigt, die den Synchronisierungsstatus von Dateien und Ordnern anzeigen.

        <center>

        | Status | Symbol |Beschreibung |
        | :------: | :------: | ------ |
        |Nur in der Cloud |<img src="../../assets/img/cloud_only.png" alt="Seafile cloud only" width="30px"></img> |Dies ist der Standardzustand von Dateien und Ordnern. Sie werden in SeaDrive angezeigt, aber nicht tatsächlich auf das lokale Gerät heruntergeladen. |
        |Synchronisiert |<img src="../../assets/img/synced.png" alt="Seafile synched" width="30px"></img> |Sobald sich ein Benutzer entscheidet, auf eine Datei oder einen Ordner zuzugreifen, wird dieser auf das lokale Gerät heruntergeladen. |
        |Teilweise synchronisiert |<img src="../../assets/img/partial_synced.png" alt="Seafile partially synched" width="30px"></img> |Ordner, die sowohl `nur in der Cloud` als auch `synchronisierte` Dateien enthalten, werden als `teilweise synchronisiert` betrachtet.|
        |Von anderen Nutzern gesperrt |<img src="../../assets/img/locked.png" alt="Seafile locked" width="30px"></img> |Von anderen Benutzern gesperrte Dateien können nur im Lesemodus geöffnet, aber nicht geändert, gelöscht oder umbenannt werden. |
        |Von mir gesperrt |<img src="../../assets/img/locked_by_me.png" alt="Seafile locked" width="30px"></img> |Eine vom Eigentümer gesperrte Datei oder ein gesperrter Ordner kann nicht von anderen BenutzerInnen geändert werden. |
        |Schreibgeschützt |<img src="../../assets/img/read_only.png" alt="Seafile read only" width="30px"></img> |Dateien oder Ordner, die im schreibgeschützten Modus für einen Benutzer freigegeben sind, können geöffnet, aber nicht geändert, gelöscht oder umbenannt werden. |

        </center>


<br>

<center> <img src="../../assets/img/separator_ip.svg" alt="Seafile Dateien teilen" width="150px"></img> </center>

## Dateien teilen

=== "Freigabe-Links"

    Jegliche Person mit einem öffentlichen Freigabelink und einer funktionsfähigen VPN-Verbindung kann auf freigegebene Dateien und Ordner zugreifen. Es ist kein Seafile-Konto oder Login erforderlich. Freigabelinks können durch ein Passwort geschützt werden, nach einer bestimmten Zeit ablaufen oder über eingeschränkte Zugriffsrechte verfügen. Untenstehend findet Ihr eine ausführliche Anleitung zum Erstellen von Freigabelinks.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Ruft die freizugebende Datei oder den freizugebenden Ordner auf.|
        |Schritt 2 |Bewegt den Mauszeiger über die Datei oder den Ordner und klickt auf das Symbol `Share`. |
        |Schritt 3 |Klickt im nun erscheinenden Fenster auf die Schaltfläche `Share Link`. |
        |Schritt 4 |Klickt auf die Schaltfläche `Generate`, um den Freigabelink zu erstellen. |
        |Schritt 5 |Legt optional ein Passwort, eine Ablaufzeit oder Zugriffsrechte fest. |
        |Schritt 6 |Teilt den Link mit anderen per E-Mail, Messenger usw. |

        </center>


=== "Hochlade-Links"

    Jegliche Person mit einem öffentlichen Hochlade-Link und einer funktionsfähigen VPN-Verbindung kann Dateien und Ordner auf den Server hochladen. Es ist kein Seafile-Konto oder Login erforderlich. Hochlade-Links können mit einem Passwort geschützt werden. Untenstehend findet Ihr eine ausführliche Anleitung zum Erstellen von Hochlade-Links.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Ruft den Ordner auf, in den die Dateien hochgeladen werden sollen. |
        |Schritt 2 |Bewegt den Mauszeiger über den Ordner und klickt auf das Symbol `Share`. |
        |Schritt 3 |Klickt im nun erscheinenden Fenster auf die Schaltfläche `Upload Link`. |
        |Schritt 4 |Klickt auf die Schaltfläche `Generate`, um den Hochlade-Link zu erstellen. |
        |Schritt 5 |Legt optional ein Passwort fest. |
        |Schritt 6 |Teilt den Link mit anderen per E-Mail, Messenger usw. |

        </center>


=== "Freigabe für Seafile-BenutzerInnen"

    Bibliotheken, Dateien und Ordner können mit anderen Seafile-BenutzerInnen oder Gruppen geteilt werden. Damit diese auf freigegebene Dateien zugreifen können, sind eine VPN-Verbindung, ein Seafile-Konto sowie die richtigen Zugriffsrechte nötig. Untenstehend findet Ihr eine ausführliche Anleitung zur Freigabe von Bibliotheken, Dateien und Ordnern für andere Seafile-BenutzerInnen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Ruft die Bibliothek oder den Ordner auf, den Ihr für andere Seafile-BenutzerInnen freigeben möchtet. |
        |Schritt 2 |Bewegt den Mauszeiger über die Bibliothek oder den Ordner und klickt auf das Symbol `Share`. |
        |Schritt 3 |Klickt im nun erscheinenden Fenster auf die Schaltfläche `Share to user` oder `Share to group`. |
        |Schritt 4 |Wählt BenutzerInnen oder Gruppen aus und klickt auf die Schaltfläche `Submit`. |
        |Schritt 5 |Legt die Zugriffsrechte für die freigegebenen Dateien und Ordner fest, z. B. `read-write`, `read-only`, `online read-write` oder `online read-only`. |

        </center>


<div style="margin-top:-20px;">
</div>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    **Freigabe-Links**: Tom möchte Orwells Essay mit einer Freundin aus dem Buchklub teilen. Diese Freundin kann sich über VPN mit dem Server verbinden, hat aber kein eigenes Seafile-Konto. Tom meldet sich daher in der Seahub-Weboberfläche an, um ihr einen Freigabelink zu schicken, der nach einer Woche abläuft.

    **Freigabe für Seafile-BenutzerInnen**: Georg möchte die Bilder seiner Wandertour mit Lenina teilen. Lenina kann sich über VPN mit dem Server verbinden und besitzt ein Seafile-Konto. Georg meldet sich also in der Seahub-Weboberfläche an und gibt den Ordner *holidays* für Lenina frei, im "Nur-Lesen"-Modus.

    **Hochlade-Links**: Georg möchte einige Arbeitsdokumente von einem Kollegen einsammeln. Dieser Kollege kann sich per VPN mit dem Server verbinden, hat aber keinen eigenen Seahub-Account. Georg meldet sich daher in der Seahub-Weboberfläche an, erstellt eine neue Bibliothek mit dem Namen *work* und sendet einen Hochlade-Link an seinen Kollegen.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4a00a8e9-b42c-4adb-ac7d-f1b1e21b40c2" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_ntp.svg" alt="Seafile Dateien wiederherstellen" width="150px"></img> </center>

## Dateien wiederherstellen

Seafile verfolgt automatisch die Änderungshistorie aller Dateien, Ordner und Bibliotheken. Sicherungskopien alter Dateiversionen sowie Schnappschüsse von Ordner- und Bibliotheksstrukturen werden für einen vordefinierten Zeitraum aufbewahrt. Dateien, Ordner oder ganze Bibliotheken können somit im Falle eines versehentlichen Löschens oder einer fehlerhaften Handhabung in einen vorherigen Zustand zurückversetzt werden.

=== "Aufbewahrungsfrist"

    Die Aufbewahrungsfrist legt fest, wie lange Seafile Dateiversionen oder Bibliothek-Schnappschüsse aufbewahrt. Sie kann für jede Bibliothek separat konfiguriert werden, untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Meldet Euch über die Seahub-Web-Oberfläche an. Klickt im Navigationsbereich auf `My Libraries`. |
        |Schritt 2 |Bewegt den Mauszeiger über die Bibliothek. |
        |Schritt 3 |Klickt auf `History Setting`. |
        |Schritt 4 |Definiert die Länge der Aufbewahrungsfrist. |

        </center>


=== "Datei-Historie"

    Untenstehend findet Ihr eine ausführliche Erklärung, wie frühere Versionen einer Datei wiederhergestellt werden können.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Meldet Euch über die Seahub-Weboberfläche an. Ruft den Ordner, der die wiederherzustellende Datei enthält, auf. |
        |Schritt 2 |Bewegt den Mauszeiger über die Datei. |
        |Schritt 3 |Klickt auf `History`. |
        |Schritt 4 |Ladet eine beliebige ältere Version der Datei herunter, stellt sie wieder her oder zeigt sie an. Bei Textdateien könnt Ihr ebenfalls den Inhalt von zwei Versionen vergleichen. Beachtet dabei, dass ältere Versionen einer Datei nicht angezeigt werden können, wenn diese die Aufbewahrungsfrist überschreiten. |

        </center>


=== "Papierkorb"

    Untenstehend findet Ihr eine ausführliche Erklärung, wie gelöschte Dateien oder Ordner mit Hilfe des Papierkorbs wiederhergestellt werden können.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Meldet Euch über die Seahub-Web-Oberfläche an. Ruft den übergeordneten Ordner auf, der die gelöschte Datei oder den gelöschten Ordner enthielt. |
        |Schritt 2 |Klickt auf das `Papierkorb` Symbol in der Betriebsleiste der Bibliothek. |
        |Schritt 3 |Stellt die gelöschte Datei oder den Ordner wieder her. Beachtet dabei, dass Dateien und Ordner nicht wiederhergestellt werden können, wenn diese vor der Aufbewahrungsfrist gelöscht wurden. |

        </center>


=== "Bibliothek-Schnappschüsse"

    Mithilfe von Schnappschüssen könnt Ihr ältere Versionen einer kompletten Bibliothek wiederherstellen, einschließlich Datei- und Ordnerstruktur. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 |Meldet Euch über die Seahub-Web-Oberfläche an. Ruft die wiederherzustellende Bibliothek auf. |
        |Schritt 2 |Klickt auf die `History` Schaltfläche in der oberen Leiste. |
        |Schritt 3 |Wählt einen früheren Zustand der Bibliothek aus und klickt auf `View snapshot`. |
        |Schritt 4 |Ihr könnt eine beliebige ältere Version einer Datei oder eines Ordners herunterladen, wiederherstellen oder anzeigen. Wenn Ihr Eigentümer der Bibliothek seid, könnt Ihr die komplette Bibliothek in ihrem vorherigen Zustand wiederherstellen. Beachtet dabei, dass auf Schnappschüsse nicht zugegriffen werden kann, wenn diese die Aufbewahrungsfrist überschreiten. |

        </center>


<div style="margin-top:-20px;">
</div>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    **Aufbewahrungsfrist**: Um Speicherplatz zu sparen, weist Georg Seafile an, ältere Versionen seiner Bilder nur für 2 Monate aufzubewahren.

    **Bibliothek-Schnappschüsse**: Georg möchte ebenfalls die Ordnerstruktur wiederherstellen, die vor dem Anlegen des Ordners *holiday* vorhanden war. Er stellt daher einen älteren Schnappschuss der Bibliothek wieder her.

    **Papierkorb**: Tom stellt mit Hilfe des Papierkorbs Orwells Aufsatz wieder her, nachdem er ihn versehentlich gelöscht hat.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5797ebdf-9075-435d-9aad-3a578d900e42" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="Seafile Dateien verschlüsseln" width="150px"></img> </center>

## Dateien verschlüsseln

Seafile bietet eine Client-seitige durchgängige Verschlüsselung an. Bibliotheken können mit einem Passwort verschlüsselt werden, wodurch der Zugriff auf autorisierte BenutzerInnen beschränkt wird. Niemand sonst kann auf den Inhalt von verschlüsselten Bibliotheken zugreifen, nicht einmal der Seafile-Administrator. Beachtet hierbei, dass verschlüsselte Bibliotheken nur Dateiinhalte verschlüsseln, nicht aber Ordner- und Dateinamen.


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    Tom arbeitet an einem vertraulichen Projekt. Er erstellt eine verschlüsselte Bibliothek, um seine Projektdokumente sicher aufzubewahren.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ea8ce4f0-58fb-4284-9072-5c60e2830cad" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_seafile.svg" alt="Seafile Upgrade" width="150px"></img> </center>

## Upgrade

Seafiles Upgrade-Prozess ist vorwiegend manuell und es kann auch mal etwas schief gehen. Geht kein Risiko ein, synchronisiert Eure Bibliotheken mit einem Gerät und [erstellt eine Sicherungskopie Eurer Daten](https://gofoss.net/de/backups/)!

Lest unbedingt [Seafiles Upgrade-Anleitung](https://manual.seafile.com/upgrade/upgrade/) sowie die [detaillierten Upgrade-Hinweise](https://manual.seafile.com/upgrade/upgrade_notes_for_9.0.x/). Nachfolgend findet Ihr eine ausführliche Anleitung für ein Hauptversions-Upgrade.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Für die Zwecke dieses Tutorials nehmen wir an, dass Ihr ein Upgrade von Version 7.0.1 auf Version 8.0.1 durchführen möchtet. Dies wird als "Hauptversion-Upgrade" bezeichnet. "Kleinere Version-Upgrades", zum Beispiel von 7.0.1 auf 7.1.1, funktionieren ähnlich.

    [Lest zunächst die Upgrade-Hinweise](https://manual.seafile.com/upgrade/upgrade/). Diese beschreiben bestimmte Einstellungen oder Änderungen, die vor oder während des Upgrades vorgenommen werden müssen.

    Ladet dann die [neueste Version von Seafiles Server für Linux](https://www.seafile.com/en/download/) herunter und entpackt diese. In unserem Beispiel wäre das die Version 8.0.1. Stellt sicher, dass Ihr in den nachfolgenden Befehlen die Versionsnummer entsprechend anpasst:

    ```bash
    sudo wget -4 https://download.seadrive.org/seafile-server_8.0.1_x86-64.tar.gz
    sudo tar -xvf seafile-server_*
    sudo mv seafile-server-* /var/www/seafile
    sudo mv seafile-server_* /var/www/seafile/installed
    ```

    Legt die richtigen Zugriffsrechte fest:

    ```bash
    sudo chown -R seafileadmin:seafileadmin /var/www/seafile
    sudo chmod -R 755 /var/www/seafile
    ```

    Überprüft, ob alle Dateien vorhanden sind:

    ```bash
    sudo ls -al /var/www/seafile
    ```

    Die Verzeichnisstruktur sollte in etwa folgendermaßen aussehen:

    ```bash
    /var/www/seafile/
    -- ccnet
    -- conf
    -- installed
    -- logs
    -- pids
    -- seafile-data
    -- seafile-server-7.0.1
    -- seafile-server-8.0.1
    -- seafile-server-latest
    -- seahub-data
    ```

    Fahrt den Seafile-Server herunter:

    ```bash
    sudo systemctl stop seafile
    sudo systemctl stop seahub
    ```

    Ruft eine Liste der verfügbaren Upgrade-Skripte ab:

    ```bash
    sudo ls -al /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_*
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    ...
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.0_6.1.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.1_6.2.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.2_6.3.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.3_7.0.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.0_7.1.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.1_8.0.sh
    ...
    ```

    Führt nun das oder die erforderliche(n) Upgrade-Skript(e) aus. Beginnt mit der aktuellen Seafile-Version und arbeitet Euch sich bis zur neuesten Version durch. In unserem Beispiel:

    * müsst Ihr zuerst das Skript `upgrade_7.0_7.1.sh` ausführen, um von Version 7.0.1 auf Version 7.1.x zu aktualisieren
    * Ihr müsst dann das Skript `upgrade_7.1_8.0.sh` ausführen, um von Version 7.1.x auf Version 8.0.1 zu aktualisieren

    Beginnt mit dem ersten Upgrade-Skript:

    ```bash
    sudo bash /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.0_7.1.sh
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    -------------------------------------------------------------
    This script would upgrade your seafile server from 7.0 to 7.1
    Press [ENTER] to contiune
    -------------------------------------------------------------

    Updating seafile/seahub database ...

    [INFO] You are using MySQL
    [INFO] updating seahub database...
    Done

    migrating avatars ...

    Done

    updating /var/www/seafile/seafile-server-latest symbolic link to /var/www/seafile/seafile-server-7.1.0 ...
    ```

    Nun das nächste Upgrade-Skript:

    ```bash
    sudo bash /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.1_8.0.sh
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    -------------------------------------------------------------
    This script would upgrade your seafile server from 7.1 to 8.0
    Press [ENTER] to contiune
    -------------------------------------------------------------

    Updating seafile/seahub database ...

    [INFO] You are using MySQL
    [INFO] updating seahub database...
    Done

    migrating avatars ...

    Done

    updating /var/www/seafile/seafile-server-latest symbolic link to /var/www/seafile/seafile-server-8.0.1 ...
    ```

    Fahrt abschließend den Seafile-Server wieder hoch:

    ```bash
    sudo systemctl start seafile
    sudo systemctl start seahub
    ```

    Wenn alles gut gelaufen ist, sollte Seafile betriebsbereit sein und den Status "Aktiv" anzeigen:

    ```bash
    sudo systemctl status seafile
    sudo systemctl status seahub
    ```

    Wenn Ihr wollt und soweit alles richtig funktioniert, könnt Ihr die vorherige Version nun entfernen:

    ```bash
    sudo rm -rf /var/www/seafile/seafile-server-7.0.1
    ```

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Seafile Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr im [Seafile-Serverhandbuch](https://manual.seafile.com/). Ihr könnt auch gerne die [Seafile-Gemeinschaft](https://forum.seafile.com/) um Unterstützung bitten.

<br>
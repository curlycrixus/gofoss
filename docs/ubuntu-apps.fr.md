---
template: main.html
title: Les 40 applications Ubuntu les plus populaires
description: Applications Ubuntu. Comment télécharger les applis Ubuntu. Comment installer les applis Ubuntu. App store Ubuntu. Top applications Ubuntu. Centre de logiciels Ubuntu.
---

# Les 40 applications Ubuntu les plus populaires

!!! level "Destiné aux utilisateurs expérimentés. Certaines compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/laptop.png" alt="Applications Ubuntu les plus populaires" width="300px"></img>
</div>


## Applis préinstallées

<center>

| Applis Ubuntu | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/unzip.svg" alt="Gestionnaire d'archives Ubuntu" width="50px"></img> | [Gestionnaire d'archives](https://wiki.gnome.org/Apps/FileRoller) | Compressez et extrayez des fichiers archives (.zip, .rar, .tar, etc.). |
| <img src="../../assets/img/transmission.svg" alt="Sauvegardes Ubuntu" width="50px"></img> | [Sauvegardes](https://apps.gnome.org/fr/app/org.gnome.DejaDup/) | Sauvegardez vos fichiers vers des emplacements locaux, distants ou dans le nuage. Possibilité de chiffrer vos sauvegardes et de programmer des sauvegardes incrémentielles. |
| <img src="../../assets/img/calculator.svg" alt="Calculatrice Ubuntu" width="50px"></img> | [Calculatrice](https://apps.gnome.org/fr/app/org.gnome.Calculator/) | Effectuez des calculs arithmétiques, scientifiques ou financiers. |
| <img src="../../assets/img/simplecalendar.svg" alt="Agenda Ubuntu" width="50px"></img> | [Agenda](https://apps.gnome.org/fr/app/org.gnome.Calendar/) | Planifiez vos rendez-vous, suivez votre agenda. |
| <img src="../../assets/img/cheese.svg" alt="Ubuntu cheese" width="50px"></img> | [Cheese](https://apps.gnome.org/fr/app/org.gnome.Cheese/) | Capturez des vidéos et des photos à partir de votre webcam. |
| <img src="../../assets/img/baobab.svg" alt="Ubuntu baobab" width="50px"></img> | [Analyseur d'utilisation du disque](https://apps.gnome.org/fr/app/org.gnome.baobab/) |Analysez les volumes des périphériques ou des répertoires spécifiques et affichez l'utilisation vos disques. |
| <img src="../../assets/img/mysql.svg" alt="Ubuntu disques" width="50px"></img> | [Disques](https://apps.gnome.org/fr/app/org.gnome.DiskUtility/) |Inspectez, formatez, partitionnez et configurez vos disques. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu numériseur de documents" width="50px"></img> | [Numériseur de documents](https://apps.gnome.org/fr/app/simple-scan/) |Numérisez, recadrez, faites pivoter, imprimez et enregistrez du texte et des images. |
| <img src="../../assets/img/evince.svg" alt="Ubuntu evince" width="50px"></img> | [Visionneur de documents](https://apps.gnome.org/fr/app/org.gnome.Evince/) | Affichez des fichiers du format .pdf, .ps, .xps, .tiff, .djvu, .cbr, .cbz, etc. |
| <img src="../../assets/img/simplefilemanager.svg" alt="Ubuntu nautilus" width="50px"></img> | [Fichiers](https://apps.gnome.org/fr/app/org.gnome.Nautilus/) | Recherchez, explorez et gérez vos fichiers. |
| <img src="../../assets/img/firefox.svg" alt="Ubuntu firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/fr/firefox/new/) | Naviguez sur la toile. |
| <img src="../../assets/img/simplegallery.svg" alt="Ubuntu eye of gnome" width="50px"></img> | [Visionneur d'images](https://apps.gnome.org/fr/app/org.gnome.eog/) | Affichez des photos uniques ou des collections d'images, des diaporamas plein écran ou des fonds d'écran. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu libreoffice" width="50px"></img> | [LibreOffice](https://fr.libreoffice.org/) | Traitez des documents Word, des feuilles de calcul, des présentations, etc. Entièrement compatible avec les formats Microsoft (.doc, .xls et .ppt). |
| <img src="../../assets/img/ssh.svg" alt="Ubuntu gestionnaire de mots de passe" width="50px"></img> | [Mots de passe & clés](https://apps.gnome.org/fr/app/org.gnome.seahorse.Application/) | Gérez vos clés de chiffrement (PGP, SSH). |
| <img src="../../assets/img/rhythmbox.svg" alt="Ubuntu rhythmbox" width="50px"></img> | [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox/) | Écoutez vos albums, organisez vos fichiers audio, créez vos listes de lecture, écoutez vos podcasts et accédez à d'autres médias en ligne. |
| <img src="../../assets/img/cheese.svg" alt="Ubuntu capture d'écran" width="50px"></img> | [Capture d'écran](https://help.gnome.org/users/gnome-help/stable/screen-shot-record.html.fr) | Enregistrez des images de votre écran ou de fenêtres individuelles. |
| <img src="../../assets/img/shot_well.svg" alt="Ubuntu shotwell" width="50px"></img> | [Shotwell](https://wiki.gnome.org/Apps/Shotwell/) | Importez, organisez et visualisez vos photos. |
| <img src="../../assets/img/terminal.svg" alt="Le terminal d'Ubuntu" width="50px"></img> | [Terminal](https://apps.gnome.org/fr/app/org.gnome.Console/) | Accédez à une interface pour exécuter des commandes en mode texte. |
| <img src="../../assets/img/simplecalendar.svg" alt="Éditeur de texte Ubuntu" width="50px"></img> | [Éditeur de texte](https://apps.gnome.org/fr/app/org.gnome.TextEditor/) | Éditez des fichiers texte. |
| <img src="../../assets/img/simpleemail.svg" alt="Ubuntu thunderbird" width="50px"></img> | [Thunderbird](https://www.thunderbird.net/fr/) | Accédez à vos courriels, calendriers, carnets d'adresses et listes de tâches. |
| <img src="../../assets/img/totem.svg" alt="Ubuntu totem" width="50px"></img> | [Vidéos](https://apps.gnome.org/fr/app/org.gnome.Totem/) | Regardez des films. |

</center>


## Autres applis populaires

Des chapitres précédents vous ont montré comment installer des applis populaires telles que [Firefox](https://gofoss.net/fr/firefox/), [Tor](https://gofoss.net/fr/tor/), [Protonmail et Tutanota](https://gofoss.net/fr/encrypted-emails/), [Signal](https://gofoss.net/fr/encrypted-messages/), [Keepass](https://gofoss.net/fr/passwords/), ou [VeraCrypt et Cryptomator](https://gofoss.net/fr/encrypted-files/). De nombreuses autres applications sont disponibles pour Ubuntu. Vous trouverez ci-dessous une sélection d'applis populaires qui pourraient vous être utiles. Il existe deux façons simples de les installer :

* **Le gestionnaire de logiciels**, qui a été présenté dans le [chapitre précédent sur Ubuntu](https://gofoss.net/fr/ubuntu/#gestionnaire-de-logiciels/). C'est un magasin d'applis pour rechercher, installer, mettre à jour ou supprimer des logiciels. Cliquez sur le bouton `Activités` en haut à gauche, cherchez `Logiciels` et ouvrez-le. Choisissez une application, lisez la page de description, cliquez sur le bouton `Installer` et entrez le mot de passe administrateur (« root ») pour continuer.

* **Le terminal** est un autre moyen d'installer, de mettre à jour ou de supprimer des logiciels. Ouvrez-le avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Par exemple, la commande suivante installe Firefox via le terminal : `sudo apt install firefox`. Voilà !

<center>

| Applis Ubuntu | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu mousepad" width="50px"></img> | [Mousepad](https://github.com/codebrainz/mousepad/) | `Éditeur de texte`: simple, facile d'utilisation et rapide. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install mousepad` |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu onlyoffice" width="50px"></img> | [OnlyOffice](https://www.onlyoffice.com/fr/) | `Suite Office`: logiciel libre et open source pour traiter des documents Word, des feuilles de calcul, des présentations, etc. Entièrement compatible avec les formats Microsoft (.doc, .xls et .ppt). Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou suivez les [instructions d'OnlyOffice](https://helpcenter.onlyoffice.com/installation/desktop-install-ubuntu.aspx?_ga=2.123053453.764533964.1595236576-1157782750.1587541027/).|
| <img src="../../assets/img/maps.svg" alt="Cartes de GNOME" width="50px"></img> | [Cartes de Gnome](https://apps.gnome.org/fr/app/org.gnome.Maps/) | `Cartes` : cartes libres et open source. Utilise la base de données collaborative OpenStreetMap. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install gnome-maps`|
| <img src="../../assets/img/vlc.svg" alt="Ubuntu vlc" width="50px"></img> | [VLC](https://www.videolan.org/vlc/) | `Vidéos`: lecteur multimédia libre et open source. Prend en charge la plupart des formats de fichiers et des protocoles de diffusion. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install vlc`|
| <img src="../../assets/img/totem.svg" alt="Ubuntu mplayer" width="50px"></img> | [Mplayer](http://mplayerhq.hu/) | `Vidéos`: lecteur multimédia libre et open source. Prend en charge la plupart des formats de fichiers. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install mplayer`|
| <img src="../../assets/img/ffmpeg.svg" alt="Ubuntu ffmpeg" width="50px"></img> | [Ffmpeg](https://ffmpeg.org/) | `Traitement de médias` : outil open source pour convertir et modifier différents formats de fichiers tels que .mp4, .avi, .mov, .mkv, .mp3, .wav, etc. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install ffmpeg`|
| <img src="../../assets/img/gimp.svg" alt="Ubuntu gimp" width="50px"></img> | [Gimp](https://www.gimp.org/) | `Traitement d'images`: alternative à Adobe Photoshop, libre et open source, avec de nombreuses extensions pour accéder à des fonctionnalités additionnelles. Prend en charge les formats de fichiers tels que .bmp, .gif, .jpeg, .mng, .pcx, .pdf, .png, .ps, .psd, .svg, .tiff, .tga, .xpm, etc. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install gimp gimp-data`|
| <img src="../../assets/img/gimp.svg" alt="Ubuntu krita" width="50px"></img> | [Krita](https://krita.org/fr/) | `Traitement d'images`: programme de dessin libre et open source pour l'art conceptuelle, la peinture de texture et de matte, les illustrations et les bandes dessinées. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `$ sudo add-apt-repository ppa:kritalime/ppa && sudo apt update && sudo apt install krita`|
| <img src="../../assets/img/audacity.svg" alt="Ubuntu tenacity" width="50px"></img> | [Audacity](https://www.audacityteam.org/) | `Traitement audio`: logiciel libre et open source pour enregistrer, échantillonner, éditer, convertir, analyser et ajouter des effets à des fichiers sonores. *Attention* : [les discussions autour de la télémétrie d'Audacity](https://github.com/audacity/audacity/pull/835/) ont donné lieu à un fork appelé [Tenacity](https://tenacityaudio.org/), qu'il peut être intéressant de consulter. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install audacity`|
| <img src="../../assets/img/openshot.svg" alt="Ubuntu openshot" width="50px"></img> | [OpenShot](https://www.openshot.org/fr/) | `Traitement vidéo`: éditeur vidéo libre et open source pour redimensionner, mettre à l'échelle, découper, couper, zoomer, faire des transitions et ajouter des effets à des fichiers vidéo. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo add-apt-repository ppa:openshot.developers/ppa && sudo apt update && sudo apt install openshot-qt` |
| <img src="../../assets/img/openshot.svg" alt="Ubuntu blender" width="50px"></img> | [Blender](https://www.blender.org/) | `Traitement vidéo`: logiciel de création 3D libre et open source, incluant modélisation, animation, simulation, rendu, suivi de mouvement et montage vidéo. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install snapd && sudo snap install blender --classic`|
| <img src="../../assets/img/games.svg" alt="Ubuntu steam" width="50px"></img> | [Steam](https://store.steampowered.com/linux/) | `Jeux vidéo`: plate-forme de distribution de jeux vidéo. Jouez à des milliers de jeux de renom sur votre ordinateur Linux. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install steam-installer`|
| <img src="../../assets/img/empathy.svg" alt="Ubuntu empathy" width="50px"></img> | [Empathy](https://wiki.gnome.org/action/show/Attic/Empathy?action=show&redirect=Apps%2FEmpathy) | `Tchat`: tchattez par texte, messages vocaux et vidéo. Les protocoles propriétaires sont pris en charge : Google Talk, MSN, IRC, AIM, Facebook, Yahoo, ICQ, etc. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install empathy` |
| <img src="../../assets/img/pidgin.svg" alt="Ubuntu pidgin" width="50px"></img> | [Pidgin](https://pidgin.im/) | `Tchat`: client de messagerie libre et open source. Supporte de nombreux protocoles dont MSN, AIM, Yahoo, Jabber, IRC, IRQ, etc. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install pidgin`|
| <img src="../../assets/img/money.svg" alt="Ubuntu gnucash" width="50px"></img> | [GNUcash](https://gnucash.org/index.phtml?lang=fr_FR) | `Finance`: logiciel de comptabilité libre et open source pour les finances personnelles et celles des petites entreprises. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install gnucash`|
| <img src="../../assets/img/deluge.svg" alt="Ubuntu deluge" width="50px"></img> | [Deluge](https://www.deluge-torrent.org/) | `Torrent`: client torrent libre et léger. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install deluge`|
| <img src="../../assets/img/transmission.svg" alt="Ubuntu transmission" width="50px"></img> | [Transmission](https://transmissionbt.com/) |`Torrent`: transmettez et recevez des fichiers à l'aide du protocole BitTorrent. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install transmission`|
| <img src="../../assets/img/simplefilemanager.svg" alt="Ubuntu filezilla" width="50px"></img> | [FileZilla](https://filezilla-project.org/) | `FTP`: client FTP libre. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install filezilla`|
| <img src="../../assets/img/unzip.svg" alt="Ubuntu rar" width="50px"></img> | [Rar, Unrar and Unzip](https://itsfoss.com/use-rar-ubuntu-linux/) | `Compression de fichiers`: trois programmes légers pour compresser ou extraire des archives (.zip, .rar, .tar, etc.). Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install rar unrar unzip`|
| <img src="../../assets/img/bleachbit.svg" alt="Ubuntu bleachbit" width="50px"></img> | [Bleachbit](https://www.bleachbit.org/) | `Entretien`: outil open source pour garder votre ordinateur désencombré. Libérez la mémoire « cache », supprimez les « cookies », effacez l'historique Internet, détruisez les fichiers temporaires, supprimez les journaux et éliminez les déchets. Installez le logiciel via le gestionnaire de logiciels d'Ubuntu ou en saisissant la commande suivante dans le terminal :<br> `sudo apt install bleachbit`|

</center>


<br>

<center> <img src="../../assets/img/separator_games.svg" alt="Jeux sur Ubuntu" width="150px"></img> </center>

## Jeux

Linux n'est pas parfait. De temps à autre, vous serez confronté à ses limitations. Par exemple, de nombreux jeux vidéos ne sont pas publiés pour Linux, ou sont incompatibles. Heureusement, vous avez toujours quelques options.

### Steam

[Steam](https://store.steampowered.com/linux/) propose des milliers de jeux de renom pour Linux, pour autant que votre matériel réponde aux exigences minimales : processeur 1 GHz Penitum 4 ou AMD Opteron, 512 Mo de mémoire, 5 Go d'espace disque, une connexion Internet et une [carte graphique avec les derniers pilotes installés](https://gofoss.net/fr/ubuntu/#pilotes-graphiques/). Pour installer Steam, cliquez sur le bouton `Activités` en haut à gauche de l'écran, ouvrez le gestionnaire de logiciels Ubuntu, cherchez `Steam` et cliquez sur `Installer`.

### Lutris

[Lutris](https://lutris.net/) est une plateforme de jeu libre et [open source](https://github.com/lutris/) pour Linux. Elle permet d'installer des milliers de jeux en toute simplicité, rendant l'expérience fluide. Avant d'installer Lutris, assurez-vous d'avoir installé les [derniers pilotes graphiques](https://gofoss.net/fr/ubuntu/#pilotes-graphiques/). Puis, suivez les instructions détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

        Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez les commandes ci-dessous pour installer [Wine](https://www.winehq.org/). Il s'agit d'un programme qui fait tourner des applications Windows sous Linux :

    ```bash
    sudo dpkg --add-architecture i386
    wget -nc https://dl.winehq.org/wine-builds/winehq.key
    sudo apt-key add winehq.key
    sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ jammy main'
    sudo apt update
    sudo apt install --install-recommends winehq-stable
    ```

    Puis, installez Lutris:

    ```bash
    sudo add-apt-repository ppa:lutris-team/lutris
    sudo apt update
    sudo apt install lutris
    ```

    Une fois installé, ouvrez Lutris, [inscrivez-vous](https://lutris.net/user/register/) ou connectez-vous, [parcourez le catalogue](https://lutris.net/games/) et installez de nouveaux jeux.


<br>

<center> <img src="../../assets/img/separator_virtualbox.svg" alt="Ubuntu VirtualBox" width="150px"></img> </center>

## Logiciels Windows

Parfois, un logiciel particulier ne se comporte pas comme prévu sur votre ordinateur Ubuntu. L'exécuter sous Windows peut résoudre le problème. [VirtualBox](https://www.virtualbox.org/) est un excellent moyen d'y parvenir, car ce logiciel fait tourner un petit ordinateur Windows dans votre machine Ubuntu. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Installer VirtualBox

    Assurez-vous que votre ordinateur Ubuntu répond aux exigences minimales : 4 Go de mémoire vive, 20 Go d'espace disque libre. Cliquez sur le bouton `Activités` en haut à gauche de l'écran, ouvrez le gestionnaire de logiciels d'Ubuntu, cherchez `VirtualBox` et cliquez sur `Installer`. Ou ouvrez simplement un terminal et exécutez la commande `sudo apt install virtualbox`.


    ### Configurer VirtualBox

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Téléchargez Windows | Téléchargez un [fichier `.iso` Windows](https://www.microsoft.com/fr-fr/software-download/). Choisissez la version 32 ou 64 bits, en fonction de l'architecture de votre ordinateur. |
    | Créez une machine virtuelle | Lancez VirtualBox et cliquez sur le bouton `Nouvelle`. |
    | Nom et système d'exploitation | Donnez un nom à votre machine virtuelle (en anglais, « virtual machine ou VM »), par exemple `Windows VM`. Sélectionnez également le système d'exploitation (`Microsoft Windows`) et la version, par exemple `Windows 10 (64-bit)`. |
    | Taille de la mémoire | Choisissez combien de mémoire vive sera allouée à Windows. 2 Go sont recommandés, 3-4 Go sont encore mieux. |
    | Disque dur |  Sélectionnez `Créer un disque dur virtuel maintenant` pour ajouter un disque dur virtuel. |
    | Type de fichier de disque dur | Choisissez le format `VDI` comme type de fichier. |
    | Stockage sur disque dur physique | Choisissez `Dynamiquement alloué` comme taille du disque dur virtuel. |
    | Emplacement du fichier et taille | Choisissez l'endroit où créer le disque dur virtuel. Décidez également combien d'espace disque doit être alloué à Windows. 32 Go sont recommandés, plus est encore mieux. |
    | Sélecteur de disque optique |De retour sur l'écran principal, cliquez sur `Démarrer` pour lancer la machine virtuelle `Windows VM`. Dans la fenêtre qui s'ouvre, cliquez sur l'icône pour choisir un fichier de disque optique virtuel. Cliquez sur `Ajouter` et cherchez le fichier `.iso' Windows téléchargé précédemment. Cliquez sur `Ouvrir`, `Choisir` et `Démarrer`. |

    </center>

    ### Installer Windows in VirtualBox

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Installez Windows | Après la phase de démarrage, l'assistant d'installation de Windows s'affiche. Suivez les instructions à l'écran : sélectionnez une langue et une disposition de clavier, fournissez une clé Windows, acceptez les conditions d'utilisation, etc. |
    | Commencez à travailler avec Windows | Lancez votre première session Windows dans la machine virtuelle. A partir de maintenant, vous pouvez lancer Windows en cliquant sur le bouton `Start` sur l'écran principal de VirtualBox. |

    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Ubuntu" width="150px"></img> </center>

## Assistance

Pour davantage de précisions ou en cas de questions, référez-vous à :

* la [documentation d'Ubuntu](https://help.ubuntu.com/), les [tutoriels d'Ubuntu](https://ubuntu.com/tutorials), le [wiki d'Ubuntu](https://wiki.ubuntu.com/) ou la [communauté d'Ubuntu](https://forum.ubuntu-fr.org/)

* la [documentation Steam](https://help.steampowered.com/fr/) ou la [communauté Reddit de Lutris](https://teddit.net/r/lutris/)


Plus de suggestions relatives aux logiciels libres et open source se trouvent ici :

* [Free Software Directory](https://directory.fsf.org/wiki/Main_Page)

* [Open Source Software Directory](https://opensourcesoftwaredirectory.com)

<br>

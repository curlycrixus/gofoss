---
template: main.html
title: Contribute to the project
description: Contribute to gofoss.net. Join the community. Make suggestions. Improve the content. Help with translations. File an issue on GitLab.
---

# Get involved in the project!

<div align="center">
<img src="../../assets/img/contributing.png" alt="Contribute to gofoss.net" width="375px"></img>
</div>

Welcome! [gofoss.net](https://gofoss.net) is a free and open source project, so anyone can review the website's code and add improvements. Join us today and learn about all the ways to get involved!

* Improve content, add features, fix bugs
* Enhance the user experience, design new artwork
* Help with translations
* Moderate discussions
* Maintain the code

<div style="margin-top:-40px">
</div>


## Contribution guidelines

??? tip "Get in touch"

     No need to open an Issue on GitLab if you want to discuss or have a question. Simply reach out on social media:

     &nbsp;&nbsp; :material-twitter: [Follow @gofoss_today on Twitter](https://nitter.net/gofoss_today)

     &nbsp;&nbsp; :material-mastodon: [Follow @don_atoms on Mastodon](https://hostux.social/@don_atoms/)

     &nbsp;&nbsp; :material-reddit: [Message @gofosstoday on Reddit](https://teddit.net/u/gofosstoday/)

     &nbsp;&nbsp; :fontawesome-solid-users: [Message @gofoss on Lemmy](https://lemmy.ml/u/gofoss)

     &nbsp;&nbsp; :material-matrix: [Join our chat on Matrix](https://matrix.to/#/#gofoss-community:matrix.org/)

     &nbsp;&nbsp; :material-email: [Send us an email](mailto:gofoss@protonmail.com)


??? tip "Open an Issue"

	<center>

	| Instructions | Guidelines |
	| ------ | ------ |
	| Do some research |• Look for similar contributions before submitting your own <br>• There's a good chance somebody else had the same issue or feature proposal <br>• [Get in contact](#got-a-question) with the gofoss.net community <br>• [Search the Issue Tracker](https://gitlab.com/curlycrixus/gofoss/-/issues) <br>• [Check the Roadmap](https://gofoss.net/roadmap/) |
	| File an Issue |• If nothing shows up during your research, you can file an Issue <br>• [Sign in to GitLab](https://gitlab.com/users/sign_in) <br>• Browse to the gofoss.net [Issue Tracker](https://gitlab.com/curlycrixus/gofoss/-/issues) <br>• Click on the `New Issue` button <br>• Fill in the title <br>• Make sure to select the correct template: <br><br>`content_request`: suggestions on new or improved content, e.g. new software, missing or incorrect information, spelling or grammar mistakes, readability improvement, etc. <br><br>`feature_request`: suggestions on new website features, e.g. improved navigation, enhanced layout, new illustrations, dark mode, etc. <br><br>`bug_report`: unexpected behavior of the website, e.g. broken links, issues loading on certain devices, etc. |
    | Get involved in the review process |• All Issues are publicly discussed and reviewed <br>• Make sure to follow the discussions related to your Issue <br>• Answer any questions & review others' contributions <br>• It's as valuable as contributing yourself <br>• [Set up email notifications](https://docs.gitlab.com/ee/user/profile/notifications.html) to stay informed |
	| Wait for the Issue to be resolved or closed |• We do our best to close Issues fast <br>• In turn, we ask that you remain available for clarifying questions or corrections <br>• Please note that if we don't hear back from you, we may close your Issue |


	</center>


??? tip "Work on open Issues"

    ### Issue branching workflow

    <div align="center">
    <img src="../../assets/img/issue_branching.png" alt="Issue Branching" width="700px"></img>
    </div>

	<center>

	| Branches | Guidelines |
	| ------ | ------ |
    |`main` |• The `main` branch is the production branch <br>• Any commit to the `main` branch is automatically deployed from GitLab to Netlify <br>• The `main` branch is therefore protected <br>• It is only accessible to `maintainers` in the gofoss.net team |
    |`dev` |• The `dev` branch is created off the `main` branch <br>• This is where new content, features & patches are developed & tested <br>• The `dev` branch is only accessible to `maintainers` in the gofoss.net team <br>• The `dev` branch is merged into `main` when it is ready to be deployed into production |
    |`issue` |• Each Issue should reside in its own branch <br>• `issue` branches are created off the `dev` branch <br>• When work on an Issue is complete, it gets merged back into `dev` <br>• `issue` branches therefore never interact directly with `main` <br>• Both gofoss.net `maintainers` & external contributors can work on `issue` branches |

	</center>


    ### Forking workflow

    <div align="center">
    <img src="../../assets/img/forking_workflow.png" alt="Forking Workflow" width="700px"></img>
    </div>

	<center>

	| Instructions | Guidelines |
	| ------ | ------ |
	| Pick an Issue to work on |• [Check the Tracker](https://gitlab.com/curlycrixus/gofoss/-/issues) for Issues marked as `accepting_merge_requests` <br> • Those Issues are open for external contributions <br> • Before you start working on an Issue, get in contact with the gofoss.net team <br> • Either leave a comment in the Issue you want to work on <br> • Or send us a message via [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today) or [email](mailto:gofoss@protonmail.com) <br> • There is also a [matrix channel](https://matrix.to/#/#gofoss-dev:matrix.org/) where the gofoss.net devs hang out (invite only) <br> • Wait until someone from the gofoss.net team validates your contribution request |
    | Fork, clone & sync the gofoss.net repository |• [Sign in to GitLab](https://gitlab.com/users/sign_in) <br>• Head over to the [gofoss.net project](https://gitlab.com/curlycrixus/gofoss) <br> • Click on the `Fork` button <br>• This creates a copy of the gofoss.net project on your own GitLab repository <br> • On your local machine, open a terminal & create a project folder: <br>`mkdir ~/git_projects` <br>• Clone your GitLab repository: <br>`cd ~/git_projects` <br>`git clone https://gitlab.com/<YOUR_USERNAME>/gofoss.git` <br>• Move to the local copy of your repository: <br>`cd ~/git_projects/gofoss` <br> • Create a link with the upstream gofoss.net project to keep your fork in sync: <br>`git remote add upstream https://gitlab.com/curlycrixus/gofoss.git` <br>• Regularly sync with the `dev` branch of the upstream project: <br>`cd ~/git_projects/gofoss` <br>`git fetch upstream` <br>`git checkout dev` <br>`git pull upstream dev` <br>`git push origin dev` <br>• Else, if your fork is too outdated, your Merge Request might get rejected |
    | Set up the development environment |• Define your accounts default identity: <br>`git config user.email "<USER>@example.com"` <br>`git config user.name "<USERNAME>"` <br>• Install the lastest Python version & dependencies: <br>`sudo apt install python3 python3-pip` <br>• Make sure Python version 3 is installed: <br>`python3 --version` <br>• Install Mkdocs: <br>`sudo apt install mkdocs` <br>• Install Material for Mkdocs: <br>`pip install mkdocs-material` <br>• Install the [multi-language plugin](https://ultrabug.github.io/mkdocs-static-i18n/en/): <br>`pip install mkdocs-static-i18n` <br>• Install dependencies: <br>`pip install -r requirements.txt` |
	| Commit locally & push to remote |• Create a new `issue` branch, respecting the naming convention `[content/feature/bug]-[#issueID]-[short description]`<br>• For example: <br>`git checkout -b feature-#72-add-dark-mode` <br>• Switch to the new branch: <br>`git checkout feature-#72-add-dark-mode` <br>• Start coding: add content & features or fix bugs <br>• Preview local changes in your browser on `localhost:8000` with the command: <br> `mkdocs serve` <br>• If everything works as expected, add the code to the staging area: <br>`git add .` <br>• Commit the changes & make sure to add a clear comment: <br>`git commit -m "<CLEAR COMMENT>"` <br>• Finally, push the `issue` branch to your remote repository: <br>`git push -u origin feature-#72-add-dark-mode` |
    | Open a Merge Request |• [Sign in to GitLab](https://gitlab.com/users/sign_in) <br>• Browse to `https://gitlab.com/<YOUR_USERNAME>/gofoss/-/merge_requests` <br>• Click on the `New merge request` button <br>• Select the source branch: `<USERNAME>/gofoss/feature-#72-add-dark-mode` <br>• Select the target branch: `curlycrixus/gofoss/dev` <br>• Click on the `Compare branches and continue` button <br>• Make sure to select the `merge_request` template <br>• Provide a clear title & description for your Merge Request <br>• Select the option `Delete source branch when merge request is accepted` <br>• Click on the button `Create merge request` |
    | Get involved in the review process |• All Merge Requests are publicly discussed & reviewed <br>• Make sure to follow the discussions related to your Merge Request <br>• Answer any questions <br>• [Set up email notifications](https://docs.gitlab.com/ee/user/profile/notifications.html) to stay informed |
	| Wait for validation |• A gofoss.net `maintainer` will pull your `issue` branch into their local repository <br>• They'll review & make sure it doesn't break anything <br>• If the `maintainer` has comments, you need to update your Merge Request <br>• If all is well, the `maintainer` will merge your changes into his local `dev` branch & push to the gofoss.net repository <br>• Your contribution is now part of the gofoss.net project! <br>• Other developers should pull from the gofoss.net repository to sync |
	| Delete branch |• After your Merge Request has been merged, the `issue` branch `feature-#72-add-dark-mode` should have automatically been deleted <br>• If this is not the case, delete it manually <br>• You can now safely pull the changes from the upstream repository |
    | Licensing |By contributing to gofoss.net, you agree that your contributions will be published under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt). |

	</center>


    ### Directory structure

	Please note that the overall directory structure must remain unchanged:

	```bash
	.
	├─ .gitlab/             # issue & merge request templates
	│
	├─ docs/
	│  │
	│  ├─ assets/
	│  │  └─ css/			# stylesheets (.css), local fonts (.ttf) & admonition icons (.svg)
	│  │  └─ echarts/		# echart files (.html, .min.js)
	│  │  └─ img/			# image files (.svg, .png, etc.)
	│  │
	│  └─ *.xyz.md			# multi-language markdown files (.en.md, .fr.md, .de.md, etc.)
	│
	├─ overrides/			# theme extensions (home_xyz.html, main.html, overrides.xyz.min.css)
	│
	├─ LICENSE			    # mkdocs-material license
	│
	├─ netlify.toml		    # build instructions for netlify deploy
	│
	├─ requirements.toml	# build dependencies
	│
	├─ runtime.txt			# python version for netlify deploy
	│
	└─ mkdocs.yml			# mkdocs configuration file
	```


??? tip "Help with translations"

	<center>

	| Instructions | Guidelines |
	| ------ | ------ |
    | Join a mailing list |If you are reasonably literate in one or more of the target languages and have basic knowledge of markdown syntax, join the appropriate mailing list: <br>• [EN ‣ FR translation team (Framasoft community)](https://framalistes.org/sympa/info/framalang/) <br>• [EN ‣ DE translation team](https://framalistes.org/sympa/info/gofosslang_de/) <br>• [EN ‣ ES translation team](https://framalistes.org/sympa/info/gofosslang_es/) <br>• [EN ‣ PL translation team](https://framalistes.org/sympa/info/gofosslang_pl/) |
    | Chat with other translators | There is also a [matrix channel](https://matrix.to/#/#gofoss-translation:matrix.org/) where translators can hang out, coordinate and provide support. |
    | Get access to translation files |Translators receive a link to access the project files. |
    | Work with Framapad |• All translations are managed using the collaborative online text editor Framapad <br>• Individual contributions are identified by a color code <br>• They appear on the screen in real time <br>• Please make sure to read the README file, which provides additional guidelines |
    | Use the glossary |Be sure to use the GLOSSARY file when you translate text. |
    | Keep the original text |Insert the translation just after the original text, while keeping the original text as is. |
    | Preserve the code |Make sure all markdown and HTML formatting elements are preserved. |
    | Respect other contributors |• Please respect the work of other contributors <br>• If you disagree with a translation, you are welcome to make your own suggestions <br>• But without modifying or deleting existing translations |
    | Do not abuse your powers |Do not abuse your powers. Translation work is based on trust. |
    | Validation |Translations are published on the website once they are 100% complete and have been reviewed by the project team. |

	</center>


??? tip "Follow the Code of Conduct"

	<center>

	| Instructions | Guidelines |
	| ------ | ------ |
    |Rule #1 |Agree to collaborate with people from around the world, regardless of gender, age, nationality, ethnicity, appearance, education, social background or religion. |
    |Rule #2 |Keep the discussion friendly, respectful, constructive and on topic. There is zero tolerance for rudeness. No flame wars; no trolling; no insults; no sexualized language; no public or private harassment; no doxing; no personal, racist, political or religious attacks. |
    |Rule #3 |Respect choices of the project team. We try to read, review and respond to questions, issues and merge request as best as possible – but remember, our time is limited. We also welcome suggestions, recommendations and contributions – which doesn't imply that every decision must be group consensus. |
    |Rule #4 |Avoid private discussions on project related matters, if there's no good reason for it. Such discussions should remain public, on GitLab or via the project's official communication channels. |
    |Rule #5 |Submit any complaint to [gofoss@protonmail.com](mailto:gofoss@protonmail.com). Infringements of rules #1 to #4 may lead to appropriate corrective action. This includes kind reminders, written warnings, removal, editing or rejection of any inappropriate contribution, including posts, comments, commits, code, issues or merge requests, and ultimately a ban from the project. |

	</center>


## Want to donate?

Thanks for your interest!

Most "free" websites sell ads, their users data or pay-walled content. In contrast, gofoss.net is truly *free* (as in *free* beer): free to use, free source code, free of ads, free of tracking, free of sponsored content, free of affiliate links, free of paywalls.

gofoss.net is a volunteer-run, non-profit project. All operating costs, development and content are funded solely by user donations. If you want to donate, or buy us a coffee, please click on the button below :)

<center>

<a href="https://liberapay.com/gofoss/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

</center>

Consider also supporting other [open source developers](https://gofoss.net/thanks/). Or helping digital rights groups and software foundations to advocate for better privacy laws, user rights and the freedom to innovate:

* the [Electronic Frontier Foundation](https://www.eff.org/)
* the [Free Software Foundation](https://www.fsf.org/)
* the [Linux Foundation](https://www.linuxfoundation.org/)
* the [Apache Software Foundation](https://www.apache.org/)
* the [Freedom of the Press Foundation](https://freedom.press/)
* and many more

<div align="center">
<img src="https://imgs.xkcd.com/comics/fixing_problems.png" alt="Contributing to gofoss.net"></img>
</div>

<br>

---
template: main.html
title: gofoss.net. Digitale Freiheit für alle
description: Digitale Freiheit für alle. Der ultimative, freie und quelloffene Leitfaden zu Online-Datenschutz, Dateneigentum und nachhaltiger Technologie.
---

# Datenschutz, Privatsphäre und nachhaltige Technologie

<center> <img src="../../assets/img/about.png" alt="gofoss.net" width="400px"></img> </center>

[gofoss.net](https://gofoss.net/de/) ist ein freier und quelloffener Leitfaden zu den Themen Online-Datenschutz, Dateneigentum und nachhaltige Technologie. Das Projekt wurde im Jahr 2020 von einem [kleinen, ehrenamtlichen Team](https://gofoss.net/de/team/) ins Leben gerufen und ist zu 100% gemeinnützig – keine Reklame, kein Tracking, keine gesponsorten Inhalte, keine Werbepartner.

Wir bedauern, dass viele Technologieunternehmen ihre Gewinne auf Kosten der Privatsphäre von InternetnutzerInnen erzielen. Wir sind zunehmend besorgt über die wiederholten Eingriffe von Regierungen in die Privatsphäre der Bürger. Und wir sehen dringenden Handlungsbedarf, um technologiebedingte Umweltbelastungen einzuschränken.

[Freiheit und Privatsphäre sind grundlegende Menschenrechte](https://gofoss.net/de/nothing-to-hide/). Jeder sollte in der Lage sein, seine digitalen Geräte auf sichere und datenschutzfreundliche Weise zu nutzen. Im Internet surfen, soziale Netzwerke nutzen, Mediendateien teilen oder kollaborativ arbeiten; all dies sollte möglich sein, ohne dass jemand Daten ausspioniert, zu Geld macht oder zensiert.

<br>
---
template: main.html
title: Warum Datensicherheit wichtig ist
description: Verwendet VeraCrypt oder Cryptomator, um Eure Daten zu verschlüsseln. Ist VeraCrypt sicher? Wie benutzt man VeraCrypt? Ist Cryptomator sicher? Wie benutzt man Cryptomator? Cryptomator vs. VeraCrypt?
---

# Schützt Eure Daten, legt Sicherungskopien an, verschlüsselt Eure Daten

<center>
<img align="center" src="../../assets/img/secure_data.png" alt="Datensicherheit" width ="250px"></img>
</center>

In diesem Kapitel wird erläutert, wie Ihr:

* den [unbefugten Zugriff auf Eure Daten verhindert](https://gofoss.net/de/passwords/)
* eine [effektive Datensicherungsroutine anlegt](https://gofoss.net/de/backups/)
* Eure [Daten durch Verschlüsselung unlesbar macht](https://gofoss.net/de/encrypted-files/)


## Wir leben in einem digitalen Zeitalter

Jeden Tag erzeugt die Menschheit Unmengen an Daten. Bis zum Jahr 2025 wird der Datenberg auf [175 Zettabytes](https://www.forbes.com/sites/tomcoughlin/2018/11/27/175-zettabytes-by-2025/) anwachsen. Das sind einhundertneunundfünfzig Milliarden einhunderteinundsechzig Millionen fünfhundertzweiundsiebzigtausendachthundertzehn Terabyte.

Jeder vernetzte Mensch kommt täglich etwa 4.900 Mal mit digitalen Daten in Berührung. Im Schnitt also einmal alle 18 Sekunden. Sie sind da keine Ausnahme. Der Großteil Eures Lebens ist bereits digitalisiert und allerorts abgespeichert. Wahrscheinlich wissen Euer Telefon, Euer Computer, Eure Uhr und Eure Cloud-Dienste bereits mehr über Euch als Eure engsten Freunde und Verwandten.


<br>

<center> <img src="../../assets/img/separator_firewall.svg" alt="Sichere und individuelle Passwörter" width="150px"></img> </center>

## Schützt Eure Daten

Nutzt sichere und individuelle Kennwörter. Vermeidet die Wiederverwendung von Kennwörtern für mehrere Geräte oder Konten. Verwendet einen Passwort-Manager. Und zieht Zwei-Faktor-Authentifizierung für zusätzliche Sicherheit in Betracht.


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="3-2-1 Sicherungs-Strategie" width="150px"></img> </center>

## Legt Sicherheitskopien an

Eine effektive Datensicherungsroutine schützt vor Datenverlust, Diebstahl, Löschung, Korruption, Datenlecks, Ransomware oder Schlimmerem.


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="VeraCrypt und Cryptomator" width="150px"></img> </center>

## Verschlüsselt Eure Daten

Durch Verschlüsselung werden Daten selbst im Falle eines Sicherheitsbruchs unlesbar. Beispielsweise wenn es einem Angreifer gelingen sollte, Euren Cloud-Speicher zu hacken oder sich unbefugten Zugriff auf Euren Computer zu verschaffen.

<br>

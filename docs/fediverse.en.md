---
template: main.html
title: Fediverse | Best Alternative Social Media Platforms
description: What's the Fediverse? A Youtube alternative, Facebook alternative, Reddit alternative, Twitter alternative, Instagram alternative & Spotify alternative.
---

# A Hitchhiker's Guide to the Fediverse

!!! level "Last updated: May 2022. For beginners. No tech skills required."

<div align="center">
<img src="../../assets/img/fediverse.png" alt="Fediverse" width="400px"></img>
</div>

The Federated Universe, or Fediverse, is a free and open source alternative to commercial social media platforms.

The Fediverse is a federated network, consisting of various interconnected social networks and micro blogging services. Users can freely communicate and share pictures, videos, music and more. At the time of writing, [the Fediverse counted over 5 million users](https://the-federation.info/). The Fediverse is:

* **Decentral** – instead of a single entity, there are various [alternative providers](https://gofoss.net/cloud-providers/) offering access to the Fediverse. Anyone can run a server, or a so-called instance, and provide services to a community. It's as if any user or organisation could run their own Twitter, Facebook, YouTube or Instagram server and invite friends to join.
* **Federated** – users from different services can communicate with each other, thanks to the [ActivityPub](https://activitypub.rocks/) protocol. It's as if Twitter, Facebook, Instagram or YouTube users could freely interact with each other.
* **Not evil** – the Fediverse isn't optimized to capture the user's attention, create user profiles and serve as much ads and paid content as possible. [Much to the contrary](https://quitsocialmedia.club/why) of Twitter, Facebook, YouTube, Instagram, Reddit and the likes.
* **About people** – There are no filters which decide what users will see in their timelines and what will be censored. No algorithms which force controversial content upon users to keep them "engaged". No dark patterns to influence users' behaviour. Again, this sets the Fediverse apart from Big Tech.
* **About agency** – There is no need to give away extended copyrights. No arbitrary and opaque terms of service or moderation policies. No digital walls between social networks. In opposition to commercial platforms run by tech giants.


<br>


<center> <img src="../../assets/img/separator_mastodon.svg" alt="Fediverse Mastodon" width="150px"></img> </center>

## Mastodon (Twitter alternative)

<img src="../../assets/img/fediverse_mastodon.jpg" alt="Mastodon instances" width="300px" align="left"></img> What is [Mastodon](https://joinmastodon.org/)? It's the Fediverse alternative to Twitter. It's a micro blogging service where users follow accounts, post 500 character messages (called *Toots*), retweet content (called *Boost*) and so forth. Created in 2016, [Mastodon is fully open source](https://github.com/mastodon/mastodon) and can be self-hosted. At the time of writing, Mastodon counted nearly 2 million users.

<br>

??? tip "Get started with Mastodon!"

    | Instructions | Description |
    | ------ | ------ |
    |Create an account |Each Mastodon user has an unique identifier, which works similar to an email address and looks like this: `@username@instance.social`. Create a new account with any instance of your choice, or even self-host an instance. Here's a selection of active instances: <br>• [Mastodon Instances](https://joinmastodon.org/communities) <br>• [Mastodon Server List](https://instances.social/) |
    |Sign in |Sign in to your instance via Mastodon's web application, or one of Mastodon's clients. You'll have access to your timelines, where posts from users in your instance or from other instances are displayed. |
    |Post a message |• Web interface: use the message box displayed in the left panel <br>• Client apps: click on the `Pencil` icon <br>• Write your Toot <br>• Optionally, add links, media files, mentions, emojis, polls or content warnings <br>• Define who can see your post |
    |Privacy settings |• `Public` messages are visible to everyone <br>• `Unlisted` messages are only visible to your followers, users mentioned in the post or users viewing your profile <br>• `Followers-only` messages are only visible to your followers or users mentioned in the post <br>• `Private` messages are only visible to users mentioned in the post <br>• Regardless of privacy settings, server administrators can access all messages, nothing is encrypted |
    |Content Warnings |Sensitive content can be hidden until users click on a link in your post. This might be useful if you share sensitive content with your audience. |
    |Hashtags |You can add hashtags `#` to your Toots, or click on words prefixed with a hashtag `#` to find similar content. |
    |Clients |[Pinafore](https://pinafore.social/) and [Halcyon](https://www.halcyon.social/) are "Twitter-like" web clients. In addition, various dedicated [mobile or desktop clients for Mastodon](https://joinmastodon.org/apps) are available. Also works on degoogled phones running [CalyxOS](https://gofoss.net/calyxos/) or [LineageOS](https://gofoss.net/lineageos/). |


<br>


<center> <img src="../../assets/img/separator_peertube.svg" alt="PeerTube" width="150px"></img> </center>

## PeerTube (YouTube alternative)

<img src="../../assets/img/fediverse_peertube.png" alt="Peertube" width="300px" align="left"></img> [Peertube](https://joinpeertube.org/en) is the Fediverse alternative to Youtube, Vimeo or Dailymotion. It's a media streaming platform where users view, like or upload videos. Created in 2015, [PeerTube is fully open source](https://github.com/Chocobozzz/PeerTube) and can be self-hosted. At the time of writing, PeerTube counted almost 200.000 users.

<br>

??? tip "Get started with PeerTube!"

    | Instructions | Description |
    | ------ | ------ |
    |Create an account |Create a new account with any instance of your choice, or even self-host an instance. Here's a selection of active instances: <br>• [PeerTube instances](https://instances.joinpeertube.org/instances) <br>• [FediDB](https://fedidb.org/network/?s=peertube) |
    |Sign in |Sign in to your instance via PeerTube's web application, or one of PeerTube's clients. You'll have access to your profile and plenty of videos. |
    |Watch videos |The interface is pretty intuitive: play & pause videos, adjust the volume, change the video quality & speed, enable subtitles, switch to full screen mode, download & share videos, and so on. |
    |Publish videos |<br>• Click on the `Publish` button <br>• Upload a video file, either from your device, via a URL or via a torrent link <br>• Videos from YouTube, Vimeo, Dailymotion and so on can be easily imported, simply provide the corresponding URL in the `Import with URL` tab <br>• PeerTube supports various formats: mp4, ogv, webm, flv, mov, avi, mkv, mp3, ogg & flac <br>• Add information to your upload: title, tags, descriptions, category, licence and so on <br>• Select a channel <br>• Adjust the privacy settings <br>• Hit the `Submit` button <br>• Click the `Share` button to let your friends and contacts know about the video <br>• Click `Share ‣ Embed` to copy a link & embed the video in your blog or website |
    |Channels |PeerTube supports so-called "channels", allowing to group videos per topic. Manage your channels under `My Library ‣ Channels`. |
    |Playlists |PeerTube also supports so-called "playlists", allowing to add videos to your collection. Playlists can be shared with others or remain private. Manage your playlists under `My Library ‣ Playlists`. |
    |Subscribe |Hit the `Subscribe` button to follow channels or playlists from other users. You can subscribe with your local account, other accounts (such as Mastodon) or via RSS. |
    |Search videos |Look for videos, channels or playlists by typing keywords in the search bar. Depending on your instances' settings, results might display videos from various instances, or from your own instance only. Alternatively, use a search engine like [Sepia Search](https://search.joinpeertube.org/). |
    |Privacy settings |• `Public` videos are visible to everyone <br>• `Internal` videos are only visible to people signed-in on your instance <br>• `Unlisted` videos are only visible to people with the private link <br>• `Private` videos are only visible to you |
    |Other settings |Refer to the `My account ‣ Settings` section to update your profile, change passwords, view storage space, specify how to display sensitive content, select language(s), manage notifications, delete your account and so on. |
    |Clients |Various dedicated [mobile or desktop clients for PeerTube](https://docs.joinpeertube.org/use-third-party-application) are available. |


<br>


<center> <img src="../../assets/img/separator_pixelfed.svg" alt="Pixelfed" width="150px"></img> </center>

## PixelFed (Instagram alternative)

<img src="../../assets/img/fediverse_pixelfed.png" alt="Pixelfed" width="200px" align="left"></img> [PixelFed](https://pixelfed.org/) is the Fediverse alternative to Instagram. It's a social network where users share, like and comment pictures and videos. Created in 2018, [PixelFed is fully open source](https://github.com/pixelfed/pixelfed) and can be self-hosted. At the time of writing, PixelFed counted over 50.000 users.

<br>

??? tip "Get started with PixelFed!"

    | Instructions | Description |
    | ------ | ------ |
    |Create an account |Create a new account with any instance of your choice, or even self-host an instance. Here's a selection of [active PixelFed instances](https://fedidb.org/software/pixelfed). |
    |Sign in |• Sign in to your instance via PixelFed's web application, or one of PixelFed's clients <br>• Click on `Menu ‣ Settings ‣ Account` to edit your profile photo, name, bio, password and so on |
    |Timelines |• Click on `Menu ‣ My Feed` to access posts from users you follow <br>• Click on `Menu ‣ Public Feed` to access posts from other users in your instance <br>• Click on `Menu ‣ Network Feed` to access posts from other instances |
    |Post your pictures & videos |• Click on the `New` button to upload one or multiple photos or videos <br>• PixelFed supports various formats: jpeg, gif, png, mp4 or webp <br>• Optionally, add captions, content warnings, tags, licenses or location <br>• Define who can see your pictures <br>• `Public` pictures & videos are visible to everyone <br>• `Unlisted` pictures & videos are only visible to followers & on your profile <br>• `Followers Only` pictures & videos are only visible to followers |
    |Discover people & content |Click on `Menu ‣ Discover` to explore trending topics and recommended content. |
    |Direct Messages |Click on the `Direct` button to send direct messages to other users. They can contain photos, videos, posts, text and so on. |
    |Hashtags |You can add hashtags `#` to your own posts' captions, and follow hashtags to stay connected with topics you care about. |
    |Stories |Click on `New ‣ New Story` to share moments during 24 hours. |
    |Collections |Click on `New ‣ New Collection` to group posts into a collection. |
    |Privacy settings |• Click on `Menu ‣ Settings ‣ Privacy` <br>• Check `Private Account` to limit access to your photos & videos to approved followers only <br>• Check `Opt-out of search engine indexing` to hide your photos & videos from search engines |
    |Two-factor authentication (2FA) |• Click on `Menu ‣ Settings ‣ Security` <br>• Provide your password <br>• Click on `Enable two-factor authentication` <br>• Scan the QR code with your mobile 2FA app, such as [andOTP](https://gofoss.net/passwords/#2fa-mobile-clients) <br>• Enter the 6 digit code into PixelFed's web form <br>• [Store the backup tokens](https://gofoss.net/passwords/#keepass) provided by PixelFed. They allow to recover access to PixelFed without your phone |
    |Clients |[PixelDroid](https://f-droid.org/en/packages/org.pixeldroid.app/) is a mobile client for Android. |


<br>


<center> <img src="../../assets/img/separator_lemmy.svg" alt="Lemmy" width="150px"></img> </center>

## Lemmy (Reddit alternative)

<img src="../../assets/img/fediverse_lemmy.webp" alt="Lemmy" width="300px" align="left"></img> [Lemmy](https://join-lemmy.org/) is the Fediverse alternative to Reddit. It's a social network where users join communities and post, like or comment on status updates and pictures. Created in 2019, [Lemmy is fully open source](https://github.com/LemmyNet/lemmy) and can be self-hosted. At the time of writing, Lemmy counted nearly 20.000 active users.

<br>

??? tip "Get started with Lemmy!"

    | Instructions | Description |
    | ------ | ------ |
    |Create an account |Create a new account with any instance of your choice, or even self-host an instance. Here's a selection of active instances: <br>• [Lemmy servers](https://join-lemmy.org/instances) <br>• [FediDB](https://fedidb.org/software/lemmy) |
    |Sign in |Sign in to your instance via Lemmy's web application or one of Lemmy's clients. |
    |Subscribe |• Click on `Menu ‣ Communities` <br>• Subscribe to any community you're interested in |
    |Post |• Click on `Menu ‣ Create Post` <br>• Add a URL, picture, title or text <br>• Select a community <br>• Optionally, provide a content warning <br>• Hit the `Create` button |
    |Other settings |Refer to the `Menu ‣ Username ‣ Settings` section to update your profile, change passwords, specify how to display sensitive content, delete your account and so on. |
    |Clients |Various dedicated [mobile or desktop clients for Lemmy](https://join-lemmy.org/apps) are available. |


<br>


<center> <img src="../../assets/img/separator_friendica.svg" alt="Friendica" width="150px"></img> </center>

## Friendica (Facebook alternative)

<img src="../../assets/img/fediverse_friendica.png" alt="Mastodon" width="300px" align="left"></img> [Friendica](https://friendi.ca/) is the Fediverse alternative to Facebook. It's a social network where users subscribe to forums to post, vote and comment on links and discussions. Created in 2010, [Friendica is fully open source](https://github.com/friendica/friendica) and can be self-hosted. At the time of writing, Friendica counted over 10.000 users.

<br><br>

??? tip "Get started with Friendica!"

    | Instructions | Description |
    | ------ | ------ |
    |Create an account |Create a new account with any instance of your choice, or even self-host an instance. Here's a selection of active instances: <br>• [Friendica Directory](https://dir.friendica.social/servers) <br>• [FediDB](https://fedidb.org/network/?s=friendica) |
    |Sign in |Sign in to your instance via Friendica's web application or one of Friendica's clients. You'll have access to your profile page, where status messages & posts are displayed. |
    |Post your status |• Click on the `Pencil & Paper` icon or the `Share` box <br>• Write your status update <br>• Optionally, add links, media files or a location <br>• Define who can see your post |
    |Network Tab |This is where you can see, comment or like posts from your friends or groups. |
    |Connect with people |Look for other people in [Friendica's User Directory](http://dir.friendica.social/). Friendica will also make suggestions on the `Friend Suggestions` page. |
    |Groups, Forums & Community Pages |If you don't know anyone on Friendica yet, connect to one of the various groups, forums or community pages. You'll be able to find and connect with people sharing common interests. |
    |Connectors |Depending on your instances' settings, you might be able to [interact with services outside of Friendica](https://forum.friendi.ca/help/Connectors), such as Twitter, Diaspora, GNU Social or RSS feeds. |
    |Clients |Various dedicated [mobile or desktop clients for Friendica](https://friendi.ca/resources/mobile-clients/) are available. |


<br>


<center> <img src="../../assets/img/separator_funkwhale.svg" alt="Soundcloud alternative" width="150px"></img> </center>

## Funkwhale (Spotify alternative)

<img src="../../assets/img/fediverse_funkwhale.png" alt="Funkwhale" width="300px" align="left"></img> [Funkwhale](https://funkwhale.audio/) is the Fediverse alternative to YouTube, Spotify or SoundCloud. It's a social network where users listen to, like or upload music, podcasts and radio broadcasts. Created in 2015, [Funkwhale is fully open source](https://dev.funkwhale.audio/funkwhale/funkwhale) and can be self-hosted. At the time of writing, Funkwhale counted almost 5.000 users.

<br>

??? tip "Get started with Funkwhale!"

    | Instructions | Description |
    | ------ | ------ |
    |Tagging |Prior to using Funkwhale, make sure your music library is tagged correctly. Good tagging makes it much easier for Funkwhale to display useful information such as album art or artist data. [Picard](https://picard.musicbrainz.org/) is a cross-platform music tagger which helps managing your music library. |
    |Create an account |Create a new account with any instance of your choice, or even self-host an instance. Here's a selection of active instances: <br>• [Funkwhale servers](https://network.funkwhale.audio/dashboards/d/overview/funkwhale-network-overview?orgId=1&refresh=2h) <br>• [FediDB](https://fedidb.org/network/?s=funkwhale) |
    |Upload content |• Sign in to your instance via Funkwhale's web application, or one of Funkwhale's clients <br>• Click on the `Add Content` icon <br>• Select a channel (to publish music) or a library (for public and/or private collections) |
    |Channels |Funkwhale supports so-called "channels", which allow to manage public music. Channels can be followed from different Funkwhale instances, as well as other Fediverse services (e.g. Mastodon). To follow a channel from another user, simply click on `Subscribe`. To manage your own channels, click on `Upload ‣ Publish your work in a channel ‣ Get Started`. |
    |Libraries |Funkwhale also supports so-called "libraries", which allow to manage public as well as personal music collections. Manage your libraries under `Upload ‣ Upload third-party content in a library ‣ Get Started`. Libraries come with different privacy levels: <br>• `Public` libraries are visible to anyone <br>• `Local` libraries are visible to other users from your instance <br>•  `Private` libraries are only visible to you <br>• Regardless of the privacy level, library links can be shared with specific users to grant access <br><br> **Caution**: note that you should always own appropriate copyrights, in particular if you use public or local libraries. |
    |Playlists |Funkwhale also supports so-called "playlists", which can contain your uploaded music as well as content from other users. Manage playlists under `Music ‣ Playlists`. |
    |Clients |Various dedicated [mobile or desktop clients for Funkwhale](https://funkwhale.audio/en_US/apps/) are available. |


<br>


<center> <img src="../../assets/img/separator_permissions.svg" alt="Support" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [Mastodon's documentation](https://docs.joinmastodon.org/) or [forum](https://discourse.joinmastodon.org/)
* [PeerTube's documentation](https://docs.joinpeertube.org/), [FAQ](https://joinpeertube.org/faq) or [forum](https://framacolibri.org/c/peertube/38)
* [PixelFed's documentation](https://docs.pixelfed.org/) or [help page](https://pixelfed.social/site/help)
* [Friendica's documentation](https://friendi.ca/resources/use-it/) or [support site](https://forum.friendi.ca/help/)
* [Lemmy's documentation](https://join-lemmy.org/docs/en/index.html)
* [Funkwhale's documentation](https://docs.funkwhale.audio/) or [forum](https://forum.funkwhale.audio/)

<center>
<img align="center" src="https://imgs.xkcd.com/comics/social_media.png" alt="Mastodon Fediverse"></img>
</center>

<br>
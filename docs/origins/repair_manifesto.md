---
template: main.html
title: Repair Manifesto
description: Platform21, 2009
---

# Repair Manifesto

!!! level "[Released](https://www.platform21.nl/download/4375) in 2009 by the design platform Platform21. Copyrighted, all rights reserved."

<center> <img src="../../../assets/img/origins_8.png" alt="Repair Manifesto" width="700px"></img> </center>

### 1. Make your products live longer!

Repairing means taking the opportunity to give your product a second life. Don’t ditch it, stitch it! Don’t end it, mend it! Repairing is not anti-consumption. It is anti- needlessly throwing things away.


### 2. Things should be designed so that they can be repaired.

Product designers: Make your products repairable. Share clear, understandable information about DIY repairs.
Consumers: Buy things you know can be repaired, or else find out why they don’t exist. Be critical and inquisitive.


### 3. Repair is not replacement.

Replacement is throwing away the broken bit. This is NOT the kind of repair that we’re talking about.


### 4. What doesn’t kill it makes it stronger.

Every time we repair something, we add to its potential, its history, its soul and its inherent beauty.


### 5. Repairing is a creative challenge.

Making repairs is good for the imagination. Using new techniques, tools and materials ushers in possibility rather than dead ends.


### 6. Repair survives fashion.

Repair is not about styling or trends. There are no due-dates for repairable items.


### 7. To repair is to discover.

As you fix objects, you’ll learn amazing things about how they actually work. Or don’t work.


### 8. Repair – even in good times!

If you think this manifesto has to do with the recession, forget it. This isn’t about money, it’s about a mentality.


### 9. Repaired things are unique.

Even fakes become originals when you repair them.


### 10. Repairing is about independence.

Don’t be a slave to technology – be its master. If it’s broken, fix it and make it better. And if you’re a master, empower others.


### 11. You can repair anything, even a plastic bag.

But we’d recommend getting a bag that will last longer, and then repairing it if necessary.


**Stop Recycling. Start Repairing.**
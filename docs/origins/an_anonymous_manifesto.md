---
template: main.html
title: An Anonymous Manifesto
description: Anonymous, 2011
---

# An Anonymous Manifesto

!!! level "[Released](https://web.archive.org/web/20130510063116/http://anonnews.org/press/item/199/) by Anonymous in 2011 under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)."

<center> <img src="../../../assets/img/origins_5.png" alt="An Anonymous Manifesto" width="700px"></img> </center>

Greetings from Anonymous. Recently there has been some confusion as to our identities and our motives. Some of us would like to try and clear a few things up.

## Who we are

Anonymous is everyone. Anonymous is no one. Anonymous exists only as an idea.
You also can be Anonymous.
Becoming Anonymous is simple. Just take action.

## Our aims

In order to promote an open, fair, transparent, accountable and just global society, the authors of this document propose the following:

1. A society  must be allowed to share information unrestricted and uncensored if it  is to maintain cultural and technological evolution and uphold the rights and liberties of its citizens. [1]

2. Citizens must be allowed to organize their own institutions without being harassed by existing institutions privileged by greater resources, influence and power, and it is the duty of the people to revoke these privileges, should they be abused.

3.  The privacy of citizens living in an open and transparent society must  be respected, and said citizens shall not be the target of any undue  surveillance. No citizen should be denied protection against any undue interference to his/her privacy.

4. Privacy and secrecy are privileges granted to the institutions built by citizens and their communities, as long as the institution does not utilize that secrecy to deceive, or act against the common interests of humanity.

5. It is the responsibility of all citizens  to take action and maintain an open and transparent society, and to  deal appropriately with institutions or citizens whose actions are a  direct transgression of these postulates.

[1] *The Universal Declaration of Human Rights, the obligations already undertaken by most nations of the world with a written constitution and all of those who form part of the UN Charter on Human rights. http://www.un.org/en/documents/udhr/index.shtml*

We are Anonymous.

We are Legion.

We do not Forget.

We do not Forgive.

Expect Us.
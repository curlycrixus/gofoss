---
template: main.html
title: Self-Repair Manifesto
description: iFixit, 2010
---

# Self-Repair Manifesto

!!! level "[Released](https://www.ifixit.com/Manifesto) in 2010 by the repair website iFixit under [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/)."

<center> <img src="../../../assets/img/origins_9.png" alt="Self-Repair Manifesto" width="700px"></img> </center>

### If you can’t fix it, you don’t own it.

### Repair is better than recycling.

Making our things last longer is both more efficient and more cost-effective than mining them for raw materials.

### Repair saves you money.

Fixing things is often free, and usually cheaper than replacing them. Doing the repair yourself saves you money.

### Repair teaches engineering.

The best way to find out how something works is to take it apart.

### Repair saves the planet.

Earth has limited resources. Eventually we will run out. The best way to be efficient is to reuse what we already have.

### Repair connects people and things.

### Repair is war on entropy.

### Repair is sustainable.

### We have the right:

* To devices that can be opened.
* To repair documentation for everything.
* To repair things in the privacy of our own homes.
* To error codes and wiring diagrams.
* To choose our own repair technician.
* To remove ‘do not remove’ stickers.
* To replace any and all consumables ourselves.
* To non-proprietary fasteners.
* To troubleshooting instructions and flowcharts.
* To available, reasonably-priced service parts.

### Because repair:

* Is independence.
* Saves money and resources.
* Requires creativity.
* Makes consumers into contributors.
* Inspires pride in ownership.
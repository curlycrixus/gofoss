---
template: main.html
title: Hackers: Heroes of the Computer Revolution
description: Steven Levy, 1984
---

# Hackers: Heroes of the Computer Revolution

!!! level "Book by Steven Levy, published in 1984 by Anchor Press/Doubleday. Extracts from [O'Reilly's](https://www.oreilly.com/library/view/hackers/9781449390259/pr02.html) 2010 edition. Copyright O’Reilly Media, Inc."

<center> <img src="../../../assets/img/origins_7.png" alt="Hackers" width="700px"></img> </center>

## Preface

I was first drawn to writing about hackers – those computer programmers and designers who regard computing as the most important thing in the world – because they were such fascinating people.

Though some in the field used the term “hacker” as a form of derision, implying that hackers were either nerdy social outcasts or “unprofessional” programmers who wrote dirty, “nonstandard” computer code, I found them quite different. Beneath their often unimposing exteriors, they were adventurers, visionaries, risk-takers, artists . . . and the ones who most clearly saw why the computer was a truly revolutionary tool. Among themselves, they knew how far one could go by immersion into the deep concentration of the hacking mind-set: one could go infinitely far. I came to understand why true hackers consider the term an appellation of honor rather than a pejorative.

As I talked to these digital explorers, ranging from those who tamed multimillion-dollar machines in the 1950s to contemporary young wizards who mastered computers in their suburban bedrooms, I found a common element, a common philosophy that seemed tied to the elegantly flowing logic of the computer itself. It was a philosophy of sharing, openness, decentralization, and getting your hands on machines at any cost to improve the machines and to improve the world. This *Hacker Ethic* is their gift to us: something with value even to those of us with no interest at all in computers.

It is an ethic seldom codified but embodied instead in the behavior of hackers themselves. I would like to introduce you to these people who not only saw, but *lived* the magic in the computer and worked to liberate the magic so it could benefit us all. These people include the true hackers of the MIT artificial intelligence lab in the fifties and sixties; the populist, less sequestered hardware hackers in California in the seventies; and the young game hackers who made their mark in the personal computer of the eighties.

This is in no way a formal history of the computer era, or of the particular arenas I focus upon. Indeed, many of the people you will meet here are not the most famous names (certainly not the most wealthy) in the annals of computing. Instead, these are the backroom geniuses who understood the machine at its most profound levels and presented us with a new kind of lifestyle and a new kind of hero.

Hackers like Richard Greenblatt, Bill Gosper, Lee Felsenstein, and John Harris are the spirit and soul of computing itself. I believe their story – their vision, their intimacy with the machine itself, their experiences inside their peculiar world, and their sometimes dramatic, sometimes absurd “interfaces” with the outside world – is the *real* story of the computer revolution.


## Chapter 2: The Hacker Ethic

1. Access to computers – and anything which might teach you something about the way the world works – should be unlimited and total. Always yield to the Hands-On Imperative !

2. All information should be free.

3. Mistrust Authority – Promote Decentralization.

4. Hackers should be judged by their hacking, not bogus criteria such as degree, age, race, or position.

5. You can create art and beauty on a computer.

6. Computers can change your life for the better.
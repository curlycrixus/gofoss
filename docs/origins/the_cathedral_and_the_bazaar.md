---
template: main.html
title: The Cathedral & The Bazaar
description: Eric S. Raymond, 1999
---

# The Cathedral and The Bazaar

!!! level "First edition of the book [released](http://www.catb.org/~esr/writings/cathedral-bazaar/) in 1999, copyright by Eric S. Raymond."

<center> <img src="../../../assets/img/origins_11.png" alt="The Cathedral and The Bazaar" width="700px"></img> </center>

We do not reproduce a static copy of *The Cathedral and The Bazaar* out of respect of the author's [copying policy](http://www.catb.org/~esr/copying.html). The original copy can be found on [Eric S. Raymond's website](http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/).
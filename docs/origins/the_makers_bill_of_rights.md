---
template: main.html
title: The Maker’s Bill of Rights
description: Philip Torrone, 2006
---

# The Maker’s Bill of Rights

!!! level "[Released](https://makezine.com/2006/12/01/the-makers-bill-of-rights/) in 2006 by Philip Torrone. Copyright Make Community LLC, all rights reserved."

<center> <img src="../../../assets/img/origins_19.png" alt="The Maker’s Bill of Rights" width="700px"></img> </center>

1. Meaningful and specific parts lists shall be included.

2. Cases shall be easy to open.

3. Batteries shall be replaceable.

4. Special tools are allowed only for darn good reasons.

5. Profiting by selling expensive special tools is wrong, and not making special tools available is even worse.

6. Torx is OK; tamperproof is rarely OK.

7. Components, not entirely subassemblies, shall be replaceable.

8. Consumables, like fuses and filters, shall be easy to access.

9. Circuit boards shall be commented.

10. Power from USB is good; power from proprietary power adapters is bad.

11. Standard connectors shall have pinouts defined.

12. If it snaps shut, it shall snap open.

13. Screws better than glues.

14. Docs and drivers shall have permalinks and shall reside for all perpetuity at archive.org.

15. Ease of repair shall be a design ideal, not an afterthought.

16. Metric or standard, not both.

17. Schematics shall be included.
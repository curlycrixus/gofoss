---
template: main.html
title: The Apache Way
description: Shane Curcuru, 2009
---

# The Apache Way

!!! level "[Released](https://theapacheway.com/) in 2009 by Shane Curcuru under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html) (Copyright Shane Curcuru)."

<center> <img src="../../../assets/img/origins_10.png" alt="The Apache Way" width="700px"></img> </center>

## Charity

The original Certificate of Incorporation ensures that the Apache Softwar Foundation (ASF) operates as a public charity. The most important action the ASF does is providing useful software products to the world, for free. Many parts of the The Apache Way are a happy coincidence of altruism and selfishness. Many contributors believe in our service to the public, and contribute from a belief that we can make the world a better place. They work side by side with contributors who are simply “scratching their own itch”: people who simply want to fix their own problems, but who find it’s often easier to do that in concert with others as it is to do alone.

The ASF is a public charity, and its purpose is to give away all of the software projects that it hosts. The point of this charity is for the public good - which includes everyone. Thus we use the pragmatic Apache License to ensure maximum freedom of all users who may choose to use our software.

The ASF will never charge for the use of its software. All Apache projects provide all code and basic mailing list support for free, without any requirements (other than basic netiquette when participating on our mailing lists).


<br>

<center> <img src="../../../assets/img/separator_anon.svg" alt="The Apache Way" width="150px"></img> </center>

## Community

"Community over Code" is a frequent saying that exemplifies Apache projects. A healthy Community uses Openness and Merit, expressed through Collaborative and Consensus driven work, to build lasting projects that use a Pragmatic License. While acting like a diverse community is a requirement for every Apache project, we also expect people to contribute as Individuals, and wear appropriate Hats.

The community behind a project are empowered with the technical decision-making for that project. Apache projects must follow a brief set of organizational rules, but otherwise technical and other decisions are completely up to the project and its PMC.

Some key aspects of Community are:

**Collaborative**

Apache projects are run collaboratively; that is, the whole of the community that has shown merit gets to decide technical direction.

**Consensus**

Communities working in The Apache Way strive to get consensus on major decisions. Having the community work together is one of the most important parts of the Way, as well as one of the most difficult to implement.

**Diversity**

Community diversity is often a key element to community health. The most important factor is a diversity of employers, for those committers who are doing some or all work on behalf of the employers. Projects that solely comprise active committers from a single project are frequently at risk of abandonment when their employer’s business cycle changes. Projects with active committers from multiple employers - or including students and hobbyists – can continue work and to draw in new contributors, even if one employer changes direction.

At the ASF, diversity of employers is a graduation requirement for Incubating projects. Incubator Podlings must show that they can attract committers from various employers to ensure that the project and technology is likely to be useful for a wider audience.

Diversity of learning and experiences within a community is often a bonus. Committers with different backgrounds will bring new ideas and perspectives to a project, which can make for stronger and more innovative technical decisions.

**Hats**

Everyone within a community comes with their own background, and may perform different roles within the community: user, writer, developer, PMC member. The concept of Hats relates to the roles you may play within a community at various times.

When you’re requesting a new feature, you’re wearing your user Hat. When checking in code to fix a bug, you’re wearing your Committer Hat. Votes on project releases are made while wearing your PMC member hat. While the difference is sometimes subtle, in a perfect world it’s an important lens to consider your actions through.

For example, as a user you might want your new feature shipped as soon as possible, because your organization needs to deploy it. When properly applying The Apache Way, however, as a PMC member you probably shouldn’t propose a release vote until the project as a whole is tested and stabilizes its API. Good community members often realize this distinction, and are happy to simply pull a changeset locally to your own organization to deploy just the new feature, while continuing to work with the community on getting to the right place for the next project release.

Hats are also another aspect of your Affiliations. While we hope committers keep their Committer Hat squarely on when checking in code, we realize that everyone has their own, external reasons for participating here – whether because of your dayjob employer, because of a consulting contract, or because you are seeking the fame and fortune of being an open source contributor. (Hint: fame and fortune not guaranteed.)

**Mailing Lists**

Work at Apache is done on mailing lists. Nearly all of these lists are publicly archived. An old adage is: “If it didn’t happen on-list, it didn’t happen.” Ensuring that all work and especially all decisions happen on-list ensures that the entire community is able to participate.


<br>

<center> <img src="../../../assets/img/separator_anon.svg" alt="The Apache Way" width="150px"></img> </center>

## Consensus

Communities working in The Apache Way strive to get consensus on major decisions. Having the community work together is one of the most important parts of the Way, as well as one of the most difficult to implement.

Consensus does not mean 100% agreement; however it does mean that all individuals with merit will accept the current progress on the project until some better proposal is put forth.


<br>

<center> <img src="../../../assets/img/separator_anon.svg" alt="The Apache Way" width="150px"></img> </center>

## Merit

Merit defines who is allowed to participate at various levels within a project community. Users help a project over time, and existing Committers Vote to invite the user to become a committer. Committers are allowed write access to the project’s source code, allowing them to improve the project. Strong communities rely on Openness in their work, usually on public Mailing Lists, to be able to recognize merit. Merit accrues to individuals regardless of affiliations, and typically does not expire.

Merit accrues to Individuals through their visible and productive work within a community. To put it another way, merit is the invisible currency that you gain by contributing useful work to the project. There are a number of common aspects to merit as it’s recognized within The Apache Way, the most important one is its recognition by the community. You don’t gain merit by simply doing a lot of work; you gain merit by doing things the community values.

**Merit Is About Your Work, Not You**

The concept of Merit within the Apache Way is often misunderstood, both on it’s merits and in particular with the specific meanings of the word within our communities. Within the Apache Way, “your merit” is always about the work you do, and never about you as an individual. Many newcomers see this at odds with common conceptions of someone’s merit meaning their character, their behaviors, their abilities, and past actions.

Since the word “merit” itself is overloaded with several meanings, here’s a more explicit phrase to try on instead. As a general concept, merit equals your Previously Accepted Contributions To A Project. That’s a mouthful, so let’s unpack it:

* **Previously** - you typically only get elected as a committer after you have made a number of contributions to a project. Since we work in distributed and asyncronous projects, merit is about past contributions.

* **Accepted** - making contributions isn’t enough. A project community needs to value the contribution enough to actually accept it to be worthwhile. It’s up to the project as a whole - independently and separately from other projects - to judge contributions and say that they’re valuable.

* **Contributions** - merit is typically based on actual work done, not plans for work, ideas, or statements of something in the future. Note that answering questions on user@ mailing lists and other ways to evangelize a project can help - once you’ve actually done them.

* **To A Project** - each project decides what’s valuable for that project. The merit needed to become a committer or PMC Member in different projects varies, and being a committer on one project does not translate to any other projects.


**More About Merit**

* Merit is measured by the community. In a general way, it’s the community’s perception of your value to the project. Different communities have different measurements of value, both in terms of what’s important to do, how it’s important to act, and in particular where the bar is to get recognized.

* Merit can and cannot be measured. The concept of merit – or useful work – within technical communities does allow for some measurements. The code you contribute compiles, or it does not; it meets the spec, or it fails the tests; it performs a function that the community values, or it does something silly. This technical aspect is a key difference to the term “merit” as it’s used in The Apache Way than it often is used in other social or political areas.

* Merit accrues to individuals. It doesn’t matter who you work for, or why you’re contributing. The merit accrues to you as a person, regardless of the Hats you may wear.

* Merit is not transitive. Your co-workers contributing to the same community do not share in your merit. Your company is not necessarily recognized for either the number of projects contributed to nor the number of developers assigned to work on projects.

* Merit is not transferable. The merit gained in one community is not necessarily valued in another community. Web Services experts should not expect to immediately gain a vote in the SpamAssassin community. Likewise, the bar for significant recognition varies significantly between different communities.

* Merit does not expire. It may get dusty if you stop participation for a while, but you still typically keep your recognition within the community if you come back later.

* Merit can be lost through harmful actions within the community, and in some cases through harmful or distrustful actions elsewhere.

* Merit is not just for code. Most communities value documentation, website, infrastructure, and mailing list help as well as code or bug report contributions.

The most obvious reward for your merit is being voted in to be a committer within a specific community. The merit level to become a committer varies from community to community; it can span anything from a handful of useful patches and helpful questions on a mailing list, to several months of sustained hard work on the code, mailing lists, or website.


**How ASF Projects Recognize Merit**

Within the ASF, most projects have fairly similar processes for actually recognizing merit. The specific processes are specific to the ASF, but in many cases are echoed in other open source projects as well.

Committers have write access to their project’s code repository. This means they may directly contribute to the project’s code, website, and other materials. Committers who continue to show merit may be recognized by voting them onto the Project Management Committee (PMC). PMC members have binding votes on the strategic direction of the project, and on making formal releases. One PMC member is typically voted in by the community to become the chair of the PMC, and with that comes the role of Vice President of that particular project.

Note that within the ASF, voting in committers is done solely by each project’s PMC individually; they act independently in this respect. When voting individuals onto the PMC itself, the Board of Directors of the ASF exercises oversight, and approves all PMC changes by a simple ‘ACK’ email. The Board also specifically passes resolutions at monthly meetings to appoint, remove, or change Vice Presidents of Apache projects.


<br>

<center> <img src="../../../assets/img/separator_anon.svg" alt="The Apache Way" width="150px"></img> </center>

## Open

Strong communities can best function when work is done openly. All ASF projects use Mailing Lists as their primary means of working and communicating. Code is always publicly available, and Votes about all aspects of project direction are performed publicly, over a specific period of time – allowing contributors worldwide time to participate. Individuals participating in healthy communities make their affiliations public, and ensure that they are wearing an ASF contributor’s hat when they are participating in the community. Releases of software products are voted upon publicly, and our code and decisions are always available for review. Openness allows new users the opportunity to learn. Every non-public mailing list at the ASF must have a specific reason to be kept private.

The concept of doing as much work as possible in public is fundamental to The Apache Way. For a community to best work and grow, its technical work must happen in the open.

* Open communication promotes community growth. Having existing and past technical discussions and decisions available in the mailing list archives makes it easier for new users to understand the project and the choices the community made to get where it is today.

* Open communication ensures community health. Having every email you write to the project lists be published may be unnerving to some at first, but it’s crucial to ensure that the whole community can contribute to the work. This also ensures that community members in different regions, or who work on different schedules, can still participate.

* Face to face meetings must have results published and voted on. While in-person meetings or telephone conferences are sometimes held to increase collaboration, the results of any meetings must be published to the project mail lists. Any decisions made off-list must be ratified by a vote or lazy consensus on the public lists to be valid.

* Open work ensures everyone can participate. Having all of the technical discussions, consensus building, and code and documentation work happen in the open ensures that everyone can participate and everyone – contributors and just lurkers alike – can learn from the process and understand where the project is going.

* Open work ensures our projects remain neutral. A healthy community has some diversity, and ensuring that all work on the project happens in the open means that no one set of contributors or no corporation or other organization can command the project or use it solely for their own ends. Everyone can see what the project is doing and who is advocating what within the project.

* Open communication allows for oversight. Within the ASF, there are a few core values that we expect all our projects to follow. Having the project’s work happen in the open ensures that the board and the PMC can review what’s happening to ensure the core values are accepted and followed.

* Open status ensures that everyone can know what’s going on. New users don’t need to know who to ask to learn about the project: everything is on the mailing list, the archives, the website.

* Open lists promote open reputations. Merit within a community is gained through open work within that community. While each community has its own standards for levels of merit (i.e. at what point an individual may be voted in as a committer), open lists mean that all of a person’s work on other communities is also visible.


<br>

<center> <img src="../../../assets/img/separator_anon.svg" alt="The Apache Way" width="150px"></img> </center>

## Pragmatic

The mission of the ASF is to provide software for the public good. We use the Apache License to do this, which ensures that users of our software have the freedom to use our software however they see fit – even in proprietary products. Our steadfast belief in freedom for our users both serves our public purpose, as well as ensures that users from all areas are able to - and often desire to - contribute back to our communities. Our pragmatic license allows contributors from individuals to corporate teams to join together, secure in the knowledge that their product is freely usable by all.

Pragmatism is about doing what makes sense for the community and the all of public, without other agendas. As a public charity, adopting a pragmatic attitude about licensing and community is a key part of our success. Our pragmatism is primarily defined through our license, which we require all of our projects to use on the software products they release. Pragmatism is also shown through how we prefer diverse, consensus based communities for our development work.

The Apache License welcomes everyone to use our software however they please – including in closed or otherwise proprietary ways. We’re happy to have users take Apache software and use it privately, with or without ever sharing their changes with the world. We’re happy to have corporations build proprietary projects on top of our software and sell the resulting product for a profit. As long as users comply with the very basic terms of our license, we want them to have the freedom to do as they will with our software.

This pragmatic background to our license and our communities is most starkly contrasted to the Free Software Foundation’s Gnu Public License (GPL). Much of the intent of GPL licenses – often called “CopyLeft” licenses to distinguish them from copyright concepts – is to ensure that the software itself and its derivatives will forever remain free. The GPL essentially requires that future changes to any GPL software released publicly are given back to the community, using the GPL on the derived work. In some ways, the GPL provides for the software itself to be free, even at the expense of developers who might want to release derived works under another (possibly proprietary) license. The Apache License, in contrast, wants to give maximum freedom to the users of our software.

All ASF projects are required to use the Apache License. The rights we ensure that users of our software have is one of the fundamental aspects of the ASF.
---
template: main.html
title: The Declaration of the Independence of the People of the Internet
description: Anonymous, 2012
---

# The Declaration of the Independence of the People of the Internet

!!! level "[Released](https://files.gendo.ch/media/Pirate_declaration_of_Independence_2_printable.pdf) in 2012 by Anonymous (no license)."

<center> <img src="../../../assets/img/origins_15.png" alt="The Declaration of the Independence of the people of the Internet" width="700px"></img> </center>

When in the Course of human events it becomes necessary for people to dissolve the commercial, legal and moral bands which have connected them with an industry and to assume among the powers of the earth, the separate and equal station to which their most fundamental principles entitle them, a decent respect to the opinions of mankind requires that they should declare the causes which impel them to the separation.

We hold these truths to be self-evident, that all lives are enriched by the sharing of culture, that citizens are endowed by their democracies with certain unalienable rights, that among these are knowledge, true ownership of their property and the sharing of culture. That to secure these rights, laws are instituted among the people, deriving their just powers from the consent of the governed. That whenever any of these laws become destructive, of these ends, it is the right of the people to alter or to abolish them, and to institute new laws, laying their foundations on such principles and organizing their powers in such form, as to them shall seem most likely to effect their safety and happiness.

Prudence, indeed, will dictate that laws long established should not be changed for light and transient causes; and accordingly all experience hath shewn that mankind are disposed to suffer, while evils are sufferable, than to right themselves by abolishing the forms to which they are accustomed. But when a long train of abuses and usurpations, pursuing invariably the same object, evinces a design to reduce them under absolute despotism, it is their right, it is their duty, to throw off such laws, and to provide new guards for their future cultural wealth.

Such has been the patient sufferance of the people of the Internet; and such is now the necessity which constrains them to alter their former systems of cultural distribution. The history of the present copyright industry is a history of repeated injuries and usurpations, all having in direct object the establishment of an absolute tyranny over the culture of the people of Earth. These are just some of the effects of the lobbying of the copyright-industry:

* The destruction of our cultural heritage by forced obliteration and decay, by forbidding or hindering the reduplication and sometimes even the restoration of cultural artifacts.

* The destruction of our future, by frustrating education and the sharing of knowledge, thereby condemning many to lower life standards than they could otherwise achieve, especially in developing countries.

* The destruction of the creative process, by legally forcing artists and authors to steer clear of any sources of inspiration, and punishing them for accidental similarities and citations.

* The destruction of free access to key, contentious pieces of political information by preventing maximum distribution of this information.

* The destruction of human and natural resources, by forcing the re-creation of works that would be perfectly usable with some minor rework, but not allowing such re-use.

* The destruction of social and economic order, by allowing the control of much of our heritage to end up in just a few hands. Leading to a society where a few have a lot, and a lot have little.

* The destruction of innocent lives by transporting citizens of other nations beyond Seas to be tried for offenses that are not even offenses in their home nations…

In every stage of these oppressions we have petitioned for redress in the most humble terms: Our repeated petitions have been answered only by repeated injury. Corporations, whose character is thus marked by every act which may define tyrants, are unfit to be conduit of culture for a free people.

Nor have we been wanting in attentions to our corporate cultural overlords. We have warned them from time to time of attempts by their lobbying to extend an unwarrantable jurisdiction over us. We have reminded them of the limits of our patience and the growing existence of alternatives to their wares. The most recent efforts of the copyright industry to circumvent our most fundamental democratic institutions leaves us no choice but to defend our culture by taking it out of the hands of these corporations.

We, therefore, the Pirates of the World, do, in the name, and by authority of the good people of the Internet, solemnly publish and declare, that we are free and united, and no longer recognize the legal or moral validity of the copyrights claims of aforementioned corporations, that we are absolved from all legal and moral allegiance to these corporations, and that all connections between the people of the Internet and the copyright industry is and ought to be totally dissolved; and that as free and Independent people, we have full power to download, distribute, remix, broadcast, perform and to do all other acts and things which Independent people may of right do. And for the support of this declaration, which a firm reliance on strong cryptological protection, we mutually pledge to each other our lives, our fortunes, and our sacred bandwidth.
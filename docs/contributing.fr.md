---
template: main.html
title: Participez au projet
description: Contribuez à gofoss.net. Rejoignez la communauté. Faites des suggestions. Améliorez le contenu. Aidez pour les traductions. Signalez toute erreur sur GitLab.
---

# Participez au projet !

<div align="center">
<img src="../../assets/img/contributing.png" alt="Contribuez à gofoss.net" width="375px"></img>
</div>

Bienvenue ! [gofoss.net](https://gofoss.net/fr/) est un projet libre et open source, tout le monde peut donc étudier le code du site et y apporter des améliorations. Rejoignez-nous et découvrez comment vous impliquer au mieux !

* Améliorez le contenu, ajoutez des fonctionnalités, corrigez des erreur
* Enrichissez l'expérience utilisateur, concevez de nouvelles illustrations
* Aidez avec les traductions
* Animez les discussions
* Maintenez le code


<div style="margin-top:-40px">
</div>


## Guide pratique de contribution

??? tip "Contactez-nous"

     Pas besoin d'ouvrir un ticket sur GitLab si vous souhaitez simplement discuter ou avez une question. Il vous suffit de nous contacter sur les médias sociaux :

     &nbsp;&nbsp; :material-twitter: [Suivez @gofoss_today sur Twitter](https://nitter.net/gofoss_today)

     &nbsp;&nbsp; :material-mastodon: [Suivez @don_atoms sur Mastodon](https://hostux.social/@don_atoms/)

     &nbsp;&nbsp; :material-reddit: [Écrivez à @gofosstoday sur Reddit](https://teddit.net/u/gofosstoday/)

     &nbsp;&nbsp; :fontawesome-solid-users: [Écrivez à @gofoss sur Lemmy](https://lemmy.ml/u/gofoss)

     &nbsp;&nbsp; :material-matrix: [Rejoignez notre tchat sur Matrix](https://matrix.to/#/#gofoss-community:matrix.org/)

     &nbsp;&nbsp; :material-email: [Envoyez-nous un courriel](mailto:gofoss@protonmail.com)


??? tip "Ouvrez un ticket sur GitLab"

	<center>

	| Instructions | Description |
	| ------ | ------ |
	| Faites quelques recherches |• Recherchez des contributions similaires avant de soumettre la vôtre <br>• Il y a de fortes chances que quelqu'un d'autre ait eu le même problème ou la même proposition que vous <br>• [Consultez](#contactez-nous) la communauté gofoss.net <br>• [Recherchez dans l'outil de suivi de tickets](https://gitlab.com/curlycrixus/gofoss/-/issues) <br>• [Consultez la feuille de route](https://gofoss.net/fr/roadmap/) |
	| Ouvrez un ticket |• Si rien ne ressort de vos recherches, ouvrez un ticket <br>• [Connectez-vous à GitLab](https://gitlab.com/users/sign_in) <br>• Allez dans l'outil de [suivi de tickets](https://gitlab.com/curlycrixus/gofoss/-/issues) de gofoss.net <br>• Cliquez sur le bouton `New Issue` <br>• Saisissez un titre <br>• Veillez à sélectionner le bon modèle : <br><br>`content_request`: suggestions concernant du contenu nouveau ou amélioré, par exemple un nouveau logiciel, des informations manquantes ou incorrectes, des fautes d'orthographe ou de grammaire, une amélioration de la lisibilité, etc. <br><br>`feature_request`: suggestions concernant de nouvelles fonctionnalités du site web, par exemple une meilleure navigation, une mise en page améliorée, de nouvelles illustrations, un mode sombre, etc. <br><br>`bug_report`: comportement inattendu du site web, par exemple des liens défectueux, des problèmes de chargement sur certains appareils, etc. |
    | Participez au processus de révision |• Tous les tickets sont discutés et examinés publiquement <br>• Assurez-vous de suivre les discussions relatives à votre ticket <br>• Répondez aux questions éventuelles et examinez les contributions des autres <br>• C'est aussi précieux que de contribuer soi-même <br>• [Configurer les notifications par courriel](https://docs.gitlab.com/ee/user/profile/notifications.html) pour rester informé |
	| Patientez jusqu'à ce que le ticket soit résolu ou clos |• Nous faisons de notre mieux pour résoudre les tickets rapidement <br>• En retour, nous vous demandons de rester disponible pour des questions de clarification ou des corrections <br>• Veuillez noter que si nous n'avons pas de nouvelles de votre part, nous pourrions être amenés à fermer votre ticket |

	</center>


??? tip "Travaillez sur des tickets ouverts"

    ### Flux de travail & branches

    <div align="center">
    <img src="../../assets/img/issue_branching.png" alt="Flux de travail & branches" width="700px"></img>
    </div>

	<center>

	| Branches | Description |
	| ------ | ------ |
    |`main` |• La branche `main` est la branche de production <br>• Tout changement (en anglais, « commit ») sur la branche `main` est automatiquement déployé de GitLab vers Netlify <br>• La branche `main` est donc protégée <br>• Elle n'est accessible qu'aux responsables de l'équipe gofoss.net (en anglais, « `maintainers` ») |
    |`dev` |• La branche `dev` est créée à partir de la branche `main` <br>• C'est là que de nouveaux contenus, de nouvelles fonctionnalités et des correctifs sont développés et testés <br>• La branche `dev` n'est accessible qu'aux `maintainers` de l'équipe gofoss.net <br>• La branche `dev` est intégrée dans `main` lorsqu'elle est prête à être déployée en production |
    |`issue` |• Chaque ticket (en anglais, « `issue` ») doit se voir attribuer sa propre branche <br>• Les branches `issue` sont créées à partir de la branche `dev` <br>• Lorsque le travail sur un ticket (une `issue`) est terminé, le résultat est intégré dans `dev` <br>• Les branches `issue' n'interagissent donc jamais directement avec `main' <br>• Les responsables de l'équipe gofoss.net (`maintainers`) et les contributeurs externes peuvent travailler sur les branches `issue` |

	</center>


    ### Bifurcation de branches

    <div align="center">
    <img src="../../assets/img/forking_workflow.png" alt="Bifurcation de branches" width="700px"></img>
    </div>

	<center>

	| Instructions | Description |
	| ------ | ------ |
	| Choisissez un ticket sur lequel travailler |• Recherchez des tickets marqués comme `accepting_merge_requests` [dans l'outil de suivi](https://gitlab.com/curlycrixus/gofoss/-/issues) <br> • Ces tickets sont ouverts aux contributions externes <br> • Avant de commencer à travailler sur un ticket, prenez contact avec l'équipe gofoss.net <br> • Laissez un commentaire dans le ticket sur lequel vous voulez travailler <br> • Ou envoyez un message via [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today) ou [courriel](mailto:gofoss@protonmail.com) <br> • Il existe également un [canal Matrix](https://matrix.to/#/#gofoss-dev:matrix.org/) où les développeurs de gofoss.net se retrouvent (sur invitation uniquement) <br> • Attendez que quelqu'un de l'équipe gofoss.net valide votre demande de contribution |
    | Bifurquez, clonez et synchronisez le dépôt gofoss.net |• [Connectez-vous à GitLab](https://gitlab.com/users/sign_in) <br>• Allez sur le [projet gofoss.net](https://gitlab.com/curlycrixus/gofoss) <br> • Cliquez sur le bouton `Fork` <br>• Cela crée une copie du projet gofoss.net dans votre propre dépôt GitLab <br> • Ouvrez un terminal sur votre ordinateur et créez un dossier de projet : <br>`mkdir ~/git_projects` <br>• Clonez votre dépôt GitLab : <br>`cd ~/git_projects` <br>`git clone https://gitlab.com/<YOUR_USERNAME>/gofoss.git` <br>• Allez dans la copie locale de votre dépôt : <br>`cd ~/git_projects/gofoss` <br> • Créez un lien avec le projet gofoss.net en amont pour garder votre copie synchronisée : <br>`git remote add upstream https://gitlab.com/curlycrixus/gofoss.git` <br>• Synchronisez régulièrement avec la branche `dev` du projet amont : <br>`cd ~/git_projects/gofoss` <br>`git fetch upstream` <br>`git checkout dev` <br>`git pull upstream dev` <br>`git push origin dev` <br>• Autrement, si votre copie est trop obsolète, votre demande de fusion pourrait être rejetée |
    | Configurez l'environnement de développement |• Définissez votre compte par défaut : <br>`git config user.email "<USER>@example.com"` <br>`git config user.name "<USERNAME>"` <br>• Installez la dernière version de Python et ses dépendances : <br>`sudo apt install python3 python3-pip` <br>• Vérifiez que la version 3 de Python est installée : <br>`python3 --version` <br>• Installez Mkdocs : <br>`sudo apt install mkdocs` <br>• Installez Material pour Mkdocs : <br>`pip install mkdocs-material` <br>• Installez l'[extension multi-langue](https://ultrabug.github.io/mkdocs-static-i18n/fr/): <br>`pip install mkdocs-static-i18n` <br>• Installez les dépendances : <br>`pip install -r requirements.txt` |
	| Écrivez dans le dépôt local & poussez les changements vers le dépôt distant |• Créez une nouvelle branche `issue`, en respectant la convention de dénomination suivante : `[content/feature/bug]-[#issueID]-[courte description]`<br>• Par exemple : <br>`git checkout -b feature-#72-add-dark-mode` <br>• Passez à la nouvelle branche : <br>`git checkout feature-#72-add-dark-mode` <br>• Commencez à coder : ajoutez du contenu et des fonctionnalités ou corrigez des erreurs <br>• Visualisez les modifications locales dans votre navigateur sur `localhost:8000` avec la commande : <br> `mkdocs serve` <br>• Si tout fonctionne comme prévu, ajoutez le code à la zone de préparation : <br>`git add .` <br>• Validez les changements et assurez-vous d'ajouter un commentaire clair : <br>`git commit -m "<CLEAR COMMENT>"` <br>• Enfin, poussez la branche `issue` vers votre dépôt distant : <br>`git push -u origin feature-#72-add-dark-mode` |
    | Ouvrez une demande de fusion (en anglais, « Merge Request ») |• [Connectez-vous à GitLab](https://gitlab.com/users/sign_in) <br>• Allez sur `https://gitlab.com/<YOUR_USERNAME>/gofoss/-/merge_requests` <br>• Cliquez sur le bouton `New merge request` <br>• Sélectionnez la branche source : `<USERNAME>/gofoss/feature-#72-add-dark-mode` <br>• Sélectionnez la branche cible : `curlycrixus/gofoss/dev` <br>• Cliquez sur le bouton `Compare branches and continue` <br>• Veillez à sélectionner le modèle `merge_request` <br>• Saisissez un titre et une description clairs pour votre demande de fusion <br>• Sélectionnez l'option `Delete source branch when merge request is accepted` <br>• Cliquez sur le bouton `Create merge request` |
    | Participez au processus de révision |• Toutes les demandes de fusion sont discutées et examinées publiquement <br>• Assurez-vous de suivre les discussions relatives à votre demande de fusion <br>• Répondez à d'éventuelles questions <br>• [Configurez les notifications par courriel](https://docs.gitlab.com/ee/user/profile/notifications.html) pour rester informé |
	| Attendez la validation |• Un `maintainer` du projet gofoss.net va copier votre branche `issue` dans son dépôt local <br>• Ils la vérifieront et s'assureront qu'il n'y a pas de souci particulier <br>• Si le `maintainer` a des commentaires, vous devrez mettre à jour votre demande de fusion <br>• Si tout va bien, le `maintainer` fusionnera vos changements dans sa branche locale `dev` et poussera le tout vers le dépôt gofoss.net <br>• Votre contribution fait désormais partie du projet gofoss.net ! <br>• D'autres développeurs devront se synchroniser avec le dépôt gofoss.net |
	| Supprimez la branche |• Après la prise en compte de votre demande de fusion, la branche `issue` `feature-#72-add-dark-mode` devrait avoir été automatiquement supprimée <br>• Si ce n'est pas le cas, supprimez-la manuellement <br>• Vous pouvez désormais récupérer les modifications du dépôt amont en toute sécurité |
    | Licence | En contribuant à gofoss.net, vous acceptez que vos contributions soient publiées sous [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt). |

	</center>


    ### Structure des répertoires

	Veuillez noter que la structure globale des répertoires doit rester inchangée :

	```bash
	.
	├─ .gitlab/             # modèles pour les tickets & demandes de fusion
	│
	├─ docs/
	│  │
	│  ├─ assets/
	│  │  └─ css/			# feuilles de style (.css), polices locales (.ttf) & icônes (.svg)
	│  │  └─ echarts/		# fichiers echart (.html, .min.js)
	│  │  └─ img/			# fichiers image (.svg, .png, etc.)
	│  │
	│  └─ *.xyz.md			# fichiers markdown multilingues (.en.md, .fr.md, .de.md, etc.)
	│
	├─ overrides/			# extensions du thème (home_xyz.html, main.html, overrides.xyz.min.css)
	│
	├─ LICENCE			    # licence mkdocs-material
	│
	├─ netlify.toml		    # instructions pour le déploiement sur netlify
	│
	├─ requirements.toml	# dépendances
	│
	├─ runtime.txt			# version python pour le déploiement sur netlify
	│
	└─ mkdocs.yml			# fichier de configuration mkdocs
	```


??? tip "Aidez-nous avec les traductions"

	<center>

	| Instructions | Description |
	| ------ | ------ |
    | Inscrivez-vous à une liste de diffusion |Si vous avez des connaissances dans une ou plusieurs des langues cibles et une connaissance de base de la syntaxe « markdown », rejoignez la liste de diffusion appropriée : <br>• [Équipe de traduction EN ‣ FR (Framalang)](https://framalistes.org/sympa/info/framalang/) <br>• [Équipe de traduction EN ‣ DE](https://framalistes.org/sympa/info/gofosslang_de/) <br>• [Équipe de traduction EN ‣ ES](https://framalistes.org/sympa/info/gofosslang_es/) <br>• [Équipe de traduction EN ‣ PL](https://framalistes.org/sympa/info/gofosslang_pl/) |
    | Discutez avec d'autres traducteurs | Il existe également un [tchat matrix](https://matrix.to/#/#gofoss-translation:matrix.org/) où les traducteurs et traductrices peuvent se rencontrer, se coordonner et se soutenir. |
    | Accéder aux fichiers de traduction |Les traductrices et traducteurs reçoivent un lien pour accéder aux fichiers du projet. |
    | Travaillez avec Framapad |• Les traductions sont gérées à l'aide de l'éditeur de texte collaboratif en ligne Framapad <br>• Les contributions individuelles sont identifiées par un code couleur <br>• Toutes les modifications apparaissent à l'écran en temps réel |
    | Utilisez le glossaire |Utilisez le GLOSSAIRE lorsque vous travaillez sur une traduction. |
    | Conservez le texte original |Insérez la traduction juste après le texte original, tout en conservant le texte original tel quel. |
    | Préservez le code |Assurez-vous que tous les éléments de mise en forme « markdown » et les balises HTML sont préservés. |
    | Respectez les autres contributeurs |• Respectez le travail des autres <br>• Si vous n'êtes pas d'accord avec une traduction, vous pouvez faire vos propres suggestions <br>• Par contre, sans modifier ou supprimer les traductions existantes |
    | N'abusez pas de vos pouvoirs |N'abusez pas de vos pouvoirs. Le travail de traduction est basé sur la confiance. |
    | Validation |Les traductions seront publiées sur le site une fois qu'elles sont complètes et qu'elles ont été revues par l'équipe projet |

	</center>


??? tip "Suivez le code de conduite"

	<center>

	| Instructions | Description |
	| ------ | ------ |
    |Règle #1 |Acceptez de collaborer avec des personnes du monde entier, quels que soient leur sexe, leur âge, leur nationalité, leur origine ethnique, leur apparence, leur éducation, leur milieu social ou leur religion. |
    |Règle #2 |Veillez à ce que la discussion reste amicale, respectueuse, constructive et pertinente. Les impolitesses ne seront pas tolérées. Pas de conflits internes, pas de trollage, pas d'insultes, pas de langage à connotation sexuelle, pas de harcèlement public ou privé, pas de « doxing », pas d'attaques personnelles, racistes, politiques ou religieuses. |
    |Règle #3 |Respectez les choix de l'équipe projet. Nous nous efforçons de lire, d'examiner et de répondre aux questions, tickets et demandes de fusion du mieux possible – mais n'oubliez pas que notre temps est limité. Nous nous réjouissons également de toute suggestion, recommandation et contribution – ce qui n'implique pas que chaque décision doive faire l'objet d'un consensus de groupe. |
    |Règle #4 |Évitez les discussions privées concernant le projet, s'il n'y a pas de raison valable. Les discussions doivent rester publiques, sur GitLab ou via les canaux de communication officiels du projet. |
    |Règle #5 |Soumettez toute réclamation à [gofoss@protonmail.com](mailto:gofoss@protonmail.com). Les infractions aux règles 1 à 4 peuvent entraîner des mesures correctives. Cela inclut des rappels amicaux, des avertissements ou la suppression, la modification ou le rejet de toute contribution inappropriée : message, commentaire, soumission de code, ticket, demande de fusion, etc. En dernier ressort, un bannissement du projet peut être prononcé. |

	</center>


## Vous souhaitez faire un don ?

Merci de votre intérêt !

La plupart des sites web « gratuits » vendent des publicités, les données de leurs utilisateurs ou du contenu payant. gofoss.net en revanche est réellement *libre* : libre d'utilisation, libre de publicité, libre de tout pistage, libre de tout contenu sponsorisé ou payant, libre de tout lien affilié et avec un code source entièrement libre.

gofoss.net est un projet à but non lucratif géré par des bénévoles. Tous les coûts de fonctionnement, le développement et le contenu sont financés uniquement par des dons d'utilisateurs et d'utilisatrices. Si vous souhaitez faire un don, ou nous offrir un café, cliquez sur le bouton ci-dessous :)


<center>

<a href="https://liberapay.com/gofoss/donate"><img alt="Faire un don avec Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

</center>

Envisagez également de soutenir d'autres [développeurs de logiciels libres](https://gofoss.net/fr/thanks/). Ou d'aider des groupes activistes et des fondations logicielles qui promeuvent de meilleures lois sur la protection de la vie privée, les droits des utilisateurs et la liberté d'innover :

* [Framasoft](https://framasoft.org/)
* l'[April](https://april.org)
* la [Quadrature du Net](https://www.laquadrature.net/)
* la [Free Software Foundation Europe](https://fsfe.org/index.fr.html)

<div align="center">
<img src="https://imgs.xkcd.com/comics/fixing_problems.png" alt="Contribuer au projet gofoss.net"></img>
</div>

<br>

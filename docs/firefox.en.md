---
template: main.html
title: Hardened Firefox | Best Firefox Addons & Browser Security Settings
description: Wondering what is the most secure web browser in 2022? Enjoy this simple guide to hardening Firefox against digital fingerprinting and other security threats.
---

# Hardened Firefox <br> (Including The Best Firefox Addons in 2022)

!!! level "Last updated: May 2022. For beginners & intermediate users. Some tech skills may be required."


<center><img align="center" src="../../assets/img/firefox_logo.png" alt="Firefox hardening" width ="150px"></img></center>


## How to install Firefox

Looking for a secure web browser? [Firefox](https://www.mozilla.org/en-US/firefox/new/) is the preferred browser when it comes to privacy, security and convenience. Firefox was first released back in 2004 by the Mozilla community. The browser is free and open source, although the Android version includes proprietary libraries, like Google Analytics. Firefox blocks cross-site tracking cookies by default, can be further hardened with customisable browser security settings or addons, and runs smoothly on virtually any device. Detailed installation instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        Download and run the [Firefox installer](https://www.mozilla.org/en-US/firefox/windows/).


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        Download the [Firefox disk image](https://www.mozilla.org/en-US/firefox/mac/), open it and drag the Firefox icon on top of the Application folder. For easy access, open the Applications folder and drag the Firefox icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        If you run a Linux distribution such as [Ubuntu](https://gofoss.net/ubuntu/), open the terminal with the `Ctrl+Alt+T` shortcut or click on the `Applications` button on the top left and search for `Terminal`. Then run the following command:

        ```bash
        sudo apt install firefox
        ```

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        Installing Firefox on Android is simple. Download Firefox from the [App Store](https://play.google.com/store/apps/details?id=org.mozilla.firefox&hl=en&gl=US), or visit the [Firefox download page](https://www.mozilla.org/en-US/firefox/browsers/mobile/) from your Android device. There's also a way to download and update Firefox without using a Google account: [Aurora Store](https://auroraoss.com/)! We'll explain how to use alternative app stores in [a later chapter](https://gofoss.net/foss-apps/).


=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        Download Firefox from the [App Store](https://apps.apple.com/us/app/firefox-private-safe-browser/id989804926/).

<div style="margin-top:-20px">
</div>

??? warning "Mozilla, the (not-so) white knight at the merci of Google"

    In a browser landscape dominated by Google, Firefox currently seems to be the only independent contender worth mentioning. That's why our browser recommendation is Firefox, a free and open source alternative to Chrome-based browsers. But we also share some of the criticism regarding Mozilla. This includes questionable technical choices, such as the integration of the partly [closed-source Pocket](https://github.com/Pocket/extension-save-to-pocket/issues/75) or turning on [sponsored content](https://support.mozilla.org/en-US/kb/firefox-suggest) and [telemetry](https://support.mozilla.org/en-US/kb/telemetry-clientid) by default. Gladly, those settings can be adjusted, as explained on this page.

    There are also broader concerns regarding long-term funding. The Mozilla Corporation is the company behind the development, distribution and promotion of Firefox, Thunderbird as well as other applications. Further, the *Mozilla Corporation* is a commercial entity under the control of a non-profit organisation called the *Mozilla Foundation*. As it happens, the Mozilla Corporation earns most of its revenues through a deal with... Google, its main rival! Dating back to 2006, this odd partnership has been extended until 2023, and secures [450 million dollars](https://www.zdnet.com/article/sources-mozilla-extends-its-google-search-deal/) in yearly revenues by assigning Google as Firefox's default search engine. While its legitimate for Mozilla to seek value streams which sustain development, some qualify this business relationship as a conflict of interest. One may indeed wonder if Google isn't paying Mozilla to keep competition in check and regulatory authorities at bay. A fox doesn't bite the hand that feeds it...


??? question "Firefox vs Chrome –⁠ What is the safest browser in 2022?"

    Many wonder which web browser is most secure when it comes to privacy. There are many private browsers, some of them are listed in the table below. Please mind that we don't recommend using Chrome or [Chromium](https://www.chromium.org/) based browsers from a privacy point of view. Leah Elliott's comic [Contra Chrome](https://contrachrome.com/) best sums up why Google’s browser has become a threat to user privacy:

    * Chrome as well as Chromium-based browsers run on code ultimately controlled by Google, and fortify Google's browser monopoly
    * Chrome, but also some other Chromium-based browsers are the gateway to Google's ecosystem, including GMail, YouTube, Google Photos, etc.
    * Google's browser as well as its other services systematically collect private user information, most of the times without consent, to then sell them to the highest bidder


    <div class="table-noscroll">

    <center>

    | | [Tor](https://gofoss.net/tor/) | [Librewolf](https://librewolf-community.gitlab.io/) | [FOSS Browser](https://github.com/scoute-dich/browser/) | [Mull](https://github.com/Divested-Mobile/mull/) | [Fennec F-Droid](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/) | [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium/) | [Brave](https://brave.com) | [Bromite](https://www.bromite.org/) |
    |------|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
    | FOSS | <span style="color:orange">✔</span>^1^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^1^ | <span style="color:green">✔</span> |
    | Desktop Version | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Android Version | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Data Isolation (Tracking Protection) | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^2^ | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^2^ | <span style="color:orange">✔</span>^2^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Process Isolation (Sandbox) | <span style="color:orange">✔</span>^3^ | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Fingerprinting Protection | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ |
    | Content Blocker | <span style="color:green">✔</span>^5^ | <span style="color:green">✔</span>^5^ | <span style="color:orange">✔</span>^6^ | <span style="color:green">✔</span>^5^ | <span style="color:green">✔</span>^5^ | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^6^ |
    | Custom Search Engine | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Regular Security Updates | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Telemetry / Tracker Free | <span style="color:red">✗</span>^7^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span>^8^ | <span style="color:green">✔</span>^9^ | <span style="color:green">✔</span> | <span style="color:red">✗</span>^10^ | <span style="color:green">✔</span> |

    </center>

    </div>

    1. *Contains proprietary libraries.*

    2. *Basic Tracking Protection only.*

    3. *Available on Desktop only. Android version does not isolate websites to their own process to limit exposure to security vulnerabilities: neither [per-site process isolation](https://wiki.mozilla.org/Project_Fission) nor [sandboxing](https://bugzilla.mozilla.org/show_bug.cgi?id=1565196) are implemented.*

    4. *Basic Fingerprinting Protection only.*

    5. *Compatible with uBlock Origin.*

    6. *Basic Content Blocker only.*

    7. *Tor Browser for Android contains [3 trackers (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/en/reports/org.torproject.torbrowser/latest/)*

    8. *Exodus Privacy reports [2 trackers (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/en/reports/us.spotco.fennec_dos/latest/) for Mull. However, according to the development team, this is a [false positive](https://forum.f-droid.org/t/classyshark3exydus-found-five-trackers-inside-tor-browser/13453/20): the tracker libraries were replaced with "stubs", meaning that they have been replaced with code that does nothing.*

    9. *Exodus Privacy reports [2 trackers (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/en/reports/org.mozilla.fennec_fdroid/latest/) for Fennec F-Droid. However, according to the development team, this is a [false positive](https://forum.f-droid.org/t/welcome-a-new-fennec-f-droid/11113): the tracker libraries were replaced with "stubs", meaning that they have been replaced with code that does nothing.*

    10. *Brave is maintained by a for profit, VC-backed company. It features an (opt-in) ad system, and faced criticism in the past for adding affiliate links that it profits from. Transmits telemetry data to an analytics service by default, this can be opted-out.*


<br>

<center> <img src="../../assets/img/separator_ublockorigin.svg" alt="Firefox vs chrome security" width="150px"></img> </center>

## Best Firefox extensions

=== "Windows, macOS & Linux"

    Add [uBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin) to your fresh Firefox install. It's a free and open source content filter — and a good one at that! [Head over to Mozilla's addon store](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/), and click on the button `Add to Firefox`. Various settings allow to increase your privacy, as detailed in the instructions below.

    ??? tip "Show me the step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        * Click on the uBlock Origin icon in Firefox's toolbar
        * Make sure uBlock Origin is enabled, the large power button needs to be blue
        * Click on the Dashboard button
        * Open the tab `Filter lists`
        * Select the check boxes shown in the table below and click on `Apply changes`

        <center>

        | Section | Check box |
        | ------ | ------ |
        | Ads | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Privacy |  ☑ AdGuard Tracking Protection <br> ☑ EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Annoyances |  ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br>  ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>


=== "Android"

    Add [HTTPS Everywhere](https://www.eff.org/https-everywhere/) to your fresh Firefox install. It's a privacy extension which enables Hypertext Transfer Protocol Secure ([HTTPS](https://en.wikipedia.org/wiki/HTTPS)) by default, a form of encryption to protect web traffic. While the desktop version of Firefox allows to enable this option in the settings, an extension for HTTPS Everywhere is still needed on Android. Navigate to `Menu ‣ Add-ons` and click on the `+` symbol next to HTTPS Everywhere.

    Also add [uBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin), a free and open source content filter — and a good one at that! Navigate to `Menu ‣ Add-ons` and click on the `+` symbol next to uBlock Origin. Various settings allow to increase your privacy, as detailed in the instructions below.

    ??? tip "Show me the step-by-step guide for Android"

        * Navigate to `Menu ‣ Add-ons ‣ uBlock Origin ‣ Settings`
        * Open the tab `Filter lists`
        * Select the check boxes shown in the table below and click on `Apply changes`

        <center>

        | Section | Check box |
        | ------ | ------ |
        | Ads | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Privacy |  ☑ AdGuard Tracking Protection <br> ☑ EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Annoyances |  ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br>  ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>

<div style="margin-top:-20px">
</div>

??? tip "Show me the 2-minute summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ae911b43-b4bd-4059-8447-891e82bd2367" frameborder="0" allowfullscreen></iframe>

    </center>

??? info "What is a browser extension? Tell me more about privacy extensions!"

    Use extensions with **parsimony**. They facilitate [fingerprinting](https://en.m.wikipedia.org/wiki/Device_fingerprint), a practice used to collect information, track browsing habits and deliver targeted advertising. The more extensions, the more unique your fingerprint, and the larger the attack surface. Want to know how easy it is to identify and track your browser? Head over to [EFF's Cover Your Tracks](https://coveryourtracks.eff.org/).

    Use extensions with **caution**. Some might break websites. Add new extensions progressively and disable them in case of negative impacts. It can be challenging to strike the right balance between privacy and usability.

    Finally, some advice for **social network users**. Don't check boxes in uBlock Origin's filter list section `Annoyances` if you use social sharing buttons from Facebook, Twitter and the like.


    | Addons | Description |
    | ------ | ------ |
    | [Clear URLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/) | Remove tracking elements from URLs. |
    | [Cookie autodelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/) | Automatically delete unused cookies when closing tabs. |
    | [I don't care about cookies](https://addons.mozilla.org/en-US/firefox/addon/i-dont-care-about-cookies/) | Get rid of cookie warnings. |
    | [Miner block](https://addons.mozilla.org/en-US/firefox/addon/minerblock-origin/) | Block cryptocurrency miners. |
    | [Cloud firewall](https://addons.mozilla.org/en-US/firefox/addon/cloud-firewall/) | Block connections to cloud services from Google, Amazon, Facebook, Microsoft, Apple and Cloudflare. |
    | [CSS exfil protection](https://addons.mozilla.org/en-US/firefox/addon/css-exfil-protection/) | Guard your browser against data theft from web pages using CSS. |
    | [Disconnect](https://addons.mozilla.org/en-US/firefox/addon/disconnect/) | Visualise and block web tracking. |
    | [Noscript](https://noscript.net/) | Allow only trusted web sites to execute JavaScript, Java, Flash and other plugins. |
    | [HTTPS everywhere](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/) | Encrypt web traffic, make browsing more secure. |
    | [Privacy badger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/) | Stop advertisers and other third-party trackers from secretly spying on you. |
    | [Decentraleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/) | Block tracking via content delivery networks operated by third parties. |
    | [Terms of service; didn't read](https://addons.mozilla.org/en-US/firefox/addon/terms-of-service-didnt-read/) | Understand websites' terms & privacy policies, with ratings and summaries. |


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Browser security model" width="150px"></img> </center>

## Firefox privacy settings & browser security

=== "Windows, macOS & Linux"

    Open a new tab in Firefox. Remove any clutter from the empty tab, such as `Top Sites` or `Highlights`. Next, type `about:preferences` in the address bar to access Firefox's privacy and security settings. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        *Warning*: Apply these settings with caution, some might break web sites. Add new settings progressively and disable them in case of negative impacts.

        <center>

        | Menu | Settings |
        | ------ | ------ |
        | General | In the section `Language`, uncheck the box `Check your spelling as you type`. |
        | General | In the section `Browsing`, uncheck the two boxes `Recommend extensions as you browse` and `Recommend features as you browse`. |
        | Home | In the section `Firefox Home Content`, uncheck the boxes `Shortcuts`, `Recent activity` and `Snippets`. |
        | Search | In the section `Search Suggestions`, uncheck the box `Provide search suggestions`. |
        | Search | In the section `Search Shortcuts`, remove Google, Bing, Amazon and Ebay. |
        | Search | Browse to [Disroot Searx](https://search.disroot.org/) and add it to the default search engines by right clicking or clicking on the 3-dot actions menu in the address bar. <br><br> *Remark*: You'll find more suggestions on privacy respecting search engines at the end of this section. |
        | Search | Go back to the section `Default Search Engine` in Firefox's settings and select Disroot SearX. |
        | Privacy & Security | The section `Enhanced Tracking Protection` contains settings to prevent third parties from tracking you across websites. Select between `Standard` and `Strict` settings. Stricter settings will block more trackers and ads, but are also more likely to break websites.|
        | Privacy & Security | In the section `Tracking Protection`, also select `Always Send websites a Do Not Track signal`.|
        | Privacy & Security | In the section `Cookies and Site Data`, select `Delete cookies and site data when Firefox is closed`. Then click on `Clear Data` and erase all cookies and site data stored by Firefox. |
        | Privacy & Security | In the section `Logins and Passwords`, unselect `Ask to save logins and passwords for websites`. |
        | Privacy & Security | In the section `History`, select `Use custom settings for history`. <br><br>Uncheck the boxes `Remember browsing and download history` and `Remember search and form history`. <br><br>Instead, check the box `Clear history when Firefox closes`. Then click on `Clear History` and erase all data stored by Firefox. <br><br>*Remark*: this is a work-around, as for some obscure reason `Never remember history` breaks many add-ons. |
        | Privacy & Security | In the section `Firefox Data Collection and Use`, uncheck all entries. |
        | Privacy & Security | In the `Security` section, uncheck the entry `Block dangerous and deceptive content`. While this setting helps spotting phishing and malware, it does so by [establishing a connection to Google's servers](https://support.mozilla.org/en-US/kb/how-does-phishing-and-malware-protection-work). |
        | Privacy & Security | In the section `HTTPS-Only Mode`, select `Enable HTTPS-Only Mode in all windows`. This enables the Hypertext Transfer Protocol Secure ([HTTPS](https://en.wikipedia.org/wiki/HTTPS)) by default, a form of encryption which protects web traffic. A (green) lock should show up in Firefox's address bar each time you navigate to a website. |
        | Privacy & Security | In the section `Address Bar`, unselect `Contextual suggestions` and `Include occasional sponsored suggestions`. |
        | General | (Optional) Check the box `Always check if Firefox is your default browser` and click on `Make Default`. |

        </center>


=== "Android"

    Launch Firefox. On the welcome screen, scroll down and click on the button `Start browsing`. Remove any clutter from the empty tab, such as `Google` or `Top Articles`. Next, navigate to `Menu ‣ Settings` and adjust Firefox's privacy and security settings. More detailed instructions below.

    ??? tip "Show me the step-by-step guide for Android"

        *Warning*: Apply these settings with caution, some might break web sites. Add new settings progressively and disable them in case of negative impacts.

        <center>

        | Menu | Settings |
        | ------ | ------ |
        | Search | In the section `Default search engine`, remove Google, Bing, Amazon, Qwant and Ebay. |
        | Search | In the section `Default search engine`, click on `+ Add search engine`. Under `Other`, fill in the following:<br><br> • `Name`: `Disroot SearX`<br><br> • `Search string to use`: `https://search.disroot.org/search?q=%s` <br><br>Then click on `🗸` to apply all changes. <br><br>*Remark*: You'll find more suggestions on privacy respecting search engines at the end of this section. |
        | Search | Back in the section `Default search engine`, select `Disroot SearX`. |
        | Search | In the section `Address bar`, disable the options `Autocomplete URLs`, `Show clipboard suggestions`, `Search browsing history` and `Show search suggestions`. |
        | Customise | In the section `Home`, disable `Show most visited sites`. |
        | Logins and passwords | Change the option `Save logins and passwords` to `Never save`. |
        | Logins and passwords | Disable `Autofill`. |
        | Enhanced Tracking Protection | Make sure the option `Enhanced Tracking Protection` is enabled to prevent third parties from tracking you across websites. Select between `Standard` and `Strict` settings. Stricter settings will block more trackers and ads, but are also more likely to break websites.|
        | Delete browsing data on quit | Enable the option `Delete browsing data on quit`. If you want open tabs to be restored after closing Firefox, unselect `Open tabs`. |
        | Data collection | Disable `Usage and technical data`, `Marketing data` as well as `Studies`. |
        | General | (Optional) Enable `Set as default browser`. |

        </center>

<div style="margin-top: -20px;">
</div>

??? tip "Show me the 3-minute summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7e4abf0d-c96b-4455-bb44-6951e1b0619c" frameborder="0" allowfullscreen></iframe>

    </center>


??? info "Tell me more about privacy-respecting search engines"

    <center>

    | Search engine | Description |
    | ------ | ------ |
    | [Searx](https://asciimoo.github.io/searx/) | Open source meta search engine. Aggregates anonymous search results from various engines. [Various instances are accessible online](https://searx.space), for example from [Disroot](https://search.disroot.org/). Can also be self-hosted. |
    | [Duckduckgo](https://duckduckgo.com/) | US-based meta search engine, mainly aggregates Bing/Yahoo results. |
    | [Ecosia](https://www.ecosia.org/) | German meta search engine, mainly provides Bing results and plants trees. |
    | [Swisscows](https://swisscows.com/) | Swiss meta search engine, mainly provides Bing results. |
    | [Mojeek](https://www.mojeek.com/) | UK based search engine. |
    | [Metager](https://metager.de/) | German open source meta search engine. |
    | [Qwant](https://www.qwant.com/) | French meta search engine, VC-funded (large multimedia company Axel-Springer is an investor). |
    | [Startpage](https://www.startpage.com/) | Dutch meta search engine, mainly provides Google results. System 1, an ad company, is shareholder since October 2019. |

    </center>


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Harden Firefox" width="150px"></img> </center>

## Firefox User.js

Firefox offers a whole range of advanced privacy and security settings. On desktop devices, these can be accessed by typing `about:config` in the address bar and confirming a security warning. Given the sheer amount of options, this can however quickly become tedious. An easier way is to install a `user.js` file. This small JavaScript file contains a number of pre-configured settings and loads each time you launch Firefox. More details on how to install the user.js file below!

??? tip "Show me a step-by-step guide for Windows, macOS & Linux (Ubuntu)"

    *Warning*: use `user.js` files with caution. The stricter the privacy level, the more websites might break!

    Back up your current configuration, which is stored in a file named `pref.js`:

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Step 1 | Type `about:support` in Firefox's address bar. |
    | Step 2 | Go to `Application Basics`. |
    | Step 3 | Click on `Open Directory`. |
    | Step 4 | Back up a copy of the `pref.js` file. |

    </center>

    Now download your preferred `user.js` template. Find out more about available templates at the end of this section. Place the downloaded `user.js` file in the same folder as the `pref.js` file. Depending on your browser and operating system, this folder should be located here:


    <center>

    | OS | Path |
    | ------ | ------ |
    | Windows | `%APPDATA%\Mozilla\Firefox\Profiles\XXXXXXXX.your_profile_name\user.js` |
    | macOS | `~/Library/Application Support/Firefox/Profiles/XXXXXXXX.your_profile_name` |
    | Linux (Ubuntu) | `~/.mozilla/firefox/XXXXXXXX.default-release/user.js` |

    </center>

    If at any point you need to revert back to your initial settings, just restore the file `pref.js` and delete the `user.js` file.


??? tip "Show me the 2-minute summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4b9255d5-3d9f-4319-bd3a-8214a396df18" frameborder="0" allowfullscreen></iframe>

    </center>


??? question "Where can I find user.js templates?"

    * [Arkenfox user.js](https://github.com/arkenfox/user.js) strikes a nice balance between privacy and usability. More information on the [wiki page](https://github.com/arkenfox/user.js/wiki)
    * [Pyllyukko](https://github.com/pyllyukko/user.js/)
    * [Relaxed Pyllyukko](https://github.com/pyllyukko/user.js/tree/relaxed/)
    * [Privacy Handbook minimal configuration (German)](https://www.privacy-handbuch.de/download/minimal/user.js)
    * [Privacy Handbook moderate configuration (German)](https://www.privacy-handbuch.de/download/moderat/user.js)
    * [Privacy Handbook strict configuration (German)](https://www.privacy-handbuch.de/download/streng/user.js)
    * [A tool to make your own user.js file](https://ffprofile.com/)
    * [Explanation of common user.js settings](http://kb.mozillazine.org/About:config_Entries/)


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Safest web browser" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [Firefox's support documentation](https://support.mozilla.org/en-US/products/firefox/get-started) or ask [the Firefox community](https://support.mozilla.org/en-US/questions/new) for help.

* [uBlock's official documentation](https://github.com/gorhill/uBlock/wiki/) or one of the many [online tutorials](https://www.maketecheasier.com/ultimate-ublock-origin-superusers-guide/) if you're interested in more advanced usage.

<center>
<img align="center" src="https://imgs.xkcd.com/comics/perspective.png" alt="Safest internet browser"></img>
</center>

<br>

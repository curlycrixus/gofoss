---
template: main.html
title: Datenschutz geht uns alle an
description: Warum ist digitale Freiheit wichtig? Sind Datenschutz und Privatsphäre Grundrechte? Wie bekämpft man den Überwachungskapitalismus? Wie bekämpft man geplante Obsoleszenz?
---

# Datenschutz geht uns alle an

!!! level "Letzte Aktualisierung: März 2022."

<center> <img src="../../assets/img/privacy.png" alt="Online-Datenschutz" width="700px"></img> </center>

## Privatsphäre ist ein Grundrecht

*"Wer nichts zu verbergen hat, hat auch nichts zu befürchten"* — diesen Satz haben wir alle schon mal gehört. Interessanterweise ist die zugrunde liegende These ein Trugschluss. Sie impliziert nämlich, dass der Wunsch nach Datenschutz oder Privatsphäre zwangsläufig auch ein Wunsch nach Verschleierung eines Fehlverhaltens ist.

Das ist natürlich falsch. Wer sein entsperrtes Telefon nur ungern dem erst besten Passanten überlässt ist deshalb noch lange kein Krimineller. Es gibt durchaus Aspekte unseres Lebens, die wir gerne für uns behalten möchten, auch ohne etwas verbrochen zu haben. Sei es, weil wir uns frei entfalten und unsere Persönlichkeit erforschen wollen, ohne verurteilt oder stigmatisiert zu werden. Oder weil wir uns vor Überwachung, Zensur, Manipulation und Identitätsdiebstahl schützen möchten. Aus genau solchen Gründen wurden Vorhänge erfunden. Oder Dinge wie das Bankgeheimnis, das Anwaltsgeheimnis, das Briefgeheimnis, das Wahlgeheimnis, das Beichtgeheimnis oder die ärztliche Schweigepflicht.

Wir neigen dazu zu vergessen, dass Datenschutz und Privatsphäre Grundrechte sind. Rechte, die im UN-Menschenrechtsrat, im Internationalen Pakt über bürgerliche und politische Rechte sowie in einer Reihe von Abkommen und Gesetzen verankert sind.

Aus diesem Grund müssen wir Unternehmen und Regierungen daran hindern, unsere Gespräche, Erinnerungen, Aufenthalte, Krankengeschichten und vieles mehr zu erfassen und dauerhaft aufzubewahren. Um es mit den Worten von [Geoffrey A. Fowler](https://www.washingtonpost.com/technology/2019/12/31/how-we-survive-surveillance-apocalypse/) von der Washington Post zusammenzufassen: *"Online-Privatsphäre ist nicht tot, aber man muss wütend und geduldig genug sein, um sie zu erlangen"*.

??? question "In den Fußstapfen von Upton Sinclairs 1917 erschienenem Roman "Religion und Profit""

    (Freie Übersetzung) *"Nicht nur meine eigene Post wurde geöffnet, sondern auch die aller meiner Verwandten und Freunde, die an so weit voneinander entfernten Orten wie Kalifornien und Florida wohnten. Ich erinnere mich an das freundliche Lächeln eines Regierungsbeamten, bei dem ich mich über diese Angelegenheit beschwerte: "Wenn Sie nichts zu verbergen haben, haben Sie auch nichts zu befürchten." Meine Antwort war, dass ich durch das Studium vieler Arbeitsrechtsfälle die Methoden des Provokateurs kennen gelernt habe. Er ist durchaus bereit, echte Beweise zu sammeln, wenn er sie finden kann; aber wenn nicht, hat er sich mit den Angelegenheiten seines Opfers vertraut gemacht und kann Beweise vorlegen, die überzeugend sind, wenn sie von der Boulevardpresse ausgewertet werden.*

??? question "Privatsphäre, Anonymität oder Sicherheit — wo liegt der Unterschied"

    <center>

    | Begriff | Beschreibung |
    | ------ | ------ |
    | Privatsphäre |Es geht darum, Informationen vertraulich zu behandeln. Andere können ruhig erfahren, wer Ihr seid — aber nicht, was Ihr denkt, sagt oder tut. |
    | Anonymität |Es geht darum, Eure Identität zu verbergen. Andere können ruhig erfahren, was Ihr denkt, sagt oder tut — aber nicht, wer Ihr seid. |
    | Sicherheit |Es geht darum, Maßnahmen zum Schutz Eurer Privatsphäre bzw. Anonymität zu ergreifen. |

    </center>

    Mehr Sicherheit bedeutet zwar meist mehr Privatsphäre bzw. Anonymität, aber auch weniger Bequemlichkeit. [Wo der richtige Mittelweg liegt, hängt von Eurem individuellen Bedrohungsmodell ab](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf). Diese Webseite sollte Euch nützlich sein, wenn Ihr ein vernünftiges Maß an Privatsphäre anstrebt. Nicht aber, falls Ihr vorhabt für Geheimdienste tätig zu werden, Euch vor einflussreichen Gegner zu verstecken oder gegen Gesetze zu verstoßen.


<br>

<center> <img src="../../assets/img/separator_money.svg" alt="Datenkraken und Überwachungskapitalismus" width="150px"></img> </center>

## Das Zeitalter der Datenkraken

Datenkraken zeichnen sich dadurch aus, dass sie NutzerInnen in Produkte verwandeln. Persönliche Nutzerdaten sind ein wichtiger Bestandteil des Geschäftsmodells dieser Unternehmen, genauso wie scheinbar "kostenlose" Dienstleistungen, Kundenbindung, Tracking und zielgerichtete Werbung.

Google und Facebook sind mit dieser Praxis besonders erfolgreich. Doch nur weil Apple, Amazon und Microsoft ihr Geschäft nicht (ausschließlich) auf das Ausspähen von Daten ausrichten, macht das noch lange keine Datenschützer aus ihnen. Ganz im Gegenteil, auch diese Unternehmen profitieren massiv von der Zusammenarbeit mit Datenmaklern und Datenschutzverletzern. Alle fünf Unternehmen — die oft als "Datenkraken", "Big Five" oder "Big Tech" bezeichnet werden — werden regelmäßig beschuldigt bzw. verurteilt: [Fehlverhalten bei der Datenerfassung und -verfolgung](https://www.gizmodo.com.au/2021/05/google-location-services-lawsuit/), [Überwachung von Bürgern](https://www.theguardian.com/commentisfree/2021/may/18/amazon-ring-largest-civilian-surveillance-network-us), [Verletzung von Datenschutzbestimmungen](https://edps.europa.eu/press-publications/press-news/press-releases/2021/edps-opens-two-investigations-following-schrems_en), [Steuerflucht](https://www.theguardian.com/technology/2019/jan/03/google-tax-haven-bermuda-netherlands), [kartellrechtliche Bedenken](https://www.statista.com/chart/14752/eu-antitrust-fines-against-tech-companies/), [Untergrabung ethischer Standards](https://en.wikipedia.org/wiki/Criticism_of_Facebook), [Missbrauch des Arbeitsrechts](https://www.theatlantic.com/technology/archive/2019/11/amazon-warehouse-reports-show-worker-injuries/602530/) und so weiter.


<center>
  <html>
   <embed src="../../assets/echarts/gafam_stats.html" style="width: 100%; margin-top:20px; height:400px">
  </html>
</center>

Doch trotz — oder gerade wegen — dieser fragwürdigen Praktiken gewinnen die Datenkraken stetig an Wert. Im Jahr 2021 betrug ihr Börsenwert fast 9 Billionen Dollar. Wären die Datenkraken eine Nation, so wären sie neben den USA und China die drittgrößte Volkswirtschaft der Welt. Big Tech ist heute mehr wert als das (gemeinsame!) Bruttoinlandsprodukt Deutschlands und des Vereinigten Königreichs.

"*Information ist Macht. Aber wie so oft gibt es diejenigen, die alle Macht für sich behalten wollen*". Bereits 2008 verurteilte Aaron Swartz in seinem umstrittenen [Guerilla Open Access Manifesto](https://archive.org/details/GuerillaOpenAccessManifesto/) Privatunternehmen dafür, Informationen zu zentralisieren, zu digitalisieren und unter Verschluss zu halten. Die Lage hat sich nicht zum Besseren gewendet.


??? question "Welche Informationen sammeln Technologieunternehmen über mich?"

    <center> <img src="../../assets/img/what_info_tech_companies_collect.png" alt="Datensammlung" width="100%"></img> </center>

    Quelle: [TruePeopleSearch.net](https://truepeoplesearch.net/).


<br>

<center> <img src="../../assets/img/separator_compatibility.svg" alt="Massenüberwachung" width="150px"></img> </center>


## Wehrt Euch gegen Massenüberwachung

Viele Regierungen schränken die Freiheiten ihrer Bürger ein. Aus Angst vor Protesten, vor Terrorismus oder vor Epidemien werden Notstandsgesetze erlassen und aufrechterhalten. Dabei werden flächendeckend immer datenschutzfeindlichere Praktiken eingesetzt.

Doch anstatt das Gemeinwohl zu wahren, werden die von Unternehmen und Regierungen gehorteten Daten dazu missbraucht die Bevölkerung zu überwachen, die politische Debatte zu polarisieren oder die Öffentlichkeit zu spalten. Diese Entwicklung schwächt die Demokratie. Unsere Geschichte hat uns immer wieder gelehrt, dass Massenüberwachung die Welt nicht sicherer macht. Ganz im Gegenteil! Überwachung, Wahleinmischung und Zensur sind Grundpfeiler der Unterdrückung und staatlicher Gewalt.

Es ist an der Zeit, sich gegen den Überwachungskapitalismus zu wehren, um das friedliche Zusammenleben in einer zunehmend digitalisierten Welt zu gewährleisten. Benjamin Franklin würde dem sicherlich zustimmen: *"Diejenigen, die grundlegende Freiheiten aufgeben, um ein wenig mehr vorübergehende Sicherheit zu erlangen, verdienen weder Freiheit, noch Sicherheit"*.


??? info "Hier erfahrt Ihr mehr über Massenüberwachung"

    <center>

    | Bedrohung für die Demokratie | Beschreibung |
    | ------ | ------ |
    | Globale Überwachungsprogramme |Im Nachgang zum US-Krieg gegen den Terror enthüllten mehrere Informanten die Existenz [globaler Massenüberwachungsprogramme](https://de.wikipedia.org/wiki/Globale_%C3%9Cberwachungs-_und_Spionageaff%C3%A4re). Mit mehr oder weniger freiwilliger Unterstützung großer Unternehmen begannen Regierungen auf der ganzen Welt, Informationen über ihre Bürger zu sammeln, zu speichern und weiterzugeben: Online-Aktivitäten, Telefonanrufe, Textnachrichten, Standortdaten usw. |
    |PRISM | Das NSA-Überwachungsprogramm PRISM wurde erstmals 2013 von The Guardian und The Washington Post enthüllt. Wie [The Verge](https://www.theverge.com/2013/7/17/4517480/nsa-spying-prism-surveillance-cheat-sheet/) erklärt, handelt es sich um ein System zur Erfassung persönlicher Nutzerdaten mit Hilfe von Technologieunternehmen und anderen Organisationen. <br><br> <center><img src="../../assets/img/prism.png" align="center" alt="Prism" width="500px"></img></center> |
    | XKeyscore| Das Xkeyscore-Programm der NSA wurde erstmals 2013 von [The Guardian](https://www.theguardian.com/world/2013/jul/31/nsa-top-secret-program-online-data) enthüllt. Wie von [The Intercept](https://theintercept.com/2015/07/01/nsas-google-worlds-private-communications/) erläutert, handelt es sich dabei um ein Massenüberwachungssystem, das mit Internet-Daten aus Glasfaserkabeln gespeist wird. Um Edward Snowden zu zitieren: *"Ich, an meinem Schreibtisch sitzend, konnte jeden abhören, von Ihnen oder Ihrem Buchhalter bis hin zum Bundesrichter oder sogar dem Präsidenten, soweit ich nur über eine persönliche E-Mail verfügte"*. <br><br> <center><img src="../../assets/img/xkeyscore.jpeg" align="center" alt="Xkeyscore" width="500px"></img></center> |
    | Boundless Informant | Boundless Informant ist eines der umfangreichen Datenanalysetools der NSA. Im Jahr 2013 veröffentlichte [The Guardian](https://www.theguardian.com/world/2013/jun/08/nsa-boundless-informant-global-datamining) folgende Karte, auf der Länder von grün (geringste Überwachung) über gelb und orange bis rot (höchste Überwachung) dargestellt sind. Nach Schätzungen von [The Atlantic Wire](https://www.theatlantic.com/national/archive/2013/06/nsa-datacenters-size-analysis/314364/) hat die NSA allein in März 2013 sieben Petabyte an Daten abgerufen und gespeichert. Es ist schwer, sich solche Zahlen vor Augen zu führen. Zum Vergleich, dieser Datenumfang entspricht in etwa dem gesamten Fernsehprogramm des letzten Jahrzehnts. <br><br> <center><img src="../../assets/img/boundlessinformant.png" align="center" alt="Boundless informant" width="500px"></img></center> |
    | Cambridge Analytica |Die Enthüllungen über die Rolle von [Cambridge Analytica](https://de.wikipedia.org/wiki/Cambridge_Analytica) während des US-Präsidentschaftswahlkampfs 2016 oder des britischen Referendums über die Mitgliedschaft in der Europäischen Union zeigen, wie der Missbrauch persönlicher Nutzerdaten sowie verzerrender Algorithmen demokratische Prozesse beeinflusst und politische Resonanzräume schürt. <br><br> <center><img src="../../assets/img/cambridge_analytica.png" align="center" alt="Cambridge Analytica" width="250px"></img></center> |
    | Die Pandemie im Blick |Um den Ausbruch der [COVID-19 Pandemie](https://www.top10vpn.com/research/covid-19-digital-rights-tracker/) einzudämmen, haben viele Länder haben damit begonnen, Bürger anhand ihres Telefonstandorts zu orten (oder zu verfolgen). Die Daten werden von Apps, Telekommunikationsbetreibern und Technologieunternehmen wie [Google und Apple](https://www.theverge.com/2020/4/10/21216715/apple-google-coronavirus-covid-19-contact-tracing-app-details-use/) gesammelt und bereitgestellt. Hinter einer scheinbar lobenswerten Absicht verbirgt sich jedoch Technologie, die digitale Verfolgung, physische Überwachung und Zensur ermöglicht. Tatsächlich hat es nicht lange gedauert, bis die ersten [Datenschutzlücken entdeckt wurden](https://themarkup.org/privacy/2021/04/27/google-promised-its-contact-tracing-app-was-completely-private-but-it-wasnt).|

    </center>


<center>
<img align="center" src="https://imgs.xkcd.com/comics/privacy_opinions.png" width="550px" alt="Warum Datenschutz wichtig ist"></img>
</center>

<br/>

---
template: main.html
title: Warum Ihr Eure Daten sichern solltet
description: Wie sichert man Daten? Was ist die 3-2-1 Regel der Datensicherung? Wie installiert man FreeFileSync? Was ist eine Spiegelsynchronisation? Was ist eine Zwei-Wege-Synchronisation?
---

# 3-2-1 Datensicherung

!!! level "Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/cloud_storage_2.png" alt="3-2-1 Datensicherung" width="550px"></img>
</div>

Ihr meint Datensicherung sei langweilig? Seit gewiss, der Verlust Eurer Daten oder [Passwörter](https://gofoss.net/de/passwords/) ist nicht wesentlich spannender. Datensicherung macht Euch unempfinglich gegen Datenverlust, Diebstahl, Beschädigung oder versehentliches Löschen. Erstellt eine regelmäßige 3-2-1 Datensicherungsroutine, um mindestens drei Kopien Eurer Daten aufzubewahren:

* *Originaldateien*: sind auf Euren Geräten gespeichert, z. B. Computer, Laptop, Mobiltelefon, Kamera usw.
* *Lokale Kopien*: werden in regelmäßigen Abständen angelegt. Bewahrt diese Kopien vor Ort auf, zum Beispiel zu Hause oder an Eurem Arbeitsplatz. Kommt es zu einem Zwischenfall der Eure Geräte betrifft, können die Daten schnell wiederhergestellt werden. Für zusätzliche Sicherheit könnt Ihr auch eine Datenverschlüsselung, z.B. mit [VeraCrypt](https://gofoss.net/de/encrypted-files/) in Betracht ziehen.
* *Remote-Kopien*: werden in regelmäßigen Abständen angelegt. Bewahrt diese Kopien außer Haus bzw. außerhalb des Arbeitsplatzes auf, zum Beispiel [in der Cloud](https://gofoss.net/de/cloud-storage/) oder an einem anderen Ort. Kommt es zu einem Zwischenfall, der sowohl Eure Geräte als auch die lokalen Sicherungskopien betrifft, können die Daten trotz allem wiederhergestellt werden. Z. B. im Falle von Diebstahl, Feuer, Wasserschäden usw. Für zusätzliche Sicherheit könnt Ihr auch eine Datenverschlüsselung, z.B. mit [Cryptomator](https://gofoss.net/de/encrypted-files/) in Betracht ziehen.

Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/ubuntu-server/), erklären wir in einem eigenen Kapitel, [wie man Server-Backups plant](https://gofoss.net/de/server-backups/).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Datensicherung" width="150px"></img> </center>

## FreeFileSync

[FreeFileSync](https://freefilesync.org/) ist ein freies und [quelloffenes](https://github.com/hkneptune/FreeFileSync/) Programm zur Datensynchronisation. Es erstellt und verwaltet Sicherungskopien, indem es Unterschiede zwischen Quell- und Zielordnern erkennt und abgleicht. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | FreeFileSync installieren | Ladet das aktuelle [FreeFileSync-Installationsprogramm für Windows](https://freefilesync.org/download.php) herunter, doppelklickt auf die `.exe`-Datei und folgt dem Installationsassistenten. |
        | Ordner auswählen | Startet FreeFileSync und wählt die beiden zu vergleichenden Ordner aus <br>• `Quellordner`: wählt im linken Fenster den Ordner aus, der die zu sichernden Daten enthält <br>• `Zielordner`: wählt im rechten Fenster den Ordner aus, in dem die Sicherungskopie abgelegt werden soll |
        | Ordner vergleichen | Klickt auf die Schaltfläche `Vergleichen`, um die Unterschiede zwischen beiden Ordnern auf Grundlage der Dateiänderungszeit- und Größe zu ermitteln. Ihr könnt ebenfalls einen detaillierteren aber langsameren Vergleich auf Grundlage des eigentlichen Dateiinhalts vornehmen. Sobald der Vorgang abgeschlossen ist, könnt Ihr in der mittleren Spalte überprüfen, welche Dateien in beiden Ordnern identisch sind, und welche Dateien sich unterscheiden oder nur in einem der beiden Ordner vorhanden sind. |
        | Ordner synchronisieren | Es gibt zwei Möglichkeiten, Quell- und Zielordner zu synchronisieren. Überlegt sorgfältig, welche Methode Euren Bedürfnissen entspricht: <br><br>*Spiegelsynchronisation*:<br><br>• geht wie oben beschrieben vor, um beide Ordnerinhalte zu vergleichen <br>• wählt die Synchronisationsoption `Spiegeln ->` <br>• die mittlere Spalte sollte nun eine einzige Synchronisationsrichtung anzeigen, und zwar vom Quell- (links) zum Zielordner (rechts) <br>• in der mittleren Spalte könnt Ihr also überprüfen, welche Dateien im Zielordner aktualisiert werden, welche Dateien dem Zielordner neu hinzugefügt werden und welche Dateien aus dem Zielordner entfernt werden <br>• nutzt das Rechtsklick-Menü, um einzelne Dateien oder ganze Unterordner vom Synchronisationsprozess auszuschließen <br>• klickt auf die Schaltfläche `Synchronisieren` wenn Ihr alle Dateien ausgewählt habt, die synchronisiert werden sollen. FreeFileSync sollte nun Dateien aus dem Quell- in den Zielordner kopieren bzw. aus dem Zielordner entfernen, bis beide Ordner komplett abgeglichen sind <br><br>*Zwei-Wege-Synchronisation*:<br><br>• geht wie oben beschrieben vor, um beide Ordnerinhalte zu vergleichen <br>• wählt die Synchronisationsoption `<- Zwei Wege ->` <br>• Im Gegensatz zur Spiegelsynchronisation werden bei der Zwei-Wege-Synchronisation Unterschiede zwischen den beiden Ordnern in beiden Richtungen erkannt. Die mittlere Spalte sollte daher zwei Synchronisationsrichtungen anzeigen, sowohl vom Quell- (links) zum Zielordner (rechts), als auch umgekehrt <br>• Ihr könnt in der mittleren Spalte überprüfen, welche Dateien im Quell- und/oder Zielordner aktualisiert werden, welche Dateien zum Quell- und/oder Zielordner hinzugefügt werden und welche Dateien aus dem Quell- und/oder Zielordner gelöscht werden <br>• nutzt das Rechstsklick-Menü, um die Synchronisationsrichtung zu ändern oder Dateien/Ordner komplett vom Synchronisationsprozess auszuschließen <br>• klickt auf die Schaltfläche `Synchronisieren` wenn Ihr definiert habt, wie alle Dateien synchronisiert werden sollen. FreeFileSync sollte nun Dateien zwischen Quell- und Zielordner kopieren bzw. löschen, bis beide Ordner komplett abgeglichen sind |

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | FreeFileSync installieren | Ladet das aktuelle [FreeFileSync-Installationsprogramm für macOS](https://freefilesync.org/download.php) herunter. Die FreeFileSync-Anwendung sollte von selbst starten und ein neues Volumen mounten. Falls nicht, öffnet die heruntergeladene `.dmg`-Datei und zieht das FreeFileSync-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das FreeFileSync-Symbol in das Andockmenü. |
        | Ordner auswählen | Startet FreeFileSync und wählt die beiden zu vergleichenden Ordner aus <br>• `Quellordner`: wählt im linken Fenster den Ordner aus, der die zu sichernden Daten enthält <br>• `Zielordner`: wählt im rechten Fenster den Ordner aus, in dem die Sicherungskopie abgelegt werden soll |
        | Ordner vergleichen | Klickt auf die Schaltfläche `Vergleichen`, um die Unterschiede zwischen beiden Ordnern auf Grundlage der Dateiänderungszeit- und Größe zu ermitteln. Ihr könnt ebenfalls einen detaillierteren aber langsameren Vergleich auf Grundlage des eigentlichen Dateiinhalts vornehmen. Sobald der Vorgang abgeschlossen ist, könnt Ihr in der mittleren Spalte überprüfen, welche Dateien in beiden Ordnern identisch sind, und welche Dateien sich unterscheiden oder nur in einem der beiden Ordner vorhanden sind. |
        | Ordner synchronisieren | Es gibt zwei Möglichkeiten, Quell- und Zielordner zu synchronisieren. Überlegt sorgfältig, welche Methode Euren Bedürfnissen entspricht: <br><br>*Spiegelsynchronisation*:<br><br>• geht wie oben beschrieben vor, um beide Ordnerinhalte zu vergleichen <br>• wählt die Synchronisationsoption `Spiegeln ->` <br>• die mittlere Spalte sollte nun eine einzige Synchronisationsrichtung anzeigen, und zwar vom Quell- (links) zum Zielordner (rechts) <br>• in der mittleren Spalte könnt Ihr also überprüfen, welche Dateien im Zielordner aktualisiert werden, welche Dateien dem Zielordner neu hinzugefügt werden und welche Dateien aus dem Zielordner entfernt werden <br>• nutzt das Rechtsklick-Menü, um einzelne Dateien oder ganze Unterordner vom Synchronisationsprozess auszuschließen <br>• klickt auf die Schaltfläche `Synchronisieren` wenn Ihr alle Dateien ausgewählt habt, die synchronisiert werden sollen. FreeFileSync sollte nun Dateien aus dem Quell- in den Zielordner kopieren bzw. aus dem Zielordner entfernen, bis beide Ordner komplett abgeglichen sind <br><br>*Zwei-Wege-Synchronisation*:<br><br>• geht wie oben beschrieben vor, um beide Ordnerinhalte zu vergleichen <br>• wählt die Synchronisationsoption `<- Zwei Wege ->` <br>• Im Gegensatz zur Spiegelsynchronisation werden bei der Zwei-Wege-Synchronisation Unterschiede zwischen den beiden Ordnern in beiden Richtungen erkannt. Die mittlere Spalte sollte daher zwei Synchronisationsrichtungen anzeigen, sowohl vom Quell- (links) zum Zielordner (rechts), als auch umgekehrt <br>• Ihr könnt in der mittleren Spalte überprüfen, welche Dateien im Quell- und/oder Zielordner aktualisiert werden, welche Dateien zum Quell- und/oder Zielordner hinzugefügt werden und welche Dateien aus dem Quell- und/oder Zielordner gelöscht werden <br>• nutzt das Rechstsklick-Menü, um die Synchronisationsrichtung zu ändern oder Dateien/Ordner komplett vom Synchronisationsprozess auszuschließen <br>• klickt auf die Schaltfläche `Synchronisieren` wenn Ihr definiert habt, wie alle Dateien synchronisiert werden sollen. FreeFileSync sollte nun Dateien zwischen Quell- und Zielordner kopieren bzw. löschen, bis beide Ordner komplett abgeglichen sind |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | FreeFileSync installieren | Ladet das aktuelle [FreeFileSync-Installationsprogramm für Linux](https://freefilesync.org/download.php) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `FreeFileSync_XX.YY_Linux.tar.gz`. Im Rahmen dieses Tutorials gehen wir davon aus, dass die Datei in den Ordner `/home/gofoss/Downloads` heruntergeladen wurde. Entpackt und installiert die Dateien mit den folgenden Befehlen (passt die Befehle entsprechend an): <br><br>`cd /home/gofoss/Downloads` <br>`tar xfv FreeFileSync_XX.YY_Linux.tar.gz` <br>`./FreeFileSync_XX.YY_Install.run` <br><br> Folgt nun den Anweisungen auf dem Bildschirm: akzeptiert die Lizenzbedingungen, wählt ein Installationsverzeichnis aus und installiert das Programm. |
        | Ordner auswählen | Startet FreeFileSync und wählt die beiden zu vergleichenden Ordner aus <br>• `Quellordner`: wählt im linken Fenster den Ordner aus, der die zu sichernden Daten enthält <br>• `Zielordner`: wählt im rechten Fenster den Ordner aus, in dem die Sicherungskopie abgelegt werden soll |
        | Ordner vergleichen | Klickt auf die Schaltfläche `Vergleichen`, um die Unterschiede zwischen beiden Ordnern auf Grundlage der Dateiänderungszeit- und Größe zu ermitteln. Ihr könnt ebenfalls einen detaillierteren aber langsameren Vergleich auf Grundlage des eigentlichen Dateiinhalts vornehmen. Sobald der Vorgang abgeschlossen ist, könnt Ihr in der mittleren Spalte überprüfen, welche Dateien in beiden Ordnern identisch sind, und welche Dateien sich unterscheiden oder nur in einem der beiden Ordner vorhanden sind. |
        | Ordner synchronisieren | Es gibt zwei Möglichkeiten, Quell- und Zielordner zu synchronisieren. Überlegt sorgfältig, welche Methode Euren Bedürfnissen entspricht: <br><br>*Spiegelsynchronisation*:<br><br>• geht wie oben beschrieben vor, um beide Ordnerinhalte zu vergleichen <br>• wählt die Synchronisationsoption `Spiegeln ->` <br>• die mittlere Spalte sollte nun eine einzige Synchronisationsrichtung anzeigen, und zwar vom Quell- (links) zum Zielordner (rechts) <br>• in der mittleren Spalte könnt Ihr also überprüfen, welche Dateien im Zielordner aktualisiert werden, welche Dateien dem Zielordner neu hinzugefügt werden und welche Dateien aus dem Zielordner entfernt werden <br>• nutzt das Rechtsklick-Menü, um einzelne Dateien oder ganze Unterordner vom Synchronisationsprozess auszuschließen <br>• klickt auf die Schaltfläche `Synchronisieren` wenn Ihr alle Dateien ausgewählt habt, die synchronisiert werden sollen. FreeFileSync sollte nun Dateien aus dem Quell- in den Zielordner kopieren bzw. aus dem Zielordner entfernen, bis beide Ordner komplett abgeglichen sind <br><br>*Zwei-Wege-Synchronisation*:<br><br>• geht wie oben beschrieben vor, um beide Ordnerinhalte zu vergleichen <br>• wählt die Synchronisationsoption `<- Zwei Wege ->` <br>• Im Gegensatz zur Spiegelsynchronisation werden bei der Zwei-Wege-Synchronisation Unterschiede zwischen den beiden Ordnern in beiden Richtungen erkannt. Die mittlere Spalte sollte daher zwei Synchronisationsrichtungen anzeigen, sowohl vom Quell- (links) zum Zielordner (rechts), als auch umgekehrt <br>• Ihr könnt in der mittleren Spalte überprüfen, welche Dateien im Quell- und/oder Zielordner aktualisiert werden, welche Dateien zum Quell- und/oder Zielordner hinzugefügt werden und welche Dateien aus dem Quell- und/oder Zielordner gelöscht werden <br>• nutzt das Rechstsklick-Menü, um die Synchronisationsrichtung zu ändern oder Dateien/Ordner komplett vom Synchronisationsprozess auszuschließen <br>• klickt auf die Schaltfläche `Synchronisieren` wenn Ihr definiert habt, wie alle Dateien synchronisiert werden sollen. FreeFileSync sollte nun Dateien zwischen Quell- und Zielordner kopieren bzw. löschen, bis beide Ordner komplett abgeglichen sind |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="FreeFileSync Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten oder Antworten findet Ihr in der [FreeFileSync Dokumentation](https://freefilesync.org/manual.php), den [FreeFileSync Tutorials](https://freefilesync.org/tutorials.php) oder bei der [FreeFileSync-Gemeinschaft](https://freefilesync.org/forum/).

<br>

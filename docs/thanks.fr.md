---
template: main.html
title: Remerciements
description: Merci à nos proches. Merci aux communautés FOSS. Merci aux codeurs dévoués. Merci aux artistes du logiciel libre.
---

# Remerciements à la communauté FOSS

<div align="center">
<img src="../../assets/img/thanks.png" alt="Remerciements" width="600px"></img>
</div>

<br>

Merci à nos proches pour leur patience et leur soutien.

Merci aux communautés de la vie privée, des FOSS, et de l'auto-hébergement :

* [/r/degoogle](https://teddit.net/r/degoogle/)
* [/r/privacy](https://teddit.net/r/privacy/)
* [/r/fossdroid](https://teddit.net/r/fossdroid/)
* [/r/selfhosted](https://teddit.net/r/selfhosted/)
* [degoogle (Josh Moore)](https://degoogle.jmoore.dev/)
* [itsfoss](https://itsfoss.com/)
* [framasoft](https://framasoft.org/en/)
* [chatons](https://www.chatons.org/)
* [disroot](https://disroot.org/fr)
* [digitalcourage](https://digitalcourage.de/en)
* [riseup](https://riseup.net/)
* [privacytools](https://www.privacytools.io/)
* [systemausfall](https://systemausfall.org/)
* [picasoft](https://picasoft.net/)
* [letsdebugit](https://www.letsdebug.it/)
* [free software directory](https://directory.fsf.org/wiki/Main_Page)
* [open source software directory](https://opensourcesoftwaredirectory.com)

Merci à tous les développeurs dévoués ici-bas qui construisent des logiciels libres et open source:

* [firefox](https://www.mozilla.org/fr/firefox/)
* [ublock origin](https://fr.wikipedia.org/wiki/UBlock_Origin)
* [searx](https://searx.me/)
* [tor](https://www.torproject.org/)
* [protonvpn](https://protonvpn.com/)
* [mullvad](https://mullvad.net/fr/)
* [riseupvpn](https://riseup.net/fr/vpn)
* [calyxvpn](https://calyx.net/)
* [protonmail](https://protonmail.com/)
* [tutanota](https://tutanota.com/)
* [thunderbird](https://www.thunderbird.net/fr/)
* [signal](https://signal.org/fr/)
* [element](https://element.io/)
* [jami](https://jami.net/)
* [briar](https://briarproject.org/)
* [keepass](https://keepass.info/)
* [andotp](https://github.com/andOTP/andOTP/)
* [freefilesync](https://freefilesync.org/)
* [veracrypt](https://www.veracrypt.fr/)
* [cryptomator](https://cryptomator.org/)
* [f-droid](https://f-droid.org/fr/)
* [aurora store](https://auroraoss.com/)
* [lineageos](https://www.lineageos.org/), [microg](https://microg.org/) & [lineageos for microg](https://lineage.microg.org/)
* [calyxos](https://calyxos.org/)
* [exodus privacy](https://exodus-privacy.eu.org/fr/)
* [linux](https://www.linux.org/)
* [ubuntu](https://ubuntu.com/)
* [linux mint](https://linuxmint.com)
* [gnome](https://www.gnome.org/)
* [virtualbox](https://www.virtualbox.org/)
* [etherpad](https://etherpad.org/)
* [cryptpad](https://cryptpad.fr/)
* [seafile](https://www.seafile.com/en/home/)
* [nextcloud](https://nextcloud.com/)
* [onlyoffice](https://www.onlyoffice.com/fr/)
* [studs](https://sourcesup.cru.fr/projects/studs/)
* [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)
* [jitsi](https://meet.jit.si/)
* [mattermost](https://mattermost.com/)
* [xmpp](https://conversejs.org/)
* [matrix](https://matrix.org/)
* [sympa](https://www.sympa.org/)
* [diaspora](https://diasporafoundation.org/)
* [friendica](https://friendi.ca/)
* [gnu social](https://gnusocial.network/)
* [mastodon](https://joinmastodon.org/)
* [nitter](https://nitter.net/)
* [write freely](https://writefreely.org/)
* [mobilizon](https://joinmobilizon.org/fr/)
* [lemmy](https://join-lemmy.org/)
* [aether](https://getaether.net/)
* [teddit](https://teddit.net/)
* [peertube](https://joinpeertube.org/)
* [invidious](https://invidious.io/)
* [pixelfed](https://pixelfed.org/)
* [bibliogram](https://bibliogram.art/)
* [lutim](https://github.com/ldidry/lutim/)
* [funkwhale](https://funkwhale.audio/)
* [bookwyrm](https://bookwyrm.social/)
* [openstreetmaps](https://www.openstreetmap.org/)
* [umap](https://github.com/umap-project/umap/)
* [gogocarto](https://gogocarto.fr/projects)
* [libre translate](https://libretranslate.com/)
* [simply translate](https://translate.metalune.xyz/)
* [lstu](https://lstu.fr/)
* [polr](https://github.com/cydrobolt/polr/)
* [hastebin](https://www.toptal.com/developers/hastebin/)
* [privatebin](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Instances-Directory)
* [ghostbin](https://ghostbin.com/)
* [libera pay](https://liberapay.com/)
* [bitcoin](https://bitcoin.org/fr/)
* [ethereum](https://ethereum.org/fr/)
* [litecoin](https://litecoin.org/fr/)
* [linux malware detect](https://www.rfxn.com/projects/linux-malware-detect/)
* [clamav](https://www.clamav.net/)
* [duckdns](https://www.duckdns.org/)
* [pihole](https://pi-hole.net/)
* [openvpn](https://openvpn.net/)
* [letsencrypt](https://letsencrypt.org/)
* [dehydrated](https://dehydrated.io/)
* [radicale](https://radicale.org/)
* [davx5](https://www.davx5.com/)
* [piwigo](https://piwigo.org/)
* [jellyfin](https://jellyfin.org/)
* [rsnapshot](https://rsnapshot.org/)
* [material for mkdocs](https://squidfunk.github.io/mkdocs-material/)
* [echarts](https://github.com/apache/echarts)
* [senfcall](https://senfcall.de/)

Merci à toute l'équipe de [Framalang](https://framablog.org/framalang/) pour l'excellent travail de traduction en français : Aliénor, CLC, Côme, Frabrice, goofy, Guestr, jums, Marius, susy, Sylvain R. et bien d'autres.

Merci aux artistes open-source:

* Fond d'écran publié par Gam-Ol sous la [Licence Pixabay](https://pixabay.com/service/license/)
* Illustrations inspirées par [freepik](https://www.freepik.com/) et [undraw](https://undraw.co/)
* Icônes respectueusement empruntées à [ameixa](https://gitlab.com/xphnx/ameixa/)
* Logos et captures d'écran pour Firefox, Friendica, Mastodon, Funkwhale, etc. gracieusement fournis par [Wikimedia](https://commons.wikimedia.org/wiki/Main_Page)
* Animations Blender inspirées par [ducky3d](https://www.ducky3d.com/)
* [Contra Chrome](https://contrachrome.com/) est une excellente bande dessinée qui explique comment le navigateur de Google est devenu une menace pour la vie privée des utilisatrices et utilisateurs
* Les photos du chapitre *Origines* sont distribuées sous la [Licence Unsplash](https://unsplash.com/license), merci à Alexander Popov, Gabriel Perez, Sandro Katalina, Roman Denisenko, Andrea De Santis, Denys Nevozhai, Hello I'm Nik, Ray Zhuang, Matthew Henry, Markus Spiske, Gabriel Heinzer, Clark Tibbs, Alexandre Debiève, cheng feng, Taylor Vick, Adi Goldstein* L'humour geek vous a été servi par [xkcd](https://xkcd.com/)
* Musique retro gracieusement fournie par [Aries The Producer](https://ariestheproducer.com/) et [nihilore](http://www.nihilore.com/)


<br>
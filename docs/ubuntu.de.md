---
template: main.html
title: So installiert Ihr Ubuntu
description: Was ist Ubuntu? Ist Ubuntu besser als Windows? Was bedeutet Ubuntu? Was ist Ubuntu LTS? Wie installiert man Ubuntu?
---

# Ubuntu für Anfänger

!!! level "Letzte Aktualisierung: Mai 2022. Für Beginner sowie erfahrenere BenutzerInnen. Grundlegende technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/ubuntu_desktop.png" alt="So installiert man Ubuntu" width="600px"></img>
</div>


## Was ist Ubuntu LTS?

Ubuntu ist ein afrikanisches Wort und bedeutet *Menschlichkeit* oder *Nächstenliebe*. Es bezeichnet auch eine Philosophie, die Gemeinschaft, Verteilung des Wohlstands und Individualität fördert.

In diesem Sinne entwickelt und veröffentlicht Canonical Ltd das gleichnamige [Ubuntu-Betriebssystem](https://ubuntu.com/), eine quelloffene Debian-basierte Linux-Distribution. Ubuntu wird alle sechs Monate veröffentlicht, und alle zwei Jahre gibt es eine sogenannte *Long-Term-Support (LTS)* Version mit Langzeitunterstützung. Zum Zeitpunkt der Verfassung ist die neueste LTS-Version Ubuntu 22.04, die bis April 2027 unterstützt wird.

Ubuntu gibt es in vielen offiziellen und inoffiziellen Varianten, die unterschiedliche Desktop-Umgebungen mit eigenem Erscheinungsbild anbieten: Ubuntu GNOME (Standard-Version), Ubuntu Mate, Xubuntu, Lubuntu, Kubuntu, elementaryOS, Linux Mint, Pop!_OS, Zorin OS und so weiter. Dieses Kapitel stellt die offizielle Ubuntu-Variante mit der GNOME-Desktop-Umgebung vor.


## Ubuntu Live USB & VirtualBox

[Ubuntu](https://ubuntu.com/) könnt Ihr am einfachsten entdecken, indem Ihr es neben Eurem aktuellen Betriebssystem ausführt, zum Beispiel mittels eines Live-USB-Laufwerks oder VirtualBox. Sollte Ubuntu Euch nicht gefallen, könnt Ihr ganz einfach wieder zu Windows oder macOS zurückkehren.

=== "Live-USB oder -DVD"

    Startet Ubuntu von einer Live-DVD oder einem USB-Laufwerk, um es schnell mal auszuprobieren. Mehr Details findet Ihr nachstehend.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Herunterladen |Ladet die aktuellste [Ubuntu Long-Term-Support (LTS)-Version](https://ubuntu.com/download/desktop) herunter. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-LTS-Version 22.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle).|
        | Brennen |Verwendet einen [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) oder [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) Rechner, um die heruntergeladene `.iso`-Datei auf eine DVD zu brennen. Alternativ könnt Ihr auch einen [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) oder [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) Rechner verwenden, um ein bootfähiges USB-Laufwerk zu erstellen. |
        | Neustarten |Legt die DVD oder das bootfähige USB-Laufwerk in den Rechner ein, auf dem Ihr Ubuntu testen möchtet, und startet den Rechner neu. Die meisten Rechner booten automatisch von der DVD oder dem USB-Laufwerk. Sollte dies nicht der Fall sein, drückt beim Hochfahren Eures Computers wiederholt auf die Tasten `F12`, `ESC`, `F2` oder `F10`. Dadurch solltet Ihr Zugang zum Boot-Menü Eures Rechners erhalten, in dem Ihr dann die DVD bzw. das USB-Laufwerk als Boot-Laufwerk auswählen könnt. |
        | Ubuntu testen |Wählt nach dem Booten von der DVD oder dem USB-Laufwerk den Eintrag `Try or Install Ubuntu` und drückt die `EINGABE`-Taste. Wartet, bis Ubuntu vom bootfähigen Laufwerk lädt, das kann eine Weile dauern. Sobald der Startbildschirm erscheint, wählt den Eintrag `Ubuntu ausprobieren`. |

        </center>


=== "VirtualBox"

    [VirtualBox](https://www.virtualbox.org/) ist eine weitere Möglichkeit, Ubuntu zu entdecken ohne Euer bestehendes Betriebssystem zu beeinträchtigen. VirtualBox erstellt hierzu sogenannte virtuelle Maschinen, die auf Euren Windows- oder macOS-Rechnern laufen. Untenstehend findet Ihr eine ausführliche und einsteigerfreundliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        ### VirtualBox installieren

        === "Windows"

            Stellt erst einmal sicher, dass Euer Rechner die Mindestanforderungen erfüllt: 2 GHz Dual-Core-Prozessor, 2 GB RAM, 25 GB freier Speicherplatz. Ladet anschließend das aktuelle [VirtualBox Platform Package für Windows Hosts](https://www.virtualbox.org/wiki/Downloads) herunter. Öffnet schließlich die heruntergeladene `.exe`-Datei und folgt dem Installationsassistenten.

        === "macOS"

            Stellt erst einmal sicher, dass Euer Rechner die Mindestanforderungen erfüllt: 2 GHz Dual-Core-Prozessor, 2 GB RAM, 25 GB freier Speicherplatz. Ladet anschließend das aktuelle [VirtualBox Platform Package für OS X Hosts](https://www.virtualbox.org/wiki/Downloads) herunter. Öffnet die heruntergeladene `.dmg`-Datei und zieht das VirtualBox-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das VirtualBox-Symbol in das Andockmenü.

        ### VirtualBox konfigurieren

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Ubuntu herunterladen | Ladet die aktuellste [Ubuntu Long-Term-Support (LTS)-Version](https://ubuntu.com/download/desktop) herunter. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-LTS-Version 22.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle). |
        | Eine virtuelle Maschine erstellen | Startet VirtualBox und klickt auf die `Neu`-Schaltfläche. |
        | Name und Betriebssystem | Gebt Eurer virtuellen Maschine (VM) einen Namen, zum Beispiel `Ubuntu`. Wählt ebenfalls das Betriebssystem (`Linux`) sowie die Version aus, zum Beispiel `Ubuntu (64-bit)`. |
        | Speichergröße | Wählt aus, wie viel Arbeitsspeicher Ubuntu zugewiesen werden soll. 2 GB sind empfohlen, 3-4 GB wären noch besser. |
        | Festplatte |  Wählt den Eintrag `Festplatte erzeugen`, um eine virtuelle Festplatte hinzuzufügen. |
        | Dateityp der Festplatte | Wählt `VDI` als Dateityp für die Festplatte. |
        | Art der Speicherung | Wählt den Eintrag `dynamisch alloziert`, um die Größe der Festplatte festzulegen. |
        | Dateiname und Größe | Gebt an, wo die virtuelle Festplatte erstellt werden soll. Weist Ubuntu genügend Speicherplatz zu, mindestens 10 GB sind empfohlen. |
        | Wahl des optischen Mediums | Klickt vom Hauptbildschirm aus auf `Start`, um die virtuelle Ubuntu-Maschine zu starten. Klickt im erscheinenden Dialogfenster auf das Ordner-Symbol, um ein virtuelles optisches Medium auszuwählen. Ruft den Pfad auf, an dem sich die vorher heruntergeladene LTS Ubuntu `.iso`-Datei befindet. Klickt auf `Öffnen`, `Auswählen` und `Start`. |
        | Booten | Wählt im Boot-Menü den Eintrag `Try or Install Ubuntu` und drückt die `EINGABE`-Taste. Nach der Boot-Phase sollte der Ubuntu-Installationsassistent erscheinen. |

        </center>


        ### Ubuntu in VirtualBox installieren

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Willkommen |Wählt eine Sprache und klickt auf `Ubuntu installieren`. |
        |Tastaturbelegung |Wählt ein Tastaturlayout. |
        |Installation von Ubuntu wird vorbereitet |Wählt zwischen einer `normalen` oder `minimalen` Installation, je nachdem, wie viele Anwendungen Ihr von Anfang an installieren möchtet. <br><br>Aktiviert optional das Kontrollkästchen `Während Ubuntu installiert wird Aktualisierungen herunterladen`, um die Einrichtung nach der Installation zu beschleunigen, sowie das Kontrollkästchen `Software von Drittanbietern installieren`, um von (proprietären) Treibern für die Grafik-Karte, das WiFi, Mediendateien usw. zu profitieren. Klickt dann auf `Weiter`. |
        |Installationsart & Verschlüsselung |Auf diesem Bildschirm könnt Ihr wählen, ob Ihr das vorhandene Betriebssystem löschen und durch Ubuntu ersetzen wollt, oder ob Ihr Ubuntu neben dem vorhandenen Betriebssystem installieren möchtet (sogenanntes "Dual-Booting"). <br><br> Nachdem wir Ubuntu in VirtualBox installieren, ist kein anderes Betriebssystem vorhanden. Wählt daher einfach `Festplatte löschen und Ubuntu installieren`, klickt auf `Erweiterte Optionen` und wählt `LVM bei der neuen Ubuntu-Installation verwenden` sowie `Die neue Ubuntu-Installation zur Sicherheit verschlüsseln`. Klickt anschließend auf `OK` und `Jetzt installieren`. |
        |Wählt einen Sicherheitsschlüssel |Wählt einen [sicheren, individuellen Sicherheitsschlüssel](https://gofoss.net/de/passwords/). Dieser wird bei jedem Neustart Eures Computers zur Entschlüsselung der Festplatte benötigt. Aktiviert ebenfalls das Kontrollkästchen `Freien Speicherplatz überschreiben`, das sorgt für noch mehr Sicherheit. Klickt dann auf `Jetzt installieren`. Klickt auf `Fortfahren`, um das Pop-up-Fenster zu schließen. <br><br> *Vorsicht*: Bei Verlust des Sicherheitsschlüssels gehen alle Daten verloren. Bewahrt ihn daher sicher auf! |
        |Wo befinden Sie sich? |Wählt eine Zeitzone und einen Ort. Klickt anschließend auf `Weiter`. |
        |Wer sind Sie? |Gebt Anmeldeinformationen an, z. B. einen Benutzernamen und ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/). Wählt `Passwort zum Anmelden abfragen` und klickt auf `Weiter`. |
        |Startet den Rechner neu & meldet Euch an |Klickt nach der erfolgreichen Installation auf `Jetzt neu starten`. Die virtuelle Maschine wird neu gestartet. Ihr könnt Euch nun bei Eurer ersten Ubuntu-Sitzung anmelden, indem Ihr den richtigen Sicherheitsschlüssel und das richtige Passwort eingebt. |
        |Ubuntu erforschen| Folgt den Einrichtungsoptionen und erforscht Ubuntu. Schließt die Sitzung, sobald Ihr fertig seid. Von nun an könnt Ihr Ubuntu starten, indem Ihr auf die `Start`-Schaltfläche auf dem Hauptbildschirm von VirtualBox klickt.|

        </center>


    ??? tip "Hier geht's zum 5-minütigen Zusammenfassungsvideo"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ef6ada2-116b-416d-8d53-10a817d95827" frameborder="0" allowfullscreen></iframe>
        </center>


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Ubuntu Installation" width="150px"></img> </center>

## Ubuntu installieren

Gefällt Euch Ubuntu? Möchtet ihr Ubuntu dauerhaft auf Eurem Rechner installieren? Prima! Führt einfach ein paar Vorabprüfungen durch und folgt den nachstehenden Installationsanweisungen.

??? tip "Ein paar Vorabprüfungen"

    <center>

    | Vorabprüfungen | Beschreibung |
    | ------ | ------ |
    | Ist mein Gerät Linux-kompatibel? |• [Testet Ubuntu](https://gofoss.net/de/ubuntu/#ubuntu-live-usb-virtualbox) mit einem Live-USB-Laufwerk oder mit VirtualBox <br>• Überprüft die [Kompatibilitätsdatenbank](https://ubuntu.com/certified) <br>• [Fragt](https://search.disroot.org/) im Internet nach <br>• Kauft einen Linux-kompatiblen Rechner, z.B. über [Linux Preloaded](https://linuxpreloaded.com/) oder [Ministry of Freedom](https://minifree.org/) |
    | Erfüllt mein Gerät die Mindestanforderungen? |• 2 GHz Dual-Core-Prozessor <br>• 4 GB Arbeitsspeicher (RAM) <br>• 25 GB freier Festplattenspeicher (Ubuntu benötigt ca. 5 GB, seht mindestens 20 GB für Eure Daten vor) |
    | Ist mein Gerät eingestöpselt? | Falls Ihr Ubuntu auf einem mobilen Gerät wie z.B. einem Laptop installiert, sollte dieses an die Stromversorgung angeschlossen sein. |
    | Ist das Installationsmedium zugänglich? | Prüft, ob Euer Rechner über ein DVD-Laufwerk oder einen freien USB-Anschluss verfügt. |
    | Hat das Gerät Zugriff aufs Internet? | Prüft, ob die Internetverbindung funktioniert. |
    | Sind Eure Daten gesichert? | [Sichert Eure Daten](https://gofoss.net/de/backups/), da ein (geringes, aber reales) Risiko des Datenverlustes während des Installationsprozesses besteht! |
    | Ist die aktuelle Ubuntu Linux-Version heruntergeladen? | Ladet die aktuellste [Ubuntu Long-Term-Support (LTS)-Version](https://ubuntu.com/download/desktop) herunter. Diese wird 5 Jahre lang unterstützt, einschließlich Sicherheits- und Wartungsupdates. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-LTS-Version 22.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle). |
    | Ist ein bootfähiges Medium vorbereitet? | Verwendet einen [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) oder [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) Rechner, um die heruntergeladene `.iso`-Datei auf eine DVD zu brennen. Alternativ könnt Ihr auch einen [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) oder [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) Rechner verwenden, um ein bootfähiges USB-Laufwerk zu erstellen. |

    </center>


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Booten |Legt die DVD oder das bootfähige USB-Laufwerk ein und startet Euren Rechner neu. Die meisten Rechner booten automatisch von der DVD oder dem USB-Laufwerk. Sollte dies nicht der Fall sein, drückt beim Hochfahren Eures Computers wiederholt auf die Tasten `F12`, `ESC`, `F2` oder `F10`. Dadurch solltet Ihr Zugang zum Boot-Menü Eures Rechners erhalten, in dem Ihr dann die DVD bzw. das USB-Laufwerk als Boot-Laufwerk auswählen könnt. <br><br>Wählt nach dem Booten von der DVD oder dem USB-Laufwerk den Eintrag `Try or Install Ubuntu` und drückt die `EINGABE`-Taste. Wartet, bis Ubuntu vom bootfähigen Laufwerk lädt, das kann eine Weile dauern. |
    |Willkommen |Der Installationsassistent wird angezeigt. Wählt eine Sprache und klickt auf `Ubuntu installieren`. |
    |Tastaturbelegung |Wählt ein Tastaturlayout. |
    |Installation von Ubuntu wird vorbereitet |Wählt zwischen einer `normalen` oder `minimalen` Installation, je nachdem, wie viele Anwendungen Ihr von Anfang an installieren möchtet. Optional: <br>• Aktiviert das Kontrollkästchen `Während Ubuntu installiert wird Aktualisierungen herunterladen`, um die Einrichtung nach der Installation zu beschleunigen <br>• Aktiviert das Kontrollkästchen `Software von Drittanbietern installieren`, um von (proprietären) Treibern für die Grafik-Karte, das WiFi, Mediendateien usw. zu profitieren <br><br> Klickt dann auf `Weiter` |
    |Installationsart & Verschlüsselung |Hier könnt Ihr wählen: <br>• Ob das vorhandene Betriebssystem gelöscht und durch Ubuntu ersetzt werden soll. *Vorsicht*: dies löscht alle Daten auf Eurer Festplatte! Stellt daher sicher, dass Ihr [Eure Daten gesichert habt](https://gofoss.net/de/backups/)! <br>• Oder ob Ubuntu parallel zum vorhandenen Betriebssystem installiert werden soll (sogenanntes "Dual-Booting"). Dies sollte keine Auswirkungen auf die bestehende Konfiguration Eures Rechners haben. Stellt trotzdem sicher, dass Ihr [Eure Daten sichert](https://gofoss.net/de/backups/), man weiß ja nie... <br><br> Um Ubuntu zu verschlüsseln, klickt auf `Erweiterte Optionen` und wählt: <br>• `LVM bei der neuen Ubuntu-Installation verwenden` <br>• `Die neue Ubuntu-Installation zur Sicherheit verschlüsseln` <br><br>Klickt dann auf`OK` und auf `Jetzt installieren`. |
    |Wählt einen Sicherheitsschlüssel |Wählt einen [sicheren, individuellen Sicherheitsschlüssel](https://gofoss.net/de/passwords/). Dieser wird bei jedem Neustart Eures Computers zur Entschlüsselung der Festplatte benötigt. Aktiviert ebenfalls das Kontrollkästchen `Freien Speicherplatz überschreiben`, das sorgt für noch mehr Sicherheit. Klickt dann auf `Jetzt installieren`. Klickt auf `Fortfahren`, um das Pop-up-Fenster zu schließen. <br><br>*Vorsicht*: Bei Verlust des Sicherheitsschlüssels gehen alle Daten verloren. Bewahrt ihn daher sicher auf! |
    |Wo befinden Sie sich? |Wählt eine Zeitzone und einen Ort. Klickt anschließend auf `Weiter`. |
    |Wer sind Sie? |Gebt Anmeldeinformationen an, z. B. einen Benutzernamen und ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/). Wählt `Passwort zum Anmelden abfragen` und klickt auf `Weiter`. |
    |Startet den Rechner neu & meldet Euch an |Das war's. Wartet, bis die Installation abgeschlossen ist, entfernt den USB-Stick und klickt auf `Jetzt neu starten`, sobald Ihr dazu aufgefordert werdet. Meldet Euch nach dem Neustart bei Ubuntu mit Eurem Sicherheitsschlüssel und Passwort an. |

    </center>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252dab-255f-42c7-885e-711d9a24da85" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Ubuntu Desktop" width="150px"></img> </center>

## Der GNOME Desktop

Nachdem Ihr Euch eingeloggt habt, erscheint der sogenannte [GNOME-Desktop](https://www.gnome.org/). Nachfolgend ein kurzer Überblick über die verschiedenen Elemente des GNOME-Desktops.

=== "Menüleiste"

    <center> <img src="../../assets/img/gnome_top_bar.png" alt="GNOME top bar" width="700px"></img> </center>

    Die obere Menüleiste bietet schnellen Zugriff auf:

    * **Aktivitäten**: klickt auf die `Aktivitäten`-Schaltfläche oben links auf dem Bildschirm, um auf offene Fenster und Arbeitsflächen zuzugreifen, oder nach Anwendungen, Dateien und Ordnern zu suchen. Alternativ könnt Ihr auch mit drei Fingern auf dem Touchpad nach oben wischen, um die Aktivitätenübersicht aufzurufen
    * **Uhr, Kalender & Termine**: zeigt die aktuelle Uhrzeit, das Datum, einen Kalender und eine Liste Eurer Termine an
    * **System-Einstellungen**: verwaltet Systemeinstellungen wie Lautstärke, Bildschirmhelligkeit, Netzwerkverbindungen, Stromverbrauch, Mikrofonstatus bei Anrufen, Abmeldungen von Sitzungen usw.

    Ihr könnt ebenfalls eine **Batterieprozentanzeige** über `Einstellungen ‣ Energie ‣ Taste für Bereitschaft und Ausschalten` hinzufügen.


=== "Dock"

    <center> <img src="../../assets/img/gnome_dock.png" alt="GNOME dock" width="300px"></img> </center>

    Standardmäßig wird das Dock auf der linken Seite des Desktops angezeigt:

    * **Angepinnte Apps**: ermöglicht den schnellen Zugriff auf Eure Lieblingsanwendungen. Um eine Anwendung anzupinnen, klickt mit der rechten Maustaste auf das entsprechende Symbol in der App-Schublade oder im Dock und wählt `Zu Favoriten hinzufügen`. Die Symbole im Dock können auch nach Belieben neu anordnen werden
    * **Laufende Apps**: greift auf aktuell laufende Anwendungen zu. Sie werden in der unteren Hälfte des Docks angezeigt
    * **Laufwerke & Papierkorb**: greift auf Eure Laufwerke oder den Papierkorb zu

    Unter `Einstellungen ‣ Darstellung ‣ Dock` könnt Ihr das Erscheinungsbild des Docks weiter abstimmen: verkleinert das Dock, indem Ihr den *Leistenmodus* deaktiviert, verschiebt das Dock an eine andere Stelle auf dem Desktop, blendet das Dock automatisch aus,ändert die Symbolgröße oder blended Laufwerke und den Papierkorb aus.

    Ihr könnt ebenfalls das **Minimieren per Mausklick** aktivieren. Damit könnt Ihr auf ein Anwendungssymbol im Dock klicken, um das entsprechende Fenster zu minimieren oder wiederherzustellen. Führt hierzu einfach den folgenden Befehl im Terminal aus:

    ```bash
    gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
    ```


=== "App-Schublade"

    <center> <img src="../../assets/img/gnome_app_launcher.png" alt="GNOME app launcher" width="700px"></img> </center>

    Die App-Schublade ermöglicht den Zugriff auf alle [installierten Apps](https://gofoss.net/de/ubuntu-apps/):

    * **Öffnet die App-Schublade**: klickt auf das Neun-Punkte-Symbol unten links auf dem Destkop oder führt zwei aufeinander folgende Wischbewegungen mit drei Fingern nach oben auf Eurem Touchpad aus
    * **Blättert durch die App-Schublade**: blättert horizontal durch Eure Anwendungen, entweder mit der Maus oder mit einer Zwei-Finger-Links/Rechts-Wischbewegung auf Eurem Touchpad
    * **Sortier Symbole neu**: verschiebt die Anwendungssymbole per Drag & Drop, um sie nach Euren Wünschen anzuordnen


=== "Desktop"

    <center> <img src="../../assets/img/gnome_desktop.png" alt="GNOME desktop" width="700px"></img> </center>

    Viele Aspekte des GNOME-Desktops können über das Menü `Einstellungen ‣ Darstellung` angepasst werden:

    * **Fenster-Stil**: wählt ein systemweites helles oder dunkles Thema, sowie aus 10 verschiedenen Akzentfarben
    * **Symbole**: passt die Größe oder Position der Destkop-Symbole an und blendet den persönlichen Ordner ein- oder aus
    * **Dateien & Verzeichnisse**: öffnet den Dateimanager, um Dateien oder Ordner per Drag & Drop auf den Desktop zu ziehen

    Ihr könnt Multitasking über das Menü `Einstellungen ‣ Multitasking` feinjustieren:

    * **Funktionale Ecken**: berührt die obere linke Ecke, um die Aktivitäten-Übersicht zu öffnen
    * **Aktive Bildschirmkanten**: zieht Fenster an den oberen, linken und rechten Bildschirmrand, um dessen Größe anzupassen
    * **Arbeitsflächen**: wählt zwischen einer dynamischen oder festen Anzahl von Arbeitsflächen. Um die Arbeitsfläche zu wechseln, ruft die Aktivitäten-Übersicht oder die App-Schublade auf, wie zuvor beschrieben. Alternativ könnt Ihr auch mit drei Fingern nach rechts oder links auf Eurem Touchpad wischen

    Ihr könnt ebenfalls **Bildschirmfotos und -aufzeichnungen** aufnehmen, indem Ihr die `DRUCK`-Taste drückt.

    Schließlich könnt Ihr **Animationen** über `Einstellungen ‣ Barrierefreiheit ‣ Sehen` deaktivieren.


=== "Terminal"

    <center> <img src="../../assets/img/gnome_terminal.png" alt="GNOME terminal" width="700px"></img> </center>

    Das Terminal ist eine Benutzeroberfläche zur Ausführung textbasierter Befehle. Es wird manchmal auch Shell, Konsole, Befehlszeile oder Eingabeaufforderung genannt. Viele neue BenutzerInnen schrecken vor dem Terminal zurück, da es oft mit obskurem Code-Hacking in Verbindung gebracht wird. In Wirklichkeit ist die Verwendung des Terminals gar nicht so schwer und kann deutlich schneller als die Nutzung einer graphischen Oberfläche sein. Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die `Aktivitäten`-Schaltfläche oben links und sucht nach `Terminal`. Hier noch etwas Lesestoff, um ein Terminal-Ninja zu werden:

    * [Spickzettel mit den gängigsten Terminalbefehlen](https://speicher.systemausfall.org/f/28456ff12e7d4b43870e/)
    * [Ein weiterer Spickzettel mit den gängigsten Terminalbefehlen](https://speicher.systemausfall.org/f/3ec10028a0cb482383bd/)
    * [Und noch ein Spickzettel mit häufig genutzten Terminalbefehlen](https://speicher.systemausfall.org/f/85700c3358444ed3942a/)
    * [Ein paar nützliche Ubuntu-Terminalbefehle](https://speicher.systemausfall.org/f/f305c26061d4472b8f9c/)
    * [Vollständiges Buch über die Linux-Kommandozeile](https://speicher.systemausfall.org/f/949ad740507741a5a2b2/)


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="GNOME Software-Center" width="150px"></img> </center>

## Snap entfernen & GNOME Software Center installieren

Ab Ubuntu 20.04 wurde das traditionelle Software-Center durch Snap ersetzt. Bei Snap handelt es sich um eine neue Technologie, die Anwendungen gebündelt in einer einzigen Datei bereitstellt. Falls Ihr es vorziehen solltet, wieder zum GNOME Software-Center zurückzukehren, könnt Ihr den nachstehenden Anweisungen folgen.

??? tip "Snap aus Ubuntu 22.04 LTS entfernen"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt dann folgende Befehle aus, um Snap-Dienste zu stoppen:

    ```bash
    sudo systemctl disable snapd.service
    sudo systemctl disable snapd.socket
    sudo systemctl disable snapd.seeded.service
    ```

    Listet alle Snap-Pakete auf, die auf Eurem Rechner installiert sind:

    ```bash
    snap list
    ```

    Das Ergebnis sollte in etwa folgendermaßen aussehen:

    ```
    Name                        Version             Rev     Tracking            Publisher       Notes
    bare                        1.0                 5       latest/stable       canonical       base
    core20                      20220318            1405    latest/stable       canonical       base
    firefox                     99.0.1-1            1232    latest/stable/...   mozilla         -
    gnome-3-38-2004             0#git.1f9014a       99      latest/stable/...   canonical       -
    gtk-common-themes           0.1-79-ga83e90c     1534    latest/stable/...   canonical       -
    snap-store                  41.3-59-gf884f48    575     latest/stable/...   canonical       -
    snapd                       2.54.4              15177   latest/stable       canonical       snapd
    snapd-desktop-integration   0.1                 10      latest/stable/...   canonical       -
    ```

    Entfernt dann jedes Snap-Paket wie folgt:

    ```bash
    sudo snap remove --purge firefox
    sudo snap remove --purge snap-store
    sudo snap remove --purge gtk-common-themes
    sudo snap remove --purge gnome-3-38-2004
    sudo snap remove --purge snapd-desktop-integration
    sudo snap remove --purge core20
    sudo snap remove --purge bare
    ```

    Löscht mögliche Restdateien:

    ```bash
    sudo rm -rf /var/cache/snapd/
    sudo rm -rf ~/snap
    ```

    Entfernt Snap vollständig aus Ubuntu 22.04 LTS:

    ```bash
    sudo apt autoremove --purge snapd
    ```

    Wenn Ihr verhindern wollt, dass Ubuntu in Zukunft Snap-Pakete installiert, könnt Ihr eine Konfigurationsdatei öffnen:

    ```bash
    sudo gedit /etc/apt/preferences.d/nosnap.pref
    ```

    Fügt dann folgende Zeilen hinzu und speichert die Datei:

    ```bash
    # This file prevents snapd from being installed by apt
    Package: snapd
    Pin: release a=*
    Pin-Priority: -10
    ```

??? tip "Das GNOME Software Center in Ubuntu 22.04 LTS installieren"

    Führt den folgenden Befehl aus, um das GNOME Software Center zu installieren:

    ```bash
    sudo apt update
    sudo apt install gnome-software
    ```

??? tip "Firefox ohne Snap in Ubuntu 22.04 LTS installieren"

    Nach dem Entfernen von Snap kann die Ausführung des Befehls `sudo apt install firefox` die Fehlermeldung `firefox : PreDepends: snapd but it is not installable` zurückgeben. Um [Firefox](https://gofoss.net/de/firefox/) dennoch neu zu installieren, öffnet eine Konfigurationsdatei:

    ```bash
    sudo gedit /etc/apt/preferences.d/firefox-nosnap.pref
    ```

    Fügt dann folgende Zeilen hinzu und speichert die Datei:

    ```bash
    # This file enables re-installing Firefox after removing snapd
    Package: firefox*
    Pin: release o=Ubuntu*
    Pin-Priority: -1
    ```

    Fügt die Mozilla-Team-Paketquellen hinzu:

    ```bash
    sudo add-apt-repository ppa:mozillateam/ppa
    sudo apt update
    ```

    Installiert schließlich Firefox:

    ```bash
    sudo apt install firefox
    ```


??? tip "Snap- und Flatpak-Pakete im GNOME Software Center unterstützen"

    Stellt zunächst sicher, dass Ihr Ubuntu nicht daran hindert, Snap-Pakete zu installieren. Wenn Ihr in einem der vorherigen Schritte die Datei `/etc/apt/preferences.d/nosnap.pref` erstellt habt, entfernt diese wieder:

    ```bash
    sudo rm /etc/apt/preferences.d/nosnap.pref
    ```

    Soll das GNOME Software Center auch Snap- und Flatpak-Pakete unterstützen, führt folgende Befehle aus:

    ```bash
    sudo apt install gnome-software-plugin-snap
    sudo apt install gnome-software-plugin-flatpak
    ```


<br>

<center> <img src="../../assets/img/separator_anon.svg" alt="Ubuntu Privacy" width="150px"></img> </center>

## Ubuntu Datenschutzeinstellungen

Unter `Einstellungen ‣ Datenschutz` können NutzerInnen datenschutzrelevante Aspekte wie Verbindungsprüfungen, Standortdienste, Dateichronik, Zugriff auf externe Geräte, Bildschirmsperren oder Fehlerdiagnosen feinjustieren. Mehr Details unten.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Datenschutzeinstellungen | Beschreibung |
    | ------ | ------ |
    | Konnektivität | Deaktiviert die Konnektivitätsprüfung, um Datenlecks zu vermeiden, falls Eure Netzwerkkommunikation überwacht wird. |
    | Standortdienste | Deaktiviert die Standortdienste, um zu verhindern, dass Anwendungen auf Euren Standort zugreifen können. |
    | Thunderbolt | Ubuntu kann Geräte erkennen, die an die Thunderbolt-Anschlüsse Eures Rechners angeschlossen werden, z. B. Docks oder externe Graphikkarten. Deaktiviert die Funktion, falls Ihr sie nicht nutzt oder nutzen wollt. |
    | Dateichronik & Papierkorb | Deaktiviert die Dateichronik, um zu verhindern, dass Anwendungen auf Eure Datei-Historie zugreifen können. Ihr könnt ebenfalls eine automatische Löschung des Papierkorbs und temporärer Dateien aktivieren, um keine sensiblen Informationen preiszugeben. |
    | Bildschirm | Aktiviert die automatische Bildschirmsperre, um zu verhindern, dass andere Personen in Eurer Abwesenheit auf Euren Rechner zugreifen. |
    | Fehlerdiagnose | Deaktiviert die Diagnosefunktion, damit keine Fehlerberichte an Canonical gesendet werden. |

    </center>


<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Ubuntu Gnome Tweaks" width="150px"></img> </center>

## GNOME Tweaks & Extensions

Wie bereits erwähnt, sind Ubuntu 22.04 und der GNOME-Desktop in hohem Maße anpassbar: Hintergrundbilder, Benachrichtigungen, helle und dunkle Themen, Akzentfarben, Multitasking, Energieprofile usw. [GNOME Tweaks](https://wiki.gnome.org/Apps/Tweaks/) treibt die Anpassungsmöglichkeiten noch weiter voran: Sperrbildschirm-Hintergrund, Position der Fenster-Steuerungsschaltflächen, Startanwendungen, usw. [GNOME Extensions](https://extensions.gnome.org/) hingegen fügen Funktionen wie Wettervorhersage, Netzwerkgeschwindigkeit, Desktop-Viertelkacheln usw. hinzu. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Um GNOME Tweaks & GNOME Extensions zu installieren, öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt dann folgende Befehle aus:

    ```bash
    sudo apt install gnome-tweaks
    sudo apt install gnome-shell-extension-manager
    ```

    Alternativ könnt Ihr auch auf `Aktivitäten` in der oberen Menüleiste klicken und nach dem Eintrag `Software` suchen. Sucht anschließend nach `Gnome Tweaks` sowie `Extension Manager` und klickt auf `Installieren`.


<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Ubuntu-Fehlerberichte" width="150px"></img> </center>

## Ubuntu Fehlerberichte

[Apport](https://wiki.ubuntu.com/Apport/) ist Ubuntus Fehlermeldesystem. Es erfasst Programmabstürze und speichert Fehlerberichte. Aus Datenschutzgründen solltet Ihr Apport jedoch abschalten. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt dann folgende Befehle aus, um die Fehlerberichtsfunktion vollständig zu entfernen:

    ```bash
    sudo rm /etc/cron.daily/apport
    sudo apt purge apport
    ```

    Wollt Ihr hingegen lediglich den automatischen Fehlerbericht deaktivieren, ohne die Funktionalität komplett zu entfernen, müsst Ihr das Terminal aufrufen und den Befehl `sudo service apport stop` eingeben. Öffnen anschließend die Konfigurationsdatei mit dem Befehl `sudo gedit /etc/default/apport` und setzt den Wert `enabled` auf Null, d.h. `enabled=0`.


<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Ubuntu-Codecs" width="150px"></img> </center>

## Ubuntu Codecs

Codecs legen fest, wie Eure Rechner Video- oder Audiodateien lesen sollen. Aus rechtlichen und ethischen Gründen enthalten einige Linux-Distributionen nicht alle Multimedia-Codecs. Um bestimmte Dateiformate richtig anzuzeigen ist es daher manchmal notwendig, zusätzliche Codecs zu installieren. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt die folgenden Befehle aus, um Euer Multimedia-Erlebnis in vollen Zügen zu genießen:

    ```bash
    sudo add-apt-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -cs) partner"
    sudo apt update
    sudo apt install ubuntu-restricted-extras
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Ubuntu-Grafiktreiber" width="150px"></img> </center>

## Ubuntu-Grafiktreiber

Die große Mehrheit der Linux-Distributionen wird mit Open-Source-Grafiktreibern ausgeliefert. Diese reichen meist zur Nutzung von Standardanwendungen aus, sind aber oft ungeeignet für Spiele. Ihr könnt zusätzlich proprietäre Grafiktreiber installieren, indem Ihr auf `Aktivitäten` in der oberen Menüleiste klickt und `Zusätzliche Treiber` eingebt. Wählt anschließend einen Grafiktreiber aus (die Standardoption ist meist geeignet) und startet Euren Rechner neu.


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Ubuntu System Aktualisierungen" width="150px"></img> </center>

## Ubuntu Software aktualisieren

Ihr solltet Ubuntu stets auf dem neuesten Stand halten und regelmäßig Sicherheitskorrekturen, Fehlerbehebungen und Programmaktualisierungen aufspielen. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Führt anschließend den folgenden Befehl aus:

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    Der erste Teil des Befehls `sudo apt update` prüft, ob neue Programmversionen verfügbar sind. Der zweite Teil des Befehls `sudo apt upgrade` installiert die neuesten Updates. Das `-y` am Ende des Befehls gestattet die Installation neuer Pakete.

    Falls Ihr ohne Terminal arbeiten möchtet, könnt Ihr in der oberen Menüleiste auf `Aktivitäten` klicken und nach dem `Software-Updater` suchen. Dieser prüft, ob Aktualisierungen bereitstehen und schlägt vor, diese zu installieren.

    Sobald das System auf den neuesten Stand gebracht wurde, könnt Ihr mit den folgenden Terminalbefehlen das System bereinigen und unnötige Pakete entfernen:

    ```bash
    sudo apt autoremove
    sudo apt autoclean
    sudo apt clean
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Ubuntu Releasewechsel" width="150px"></img> </center>

## Ubuntu Releasewechsel

Neben Software-Aktualisierungen könnt Ihr ebenfalls Releasewechsel vornehmen, zum Beispiel von Ubuntu 20.04 LTS auf 22.04 LTS oder von 22.04 LTS auf 22.10 non-LTS. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Beginnt damit [Eure Daten zu sichern](https://gofoss.net/de/backups/). Öffnet anschließend das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Spielt alle Programmaktualisierungen auf, wie im vorherigen Abschnitt beschrieben:

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    Ruft die aktuell installierte Ubuntu-Version ab:

    ```bash
    lsb_release -a
    ```

    Erlaubt Ubuntu-Releasewechsel:

    ```bash
    sudo gedit /etc/update-manager/release-upgrades
    ```

    Setzt hierzu den Eintrag auf `Prompt=normal`, falls Ihr alle Releasewechsel zulassen möchtet, oder auf `Prompt=lts`, falls Ihr nur LTS-Releasewechsel zulassen wollt.

    Startet Euren Rechner neu. Führt schließlich den Ubuntu-Releasewechsel durch:

    ```bash
    sudo do-release-upgrade -d -f DistUpgradeViewGtk3
    ```

    Folgt dem Upgrade-Assistenten. Etwas Geduld ist gefragt, denn das Upgrade kann eine Weile dauern. Vergewissert Euch, dass Euer Gerät eingesteckt ist, und macht Euch eine frische Kanne Kaffee! Sobald das Upgrade abgeschlossen ist, startet der Rechner neu und Ihr könnt Euch anmelden. Beachtet, dass die Software von Drittanbietern während des Releasewechsels deaktiviert wurde und erneut freigegeben werden muss. Vergewissert Euch abschließend, dass das Upgrade erfolgreich war, indem Ihr die Ubuntu-Version in der Kommandozeile abruft:

    ```bash
    lsb_release -a
    ```


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr in [Ubuntus Dokumentation](https://help.ubuntu.com/), [Ubuntus Tutorials](https://ubuntu.com/tutorials) oder [Ubuntus Wiki](https://wiki.ubuntu.com/). Ihr könnt auch gerne die [beginnerfreundliche Ubuntu-Gemeinschaft](https://ubuntuusers.de/) um Unterstützung bitten.

<div align="center">
<img src="https://imgs.xkcd.com/comics/sandwich.png" alt="Ubuntu Desktop"></img>
</div>

<br>

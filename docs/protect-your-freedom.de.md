---
template: main.html
title: FOSS schützt vor Überwachungskapitalismus
description: Was ist FOSS? Was ist ein Bedrohungsmodell? Was ist der Unterschied zwischen Datenschutz, Anonymität und Sicherheit?
---

# Schützt Eure Daten <br> mit freier und quelloffener Software

!!! level "Letzte Aktualisierung: März 2022"

[gofoss.net](https://gofoss.net/de/) möchte freie, quelloffene und datenschutzfreundliche Software zugänglicher machen. Für BenutzerInnen, die wenig Zeit und Lust zum Herumbasteln haben. Aber auch für ExpertInnen, die mehr über Datenschutz und IT-Sicherheit erfahren möchten.

Die gesamte Webseite ist 100% frei und quelloffen – keine Werbung, kein Tracking, keine Sponsoren. Der Inhalt dürfte all diejenigen interessieren, die:

* ein iPhone, Android-Smartphone oder Windows-, macOS- bzw. ChromeOS-Computer besitzen
* mit Chrome, Safari oder Edge im Internet surfen
* auf Facebook, Twitter, WhatsApp, Tiktok oder Snapchat aktiv sind


<center><img src="../../assets/img/closed_ecosystem.png" alt="Walled garden" width="500px"></img></center>

## Was ist Überwachungskapitalismus?

Vielen ist gar nicht bewusst, dass die oben genannten Dienste von Unternehmen entwickelt werden, die Teil des sogenannten [Überwachungskapitalismus](https://de.wikipedia.org/wiki/%C3%9Cberwachungskapitalismus) sind. Dieser Begriff beschreibt ein Wirtschaftssystem, in dem Technologiemonopole persönliche Nutzerdaten zur Gewinnmaximierung ausbeuten.

Unklar, wo dabei das Problem liegt? Nun ja, der Überwachungskapitalismus bedroht den Kern unserer Gesellschaftsordnung. Er führt zu Massenüberwachung, polarisiert die politische Debatte, mischt sich in Wahlen ein, vereinheitlicht Denkstrukturen, begünstigt die Zensur und fördert geplante Obsoleszenz. Die Bewältigung dieser Probleme erfordert umfassende Änderungen unserer Rechtssysteme und gesellschaftlichen Konventionen.


<center><img src="../../assets/img/foss_ecosystem.png" alt="Freie und quelloffene Software" width="500px"></img></center>

## Wie kann man sich mit FOSS schützen?

Ihr könnt selbst eine Menge tun, um Eure Daten zu schützen und Euer Recht auf Reparatur einzufordern. Das fängt z.B. mit der Nutzung freier und quelloffener Software an, die oft auch [FOSS](https://fsfe.org/freesoftware/freesoftware.de.html) genannt wird. Damit könnt Ihr online surfen, kommunizieren und arbeiten, ohne dass Eure Daten erfasst, zu Geld gemacht oder zensiert werden. Oder alle zwei Jahre neue Geräte kaufen zu müssen.

Es gibt keine Patentlösung: der Wechsel zu freier und quelloffener Software ist ein Lernprozess. Diese Webseite geht schrittweise vor und beginnt mit den Grundlagen, bevor fortgeschrittenere Themen behandelt werden. Dabei wird freie und quelloffene Software grundsätzlich bevorzugt. Werft einen Blick unter die (Rechner-)Haube und entscheidet selbst welche Software Ihr übernehmen, verwerfen oder anpassen wollt.

<center>

| Schritte | Beschreibung |
| ------ | ------ |
|[1. Surft sicher im Netz](https://gofoss.net/de/intro-browse-freely/) | • Wechselt zu [Firefox](https://gofoss.net/de/firefox/) oder [Tor](https://gofoss.net/de/tor/) <br>• Verwendet ein [VPN](https://gofoss.net/de/vpn/), um noch sicherer Online unterwegs zu sein |
|[2. Führt vertrauliche Gespräche](https://gofoss.net/de/intro-speak-freely/) |• Verwendet [verschlüsselte Messenger](https://gofoss.net/de/encrypted-messages/) <br>• Nutzt [verschlüsselte E-Mails](https://gofoss.net/de/encrypted-emails/) |
|[3. Schützt Eure digitalen Daten](https://gofoss.net/de/intro-store-safely/) |• Wählt [sichere Passwörter & Zwei-Faktor-Authentifizierung](https://gofoss.net/de/passwords/) <br> • Erstellt eine [3-2-1 Backup-Strategie](https://gofoss.net/de/backups/) <br>• [Verschlüsselt](https://gofoss.net/de/encrypted-files/) Eure vertraulichen Daten |
|[4. Erschließt das volle Potenzial Eurer Computer](https://gofoss.net/de/intro-free-your-computer/) |• Wechselt zu [Linux](https://gofoss.net/de/ubuntu/) <br>• Bevorzugt [freie & quelloffene Apps](https://gofoss.net/de/ubuntu-apps/) auf Eurem Rechner |
|[5. Bleibt mobil und frei](https://gofoss.net/de/intro-free-your-phone/) |• Nutzt [freie & quelloffene mobile Apps](https://gofoss.net/de/foss-apps/) <br>• Befreit Euer Telefon von Google & Apple mithilfe von [CalyxOS](https://gofoss.net/de/calyxos/) oder [LineageOS](https://gofoss.net/de/lineageos/) |
|[6. Erobert Eure Cloud zurück](https://gofoss.net/de/intro-free-your-cloud/) |• Tretet dem [Fediverse](https://gofoss.net/de/fediverse/) bei, lasst kommerzielle soziale Medien hinter Euch <br>• Nutzt [alternative Cloud-Anbieter](https://gofoss.net/de/cloud-providers/) <br>• Richtet Eure [eigene private Cloud](https://gofoss.net/de/ubuntu-server/) ein <br>• Sichert Euren [Server](https://gofoss.net/de/server-hardening-basics/) & Eure [Cloud-Dienste](https://gofoss.net/de/secure-domain/) ordentlich ab <br>• Greift blitzschnell auf Euren [Cloud-Speicher](https://gofoss.net/de/cloud-storage/) zu <br>• Organisiert und teilt Eure [Fotosammlungen](https://gofoss.net/de/photo-gallery/) <br>• Verwaltet Eure [Kontakte, Kalender & Aufgabenlisten](https://gofoss.net/de/contacts-calendars-tasks/) <br>• Streamt Eure [Filme, TV-Sendungen & Musik](https://gofoss.net/de/media-streaming/) <br>• Legt [Sicherunngskopien Eures Servers](https://gofoss.net/de/server-backups/) an |
|[7. Lernt, netzwerkt und macht mit](https://gofoss.net/de/nothing-to-hide/) |Man lernt nie aus. Über Online-Datenschutz, Datenausbeutung, Filterblasen, Überwachung, Zensur und geplante Obsoleszenz gibt es viel zu sagen. Erfahrt mehr über unser Projekt, macht mit und teilt Eure Wissen mit anderen. |

</center>

<br>
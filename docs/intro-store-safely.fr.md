---
template: main.html
title: Pourquoi la sécurité des données est importante
description: Chiffrer vos données avec VeraCrypt ou Cryptomator. Est-ce que VeraCrypt est sûr ? Comment utiliser VeraCrypt ? Est-ce que Cryptomator est sûr ? Comment utiliser Cryptomator ?
---

# Sécurisez, sauvegardez et chiffrez vos données

<center>
<img align="center" src="../../assets/img/secure_data.png" alt="La sécurité des données" width ="250px"></img>
</center>

Dans ce chapitre, nous allons voir comment:

* [empêcher l'accès non autorisé à vos données](https://gofoss.net/fr/passwords/)
* [établir une bonne méthode de sauvegarde](https://gofoss.net/fr/backups/)
* [chiffrer vos données pour les rendre illisibles](https://gofoss.net/fr/encrypted-files/)


## Nous vivons une ère numérique

L'humanité génère de plus en plus de données. Ces données atteindront les [175 zettaoctets](https://www.forbes.com/sites/tomcoughlin/2018/11/27/175-zettabytes-by-2025/) d'ici à 2025. Ça fait cent cinquante-neuf milliards cent soixante et un millions cinq cent soixante-douze mille huit cent dix téraoctets.

Chaque personne connectée interagit avec des données environ 4 900 fois par jour. Soit une fois toutes les 18 secondes. Vous ne faites pas exception. Une part significative de votre vie est numérisée et stockée partout dans le monde. Vos téléphone, ordinateur, montre et services cloud pourraient en savoir plus sur vous que vos amis les plus proches.


<br>

<center> <img src="../../assets/img/separator_firewall.svg" alt="Mots de passe forts et uniques" width="150px"></img> </center>

## Sécurisez vos données

Utiliser des mots de passe forts et uniques. Éviter d'utiliser un même mot de passe pour différents appareils ou pour différents comptes. Utiliser un gestionnaire de mots de passe. Et envisager la double authentification pour une sécurité renforcée.


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="3-2-1 strategie de sauvegarde" width="150px"></img> </center>

## Sauvegardez vos données

Une bonne méthode de sauvegarde peut vous protéger d'une perte de données, d'un vol, d'une suppression, d'une corruption, d'une fuite, de rançongiciels, ou pire.


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="VeraCrypt et Cryptomator" width="150px"></img> </center>

## Chiffrez vos données

Des données chiffrées sont illisibles, même en cas d'attaque informatique. Par exemple, si un attaquant réussit à pénétrer votre espace de stockage cloud ou obtient un accès non autorisé à votre ordinateur.

<br>

---
template: main.html
title: So benutzt man den Tor Browser
description: Wie benutzt man den Tor Browser? Ist die Nutzung von Tor illegal? Kann Tor zurückverfolgt werden? Gibt es den Tor-Browser für Android?
---

# Bleibt dank Tor anonym im Netz

!!! level "Letzte Aktualisierung: März 2022. Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/tor_connection.png" alt="Tor Browser" width="500px"></img>
</div>

Wollt Ihr anonym im Netz surfen und Eure Identität vor Internetanbietern, Webseitenbetreibern, Werbefirmen oder Überwachungssystem verbergen? Dann ist der [Tor Browser](https://www.torproject.org/de/) das Richtige für Euch!


## Wie funktioniert Tor?

Tor basiert auf einer modifizierten Version des [Firefox ESR-Browsers](https://gofoss.net/de/firefox/) und wird seit 2002 weltweit von Millionen von Menschen genutzt, einschließlich Journalisten und Aktivisten. Tor leitet den Datenverkehr durch ein weltweites Netz aus Tausenden von Computern (sogenannten Relays), bevor dieser sein Ziel erreicht. Herkunft, Ziel oder Inhalt des Datenverkehrs kann somit von niemanden nachvollzogen werden, da alle Informationen verschlüsselt sind und nichts aufgezeichnet wird. Eine aufgerufene Webseite hat lediglich Kenntnis von der IP-Adresse des letzten Relais im Tor-Netz, dem sogenannten Ausgangsknoten. Untenstehend findet Ihr ausführliche Anweisungen zur Installation des Tor Browsers.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

         Ladet das [Tor-Installationsprogramm für Windows](https://www.torproject.org/de/download/) herunter und führt es aus.


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        Ladet das [Tor-Installationsprogramm für OS X](https://www.torproject.org/de/download/) herunter. Öffnet die `.dmg`-Datei und zieht das Tor-Browser-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das Tor-Browser-Symbol in das Andockmenü.


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, öffnet das Terminal mit der Tastenkombination `STRG + ALT + T` oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`.

        ```bash
        sudo apt install torbrowser-launcher
        ```

        Überprüft die Versionsnummer um sicherzustellen, dass Tor korrekt installiert wurde:

        ```bash
        tor --version
        ```

        Das war's, Tor wurde erfolgreich installiert! Klickt auf `Aktivitäten` und sucht nach `Tor-Browser`, oder führt den Terminal-Befehl `torbrowser-launcher` aus, um den Tor-Browser zu starten.


=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Ladet den Tor-Browser von [Google's App Store](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=de_DE/) herunter. Der Tor-Browser kann auch ohne Nutzung eines Google-Kontos heruntergeladen und aktualisiert werden. Ruft dazu die [Tor Downloadseite](https://www.torproject.org/de/download/) von Eurem Android-Gerät aus auf, tippt auf das `Android`-Symbol und ladet die `.apk`-Datei herunter. Ihr könnt ebenfalls einen der alternativen App Stores nutzen: [F-Droid](https://support.torproject.org/de/tormobile/tormobile-7/) oder [Aurora Store](https://auroraoss.com/de/). Mehr dazu in [einem späteren Kapitel](https://gofoss.net/de/foss-apps/)!


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        Ladet den Onion Browser aus dem [App Store](https://apps.apple.com/de/app/onion-browser/id519296448) herunter.

        **Warnhinweis**: Beachtet bitte, dass Apple die Nutzung von Webkit auf iOS vorschreibt. Damit kann der Onion-Browser nicht das gleiche Maß an Datenschutz wie der Tor-Browser bieten.


<div style="margin-top:-20px">
</div>

??? warning "Tor ist super, aber noch lange nicht perfekt"

    Obwohl der Tor Browser zahlreich Vorteile bietet, bringt er doch einige Nachteile mit sich. So kann das Surfen im Netz gegebenenfalls langsamer sein als mit einem "normalen" Browser. Einige bekannte Webseiten blockieren den Tor Browser. In manchen Ländern ist Tor illegal oder wird von der Regierung blockiert. Außerdem garantiert die Verwendung von Tor nicht zwangsmäßig Eure Anonymität oder Sicherheit (ähnliches gilt übrigens auch für [VPN](https://gofoss.net/de/vpn/)). Eure Geräte können beispielsweise nach wie vor für sogenannte [Man-in-the-Middle Angriffe](https://de.wikipedia.org/wiki/Man-in-the-Middle-Angriff), Korrelationsangriffe oder ähnliche Gefahren anfällig sein.


<br>

<center> <img src="../../assets/img/separator_tor.svg" alt="Tor-Anonymitätsprüfung" width="150px"></img> </center>

## Überprüfung der Anonymität

Sobald Ihr Tor installiert habt, ruft eine der folgenden Webseiten auf um zu prüfen, dass keine identifizierenden Informationen durchsickern, wie z.B. Eure IP-Adresse:

* [torproject.org](https://check.torproject.org/)
* [ipleak.net](https://ipleak.net/)
* [coveryourtracks.eff.org](https://coveryourtracks.eff.org/)
* [amiunique.org](https://amiunique.org/)
* [browserleaks.com](https://browserleaks.com/)

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Tor Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr auf dem [Tor Hilfsportal](https://support.torproject.org/de/). Ihr könnt auch gerne die [Tor-Gemeinschaft auf Reddit](https://teddit.net/r/TOR/) um Unterstützung bitten.

<div align="center">
<img src="https://imgs.xkcd.com/comics/incognito_mode.png" alt="Tor Browser"></img>
</div>

<br>

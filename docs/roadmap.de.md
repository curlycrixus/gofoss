---
template: main.html
title: Ein Blick in die Zukunft
description: Der gofoss.net Fahrplan und das Änderungsverzeichnis. Ein Blick in die Zukunft. Ein Blick in den Rückspiegel. Neue Funktionen. Inhaltliche Aktualisierungen. Problembehebungen.
---

# gofoss.net Fahrplan und Änderungsverzeichnis

<center> <img src="../../assets/img/roadmap.png" alt="Fahrplan" width="500px"></img> </center>

Die Webseite wird regelmäßig gepflegt, übersetzt und auf den neuesten Stand gebracht. Kleinere Aktualisierungen umfassen Korrekturen und Wartungsarbeiten, und in größeren Abständen werden neue Funktionen und Inhalte eingeführt.

##Fahrplan

* Spanische Version hinzufügen ([#50](https://gitlab.com/curlycrixus/gofoss/-/issues/50))
* Nachrichten- oder Ankündigungsbereich hinzufügen ([#68](https://gitlab.com/curlycrixus/gofoss/-/issues/68))
* [Material für MkDocs](https://squidfunk.github.io/mkdocs-material/upgrading/) aktualisieren
* [E-Charts](https://echarts.apache.org/en/index.html) aktualisieren
* Leitlinien für Mitwirkende und Entwickler aktualisieren und klarstellen ([#9](https://gitlab.com/curlycrixus/gofoss/-/issues/9), [#10](https://gitlab.com/curlycrixus/gofoss/-/issues/10))
* Session zu verschlüsselten Messengern hinzufügen ([#29](https://gitlab.com/curlycrixus/gofoss/-/issues/29))
* Delta Chat zu verschlüsselten Messengern hinzufügen ([#67](https://gitlab.com/curlycrixus/gofoss/-/issues/67))
* Anleitungen für iOS, macOS und Windows für verschlüsselte Messenger überprüfen ([#65](https://gitlab.com/curlycrixus/gofoss/-/issues/65))
* GPG zum E-Mail-Bereich hinzufügen ([#62](https://gitlab.com/curlycrixus/gofoss/-/issues/62))
* Protonmail-Datenschutzhinweis zum E-Mail-Bereich hinzufügen ([#66](https://gitlab.com/curlycrixus/gofoss/-/issues/66))
* Bitwarden zum Passwortbereich hinzufügen ([#30](https://gitlab.com/curlycrixus/gofoss/-/issues/30))
* Yubikey im Passwortbereich ersetzen ([#63](https://gitlab.com/curlycrixus/gofoss/-/issues/63))
* RaivoOTP zum Passwortbereich hinzufügen ([#71](https://gitlab.com/curlycrixus/gofoss/-/issues/71))
* PinePhone zum Telefonbereich hinzufügen ([#41](https://gitlab.com/curlycrixus/gofoss/-/issues/41))
* GrapheneOS zum Telefonbereich hinzufügen ([#48](https://gitlab.com/curlycrixus/gofoss/-/issues/48))
* /e/ zum Telefonbereich hinzufügen ([#53](https://gitlab.com/curlycrixus/gofoss/-/issues/53))
* FOSS-App-Liste kuratieren ([#64](https://gitlab.com/curlycrixus/gofoss/-/issues/64))
* Ubuntu-Anweisungen auf LTS 22.04 aktualisieren ([#69](https://gitlab.com/curlycrixus/gofoss/-/issues/69))
* Linux Mint & Pop OS! zum Computerbereich hinzufügen ([#49](https://gitlab.com/curlycrixus/gofoss/-/issues/49))
* Abschnitt zur Serversicherung aktualisieren ([#70](https://gitlab.com/curlycrixus/gofoss/-/issues/70))
* Screencasts zu Cron-Jobs aktualisieren ([#17](https://gitlab.com/curlycrixus/gofoss/-/issues/17))
* Nextcloud zum Cloud-Bereich hinzufügen ([#51](https://gitlab.com/curlycrixus/gofoss/-/issues/51))
* Lesbarkeit verbessern ([#15](https://gitlab.com/curlycrixus/gofoss/-/issues/15))
* Defekte Links beheben
* mkdocs-static-i18n-Plugin entfernen ([#5](https://gitlab.com/curlycrixus/gofoss/-/issues/5))
* Mehrsprachige Suche korrigieren ([#4](https://gitlab.com/curlycrixus/gofoss/-/issues/4))


##Änderungsverzeichnis

###Januar 2022

* Zu [https://gofoss.net](https://gofoss.net) migriert ([#80](https://gitlab.com/curlycrixus/gofoss/-/issues/80))
* Quellcode auf Gitlab veröffentlicht ([#57](https://gitlab.com/curlycrixus/gofoss/-/issues/57))
* Lizenz auf AGPLv3 geändert ([#72](https://gitlab.com/curlycrixus/gofoss/-/issues/72))
* Überarbeitetes Erscheinungsbild: Startseite, Schriftarten, Symbole, Mahnungen, png-Dateien, usw. ([#7](https://gitlab.com/curlycrixus/gofoss/-/issues/7), [#14](https://gitlab.com/curlycrixus/gofoss/-/issues/14), [#79](https://gitlab.com/curlycrixus/gofoss/-/issues/79))
* Material für MkDocs aktualisiert ([#34](https://gitlab.com/curlycrixus/gofoss/-/issues/34))
* E-Charts aktualisiert ([#14](https://gitlab.com/curlycrixus/gofoss/-/issues/14))
* Reddit- & Twitter-Links zu Teddit & Nitter migriert ([#13](https://gitlab.com/curlycrixus/gofoss/-/issues/13))
* Dunkelmodus hinzugefügt ([#3](https://gitlab.com/curlycrixus/gofoss/-/issues/3))
* Mehrsprach-Funktion und lokale Versionen hinzugefügt: Englisch, Deutsch & Französisch ([#2](https://gitlab.com/curlycrixus/gofoss/-/issues/2))
* README hinzugefügt ([#36](https://gitlab.com/curlycrixus/gofoss/-/issues/36))
* Beitragsrichtlinien hinzugefügt ([#8](https://gitlab.com/curlycrixus/gofoss/-/issues/8), [#73](https://gitlab.com/curlycrixus/gofoss/-/issues/73), [#74](https://gitlab.com/curlycrixus/gofoss/-/issues/74), [#77](https://gitlab.com/curlycrixus/gofoss/-/issues/77))
* Vorlagen für Issues und Zusammenführungsanfragen hinzugefügt ([#45](https://gitlab.com/curlycrixus/gofoss/-/issues/45))
* Browser-Übersicht zum Firefox-Abschnitt hinzugefügt ([#12](https://gitlab.com/curlycrixus/gofoss/-/issues/12), [#22](https://gitlab.com/curlycrixus/gofoss/-/issues/22))
* VPN-Anbieter hinzugefügt: RiseupVPN, CalyxVPN ([#26](https://gitlab.com/curlycrixus/gofoss/-/issues/26))
* Verschlüsselte Nachrichten-Apps hinzugefügt: Element, Jami, Briar ([#24](https://gitlab.com/curlycrixus/gofoss/-/issues/24), [#27](https://gitlab.com/curlycrixus/gofoss/-/issues/27), [#33](https://gitlab.com/curlycrixus/gofoss/-/issues/33), [#42](https://gitlab.com/curlycrixus/gofoss/-/issues/42))
* FreeFileSync zum Backup-Bereich hinzugefügt ([#21](https://gitlab.com/curlycrixus/gofoss/-/issues/21))
* Cloud-Anbieter hinzugefügt: Chatons, Digitalcourage, Picasoft ([#19](https://gitlab.com/curlycrixus/gofoss/-/issues/19), [#25](https://gitlab.com/curlycrixus/gofoss/-/issues/25), [#31](https://gitlab.com/curlycrixus/gofoss/-/issues/31))
* Fediverse hinzugefügt: Mastodon, PeerTube, PixelFed, Friendica, Lemmy, Funkwhale, usw. ([#40](https://gitlab.com/curlycrixus/gofoss/-/issues/40))
* Electronmail zu Protonmail-Abschnitt hinzugefügt ([#28](https://gitlab.com/curlycrixus/gofoss/-/issues/28))
* PhotoPrism zur Fotogalerie hinzugefügt ([#59](https://gitlab.com/curlycrixus/gofoss/-/issues/59))
* Xiaomi Mi A2 zu CalyxOS hinzugefügt ([#61](https://gitlab.com/curlycrixus/gofoss/-/issues/61))
* Berechtigungen für die Serversicherheit aktualisiert ([#11](https://gitlab.com/curlycrixus/gofoss/-/issues/11))
* Abschnitt Sichere Domäne aktualisiert ([#18](https://gitlab.com/curlycrixus/gofoss/-/issues/18), [#44](https://gitlab.com/curlycrixus/gofoss/-/issues/44))
* Fahrplan und Änderungsverzeichnis aktualisiert ([#58](https://gitlab.com/curlycrixus/gofoss/-/issues/58), [#38](https://gitlab.com/curlycrixus/gofoss/-/issues/38), [#75](https://gitlab.com/curlycrixus/gofoss/-/issues/75))
* FOSS-App-Liste kuratiert ([#23](https://gitlab.com/curlycrixus/gofoss/-/issues/23), [#47](https://gitlab.com/curlycrixus/gofoss/-/issues/47))
* Ubuntu-App-Liste kuratiert ([#39](https://gitlab.com/curlycrixus/gofoss/-/issues/39), [#43](https://gitlab.com/curlycrixus/gofoss/-/issues/43), [#46](https://gitlab.com/curlycrixus/gofoss/-/issues/46), [#56](https://gitlab.com/curlycrixus/gofoss/-/issues/56))
* Dankesrubrik kuratiert ([#37](https://gitlab.com/curlycrixus/gofoss/-/issues/37))
* Cron-Jobs für dehydrated & ClamAV korrigiert ([#16](https://gitlab.com/curlycrixus/gofoss/-/issues/16))
* Digitalcourage DNS korrigiert ([#20](https://gitlab.com/curlycrixus/gofoss/-/issues/20))
* Fehlerhafte Links und veraltete Dienste behoben ([#6](https://gitlab.com/curlycrixus/gofoss/-/issues/6))
* Fehlerhafte Tabellenüberschriften behoben ([#35](https://gitlab.com/curlycrixus/gofoss/-/issues/35))


###Mai 2021

* Von Docsify zu Material für MkDocs migriert
* Screencasts hinzugefügt
* Links zu sozialen Medien hinzugefügt ([#13](https://gitlab.com/curlycrixus/gofoss/-/issues/13))
* Erscheinungsbild überarbeitet: Startseite, Menüführung, Tabellen, Farben, Schriftarten, Abschnitte, usw.
* E-Charts aktualisiert
* 3-Sterne-System für Anfänger, Fortgeschrittene und Techies neu gestaltet ([#54](https://gitlab.com/curlycrixus/gofoss/-/issues/54))
* Alle Bereiche der Webseite aktualisiert
* Arkenfox zum Abschnitt user.js hinzugefügt
* Einzelheiten zur Verschlüsselung von Protonmail hinzugefügt ([#52](https://gitlab.com/curlycrixus/gofoss/-/issues/52))
* Ubuntu-App-Liste kuratiert
* Abschnitt Sichere Domäne hinzugefügt
* Dankesrubrik kuratiert
* Fahrplan hinzugefügt
* Links zu CDNs entfernt
* NordVPN entfernt ([#32](https://gitlab.com/curlycrixus/gofoss/-/issues/32))


###Dezember 2020

* Visuelle Identität überarbeitet
* E-Charts aktualisiert
* Webseiten-Inhalt aktualisiert


###August 2020

* Erste Veröffentlichung von [https://gofoss.today](https://gofoss2020.netlify.app/#/)


<br>
---
template: main.html
title: So benutzt man ein VPN
description: Wie funktioniert ein VPN? Wie benutzt man ein VPN? Ist die Nutzung eines VPN illegal? Kann ein VPN zurückverfolgt werden? Ist VPN kostenlos? VPN für Ubuntu. Kostenloses VPN. VPN Bedeutung.
---

# Wie man VPN verwendet <br> (zum Verbergen Eures Standorts & mehr)

!!! level "Letzte Aktualisierung: März 2022. Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/vpn_connection.png" alt="VPN" width="500px"></img>
</div>

Der Begriff [Virtual Private Network](https://de.wikipedia.org/wiki/Virtual_Private_Network) (VPN) bezieht sich auf eine Form der Verschlüsselung auf der Transportebene. Das bringt mehrere Vorteile mit sich, wie z.B. das Verbergen Eures Standorts, um auf geoblockierte Webseiten zuzugreifen. Wenn Ihr eine Verbindung zu einem VPN herstellt, wird der von Eurem Gerät gesendete oder empfangene Datenverkehr durch eine Art verschlüsselten Tunnel geleitet. Falls jemand mithört, z. B. Euer Internetdienstanbieter oder ein Angreifer, kann dieser nur feststellen, dass Ihr Daten übertragt. Dabei sind weder Ziel noch Inhalt der Daten bekannt. Online-Anbieter, mit denen Ihr interagiert — z.B. Suchmaschinen, Banken oder E-Mail-Anbieter - können Eure Daten jedoch einsehen, speichern und verändern. Das gilt auch für Euren VPN-Anbieter.

Wählt einen vertrauenswürdigen VPN-Anbieter, um Eure Netzaktivitäten zu schützen. Es gibt dafür zahlreiche VPN-Vergleichsportale und -Leitfäden. Ihr könnt Euch ebenfalls mit Fragen an die [Reddit VPN-Gemeinschaft](https://teddit.net/r/VPN/) wenden. Folgende Anbieter haben einen guten Ruf in Bezug auf Standort, Protokollierung, Bezahlung, Sicherheitsniveau, Verfügbarkeit, Preise und Ethik.

??? warning "Kann ein VPN meinen Standort verbergen? Und kann ein VPN zurückverfolgt werden?"

    Die Verwendung eines VPNs bedeutet nicht unbedingt, dass Ihr anonym oder sicher seid (ähnliches gilt übrigens auch für [Tor](https://gofoss.net/de/tor/)). Auf einigen Websites wird zum Beispiel erklärt, wie man seinen Standort mit Hilfe eines VPNs versteckt. Eure Geräte können jedoch immer noch für Browser-Fingerprinting, Man-in-the-Middle-Angriffe, Korrelationsangriffe oder andere Sicherheitslücken anfällig sein. [Ein weiteres Kapitel befasst sich mit der Einrichtung von OpenVPN, damit Ihr mit Eurem eigenen Server kommunizieren könnt](https://gofoss.net/de/secure-domain/). Beachtet bitte, dass eine solche Einrichtung nicht zum Schutz Eurer Online-Identität gedacht ist.


<br>

<center> <img src="../../assets/img/separator_protonvpn.svg" alt="Proton VPN" width="150px"></img> </center>

## ProtonVPN

[ProtonVPN](https://protonvpn.com/de/) gibt an, das einzige kostenfreie VPN ohne Werbung und ohne Geschwindigkeitsbegrenzung zu sein. Es bietet eine Vielzahl von Funktionen, wie z. B. einen "Kill Switch" und DNS-Leckschutz, Split-Tunneling oder Verkehrsverschlüsselung über zwei Standorte.

ProtonVPN hat seinen Sitz in der Schweiz, außerhalb der [Vierzehn-Augen-Länder](https://de.wikipedia.org/wiki/UKUSA-Vereinbarung). Das Unternehmen gibt an, ein [protokollfreier VPN-Anbieter](https://protonvpn.com/de/privacy-policy) zu sein. Einzige Ausnahme ist die Speicherung eines Zeitstempels des letzten erfolgreichen Anmeldeversuchs, um Benutzerkonten vor Brute-Force-Angriffen zu schützen.

Das [Client-Programm ist quelloffen](https://github.com/ProtonVPN/) und für alle einsehbar. Die Android-App [enthält keine Tracker und erfordert 7 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/ch.protonvpn.android/latest/). ProtonVPN verwendet Berichten zufolge [dedizierte Server](https://protonvpn.com/de/vpn-servers/).

Die kostenfreie Version von ProtonVPN beinhaltet allerdings einige Einschränkungen: 1 VPN-Verbindung, Verbindungsmöglichkeiten in nur 3 Ländern und mittelmäßige Geschwindigkeit. Der Zugriff auf zusätzliche Funktionen erfordert ein kostenpflichtiges Konto, das zum Zeitpunkt der Abfassung dieses Textes bei etwa 4 €/Monat beginnt. ProtonVPN akzeptiert anonyme Zahlungen.


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Mullvad Mozilla VPN" width="150px"></img> </center>

## Mozilla / Mullvad

[Mullvad](https://mullvad.net/de/) gibt an, ein schnelles, vertrauenswürdiges und benutzerfreundliches VPN zu sein. Mullvad betreibt auch [Mozillas VPN-Lösung](https://www.mozilla.org/de/products/vpn/).

Das Unternehmen hat seinen Sitz in Schweden, das zu den [Vierzehn-Augen-Ländern](https://de.wikipedia.org/wiki/UKUSA-Vereinbarung) gehört. Mullvad gibt an, ein [protokollfreier VPN-Anbieter](https://mullvad.net/de/help/no-logging-data-policy/) zu sein.

Das [Client-Programm ist quelloffen](https://github.com/mullvad/) und für alle einsehbar. Die Android-App [enthält keine Tracker und erfordert 4 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/net.mullvad.mullvadvpn/latest/). Mullvad [betreibt Server auf der ganzen Welt](https://mullvad.net/de/servers/), die meisten davon sind jedoch gemietet.

Zum Zeitpunkt der Abfassung dieses Textes beginnen die Abonnements bei etwa 5 €/Monat. Mullvad akzeptiert anonyme Zahlungen und erfordert keine E-Mail zur Anmeldung.


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="RiseupVPN" width="150px"></img> </center>

## RiseupVPN

[RiseupVPN](https://riseup.net/de/vpn) gibt an, Zensurumgehung, Standort-Anonymisierung und Verkehrsverschlüsselung anzubieten.

RiseUp ist ein ehrenamtlich geführtes Kollektiv, das 1999 gegründet wurde. Die in Seattle ansässige Organisation befindet sich in einem [Vierzehn-Augen-Land](https://de.wikipedia.org/wiki/UKUSA-Vereinbarung). RiseUp gibt an, ein [protokollfreier VPN-Anbieter](https://riseup.net/de/privacy-policy) zu sein.

Das Client-Programm von RiseupVPN basiert auf der quelloffenen [Bitmask](https://bitmask.net/). Die Android-App [enthält keine Tracker und erfordert 8 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/se.leap.riseupvpn/latest/).

Zum Zeitpunkt der Abfassung dieses Artikels war RiseupVPN kostenfrei. Der Dienst wird vollständig durch Spenden finanziert.


<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="CalyxVPN" width="150px"></img> </center>

## CalyxVPN

[CalyxVPN](https://calyx.net/) wird vom Calyx Institute, einer 2010 gegründeten gemeinnützigen Organisation, zur Verfügung gestellt. Sie befindet sich in einem [Vierzehn-Augen-Land](https://de.wikipedia.org/wiki/UKUSA-Vereinbarung).

In der [Datenschutzrichtlinie](https://calyxinstitute.org/legal/privacy-policy/) des Calyx-Instituts heißt es, dass IP-Adressen zwar protokolliert, aber regelmäßig gelöscht werden.

Das Client-Programm von CalyxVPN basiert auf der quelloffenen [Bitmask](https://bitmask.net/). Die Android-App [enthält keine Tracker und erfordert 8 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/org.calyxinstitute.vpn/latest/).

Zum Zeitpunkt der Abfassung dieses Artikels war CalyxVPN kostenfrei.


<br>

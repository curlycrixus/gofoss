---
template: main.html
title: So installiert Ihr CalyxOS
description: Degoogelt Eure Android-Telefone. Wie installiert man Calyxos? Was ist microG? Ist microG legal? Lineageos vs. Calyxos? Grapheneos vs. Calyxos?
---

# CalyxOS, ein datenschutzfreundliches mobiles Betriebssystem

!!! level "Letzte Aktualisierung: März 2022. Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/calyxos.png" alt="CalyxOS installieren" width ="600px"></img>
</center>


Ähnlich wie [LineageOS](https://gofoss.net/de/lineageos/) ist CalyxOS ein datenschutzfreundliches mobiles Android-Betriebssystem, das ohne Googles Play Services auskommt. [CalyxOS](https://calyxos.org) setzt auf verschlüsselte Kommunikation, privates Surfen mit Tor und DuckDuckGo, verifizierten Systemstart und vieles mehr. Die Nutzung von [microG](https://microg.org/) ist optional und ermöglicht es, einige von Googles proprietären Bibliotheken durch freien und quelloffenen Code zu ersetzen.

CalyxOS ist derzeit für die Pixel-Telefonreihe sowie das Xiaomi Mi A2 verfügbar. Solltet Ihr eines dieser Telefone besitzen, stellt sicher, dass *Euer Handymodell* in der [CalyxOS Geräteliste](https://calyxos.org/get/) aufgeführt ist. Stellt außerdem sicher, dass der sogenannte Bootloader Eures Telefons (vorübergehend) "entsperrbar" ist. In den USA z.B. kann der Bootloader der Pixel-Telefone von Verizon nicht entsperrt werden, sodass CalyxOS nicht installiert werden kann.


!!! warning "Ein paar Anmerkungen zu Kompatibilität und Sicherheit"

    Bitte seid vorsichtig, wenn Ihr CalyxOS auf Euer Telefon aufspielt. Dadurch erlischt die Garantie Eures Geräts, das im schlimmsten Fall sogar unbrauchbar werden kann. Ihr solltet Euch daher des Risikos für eventuelle Schäden oder Datenverluste bewusst sein.

    Bevor Ihr auf CalyxOS umsteigt, bedenkt bitte dass einige Apps [nicht voll funktionsfähig](https://github.com/microg/GmsCore/wiki/Implementation-Status) sind — auch wenn sonst alles in allem hervorragend funktioniert. Darunter befinden sich ein paar Apps von Google, wie z. B. Android Wear, Google Fit, Google Cast oder Android Auto. Zum Glück gibt es dafür großartige [FOSS-Alternativen](https://gofoss.net/de/foss-apps/). Bedenkt schließlich auch, dass die Verwendung von kostenpflichtigen Apps ohne Googles Play Store ein bisschen aufwendiger sein kann.

    CalyxOS gilt sicherer als [LineageOS](https://gofoss.net/de/lineageos/). CalyxOS ist allerdings in der Vergangenheit mit Sicherheitsupdates in Verzug geraten. Wir empfehlen, regelmäßig auf [Updates zu prügen](https://calyxos.org/get/ota/). Proprietäre Komponenten wie Bootloader oder Firmware werden für bestimmte Handys, wie das Xiaomi Mi A2 oder das Pixel 2 und 2 XL, gar nicht mehr aktualisiert. Schließlich gab es auch Berichte über mögliche Sicherheitslücken, wie z. B. Lecks in der Datura-Firewall (z. B. [mobile Datenlecks beim Trennen der WiFi-Verbindung](https://gitlab.com/CalyxOS/calyxos/-/issues/572) oder [DNS-Lecks bei aktiviertem Private DNS und deaktiviertem VPN](https://gitlab.com/CalyxOS/calyxos/-/issues/581)). Bewertet diese potenziellen Sicherheitslücken im Hinblick auf Euer [Bedrohungsmodell](https://ssd.eff.org/en/module/your-security-plan/).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Sicherungskopie" width="150px"></img> </center>

## Sicherungskopien

Während des Installationsvorgangs werden alle Daten auf Eurem Gerät gelöscht. Geht kein Risiko ein, [legt Sicherheitskopien an](https://gofoss.net/de/backups/)!

<br>


<center> <img src="../../assets/img/separator_bug.svg" alt="CalyxOS flashen" width="150px"></img> </center>

## Vorbereitung

### Firmware

Meldet Euch an Eurem Computer an und ladet die [CalyxOS-Firmware](https://calyxos.org/get/) herunter. Stellt sicher, dass Ihr die *Eurem Handymodell* entsprechende `.zip`-Datei herunterladen. Der Dateiname sollte in etwa folgendermaßen lauten: `XXX-factory-2021.XX.XX.XX.zip`.


### Flashen

Um die CalyxOS-Firmware auf Euer Telefon zu übertragen — oder zu *flashen* —, ist ein sogenannter Geräteflasher erforderlich. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        Ladet die aktuellste Version des [Geräteflashers für Windows](https://github.com/AOSPAlliance/device-flasher/releases/) auf Euren Computer herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `device-flasher.exe`. Installiert ebenfalls den [USB-Treiber von Google](https://developer.android.com/studio/run/win-usb/). Hier eine Anleitung für Windows 10:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Schließt das Android-Gerät per USB an den Computer an. |
        | 2 | Öffnet im Windows Explorer die `Computerverwaltung`. |
        | 3 | Wählt im linken Bereich der Computerverwaltung den Eintrag `Gerätemanager`. |
        | 4 | Sucht im rechten Bereich des Geräte-Managers nach dem Eintrag `Tragbare Geräte` oder `Andere Geräte` und erweitert diesen. |
        | 5 | Klickt mit der rechten Maustaste auf den Namen Eures Geräts und wählt `Treibersoftware aktualisieren`. |
        | 6 | Wählt im Assistenten für die Hardware-Aktualisierung die Option `Meinen Computer nach Treibersoftware durchsuchen` und klickt auf `Weiter`. |
        | 7 | Klickt auf `Durchsuchen` und sucht den Ordner mit dem Google USB-Treiber. Der Pfad sollte in etwa `android_sdk\extras\google\usb_driver\` lauten. |
        | 8 | Klickt auf `Weiter`, um den Treiber zu installieren. |

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        Ladet die aktuellste Version des [Geräteflashers für macOS](https://github.com/AOSPAlliance/device-flasher/releases/) auf Euren Computer herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `device-flasher.darwin`.


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Ladet die aktuellste Version des [Geräteflashers für Linux](https://github.com/AOSPAlliance/device-flasher/releases/) auf Euren Computer herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `device-flasher.linux`.



### USB-Debuggen & OEM-Entsperrung

Wie bereits erwähnt, muss der Bootloader des Pixel "entsperrbar" sein. Ihr müsst mit anderen Worten in der Lage sein, die *OEM-Entsperrungsfunktion* Eures Telefons zu aktivieren. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Schritte | Beschreibung |
    | :------: | ------ |
    | 1 | Entnehmt die SIM-Karte und stellt eine Verbindung zu einem WiFi-Netzwerk her. |
    | 2 | Öffnet das Menü `Einstellungen ► Über das Telefon`. |
    | 3 | Tippt siebenmal auf `Build-Nummer`, um die Entwickleroptionen zu aktivieren. |
    | 4 | Öffnet das Menü `Einstellungen ► System ► Erweiterte Einstellungen ► Entwickleroptionen`. |
    | 5 | Scrollt nach unten, und aktiviert das Kontrollkästchen `Android-Debugging` oder `USB-Debugging`. |
    | 6 | Aktiviert `OEM-Entsperrung` im Menü `Entwickleroptionen`. |
    | 7 | Schaltet Euer Telefon aus. Startet das Telefon neu im sogenannten *Bootloader*- oder *Fastboot*-Modus, indem Ihr die beiden Tasten `Lautstärke AB` und `EIN/AUS` gedrückt haltet. Lasst sie los, sobald das Wort `FASTBOOT` auf dem Bildschirm erscheint. |
    | 8 | Schließt Euer Android-Gerät an den Computer an. |
    | 9 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

    </center>

<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="CalyxOS installieren" width="150px"></img> </center>


## Installation

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        <center>

        | Anweisungen | Beschreibung |
        | :------: | ------ |
        |Dateien vorbereiten |Legt auf Eurem Computer die zuvor heruntergeladene CalyxOS-Firmware-Datei `XXX-factory-2021.XX.XX.XX.zip` sowie die Datei `device-flasher.exe` im selben Ordner ab. |
        |Flasher ausführen |Führt den Flasher mit einem Doppelklick aus und folgt den Anweisungen auf dem Bildschirm. Alternativ könnt Ihr auch eine Eingabeaufforderung öffnen, das Flash-Programm mit dem Befehl `.\device-flasher.exe` ausführen und den Anweisungen auf dem Bildschirm folgen. |
        | Bootloader entsperren | Wählt mit Hilfe der Lautstärke- sowie Einschalttasten den Menü-Eintrag `Bootloader entsperren` aus. |
        | Flashvorgang abwarten | Das Telefon wird geflasht und startet dabei mehrfach neu. Das kann ein wenig dauern *Bleibt geduldig und steckt auf keinen Fall das Telefon ab!* |
        | Bootloader sperren | Sobald die Anweisungen auf dem Bildschirm Euch dazu auffordern könnt Ihr mit Hilfe der Lautstärke- sowie Einschalttasten den Menü-Eintrag `Bootloader sperren` auswählen. |
        | Bootvorgang abwarten | Wartet bis das Ende des Flash-Vorgangs angezeigt wird. Das Telefon sollte nun neu starten, das kann wieder eine Weile dauern. |
        | Glückwunsch! | Ihr habt CalyxOS installiert! Vergesst nicht den Eintrag `OEM-Freischaltung` im Menü `Entwickleroptionen` zu deaktivieren. |

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        <center>

        | Anweisungen | Beschreibung |
        | :------: | ------ |
        |Dateien vorbereiten |Legt auf Eurem Computer die zuvor heruntergeladene CalyxOS-Firmware-Datei `XXX-factory-2021.XX.XX.XX.zip` sowie die Datei `device-flasher.darwin` im selben Ordner ab. |
        |Flasher ausführen |Führt den Flasher mit einem Doppelklick aus und folgt den Anweisungen auf dem Bildschirm. Alternativ könnt Ihr auch ein Terminal öffnen, das Flash-Programm mit dem Befehl `chmod +x ./device-flasher.darwin; ./device-flasher.darwin` ausführen und den Anweisungen auf dem Bildschirm folgen. <br><br>*Hinweis*: möglicherweise müsst Ihr den [macOS Gatekeeper](https://support.apple.com/de-de/HT202491) deaktivieren, damit die Installation erfolgreich abgeschlossen werden kann. |
        | Bootloader entsperren | Wählt mit Hilfe der Lautstärke- sowie Einschalttasten den Menü-Eintrag `Bootloader entsperren` aus. |
        | Flashvorgang abwarten | Das Telefon wird geflasht und startet dabei mehrfach neu. Das kann ein wenig dauern *Bleibt geduldig und steckt auf keinen Fall das Telefon ab!* |
        | Bootloader sperren | Sobald die Anweisungen auf dem Bildschirm Euch dazu auffordern könnt Ihr mit Hilfe der Lautstärke- sowie Einschalttasten den Menü-Eintrag `Bootloader sperren` auswählen. |
        | Bootvorgang abwarten | Wartet bis das Ende des Flash-Vorgangs angezeigt wird. Das Telefon sollte nun neu starten, das kann wieder eine Weile dauern. |
        | Glückwunsch! | Ihr habt CalyxOS installiert! Vergesst nicht den Eintrag `OEM-Freischaltung` im Menü `Entwickleroptionen` zu deaktivieren. |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | :------: | ------ |
        |Dateien vorbereiten |Legt auf Eurem Computer die zuvor heruntergeladene CalyxOS-Firmware-Datei `XXX-factory-2021.XX.XX.XX.zip` sowie die Datei `device-flasher.linux` im selben Ordner ab. |
        |Flasher ausführen |Führt den Flasher mit einem Doppelklick aus und folgt den Anweisungen auf dem Bildschirm. Alternativ könnt Ihr auch ein Terminal öffnen, das Flash-Programm mit dem Befehl `sudo chmod +x ./device-flasher.linux; sudo ./device-flasher.linux` ausführen und den Anweisungen auf dem Bildschirm folgen. |
        | Bootloader entsperren | Wählt mit Hilfe der Lautstärke- sowie Einschalttasten den Menü-Eintrag `Bootloader entsperren` aus. |
        | Flashvorgang abwarten | Das Telefon wird geflasht und startet dabei mehrfach neu. Das kann ein wenig dauern *Bleibt geduldig und steckt auf keinen Fall das Telefon ab!* |
        | Bootloader sperren | Sobald die Anweisungen auf dem Bildschirm Euch dazu auffordern könnt Ihr mit Hilfe der Lautstärke- sowie Einschalttasten den Menü-Eintrag `Bootloader sperren` auswählen. |
        | Bootvorgang abwarten | Wartet bis das Ende des Flash-Vorgangs angezeigt wird. Das Telefon sollte nun neu starten, das kann wieder eine Weile dauern. |
        | Glückwunsch! | Ihr habt CalyxOS installiert! Vergesst nicht den Eintrag `OEM-Freischaltung` im Menü `Entwickleroptionen` zu deaktivieren. |

        </center>


<br>

<center> <img src="../../assets/img/separator_microg.svg" alt="microG" width="150px"></img> </center>

## microG

[microG](https://calyxos.org/tech/microg/) ersetzt einige von Googles proprietären Bibliotheken und Anwendungen mit quelloffenen Code. Durch die Aktivierung von microG können Funktionen wie Push-Nachrichten oder Standortbestimmung genutzt werden, ohne das datenhungrige Google Services Framework (GSF) zu installieren. Viele GSF-abhängige Apps können auf diese Weise mit CalyxOS verwendet werden. Es gilt jedoch zu beachten, dass diese Apps noch immer in der Lage sind, Google-Dienste zu kontaktieren. Um Datenlecks weitestgehend einzuschränken, empfehlen wir GSF-unabhängige, [freie und quelloffene Apps](https://gofoss.net/de/foss-apps/) zu bevorzugen.


=== "Standortbestimmung"

    Standardmäßig ist CalyxOS so konfiguriert, dass es Standortinformationen von Mozilla verwendet. Falls Ihr stattdessen eine lokale Datenbank mit Funkmast-Informationen verwenden und völlig unabhängig von Drittanbietern sein wollt, folgt den unten aufgeführten Anweisungen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet F-Droid und installiert die App namens `Lokaler GSM-Standort`. |
        | 2 | Ruft die microG-Einstellungs-App auf. Wählt den Menüeintrag `Standortdienste` und aktiviert `GSM Standortdienst` sowie `Nominatim`. |
        | 3 | Wählt den Menüeintrag `Selbstkontrolle` und überprüft, ob alles richtig eingerichtet ist. |

        </center>


=== "Push-Nachrichten"

    CalyxOS vermeidet die Nutzung von Googles Diensten soweit möglich. Einzige Ausnahme: Viele Apps verlassen sich auf Google Cloud Messaging (GCM), ein von Google entwickeltes proprietäres System, um Push-Nachrichten an Euer Gerät zu senden. microG kann den Zugriff auf Push-Nachrichten ermöglichen, indem es die (eingeschränkte) Nutzung des GCM-Dienstes aktiviert. Untenstehend findet Ihr weitere Details zum Thema.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet die microG-Einstellungs-App. |
        | 2 | Wählt den Menüeintrag `Google Geräte-Registrierung` und registriert Euer Gerät. |
        | 3 | Wählt den Menüeintrag `Google Cloud Messaging` und aktiviert Push-Nachrichten. |

        </center>

    ??? question "Kann ich CalyxOS vertrauen, obwohl es Google Cloud Messaging verwendet?"

        Die Registrierung Eures Geräts und die Aktivierung von Push-Nachrichten ist optional. Dadurch werden möglicherweise begrenzt Daten an Google übermittelt, wie z. B. eine eindeutige ID. microG stellt jedoch sicher, dass so viele identifizierende Informationen wie möglich entfernt werden. Die Aktivierung von Push-Nachrichten kann Google auch ermöglichen, den Inhalt Eurer Nachrichten (teilweise) zu lesen, je nachdem, wie Apps Google Cloud Messaging verwenden.


=== "F-Droid"

    [F-Droid](https://f-droid.org/de/) ist ein App-Store, der ausschließlich freie und quelloffene Anwendungen anbietet, wie in einem [vorherigen Kapitel über FOSS-Apps](https://gofoss.net/de/foss-apps/) beschrieben. Stellt sicher, dass Ihr die microG-Paketquelle in F-Droid aktiviert. Folgt dazu den untenstehenden Anweisungen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        |1 |Startet F-Droid. |
        |2 |Wählt den Menüeintrag `Einstellungen ► Paketquellen`.  |
        |3 |Aktiviert die microG-Paketquelle. |

        </center>


=== "Kostenpflichtige Apps"

    Die Einrichtung von kostenpflichtigen Apps ohne Googles Play Store kann ein wenig knifflig sein. Hier ein Lösungsansatz:

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        |1 |Stöbert durch [Googles Online-Playstore](https://play.google.com/store?hl=de). |
        |2 |Kauft Apps mit einem ausgedienten oder wegwerfbaren Google-Konto. |
        |3 |Meldet Euch bei [Aurora store](https://f-droid.org/de/packages/com.aurora.store/) mit demselben Google-Konto an.|
        |4 |Ladet die erworbenen Apps herunter. |

        </center>

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten oder Antworten findet Ihr in der [CalyxOS-Dokumentation](https://calyxos.org/install/) oder bei der [CalyxOS-Gemeinschaft](https://calyxos.org/community/).

<div align="center">
<img src="https://imgs.xkcd.com/comics/xkcd_phone.png" alt="CalyxOS"></img>
</div>

<br>

---
template: main.html
title: Eine Beifallsrunde
description: Dank geht an unsere Lieben. Dank geht an alle DatenschutzwächterInnen, freie und quelloffene Softwareschmiede und selbstverwaltende ServermagierInnen. Dank geht an engagierte ProgramiererInnen. Dank geht an Open-Source-KünstlerInnen.
---

# Dank an die FOSS-Gemeinschaft

<div align="center">
<img src="../../assets/img/thanks.png" alt="Dankeschön" width="600px"></img>
</div>

<br>

Dank geht an unsere Lieben für ihre Geduld und Unterstützung.

Dank geht an die Datenschutz-, FOSS- und Selbst-Hosting-Gemeinschaft:

* [/r/degoogle](https://teddit.net/r/degoogle/)
* [/r/privacy](https://teddit.net/r/privacy/)
* [/r/fossdroid](https://teddit.net/r/fossdroid/)
* [/r/selfhosted](https://teddit.net/r/selfhosted/)
* [degoogle (Josh Moore)](https://degoogle.jmoore.dev/)
* [itsfoss](https://itsfoss.com/)
* [framasoft](https://framasoft.org/de/)
* [chatons](https://www.chatons.org/)
* [disroot](https://disroot.org/de)
* [digitalcourage](https://digitalcourage.de/)
* [riseup](https://riseup.net/de)
* [privacytools](https://www.privacytools.io/)
* [systemausfall](https://systemausfall.org/)
* [picasoft](https://picasoft.net/)
* [letsdebugit](https://www.letsdebug.it/)
* [free software directory](https://directory.fsf.org/wiki/Main_Page)
* [open source software directory](https://opensourcesoftwaredirectory.com)

Dank geht an alle engagierten ProgrammiererInnen, die freie und quelloffenen Software entwickeln.

* [firefox](https://www.mozilla.org/de/firefox/)
* [ublock origin](https://de.wikipedia.org/wiki/UBlock_Origin)
* [searx](https://searx.me/)
* [tor](https://www.torproject.org/de/)
* [protonvpn](https://protonvpn.com/de/)
* [mullvad](https://mullvad.net/de/)
* [riseupvpn](https://riseup.net/de/vpn)
* [calyxvpn](https://calyx.net/)
* [protonmail](https://protonmail.com/de/)
* [tutanota](https://tutanota.com/de/)
* [thunderbird](https://www.thunderbird.net/de/)
* [signal](https://signal.org/de/)
* [element](https://element.io/)
* [jami](https://jami.net/)
* [briar](https://briarproject.org/)
* [keepass](https://keepass.info/)
* [andotp](https://github.com/andOTP/andOTP/)
* [freefilesync](https://freefilesync.org/)
* [veracrypt](https://www.veracrypt.fr/)
* [cryptomator](https://cryptomator.org/de/)
* [f-droid](https://f-droid.org/de/)
* [aurora store](https://auroraoss.com/de/)
* [lineageos](https://www.lineageos.org/), [microg](https://microg.org/) & [lineageos for microg](https://lineage.microg.org/)
* [calyxos](https://calyxos.org/)
* [exodus privacy](https://exodus-privacy.eu.org/en/)
* [linux](https://www.linux.org/)
* [ubuntu](https://ubuntu.com/)
* [linux mint](https://linuxmint.com)
* [gnome](https://www.gnome.org/)
* [virtualbox](https://www.virtualbox.org/)
* [etherpad](https://etherpad.org/)
* [cryptpad](https://cryptpad.fr/)
* [seafile](https://www.seafile.com/en/home/)
* [nextcloud](https://nextcloud.com/de/)
* [onlyoffice](https://www.onlyoffice.com/de/)
* [studs](https://sourcesup.cru.fr/projects/studs/)
* [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)
* [jitsi](https://meet.jit.si/)
* [mattermost](https://mattermost.com/)
* [xmpp](https://conversejs.org/)
* [matrix](https://matrix.org/)
* [sympa](https://www.sympa.org/)
* [diaspora](https://diasporafoundation.org/)
* [friendica](https://friendi.ca/)
* [gnu social](https://gnusocial.network/)
* [mastodon](https://joinmastodon.org/)
* [nitter](https://nitter.net/)
* [write freely](https://writefreely.org/)
* [mobilizon](https://joinmobilizon.org/de/)
* [lemmy](https://join-lemmy.org/)
* [aether](https://getaether.net/)
* [teddit](https://teddit.net/)
* [peertube](https://joinpeertube.org/de/)
* [invidious](https://invidious.io/)
* [pixelfed](https://pixelfed.org/)
* [bibliogram](https://bibliogram.art/)
* [lutim](https://github.com/ldidry/lutim/)
* [funkwhale](https://funkwhale.audio/de/)
* [bookwyrm](https://bookwyrm.social/)
* [openstreetmaps](https://www.openstreetmap.org/)
* [umap](https://github.com/umap-project/umap/)
* [gogocarto](https://gogocarto.fr/projects)
* [libre translate](https://libretranslate.com/)
* [simply translate](https://translate.metalune.xyz/)
* [lstu](https://lstu.fr/)
* [polr](https://github.com/cydrobolt/polr/)
* [hastebin](https://www.toptal.com/developers/hastebin/)
* [privatebin](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Instances-Directory)
* [ghostbin](https://ghostbin.com/)
* [libera pay](https://de.liberapay.com/)
* [bitcoin](https://bitcoin.org/de/)
* [ethereum](https://ethereum.org/de/)
* [litecoin](https://litecoin.org/de/)
* [linux malware detect](https://www.rfxn.com/projects/linux-malware-detect/)
* [clamav](https://www.clamav.net/)
* [duckdns](https://www.duckdns.org/)
* [pihole](https://pi-hole.net/)
* [openvpn](https://openvpn.net/)
* [letsencrypt](https://letsencrypt.org/de/)
* [dehydrated](https://dehydrated.io/)
* [radicale](https://radicale.org/)
* [davx5](https://www.davx5.com/)
* [piwigo](https://piwigo.org/)
* [jellyfin](https://jellyfin.org/)
* [rsnapshot](https://rsnapshot.org/)
* [material for mkdocs](https://squidfunk.github.io/mkdocs-material/)
* [echarts](https://github.com/apache/echarts)
* [senfcall](https://senfcall.de/)

Vielen Dank an das gesamte [Framalang-Team](https://framablog.org/framalang/) für die tolle Arbeit an der französischen Übersetzung: Aliénor, CLC, Côme, Frabrice, goofy, Guestr, jums, Marius, susy, Sylvain R. und viele mehr.

Dank geht an die Open-Source-KünstlerInnen:

* Homepage-Hintergrund von Gam-Ol unter der [Pixabay Lizenz](https://pixabay.com/service/license/) veröffentlicht
* Abbildungen inspiriert von [freepik](https://www.freepik.com/) und [undraw](https://undraw.co/)
* Symbole respektvoll entliehen von [ameixa](https://gitlab.com/xphnx/ameixa/)
* Logos und Bildschirmabzüge für Firefox, Friendica, Mastodon, Funkwhale, usw. freundlicherweise von [Wikimedia](https://commons.wikimedia.org/wiki/Main_Page) bereitgestellt
* Blender-Animationen inspiriert von [duck3d](https://www.ducky3d.com/)
* [Contra Chrome](https://contrachrome.com/comic/279/) ist ein großartiger Webcomic, der erklärt, wie der Google-Browser zu einer Bedrohung für die Privatsphäre der Nutzer geworden ist
* Die Fotos im Kapitel *Ursprünge* sind unter der [Unsplash-Lizenz](https://unsplash.com/license) veröffentlicht, Dank geht an Alexander Popov, Gabriel Perez, Sandro Katalina, Roman Denisenko, Andrea De Santis, Denys Nevozhai, Hello I'm Nik, Ray Zhuang, Matthew Henry, Markus Spiske, Gabriel Heinzer, Clark Tibbs, Alexandre Debiève, cheng feng, Taylor Vick, Adi Goldstein
* Geek-Humor serviert von [xkcd](https://xkcd.com/)
* Retro-Musik kommt von [Aries The Producer](https://ariestheproducer.com/) und [nihilore](http://www.nihilore.com/)

<br>
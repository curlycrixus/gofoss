---
template: main.html
title: Auto-hébergez votre propre stockage en nuage
description: Auto-hébergez votre service en nuage. Comment installer Seafile. Qu'est-ce que Seafile ? Seafile vs Nextcloud ? Seafile vs Syncthing ? Comment installer Seafile ? Est-ce que Seafile est sécurisé ?
---

# Seafile, une solution de stockage en nuage auto-hébergée

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises"

<center>
<img align="center" src="../../assets/img/seafile_picture.png" alt="Installation de Seafile" width="550px"></img>
</center>

<br>

[Seafile](https://www.seafile.com/en/home/) est une solution de synchronisation et de partage de fichier auto-hébergée pour votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/). Elle ne se targue pas d'une large éventail de fonctionnalités, mais excelle à garder vos fichiers synchronisés entre plusieurs appareils, et prend en charge le chiffrement. Seafile est entièrement open source, rapide et fiable. Grâce à l'utilisation de la méthode delta-sync, Seafile ne synchronise que les fragments de fichiers mis à jour ou modifiés, plutôt que des fichiers complets. Des clients sont disponibles pour de nombreuses plateformes, y compris les équipements mobiles.

??? question "Qu'en est-il de Nextcloud ou de Syncthing ?"

    [Nextcloud](https://nextcloud.com/) est une solution populaire et très complète de synchronisation de fichiers. Elle est open source et offre de nombreuses fonctionnalités additionnelles, comme de la gestion des contacts, de la conférence vidéo, un webmail en ligne, un calendrier, et ainsi de suite. Son revers est que la synchronisation des fichiers est comparativement lente.

    [Syncthing](https://syncthing.net/) est une solution de synchronisation de fichiers décentralisée. Elle est open source, et contrairement à Seafile et Nextcloud, elle ne nécessite pas de serveur central ou de base de données. Point faible, la synchronisation de fichiers est comparativement lente et, selon votre configuration réseau, vous pouvez vous heurter à des problèmes de pare-feu.


??? question "Est-ce que je ne peux pas simplement continuer à utiliser mon stockage en nuage actuel ?"

    Bien sûr, si vous avez confiance dans ce que font les fournisseurs de services en nuage commerciaux de vos données personnelles. En dépit de leurs affirmations, les solutions en nuage comme iCloud, GDrive, Dropbox ou OneDrive copient les clés protégeant votre vie privée et peuvent accéder à vos données ou les fournir à tiers.

    <center>

    | Fournisseurs de services en nuage | Politique de confidentialité |
    | ------ | ------ |
    | [Apple iCloud](https://support.apple.com/fr-fr/HT202303) |“Messages sur iCloud utilise également le chiffrement de bout en bout. Si la sauvegarde iCloud est activée, une copie de la clé protégeant vos messages est incluse dans votre sauvegarde. Cela vous permet de récupérer vos messages si vous perdez l'accès au trousseau iCloud et à vos appareils de confiance.“ |
    | [Google Drive & WhatsApp](https://faq.whatsapp.com/android/chats/about-google-drive-backups/) |“Les fichiers médias et messages que vous sauvegardez ne sont pas protégés par le chiffrement de bout en bout de WhatsApp lorsqu'ils sont dans Google Drive.“ |
    |[Dropbox](https://www.dropbox.com/privacy#terms) |“Pour vous fournir cela et d'autres fonctionnalités, Dropbox accède, stocke et scanne vos Données. Vous nous donnez la permission de faire ces choses, et cette permission s'étend à nos filiales et aux tiers de confiance avec lesquels nous travaillons.” |
    | [Microsoft OneDrive](https://privacy.microsoft.com/fr-fr/privacystatement/) |“Lorsque vous utilisez OneDrive, nous collectons des données sur votre utilisation du service, comme le contenu que vous stockez, pour fournir, améliorer et protéger les services. Par exemple, cela inclut l'indexation des contenus de vos documents OneDrive pour que vous puissiez les chercher plus tard, ou bien l'utilisation d'informations de localisation pour vous permettre de chercher des photos en vous basant sur le lieu de prise de vue.” |

    </center>


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Seafile MySQL" width="150px"></img> </center>

## Préparation de la base de données

Seafile peut être déployé avec différentes bases de données, incluant MySQL, SQLite, PostgreSQL, et ainsi de suite. Dans ce tutoriel, nous allons générer les bases MySQL requises pour les composants serveur de Seafile. Lisez ce qui suit pour des instructions détaillées.

??? tip "Montrez-moi le guide étape par étape"

    Connectez-vous au serveur et créez un utilisateur système qui peut lancer Seafile. Pour l'exemple de ce tutoriel, nous appellerons cet utilisateur système `seafileadmin`. Bien sûr, vous pouvez choisir n'importe quel nom, soyez juste certain d'ajuster les commandes en conséquence.

    ```bash
    sudo adduser --disabled-login seafileadmin
    ```

    Connectez-vous à MySQL en tant que root :

    ```bash
    sudo mysql -u root -p
    ```

    Lancez la commande ci-dessous pour créer l'utilisateur MySQL `seafileadmin` (ajustez en conséquence). Assurez-vous de remplacer la chaîne "MotDePasseFort" par un [mot de passe unique et fort](https://gofoss.net/fr/passwords/) :

    ```bash
    CREATE USER 'seafileadmin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'MotDePasseFort';
    ```

    Générez ensuite les bases de données nécessaires pour Seafile et accordez leurs les bonnes permissions :

    ```bash
    CREATE DATABASE ccnet;
    CREATE DATABASE seafile;
    CREATE DATABASE seahub;
    GRANT ALL ON ccnet.* TO 'seafileadmin'@'localhost';
    GRANT ALL ON seafile.* TO 'seafileadmin'@'localhost';
    GRANT ALL ON seahub.* TO 'seafileadmin'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;
    ```

    Connectez-vous de nouveau à MySQL en tant qu'utilisateur `seafileadmin` (ajustez en conséquence) :

    ```bash
    sudo mysql -u seafileadmin -p
    ```

    Assurez-vous que toutes les bases ont bien été créées correctement :

    ```bash
    SHOW DATABASES;
    ```

    L'affichage devrait être similaire à ceci :

    ```bash
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | ccnet              |
    | seafile            |
    | seahub             |
    +--------------------+
    4 rows in set (0.02 sec)
    ```

    Quittez MySQL:

    ```bash
    EXIT;
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/0a9f7981-aff4-4c63-be92-60e4fb111c51" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Erreur durant la création de l'administrateur Seafile"

    Plusieurs utilisateurs ont remonté des problèmes lors de l'installation de Seafile sur un serveur Ubuntu. Le problème semble être lié à MySQL 8, qui utilise `caching_sha2_password` comme plugin d'authentification par défaut. La solution est de créer l'utilisateur `seafileadmin` dans MySQL avec le plugin d'authentification `mysql_native_password`, comme décrit dans la section ci-dessus.


<br>

<center> <img src="../../assets/img/separator_seafile.svg" alt="Installation de Seafile" width="150px"></img> </center>

## Installation de Seafile

Suivez les instructions ci-dessous pour résoudre toutes les [dépendances](https://manual.seafile.com/deploy/using_mysql/#requirements) et installer Seafile sur votre serveur Ubuntu 22.04.

??? tip "Montrez-moi le guide étape par étape "

    ### Pré-requis

    Seafile requiert Python pour fonctionner, installez-le sur le serveur :

    ```bash
    sudo apt install python3 python3-{setuptools,pip} libmysqlclient-dev memcached libmemcached-dev libffi-dev
    ```

    Ensuite, installez les paquets additionnels en tant qu'utilisateur `seafileadmin` (ajustez le nom en conséquence):

    ```bash
    sudo -H -u seafileadmin pip3 install --user django==3.2.* Pillow pylibmc captcha jinja2 sqlalchemy==1.4.3 django-pylibmc django-simple-captcha python3-ldap mysqlclient pycryptodome==3.12.0
    ```

    ### Script d'installation

    [Vérifiez la dernière version du serveur Seafile disponible pour Linux](https://www.seafile.com/en/download/). Au moment de la rédaction de ces lignes, c'est la version 9.0.5. Téléchargez et décompressez le paquet avec la commande suivante (ajustez le numéro de version le cas échéant) :

    ```bash
    sudo wget -4 https://download.seadrive.org/seafile-server_9.0.5_x86-64.tar.gz
    sudo tar -xvf seafile-server_*
    sudo mkdir /var/www/seafile
    sudo mkdir /var/www/seafile/installed
    sudo mv seafile-server-* /var/www/seafile
    sudo mv seafile-server_* /var/www/seafile/installed
    ```

    !!! warning "Solution de contournement temporaire pour Ubuntu 22.04"

        Au moment d'écrire ces lignes, Seafile 9.0.5 [ne prend pas en charge la version `1.15.0` du module `cffi`](https://forum.seafile.com/t/seafile-community-edition-9-0-4-is-ready/15988/7), qui est fourni avec Ubuntu 22.04. Pour cette raison, il n'a pas été déployé avec les autres modules Python dans l'étape précédente. Vous pouvez cependant appliquer une [solution de contournement temporaire](https://forum.seafile.com/t/seafile-community-edition-9-0-5-is-ready/16388/14). Après avoir défini les bonnes permissions pour l'utilisateur `seafileadmin` (ajustez en conséquence), installez la version `1.14.6` du module `cffi` dans le dossier `thirdpart` :

        ```bash
        sudo chown -R seafileadmin:seafileadmin /var/www/seafile
        sudo chmod -R 755 /var/www/seafile
        sudo -H -u seafileadmin pip3 install --force-reinstall --upgrade --target /var/www/seafile/seafile-server-9.0.5/seahub/thirdpart cffi==1.14.6
        ```

    Lancez le script d'installation :

    ```bash
    sudo bash /var/www/seafile/seafile-server-9.0.5/setup-seafile-mysql.sh
    ```

    Suivez les instructions à l'écran :

    <center>

    | Instruction | Description |
    | ------ | ------ |
    | Server name | Choisissez un nom pour le serveur Seafile. Pour ce tutoriel, nous choisirons `gofoss_seafile`, ajustez le cas échéant.  |
    | Domain | Fournissez le nom de domaine utilisé par le stockage en nuage. Il s'agit de la même adresse que celle de l'interface que nous installerons par la suite. Pour ce tutoriel, nous utiliserons `https://myfiles.gofoss.duckdns.org`, ajustez le cas échéant. |
    | Port | Choisissez le port TCP que Seafile utilisera. La valeur par défaut est `8082`. |
    | Databases | Utilisez les bases de données ccnet/seafile/seahub que nous avons créées précédemment. |
    | Host of MySQL server | La valeur par défaut est `localhost`. |
    | Port of MySQL server | La valeur par défaut est `3306`. |
    | MySQL user | Saisissez le nom de l'utilisateur MySQL. Dans notre exemple, c'est `seafileadmin`, ajustez le cas échéant. |
    | Password of MySQL user | Saisissez le mot de passe de l'utilisateur MySQL. |
    | ccnet database name | Saisissez le nom de la base de données ccnet. Dans notre cas, c'est `ccnet`. |
    | seafile database name | Saisissez le nom de la base de données seafile. Dans notre cas, c'est `seafile`. |
    | seahub database name | Saisissez le nom de la base de données seahub. Dans notre cas, c'est `seahub`. |

    </center>

    Après une installation réussie, le terminal devrait afficher quelque chose comme :

    ```bash
    -----------------------------------------------------------------
    Your seafile server configuration has been finished successfully.
    -----------------------------------------------------------------

    run seafile server:     ./seafile.sh { start | stop | restart }
    run seahub  server:     ./seahub.sh  { start <port> | stop | restart <port> }

    -----------------------------------------------------------------
    If you are behind a firewall, remember to allow input/output of these tcp ports:
    -----------------------------------------------------------------

    port of seafile fileserver:   8082
    port of seahub:               8000

    When problems occur, refer to https://download.seafile.com/published/seafile-manual/home.md for information.
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8a1189e-ce82-47ee-8a70-1e5eb9ab8462" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Configuration de Seafile" width="150px"></img> </center>

## Configuration

Après l'installation de Seafile, nous allons configurer un certain nombre de paramètre comme la préférence de langue, les permissions et les comptes administrateurs, le démarrage automatique et ainsi de suite. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Langue

    Changez la langue. Dans cet exemple, nous utiliserons le français:

    ```bash
    echo "export LC_ALL=fr_FR.UTF-8" >>~/.bashrc
    echo "export LANG=fr_FR.UTF-8" >>~/.bashrc
    echo "export LANGUAGE=fr_FR.UTF-8" >>~/.bashrc
    source ~/.bashrc
    ```

    Assurez-vous que les paramètres sont appliqués correctement avec la commande suivante :

    ```bash
    locale
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    LANG=fr_FR.UTF-8
    LANGUAGE=fr_FR.UTF-8
    LC_CTYPE="fr_FR.UTF-8"
    LC_NUMERIC="fr_FR.UTF-8"
    LC_TIME="fr_FR.UTF-8"
    LC_COLLATE="fr_FR.UTF-8"
    LC_MONETARY="fr_FR.UTF-8"
    LC_MESSAGES="fr_FR.UTF-8"
    LC_PAPER="fr_FR.UTF-8"
    LC_NAME="fr_FR.UTF-8"
    LC_ADDRESS="fr_FR.UTF-8"
    LC_TELEPHONE="fr_FR.UTF-8"
    LC_MEASUREMENT="fr_FR.UTF-8"
    LC_IDENTIFICATION="fr_FR.UTF-8"
    LC_ALL=fr_FR.UTF-8
    ```

    Bien sûr, vous pouvez configurer n'importe quelle langue. Vérifiez les langues (ou "locales") que le serveur supporte :

    ```bash
    locale -a
    ```

    Si vous voulez ajouter d'autres langues, éditez le fichier de configuration suivant en enlevant les commentaires des lignes requises (par exemple `de_DE.UTF-8` pour l'allemand, `es_ES.UTF-8` pour l'espagnol, `nl_NL.UTF-8` pour le néerlandais, et ainsi de suite) :

    ```bash
    sudo vi /etc/locale.gen
    ```

    Appliquez les changements :

    ```bash
    sudo locale-gen
    ```

    Lancez les commandes présentées dans la section ci-dessus, en appliquant vos paramètres de langue.


    ### Compte administrateur

    Assignez les bonnes permissions, et déplacez-vous dans le répertoire `/var/www/seafile` :

    ```bash
    sudo chown -R seafileadmin:seafileadmin /var/www/seafile
    sudo chmod -R 755 /var/www/seafile
    sudo chmod 750 /home/seafileadmin
    cd /var/www/seafile
    ```

    Démarrez Seafile:

    ```bash
    sudo -H -u seafileadmin bash -C '/var/www/seafile/seafile-server-latest/seafile.sh' start
    ```

    Démarrez Seahub:

    ```bash
    sudo -H -u seafileadmin bash -C '/var/www/seafile/seafile-server-latest/seahub.sh' start
    ```

    Au premier démarrage de Seahub, il vous demande de configurer un compte administrateur. Dans ce tutoriel, nous utiliserons l'email `seafileadmin@gofoss.net` pour l'administrateur. Bien sûr, n'importe quelle adresse appropriée fonctionnera. Assurez-vous simplement d'ajuster les commandes correspondantes en conséquence. Lorsque cela vous sera demandé, saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/).

    ### Démarrage automatique

    Assurez-vous que Seafile se lance automatiquement à chaque démarrage du serveur. Créez un premier fichier de configuration :

    ```bash
    sudo vi /etc/systemd/system/seafile.service
    ```

    Ajoutez le contenu suivant:

    ```bash
    [Unit]
    Description=Seafile
    After= mysql.service network.target

    [Service]
    Type=forking
    ExecStart=/var/www/seafile/seafile-server-latest/seafile.sh start
    ExecStop=/var/www/seafile/seafile-server-latest/seafile.sh stop
    User=seafileadmin

    [Install]
    WantedBy=multi-user.target
    ```

    Sauvegardez et fermez le fichier (`:wq!`), puis créez un second fichier de configuration:

    ```bash
    sudo vi /etc/systemd/system/seahub.service
    ```

    Ajoutez le contenu suivant:

    ```bash
    [Unit]
    Description=Seahub
    After= mysql.service network.target seafile.service

    [Service]
    Type=forking
    Environment="LC_ALL=en_US.UTF-8"
    ExecStart=/var/www/seafile/seafile-server-latest/seahub.sh start
    ExecStop=/var/www/seafile/seafile-server-latest/seahub.sh stop
    User=seafileadmin

    [Install]
    WantedBy=multi-user.target
    ```

    Sauvegardez et quittez le fichier (`:wq!`).

    Lancez Seafile automatiquement à chaque démarrage :

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable seafile
    sudo systemctl enable seahub
    ```

    Redémarrez le serveur :

    ```bash
    sudo reboot
    ```

    Assurez-vous que Seafile est démarré et opérationnel (le statut devrait être "Active") :

    ```bash
    sudo systemctl status seafile
    sudo systemctl status seahub
    ```


    ### Dernière étape

    Le stockage en nuage sera accessible depuis le domaine de votre choix. Pour ce tutoriel, nous avons choisi `https://myfiles.gofoss.duckdns.org`. Bien sûr, n'importe quelle autre adresse appropriée conviendra. Assurez-vous simplement d'ajuster les commandes correspondantes en conséquence. Ouvrez le premier fichier de configuration :

    ```bash
    sudo vi /var/www/seafile/conf/seahub_settings.py
    ```

    Assurez-vous que le `SERVICE__URL` pointe vers la bonne adresse :

    ```bash
    SERVICE_URL = "https://myfiles.gofoss.duckdns.org/"
    ```

    Ajoutez ensuite les lignes ci-dessous à la fin du fichier (ajustez le fuseau horaire et l'URL à vos propres réglages) :

    ```bash
    TIME_ZONE = 'Europe/Budapest'
    SESSION_EXPIRE_AT_BROWSER_CLOSE = True
    LOGIN_ATTEMPT_LIMIT = 2
    FILE_SERVER_ROOT = 'https://myfiles.gofoss.duckdns.org/seafhttp'
    ```

    Sauvez et quittez le fichier (`:wq!`).


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4812ac9b-29e3-4941-9681-1332e1b3047d" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Interface web de Seafile" width="150px"></img> </center>

## Interface web

Nous allons configurer un hôte virtuel Apache comme proxy inverse pour accéder à l'interface web de Seafile. Des instructions plus détaillées sont disponibles ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Créez un fichier de configuration Apache:

    ```bash
    sudo vi /etc/apache2/sites-available/myfiles.gofoss.duckdns.org.conf
    ```

    Ajoutez le contenu ci-dessous et assurez-vous d'ajuster les paramètres à vos propres réglages, comme les noms de domaine (`myfiles.gofoss.duckdns.org`), chemins vers les clés SSL, l'adresse IP, et ainsi de suite:

    ```bash
    <VirtualHost *:80>

        ServerName              myfiles.gofoss.duckdns.org
        ServerAlias             www.myfiles.gofoss.duckdns.org
        Redirect permanent /    https://myfiles.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              myfiles.gofoss.duckdns.org
        ServerAlias             www.myfiles.gofoss.duckdns.org
        ServerSignature         Off

        SecRuleEngine           Off
        SSLEngine               On
        SSLProxyEngine          On
        SSLProxyCheckPeerCN     Off
        SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
        SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
        DocumentRoot            /var/www/seafile

        <Location />
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        Alias /media  /var/www/seafile/seafile-server-latest/seahub/media

        RewriteEngine On

        <Location /media>
        Require all granted
        </Location>

        # seafile fileserver
        ProxyPass /seafhttp http://127.0.0.1:8082
        ProxyPassReverse /seafhttp http://127.0.0.1:8082

        # seahub
        SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
        ProxyPreserveHost On
        ProxyPass / http://127.0.0.1:8000/
        ProxyPassReverse / http://127.0.0.1:8000/

        ErrorLog ${APACHE_LOG_DIR}/myfiles.gofoss.duckdns.org-error.log
        CustomLog ${APACHE_LOG_DIR}/myfiles.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Une fois que ce contenu est ajouté, sauvez et fermez le fichier (`:wq!`).

    Notez comment nous avons activé le chiffrement SSL pour Seafile avec l'instruction `SSLEngine On`, et utilisé le certificat SSL `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` ainsi que la clé privée SSL `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, que nous avions créés précédemment.

    Notez également comment nous avons désactivé ModSecurity dans le fichier de configuration Apache avec l'instruction `SecRuleEngine Off`, car Seafile et ModSecurity ne font pas bon ménage.

    Ensuite, activez l'hôte virtuel Apache et redémarrez Apache :

    ```bash
    sudo a2ensite myfiles.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configurez Pi-Hole pour résoudre l'adresse locale de Seafile. Naviguez vers `https://mypihole.gofoss.duckdns.org` et connectez-vous à l'interface web de Pi-Hole (ajustez en conséquence). Naviguez vers l'entrée `Local DNS Record` et ajoutez la combinaison domaine/IP suivante (ajustez en conséquence) :

    ```bash
    DOMAIN:      myfiles.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Naviguez vers [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) et connectez-vous en tant que `seafileadmin@gofoss.net` (ajustez en conséquence). Puis appliquez les paramètres suivants :

    <center>

    | Paramètre | Description |
    | ------ | ------ |
    | System Admin ‣ Settings | Changez le champs `SERVICE_URL` pour `https://myfiles.gofoss.duckdns.org` et cliquez sur `Save`. |
    | System Admin ‣ Settings | Changez le champs `FILE_SERVER_ROOT` pour `https://myfiles.gofoss.duckdns.org/seafhttp` and cliquez sur `Save`. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/59faee72-c4e9-491f-8bb7-4a2cbfb1fbb4" frameborder="0" allowfullscreen></iframe>
    </center>


<br>


<center> <img src="../../assets/img/separator_fdroid.svg" alt="Clients Seafile" width="150px"></img> </center>

## Clients

### Seahub

Seahub n'est pas vraiment un client, mais l'interface web de Seafile. Les utilisateurs peuvent se connecter depuis un navigateur et parcourir les répertoires, téléverser, éditer et télécharger des fichiers, partager des données avec d'autres, et ainsi de suite. Seahub affiche automatiquement le format des fichiers de Microsoft et de LibreOffice, de même que les vidéos, images, PDF et fichiers textes, et ainsi de suite. Les changements qui sont fait dans Seahub sont répliqués sur tous les appareils synchronisés.


### Client de synchronisation

Ce client permet de synchroniser les fichiers et répertoires sélectionnés à travers les appareils connectés. Il convient donc mieux à des équipements avec une capacité de stockage suffisante.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installation | [Téléchargez et installez la dernière version du client Windows de synchronisation Seafile pour ordinateur](https://www.seafile.com/en/download/). <br><br><center> <img src="../../assets/img/seafile_desktop_client_1.png" alt="Seafile Desktop Sync" width="150px"></img> </center> |
        | Stockage | Démarrez le client Seafile et sélectionnez un répertoire local à synchroniser avec le cloud. <br><center> <img src="../../assets/img/seafile_desktop_client_2.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Identifiants | Saisissez le nom du serveur et les identifiants de l'utilisateur. Si c'est activé, saisissez aussi l'authentification à deux-étapes. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installation | [Téléchargez et installez la dernière version du client de synchronisation Seafile pour ordinateur pour macOS](https://www.seafile.com/en/download/). <br><br><center> <img src="../../assets/img/seafile_desktop_client_1.png" alt="Seafile Desktop Sync" width="150px"></img> </center> |
        | Stockage | Lancez le client Seafile et selectionnez un répertoire local à synchroniser avec le cloud. <br><center> <img src="../../assets/img/seafile_desktop_client_2a.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Identifiants | Saisissez le nom du serveur et les identifiants de l'utilisateur. Si c'est activé, saisissez aussi l'authentification à deux-étapes. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Clé de signature | Ouvrez un terminal et ajoutez la clé de signature : <br>`sudo wget https://linux-clients.seafile.com/seafile.asc -O /usr/share/keyrings/seafile-keyring.asc` |
        | Dépôt | Ajoutez le dépot à la liste des sources apt d'Ubuntu 22.04 (Jammy Jellyfish) : <br> `sudo bash -c "echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/seafile-keyring.asc] https://linux-clients.seafile.com/seafile-deb/jammy/ stable main' > /etc/apt/sources.list.d/seafile.list"`<br><br> Puis mettez à jour le cache apt local : `sudo apt update` |
        | Installation | Installez le client de synchronisation Seafile : `sudo apt install -y seafile-gui` |
        | Stockage | Démarrez le client Seafile et sélectionnez un répertoire local à synchroniser avec le cloud. <br><center> <img src="../../assets/img/seafile_desktop_client_2b.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Identifiants | Saisissez le nom du serveur et les identifiants de l'utilisateur. Si c'est activé, saisissez aussi l'authentification à deux-étapes. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |

        </center>


### SeaDrive

Le client SeaDrive fonctionne comme un disque réseau. Les utilisateurs peuvent accéder aux fichiers sur le serveur sans synchronisation préalable. Les fichiers peuvent aussi être mis en cache pour une utilisation hors ligne. SeaDrive convient bien pour étendre la capacité de stockage des appareils avec un espace disque limité.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installation | [Téléchargez et installez la dernière version du client SeaDrive pour Windows](https://www.seafile.com/en/download/). <br><br><center> <img src="../../assets/img/seadrive_client_1.png" alt="Seadrive" width="300px"></img> </center> |
        | Disque virtuel | Démarrez le client SeaDrive et sélectionnez une lettre de lecteur pour le disque virtuel. Par défaut `S:` sera utilisé. |
        | Identifiants | Saisissez le nom du serveur et les identifiants de l'utilisateur. Si c'est activé, saisissez aussi l'authentification à deux-étapes. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Liste des fichiers | SeaDrive va récupérer une liste des fichiers et répertoires disponibles sur le serveur, sans pour autant télécharger les données. Une notification sera affichée une fois la tâche terminée. Les fichiers du cloud sont désormais accessibles dans l'explorateur Windows, comme sur un disque ordinaire. <br><center> <img src="../../assets/img/seadrive_client_2.png" alt="Seadrive" width="300px"></img> </center> |

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installation | [Téléchargez et installez la dernière version du client SeaDrive pour macOS](https://www.seafile.com/en/download/). <br><br><center> <img src="../../assets/img/seadrive_client_1.png" alt="Seadrive" width="300px"></img> </center> |
        | Disque virtuel | Lancez le client SeaDrive et sélectionnez une lettre de lecteur pour le disque virtuel. Par défaut, `S:` sera utilisé. |
        | Identifiants | Saisissez le nom du serveur et les identifiants de l'utilisateur. Si c'est activé, saisissez aussi l'authentification à deux-étapes. <br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Liste des fichiers | SeaDrive va récupérer une liste des fichiers et répertoires disponibles sur le serveur, sans pour autant télécharger les données. Une notification sera affichée une fois la tâche terminée. Les fichiers du cloud sont désormais accessibles, comme sur un disque ordinaire. <br><center> <img src="../../assets/img/seadrive_client_2.png" alt="Seadrive" width="300px"></img> </center> |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Clé de signature | Ouvrez un terminal et ajoutez la clé de signature : <br> `sudo wget https://linux-clients.seafile.com/seafile.asc -O /usr/share/keyrings/seafile-keyring.asc` |
        | Dépôt | Ajoutez le dépot à la liste des sources apt d'Ubuntu 22.04 (Jammy Jellyfish) : <br> `sudo bash -c "echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/seafile-keyring.asc] https://linux-clients.seafile.com/seadrive-deb/jammy/ stable main' > /etc/apt/sources.list.d/seadrive.list"`<br><br> Then update the local apt cache: `sudo apt update` |
        | Installation | Installez le client SeaDrive : `sudo apt install -y seadrive-gui` |
        | Identifiants | Saisissez le nom du serveur et les identifiants de l'utilisateur. Si c'est activé, saisissez aussi l'authentification à deux-étapes. Après identification, le lecteur virtuel sera monté dans `~/SeaDrive`.<br><center> <img src="../../assets/img/seafile_desktop_client_3.png" alt="Seafile Desktop Client" width="300px"></img> </center> |

        </center>


### Clients mobiles

Seafile est disponible pour les smartphones et les tablettes. Les utilisateurs peuvent naviguer à travers les répertoires, envoyer, éditer et télécharger des fichiers. Pour sauver de l'espace de stockage, les fichiers sont mis en cache mais pas entièrement synchronisés. Les applications mobiles Seafile incluent également une fonctionnalité de sauvegarde automatique des photos sur le serveur. Plus d'instructions sont données ci-après.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        Ouvrez F-Droid sur votre téléphone et installez [l'application Seafile](https://f-droid.org/fr/packages/com.seafile.seadroid2/). Vous pouvez aussi installer Seafile depuis le [dépot Aurora](https://auroraoss.com/) ou le [dépot Google Play](https://play.google.com/store/apps/details?id=com.seafile.seadroid2).

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Serveur | Saisissez l'adresse du serveur Seafile, e.g. [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/)(ajustez en conséquence). |
        | Email | Saisissez l'adresse électronique de l'utilisateur de Seafile, e.g. `seafileuser@gofoss.net` (ajustez en conséquence). |
        | Mot de passe | Saisissez le mot de passe de l'utilisateur Seafile. |
        | Token | Si l'authentification à deux facteurs est activée, entrez le code généré par andOPT sur votre téléphone. |

        </center>

=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        Installez Seafile via l'[App Store](https://apps.apple.com/cn/app/seafile-pro/id639202512?l=fr&mt=8).

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Serveur | Saisissez l'adresse du serveur Seafile, e.g. [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/)(ajustez en conséquence). |
        | Email | Saisissez l'adresse électronique de l'utilisateur de Seafile, e.g. `seafileuser@gofoss.net` (adjust accordingly). |
        | Mot de passe | Saisissez le mot de passe de l'utilisateur Seafile. |
        | Token | Si l'authentification à deux facteurs est activée, entrez le code généré par andOPT sur votre téléphone. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Seafile ajoutez des utilisateurs" width="150px"></img> </center>

## Ajoutez des utilisateurs

Seafile différencie deux types d'utilisateurs :

* **Les administrateurs** ont un accès complet à Seafile. Ils peuvent ajouter, éditer et supprimer des fichiers et des répertoires, comme tous les autres utilisateurs. En complément, les administrateurs peuvent maintenir et mettre à jour le site web. Dans notre exemple, l'administrateur `seafileadmin` a été créé durant l'installation du serveur Seafile.

* **Les Utilisateurs** peuvent être ajoutés par les administrateurs, mais ils n'ont qu'un accès limité à Seafile. Ils peuvent seulement gérer les fichiers & dossiers si ils ont les permissions adéquates.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Description |
    | ------ | ------ |
    | Étape 1 | Allez sur [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org) et connectez-vous en tant qu'administrateur/administratrice, p.ex.`seafileadmin@gofoss.net` (à modifier selon votre identifiant). |
    | Étape 2 | Allez dans `System Admin ‣ Users ‣ Add user`. |
    | Étape 3 | Saisissez un nom d'utilisateur, ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Puis cliquez sur `Submit`. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    L'administrateur système s'identifie sur l'interface web de Seahub pour ajouter les utilisateurs Georg, Tom et l'utilisatrice Lenina.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/d6db71f4-a793-472a-ba6a-e2095c6f8e47" frameborder="0" allowfullscreen></iframe>
    </center>

??? warning "Administrateurs, utilisateurs et utilisatrices ont besoin d'un accès VPN"

    Tout.e.s les utilisateurs et utilisatrices doivent être connecté.e.s via [VPN](https://gofoss.net/fr/secure-domain/) pour accéder à Seafile.



<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Seafile authentification à deux facteurs" width="150px"></img> </center>

## Authentification à deux facteurs

L'authentification à deux facteurs (2FA) est une fonctionnalité optionnelle qui permet de sécuriser la connexion. Si elle est activée, se connecter à Seafile nécessitera, en plus du mot de passe, un code à 6 chiffres généré par une application installée sur le téléphone de l'utilisateur. Nous recommandons d'utiliser [andOTP](https://f-droid.org/fr/packages/org.shadowice.flocke.andotp/) qui peut être installé depuis F-Droid.


??? tip "Montrez-moi le guide étape par étape"

    Premièrement, l'administrateur de Seafile doit activer l'authentification à deux facteurs.

    <center>

    | Étapes | Description |
    | ------ | ------ |
    | Étape 1 | Connectez vous à l'interface web de Seafile [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) avec l'adresse `seafileadmin@gofoss.net` (ajustez le cas échéant). |
    | Étape 2 | Naviguez vers `Administration Système ‣ Paramètres ‣ Mots de Passes`. |
    | Étape 3 | Activez l'authentification à deux facteurs pour tout les utilisateurs. |

    </center>

    Ensuite, chaque utilisateur doit configurer l'authentification à deux facteurs. Commencez par l'administrateur puis répétez pour chaque compte la configuration suivante:

    <center>

    | Étapes | Description |
    | ------ | ------ |
    | Étape 1 | Connectez vous à l'interface web Seafile [https://myfiles.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/). |
    | Étape 2 | Naviguez vers `Paramètres`. |
    | Étape 3 | Activez l'authentification à deux facteurs. |
    | Étape 4 | Ouvrez andOTP sur l'appareil de l'utilisateur. |
    | Étape 5 | Scannez le QRCode affiché avec andOTP. |
    | Étape 6 | Saisissez le code à 6 chiffres généré par andOTP sur l'interface web Seafile. |
    | Étape 7 | Enregistrez la liste des `jetons de récupérations` générée par Seafile. Ils vous permettent de récupérer l'accès à Seafile sans andOTP. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/1c615577-9a2b-4c7e-aa50-9a8d575abc98" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Seafile ajouter des bibliothèques, fichiers et dossiers" width="150px"></img> </center>

## Ajouter des bibliothèques, fichiers et dossiers

=== "Seahub"

    Seahub permet aux utilisateurs de créer de nouvelles bibliothèques, fichiers et dossiers via le navigateur. Les utilisateurs peuvent aussi téléverser des fichiers et dossiers dans les bibliothèques existantes.

    ??? tip "Montrez-moi une vidéo récapitulative (2min)"

        Georg se connecte à l'interface web Seahub pour créer une bibliothèques avec les photos de sa randonnée avec Lenina. Tom se connecte aussi pour téléverser un roman "Politics and the English Language", écrit par George Orwell en 1946.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7e5847ea-881b-49fa-a1ab-1c16223ea366" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Client de synchronisation"

    Les utilisateurs peuvent créer de nouvelles bibliothèques ou téléverser des fichiers et dossiers dans celles existantes via le client de synchronisation de Seafile. Des instructions plus détaillées sont disponibles ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Créer une nouvelle bibliothèque | Ouvrez le client Synching, cliquez sur `Sélectionner` et naviguez jusqu'au dossier que vous voulez synchroniser avec le serveur. Autrement, glissez et déposez le dossier local dans la zone prévue. Ensuite, donnez un nom à la nouvelle bibliothèque et précisez si elle doit être chiffrée par un mot de passe ou non. <br> <center> <img src="../../assets/img/seafile_desktop_client_5b.png" alt="Seafile Desktop Sync" width="300px"></img> </center> |
        | Ajouter des fichiers et des dossiers à une bibliothèque existante | Utilisez le gestionnaire de fichiers de l'appareil. naviguez vers le dossier synchronisé et ajoutez des fichiers et dossiers voulus. Les modifications  seront synchronisés avec le serveur de Seafile, tout comme  pour tout autre apparel connecté. |

        </center>


=== "SeaDrive"


    SeaDrive fonctionne comme un lecteur réseau. Les utilisateurs peuvent créer directement de nouvelles bibliothèques ou ajouter des bibliothèques existantes, des fichiers et des dossiers depuis le gestionnaire de fichiers de l'appareil. Pour davantage de détails, parcourez les manuel utilisateur de Seafile qui décrit les caractéristiques de [la version 1 de SeaDrive](https://help.seafile.com/drive_client/using_drive_client/) comme celles de [la version 2 de SeaDrive](https://help.seafile.com/drive_client/drive_client_2.0_for_windows_10/).



<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Seafile synchronisation de données" width="150px"></img> </center>

## Synchronisez vos données

=== "Client de synchronisation"

    Seafile fonctionne avec ce que l'on appelle des bibliothèques, qui contiennent les fichiers et dossiers à proprement parler. Le client de synchronisation s'assure que tout changement local apporté à une bibliothèque est reflété sur le serveur Seafile et les autres appareils connectés, et vice versa. Plus d'instructions sur la façon de synchroniser les bibliothèques sont décrites ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Synchroniser une bibliothèque | Ouvrez le client Synching et faites un clic-droit sur la bibliothèque. Ensuite, sélectionnez l'entrée `Synchroniser cette bibliothèque` du menu. Tous les fichiers et les répertoires seront téléchargés du serveur sur le stockage local. À partir de ce moment, toute modification du fichier local sera répliquée sur le serveur, et vice versa.<br><center> <img src="../../assets/img/seafile_desktop_client_4.png" alt="Seafile Desktop Sync" width="200px"></img> </center> |
        | Désynchroniser une bibliothèque | Pour arrêter la synchronisation d'une librairie, faites un clic-droit et sélectionnez l'entrée `Désynchroniser`. Les modifications locales ne seront plus synchronisées sur le serveur, et vice versa. |
        | Navigateur de fichiers cloud | Le client de synchronisation fournit aussi un navigateur de fichiers cloud. Il permet aux utilisateurs d'accéder et de modifier les fichiers directement sur le serveur, sans synchronisation préalable. Faites un clic-droit sur une bibliothèque (désynchronisée) et choisissez l'entrée `Ouvrir dans le navigateur de fichiers cloud`. <br><br><center> <img src="../../assets/img/seafile_desktop_client_6.png" alt="Seafile Desktop Sync" width="250px"></img> </center> |
        |Synchroniser des sous-répertoires | Pour éviter de synchroniser de grandes bibliothèques, il est possible de synchroniser uniquement certains sous-répertoires. Faites un clic-droit sur la bibliothèque, choisissez l'entrée `Ouvrir dans le navigateur de fichiers cloud`, faites un clic-droit sur le sous-répertoire et sélectionnez l'entrée `Synchroniser ce répertoire`. <br><br><center> <img src="../../assets/img/seafile_desktop_client_7.png" alt="Seafile Desktop Sync" width="250px"></img> </center> |

        </center>


    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        Georg utilise le client de synchronisation Seafile pour synchroniser ses photos de randonnées sur son ordinateur. Il accède ensuite à la copie locale de ses photos et les déplace dans un nouveau répertoire nommé « holidays ». Ces changements locaux sont immédiatement répliqués sur le serveur Seafile, comme sur n'importe quel appareil connecté. Lorsque Georg se reconnecte à l'interface web de Seahub, le nouveau dossier « holidays » apparaît.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/9e254a58-7a44-4687-b521-897731db9983" frameborder="0" allowfullscreen></iframe>
        </center>


=== "SeaDrive"

    Le client SeaDrive fonctionne comme un disque réseau. Les utilisatrices et utilisateurs peuvent directement accéder aux fichiers stockés sur le serveur Seafile, sans synchronisation préalable. Les instructions pour savoir comment utiliser SeaDrive se trouvent ci-dessous.

    ??? info "En apprendre davantage sur le statut de synchronisation"

        L'explorateur Windows utilise plusieurs icônes pour indiquer le statut de synchronisation des fichiers et des répertoires.

        <center>

        | Status | Symbole |Description |
        | :------: | :------: | ------ |
        | Cloud-uniquement |<img src="../../assets/img/cloud_only.png" alt="Seafile cloud uniquement" width="30px"></img> |C'est l'état par défaut des fichiers et des répertoires. Ils sont affichés dans SeaDrive, mais ne sont pas téléchargés sur le stockage local.|
        | Synchronisé |<img src="../../assets/img/synced.png" alt="Seafile synchronisé" width="30px"></img> |Dès qu'une utilisatrice ou un utilisateur décide d'accéder à un fichier ou à un répertoire, il sera téléchargé sur le stockage local. |
        | Partiellement synchronisé |<img src="../../assets/img/partial_synced.png" alt="Seafile partiellement synchronisé" width="30px"></img> |Les répertoires contenant à la fois des fichiers `cloud-uniquement` et `synchronisés` sont considérés comme `partiellement synchronisés`. |
        | Bloqué par d'autres |<img src="../../assets/img/locked.png" alt ="Seafile bloqué" width="30px"></img> |Les fichiers qui sont bloqués par d'autres utilisatrices ou utilisateurs ne peuvent être ouvert qu'en lecture seule, et ne peuvent être modifiés, effacés ou renommés. |
        | Bloqué par moi |<img src="../../assets/img/locked_by_me.png" alt="Seafile bloqué" width="30px"></img> |Un fichier ou un répertoire bloqué par son propriétaire ne peut être modifié par d'autres. |
        | Lecture seule |<img src="../../assets/img/read_only.png" alt="Seafile lecture seule" width="30px"></img> |Les fichiers et les répertoires partagés en mode lecture seule avec une utilisatrice ou un utilisateur peuvent être ouverts, mais ne peuvent être modifiés, effacés ou renommés. |

        </center>


<br>

<center> <img src="../../assets/img/separator_ip.svg" alt="Seafile partagez vos fichiers" width="150px"></img> </center>

## Partagez vos fichiers

=== "Partage de liens"

    Quiconque dispose d'un lien public de partage et d'une connexion à un VPN fonctionnel peut accéder aux dossiers et fichiers partagés sur le serveur. Aucun compte ni identifiant Seafile n'est nécessaire. Les liens de partage peuvent être protégés par un mot de passe paramétré pour expirer après un certain délai, ou bien pour limiter les permissions d'accès. Vous trouverez ci-dessous les instructions nécessaires pour créer des liens de partage.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        |Étape 1 |Naviguez jusqu'au fichier ou répertoire à partager. |
        |Étape 2 |Survolez le fichier ou dossier et cliquez sur l'icône `Partager`. |
        |Étape 3 |Cliquez sur l'onglet `Lien de partage` dans la fenêtre popup. |
        |Étape 4 |Cliquez sur le bouton `Générer` pour créer le lien de partage. |
        |Étape 5 |Facultatif : créez un mot de passe, une date d'expiration ou des permissions. |
        |Étape 6 |Partagez le lien avec d'autres par mail, messagerie instantanée etc. |

        </center>


=== "Liens de téléversement"

       Quiconque dispose d'un lien public de téléversement et d'une connexion à un VPN fonctionnel peut téléverser des dossiers et fichiers sur le serveur. Aucun compte ni identifiant Seafile n'est nécessaire. Les liens de téléversement peuvent être protégés par un mot de passe. Vous trouverez ci-dessous les instructions pour créer des liens de téléversement.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        |Étape 1 |Naviguez jusqu'au répertoire dans lequel les fichiers devront être téléversés. |
        |Étape 2 |Survolez sur le répertoire et cliquez sur l'icône `Partager`. |
        |Étape 3 |Cliquez sur l'onglet `Lien de partage` dans la fenêtre qui s'affiche. |
        |Étape 4 |Cliquez sur le bouton `Générer` pour créer un lien de partage. |
        |Étape 5 |Facultatif : créez un mot de passe. |
        |Étape 6 |Partagez le lien avec d'autres par mail, messagerie instantanée etc. |

        </center>


=== "Partagez avec des utilisateurs de Seafile"

    Les bibliothèques, fichiers et répertoires peuvent être partagés avec d'autres personnes ou groupes utilisant Seafile. Pour accéder aux fichiers partagés, ils ont besoin d'une connexion VPN fonctionnelle, d'un compte Seafile, et des permissions d'accès. Vous trouverez ci-dessous les instructions pour partager les bibliothèques, fichiers et répertoires avec d'autres personnes utilisant Seafile.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étape | Description |
        | ------ | ------ |
        |Étape 1 |Naviguez jusqu'à la bibliothèque ou le répertoire que vous voulez partager avec les autres personnes utilisant Seafile. |
        |Étape 2 |Survolez la bibliothèque ou le répertoire et cliquez sur l'icône `Partager`. |
        |Étape 3 |Cliquez sur `Partager avec une personne` ou `Partager avec un groupe` dans la fenêtre qui s'affiche. |
        |Étape 4 |Sélectionnez la personne ou le groupe et cliquez sur le bouton `Soumettre`. |
        |Étape 5 |Donnez des permissions aux fichiers et répertoires partagés, comme `lecture-écriture`, `lecture-seule`, `lecture-écriture en ligne` ou `lecture-seule en ligne`. |

        </center>


<div style="margin-top:-20px;">
</div>

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    ** Partage de lien** : Tom souhaite partager un essai d'Orwell avec un ami du club de lecture. Cet ami peut se connecter au serveur via un VPN, mais n'a pas son propre compte Seafile. Tom se connecte donc dans l'interface web de Seahub pour lui envoyer un lien de partage, qui expire après une semaine.

    **Partage avec des utilisatrices et utilisateurs de Seafile** : Georg souhaite partager ses photos de randonnées avec Lenina. Lenina peut se connecter au serveur via un VPN, et dispose d'un compte Seafile. Georg se connecte donc dans l'interface web de Seahub et partage le dossier "vacances" avec Lenina, en mode "lecture-seule".

    **Liens de téléversement** : Georg a besoin de récupérer des documents d'un collègue. Ce collègue peut se connecter au serveur via le VPN, mais ne dispose pas de compte Seahub. Georg se connecte donc dans l'interface web Seahub, créé une nouvelle bibliothèque qu'il appelle "travail", et envoie un lien de téléversement à son collègue.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4a00a8e9-b42c-4adb-ac7d-f1b1e21b40c2" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_ntp.svg" alt="Seafile rétablissez vos fichiers" width="150px"></img> </center>

## Rétablissez vos fichiers

Seafile suit automatiquement l'historique des modifications de tous les fichiers, répertoires et bibliothèques. Il garde une sauvegarde des anciennes versions des fichiers, de même qu'une image des répertoires et de la structure des bibliothèques pour une période de temps prédéfinie. Fichiers, répertoires ou bibliothèques entières peuvent être rétablies dans un état précédent en cas de suppression accidentelle ou une opération malheureuse.

=== "Période de rétention"

    La période de rétention définie combien de temps Seafile garde les versions des fichiers ou les images des bibliothèques. Cela peut être configuré distinctement pour chaque bibliothèque, les instructions sont ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        |Étape 1 |Connectez-vous dans l'interface web Seahub. Cliquez sur `Mes bibliothèques` dans le panneau de navigation.  |
        |Étape 2 |Pointez votre curseur sur la bibliothèque. |
        |Étape 3 |Cliquez sur `Paramètres de l'historique`. |
        |Étape 4 |Définissez la période de rétention. |

        </center>


=== "Historique des fichiers"

    Les instructions ci-dessous montrent comment rétablir une version précédente d'un fichier.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        |Étape 1 |Connectez-vous à l'interface web de Seahub. Naviguez vers le dossier contenant le fichier. |
        |Étape 2 |Pointez votre curseur sur le fichier. |
        |Étape 3 |Cliquez sur `Historique`. |
        |Étape 4 |Téléchargez, rétablissez ou visualisez toute ancienne version du fichier. Pour les fichiers texte, le contenu de deux versions peut être comparé. Notez que les anciennes versions d'un fichier ne peuvent pas être visualisées si elles dépassent la période de rétention.|

        </center>


=== "Corbeille"

    Vous trouverez ci-dessous des instructions sur la manière de rétablir des fichiers ou des dossiers supprimés depuis la corbeille.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        |Étape 1 |Connectez-vous dans l'interface web de Seahub. Naviguez jusqu'au dossier parent contenant le fichier ou le répertoire effacé. |
        |Étape 2 |Cliquez sur l'icône `corbeille` dans la barre d'opération de la bibliothèque. |
        |Étape 3 |Rétablissez le fichier ou le répertoire effacé depuis la liste. Notez que les fichiers et répertoires ne peuvent pas être rétablis s'ils ont été effacés avant la période de rétention. |

        </center>


=== "Images de bibliothèque"

    Les images des bibliothèques permettent de lister et de rétablir d'anciennes versions d'une bibliothèque entière, incluant toute la structure des fichiers et répertoires. Les instructions sont détaillées ci-après.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        |Étape 1 |Connectez-vous dans l'interface web de Seahub. Naviguez jusqu'à la bibliothèque. |
        |Étape 2 |Cliquez sur le bouton `Historique` dans la barre supérieure. |
        |Étape 3 |Sélectionnez un état précédent de la bibliothèque, et cliquez sur `Voir l'image`. |
        |Étape 4 |Téléchargez, rétablissez, ou visualisez n'importe quelle version précédente d'un fichier ou d'un répertoire. Si vous êtes le propriétaire de la bibliothèque, vous pouvez rétablir la bibliothèque en entier dans un état précédent. Notez que les images des bibliothèques ne peuvent être accédées au delà de la période de rétention. |

        </center>


<div style="margin-top:-20px;">
</div>

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    **Période de rétention** : afin de gagner de l'espace de stockage, Georg configure Seafile pour ne garder les anciennes versions de ses images que pour 2 mois.

    **Images de bibliothèque** : Georg souhaite aussi revenir à la structure du répertoire qui était en place avant qu'il ne créé le dossier "vacances". Il restaure donc une ancienne image de la bibliothèque.

    **Corbeille** : Tom restaure l'essai d'Orwell de la corbeille, après l'avoir supprimé par inadvertance.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5797ebdf-9075-435d-9aad-3a578d900e42" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="Seafile fichiers chiffrés" width="150px"></img> </center>

## Chiffrez vos fichiers

Seafile fournit un chiffrement de bout-en-bout. Les bibliothèques peuvent être chiffrées avec un mot de passe, limitant l'accès aux utilisateurs autorisés uniquement. Personne ne peut accéder au contenu des bibliothèques chiffrées, même pas un administrateur Seafile. Notez cependant que le chiffrement des bibliothèques ne chiffre que le contenu des fichiers, pas les répertoires ni les noms de fichiers.

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    Tom travaille sur un projet sensible. Il créé une bibliothèque chiffrée pour sécuriser les documents de son projet.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ea8ce4f0-58fb-4284-9072-5c60e2830cad" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_seafile.svg" alt="Seafile mise à jour" width="150px"></img> </center>

## Mettez Seafile à jour

Le processus de mise à jour de Seafile est principalement manuel et des étapes peuvent mal se passer. Ne prenez pas de risque, synchronisez toutes vos bibliothèques sur un appareil et [sauvegardez vos données](https://gofoss.net/fr/backups/) !

Assurez-vous de lire les [instructions de mise à jour de Seafile](https://manual.seafile.com/upgrade/upgrade/), de même que les [notes détaillées sur la mise à jour](https://manual.seafile.com/upgrade/upgrade_notes_for_9.0.x/). Vous trouverez ci-dessous un guide pour une mise à jour vers une version majeure.

??? tip "Guidez-moi au cours du processus de mise à jour"

    Pour ce tutoriel, nous supposerons que vous effectuez une mise à jour de la version 7.0.1 à la version 8.0.1. Cela s'appelle "mise à jour d'une version majeure". Notez que les "mises à jour de versions mineures", par exemple de 7.0.1 à 7.1.1, fonctionnent de la même manière.

    [Commencez par lire les notes de mise à jour](https://manual.seafile.com/upgrade/upgrade/), qui contiennent les paramètres spécifiques ou les changements qui doivent être appliqués avant ou pendant la mise à jour.

    [Téléchargez et décompressez la dernière version de Seafile Serveur pour Linux](https://www.seafile.com/en/download/). Dans cet exemple, cela serait la version 8.0.1. Assurez-vous d'ajuster la version en conséquence :

    ```bash
    sudo wget -4 https://download.seadrive.org/seafile-server_8.0.1_x86-64.tar.gz
    sudo tar -xvf seafile-server_*
    sudo mv seafile-server-* /var/www/seafile
    sudo mv seafile-server_* /var/www/seafile/installed
    ```

    Donnez les bons droits :

    ```bash
    sudo chown -R seafileadmin:seafileadmin /var/www/seafile
    sudo chmod -R 755 /var/www/seafile
    ```

    Vérifiez que tout est prêt :

    ```bash
    sudo ls -al /var/www/seafile
    ```

    L'architecture du répertoire devrait ressembler à ceci :

    ```bash
    /var/www/seafile/
    -- ccnet
    -- conf
    -- installed
    -- logs
    -- pids
    -- seafile-data
    -- seafile-server-7.0.1
    -- seafile-server-8.0.1
    -- seafile-server-latest
    -- seahub-data
    ```

    Arrêtez le serveur Seafile :

    ```bash
    sudo systemctl stop seafile
    sudo systemctl stop seahub
    ```

    Listez les scripts disponibles pour la mise à jour :

    ```bash
    sudo ls -al /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_*
    ```

    Vous devriez voir quelque chose comme ceci :

    ```bash
    ...
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.0_6.1.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.1_6.2.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.2_6.3.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_6.3_7.0.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.0_7.1.sh
    /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.1_8.0.sh
    ...
    ```

    Maintenant, lancez les scripts de mise à jour nécessaires, en commençant par votre version actuelle de Seafile et en continuant jusqu'à la dernière version. Dans cet exemple :

    * Nous devons d'abord lancer le script `upgrade_7.0_7.1.sh` pour mettre à jour de la version 7.0.1 vers la version 7.1.x
    * Nous devons ensuite lancer le script `upgrade_7.1_8.0.sh` pour mettre à jour de le version 7.1.x vers la version 8.0.1

    Commencez par le premier script de mise à jour :

    ```bash
    sudo bash /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.0_7.1.sh
    ```

    Le terminal devrait afficher quelque chose ressemblant à :

    ```bash
    -------------------------------------------------------------
    This script would upgrade your seafile server from 7.0 to 7.1
    Press [ENTER] to contiune
    -------------------------------------------------------------

    Updating seafile/seahub database ...

    [INFO] You are using MySQL
    [INFO] updating seahub database...
    Done

    migrating avatars ...

    Done

    updating /var/www/seafile/seafile-server-latest symbolic link to /var/www/seafile/seafile-server-7.1.0 ...
    ```

    Maintenant le script de mise à jour suivant :

    ```bash
    sudo bash /var/www/seafile/seafile-server-8.0.1/upgrade/upgrade_7.1_8.0.sh
    ```

    Le terminal devrait afficher quelque chose ressemblant à :

    ```bash
    -------------------------------------------------------------
    This script would upgrade your seafile server from 7.1 to 8.0
    Press [ENTER] to contiune
    -------------------------------------------------------------

    Updating seafile/seahub database ...

    [INFO] You are using MySQL
    [INFO] updating seahub database...
    Done

    migrating avatars ...

    Done

    updating /var/www/seafile/seafile-server-latest symbolic link to /var/www/seafile/seafile-server-8.0.1 ...
    ```

    Enfin, redémarrez le serveur Seafile :

    ```bash
    sudo systemctl start seafile
    sudo systemctl start seahub
    ```

    Si tout s'est bien passé, Seafile devrait fonctionner, avec un statut « Active » :

    ```bash
    sudo systemctl status seafile
    sudo systemctl status seahub
    ```

    C'est facultatif, mais si la nouvelle version fonctionne bien, vous pouvez supprimer la précédente :

    ```bash
    sudo rm -rf /var/www/seafile/seafile-server-7.0.1
    ```

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Seafile" width="150px"></img> </center>

## Assistance

Pour davantage de précisions, consultez le [manuel du server de Seafile](https://manual.seafile.com/) ou demandez de l'aide à la [communauté Seafile](https://forum.seafile.com/).

<br>
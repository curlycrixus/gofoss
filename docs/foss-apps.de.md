---
template: main.html
title: So installiert Ihr FOSS-Apps
description: Degooglen. Was ist F-Droid? Wie kann ich F-Droid installieren? Ist F-Droid sicher? Die besten F-Droid Apps. Was ist Aurora Store? Wie kann ich Aurora Store installieren?
---

# Top 75 Tracker-freie FOSS-Apps (FDroid & Aurora Store)

!!! level "Letzte Aktualisierung: Mai 2022. Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/mobile_3.png" alt="F-Droid" width="500px"></img>
</div>


## App-Store

<center>

| App-Store | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/fdroid.svg" alt="F-Droid" width="50px"></img> | [F-Droid](https://f-droid.org/de/) | Falls Ihr stolze BesitzerIn eines Android-Smartphones seid (einschließlich [CalyxOS](https://gofoss.net/de/calyxos/) oder [LineageOS](https://gofoss.net/de/lineageos/)), solltet Ihr F-Droid einen Besuch abstatten! Es handelt sich um einen App-Store, der ausschließlich freie und quelloffene Apps anbietet. Die Installation und Instandhaltung von F-Droid ist relativ einfach. Untenstehend findet Ihr eine ausführliche Anleitung. |
| <img src="../../assets/img/aurora.svg" alt="Aurora" width="50px"></img> | [Aurora](https://f-droid.org/de/packages/com.aurora.store/) | Nicht alle sinnvollen Android-Apps sind frei, quelloffen, trackerfrei oder auf F-Droid verfügbar. Das bedeutet nicht zwangsläufig, dass sie datenschutzschädlich sind. Es bedeutet auch nicht, dass Ihr Googles Play Store verwenden müsst, um solche Apps herunterzuladen. Installiert stattdessen Aurora Store, einen alternativen App-Store, in dem Ihr ohne Google-Konto Apps suchen, herunterladen und aktualisieren könnt. |
| <img src="../../assets/img/fossdroid.svg" alt="Fossdroid" width="50px"></img> | [Fossdroid](https://fossdroid.com/) |App-Store ähnlich wie F-Droid, bietet freie und quelloffene Android-Apps an. |

</center>


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für F-Droid"

    <center>

    | Schritte | Anweisungen |
    | ------ | ------ |
    | Herunterladen |Öffnet den Browser auf Eurem Smartphone, besucht [F-Droids Webseite](https://f-droid.org/de/) und ladet die `.apk`-Datei herunter. |
    | Installation aus unbekannten Quellen erlauben |Öffnet die heruntergeladene `.apk`-Datei. Ein Warnhinweis sollte angezeigt werden: *"Aus Sicherheitsgründen kannst du auf dem Smartphone keine unbekannten Apps aus dieser Quelle installieren"*. Um die Installation aus unbekannten Quellen vorübergehend zu erlauben, klickt auf `Einstellungen ‣ Dieser Quelle vertrauen`. |
    | F-Droid installieren | Verlasst die Einstellungsseite und klickt auf `Installieren`. |
    | Installation aus unbekannten Quellen verbieten |Begebt Euch in die `Einstellungen` Eures Smartphones und deaktiviert die Installation aus unbekannten Quellen. Je nach Telefon befindet sich diese Option unter `Einstellungen ‣ Apps & Benachrichtigungen ‣ Erweitert ‣ Spezieller App-Zugriff ‣ Unbek. Apps installieren`. Wählt Euren Browser oder Dateimanager und deaktiviert den Eintrag `Dieser Quelle vertrauen`. |
    | F-Droid starten |Nach dem ersten Start aktualisiert F-Droid das Software-Paketquelle. Es handelt sich um die Liste aller auf F-Droid verfügbaren Softwarepakete. Das kann eine Weile dauern, ein wenig Geduld ist gefordert. |
    | Automatische Updates aktivieren |Öffnet F-Droids `Einstellungen` und aktiviert die Optionen `Automatisch installieren` sowie `Aktualisierungsbenachrichtigungen`. |
    | Paketquellen manuell aktualisieren |Wischt auf dem Startbildschirm von F-Droid nach unten, um alle Paketquellen zu aktualisieren. |

    </center>


??? question "Können F-Droid bzw. FOSS-Apps auch auf dem iPhone installiert werden?"

    Besitzer von iOS-Geräten haben in dieser Hinsicht leider Pech. Apples abgeschirmtes Ökosystem macht es [praktisch inkompatibel mit mobilen FOSS-Apps](https://www.fsf.org/blogs/community/why-free-software-and-apples-iphone-dont-mix/). Unter dem Vorwand der Sicherheit schließt Apple mit [Copyright-Sperren](https://www.law.cornell.edu/uscode/text/17/1201) und [Zugriffskontrollen](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32001L0029:DE:HTML) jegliche Bastelei, Reparatur oder Diagnose an iOS-Geräten aus. Apple — nicht die NutzerInnen — darf entscheiden, auf welche Inhalte zugegriffen werden kann, welche Apps installiert werden dürfen und wann das Gerät weggeworfen werden muss.


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Browser" width="150px"></img> </center>

## Browser

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/firefox.svg" alt="Firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/de/firefox/browsers/mobile/) | Schneller, sicherer und privater Browser. *Hinweis*: [Enthält 3 Tracker (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/de/reports/org.mozilla.firefox/latest/). |
| <img src="../../assets/img/tor.svg" alt="Tor browser" width="50px"></img> | [Tor browser](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=de_DE/) |Tor Browser für Android. Blockiert Tracker, schützt vor Überwachung, widersteht digitalen Fingerabdrücken und verschlüsselt den Internetverkehr. *Hinweis*: [Enthält 3 Tracker (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/de/reports/org.torproject.torbrowser/latest/). |
| <img src="../../assets/img/fossbrowser.svg" alt="FOSS browser" width="50px"></img> | [FOSS browser](https://f-droid.org/de/packages/de.baumann.browser/) | Einfacher und ressourcenarmer Browser. Tracker-frei. |
| <img src="../../assets/img/fennec.svg" alt="Fennec" width="50px"></img> | [Fennec F-Droid](https://f-droid.org/de/packages/org.mozilla.fennec_fdroid/) |Datenschutzfreundliche Version von Firefox. Ist darauf ausgelegt, alle in den offiziellen Mozilla-Builds enthaltenen proprietären Elemente zu entfernen. *Hinweis*: Exodus Privacy meldet [2 Tracker (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/de/reports/org.mozilla.fennec_fdroid/latest/). Nach Angaben des Entwicklerteams handelt es sich dabei jedoch um einen [Fehlalarm](https://forum.f-droid.org/t/welcome-a-new-fennec-f-droid/11113): Die Tracker-Bibliotheken wurden durch sogenannte "Stubs" ersetzt, also durch wirkungsfreien Code. |
| <img src="../../assets/img/mull.svg" alt="Mull browser" width="50px"></img> | [Mull](https://f-droid.org/de/packages/us.spotco.fennec_dos/) | Firefox-Fork für Android. Von der DivestOS-Mannschaft entwickelt, basiert auf den Tor Uplift und arkenfox-user.js Projekten. Mull bietet Tracking- (Datenisolierung pro Webseite) und Fingerprinting-Schutz, unterstützt den Inhaltsfilter uBlock Origin und erhält regelmäßige Sicherheitsupdates. *Hinweis*: Exodus Privacy meldet [2 Tracker (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/de/reports/us.spotco.fennec_dos/latest/). Nach Angaben des Entwicklerteams handelt es sich dabei jedoch um einen [Fehlalarm](https://forum.f-droid.org/t/classyshark3exydus-found-five-trackers-inside-tor-browser/13453/20): Die Tracker-Bibliotheken wurden durch sogenannte "Stubs" ersetzt, also durch wirkungsfreien Code. |
| <img src="../../assets/img/bromite.svg" alt="Bromite" width="50px"></img> | [Bromite](https://www.bromite.org/) |Chromium-basierter Browser, inklusive Werbeblocker und erhöhten Datenschutz. Nicht über F-Droid erhältlich. |
| <img src="../../assets/img/webapps.svg" alt="Webapps" width="50px"></img> | [Webapps](https://f-droid.org/de/packages/com.tobykurien.webapps/) | Verwandelt Eure Lieblingswebseiten in sichere Anwendungen. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_signal.svg" alt="Chat-Apps" width="150px"></img> </center>

## Messenger

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/signal.svg" alt="Signal" width="50px"></img> | [Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=de_DE/) |Schneller, einfacher und sicherer Nachrichtenaustausch. [Durchgängig verschlüsselte](https://gofoss.net/de/encrypted-messages/) Text-, Sprach- und Videonachrichten, sowie Übermittlung von Dokumenten und Bildern. Quelloffen, werbefrei, Tracker-frei. *Hinweis*: benötigt die Angabe Eurer Telefonnummer. |
| <img src="../../assets/img/element.svg" alt="Element" width="50px"></img> | [Element](https://f-droid.org/de/packages/im.vector.app/) | Quelloffener Client für das Matrix-Protokoll. Matrix ist ein Netzwerk für sichere, [durchgängig verschlüsselte](https://gofoss.net/de/encrypted-messages/) und dezentrale Kommunikation. Kann [selbst gehostet](https://gofoss.net/de/ubuntu-server/) werden. Tracker-frei. |
| <img src="../../assets/img/briar.svg" alt="Briar" width="50px"></img> | [Briar](https://f-droid.org/de/packages/org.briarproject.briar.android/) | Quelloffener Messenger. Peer-to-Peer (stützt sich nicht auf zentrale Server), [durchgängig verschlüsselt](https://gofoss.net/de/encrypted-messages/), legt minimale Informationen offen. Nutzt Tor. Tracker-frei. |
| <img src="../../assets/img/jami.svg" alt="Jami" width="50px"></img> | [Jami](https://f-droid.org/de/packages/cx.ring/) | Quelloffener Messenger. Peer-to-Peer (stützt sich nicht auf zentrale Server), [durchgängig verschlüsselt](https://gofoss.net/de/encrypted-messages/). |
| <img src="../../assets/img/conversations.svg" alt="Conversations" width="50px"></img> | [Conversations](https://f-droid.org/de/packages/eu.siacs.conversations/) | Quelloffener Client für das XMPP-Protokoll. Ermöglicht sichere und dezentrale Kommunikation. Kann selbst gehostet werden. Tracker-frei. |
| <img src="../../assets/img/jitsi.svg" alt="Jitsi" width="50px"></img> | [Jitsi](https://f-droid.org/de/packages/org.jitsi.meet/) | Video- und Audiokonferenzdienst. Quelloffen und verschlüsselt. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Soziale Netwerke" width="150px"></img> </center>

## Soziale Netzwerke

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/tusky.svg" alt="Tusky" width="50px"></img> | [Tusky](https://f-droid.org/de/packages/com.keylesspalace.tusky/) | Ressourcenarmer Client für Mastodon, ein freies und quelloffenes soziales Netzwerk als Alternative zu Twitter oder Facebook. Teil des [Fediverses](https://gofoss.net/de/fediverse). Tracker-frei. |
| <img src="../../assets/img/pixelfed.svg" alt="Pixeldroid" width="50px"></img> | [Pixeldroid](https://f-droid.org/de/packages/org.pixeldroid.app/) | Client für Pixelfed, eine freie und quelloffene Alternative zu Instagram. Teil des [Fediverses](https://gofoss.net/de/fediverse). Tracker-frei. |
| <img src="../../assets/img/lemmur.svg" alt="Lemmur" width="50px"></img> | [Lemmur](https://f-droid.org/de/packages/com.krawieck.lemmur/) | Client für Lemmy, eine freie und quelloffene Alternative zu Reddit. Teil des [Fediverses](https://gofoss.net/de/fediverse). Tracker-frei. |
| <img src="../../assets/img/friendica.svg" alt="Friendica" width="50px"></img> | [Friendica](https://f-droid.org/de/packages/de.wikilab.android.friendica01/) | Client für Friendica, eine freie und quelloffene Alternative zu Facebook. Teil des [Fediverses](https://gofoss.net/de/fediverse). Tracker-frei. |
| <img src="../../assets/img/simplemusic.svg" alt="Funkwhale" width="50px"></img> | [Funkwhale](https://f-droid.org/de/packages/audio.funkwhale.ffa/) | Client für Funkwhale, eine freie und quelloffene Alternative zu YouTube, Spotify oder SoundCloud.Teil des [Fediverses](https://gofoss.net/de/fediverse).  Tracker-frei. |
| <img src="../../assets/img/redreader.svg" alt="RedReader" width="50px"></img> | [RedReader](https://f-droid.org/de/packages/org.quantumbadger.redreader/) | Freier und quelloffener Client für reddit.com. Keine Reklame. Tracker-frei. |
| <img src="../../assets/img/infinity.svg" alt="Infinity" width="50px"></img> | [Infinity for Reddit](https://f-droid.org/de/packages/ml.docilealligator.infinityforreddit/) | Freier und quelloffener Client für reddit.com. Keine Reklame. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="E-Mail" width="150px"></img> </center>

## E-Mail

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/protonmail.svg" alt="Protonmail" width="50px"></img> | [Protonmail](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=de_DE/) |Der weltweit größte sichere E-Mail-Dienst, entwickelt von Wissenschaftlern des CERN und des MIT. Quelloffen und durch Schweizer Datenschutzgesetze geschützt. Tracker-frei. |
| <img src="../../assets/img/tutanota.svg" alt="Tutanota" width="50px"></img> | [Tutanota](https://f-droid.org/de/packages/de.tutao.tutanota/) | Quelloffene E-Mail-App. Inklusive dunklem Thema, Verschlüsselung, Push-Benachrichtigungen, automatischer Synchronisierung, Volltextsuche, Wischgesten usw. Tracker-frei. |
| <img src="../../assets/img/k9mail.svg" alt="K-9 mail" width="50px"></img> | [K-9 mail](https://f-droid.org/de/packages/com.fsck.k9/) |Quelloffenene E-Mail-App. Unterstützt POP3 und IMAP, Push-Mail wird allerdings nur für IMAP unterstützt. Tracker-frei. |
| <img src="../../assets/img/libremmail.svg" alt="Librem mail" width="50px"></img> | [Librem mail](https://f-droid.org/de/packages/one.librem.mail/) | Quelloffene E-Mail-App, Fork von K-9 Mail. Inklusive Verschlüsselung und Multi-Konten-Unterstützung. Tracker-frei. |
| <img src="../../assets/img/simpleemail.svg" alt="Simple email" width="50px"></img> | [Simple email](https://f-droid.org/de/packages/org.dystopia.email/) | Quelloffene E-Mail-App. Datenschutzfreundlich, inklusive Verschlüsselung, Multi-Konten-Unterstützung, Zwei-Wege-Synchronisation, Offline-Speicher, dunklem Thema, serverseitiger Suche und einfachem Design. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Nachrichten" width="150px"></img> </center>

## Nachrichten

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/feeder.svg" alt="Feeder" width="50px"></img> | [Feeder](https://f-droid.org/de/packages/com.nononsenseapps.feeder/) | Quelloffener RSS-Feed-Reader. Kein Tracking, keine Kontoerstellung, Offline-Modus, Hintergrundsynchronisation und Benachrichtigungen. Tracker-frei. |
| <img src="../../assets/img/flym.svg" alt="Flym" width="50px"></img> | [Flym](https://f-droid.org/de/packages/net.frju.flym/) |Quelloffener RSS-Feed-Reader. Tracker-frei. |
| <img src="../../assets/img/antennapod.svg" alt="AntennaPod" width="50px"></img> | [AntennaPod](https://f-droid.org/de/packages/de.danoeh.antennapod/) |Funktionsreicher Podcast-Manager und -Player. Bietet sofortigen Zugriff auf Millionen von kostenlosen oder -pflichtigen Programmen, von unabhängigen Podcastern bis hin zu großen Verlagshäusern wie der BBC, NPR oder CNN. Tracker-frei. |
| <img src="../../assets/img/radiodroid.svg" alt="RadioDroid" width="50px"></img> | [RadioDroid](https://f-droid.org/de/packages/net.programmierecke.radiodroid2/) |Eine App zum Hören von Online-Radiosendern. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="Karten" width="150px"></img> </center>

## Karten & Reisen

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/osmand.svg" alt="Osmand" width="50px"></img> | [Osmand](https://f-droid.org/de/packages/net.osmand.plus/) |Quelloffene App für Online- sowie Offline-Karten und Navigation. Tracker-frei. |
| <img src="../../assets/img/maps.svg" alt="Organic Maps" width="50px"></img> | [Organic maps](https://f-droid.org/de/packages/app.organicmaps/) |Quelloffene App für Online- sowie Offline-Karten und Navigation. Fork von Maps.me. Tracker-frei. |
| <img src="../../assets/img/transportr.svg" alt="Transportr" width="50px"></img> | [Transportr](https://f-droid.org/de/packages/de.grobox.liberario/) |Quelloffene App für Fahrpläne des öffentlichen Nahverkehrs in Europa und anderen Regionen. *Hinweis*: [Enthält 1 Tracker (mapbox)](https://reports.exodus-privacy.eu.org/de/reports/de.grobox.liberario/latest/) |
| <img src="../../assets/img/money.svg" alt="SettleUp" width="50px"></img> | [Split it easy](https://f-droid.org/de/packages/com.nishantboro.splititeasy/) |Quelloffene App zur Verwaltung von Gruppenausgaben. Tracker-frei.  |

</center>


<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Multimedia" width="150px"></img> </center>

## Multimedia

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/opencamera.svg" alt="Open camera" width="50px"></img> | [Open camera](https://f-droid.org/de/packages/net.sourceforge.opencamera/) |Funktionsreiche Kamera-App mit Autostabilisierung, Multizoom-Touch, Blitz, Gesichtserkennung, Timer, Serienbildmodus, stummschaltbarem Auslöser und vielem mehr. Tracker-frei. |
| <img src="../../assets/img/simplegallery.svg" alt="Simple gallery" width="50px"></img> | [Simple gallery](https://f-droid.org/de/packages/com.simplemobiletools.gallery.pro/) |Hochgradig anpassbare Offline-Foto-Galerie. Kommt ohne Internetzugang aus, zum Wohle Eurer Privatsphäre. Erlaubt es Fotos zu organisieren und zu bearbeiten, gelöschte Dateien wiederherzustellen, Dateien zu schützen oder verstecken, sowie eine Vielzahl verschiedener Foto- und Videoformate zu betrachten (einschließlich RAW, SVG, usw.). Tracker-frei. |
| <img src="../../assets/img/simplemusic.svg" alt="Simple music player" width="50px"></img> | [Simple music player](https://f-droid.org/de/packages/com.simplemobiletools.musicplayer/) |Leicht zu steuernder Musikplayer, sei es über die Statusleiste, das Home-Screen-Widget oder die Kopfhörer. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. Tracker-frei. |
| <img src="../../assets/img/newpipe.svg" alt="Newpipe" width="50px"></img> | [Newpipe](https://f-droid.org/de/packages/org.schabi.newpipe/) | Ressourcenarme YouTube-App, ohne proprietäre API oder Googles Play-Dienste. Unterstützt ebenfalls PeerTube. Tracker-frei. |
| <img src="../../assets/img/peertube.svg" alt="Thorium" width="50px"></img> | [Thorium](https://f-droid.org/de/packages/net.schueller.peertube/) |PeerTube ist eine freie, quelloffene und dezentrale Video-Hosting-Plattform. Tracker-frei. |
| <img src="../../assets/img/totem.svg" alt="Free tube" width="50px"></img> | [Free tube](https://github.com/FreeTubeApp/FreeTube/) |Quelloffenen und datenschutzfreundliche Youtube-App. Nicht über F-Droid erhältlich. |

</center>


<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Produktivität" width="150px"></img> </center>

## Produktivität

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/documentviewer.svg" alt="Document viewer" width="50px"></img> | [Document viewer](https://f-droid.org/de/packages/org.sufficientlysecure.viewer/) |Zeigt verschiedene Dateiformate an, inklusive pdf, djvu, epub, xps und Comics (cbz, fb2). Tracker-frei. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Libre Office viewer" width="50px"></img> | [Libre Office viewer](https://f-droid.org/de/packages/org.documentfoundation.libreoffice/) |Zeigt docx, doc, xlsx, xls, pptx, ppt, odt, ods und odp Dateien an. Tracker-frei. |
| <img src="../../assets/img/simplenotes.svg" alt="Simple notes" width="50px"></img> | [Simple notes](https://f-droid.org/de/packages/com.simplemobiletools.notes.pro/) |Quelloffene Notiz-App, werbefrei und zugriffsarm. Anpassungsfähiges Design und Widgets. Tracker-frei. |
| <img src="../../assets/img/editor.svg" alt="Notepad" width="50px"></img> | [Notepad](https://f-droid.org/de/packages/com.farmerbb.notepad/) |Einfache, quelloffene Notiz-App. Tracker-frei. |
| <img src="../../assets/img/carnet.svg" alt="Carnet" width="50px"></img> | [Carnet](https://f-droid.org/de/packages/com.spisoft.quicknote/) |Funktionsreiche, quelloffene Notiz-App, inklusive Synchronisationsmöglichkeiten (z.B. NextCloud) und Online-Editor. Tracker-frei. |
| <img src="../../assets/img/markor.svg" alt="Markor" width="50px"></img> | [Markor](https://f-droid.org/de/packages/net.gsantner.markor/) |Quelloffener Texteditor mit Markdown-Unterstützung. Tracker-frei. |
| <img src="../../assets/img/joplin.svg" alt="Joplin" width="50px"></img> | [Joplin](https://play.google.com/store/apps/details?id=net.cozic.joplin&hl=de) |Quelloffene, verschlüsselte Notiz-App, inklusive Synchronisationsmöglichkeiten (z.B. NextCloud). Tracker-frei. |
| <img src="../../assets/img/standardnotes.svg" alt="Standard notes" width="50px"></img> | [Standard notes](https://f-droid.org/de/packages/com.standardnotes/) |Freie, quelloffene und verschlüsselte Notiz-App. Tracker-frei. |
| <img src="../../assets/img/simplecalendar.svg" alt="Simple calendar" width="50px"></img> | [Simple calendar](https://f-droid.org/de/packages/com.simplemobiletools.calendar.pro/) |Anpassungsfähiger Offline-Kalender zur Organisation von einmaligen oder wiederkehrenden Ereignissen, Geburtstagen, Jahrestagen, Geschäftsterminen, und so weiter. Tages-, Wochen- und Monatsansichten verfügbar. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. Wenn Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/) könnt Ihr diese App zur [Verwaltung und Synchronisierung Eurer Kalender](https://gofoss.net/de/contacts-calendars-tasks/) verwenden. Tracker-frei. |
| <img src="../../assets/img/doodle.svg" alt="Etar" width="50px"></img> | [Etar](https://f-droid.org/de/packages/ws.xsoh.etar/) |Quelloffener Kalender im Materialdesign. Funktioniert auch mit Online-Kalendern. Frei, quelloffen und werbefrei. Tracker-frei. |
| <img src="../../assets/img/simplecontacts.svg" alt="Simple contacts" width="50px"></img> | [Simple contacts](https://f-droid.org/de/packages/com.simplemobiletools.contacts.pro/) |Einfache App zur Verwaltung Eurer Kontakte. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. Kontakte können lokal gespeichert oder mit der Cloud synchronisiert werden. Wenn Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/) könnt Ihr diese App zur [Verwaltung und Synchronisierung Eurer Kontakte](https://gofoss.net/de/contacts-calendars-tasks/) verwenden. Tracker-frei. |
| <img src="../../assets/img/opencontacts.svg" alt="Open contacts" width="50px"></img> | [Open contacts](https://f-droid.org/de/packages/opencontacts.open.com.opencontacts/) |Quelloffene Kontakt-App. Tracker-frei. |
| <img src="../../assets/img/opentasks.svg" alt="Open tasks" width="50px"></img> | [OpenTasks](https://f-droid.org/de/packages/org.dmfs.tasks/) |Quelloffener Task-Manager. Wenn Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/) könnt Ihr diese App zur [Verwaltung und Synchronisierung Eurer Aufgabenlisten](https://gofoss.net/de/contacts-calendars-tasks/) verwenden. Tracker-frei. |
| <img src="../../assets/img/tasks.svg" alt="Tasks.org" width="50px"></img> | [Tasks.org](https://f-droid.org/de/packages/org.tasks/) |Quelloffener Task-Manager. Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/), erlaubt Euch diese App [Eure Aufgabenlisten zu verwalten und zu synchronisieren](https://gofoss.net/de/contacts-calendars-tasks/). *Hinweis*: [Enthält 1 Tracker (OpenTelemetry).](https://reports.exodus-privacy.eu.org/de/reports/org.tasks/latest/) |
| <img src="../../assets/img/davx5.svg" alt="Davx5" width="50px"></img> | [Davx5](https://f-droid.org/de/packages/at.bitfire.davdroid/) | Quelloffener Client zum Synchronisieren von Kontakten, Kalendern und Aufgabenlisten. Kann [selbst gehostet](https://gofoss.net/de/contacts-calendars-tasks/), oder mit einem vertrauenswürdigen Hoster verwendet werden. Tracker-frei. |
| <img src="../../assets/img/simplecalculator.svg" alt="Simple calculator" width="50px"></img> | [Simple calculator](https://f-droid.org/de/packages/com.simplemobiletools.calculator/) |Quelloffener Rechner, werbefrei und zugriffsarm. Anpassungsfähiges Design. Tracker-frei. |
| <img src="../../assets/img/simpleclock.svg" alt="Simple clock" width="50px"></img> | [Simple clock](https://f-droid.org/de/packages/com.simplemobiletools.clock/) |Uhr, Alarm, Stoppuhr, Timer. Quelloffen, werbefrei und zugriffsarm. Anpassungsfähiges Design. Tracker-frei. |
| <img src="../../assets/img/alarm.svg" alt="Simple alarm clock" width="50px"></img> | [Simple alarm clock](https://play.google.com/store/apps/details?id=com.better.alarm&hl=de) |Quelloffener und funktionsreicher Wecker mit einer übersichtlichen Oberfläche. Tracker-frei. |
| <img src="../../assets/img/quickdic.svg" alt="Quick dic" width="50px"></img> | [Quick dic](https://f-droid.org/de/packages/de.reimardoeffinger.quickdic/) |Offline-Wörterbuch. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_letsencrypt.svg" alt="Authentifizierung" width="150px"></img> </center>

## Authentifizierung

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/https.svg" alt="Keepass DX" width="50px"></img> | [Keepass DX](https://f-droid.org/de/packages/com.kunzisoft.keepass.libre/) |Sicherer und quelloffener Passwortmanager. Tracker-frei. |
| <img src="../../assets/img/andotp.svg" alt="andOTP" width="50px"></img> | [andOTP](https://f-droid.org/de/packages/org.shadowice.flocke.andotp/) |Freie und quelloffene Anwendung zur Zwei-Faktor-Authentifizierung. Tracker-frei. |
| <img src="../../assets/img/freeotp.svg" alt="Free OTP" width="50px"></img> | [Free OTP](https://f-droid.org/de/packages/org.liberty.android.freeotpplus/) | Quelloffener Zwei-Faktor-Authentifikator. Tracker-frei. |
| <img src="../../assets/img/aegis.svg" alt="Aegis" width="50px"></img> | [Aegis](https://f-droid.org/de/packages/com.beemdevelopment.aegis/) |Freier, sicherer und quelloffener Zwei-Faktor-Authentifikator. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_simplefilemanager.svg" alt="Datenspeicherung" width="150px"></img> </center>

## Datenspeicherung

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/simplefilemanager.svg" alt="Simple file manager" width="50px"></img> | [Simple file manager](https://f-droid.org/de/packages/com.simplemobiletools.filemanager.pro/) |Durchsucht und bearbeitet Dateien auf Eurem Telefon. Quelloffen, werbefrei und mit anpassungsfähigem Design. Tracker-frei. |
| <img src="../../assets/img/droidfs.svg" alt="DroidFS" width="50px"></img> | [DroidFS](https://f-droid.org/de/packages/sushi.hardcore.droidfs/) |Quelloffene App zum sicheren Speichern und Zugreifen auf Eure verschlüsselten Dateien. Tracker-frei. |
| <img src="../../assets/img/seafile.svg" alt="Seafile" width="50px"></img> | [Seafile](https://f-droid.org/de/packages/com.seafile.seadroid2/) |Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/), erlaubt Euch diese App [Eure Cloud-Dateien zu verwalten und zu synchronisieren](https://gofoss.net/de/cloud-storage/). Tracker-frei. |
| <img src="../../assets/img/letsencrypt.svg" alt="Cryptomator" width="50px"></img> | [Cryptomator](https://cryptomator.org/de/android/) |Verschlüsselt Eure Cloud-Daten. Quelloffene und kostenpflichtige App. |

</center>


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="VPN" width="150px"></img> </center>

## VPN

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/openvpn.svg" alt="Open VPN" width="50px"></img> | [Open VPN](https://f-droid.org/de/packages/de.blinkt.openvpn/) |Falls Ihr [Euren eigenen Server hostet](https://gofoss.net/de/intro-free-your-cloud/), erlaubt Euch diese App eine VPN-Verbindung herzustellen. Tracker-frei. |
| <img src="../../assets/img/protonvpn.svg" alt="Proton VPN" width="50px"></img> | [Proton VPN](https://f-droid.org/de/packages/ch.protonvpn.android/) |Sicherer und (teilweise) kostenloser VPN-Anbieter. Gibt an, keine Benutzeraktivitäten zu erfassen. Bietet Verschlüsselung, Schweizer Datenschutzgesetze, DNS-Leckschutz, Kill-Switch und mehr. Tracker-frei. |
| <img src="../../assets/img/mullvad.svg" alt="Mullvad VPN" width="50px"></img> | [Mullvad VPN](https://f-droid.org/de/packages/net.mullvad.mullvadvpn/) |Sicherer VPN-Anbieter, gibt an, keine Benutzeraktivitäten zu erfassen. Abonnements beginnen bei etwa 5 €/Monat. Mullvad akzeptiert anonyme Zahlungen und erfordert keine E-Mail zur Anmeldung. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_games.svg" alt="Tastaturen" width="150px"></img> </center>

## Tastaturen

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/florisboard.svg" alt="Florisboard" width="50px"></img> | [Florisboard](https://f-droid.org/de/packages/dev.patrickgold.florisboard/) |Quelloffene und datenschutzfreudliche Tastatur, unterstützt mehrere Sprachen, Gesten, Nachtmodus und verschiedene Themen. Tracker-frei. |
| <img src="../../assets/img/anysoft.svg" alt="Anysoft keyboard" width="50px"></img> | [Anysoft keyboard](https://f-droid.org/de/packages/com.menny.android.anysoftkeyboard/) |Quelloffene und datenschutzfreudliche Tastatur, unterstützt mehrere Sprachen, Spracheingabe, Gesten, Nachtmodus und verschiedene Themen. Tracker-frei. |
| <img src="../../assets/img/simplekeyboard.svg" alt="Simple keyboard" width="50px"></img> | [Simple keyboard](https://f-droid.org/de/packages/rkr.simplekeyboard.inputmethod/) |Quelloffene Tastatur. Tracker-frei. |
| <img src="../../assets/img/openboard.svg" alt="Open board" width="50px"></img> | [Open board](https://f-droid.org/de/packages/org.dslul.openboard.inputmethod.latin/) |Quelloffene Tastatur. Tracker-frei. |
| <img src="../../assets/img/hackerskeyboard.svg" alt="Hacker's keyboard" width="50px"></img> | [Hacker's keyboard](https://f-droid.org/de/packages/org.pocketworkstation.pckeyboard/) |Quelloffene Tastatur mit separaten Zifferntasten, Interpunktionszeichen sowie Pfeiltasten. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Weitere Apps" width="150px"></img> </center>

## Weitere Apps

<center>

| FOSS-App | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/exodus.svg" alt="Exodus" width="50px"></img> | [Exodus](https://f-droid.org/de/packages/org.eu.exodus_privacy.exodusprivacy/) |Findet heraus, welche Tracker und Berechtigungen Eure Apps enthalten. Tracker-frei. |
| <img src="../../assets/img/zimlauncher.svg" alt="Zim launcher" width="50px"></img> | [Zim launcher](https://f-droid.org/de/packages/org.zimmob.zimlx/) |Werbefreie und quelloffene Launcher-App. Tracker-frei. |
| <img src="../../assets/img/lawnchair.svg" alt="Lawnchair" width="50px"></img> | [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah&hl=de) |Freier und quelloffener Launcher. Symbolgröße, Schriftart, Symbole, Benachrichtigungen usw. können angepasst werden. Tracker-frei. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten finden Ihr:

* in der [F-Droid Dokumentation](https://f-droid.org/de/docs/)
* Ihr könnt auch gerne die [F-Droid Gemeinschaft](https://forum.f-droid.org/) um Unterstützung bitten
* oder aber die [Reddit Gemeinschaft](https://teddit.net/r/fossdroid/)


<br>
---
template: main.html
title: Comment configurer un domaine sécurisé
description: Auto-hébergement. DuckDNS. Let's Encrypt. Installer OpenVPN ? Qu'est-ce que Pi-hole ? Qu'est-ce qu'un hôte virtuel Apache ? Qu'est-ce qu'un reverse proxy ?
---

# Accédez à votre serveur à distance et en toute sécurité

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/pihole.png" alt="Installer Pi-hole" width="450px"></img>
</div>

Après avoir installé avec succès votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/) et configuré les mesures de sécurité [de base](https://gofoss.net/fr/server-hardening-basics/) et [avancées](https://gofoss.net/fr/server-hardening-advanced/), ce chapitre explique comment :

* faire pointer un (sous-)domaine vers votre adresse IP personnelle avec DuckDNS
* chiffrer le trafic avec Let's Encrypt
* accéder à des services locaux, utiliser un fournisseur DNS respectant la vie privée et bloquer les publicités avec Pi-hole
* protéger votre serveur avec un proxy inverse
* accéder à votre serveur de partout dans le monde et en toute sécurité avec OpenVPN


## Configurer un domaine avec DuckDNS

<div align="center">
<img src="../../assets/img/duckdns.png" alt="DuckDNS" width="500px"></img>
</div>

Les adresses lisibles sont plus faciles à retenir que des adresses IP aléatoires. Plutôt que de taper `18.192.76.182` dans le navigateur, la plupart des utilisateurs préfèrent naviguer vers [www.gofoss.net](https://gofoss.net/fr/). Pour que cela fonctionne aussi avec votre serveur, il vous faut un nom unique qui pointe vers votre adresse IP personnelle, également appelé *nom de domaine*. En outre, un nom de domaine est nécessaire pour chiffrer le trafic, comme expliqué par la suite.

[DuckDNS](https://www.duckdns.org/) permet d'enregistrer gratuitement un (sous-)domaine et de le faire pointer dynamiquement vers votre adresse IP personnelle. Ce service fonctionne même si votre adresse IP change souvent. Il suffit de se rendre sur le site de DuckDNS et d'enregistrer le (sous-)domaine de votre choix. Vous recevrez ensuite un jeton unique (en anglais, « token »).

Dans le cadre de ce tutoriel, nous avons enregistré le (sous-)domaine `gofoss.duckdns.org` et reçu le jeton `ebaa3bd3-177c-4230-8d91-a7a946b5a51e`. Cela nous permettra plus tard d'accéder aux services du serveur via des adresses telles que `https://monservice.gofoss.duckdns.org`.

<br>

<center> <img src="../../assets/img/separator_letsencrypt.svg" alt="Certificats Let's Encrypt" width="150px"></img> </center>

## Chiffrer le trafic avec Let's Encrypt

Tout trafic devrait être chiffré via HTTPS. La barre d'adresse de votre navigateur devrait à tout moment afficher une connexion sécurisée, que vous naviguez sur Internet ou que vous accédez à vos services auto-hébergés. Cela signifie que vous avez besoin de certificats SSL – vous pouvez en achetez ou en obtenir gratuitement auprès de [Let's Encrypt](https://letsencrypt.org/fr/), une [autorité de certification](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification) de confiance. Vous trouverez des instructions détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Installer Dehydrated

    Let's Encrypt doit [vérifier que vous êtes réellement propriétaire du domaine](https://letsencrypt.org/fr/docs/challenge-types/) avant de délivrer un certificat. Un programme astucieux appelé [Dehydrated](https://github.com/dehydrated-io/dehydrated/) rend cela possible. Connectez-vous à votre serveur et téléchargez Dehydrated :

    ```bash
    sudo apt install git
    git clone https://github.com/dehydrated-io/dehydrated.git
    ```

    Allez dans le répertoire `dehydrated` et créer un premier fichier de configuration :

    ```bash
    cd dehydrated
    vi domains.txt
    ```

    Ajoutez la ligne suivante, et assurez-vous de saisir votre propre nom de domaine :

    ```bash
    *.gofoss.duckdns.org > gofoss.duckdns.org
    ```

    Enregistrez et fermez le fichier (`:wq!`), puis créez un second fichier de configuration :

    ```bash
    vi config
    ```

    Ajoutez les lignes suivantes. Veillez à saisir votre propre adresse électronique :

    ```bash
    CHALLENGETYPE="dns-01"
    BASEDIR=/etc/dehydrated
    HOOK="${BASEDIR}/hook.sh"
    CONTACT_EMAIL=gofoss@gofoss.net
    ```

    Enregistrez et fermez le fichier (`:wq!`), puis créez un troisième fichier de configuration :

    ```bash
    vi hook.sh
    ```

    Ajoutez les lignes suivantes. Assurez-vous de saisir votre propre nom de domaine, ainsi que votre propre jeton délivré par DuckDNS :

    ```bash
    DOMAIN="gofoss.duckdns.org"                         # indiquez votre domaine ici
    TOKEN="ebaa3bd3-177c-4230-8d91-a7a946b5a51e"        # indiquez votre jeton ici
    case "$1" in
        "deploy_challenge")
            curl "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=$4"
            echo
            ;;
        "clean_challenge")
            curl "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=removed&clear=true"
            echo
            ;;
        "deploy_cert")
            ;;
        "unchanged_cert")
            ;;
        "startup_hook")
            ;;
        "exit_hook")
            ;;
        *)
            echo Unknown hook "${1}"
            exit 0
            ;;
    esac
    ```

    Enregistrez et fermez le fichier (`:wq!`).

    **Attention :** Les instructions ci-dessus doivent être modifiées si vous utilisez un autre domaine que DuckDNS. Veuillez consulter la [documentation de Dehydrated](https://github.com/dehydrated-io/dehydrated/wiki/) ou la [communauté de Let's Encrypt](https://community.letsencrypt.org/) pour configurer correctement la validation DNS.

    La prochaine série de commandes crée un répertoire où tous les certificats SSL seront stockés, appelé `certs`. Les fichiers `dehydrated` et `hook.sh` sont rendus exécutables. Le répertoire `dehydrated` est déplacé vers `/etc/dehydrated`. Enfin, `gofossadmin` reçoit la propriété du répertoire `/etc/dehydrated`. Assurez-vous d'ajuster le nom de l'administrateur à votre propre configuration :

    ```bash
    mkdir certs
    chmod a+x dehydrated
    chmod a+x hook.sh
    cd ..
    sudo mv dehydrated /etc/dehydrated
    sudo chown -R gofossadmin:gofossadmin /etc/dehydrated
    ```

    Vérifiez le contenu du répertoire :

    ```bash
    sudo ls -al /etc/dehydrated
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    drwxr-xr-x 2 gofossadmin gofossadmin   4096 Jan 01 00:00 .
    drwxr-xr-x 4 gofossadmin gofossadmin   4096 Jan 01 00:00 ..
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 certs
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 config
    -rwxr-xr-x 1 gofossadmin gofossadmin 700000 Jan 01 00:00 dehydrated
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 docs
    -rw-r--r-- 1 gofossadmin gofossadmin     20 Jan 01 00:00 domains.txt
    -rwxr-xr-x 1 gofossadmin gofossadmin    700 Jan 01 00:00 hook.sh
    ```


    ### Créer les certificats

    Inscrivez-vous auprès de Let's Encrypt :

    ```bash
    bash /etc/dehydrated/dehydrated --register --accept-terms
    ```

    Cette commande crée le répertoire `/etc/dehydrated/accounts`, contenant vos informations d'enregistrement. Le terminal devrait afficher quelque chose comme :

    ```bash
    # INFO: Using main config file /etc/dehydrated/config
    + Generating account key...
    + Registering account key with ACME server...
    + Done!
    ```

    Maintenant, créez les certificats SSL :

    ```bash
    bash /etc/dehydrated/dehydrated -c
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    + Creating chain cache directory /etc/dehydrated/chains
    Processing gofoss.duckdns.org
    + Creating new directory /etc/dehydrated/certs/gofoss.duckdns.org ...
    + Signing domains...
    + Generating private key...
    + Generating signing request...
    + Requesting new certificate order from CA...
    + Received 1 authorizations URLs from the CA
    + Handling authorization for gofoss.duckdns.org
    + 1 pending challenge(s)
    + Deploying challenge tokens...
    OK
    + Responding to challenge for gofoss.duckdns.org authorization...
    + Challenge is valid!
    + Cleaning challenge tokens...
    OK
    + Requesting certificate...
    + Checking certificate...
    + Done!
    + Creating fullchain.pem...
    + Done!
    ```

    C'est fait ! Deux clés SSL ont été créées pour chiffrer tout le trafic vers et depuis vos services auto-hébergés. Nous expliquerons plus tard comment utiliser ces clés.

    <center>

    | Clés | Emplacement |
    | ------ | ------ |
    | Clé publique | /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem |
    | Clé privée | /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem |

    </center>


    ### Renouveler les certificats automatiquement

    Les certificats de Let's Encrypt expirent après 90 jours, et peuvent être renouvelés 30 jours avant leur expiration. Pour automatiser ces mises à jour, créez d'abord un fichier journal :

    ```bash
    sudo touch /var/log/dehydrated
    ```

    Ensuite, créez une tâche cron hebdomadaire qui vérifie la validité du certificat et, si nécessaire, le renouvelle :

    ```bash
    sudo vi /etc/cron.weekly/dehydrated
    ```

    Ajoutez le contenu suivant :

    ```bash
    #!/bin/sh

    MYLOG=/var/log/dehydrated
    echo "Checking cert renewals at `date`" >> $MYLOG
    /etc/dehydrated/dehydrated -c >> $MYLOG 2>&1
    ```

    Enregistrez et fermez le fichier (`:wq!`). Puis, rendez le script exécutable :

    ```bash
    sudo chmod +x /etc/cron.weekly/dehydrated
    ```

    Testez la tâche cron :

    ```bash
    sudo bash /etc/cron.weekly/dehydrated
    sudo cat /var/log/dehydrated
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    Checking cert renewals at Sun 01 Jan 2021 01:29:06 PM CEST
    # INFO: Using main config file /etc/dehydrated/config
    Processing *.gofoss.duckdns.org
     + Checking domain name(s) of existing cert... unchanged.
     + Checking expire date of existing cert...
     + Valid till Mar 12 10:23:18 2021 GMT (Longer than 30 days). Skipping renew!
    ```


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/8ab6fc0e-fee9-4ca4-acc5-aa143c967e41" frameborder="0" allowfullscreen></iframe>
    </center>


??? question "Qu'en est-il des certificats auto-signés ?"

    Il est possible d'utiliser des certificats auto-signés pour le chiffrement HTTPS. Cela dit, il arrive souvent que ce type de certificats ne soit pas reconnus par les navigateurs et les applications, ou génère des avertissements et des messages d'erreur. Sur les téléphones Android par exemple, les certificats auto-signés doivent d'abord être importés et approuvés. Pour ces raisons, nous suggérons de privilégier une autorité de certification comme Let's Encrypt.


<br>

<center> <img src="../../assets/img/separator_pihole.svg" alt="Pi-hole" width="150px"></img> </center>

##  Gérer le trafic et bloquer les publicités avec Pi-hole

<br>

<div align="center">
<img src="../../assets/img/pihole_interface.png" alt="Interface Pi-hole" width="500px"></img>
</div>

[Pi-hole](https://pi-hole.net/) est un outil très pratique : il est capable de rediriger vers des adresses locales telles que `https://monservice.gofoss.duckdns.org`, vous permet de choisir un fournisseur DNS et bloque les publicités et traqueurs. Suivez les instructions ci-dessous pour configurer Pi-hole sur votre serveur.

??? tip "Montrez-moi le guide étape par étape"

    ### Installation de Pi-hole

    Au moment d'écrire ces lignes, 5.10 était la dernière version de Pi-hole. Le logiciel est assez léger, 50 Mo d'espace libre et 512 Mo de RAM suffisent pour l'exécuter sur Ubuntu. Pi-hole nécessite également une adresse IP statique ainsi qu'Apache et PHP, conforme aux réglages que nous avons [configurés dans les chapitres précédents](https://gofoss.net/fr/server-hardening-basics/). Exécutez le script d'installation de Pi-hole :

    ```bash
    sudo curl -sSL https://install.pi-hole.net | bash
    ```

    Suivez les instructions à l'écran:

    <center>

    | Instruction | Description |
    | ------ | ------ |
    | This installer will transform your device into a network-wide ad blocker! | Appuyez sur `ENTRÉE` pour continuer. |
    | The Pi-hole is free, but powered by your donations | Appuyez sur `ENTRÉE` pour continuer. |
    | The Pi-hole is a SERVER so it needs a STATIC IP ADDRESS to function properly | Sélectionnez `Yes` et appuyez sur `ENTRÉE` pour continuer. |
    | Choose An Interface | Sélectionnez votre interface réseau. Appuyez sur `TAB` pour changer de sélection, `ESPACE` pour confirmer votre sélection, et `ENTRÉE` pour continuer. |
    | Select Upstream DNS Provider. To use your own, select Custom | Choisissez un fournisseur DNS. Dans cet exemple, nous utiliserons [UncensoredDNS](https://blog.uncensoreddns.org/), mais vous pouvez choisir n'importe quel autre fournisseur DNS ! Nous vous suggérons de choisir un fournisseur DNS autre que votre FAI, Cloudfare ou Google. Vous trouverez plus d'informations sur les fournisseurs DNS à la fin de cette section. |
    | Pi-hole relies on third party lists in order to block ads. | Appuyez sur `ENTRÉE` pour continuer. Nous vous expliquerons plus tard comment ajouter plus de listes de blocage. |
    | Do you wish to install the web admin interface? | Sélectionnez `On` si vous voulez une interface web (recommandé), et appuyez sur `ENTRÉE` pour continuer. |
    | Do you wish to install the web server (lighttpd)? | Nous allons utiliser Apache comme serveur web, au lieu de lighttpd. Sélectionnez `Off` et confirmez avec `ESPACE`, puis appuyez sur `ENTRÉE` pour continuer. |
    | Do you want to log queries? | Sélectionnez `On` (recommandé) et appuyez sur `ENTRÉE` pour continuer. |
    | Select a privacy mode for FTL | Sélectionnez le niveau de confidentialité de votre choix, et appuyez sur `ENTRÉE` pour continuer : <br><br> • `Show everything`: enregistre tout ; fournit le maximum de statistiques. <br> • `Hide domains`: affiche et stocke tous les domaines comme « cachés » (en anglais, « hidden ») ; désactive les panneaux « Top Domains » et « Top Ads » sur le tableau de bord. <br> • `Hide domains and clients`: affiche et enregistre tous les domaines comme « cachés » (en anglais, « hidden ») et tous les clients comme « 0.0.0.0 » ; désactive tous les panneaux du tableau de bord. <br> • `Anonymous mode`: désactive pratiquement tout, à l'exception des statistiques anonymes ; aucun historique n'est enregistré dans la base de données, rien n'apparaît dans le journal des requêtes, il n'y a pas de liste des principaux éléments; offre une meilleure confidentialité. |
    | Installation Complete! |Une fois l'installation terminée, vous devriez voir ceci :<br><br><center><img src="../../assets/img/pihole-installation.png" alt="Installation de Pi-hole" width="60%"></img></center> |

    </center>


    ### Interface web

    Nous allons configurer un hôte virtuel Apache comme proxy inverse pour accéder à l'interface web de Pi-hole. Cela semble complexe, mais en substance le rôle d'un proxy inverse est de protéger votre serveur en l'isolant d'avantage de la toile. Vous pouvez lire plus à ce sujet à la fin de cette section.

    Définissez les bonnes autorisations :

    ```bash
    sudo chown www-data:www-data -R /var/www/html/admin/
    sudo usermod -aG pihole www-data
    sudo chown -R pihole:pihole /etc/pihole
    ```

    Créez un fichier de configuration Apache :

    ```bash
    sudo vi /etc/apache2/sites-available/mypihole.gofoss.duckdns.org.conf
    ```

    Ajoutez le contenu suivant et assurez-vous de régler les paramètres selon votre propre configuration, comme les noms de domaines (`mypihole.gofoss.duckdns.org`), chemins vers les clés SSL, adresses IP et ainsi de suite :

    ```bash
    <VirtualHost *:80>

        ServerName              mypihole.gofoss.duckdns.org
        ServerAlias             www.mypihole.gofoss.duckdns.org
        Redirect permanent /    https://mypihole.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              mypihole.gofoss.duckdns.org
        ServerAlias             www.mypihole.gofoss.duckdns.org
        ServerSignature         Off

        SSLEngine               On
        SSLProxyEngine          On
        SSLProxyCheckPeerCN     Off
        SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
        SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
        DocumentRoot            /var/www/html/admin

        <Location />
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        <Directory /var/www/html/admin/>
            Options +FollowSymlinks
            AllowOverride All
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/mypihole.gofoss.duckdns.org-error.log
        CustomLog ${APACHE_LOG_DIR}/mypihole.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Une fois le contenu ajouté, sauvegardez et fermez le fichier (`:wq!`). Notez comment nous avons activé le chiffrement SSL avec l'instruction `SSLEngine On`, et utilisé le certificat SSL `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` ainsi que la clé privée SSL `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, que nous avions créés précédemment.

    Maintenant, activez l'hôte virtuel d'Apache et redémarrez Apache :

    ```bash
    sudo a2ensite mypihole.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Redémarrez Pi-hole et vérifiez que tout fonctionne correctement. Le terminal devait afficher "Active" :

    ```bash
    sudo systemctl restart pihole-FTL
    sudo systemctl status pihole-FTL
    ```


    ### Configuration de Pi-hole

    Modifiez le mot de passe par défaut de l'interface web de Pi-hole. [Choisissez un mot de passe fort et unique](https://gofoss.net/fr/passwords/) :

    ```bash
    sudo pihole -a -p
    ```

    Allez sur `http://192.168.1.100/admin/` et connectez-vous avec les nouveaux identifiants. Assurez-vous d'ajuster l'adresse IP du serveur à votre propre configuration. L'interface web devrait s'afficher à l'écran. Elle propose différents réglages, [décrits en détail sur le site de Pi-hole](https://github.com/pi-hole/AdminLTE/) :

    <center>

    | Réglage | Description |
    | ------ | ------ |
    | Gravity |Allez dans `Tools ‣ Update Gravity` pour récupérer les dernières listes de blocage. Soyez patient, cela peut prendre quelques secondes. |
    | Dashboard |Le tableau de bord affiche des statistiques : combien de domaines ont été visités ou bloqués, combien de domaines sont sur la liste de blocage, etc. |
    | Queries |Informations détaillées sur les requêtes. |
    | Blocklists |Allez sur `Group management ‣ Adlists` pour ajouter d'autres de listes de blocage et filtrer davantage de publicités et de logiciels malveillants. Vous trouverez plus d'informations sur les listes de blocage à la fin de cette section. |
    | Settings |Gérez et configurez Pi-hole : serveurs DNS, confidentialité, etc. |
    | Local DNS |Pi-hole est capable d'interpréter des adresses locales. Allez dans `Local DNS ‣ DNS Records` et ajoutez la combinaison domaine/IP suivante (ajustez en fonction de votre propre configuration) : <br><br>`Domain`: `mypihole.gofoss.duckdns.org` <br> `IP Address`: `192.168.1.100` |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (3min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/09477516-5382-44f7-bcb5-c846375e723e" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Solution de contournement temporaire pour Ubuntu 22.04"

    Au moment d'écrire ces lignes, [Pi-hole 5.10 n'est pas officiellement compatible avec Ubuntu 22.04](https://docs.pi-hole.net/main/prerequisites/). Pour cette raison, le script d'installation habituel de Pi-hole ne fonctionne pas correctement. En guise de [solution de contournement temporaire](https://github.com/pi-hole/pi-hole/issues/4693), exécutez plutôt les commandes suivantes :

    ```bash
    sudo bash
    curl -sSL https://install.pi-hole.net | PIHOLE_SKIP_OS_CHECK=true bash
    ```

    N'oubliez pas de quitter le shell bash une fois l'installation terminée. Une fois que Pi-hole est installé, il suffit de taper :

    ```
    exit
    ```


??? question "Dites-m'en plus sur le DNS et la confidentialité"

    <center><img src="../../assets/img/dns.jpg" alt="DNS" width="70%"></img></center>

    Sur la toile, chaque ordinateur possède sa propre [adresse IP](https://fr.wikipedia.org/wiki/Adresse_IP). Comme ces adresses ne sont pas faciles à retenir, on a inventé le système de noms de domaine (en anglais, [« Domain Name System » ou DNS](https://fr.wikipedia.org/wiki/Domain_Name_System)). Ça fonctionne un peu comme les anciennes centrales téléphoniques : chaque fois que vous saisissez l'adresse d'un site, un résolveur DNS recherche l'IP correspondante et connecte votre appareil au serveur approprié. Le DNS joue donc un rôle essentiel lors de la navigation sur la toile. Mais comme tout se passe automatiquement, les utilisateurs oublient souvent que les requêtes DNS représentent un risque pour leur vie privée :

    <center>

    | Risques | Description |
    | ------ | ------ |
    |Enregistrement |Les résolveurs DNS enregistrent tous les sites que vous visitez. Selon le résolveur DNS que vous utilisez — ou plutôt, qui vous a été imposé — [Google](https://fr.wikipedia.org/wiki/Google_Public_DNS), [Cloudflare](https://www.cloudflare.com/fr-fr/), votre fournisseur d'accès à Internet, votre opérateur télécoms ou tout autre tiers voit et stocke une liste de tous les sites que vous visitez. Que le trafic soit chiffré ou non via HTTPS n'a aucune importance. |
    |Pas de chiffrement |Les requêtes DNS ne sont généralement pas chiffrées. Même si vous faites entièrement confiance à votre résolveur DNS, d'autres personnes pourraient les intercepter pour essayer de vous manipuler ([attaque par usurpation d'identité](https://en.wikipedia.org/wiki/Spoofing_attack)). |
    |Censure |Les fournisseurs d'accès à Internet peuvent également censurer vos activités en ligne en suivant les requêtes DNS. |

    </center>

    Pi-hole vous permet de choisir le résolveur DNS auquel vous voulez faire confiance lorsque vous naviguez sur la toile. Voici quelques fournisseurs DNS soucieux du respect de la vie privée :

    <center>

    | Fournisseurs DNS | Pays | DNS #1 | DNS #2 |Politique de confidentialité |
    | ------ | ------ | ------  |------ |------ |
    | [Digitalcourage](https://digitalcourage.de/en) | Allemagne | 5.9.164.112 | -- |[Politique de confidentialité](https://digitalcourage.de/datenschutz-bei-digitalcourage) |
    | [UncensoredDNS](https://blog.uncensoreddns.org/) | Danemark |89.233.43.71 | 91.239.100.100 |[Politique de confidentialité](https://blog.uncensoreddns.org/faq/) |
    | [Dismail](https://dismail.de/info.html#dns/) | Allemagne |80.241.218.68 | 159.69.114.157 |[Politique de confidentialité](https://dismail.de/datenschutz.html#dns) |
    | [DNS Watch](https://dns.watch/) | Allemagne | 84.200.69.80 | 84.200.70.40 | -- |
    | [FDN](https://www.fdn.fr/actions/dns/) | France | 80.67.169.12 | 80.67.169.40 | -- |
    | [OpenNIC](https://servers.opennic.org/) | Divers | Divers | Divers | Divers |

    </center>


??? question "Dites-m'en plus sur le blocage des publicités"

    Pi-hole filtre le trafic web dans votre réseau et bloque l'affichage de publicités ainsi que les traqueurs. Tout cela sans que vous ayez à installer des logiciels spécifiques sur vos appareils. Rendez-vous sur l'interface web de Pi-hole pour ajouter les listes de blocage et filtrer davantage de publicités et logiciels malveillants. Notez cependant que Pi-Hole ne bloque pas les publicités des vidéos YouTube (pour cela, vous aurez besoin de [uBlock Origin](https://gofoss.net/fr/firefox/)).

    <center>

    | Listes de blocage | Description |
    | ------ | ------ |
    |Par défaut |• https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts <br>• https://mirror1.malwaredomains.com/files/justdomains <br>• http://sysctl.org/cameleon/hosts <br>• https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt |
    |Publicités |• https://adaway.org/hosts.txt <br>• https://v.firebog.net/hosts/AdguardDNS.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt <br>• https://v.firebog.net/hosts/Easylist.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/SpotifyAds/hosts <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts |
    |Traçage et télémétrie |• https://v.firebog.net/hosts/Airelle-trc.txt <br>• https://v.firebog.net/hosts/Easyprivacy.txt <br>• https://v.firebog.net/hosts/Prigent-Ads.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/tyzbit/hosts |
    |Sites malveillants |• https://v.firebog.net/hosts/Airelle-hrsk.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt <br>• https://mirror1.malwaredomains.com/files/justdomains <br>• https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt <br>• https://www.malwaredomainlist.com/hostslist/hosts.txt <br>• https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt <br>• https://v.firebog.net/hosts/Prigent-Malware.txt <br>• https://v.firebog.net/hosts/Prigent-Phishing.txt <br>• https://v.firebog.net/hosts/Shalla-mal.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts |

    </center>


??? question "Dites-m'en plus sur les proxies inverse"

    Un [proxy inverse](https://fr.wikipedia.org/wiki/Proxy_inverse) est une application placé entre un service qui tourne sur votre serveur et Internet. Tous les services présentés sur notre site, y compris Pi-hole, sont configurés comme des hôtes virtuels Apache derrière un proxy inverse. Cela permet :

    * d'empêcher que votre serveur soit directement exposé à l'Internet
    * d'exécuter plusieurs services côte à côte sur votre serveur
    * de minimiser le nombre de ports ouverts (on se limite aux ports 80 et 443)
    * d'accéder à vos services par des adresses personnalisées, plutôt que par des adresses IP et des numéros de port
    * de gérer plus facilement vos certificats SSL
    * de configurer des mesures de sécurité additionnelles, telles que les en-têtes HTTP, l'authentification des utilisateurs, les restrictions d'accès, etc.

    Dans Ubuntu, les fichiers de configuration de l'hôte virtuel Apache sont situés dans le répertoire `etc/apache2/sites-available`. Leur structure ressemble généralement à ceci :

    ```bash
    <VirtualHost *:80>

        ServerName              example.com                         # l'adresse du service
        ServerAlias             www.example.com                     # l'adresse du service, y compris le sous-domaine www
        Redirect permanent /    https://example.com/                # redirection de tout trafic non chiffré de http:// vers le trafic chiffré de https://

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              example.com                         # l'adresse du service
        ServerAlias             www.example.com                     # l'adresse du service, y compris le sous-domaine www
        ServerSignature         Off                                 # cache les informations du serveur

        SSLEngine               On                                  # utilise le protocole SSL/TLS
        SSLProxyEngine          On                                  # utilise le protocole SSL/TLS pour le proxy
        SSLCertificateFile      /path/to/fullchain.pem              # emplacement du certificat SSL au format PEM
        SSLCertificateKeyFile   /path/to/privkey.pem                # emplacement de la clé SSL privée codée via PEM

        DocumentRoot            /var/www/example                    # répertoire à partir duquel Apache servira les fichiers

        <Location />                                                # limite l'accès au serveur, au réseau domestique (LAN) et au VPN
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        <Directory /var/www/example>
            Options -Indexes +FollowSymLinks                        # empêche les index de répertoire et suit les liens symboliques
            AllowOverride All                                       # les directives du fichier .htaccess peuvent remplacer les directives de configuration
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/example.com-error.log            # les fichiers journaux
        CustomLog ${APACHE_LOG_DIR}/example.com-access.log combined # les fichiers journaux

    </VirtualHost>
    ```

    L'hôte virtuel est activé avec la commande `sudo a2ensite example.com`. La commande `sudo apachectl configtest` vous permet de vérifier au préalable si le fichier de configuration contient des erreurs de syntaxe.


??? question "Dites-m'en plus sur les commandes de terminal pour Pi-hole"

    La plupart des réglages de Pi-hole peuvent être effectuées via l'interface web. Vous pouvez également configurer Pi-hole directement sur le serveur, via le terminal. Vous trouverez la liste complète des commandes sur [le site de Pi-hole](https://docs.pi-hole.net/core/pihole-command/) :

    <center>

    | Commandes de terminal | Description |
    | ------ | ------ |
    | pihole status | Affichez le statut de Pi-hole. |
    | pihole -t tail log | Affichez le journal en temps réel. |
    | pihole -c | Affichez des statistiques. |
    | pihole -w -l | Affichez la liste des domaines mis sur liste blanche. |
    | pihole -w example.com | Ajoutez example.com à la liste blanche. |
    | pihole -w -d example.com | Retirez example.com de la liste blanche. |
    | pihole -b -l | Affichez la liste des domaines mis sur liste noire. |
    | pihole -b example.com | Ajoutez example.com à la liste noire. |
    | pihole -b -d example.com | Retirez example.com de la liste noire. |
    | pihole -up | Mettez à jour Pi-hole. |
    | pihole -l off | Désactivez l'enregistrement des requêtes. |
    | pihole -l on | Activez l'enregistrement des requêtes. |
    | pihole enable | Activez Pi-hole. |
    | pihole disable | Désactivez Pi-hole. |
    | pihole disable 10m | Désactivez Pi-hole pour 10 minutes. |
    | pihole disable 60s | Désactivez Pi-hole pour 60 secondes. |
    | pihole uninstall | Désinstallez Pi-hole. |

    </center>


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="Installer OpenVPN" width="150px"></img> </center>

## Accès distant sécurisé avec OpenVPN

À ce stade, le serveur n'est accessible que depuis votre réseau domestique, puisque le pare-feu l'isole d'Internet. Pour permettre un accès à distance, vous pourriez « percer » des trous dans le pare-feu et rediriger les ports de chaque service du routeur au serveur. Cependant, cela exposerait plusieurs ports à la toile et faciliterait potentiellement des attaques sur votre réseau.

Un réseau privé virtuel (en anglais, [« Virtual Private Network ou VPN »](https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9_virtuel)) permet de vous connecter à votre serveur depuis partout dans le monde, sans encourir ce genre de risque. En utilisant un VPN, seul un port est rendu public. En outre, seules les connexions préalablement autorisées sont possibles, et nécessitent à la fois un certificat et un mot de passe. Vous trouverez ci-dessous des instructions plus détaillées.

??? tip "Montrez-moi le guide étape par étape"

    ### Installer OpenVPN

    [OpenVPN](https://openvpn.net/) est un logiciel libre qui utilise OpenSSL, TLS et de nombreuses mesures de sécurité. OpenVPN peut être installé avec le [script suivant](https://github.com/Nyr/openvpn-install/) :

    ```bash
    wget https://git.io/vpn -O openvpn-install.sh
    sudo bash openvpn-install.sh
    ```

    Suivez les instructions à l'écran :

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | What is the public IPv4 address or hostname? | Saisissez votre adresse IPv4 publique, que vous trouverez par exemple sur [whatsmyip.org](https://www.monippublique.com/). Dans le cadre de ce tutoriel, supposons que votre adresse IP publique soit `88.888.88.88` (ajustez en conséquence). |
    | Which protocol do you want for OpenVPN connections? | Choisissez le protocole UDP recommandé. |
    | What port do you want OpenVPN listening to? | Choisissez un port, dans le cadre de ce tutoriel nous utiliserons le port standard `1194`. |
    | Which DNS do you want to use with the VPN? | Comme nous utilisons Pi-hole comme serveur DNS, sélectionnez l'option `Current system resolvers`. |
    | Finally, tell me your name for the client certificate. | Saisissez un nom pour votre premier certificat client VPN. Pour ce tutoriel, nous allons créer un certificat nommé `ordinateur_vpn`, ajustez en conséquence. |

    </center>


    Une fois l'installation terminée, le terminal devrait afficher quelque chose comme :

    ```bash
    Welcome to this OpenVPN road warrior installer!

    I need to ask you a few questions before starting setup.
    You can use the default options and just press enter if you are ok with them.

    This server is behind NAT. What is the public IPv4 address or hostname?
    Public IPv4 address / hostname [88.888.88.88]: 88.888.88.88

    What IPv6 address should the OpenVPN server use?
        1) 1a00:a0a:10a:10a0:a00:00ff:faf0:a011
        2) 1a00:a0a:10a:10a0:a00:00bb:faf0:a022
    IPv6 address [1]: 1

    Which protocol do you want for OpenVPN connections?
    1) UDP (recommended)
    2) TCP
    Protocol [1]: 1

    What port do you want OpenVPN listening to?
    Port [1194]: 1194

    Which DNS do you want to use with the VPN?
    1) Current system resolvers
    2) 1.1.1.1
    3) Google
    4) OpenDNS
    5) NTT
    6) AdGuard
    DNS [1]: 1

    Finally, tell me a name for the client certificate.
    Client name [client]: ordinateur_vpn

    We are ready to set up your OpenVPN server now.

    [etc.]

    Finished!

    The client configuration is available in: /root/ordinateur_vpn.ovpn
    New clients can be added by running this script again.
    ```


    ### Certificats

    Un premier certificat client VPN, `ordinateur_vpn.ovpn`, a été généré lors de l'installation d'OpenVPN. Vous devez créer un certificat pour chaque appareil qui souhaite se connecter au serveur: ordinateurs de bureau, ordinateurs portables, tablettes, téléphones, etc. Créez en autant que nécessaire en relançant plusieurs fois le script d'installation.

    Dans le cadre de ce tutoriel, nous allons créer un second certificat appelé `telephone_vpn.ovpn`. Bien sûr, vous pouvez choisir n'importe quel nom pour ces certificats. Assurez-vous simplement d'adapter les commandes en conséquence :

    ```bash
    sudo bash openvpn-install.sh
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    Looks like OpenVPN is already installed.

    What do you want to do?
    1) Add a new user
    2) Revoke an existing user
    3) Remove OpenVPN
    4) Exit
    Select an option: 1

    Tell me a name for the client certificate.
    Client name: telephone_vpn

    [...]

    Write out database with 1 new entries
    Data Base Updated

    Client telephone_vpn added, configuration is available at: /root/telephone_vpn.ovpn
    ```

    Enfin, déplacez le(s) certificat(s) dans le dossier personnel `gofossadmin` et définissez les autorisations correctes :

    ```bash
    cd
    sudo mv /root/ordinateur_vpn.ovpn .
    sudo mv /root/telephone_vpn.ovpn .
    sudo chmod 755 *.ovpn
    sudo chown gofossadmin:gofossadmin *.ovpn
    ```


    ### Configurer OpenVPN

    Ouvrez le port 1194 (UDP), ou le port que vous avez spécifié lors de l'installation d'OpenVPN :

    ```bash
    sudo ufw allow 1194/udp
    ```

    Vérifiez si le port a bien été ajouté aux règles du pare-feu :

    ```bash
    sudo ufw status numbered
    ```

    Vérifiez si l'interface virtuelle `tun0` fonctionne, et obtenez le nom du sous-réseau par défaut :

    ```bash
    ip ad | grep tun0
    ```

    Obtenez l'adresse IP du serveur OpenVPN :

    ```bash
    ip route | grep tun0
    ```

    Assurez-vous que le trafic est acheminé via le tunnel VPN :

    ```bash
    sudo apt install traceroute
    traceroute 10.8.0.1
    ```

    Puis, indiquez aux clients qui se connectent via VPN d'utiliser Pi-hole comme serveur DNS primaire. Pour cela, éditez le fichier de configuration OpenVPN :

    ```bash
    sudo vi /etc/openvpn/server/server.conf
    ```

    Commentez tous les paramètres `dhcp-option` existants (mettez un hashtag `#` au début de la ligne respective) et remplacez-les par l'interface `tun0` :

    ```bash
    #push "dhcp-option DNS XX.XX.XXX.XXX"
    #push "dhcp-option DNS XX.XX.XXX.XXX"
    push "dhcp-option DNS 10.8.0.1"
    ```

    Redémarrez le serveur OpenVPN :

    ```bash
    sudo systemctl restart openvpn-server@server
    ```

    Allez sur l'interface web de Pi-hole, dans notre exemple `http://192.168.1.100/admin` (ajustez en conséquence). Puis, allez dans `Settings ‣ DNS ‣ Interface settings` et sélectionnez l'option `Permit all origins`. Cliquez ensuite sur `Save`.

    !!! warning "Réglages de votre routeur"

        Vérifiez les réglages de votre routeur et assurez-vous que:

        * le port utilisé par OpenVPN est correctement transféré (dans notre exemple `1194`, ajustez en conséquence). Consultez le manuel de votre routeur pour plus d'informations
        * le port `53` est bien fermé. rechercher les ports ouverts à l'aide des outils de [Gibson Research Corporation](https://www.grc.com/x/ne.dll?bh0bkyd2) : sélectionnez `Proceed ‣ All service ports` et vérifiez que le port `53` n'apparaît pas comme `open`


    ### Clients

    Seuls des appareils dotés de certificats valides peuvent établir une connexion VPN pour accéder en toute sécurité à vos services auto-hébergés. Voici comment transférer les certificats précédemment générés du serveur vers ces appareils.

    === "Appareils de bureau (Ubuntu/Linux)"

        Nous supposons que l'appareil client fonctionne sous Ubuntu/Linux et peut établir un [accès SSH à distance](https://gofoss.net/fr/ubuntu-server/) avec le serveur. Ouvrez un terminal, connectez-vous en tant qu'utilisateur administrateur (à adapter en conséquence) et récupérez tous les certificats du serveur :

        ```bash
        su - gofossadmin
        scp -v -P 2222 gofossadmin@192.168.1.100:/home/gofossadmin/*.ovpn .
        ```

        Enfin, configurez la connexion réseau de l'appareil :

        <center>

        | Étapes | Instruction |
        | :------: | ------ |
        | 1 |Cliquez sur l'icône Wifi dans la barre supérieure. |
        | 2 |Cliquez sur `Paramètres`. |
        | 3 |Allez dans `Réseau`. |
        | 4 |Cliquez sur `+` dans la rubrique `VPN`. |
        | 5 |Sélectionnez `Importer depuis un fichier`. |
        | 6 |Naviguez jusqu'au certificat (dans cet exemple `ordinateur _vpn.ovpn`) et cliquez sur `Ouvrir`. |
        | 7 |Cliquez sur l'icône Wifi dans la barre supérieure. |
        | 8 |Activez la connexion VPN `ordinateur_vpn`. |

        </center>

        Et voilà ! Votre appareil peut établir une connexion VPN sécurisée à votre serveur, depuis partout dans le monde.


    === "Appareils mobiles (Android)"

        Ouvrez F-Droid sur votre téléphone et installez [OpenVPN pour Android](https://f-droid.org/fr/packages/de.blinkt.openvpn/).

        Connectez le le téléphone via USB à l'appareil avec lequel vous venez de récupérer les certificats VPN du serveur (voir ci-dessus). Copiez le certificat vers votre téléphone (dans cet exemple, le certificat s'appelle `telephone_vpn.ovpn`, adaptez en conséquence).

        Enfin, ouvrez l'appli OpenVPN sur votre téléphone et configurez-la :

        <center>

        | Étape | Instruction |
        | :------: | ------ |
        | 1 |Appuyez sur `+`, puis `Importer`. |
        | 2 |Cherchez le certificat (dans cet exemple `telephone_vpn.ovpn`) et importez-le. |

        </center>

        Et voilà ! Votre appareil peut établir une connexion VPN sécurisée à votre serveur, depuis partout dans le monde.


<br>

<center> <img src="../../assets/img/separator_tasks.svg" alt="Dernières vérifications" width="150px"></img> </center>

## Dernières vérifications

Une fois tout configuré, le trafic devrait être géré comme suit :

* Chaque fois qu'un de vos appareils consulte une adresse, il se connecte d'abord en toute sécurité à votre serveur via le VPN
* Si l'adresse contient des publicités ou des traqueurs, Pi-hole bloque ces éléments
* Si l'adresse pointe vers un service auto-hébergé, Pi-hole redirige la requête vers le service sur votre serveur
* Si l'adresse pointe vers un site ou un service externe, Pi-hole redirige la demande vers un serveur DNS de votre choix (par exemple Digitalcourage, UncensoredDNS, etc.).
* Tout le trafic est chiffré via HTTPS

Assurez-vous que tout fonctionne correctement :

* Allez sur [ads-blocker.com](https://ads-blocker.com/testing/) ou [voici.fr](https://www.voici.fr/) et vérifiez si les publicités sont bloquées. Vous pouvez également vous rendre sur `https://mypihole.gofoss.duckdns.org` (ajustez en conséquence) et vérifier le tableau de bord pour voir les requêtes bloquées
* Naviguez vers un service auto-hébergé, tel que `https://mypihole.gofoss.duckdns.org` (ajustez en conséquence), et vérifiez que vous êtes redirigé correctement
* Allez sur [dnsleaktest.com](https://www.dnsleaktest.com/) ou [bash.ws](https://bash.ws/dnsleak) et vérifiez que vous utilisez le bon résolveur DNS


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance" width="150px"></img> </center>

## Assistance

Pour davantage de précisions, consultez la [documentation de Pi-hole](https://docs.pi-hole.net/), [Let's Encrypt](https://letsencrypt.org/fr/docs/), [OpenVPN](https://openvpn.net/vpn-server-resources/) ou demandez de l'aide à la [communauté Pi-hole](https://discourse.pi-hole.net/), la [communauté Let's Encrypt](https://community.letsencrypt.org/) ou la [communauté OpenVPN](https://openvpn.net/community/).


</br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/google_announcement.png" alt="Annonce Google" width="250"></img>
</div>

</br>
---
template: main.html
title: Comment installer CalyxOS
description: Dégooglisez votre téléphone. Qu'est-ce que CalyxOS ? Comment installer CalyxOS ? Qu'est-ce que microg ? LineageOS vs CalysOS ? GrapheneOS vs CalyxOS ?
---

# CalyxOS, un système d'exploitation mobile respectant la vie privée

!!! level "Dernière mise à jour: mars 2022. Destiné aux utilisateurs expérimentés. Certaines compétences techniques sont requises."

<center>
<img align="center" src="../../assets/img/calyxos.png" alt="Comment installer CalyxOS" width ="600px"></img>
</center>


À l'instar de [LineageOS](https://gofoss.net/fr/lineageos/), CalyxOS est un système d'exploitation mobile Android respectant la vie privée, sans les Google Play Services. [CalyxOS](https://calyxos.org) propose des communications chiffrées de bout en bout, une navigation privée avec Tor et DuckDuckGo, un démarrage vérifié (en anglais, « verified boot ») et bien plus encore. L'utilisation de [microG](https://microg.org/) est facultative, il s'agit d'un clone libre de certaines bibliothèques et applications propriétaires de Google.

[CalyxOS](https://calyxos.org) est actuellement disponible pour les téléphones Pixel et Xiaomi Mi A2. Si vous détenez l'un de ces téléphones, assurez-vous que le modèle *exact* figure dans la [liste des appareils pris en charge par CalyxOS](https://calyxos.org/get/). Assurez-vous aussi que le « bootloader » (chargeur de démarrage) est temporairement « déblocable ». Aux États-Unis par exemple, le « bootloader » des téléphones Pixel Verizon ne peut pas être débloqué, ce qui empêche d'y installer CalyxOS.


!!! warning "Quelques mots sur la compatibilité et la sécurité"

    Soyez prudent lors de l'installation de CalyxOS sur votre appareil. Cela invalide la garantie de votre téléphone et peut, en cas d'erreur, même le rendre inutile. Vous êtes seul à assumer les risques de tout dommage ou perte de données qui pourrait survenir.

    Avant de passer à CalyxOS, souvenez-vous que même si quasiment tout marche bien, [quelques applications ne sont pas prises en charge](https://github.com/microg/GmsCore/wiki/Implementation-Status). Ceci inclut quelques applications provenant de Google, comme Android Wear, Google Fit, Google Cast ou Android Auto. Heureusement, des [alternatives libres](https://gofoss.net/fr/foss-apps/) sont disponibles. Enfin, n'oubliez pas qu'utiliser des applications payantes sans le Play Store de Google peut être compliqué.

    Notez également que CalyxOS a pris du retard sur les mises à jour de sécurité dans le passé. Nous vous recommandons de régulièrement [vérifier les mises à jour](https://calyxos.org/get/ota/). Pour certains téléphones, comme le Xiaomi Mi A2 ou les Pixels 2 et 2 XL, les composants propriétaires tels que le « bootloader » ou le « firmware » ne sont plus mis à jour. Enfin, des rapports ont également fait état de possibles failles de sécurité, comme des fuites dans le pare-feu Datura (par exemple, [fuites de données mobiles lorsque la connexion WiFi se déconnecte](https://gitlab.com/CalyxOS/calyxos/-/issues/572) ou [fuites DNS avec le DNS privé activé et le VPN désactivé](https://gitlab.com/CalyxOS/calyxos/-/issues/581)). Évaluez ces risques de sécurité potentiels à la lumière de votre [modèle de menace](https://ssd.eff.org/en/module/your-security-plan/).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Sauvegarde" width="150px"></img> </center>

## Sauvegarde

Toutes les données seront effacées de votre smartphone pendant l'installation. Ne prenez pas de risques, [sauvegardez votre téléphone](https://gofoss.net/fr/backups/)!

<br>


<center> <img src="../../assets/img/separator_bug.svg" alt="Flasher CalyxOS" width="150px"></img> </center>

## Préparation

### Micrologiciel

Démarrez votre ordinateur et [téléchargez le micrologiciel de CalysOS](https://calyxos.org/get/) pour votre appareil. Faites attention à bien choisir votre modèle *exact* et téléchargez le fichier `.zip` correspondant. Il devrait normalement s'appeler `XXX-factory-2021.XX.XX.XX.zip`.


### Flasher

Pour transférer — ou *flasher* — le micrologiciel de CalyxOS sur votre téléphone, vous devez disposer d'un flasher. Plus d'instructions sont données ci-après.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        [Téléchargez la dernière version du flasher pour Windows](https://github.com/AOSPAlliance/device-flasher/releases/). Le fichier devrait s'appeler `device-flasher.exe`. Téléchargez aussi les [pilotes USB Google](https://developer.android.com/studio/run/win-usb/) puis installez-les. Voilà les instructions pour Windows 10:

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez l'appareil Android à l'ordinateur via USB. |
        | 2 | Depuis l'explorateur Windows, ouvrez `Gestion de l'ordinateur`. |
        | 3 | Dans le panneau gauche de `Gestion de l'ordinateur`, sélectionnez `Gestionnaire de périphériques`. |
        | 4 | Dans le panneau droit du `Gestionnaire de périphériques`, trouvez `Périphériques Mobiles` ou `Autres Périphériques` et développez-le. |
        | 5 | Faites un clic droit sur le nom de votre appareil et sélectionnez `Mettre à jour le pilote`. |
        | 6 | Dans l'Assistant de mise à jour du matériel, sélectionnez `Parcourir mon ordinateur pour trouver le pilote` et cliquez sur `Suivant`. |
        | 7 | Cliquez sur `Parcourir` et naviguez jusqu'au dossier du pilote Google USB. Il devrait être situé dans `android_sdk\extras\google\usb_driver\`. |
        | 8 | Cliquez sur `Suivant` pour l'installer. |

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        [Téléchargez la dernière version du flasher pour macOS](https://github.com/AOSPAlliance/device-flasher/releases/) sur votre ordinateur. Le fichier devrait s'appeler `device-flasher.darwin`.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        [Téléchargez la dernière version du flasher pour Linux](https://github.com/AOSPAlliance/device-flasher/releases/) sur votre ordinateur. Le fichier devrait s'appeler `device-flasher.linux`.



### Débogage USB & Déverrouillage du matériel

Comme dit précédemment, le « bootloader » du Pixel doit être « déverrouillable ». En d'autres termes, vous devez être capable d'activer la fonction *Déverrouillage OEM* sur votre téléphone. Plus d'instructions sont données ci-après.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Description |
    | :------: | ------ |
    | 1 | Enlevez votre carte SIM et connectez-vous à un réseau wifi. |
    | 2 | Ouvrez le menu `Paramètres ► À propos du téléphone`. |
    | 3 | Appuyez sept fois sur `Numéro de build` pour activer les options développeur. |
    | 4 | Allez dans `Paramètres ► Système ► Options avancées ► Options développeur`. |
    | 5 | Défilez jusqu'en bas, puis cochez la case `Débogage Android` ou `Débogage USB`. |
    | 6 | Activez `Déverrouillage OEM` dans le menu `Options développeur`. |
    | 7 | Éteignez votre téléphone. Redémarrez le en mode *bootloader* ou *fastboot* en appuyant sur les boutons `Volume BAS` et `POWER` Relâchez-les dès que le mot `FASTBOOT` apparaît à l'écran. |
    | 8 | Connectez votre appareil Android à l'ordinateur. |
    | 9 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

    </center>

<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Installer CalyxOS" width="150px"></img> </center>


## Installation

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        <center>

        | Instructions | Description |
        | :------: | ------ |
        | Préparez les fichiers | Sur votre ordinateur, déposez le fichier de CalysOS `XXX-factory-2021.XX.XX.XX.zip` téléchargé précédemment, ainsi que le flasher `device-flasher.exe` dans le même dossier. |
        | Exécutez le flasher | Double cliquez sur le flasher pour l'exécuter et suivez les instructions à l'écran comme décrit ci-dessous. Ou alors, vous pouvez ouvrir un terminal de commande dans ce dossier et lancer le flasher avec la commande `.\device-flasher.exe` et suivre les instructions à l'écran comme décrit ci-dessous. |
        | Déverrouillez le « bootloader » | Utilisez les boutons de volume et d'allumage pour sélectionner `Déverrouiller le « bootloader » `. |
        | Attendez lors du flashage | Attendez que votre téléphone soit flashé, cela peut prendre du temps. Le téléphone va redémarrer plusieurs fois et montrer plusieurs écrans différents. *Soyez patient et surtout ne débranchez pas le téléphone pendant l'opération!* |
        | Verrouillez le « bootloader » | Lorsque les instructions vous le demandent, utilisez les boutons de volume et d'allumage pour sélectionner `Verrouiller le « bootloader »`. |
        | Attendez que le téléphone redémarre | Attendez jusqu'à ce que les instructions confirment que le flashage est complet. Le téléphone devrait redémarrer. Le premier démarrage peut prendre du temps. |
        | Félicitations! | CalyxOS est installé sur votre téléphone ! N'oubliez pas de désactiver `Déverrouillage OEM` dans le menu `Options développeur`. |

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        <center>

        | Instructions | Description |
        | :------: | ------ |
        | Préparez les fichiers | Sur votre ordinateur, déposez le fichier de CalysOS `XXX-factory-2021.XX.XX.XX.zip` téléchargé précédemment, ainsi que le flasher `device-flasher.darwin` dans le même dossier. |
        | Exécutez le flasher | Double-cliquez sur le flasher pour l'exécuter et suivez les instructions à l'écran comme décrit ci-dessous. Ou alors, vous pouvez ouvrir un terminal de commande dans ce dossier et lancer le flasher avec la commande `chmod +x device-flasher.darwin; ./device-flasher.darwin` et suivre les instructions à l'écran comme décrit ci-dessous.<br><br>*Note*: il est possible que vous deviez [désactiver le Gatekeeper de macOS](https://support.apple.com/fr-fr/HT202491) pour que l'installation fonctionne. |
        | Dévérrouillez le « bootloader » | Utilisez les boutons de volume et d'allumage pour sélectionner `Déverrouiller le « bootloader » `. |
        | Attendez lors du flashage | Attendez que votre téléphone soit flashé, cela peut prendre du temps. Le téléphone va redémarrer plusieurs fois et montrer plusieurs écrans différents. *Soyez patient et surtout ne débranchez pas le téléphone pendant l'opération!* |
        | Verrouillez le « bootloader » | Lorsque les instructions vous le demandent, utilisez les boutons de volume et d'allumage pour sélectionner `Verrouiller le « bootloader »`. |
        | Attendez que le téléphone redémarre | Attendez jusqu'à ce que les instructions confirment que le flashage est complet. Le téléphone devrait redémarrer. Le premier démarrage peut prendre du temps. |
        | Félicitations! | CalyxOS est installé sur votre téléphone ! N'oubliez pas de désactiver `Déverrouillage OEM` dans le menu `Options développeur`. |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | :------: | ------ |
        | Préparez les fichiers | Sur votre ordinateur, déposez le fichier de CalysOS `XXX-factory-2021.XX.XX.XX.zip` téléchargé précédemment, ainsi que le flasher `device-flasher.linux` dans le même dossier. |
        | Exécutez le flasher | Double-cliquez sur le flasher pour l'exécuter et suivez les instructions à l'écran comme décrit ci-dessous. Ou alors, vous pouvez ouvrir un terminal de commande dans ce dossier et lancer le flasher avec la commande `sudo chmod +x device-flasher.darwin; sudo ./device-flasher.darwin` et suivre les instructions à l'écran comme décrit ci-dessous. Plus d'instructions sont données ci-après. |
        | Dévérrouillez le « bootloader » | Utilisez les boutons de volume et d'allumage pour sélectionner `Déverrouiller le « bootloader » `. |
        | Attendez lors du flashage | Attendez que votre téléphone soit flashé, cela peut prendre du temps. Le téléphone va redémarrer plusieurs fois et montrer plusieurs écrans différents. *Soyez patient et surtout ne débranchez pas le téléphone pendant l'opération!* |
        | Verrouillez le « bootloader » | Lorsque les instructions vous le demandent, utilisez les boutons de volume et d'allumage pour sélectionner `Verrouiller le « bootloader »`. |
        | Attendez que le téléphone redémarre | Attendez jusqu'à ce que les instructions confirment que le flashage est complet. Le téléphone devrait redémarrer. Le premier démarrage peut prendre du temps. |
        | Félicitations! | CalyxOS est installé sur votre téléphone ! N'oubliez pas de désactiver `Déverrouillage OEM` dans le menu `Options développeur`. |

        </center>


<br>

<center> <img src="../../assets/img/separator_microg.svg" alt="microG" width="150px"></img> </center>

## microG

[microG](https://calyxos.org/tech/microg/) est une alternative open source à certaines bibliothèques et applications propriétaires de Google. En activant microG, vous pourrez continuer à utiliser des fonctionnalités comme les notifications instantanées ou la géolocalisation sans installer le « Google Services Framework (GSF)», gourmand en données. De nombreuses applis dépendantes du GSF peuvent ainsi être utilisées avec CalyxOS. Notez cependant qu'elles sont toujours susceptibles de contacter les services de Google. Pour limiter davantage les fuites de données, nous recommandons de privilégier autant que possible les [applis libres, open source et indépendantes du GSF](https://gofoss.net/fr/foss-apps/).


=== "Géolocalisation"

    Par défaut, CalyxOS est configuré pour utiliser la géolocalisation fournie par Mozilla. MicroG permet également à votre téléphone d'établir sa position à partir d'une base de données locale contenant des informations sur les antennes relais. Ce mode de géolocalisation est donc totalement indépendant de tierces parties. Vous trouverez des instructions détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Ouvrez F-Droid et installez l'application nommée `Local GSM location`. |
        | 2 | Ouvrez les paramètres de microG. Allez dans le menu `Modules de localisation` et activez `Service de localisation GSM` ainsi que `Nominatim`. |
        | 3 | Allez dans `Auto-Test` et vérifiez que tout marche correctement. |

        </center>


=== "Notifications instantanées"

    CalyxOS essaie d'éviter les services Google autant que possible. Il y a une exception : beaucoup d'applications reposent sur « Google Cloud Messaging (GCM) », un système propriétaire développé par Google pour afficher des notifications sur votre appareil. microG permet d'avoir accès à ces notifications instantanées en activant (de manière limitée) les services GCM. Vous trouverez plus de détails ci-après.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Ouvrez l'application de paramètres de microG. |
        | 2 | Naviguez vers `Enregistrement du terminal auprès de Google` et enregistrez votre appareil. |
        | 3 | Naviguez vers `Cloud Messaging` et activez les notifications instantanées. |

        </center>

    ??? question "Comment faire confiance à CalyxOS si le système utilise « Google Cloud Messaging » ?"

        Enregistrer votre appareil et activer les notifications instantanées est facultatif. En le faisant, vous dévoilez des informations limitées à Google, comme un identifiant unique. microG s'assure cependant d'enlever autant d'éléments identifiant que possible. Activer les notifications instantanées peut aussi permettre à Google de (partiellement) lire leur contenu, dépendant de l'usage que font les différentes applis du « Google Cloud Messaging ».


=== "F-Droid"

    [F-Droid](https://f-droid.org/fr/) est un magasin d'applications qui héberge exclusivement des [applis libres et open source](https://gofoss.net/fr/foss-apps/). Assurez-vous d'activer le dépôt microG dans F-Droid comme détaillé ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        |1 |Lancez F-Droid. |
        |2 |Allez dans le menu `Paramètres ► Dépôts`. |
        |3 |Activez le dépôt microG. |

        </center>

=== "Applis payantes"


    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        |1 |[Naviguez sur le magasin Google en ligne](https://play.google.com/store/). |
        |2 |Achetez des applications avec un ancien compte Google ou un compte dédié. |
        |3 |Connectez-vous à [Aurora Store](https://f-droid.org/fr/packages/com.aurora.store/) avec ce même compte Google. |
        |4 |Téléchargez les applications achetées. |

        </center>

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions, référez-vous à la [documentation de CalyxOS](https://calyxos.org/install/) ou demandez de l'aide à la [communauté CalyxOS](https://calyxos.org/community/).


<div align="center">
<img src="https://imgs.xkcd.com/comics/xkcd_phone.png" alt="CalyxOS"></img>
</div>

<br>

---
template: main.html
title: So hostet Ihr Eure Cloud-Dienste selbst
description: Das Fediverse. Alternative Cloud-Anbieter. Selbst hosten. Ubuntu-Server. LAMP-Stack. Server-Härtung. SSH. Firewall. NTP. Antivirus. Let's Encrypt. OpenVPN. Pi-Hole.
---

# Tretet dem Fediverse bei. Wechselt Cloud-Anbieter. Hostet Eure Cloud-Dienste selbst.

<div align="center">
<img src="../../assets/img/server.png" alt="Befreit Eure Cloud" width="400px"></img>
</div>

Herzlichen Glückwunsch, wenn Ihr es bis hierher geschafft habt! Vielleicht habt Ihr bereits [Browser](https://gofoss.net/de/firefox/) und [E-Mail-Anbieter](https://gofoss.net/de/encrypted-emails/) gewechselt; Eure [Daten gesichert](https://gofoss.net/de/backups/) und  [verschlüsselt](https://gofoss.net/de/encrypted-files/); [Linux](https://gofoss.net/de/ubuntu/), [CalyxOS](https://gofoss.net/de/calyxos/) oder [LineageOS](https://gofoss.net/de/lineageos/) installiert; und [FOSS-Apps](https://gofoss.net/de/foss-apps/) bevorzugt. Das sind wichtige Schritte in Richtung Datenschutz!

Doch das ist noch nicht das Ende der Fahnenstange. Eure Daten werden weiterhin zweckentfremdet solange Ihr Cloud-Dienste von Google, Apple, Facebook, Twitter, Amazon oder Microsoft nutzt. Um die volle Kontrolle über Eure Daten wiederzuerlangen, steht Ihr letztlich vor drei Optionen:

* [Tretet dem Fediverse bei](https://gofoss.net/de/fediverse/)
* [Wählt datenschutzfreundliche Cloud-Anbieter](https://gofoss.net/de/cloud-providers/)
* [Hostet Eure Cloud-Dienste selbst](https://gofoss.net/de/ubuntu-server/)

<br>

<center> <img src="../../assets/img/separator_mastodon.svg" alt="Fediverse" width="150px"></img> </center>

**Option 1** - Tretet dem Fediverse bei: In diesem Kapitel tauchen wir ins Fediverse ein. Es handelt sich um eine frei, quelloffene und dezentrale Alternative zu kommerziellen sozialen Medien. Tretet einer der verschiedenen Gemeinschaften bei, um Euch mit Menschen zu vernetzen, zu kommunizieren, zu diskutieren oder Bilder, Videos und Musik zu teilen.

<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Cloud-Anbieter" width="150px"></img> </center>

**Option 2** - Wählt datenschutzfreundliche Cloud-Anbieter: In diesem Kapitel empfehlen wir einige datenschutzfreundliche Cloud-Dienste, um Dateien zu synchronisieren und zu teilen, gemeinsam an Dokumenten zu arbeiten, Termine zu planen, Online-Gespräche zu organisieren, Videos zu übertragen und so weiter. Egal, ob Ihr diesen Empfehlungen oder Euren eigenen Vorlieben folgt: Ihr solltet sicherstellen, dass Eure Cloud-Anbieter strengen Datenschutzrichtlinien folgen und auf freie und quelloffene Software-Lösungen setzen.

<br>

<center> <img src="../../assets/img/separator_duckdns.svg" alt="Selbst hosten" width="150px"></img> </center>

**Option 3** - Hostet Eure Cloud-Dienste selbst: In diesen Kapiteln erklären wir, wie Ihr Euren eigenen Server zu Hause einrichten und ihn *vernünftig* vor [unbefugtem Zugriff](https://gofoss.net/de/server-hardening-basics/) oder [schädlichen Angriffen](https://gofoss.net/de/server-hardening-advanced/) schützen könnt. Außerdem erklären wir, wie Ihr Eure [eigenen Cloud-Dienste hosten](https://gofoss.net/de/secure-domain/) könnt, z. B. [Dateisynchronisation](https://gofoss.net/de/cloud-storage/), [Fotogalerien](https://gofoss.net/de/photo-gallery/), [Kontakt-, Kalender- und Aufgabenverwaltung](https://gofoss.net/de/contacts-calendars-tasks/) oder [Medien-Übertragung](https://gofoss.net/de/media-streaming/). Wir werden ebenfalls erklären, wie Ihr Eure [Server-Daten sichern](https://gofoss.net/de/server-backups/) könnt.

<br>

---
template: main.html
title: Comment auto-héberger vos fichiers multimédias
description: Auto-hébergez vos services en nuage. Comment installer Jellyfin ? Qu'est-ce que Jellyfin ? Est-ce que Jellyfin est meilleur que Plex ? Est-ce que Jellyfin est sûr ?
---

# Jellyfin, une solution de streaming média auto-hébergée

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises."

<center>
<img align="center" src="../../assets/img/jellyfin.png" alt="Jellyfin" width="550px"></img>
</center>

<br>

[Jellyfin](https://jellyfin.org/) est une solution de streaming multimédia auto-hébergée pour votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/). Diffusez des films, des émissions de télévision et de la musique sur n'importe quel appareil. Jellyfin est un logiciel libre, rapide et élégant. Il peut prendre en charge plusieurs utilisatrices et utilisateurs, la télévision en direct, la gestion des métadonnées, les plugins, divers clients, etc.


<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Installation Jellyfin" width="150px"></img> </center>

## Installation

Le processus d'installation de Jellyfin est assez simple. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Connectez-vous au serveur, puis exécutez les commandes suivantes pour ajouter le dépôt à la liste des sources apt et installer le logiciel :

    ```bash
    sudo apt install apt-transport-https
    sudo apt-get install software-properties-common
    sudo add-apt-repository universe
    curl -fsSL https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/debian-jellyfin.gpg
    sudo echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/ubuntu $( lsb_release -c -s ) main" | sudo tee /etc/apt/sources.list.d/jellyfin.list
    sudo apt update
    sudo apt install jellyfin
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/65b649cf-9b0b-4448-8823-0162e51fd9f0" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Solution de contournement temporaire pour Ubuntu 22.04"

    Au moment d'écrire ces lignes, Jellyfin ne peut pas être installé à partir des dépôts d'Ubuntu 22.04 en utilisant les instructions décrites ci-dessus. Ceci est principalement dû au fait que Jellyfin 10.7.7 dépend de `libssl1`, qui n'est [plus supporté par Ubuntu 22.04](https://github.com/jellyfin/jellyfin/pull/7648). Installez plutôt [Jellyfin 10.8.0 Beta 3](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.0-beta3) en guise de solution de contournement temporaire.

    Activez les dépôts Universe :

    ```bash
    sudo add-apt-repository universe
    sudo apt update
    ```

    Téléchargez les paquets `.deb` de Jellyfin :

    ```bash
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/10.8.0-beta3/web/jellyfin-web_10.8.0~beta3_all.deb
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/10.8.0-beta3/server/jellyfin-server_10.8.0~beta3_amd64.deb
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/ffmpeg/jellyfin-ffmpeg5_5.0.1-4-jammy_amd64.deb
    ```

    Installez les dépendances requises :

    ```bash
    sudo apt install at libsqlite3-0 libfontconfig1 libfreetype6
    ```

    Installez les paquets `.deb` téléchargés :


    ```bash
    sudo dpkg -i jellyfin-*.deb
    ```

    Corrigez les dépendances manquantes :

    ```bash
    sudo apt -f install
    ```

    Redémarrez le service Jellyfin et assurez-vous que le statut est bien `Active` :

    ```bash
    sudo systemctl restart jellyfin
    sudo systemctl status jellyfin
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Configuration Jellyfin" width="150px"></img> </center>

## Configuration

Après avoir installé Jellyfin avec succès, nous allons configurer un certain nombre de paramètres tels que le démarrage automatique, les comptes utilisateurs, les préférences linguistiques, etc. Des instructions plus détaillées sont disponibles ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Démarrage automatique

    Assurez-vous que Jellyfin se lance automatiquement à chaque démarrage du serveur :

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable jellyfin
    ```

    Redémarrez le serveur :

    ```bash
    sudo reboot
    ```

    Assurez-vous que Jellyfin est démarré et opérationnel (le statut devrait être "Active") :

    ```bash
    sudo systemctl status jellyfin
    ```

    ### Configuration initiale

    Nous allons accéder à l'interface web de Jellyfin pour effectuer la configuration initiale. Pour cela, nous devons ouvrir temporairement le port `8096` sur le pare-feu :

    ```bash
    sudo ufw allow 8096/tcp
    ```

    Allez maintenant sur `http://192.168.1.100:8096` (à ajuster en conséquence) et suivez l'assistant de configuration initiale :

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Langue d'affichage préférée |Sélectionnez votre langue préférée, par exemple le français. |
    | Nom d'utilisateur : |Saisissez un nom d'utilisateur pour l'administrateur Jellyfin. Pour ce tutoriel, nous appellerons cet administrateur `jellyfinadmin`. Bien sûr, vous pouvez choisir n'importe quel nom, assurez-vous simplement d'ajuster les commandes en conséquence.  |
    | Mot de passe |Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour l'administrateur Jellyfin. |
    | Configurer vos médiathèques |Vous pouvez ajouter de la musique, des films ou des émissions de télé à Jellyfin maintenant, ou à tout moment ultérieur à partir du tableau de bord. Les médias doivent être stockés sur : le stockage du serveur, un lecteur externe connecté au serveur ou un périphérique de stockage réseau directement monté sur le système d'exploitation. |
    | Langue de métadonnées préférée |Sélectionnez votre langue préférée, par exemple le français. |
    | Configurer l'accès à distance |Ne pas autoriser l'accès à distance. |

    </center>

    Une fois la configuration initiale terminée, fermez le port `8096` :

    ```
    sudo ufw delete allow 8096/tcp
    ```


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/006c7186-00ea-4c0e-b804-6a7b8629f3e0" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Interface web de Jellyfin" width="150px"></img> </center>

## Interface web

Nous allons configurer un hôte virtuel Apache comme proxy inverse pour accéder à l'interface web de Jellyfin. Continuez votre lecture ci-dessous pour plus de détails.

??? tip "Montrez-moi le guide étape par étape"

    Créez un fichier de configuration Apache :

    ```bash
    sudo vi /etc/apache2/sites-available/mymedia.gofoss.duckdns.org.conf
    ```

    Ajoutez le contenu suivant et assurez-vous de régler les paramètres selon votre propre configuration, comme les noms de domaines (`mymedia.gofoss.duckdns.org`), chemins vers les clés SSL, adresses IP et ainsi de suite :

    ```bash
    <VirtualHost *:80>

      ServerName mymedia.gofoss.duckdns.org
      ServerAlias www.mymedia.gofoss.duckdns.org
      Redirect permanent / https://mymedia.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

      ServerName  mymedia.gofoss.duckdns.org
      ServerAlias www.mymedia.gofoss.duckdns.org
      ServerSignature Off

      SecRuleEngine Off
      SSLEngine On
      SSLCertificateFile  /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
      SSLCertificateKeyFile /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
      DocumentRoot /var/www

      <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
      </Location>

      ProxyPreserveHost On
      ProxyPass "/socket" "ws://127.0.0.1:8096/socket"
      ProxyPassReverse "/socket" "ws://127.0.0.1:8096/socket"
      ProxyPass "/" "http://127.0.0.1:8096/"
      ProxyPassReverse "/" "http://127.0.0.1:8096/"

      ErrorLog ${APACHE_LOG_DIR}/mymedia.gofoss.duckdns.org-error.log
      CustomLog ${APACHE_LOG_DIR}/mymedia.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Une fois que le contenu est ajouté, sauvez et quittez le fichier (`:wq!`).

    Notez comment nous avons activé le chiffrement SSL pour Jellyfin avec l'instruction `SSLEngine On`, et utilisé le certificat SSL `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` ainsi que la clé privée SSL `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, que nous avions créés précédemment.

    Notez également comment nous avons désactivé ModSecurity dans le fichier de configuration d'Apache avec l'instruction `SecRuleEngine Off`, car Jellyfin et ModSecurity ne font pas bon ménage.

    Maintenant, activez les hôtes virtuels d'Apache et redémarrez Apache :

    ```bash
    sudo a2ensite mymedia.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configurez Pi-Hole pour résoudre l'adresse locale de Jellyfin. Naviguez vers `https://mypihole.gofoss.duckdns.org` et connectez-vous à l'interface web de Pi-Hole (ajustez en conséquence). Naviguez vers l'entrée `Local DNS Record` et ajoutez la combinaison domaine/IP suivante (ajustez en conséquence) :

    ```bash
    DOMAIN:      mymedia.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Naviguez jusqu'à [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) et connectez-vous comme `jellyfinadmin` (à ajuster en conséquence) ou avec tout autre compte Jellyfin valide.


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/252127f0-8931-4a5e-a987-7f6502e55322" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Clients Jellyfin" width="150px"></img> </center>

## Clients

Jellyfin prend en charge différents clients pour les ordinateurs, les télés ou les appareils mobiles. Choisissez votre client préféré et suivez les instructions d'installation fournies sur [le site de Jellyfin](https://jellyfin.org/clients/).


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ajouter des utilisateurs à Jellyfin" width="150px"></img> </center>

## Ajouter des utilisatrices et utilisateurs

Jellyfin différencie [deux types d'utilisateurs](https://jellyfin.org/docs/general/server/users/index.html):

* **Les administrateurs** ont un accès complet à Jellyfin. Ils peuvent ajouter, modifier et supprimer des médias ainsi que des utilisatrices et utilisateurs. De plus, les administrateurs peuvent maintenir et mettre à jour Jellyfin. Dans notre exemple, l'administrateur `jellyfinadmin` a été créé pendant le processus d'installation. D'autres administrateurs peuvent être ajoutés.

* **Les Utilisateurs** n'ont qu'un accès limité à Jellyfin. Les fonctionnalités sont configurables individuellement pour chaque utilisatrice et chaque utilisateur.


??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Connexion | Naviguez sur [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) et connectez-vous en tant qu'administrateur, par exemple `jellyfinadmin` (à ajuster en conséquence). |
    | Paramètres | Naviguez vers `Menu ‣ Tableau de bord ‣ Utilisateurs ‣ Ajouter des utilisateurs`. |
    | Ajouter des utilisateurs | Saisissez un nom d'utilisateur, ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Cliquez ensuite sur `Sauvegarder`. Définissez également les médiathèques auxquelles cette utilisatrice ou cet utilisateur peut accéder. |
    | Définir les droits des utilisateurs | Facultatif : définissez d'autres paramètres, tels que :<br>• Les droits d'administrateur <br>• l'accès au « Live TV » <br>• Le transcodage <br>• La restriction du débit internet <br>• L'autorisation de supprimer des fichiers multimédias <br>• La télécommande <br>• Le verrouillage de compte <br>• L'accès aux médiathèques et aux appareils <br>• Le contrôle parental <br>• Le code NIP |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ba6374ef-d524-43e7-97eb-d1bc81d6f6cb" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Administrateurs, utilisateurs et utilisatrices ont besoin d'un accès VPN"

    Tout.e.s les utilisateurs et utilisatrices doivent être connecté.e.s via [VPN](https://gofoss.net/fr/secure-domain/) pour accéder à Jellyfin.

<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Sous-titres Jellyfin" width="150px"></img> </center>

## Sous-titres

Jellyfin prend en charge les [sous-titres intégrés et externes](https://jellyfin.org/docs/general/server/media/external-files.html). L'extension « Open Subtitle » permet à Jellyfin de télécharger automatiquement les sous-titres. Vous trouverez ci-dessous des instructions détaillées.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Connexion | Naviguez sur [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) et connectez-vous en tant qu'administrateur, par exemple `jellyfinadmin` (à ajuster en conséquence). |
    | Installation de l'extension | Naviguez vers `Menu ‣ Tableau de bord ‣ Extensions ‣ Catalogue ‣ Open Subtitles` et cliquez sur `Installer`. |
    | Redémarrage | Allez dans `Menu ‣ Tableau de bord` et cliquez sur `Redémarrer`. |
    | Configuration de l'extension | L'extension « Open Subtitle » ne nécessite pas de compte pour télécharger les sous-titres. Facultatif: vous pouvez créer un compte sur [https://www.opensubtitles.org](https://www.opensubtitles.org/fr/search/subs). Ensuite, naviguez vers `Menu ‣ Tableau de bord ‣ Extensions ‣ Open Subtitles ‣ Paramètres` et saisissez vos identifiants « Open Subtitle ». |
    | Paramètres | Naviguez vers `Menu ‣ Médiathèques ‣ Films ou séries télé ‣ Gérer la médiathèque` et définissez les paramètres dans la section `Téléchargements de sous-titres` :<br><br>• Langues de téléchargement <br>• Téléchargement de sous-titres <br>• Filtrez les sous-titres en fonction du nom du fichier et de l'audio <br>• Emplacement pour stocker les sous-titres |
    | Téléchargement des sous-titres | Jellyfin programme automatiquement une tâche pour télécharger régulièrement les sous-titres manquants. Pour forcer le téléchargement, naviguez dans `Menu ‣ Tableau de bord ‣ Tâches programmées` et cliquez sur `Télécharger les sous-titres manquants`. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (3min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/3d82e2cb-9bd7-41b4-866b-a1db979b584a" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Mise à jour Jellyfin" width="150px"></img> </center>

## Mise à jour

Les mises à jour de Jellyfin sont automatiquement gérées par le gestionnaire de paquets du serveur Ubuntu. Pour effectuer une mise à niveau manuelle, suivez les instructions ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Connectez-vous au serveur et exécutez la commande suivante :

    ```bash
    sudo apt update && sudo apt upgrade
    ```

    Ensuite, redémarrez Jellyfin et vérifiez que tout fonctionne (l'état doit être "Active") :

    ```bash
    sudo systemctl restart jellyfin
    sudo systemctl status jellyfin
    ```

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Jellyfin" width="150px"></img> </center>

## Assistance

Pour davantage de précisions, consultez la [documentation de Jellyfin](https://jellyfin.org/docs/) ou demandez de l'aide à la [communauté Jellyfin](https://jellyfin.org/docs/general/getting-help.html).


<br>
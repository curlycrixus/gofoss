---
template: main.html
title: Radicale | Calendar Management & Contact Management
description: How to sync calendars? How to sync contacts? Self-host a CardDAV account & CalDAV account with contact management software & task management software
---

# Radicale – Self-hosted contact, task & calendar management

!!! level "Last updated: May 2022. For advanced users. Solid tech skills required."

<div align="center">
<img src="../../assets/img/radicale.png" alt="What is Carddav" width="550px"></img>
</div>

<br>

Wondering how to sync contacts, calendars and tasks without relying on Google, Apple or Microsoft? [Radicale](https://radicale.org/) is a self-hosted calendar, task and contact management software for your [home server](https://gofoss.net/ubuntu-server/). It doesn't boast a wide array of features, but excels at what it does. Radicale is free, open source and supports various clients.

<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Sync contacts Android" width="150px"></img> </center>

## How to install Radicale

Follow the instructions below to resolve all dependencies and install Radicale on your server.

??? tip "Show me the step-by-step guide"

    ### Prerequisite

    Radicale requires Python to run. Log into the server and install the following packages:

    ```bash
    sudo apt install python3 python3-pip
    ```

    ### Installation

    Install Radicale:

    ```bash
    sudo -H python3 -m pip install --upgrade radicale
    ```

    Next, create a system user which will run Radicale. For the purpose of this tutorial, we'll call this system user `radicaleadmin`. Of course, you can choose any name, just make sure to adjust the commands accordingly:


    ```bash
    sudo useradd --system --home-dir / --shell /sbin/nologin radicaleadmin
    ```

    No need for a database, just create a folder where Radicale's data is stored in plain files:

    ```bash
    sudo mkdir -p /var/lib/radicale/collections
    ```

??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/102824be-fd90-4384-ae76-e973fdaa77c5" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="What is CardDAV account" width="150px"></img> </center>

## Configuration

After successfully installing Radicale, we're going to configure a number of settings such as multi-user support, permissions, auto start and so on. More details below.

??? tip "Show me the step-by-step guide"

    ### Users

    Radicale supports multiple users. Each one can have its own set of calendars, contacts and task lists. Access can be secured by user credentials, which are stored in an [encrypted file](https://httpd.apache.org/docs/current/programs/htpasswd.html) on the server.

    Install the following packages to enable encryption:

    ```bash
    sudo -H python3 -m pip install --upgrade passlib bcrypt
    ```

    Next, create a directory to store user credentials:

    ```bash
    sudo mkdir /var/www/radicale
    ```

    Finally, create a first user account with the command below, using the `-c` flag. Replace `$USER` with the actual user name. When prompted, enter a [strong, unique password](https://gofoss.net/passwords/):

    ```bash
    sudo htpasswd -B -c /var/www/radicale/users $USER
    ```

    As an illustration, let's create three users — Georg, Lenina and Tom:

    ```bash
    sudo htpasswd -B -c /var/www/radicale/users Georg
    sudo htpasswd -B /var/www/radicale/users Lenina
    sudo htpasswd -B /var/www/radicale/users Tom
    ```

    Note how we used the `-c` flag to create the first user Georg, and then removed the flag to create additional users Lenina and Tom.

    Finally, verify that all user credentials have been correctly added:

    ```bash
    sudo cat /var/www/radicale/users
    ```

    The terminal should prompt something similar to:

    ```bash
    Georg:$2y$05$Zsb8MYlKjRr2InVi/IQNtustqfdnHWN14uG9jNNhx1g rF7a/8Cg6
    Lenina:$2y$05$Hh2Y8nrikm 9f1Pz8LrOFe K5NR HlL0Qz3Yvv gupSBgIr4lT0bi
    Tom:$2y$05$1BnHnXCqNHXZtWMiFhnxBOpYM9xeVnZXm70SoTclEfsrI JPSvDVy
    ```


    ### Settings

    Create a directory containing all settings:

    ```bash
    sudo mkdir /etc/radicale
    ```

    Open the configuration:

    ```bash
    sudo vi /etc/radicale/config
    ```

    Add or modify the following lines:

    ```bash
    [server]
    hosts               = 0.0.0.0:5232, [::]:5232
    max_connections     = 20
    max_content_length  = 10000000
    timeout             = 10

    [auth]
    type		        = http_x_remote_user
    htpasswd_filename   = /var/www/radicale/users
    htpasswd_encryption = bcrypt
    delay               = 5

    [storage]
    filesystem_folder   = /var/lib/radicale/collections
    ```

    Some additional information about those settings:

    <center>

    | Setting | Description |
    | ------ | ------ |
    | `hosts` | Allow remote access to calendars and contacts. |
    | `max_connections` | Limit the number of parallel connections to 20. |
    | `max_content_length` | Limit the file size to 1 MB. Important for large contact photos. |
    | `timeout` | Terminate connections after 10 seconds. |
    | `type` | Enable authentication with the reverse proxy. Radicale uses the user name provided in the X-Remote-User HTTP header. |
    | `htpasswd_filename` | Provide the location where usernames and passwords are stored. In this setup, that's `/var/www/radicale/users`. |
    | `htpasswd_encryption` | Provide the method used to encrypt usernames and passwords. In this setup, it's `bcrypt`. |
    | `delay` | Set the delay after a failed login attempt to 5 seconds, to avert brute force attacks. |
    | `filesystem_folder` | Provide the location where Radicale stores data. In this setup, that's `/var/lib/radicale/collections`. |

    </center>


    ### Permissions

    We'll limit access to the user `radicaleadmin` for the directories `/var/lib/radicale` (contains the data), `/var/www/radicale` (contains encrypted user credentials) and `/etc/radicale` (contains settings):

    ```bash
    sudo chown -R radicaleadmin:radicaleadmin /var/lib/radicale
    sudo chmod -R 700 /var/lib/radicale

    sudo chown -R radicaleadmin:radicaleadmin /var/www/radicale
    sudo chmod -R 755 /var/www/radicale

    sudo chown -R radicaleadmin:radicaleadmin /etc/radicale
    sudo chmod -R 700 /etc/radicale
    ```


    ### Auto start

    Make sure Radicale automatically starts at every system boot. Create a configuration file:

    ```bash
    sudo vi /etc/systemd/system/radicale.service
    ```

    Add the following content (adjust accordingly):

    ```bash
    [Unit]
    Description=A simple CalDAV (calendar) and CardDAV (contact) server
    After=network.target
    Requires=network.target

    [Service]
    ExecStart=/usr/bin/env python3 -m radicale
    Restart=on-failure
    User=radicaleadmin
    Group=radicaleadmin
    PrivateTmp=true
    ProtectSystem=strict
    ProtectHome=true
    PrivateDevices=true
    ProtectKernelTunables=true
    ProtectKernelModules=true
    ProtectControlGroups=true
    NoNewPrivileges=true
    ReadWritePaths=/var/lib/radicale/collections

    [Install]
    WantedBy=multi-user.target
    ```

    Let's make sure Radicale automatically starts every time the server boots:

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable radicale
    ```

    Reboot the server:

    ```bash
    sudo reboot
    ```

    Make sure Radicale is up and running (the status should be"Active"):

    ```bash
    sudo systemctl status radicale
    ```

??? tip "Show me the 2-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/401e2339-44c8-4bea-835e-c7e4b52ce866" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Contact management system" width="150px"></img> </center>

## Web interface

We are going to set up an Apache Virtual Host as a Reverse Proxy to access Radicale's web interface. Read on below for more details on how to set this up.

??? tip "Show me the step-by-step guide"

    Create an Apache configuration file:

    ```bash
    sudo vi /etc/apache2/sites-available/myradicale.gofoss.duckdns.org.conf
    ```

    Add the following content and make sure to adjust the settings to your own setup, such as domain names (`myradicale.gofoss.duckdns.org`), path to SSL keys, IP addresses and so on:

    ```bash
    <VirtualHost *:80>

    ServerName              myradicale.gofoss.duckdns.org
    ServerAlias             www.myradicale.gofoss.duckdns.org
    Redirect permanent /    https://myradicale.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

    ServerName              myradicale.gofoss.duckdns.org
    ServerAlias             www.myradicale.gofoss.duckdns.org
    ServerSignature         Off

    SecRuleEngine           Off
    SSLEngine               On
    SSLProxyEngine          On
    SSLProxyCheckPeerCN     Off
    SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
    SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem

    <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
    </Location>

    RewriteEngine On
    RewriteRule ^/radicale$ /radicale/ [R,L]

    <Location "/radicale/">
        AuthType      Basic
        AuthName      "Radicale - Password Required"
        AuthUserFile  "/var/www/radicale/users"
        Require       valid-user

        ProxyPass        http://localhost:5232/ retry=0
        ProxyPassReverse http://localhost:5232/
        RequestHeader    set X-Script-Name /radicale/
    RequestHeader    set X-Remote-User expr=%{REMOTE_USER}
    </Location>

    ErrorLog ${APACHE_LOG_DIR}/myradicale.gofoss.duckdns.org-error.log
    CustomLog ${APACHE_LOG_DIR}/myradicale.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Once the content is added, save and close the file (`:wq!`).

    Note how we enable SSL encryption for Radicale with the instruction `SSLEngine On`, and use the SSL certicate `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` as well as the private SSL key `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, which were created earlier on.

    Also note how we disabled ModSecurity in the Apache configuration file with the instruction `SecRuleEngine Off`, as Radicale and ModSecurity don't play well together.

    Next, enable the Apache Virtual Host and reload Apache:

    ```bash
    sudo a2ensite myradicale.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configure Pi-Hole to resolve Radicale's local address. Browse to `https://mypihole.gofoss.duckdns.org` and log into Pi-Hole's web interface (adjust accordingly). Navigate to the menu entry `Local DNS Records` and add the following domain/IP combination (adjust accordingly):

    ```bash
    DOMAIN:      myradicale.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Browse to [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) and log in with valid credentials. Then create calendars, address books and task lists as required.


??? tip "Show me the 1-minute summary video"

    In this video, we set up the web interface, create an address book and calendar for Georg, and a task list for Lenina.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/11026915-b199-44f1-abd9-916167fc805b" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Thunderbird CardDAV" width="150px"></img> </center>

## Clients

On Android, [DAVx⁵](https://www.davx5.com/) natively integrates with your device's calendar, contact and task apps to keep them synchronised with Radicale. On Windows, macOS or Linux (Ubuntu), Radicale works well with Thunderbird's desktop clients. More detailed instructions below.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        Open F-Droid and install [DAVx⁵](https://f-droid.org/en/packages/at.bitfire.davdroid/). Alternatively, you can find the app on [Aurora Store](https://auroraoss.com/) or [Google's Play Store](https://play.google.com/store/apps/details?id=at.bitfire.davdroid&hl=en_US&gl=US).

        <center>

        | Settings | Description |
        | ------ | ------ |
        | Launch DAVx⁵ |Open the DAVx⁵ app and disable battery optimisation when prompted. |
        | Create new account | Tap on the `+` sign to create a new account. Select `Login with URL and user name`. |
        | Base URL | Provide the address of the Radicale server. In our example, that's [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/), adjust accordingly. |
        | User & Password | Provide valid user credentials. In our example, valid users are Georg, Lenina or Tom. Adjust accordingly. |
        | Contact group method | On the next screen, select `Groups are separate VCards`. |
        | Create account | Click on `Create account`. |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        For detailed instructions on how to install Thunderbird, refer to the [previous chapter on email clients](https://gofoss.net/encrypted-emails/). Once installed, open Thunderbird, navigate to `Tools ‣ Add-ons` and install the [Cardbook](https://addons.thunderbird.net/en-US/thunderbird/addon/cardbook/) extension. Finally, restart Thunderbird.


<div style="margin-top:-20px">
</div>

??? warning "Clients need a VPN access"

    Clients must be connected to the server via [VPN](https://gofoss.net/secure-domain/) to access Radicale.



<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="What is a CardDAV account" width="150px"></img> </center>

## Import contacts

Your existing contacts are likely scattered all over the place. Follow below instructions to import them to Radicale.

??? tip "Show me a step-by-step guide"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Step 1 | Head over to devices or cloud services where your contacts are stored (Google, Microsoft, Apple, and so on). Export them in the vCard format (`.vcf`).|
    | Step 2 | [Backup up the `.vcf` files](https://gofoss.net/backups/). |
    | Step 3 | Transfer the various `.vcf` files to your Android phone. |
    | Step 4 | Open the stock `Contacts` app on your phone. If you use another contact app, instructions might slightly change. |
    | Step 5 | Go to `Settings ‣ Import`. |
    | Step 6 | Select `Import contacts from .vcf file`. |
    | Step 7 | Browse to the `.vcf` files location and select one. |
    | Step 8 | Select `Save imported contacts to DAVx⁵` and choose the appropriate Radicale address book. |
    | Step 9 | Repeat until all contacts have been imported. |

    </center>


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Android sync contacts" width="150px"></img> </center>

## Sync contacts

When synchronising contacts, all changes made on your local Android, Windows, macOS or Linux (Ubuntu) device will be mirrored to the Radicale CardDav server and other connected devices, and the other way round. More detailed instructions below.

=== "Android"

    ??? tip "Contacts sync for Android"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Step 1 | On your Android phone, open DAVx⁵'s main screen and select an account. |
        | Step 2 | Navigate to the `CardDAV` tab and select Radicale's address books to be synced with the phone. |
        | Step 3 | Click on the synchronisation button. |
        | Step 4 | Make sure everything worked. Open your phone's contact management app and click on `Settings ‣ Accounts`. Radicale's address books should show up here. |

        </center>

=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Show me a step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Step 1 | Log into [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (adjust accordingly). |
        | Step 2 | Navigate to the address book you want to synchronise and copy it's URL link. |
        | Step 3 | Open Thunderbird and navigate to `Cardbook ‣ Address book ‣ New Address Book`. |
        | Step 4 | Follow the setup wizard: <br>• `Location`: `Remote` <br>• `Type`: `CardDAV`<br>• `URL`:`Paste the URL link to the address book, as copied previously`<br>• `Username & Password`: `Provide valid Radicale user credentials`<br>• `Properties`: `Provide the address book's name, color, offline availability, etc.`|
        | Step 5 | Make sure everything worked. Radicale's address book should show up in the `CardBook` tab.|

        </center>


<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Android sync calendars" width="150px"></img> </center>

## Import calendars

Your existing calendars are likely scattered all over the place. Follow below instructions to import them to Radicale.

??? tip "Show me a step-by-step guide"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Step 1 | Head over to devices or cloud services where your calendars are stored (Google, Microsoft, Apple, and so on). Export them in the iCalendar format (`.ics`).|
    | Step 2 | [Backup up the `.ics` files](https://gofoss.net/backups/). |
    | Step 3 | Transfer the various `.ics` files to your Android phone. |
    | Step 4 | On your phone, open F-Droid and install the [Calendar Import-Export app](https://f-droid.org/en/packages/org.sufficientlysecure.ical/). Alternatively, install the calendar management software from [Google's Play Store](https://play.google.com/store/apps/details?id=org.sufficientlysecure.ical) |
    | Step 5 | Open the Calendar Import-Export app and click on `Import files`. |
    | Step 6 | Browse to the `.ics` files location and select one. |
    | Step 7 | Import to DAVx⁵ and choose the appropriate Radicale calendar. |
    | Step 8 | Repeat until all calendars have been imported. |

    </center>



<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Sync calendars Android" width="150px"></img> </center>

## How to sync calendars

When synchronising calendars, all changes made on your local Android, Windows, macOS or Linux (Ubuntu) device will be mirrored to the opensource calendar server and other connected devices, and the other way round. More detailed instructions below.

=== "Android"

    ??? tip "How to sync calendars on Android"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Step 1 | On your Android phone, open DAVx⁵'s main screen and select an account. |
        | Step 2 | Navigate to the `CalDAV` tab and select Radicale's calendars to be synced with the phone. |
        | Step 3 | Click on the `Sync calendar` button. |
        | Step 4 |Install and open the [Simple Calendar app](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/) on your phone. If you use another calendar app or CalDAV synchroniser, the following instructions might slightly change.  |
        | Step 5 | Click on `Settings` and make sure the option `CalDAV sync` is enabled.|
        | Step 6 | Click on `Manage synced calendars`. Radicale's calendars should show up here.|
        | Step 7 | Select Radicale's calendars and click on `OK`.|

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Calendar management tools for Windows, macOS & Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Step 1 | Log into [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (adjust accordingly). |
        | Step 2 | Navigate to the calendar you want to synchronise and copy it's URL link. |
        | Step 3 | Open Thunderbird and navigate to `File ‣ New ‣ Calendar`. |
        | Step 4 | Follow the setup wizard: <br>• `Create a new calendar`: `On the Network` <br>• `Format`: `CalDAV` (do *not* select `iCalendar (ICS)`!)<br>• `Username`: `Provide a valid Radicale username`<br>• `Location`:`Paste the URL link to the calendar, as copied previously`<br>• `Offline support`: `Check the box`<br>• `Customisation`: `Provide the calendar's name & color, enable reminders, associate an email address to manage invitations, etc.`|
        | Step 5 | Make sure everything worked. Radicale's calendar should show up in the `Calendar` tab. |

        </center>



<br>

<center> <img src="../../assets/img/separator_tasks.svg" alt="Best task management software" width="150px"></img> </center>

## Task management software

When synchronising task lists, all changes made on your local Android, Windows, macOS or Linux (Ubuntu) device will be mirrored to the Radicale server and other connected devices, and the other way round. More detailed instructions below.

=== "Android"

    ??? tip "Show me a step-by-step guide for Android"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Step 1 | On your Android phone, open DAVx⁵'s main screen and select an account. |
        | Step 2 | Navigate to the `CalDAV` tab and select Radicale's task lists to be synced with the phone. |
        | Step 3 | Click on the synchronisation button. |
        | Step 4 |Install and open the [Tasks.org app](https://f-droid.org/en/packages/org.tasks/) on your phone. If you use another tasks app, the following instructions might slightly change. |
        | Step 5 | Click on `Settings` and select `Displayed Lists`. Radicale's task lists should show up here.|

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Show me a step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Step 1 | Log into [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (adjust accordingly). |
        | Step 2 | Navigate to the task list you want to synchronise and copy it's URL link. |
        | Step 3 | Open Thunderbird and navigate to `File ‣ New ‣ Calendar`. |
        | Step 4 | Follow the setup wizard: <br>• `Create a new calendar`: `On the Network` <br>• `Format`: `CalDAV` (do *not* select `iCalendar (ICS)`!)<br>• `Username`: `Provide a valid Radicale username`<br>• `Location`:`Paste the URL link to the calendar, as copied previously`<br>• `Offline support`: `Check the box`<br>• `Customisation`: `Provide the task list's name & color, enable reminders. No need to add an email address.`|
        | Step 5 | Make sure everything worked. Radicale's task list should show up in the `Calendar` tab. |

        </center>



<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Best task management app" width="150px"></img> </center>

## Backup

[Regularly perform backups](https://gofoss.net/backups/) to avoid any data loss! While Radicale is great, it's not totally immune to errors. Contacts, calendars and tasks remain synchronised as long as your devices stay connected with the server over VPN. But sometimes synchronisation errors can occur, for example when changes aren't properly saved or devices get disconnected. Users might also inadvertently delete information, and once this is synchronised across the server and all devices the data is gone forever.

??? tip "Show me a step-by-step guide"

    To perform manual backups, log into [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (adjust accordingly) and click on the URL links of address books, calendars and tasks to download your data.

    You can also [perform automatic backups](https://gofoss.net/server-backups/) of the directories `/var/lib/radicale`, `/var/www/radicale` and `/etc/radicale`.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="CardDAV password" width="150px"></img> </center>

## Support

For further reference, consult [Radicale's](https://radicale.org/), [DAVx⁵'s](https://www.davx5.com/manual/) or [Thunderbird's](https://support.mozilla.org/en-US/products/thunderbird) documentation.


<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/time_management.png" width="550px" alt="What is CalDAV"></img>
</div>

<br>
---
template: main.html
title: Comment installer LineageOS pour microG
description: Dégooglisez votre téléphone. Qu'est-ce que LineageOS ? Est-ce que LineageOS est légal ? Qu'est-ce que microG ? LineageOS vs CalyxOS ? LineageOS vs GrapheneOS ?
---

# LineageOS pour microG, un OS mobile alternatif et populaire

!!! level "Dernière mise à jour: mars 2022. Destiné aux utilisateurs expérimentés. Certaines compétences techniques sont requises."

<center>
<img align="center" src="../../assets/img/lineageosmicrog.png" alt="LineageOS pour microG" width ="600px"></img>
</center>


Si vous ne possédez pas de téléphone Pixel, [LineageOS pour microG](https://lineage.microg.org) peut être une alternative à [CalyxOS](https://gofoss.net/fr/calyxos/). Ce projet est un dérivé non-officiel de [LineageOS](https://lineageos.org/) et s'appuie sur [microG](https://microg.org/), un clone libre de certaines bibliothèques et applications propriétaires de Google. LineageOS pour microG prend en charge des centaines de modèles de téléphones. Assurez-vous que votre modèle de téléphone *exact* figure dans la [liste des appareils](https://wiki.lineageos.org/devices/).

!!! warning "Quelques mots sur la compatibilité et la sécurité"

    Soyez prudent lors de l'installation de LineageOS pour microG sur votre appareil. Cela invalide la garantie de votre téléphone et peut, en cas d'erreur, même le rendre inutile. Vous êtes seul à assumer les risques de tout dommage ou perte de données qui pourrait survenir.

    Avant de passer à LineageOS pour microG, souvenez-vous que même si quasiment tout marche bien, [quelques applications ne sont pas prises en charge](https://github.com/microg/GmsCore/wiki/Implementation-Status). Ceci inclut quelques applications provenant de Google, comme « Android Wear », « Google Fit », « Google Cast » ou « Android Auto ». Heureusement, des [alternatives libres](https://gofoss.net/fr/foss-apps/) sont disponibles. Enfin, n'oubliez pas qu'utiliser des applications payantes sans le Play Store de Google peut être compliqué.

    L'installation de LineageOS pour microG nécessite le déverrouillage du « bootloader ». Ceci réduit la sécurité de votre téléphone. [Tout attaquant ayant un accès physique pourrait en théorie installer des logiciels sans devoir déchiffrer votre téléphone](https://en.wikipedia.org/wiki/Evil_Maid_attack), comme par exemple un enregistreur de mot de passe. Notez également que LineageOS pour microG a pris du retard sur les mises à jour de sécurité dans le passé. Nous vous recommandons de régulièrement vérifier les mises à jour. Pour certains téléphones, les composants propriétaires tels que le « bootloader » ou le « firmware » ne sont plus mis à jour. Évaluez ces risques de sécurité potentiels à la lumière de votre [modèle de menace](https://ssd.eff.org/en/module/your-security-plan/).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Sauvegarde LineageOS" width="150px"></img> </center>

## Sauvegarde

Toutes les données seront effacées de votre smartphone pendant l'installation. Ne prenez pas de risques, [sauvegardez votre téléphone](https://gofoss.net/fr/backups/)!


<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Déboguage LineageOS" width="150px"></img> </center>

## Débogage USB

Le débogage USB doit être activé pour permettre à « Android Debug Bridge (ADB) » de connecter votre téléphone à l'ordinateur. Des instructions plus détaillées sont disponibles ci-dessous.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Allez dans `Paramètres ► À propos du téléphone`. |
        | 2 | Appuyez sept fois sur `Numéro de build` pour activer les options développeur. |
        | 3 | Allez dans `Paramètres ► Système ► Options avancées ► Options développeur`. |
        | 4 | Défilez jusqu'en bas, puis cochez la case `Débogage Android` ou `Débogage USB`. |
        | 5 | Connectez votre appareil Android à l'ordinateur Windows. |
        | 6 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

        ### Sur votre ordinateur Windows

        | Étapes | Description |
        | :------: | ------ |
        | 1 | [Téléchargez ADB pour Windows](https://dl.google.com/android/repository/platform-tools-latest-windows.zip/). |
        | 2 | Extrayez le contenu de l'archive `.zip`. |
        | 3 | Ouvrez l'Explorateur Windows et naviguez jusqu'au dossier contenant les fichiers ADB extraits. |
        | 4 | Faites un clic droit dans ce dossier et sélectionnez l'entrée `Ouvrir une fenêtre de commandes ici` ou `Ouvrir la fenêtre PowerShell ici`. |
        | 5 | Exécutez la commande suivante pour lancer le démon ADB :<br> `adb devices` |
        | 6 | Une boîte de dialogue devrait apparaître sur votre téléphone Android, vous demandant d'autoriser le débogage USB. Cochez la case `Toujours autoriser`, et cliquez sur `OK`. |


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Allez dans `Paramètres ► À propos du téléphone`. |
        | 2 | Appuyez sept fois sur `Numéro de build` pour activer les options développeur. |
        | 3 | Allez dans `Paramètres ► Système ► Options avancées ► Options développeur`. |
        | 4 | Défilez jusqu'en bas, puis cochez la case `Débogage Android` ou `Débogage USB`. |
        | 5 | Connectez votre appareil Android à l'appareil macOS. |
        | 6 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

        ### Sur votre appareil macOS

        | Étapes | Description |
        | :------: | ------ |
        | 1 | [Téléchargez ADB pour macOS](https://dl.google.com/android/repository/platform-tools-latest-darwin.zip/). |
        | 2 | Extrayez le contenu de l'archive `.zip`. |
        | 3 | Ouvrez le terminal et naviguez jusqu'au dossier contenant les fichiers ADB extraits. |
        | 4 | Exécutez la commande suivante pour lancer le démon ADB :<br> `adb devices` |
        | 5 | Une boîte de dialogue devrait apparaître sur votre téléphone Android, vous demandant d'autoriser le débogage USB. Cochez la case `Toujours autoriser`, et cliquez sur `OK`. |


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Allez dans `Paramètres ► À propos du téléphone`. |
        | 2 | Appuyez sept fois sur `Numéro de build` pour activer les options développeur. |
        | 3 | Allez dans `Paramètres ► Système ► Options avancées ► Options développeur`. |
        | 4 | Défilez jusqu'en bas, puis cochez la case `Débogage Android` ou `Débogage USB`. |
        | 5 | Connectez votre appareil Android à l'appareil Linux (Ubuntu). |
        | 6 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

        ### Sur votre appareil Linux (Ubuntu)

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. |
        | 2 | Exécutez les commandes suivantes pour mettre à jour vos paquets système :<br> `sudo apt update` <br> `sudo apt upgrade` |
        | 3 | Exécutez les commandes suivantes pour installer ADB Fastboot :<br> `sudo apt install android-tools-adb` <br> `sudo apt install android-tools-fastboot`|
        | 4 | Exécutez les commandes suivantes pour lancer le serveur ADB (s'il n'a pas encore démarré automatiquement) :<br> `sudo adb start-server`|
        | 5 | Exécutez les commandes suivantes pour lancer le démon ADB :<br> `adb devices` |
        | 6 | Une boîte de dialogue devrait apparaître sur votre téléphone Android, vous demandant d'autoriser le débogage USB. Cochez la case `Toujours autoriser`, et cliquez sur `OK`. |


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="LineageOS bootloader" width="150px"></img> </center>

## Déverrouiller le « bootloader »

Le chargeur de démarrage (en anglais, « bootloader ») est le premier élément de code qui s'exécute lors de la mise en marche d'un téléphone. De nombreux fabricants verrouillent ce « bootloader » pour éviter toute modification du téléphone. Afin d'installer LineageOS pour microG, le « bootloader » doit être déverrouillé. *C'est la partie la plus délicate*, car le processus diffère d'un téléphone à l'autre et nécessite un petit travail de recherche :

* Consultez le [wiki de LineageOS](https://wiki.lineageos.org/devices/). Localisez votre modèle de téléphone *exact*, naviguez jusqu'au `guide d'installation` et lisez le chapitre `Unlocking the bootloader`.
* Si vous avez besoin de plus de conseils, demandez de l'aide à la [communauté LineageOS](https://lineageos.org/community/) ou à la [communauté LineageOS Reddit](https://teddit.net/r/LineageOS/).


<br>

<center> <img src="../../assets/img/separator_twrp.svg" alt="LineageOS TWRP" width="150px"></img> </center>

## Adapter le logiciel de récupération

Les téléphones Android sont livrés avec un logiciel de récupération fourni par Google, également appelé *« stock recovery »*. Il est utilisé pour rétablir les paramètres d'usine ou mettre à jour le système d'exploitation. Pour pouvoir installer LineageOS pour microG, ce logiciel doit être remplacé par un logiciel de récupération spécifique, dit *« custom recovery »*.

Le [Team Win Recovery Project](https://twrp.me/) (TWRP) est une *« custom recovery »* populaire. Au-delà de permettre l'installation de LineageOS pour microG, TWRP rend également possible la création de sauvegardes et la modification des paramètres systèmes. Vous trouverez des instructions plus détaillées ci-dessous.


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez votre appareil Android à l'ordinateur Windows. |
        | 2 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

        ### Sur votre ordinateur Windows

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Allez sur le [site de TWRP](https://twrp.me/Devices/), sélectionnez le bon modèle de téléphone et téléchargez la version TWRP appropriée. Dans le cadre de ce tutoriel, supposons que le fichier ait été téléchargé dans le répertoire `/Downloads/twrp-x.x.x.img`. |
        | 2 | Ouvrez l'Explorateur Windows et naviguez jusqu'au répertoire contenant le fichier `.img` téléchargé. |
        | 3 | Faites un clic droit dans ce répertoire et sélectionnez l'entrée `Ouvrir une fenêtre de commandes ici` ou `Ouvrir la fenêtre PowerShell ici`. |
        | 4 | Exécutez la commande suivante pour redémarrer le téléphone en mode *bootloader* :<br> `adb reboot bootloader` <br><br> Vous pouvez également redémarrer le téléphone en mode *bootloader* en l'éteignant, puis en maintenant les boutons `Volume BAS` et `MARCHE` enfoncés. Relâchez-les lorsque le mot `FASTBOOT` s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://wiki.lineageos.org/devices). |
        | 5 | Exécutez la commande suivante pour vous assurer que l'ordinateur puisse détecter votre téléphone :<br> `fastboot devices`|
        | 6 | Le terminal devrait afficher un message du type :<br> `$ 0077fxe89p12 fastboot`|
        | 7 | Exécutez les commandes suivantes pour naviguer jusqu'au répertoire contenant le fichier « custom recovery » (ajustez-le chemin en conséquence) et « flashez » le « custom recovery » sur votre téléphone : <br>`cd /Downloads` <br>`fastboot flash recovery twrp-x.x.x.img`|
        | 8 | Le terminal devrait afficher un message du type :<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 9 | Exécutez la commande suivante pour démarrer directement dans TWRP :<br> `fastboot boot recovery twrp-x.x.x.img`|


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez votre appareil Android à l'appareil macOS. |
        | 2 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

        ### Sur votre appareil macOS

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Allez sur le [site de TWRP](https://twrp.me/Devices/), sélectionnez le bon modèle de téléphone et téléchargez la version TWRP appropriée. Dans le cadre de ce tutoriel, supposons que le fichier ait été téléchargé dans le répertoire `/Downloads/twrp-x.x.x.img`. |
        | 2 | Ouvrez le terminal. |
        | 3 | Exécutez la commande suivante pour redémarrer le téléphone en mode *bootloader* :<br> `adb reboot bootloader` <br><br> Vous pouvez également redémarrer le téléphone en mode *bootloader* en l'éteignant, puis en maintenant les boutons `Volume BAS` et `MARCHE` enfoncés. Relâchez-les lorsque le mot `FASTBOOT` s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://wiki.lineageos.org/devices). |
        | 4 | Exécutez la commande suivante pour vous assurer que l'ordinateur puisse détecter votre téléphone :<br> `sudo fastboot devices`|
        | 5 | Le terminal devrait afficher un message du type :<br> `$ 0077fxe89p12 fastboot`|
        | 6 | Exécutez les commandes suivantes pour naviguer jusqu'au répertoire contenant le fichier « custom recovery » (ajustez-le chemin en conséquence) et « flashez » le « custom recovery » sur votre téléphone : <br>`cd /Downloads` <br>`sudo fastboot flash recovery twrp-x.x.x.img`|
        | 7 | Le terminal devrait afficher un message du type :<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 8 | Exécutez la commande suivante pour démarrer directement dans TWRP :<br> `sudo fastboot boot recovery twrp-x.x.x.img`|


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez votre appareil Android à l'appareil Linux (Ubuntu). |
        | 2 | Changez le mode USB vers `Transfert de fichier (MTP)`. |

        ### Sur votre appareil Linux (Ubuntu)

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Allez sur le [site de TWRP](https://twrp.me/Devices/), sélectionnez le bon modèle de téléphone et téléchargez la version TWRP appropriée. Dans le cadre de ce tutoriel, supposons que le fichier ait été téléchargé dans le répertoire `/home/gofoss/Downloads/twrp-x.x.x.img`. |
        | 2 | Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. |
        | 3 | Exécutez la commande suivante pour redémarrer le téléphone en mode *bootloader* :<br> `adb reboot bootloader` <br><br> Vous pouvez également redémarrer le téléphone en mode *bootloader* en l'éteignant, puis en maintenant les boutons `Volume BAS` et `MARCHE` enfoncés. Relâchez-les lorsque le mot `FASTBOOT` s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://wiki.lineageos.org/devices). |
        | 4 | Exécutez la commande suivante pour vous assurer que l'ordinateur puisse détecter votre téléphone :<br> `sudo fastboot devices`|
        | 5 | Le terminal devrait afficher un message du type :<br> `$ 0077fxe89p12 fastboot`|
        | 6 | Exécutez les commandes suivantes pour naviguer jusqu'au répertoire contenant le fichier « custom recovery » (ajustez-le chemin en conséquence) et « flashez » le « custom recovery » sur votre téléphone : <br>`cd /home/gofoss/Downloads` <br>`sudo fastboot flash recovery twrp-x.x.x.img`|
        | 7 | Le terminal devrait afficher un message du type :<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 8 | Exécutez la commande suivante pour démarrer directement dans TWRP :<br> `sudo fastboot boot recovery twrp-x.x.x.img`|


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Installation LineageOS" width="150px"></img> </center>

## Installation

Une fois que le téléphone a démarré avec succès dans le « custom recovery » (TWRP), LineageOS pour microG peut être installé. Vous trouverez ci-dessous des instructions plus détaillées.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        ### Sur votre ordinateur Windows

        | Étapes | Description |
        | :------: | ------ |
        | 1 | [Identifiez le nom de code associé à votre modèle de téléphone sur le site de LineageOS](https://wiki.lineageos.org/devices/). Par exemple, le nom de code du Fairphone est « FP2 », le Nexus 5X est appelé « bullhead », le Samsung Galaxy S9 est appelé « starlte », etc. |
        | 2 | [Recherchez le même nom de code sur le site de LineageOS pour microG](https://download.lineage.microg.org/). Cliquez sur le dossier correspondant et téléchargez la dernière version de LineageOS pour microG. Pour ce tutoriel, supposons que le fichier ait été téléchargée à l'emplacement `/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 3 | Ouvrez l'Explorateur Windows et naviguez jusqu'au répertoire contenant le fichier `.zip` téléchargé. |
        | 4 | Faites un clic droit dans ce répertoire et sélectionnez l'entrée `Ouvrir une fenêtre de commandes ici` ou `Ouvrir la fenêtre PowerShell ici`. |
        | 5 | Exécutez la commande suivante pour redémarrer le téléphone en mode *recovery* : <br> `adb reboot recovery`<br><br> Vous pouvez également redémarrer le téléphone en mode *recovery* en l'éteignant, puis en maintenant les boutons `Volume HAUT` et `MARCHE` enfoncés. Relâchez-les lorsque le logo s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Le « custom recovery » TWRP devrait maintenant se charger. Faites glisser le curseur pour accéder à l'écran principal. |
        | 2 | Appuyez sur le bouton `Wipe` et ensuite sur le bouton `Format Data`. *Attention !* Cela supprimera le chiffrement et effacera tous les fichiers du stockage interne. [Assurez-vous d'avoir sauvegardé vos données](https://gofoss.net/fr/backups/) ! |
        | 3 | Revenez au menu précédent, appuyez sur le bouton `Wipe` et cette fois-ci sur le bouton `Advanced Wipe`. |
        | 4 | Sélectionnez `Cache`, `System` et `Data` et faites glisser le curseur `Swipe to Wipe` en bas de l'écran. |

        ### Sur votre ordinateur Windows

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Exécutez la commande suivante pour vous assurer que l'ordinateur puisse détecter votre téléphone :<br> `fastboot devices` |
        | 2 | Le terminal devrait afficher un message du type :<br><br>`$ 0077fxe89p12 fastboot`|
        | 3 | Exécutez les commandes suivantes pour naviguer vers le répertoire contenant le fichier LineageOS pour microG téléchargé (ajustez le chemin en conséquence) et copier le fichier `.zip` vers le stockage interne de votre téléphone :<br> `cd /Downloads`<br>`adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Vous pouvez également copier manuellement le fichier `.zip` de votre ordinateur vers votre téléphone, en utilisant le gestionnaire de fichiers. |
        | 4 | Exécutez la commande suivante pour redémarrer le téléphone en mode *recovery* : <br> `adb reboot recovery`<br><br> Vous pouvez également redémarrer le téléphone en mode *recovery* en l'éteignant, puis en maintenant les boutons `Volume HAUT` et `MARCHE` enfoncés. Relâchez-les lorsque le logo s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Naviguez jusqu'à l'écran principal de TWRP et appuyez sur le bouton `Install`. Choisissez le fichier LineageOS `lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 2 | Faites glisser le curseur `Swipe to confirm Flash`, attendez que LineageOS pour microG soit installé et redémarrez le téléphone (cette fois-ci, un redémarrage « normal », pas un redémarrage *bootloader* ou *recovery*). |
        | 3 | Le premier démarrage peut prendre un certain temps. Félicitations, LineageOS pour microG est installé sur votre téléphone ! |


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        ### Sur votre appareil macOS

        | Étapes | Description |
        | :------: | ------ |
        | 1 | [Identifiez le nom de code associé à votre modèle de téléphone sur le site de LineageOS](https://wiki.lineageos.org/devices/). Par exemple, le nom de code du Fairphone est « FP2 », le Nexus 5X est appelé « bullhead », le Samsung Galaxy S9 est appelé « starlte », etc. |
        | 2 | [Recherchez le même nom de code sur le site de LineageOS pour microG](https://download.lineage.microg.org/). Cliquez sur le dossier correspondant et téléchargez la dernière version de LineageOS pour microG. Pour ce tutoriel, supposons que le fichier ait été téléchargée à l'emplacement `/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 3 | Ouvrez le terminal. |
        | 4 | Exécutez la commande suivante pour redémarrer le téléphone en mode *recovery* : <br> `adb reboot recovery`<br><br> Vous pouvez également redémarrer le téléphone en mode *recovery* en l'éteignant, puis en maintenant les boutons `Volume HAUT` et `MARCHE` enfoncés. Relâchez-les lorsque le logo s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Le « custom recovery » TWRP devrait maintenant se charger. Faites glisser le curseur pour accéder à l'écran principal. |
        | 2 | Appuyez sur le bouton `Wipe` et ensuite sur le bouton `Format Data`. *Attention !* Cela supprimera le chiffrement et effacera tous les fichiers du stockage interne. [Assurez-vous d'avoir sauvegardé vos données](https://gofoss.net/fr/backups/) ! |
        | 3 | Revenez au menu précédent, appuyez sur le bouton `Wipe` et cette fois-ci sur le bouton `Advanced Wipe`. |
        | 4 | Sélectionnez `Cache`, `System` et `Data` et faites glisser le curseur `Swipe to Wipe` en bas de l'écran. |

        ### Sur votre appareil macOS

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Exécutez la commande suivante pour vous assurer que l'ordinateur puisse détecter votre téléphone :<br> `sudo fastboot devices` |
        | 2 | Le terminal devrait afficher un message du type :<br><br>`$ 0077fxe89p12 fastboot`|
        | 3 | Exécutez les commandes suivantes pour naviguer vers le répertoire contenant le fichier LineageOS pour microG téléchargé (ajustez le chemin en conséquence) et copier le fichier `.zip` vers le stockage interne de votre téléphone :<br> `cd /Downloads`<br>`sudo adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Vous pouvez également copier manuellement le fichier `.zip` de votre ordinateur vers votre téléphone, en utilisant le gestionnaire de fichiers. |
        | 4 | Exécutez la commande suivante pour redémarrer le téléphone en mode *recovery* : <br> `adb reboot recovery`<br><br> Vous pouvez également redémarrer le téléphone en mode *recovery* en l'éteignant, puis en maintenant les boutons `Volume HAUT` et `MARCHE` enfoncés. Relâchez-les lorsque le logo s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Naviguez jusqu'à l'écran principal de TWRP et appuyez sur le bouton `Install`. Choisissez le fichier LineageOS `lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 2 | Faites glisser le curseur `Swipe to confirm Flash`, attendez que LineageOS pour microG soit installé et redémarrez le téléphone (cette fois-ci, un redémarrage « normal », pas un redémarrage *bootloader* ou *recovery*). |
        | 3 | Le premier démarrage peut prendre un certain temps. Félicitations, LineageOS pour microG est installé sur votre téléphone ! |


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        ### Sur votre appareil Linux (Ubuntu)

        | Étapes | Description |
        | :------: | ------ |
        | 1 | [Identifiez le nom de code associé à votre modèle de téléphone sur le site de LineageOS](https://wiki.lineageos.org/devices/). Par exemple, le nom de code du Fairphone est « FP2 », le Nexus 5X est appelé « bullhead », le Samsung Galaxy S9 est appelé « starlte », etc. |
        | 2 | [Recherchez le même nom de code sur le site de LineageOS pour microG](https://download.lineage.microg.org/). Cliquez sur le dossier correspondant et téléchargez la dernière version de LineageOS pour microG. Pour ce tutoriel, supposons que le fichier ait été téléchargée à l'emplacement `/home/gofoss/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 3 | Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. |
        | 4 | Exécutez la commande suivante pour redémarrer le téléphone en mode *recovery* : <br> `adb reboot recovery`<br><br> Vous pouvez également redémarrer le téléphone en mode *recovery* en l'éteignant, puis en maintenant les boutons `Volume HAUT` et `MARCHE` enfoncés. Relâchez-les lorsque le logo s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Le « custom recovery » TWRP devrait maintenant se charger. Faites glisser le curseur pour accéder à l'écran principal. |
        | 2 | Appuyez sur le bouton `Wipe` et ensuite sur le bouton `Format Data`. *Attention !* Cela supprimera le chiffrement et effacera tous les fichiers du stockage interne. [Assurez-vous d'avoir sauvegardé vos données](https://gofoss.net/fr/backups/) ! |
        | 3 | Revenez au menu précédent, appuyez sur le bouton `Wipe` et cette fois-ci sur le bouton `Advanced Wipe`. |
        | 4 | Sélectionnez `Cache`, `System` et `Data` et faites glisser le curseur `Swipe to Wipe` en bas de l'écran. |

        ### Sur votre appareil Linux (Ubuntu)

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Exécutez la commande suivante pour vous assurer que l'ordinateur puisse détecter votre téléphone :<br> `sudo fastboot devices` |
        | 2 | Le terminal devrait afficher un message du type :<br><br>`$ 0077fxe89p12 fastboot`|
        | 3 | Exécutez les commandes suivantes pour naviguer vers le répertoire contenant le fichier LineageOS pour microG téléchargé (ajustez le chemin en conséquence) et copier le fichier `.zip` vers le stockage interne de votre téléphone :<br> `cd /home/gofoss/Downloads`<br>`sudo adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Vous pouvez également copier manuellement le fichier `.zip` de votre ordinateur vers votre téléphone, en utilisant le gestionnaire de fichiers. |
        | 4 | Exécutez la commande suivante pour redémarrer le téléphone en mode *recovery* : <br> `adb reboot recovery`<br><br> Vous pouvez également redémarrer le téléphone en mode *recovery* en l'éteignant, puis en maintenant les boutons `Volume HAUT` et `MARCHE` enfoncés. Relâchez-les lorsque le logo s'affiche à l'écran. La combinaison des boutons peut varier d'un appareil à l'autre. Plus d'informations [ici](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Sur votre smartphone Android

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Naviguez jusqu'à l'écran principal de TWRP et appuyez sur le bouton `Install`. Choisissez le fichier LineageOS `lineage-XX.X-202XXXXX-microG-CODENAME.zip`. |
        | 2 | Faites glisser le curseur `Swipe to confirm Flash`, attendez que LineageOS pour microG soit installé et redémarrez le téléphone (cette fois-ci, un redémarrage « normal », pas un redémarrage *bootloader* ou *recovery*). |
        | 3 | Le premier démarrage peut prendre un certain temps. Félicitations, LineageOS pour microG est installé sur votre téléphone ! |


<br>

<center> <img src="../../assets/img/separator_microg.svg" alt="LineageOS microG" width="150px"></img> </center>

## microG

LineageOS pour microG embarque évidemment... [microG](https://microg.org/). Il s'agit d'une alternative open source à certaines bibliothèques et applications propriétaires de Google. En activant microG, vous pourrez continuer à utiliser des fonctionnalités comme les notifications instantanées ou la géolocalisation sans installer le « Google Services Framework (GSF)», gourmand en données. De nombreuses applis dépendantes du GSF peuvent ainsi être utilisées avec LineageOS pour microG. Notez cependant qu'elles sont toujours susceptibles de contacter les services de Google. Pour limiter davantage les fuites de données, nous recommandons de privilégier autant que possible les [applis libres, open source et indépendantes du GSF](https://gofoss.net/fr/foss-apps/).


=== "Géolocalisation"

    MicroG permet à votre téléphone d'établir sa position à partir d'une base de données locale contenant des informations sur les antennes relais. Ce mode de géolocalisation est donc totalement indépendant de tierces parties. Vous trouverez des instructions détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Ouvrez F-Droid et installez l'application nommée `Local GSM location`. |
        | 2 | Ouvrez les paramètres de microG. Allez dans le menu `Modules de localisation` et activez `Service de localisation GSM` ainsi que `Nominatim`. |
        | 3 | Allez dans `Auto-Test` et vérifiez que tout marche correctement. |

        </center>


=== "Notifications instantanées"

    LineageOS pour microG essaie d'éviter les services Google autant que possible. Il y a une exception : beaucoup d'applications reposent sur « Google Cloud Messaging (GCM) », un système propriétaire développé par Google pour afficher des notifications sur votre appareil. microG permet d'avoir accès à ces notifications instantanées en activant (de manière limitée) les services GCM. Vous trouverez plus de détails ci-après.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Ouvrez l'application de paramètres de microG. |
        | 2 | Naviguez vers `Enregistrement du terminal auprès de Google` et enregistrez votre appareil. |
        | 3 | Naviguez vers `Cloud Messaging` et activez les notifications instantanées. |

        </center>

    ??? question "Comment faire confiance à LineageOS pour microG si le système utilise Google Cloud Messaging?"

        Enregistrer votre appareil et activer les notifications instantanées est facultatif. En le faisant, vous dévoilez des informations limitées à Google, comme un identifiant unique. microG s'assure cependant d'enlever autant d'éléments identifiant que possible. Activer les notifications instantanées peut aussi permettre à Google de (partiellement) lire leur contenu, dépendant de l'usage que font les différentes applis du « Google Cloud Messaging ».


=== "F-Droid"

    [F-Droid](https://f-droid.org/fr/) est un magasin d'applications qui héberge exclusivement des [applis libres et open source](https://gofoss.net/fr/foss-apps/). Assurez-vous d'activer le dépôt microG dans F-Droid comme détaillé ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        |1 |Lancez F-Droid. |
        |2 |Allez dans le menu `Paramètres ► Dépôts`. |
        |3 |Activez le dépôt microG. |

        </center>


=== "Applis payantes"

    Installer des applications payantes sans le magasin Google peut être un peu compliqué. Voici une astuce:

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Description |
        | :------: | ------ |
        |1 |[Naviguez sur le magasin Google en ligne](https://play.google.com/store/). |
        |2 |Achetez des applications avec un ancien compte Google ou un compte dédié. |
        |3 |Connectez-vous à [Aurora Store](https://f-droid.org/fr/packages/com.aurora.store/) avec ce même compte Google. |
        |4 |Téléchargez les applications achetées. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance LineageOS" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions, référez-vous à la [documentation de LineageOS](https://lineage.microg.org/) ou demandez de l'aide à la [communauté LineageOS](https://lineageos.org/community/).

<div align="center">
<img src="https://imgs.xkcd.com/comics/xkcd_phone_2.png" alt="LineageOS"></img>
</div>

<br>

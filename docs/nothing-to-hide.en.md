---
template: main.html
title: GAFAM | What Is Big Tech? A Simple Guide To Data Privacy
description: You have nothing to hide? Learn more about data privacy, anonymity and Big Tech censorship. Without falling into the trap of conjuring a "tyranny of Big Tech".
---

# How GAFAM Threatens Data Privacy

!!! level "Last updated: March 2022."

<center> <img src="../../assets/img/privacy.png" alt="online privacy protection" width="700px"></img> </center>

## What is privacy?

You think you've got nothing to hide, and therefore nothing to fear? Think again, this argument is fallacious and denies the value of data privacy. It implies that the desire to keep certain aspects of your life private, including from Big Tech or GAFAM companies, necessarily means you're covering up wrongdoing.

Of course, that's not true. Not handing your unlocked phone to the first who comes along doesn't make you a criminal. There are more than a few aspects in your life worth keeping private, even without wrongdoing.

So, why is privacy important? Privacy helps us to freely express ourselves and explore our personality without being judged or stigmatised. Or to shield us from surveillance, censorship, manipulation and identity theft. That's why curtains were invented. And things like banking secrecy, attorney-client privilege, secrecy of correspondence, secrecy of the ballot, confessional secret or medical confidentiality.

We tend to forget that privacy is a fundamental right. It’s enshrined by the UN Human Rights Council, the International Covenant on Civil and Political Rights and a number of international privacy laws.

We need to prevent companies and governments from permanently recording our conversations, memories, location, medical history, and much, much more. To paraphrase [Geoffrey A. Fowler](https://www.washingtonpost.com/technology/2019/12/31/how-we-survive-surveillance-apocalypse/) from the Washington Post: *"Online privacy is not dead, but you have to be angry and patient enough to obtain it"*.

??? question "In the footsteps of Upton Sinclair's 1917 novel "The Profits of Religion""

    *"Not merely was my own mail opened, but the mail of all my relatives and friends — people residing in places as far apart as California and Florida. I recall the bland smile of a government official to whom I complained about this matter: "If you have nothing to hide you have nothing to fear." My answer was that a study of many labor cases had taught me the methods of the agent provocateur. He is quite willing to take real evidence if he can find it; but if not, he has familiarised himself with the affairs of his victim, and can make evidence which will be convincing when exploited by the yellow press."*

??? question "What does privacy mean? And what is the difference between privacy and security?"

    <center>

    | Concept | Description |
    | ------ | ------ |
    | Privacy |It's about keeping content private. Others can know who you are — but not what you think, say or do. |
    | Anonymity |It's about concealing your identity. Others can know what you think, say or do — but not who you are. |
    | Security |It's about putting security tools in place to protect against the invasion of your privacy. |

    </center>

    While more security often means more privacy or anonymity, it also means less convenience. [The right trade off depends on your individual threat model](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf). This complete privacy guide should be helpful if your objective is a reasonable level of privacy and data security. Not so much if you plan on becoming a spy, hiding from powerful adversaries or infringing laws.


<br>

<center> <img src="../../assets/img/separator_money.svg" alt="GAFAM meaning" width="150px"></img> </center>

## What is Big Tech?

Big Tech, or the so-called GAFAM companies, are large and dominant tech companies like Google, Apple, Facebook, Amazon or Microsoft. They excel at turning users into a product. Private data ownership is a key ingredient to their business model, which is usually build around seemingly "free" services, lock-in, tracking and ad targeting.

Google and Facebook have been most successful at this game. And while Apple, Amazon and Microsoft have not (entirely) built their business around spying, they remain privacy threats. In fact, they hugely benefit from doing business with data brokers and privacy offenders. All five companies — often called the "Big Five" or "Big Tech" — regularly face allegations or are condemned for [data collection and tracking malpractices](https://www.gizmodo.com.au/2021/05/google-location-services-lawsuit/), [civilian surveillance](https://www.theguardian.com/commentisfree/2021/may/18/amazon-ring-largest-civilian-surveillance-network-us), [breaches of privacy rules](https://edps.europa.eu/press-publications/press-news/press-releases/2021/edps-opens-two-investigations-following-schrems_en), [tax avoidance](https://www.theguardian.com/technology/2019/jan/03/google-tax-haven-bermuda-netherlands), [antitrust concerns](https://www.statista.com/chart/14752/eu-antitrust-fines-against-tech-companies/), [erosion of ethical standards](https://en.wikipedia.org/wiki/Criticism_of_Facebook), [reported labor abuses](https://www.theatlantic.com/technology/archive/2019/11/amazon-warehouse-reports-show-worker-injuries/602530/) and so on.

<center>
  <html>
   <embed src="../../assets/echarts/gafam_stats.html" style="width: 100%; margin-top:20px; height:400px">
  </html>
</center>

Yet in spite — or because of — these questionable practices, the market value of Big Tech is skyrocketing. In 2021, the value of these companies reached almost 9 trillion dollars. If Big Tech was a nation, it would be the world's third largest economy, right after the US and China. "Big Tech" is now worth more than Germany’s and the UK's gross domestic product – combined!

"*Information is power. But like all power, there are those who want to keep it for themselves*". As early as 2008, Aaron Swartz condemned private corporations for centralizing, digitizing and locking up information in his controversial [Guerilla Open Access Manifesto](https://archive.org/details/GuerillaOpenAccessManifesto/). Things haven't changed for the better.


??? question "What does Google know about me?"

    <center> <img src="../../assets/img/what_info_tech_companies_collect.png" alt="How can I stop online surveillance? What does Facebook know about me? What does Apple know about me? What does Amazon know about me? What does Microsoft know about me?" width="100%"></img> </center>

    Courtesy of [TruePeopleSearch.net](https://truepeoplesearch.net/).


<br>

<center> <img src="../../assets/img/separator_compatibility.svg" alt="Secure privacy" width="150px"></img> </center>


## What is mass surveillance?

Many governments cut back on civil liberties. Out of fear from protests, terrorism or diseases, emergency laws are put in place to stay. Ever more privacy intruding practices are deployed on a massive scale.

Yet instead of protecting the public interest, the data harvested by companies and governments is being misused to surveil the population, polarise the political debate and divide the public opinion. This evolution weakens Democracy. History has taught us time and again that mass surveillance doesn't make the world a safer place. Quite the contrary! Surveillance, electoral interference and censorship are the building blocks for oppression and state brutality.

It's time to fight back against surveillance capitalism and safeguard our coexistence in an increasingly digitised world. Benjamin Franklin would agree: *"They who can give up essential liberty to obtain a little temporary safety deserve neither liberty nor safety"*.


??? info "Tell me more about mass surveillance"

    <center>

    | Threats to Democracy | Description |
    | ------ | ------ |
    | Global surveillance programs |In the aftermath of the US war on terror, several whistle-blowers revealed the existence of [global mass surveillance programs](https://en.wikipedia.org/wiki/Global_surveillance_disclosures_(2013%E2%80%93present)). With more or less voluntary support of large corporations, governments throughout the world started to collect, store and share information on their citizens: online activities, phone calls, text messages, location history, etc. |
    | PRISM | NSA's PRISM surveillance program was first revealed by The Guardian and The Washington Post in 2013. As explained by [The Verge](https://www.theverge.com/2013/7/17/4517480/nsa-spying-prism-surveillance-cheat-sheet/), it's a tool to collect private data from Big Tech and other organisations. <br><br> <center><img src="../../assets/img/prism.png" align="center" alt="What is PRISM privacy?" width="500px"></img></center> |
    | XKeyscore| NSA's Xkeyscore program was first revealed by [The Guardian](https://www.theguardian.com/world/2013/jul/31/nsa-top-secret-program-online-data) in 2013. As explained by [The Intercept](https://theintercept.com/2015/07/01/nsas-google-worlds-private-communications/), it's a tool for mass surveillance, fed with Internet traffic from fiber optic cables. To quote Edward Snowden: *"I, sitting at my desk, could wiretap anyone, from you or your accountant, to a federal judge or even the president, if I had a personal email"*. <br><br> <center><img src="../../assets/img/xkeyscore.jpeg" align="center" alt="What is xkeyscore program" width="500px"></img></center> |
    | Boundless Informant | Boundless Informant is one of the NSA's big data analysis tools. In 2013, [The Guardian published the following heat map](https://www.theguardian.com/world/2013/jun/08/nsa-boundless-informant-global-datamining), on which countries range from green (least surveillance) through yellow and orange to red (most surveillance). [The Atlantic Wire](https://www.theatlantic.com/national/archive/2013/06/nsa-datacenters-size-analysis/314364/) estimated that in March 2013 alone, the NSA retrieved and stored 9,7 petabytes of data. It's hard to wrap one's head around such figures, but just consider that this amount of data corresponds more or less to the entire television programming of the past decade. <br><br> <center><img src="../../assets/img/boundlessinformant.png" align="center" alt="Security privacy, Security and privacy" width="500px"></img></center> |
    | Cambridge Analytica |Revelations around the role of [Cambridge Analytica](https://en.wikipedia.org/wiki/Cambridge_Analytica) during the US presidential campaign in 2016 or the UK's referendum on European Union membership illustrate how misuse of private data and biased algorithms influence democratic processes and fuel political echo chambers. <br><br> <center><img src="../../assets/img/cambridge_analytica.png" align="center" alt="Data and privacy" width="250px"></img></center> |
    | Tracking the pandemic |Many countries started tracing (or tracking) citizens based on their phone's location to contain the [COVID-19](https://www.top10vpn.com/research/covid-19-digital-rights-tracker/) outbreak. The data is collected via apps, or provided by telecom operators and tech firms such as [Google and Apple](https://www.theverge.com/2020/4/10/21216715/apple-google-coronavirus-covid-19-contact-tracing-app-details-use/). Behind a seemingly noble cause lies technology which enables digital tracking, physical surveillance and censorship. In fact, it didn't take long before the first [privacy flaws where discovered](https://themarkup.org/privacy/2021/04/27/google-promised-its-contact-tracing-app-was-completely-private-but-it-wasnt).|

    </center>


<center>
<img align="center" src="https://imgs.xkcd.com/comics/privacy_opinions.png" width="550px" alt="Why privacy matters"></img>
</center>

<br/>

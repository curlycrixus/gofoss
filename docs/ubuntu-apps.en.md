---
template: main.html
title: A list of 40 popular Ubuntu apps
description: Ubuntu apps. How to download Ubuntu apps. How to install Ubuntu apps. Ubuntu app store. Top Ubuntu apps. Ubuntu software center.
---

# A List of 40 Popular Ubuntu Apps

!!! level "For intermediate users. Some tech skills required."

<div align="center">
<img src="../../assets/img/laptop.png" alt="Most popular Ubuntu apps" width="300px"></img>
</div>


## Pre-installed apps

<center>

| Ubuntu apps | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/unzip.svg" alt="Ubuntu archive manager" width="50px"></img> | [Archive Manager](https://wiki.gnome.org/Apps/FileRoller) | Compress or extract archives (.zip, .rar, .tar, etc.). |
| <img src="../../assets/img/transmission.svg" alt="Ubuntu backups" width="50px"></img> | [Backups](https://apps.gnome.org/app/org.gnome.DejaDup/) | Backup files to local, remote or cloud locations. Possibility to encrypt backups and schedule incremental backups. |
| <img src="../../assets/img/calculator.svg" alt="Ubuntu calculator" width="50px"></img> | [Calculator](https://apps.gnome.org/app/org.gnome.Calculator/) | Perform arithmetic, scientific or financial calculations. |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu calendar" width="50px"></img> | [Calendar](https://apps.gnome.org/app/org.gnome.Calendar/) | Schedule appointments, keep track of your agenda. |
| <img src="../../assets/img/cheese.svg" alt="Ubuntu cheese" width="50px"></img> | [Cheese](https://apps.gnome.org/app/org.gnome.Cheese/) | Capture videos and stills from your webcam. |
| <img src="../../assets/img/baobab.svg" alt="Ubuntu baobab" width="50px"></img> | [Disk Usage Analyzer](https://apps.gnome.org/app/org.gnome.baobab/) |Scan device volumes or specific directories and display disk usage. |
| <img src="../../assets/img/mysql.svg" alt="Ubuntu disks" width="50px"></img> | [Disks](https://apps.gnome.org/app/org.gnome.DiskUtility/) |Inspect, format, partition and configure disks. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu document scanner" width="50px"></img> | [Document Scanner](https://apps.gnome.org/app/simple-scan/) |Scan, crop, rotate, print and save text and images. |
| <img src="../../assets/img/evince.svg" alt="Ubuntu evince" width="50px"></img> | [Document Viewer](https://apps.gnome.org/app/org.gnome.Evince/) | View file formats such as .pdf, .ps, .xps, .tiff, .djvu, .cbr, .cbz, etc. |
| <img src="../../assets/img/simplefilemanager.svg" alt="Ubuntu nautilus" width="50px"></img> | [Files](https://apps.gnome.org/app/org.gnome.Nautilus/) | Search, navigate and manage files. |
| <img src="../../assets/img/firefox.svg" alt="Ubuntu firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/en-US/firefox/new/) | Browse the web. |
| <img src="../../assets/img/simplegallery.svg" alt="Ubuntu eye of gnome" width="50px"></img> | [Image Viewer](https://wiki.gnome.org/Apps/EyeOfGnome) | View single pictures or image collections, view fullscreen slideshows or set wallpapers. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu libreoffice" width="50px"></img> | [LibreOffice](https://www.libreoffice.org/) | Process word documents, spreadsheets, presentations, etc. Fully compatible with Microsoft's formats (.doc, .xls and .ppt).|
| <img src="../../assets/img/ssh.svg" alt="Ubuntu password manager" width="50px"></img> | [Password & Keys](https://apps.gnome.org/app/org.gnome.seahorse.Application/) | Manage encryption keys (PGP, SSH). |
| <img src="../../assets/img/rhythmbox.svg" alt="Ubuntu rhythmbox" width="50px"></img> | [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox/) | Play albums, organise audio files, create playlists, listen to podcasts, and access other online media. |
| <img src="../../assets/img/cheese.svg" alt="Ubuntu screenshot" width="50px"></img> | [Screenshot](https://help.gnome.org/users/gnome-help/stable/screen-shot-record.html.en) | Save images of your screen or individual windows. |
| <img src="../../assets/img/shot_well.svg" alt="Ubuntu shotwell" width="50px"></img> | [Shotwell](https://wiki.gnome.org/Apps/Shotwell/) | Import, organise and view your photos. |
| <img src="../../assets/img/terminal.svg" alt="Ubuntu terminal" width="50px"></img> | [Terminal](https://apps.gnome.org/fr/app/org.gnome.Console/) | Access an interface to execute text based commands. |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu text editor" width="50px"></img> | [Text Editor](https://apps.gnome.org/fr/app/org.gnome.TextEditor/) | Edit text files. |
| <img src="../../assets/img/simpleemail.svg" alt="Ubuntu thunderbird" width="50px"></img> | [Thunderbird](https://www.thunderbird.net/en-US/) | Access your emails, calendars, address books and to-do lists. |
| <img src="../../assets/img/totem.svg" alt="Ubuntu totem" width="50px"></img> | [Videos](https://apps.gnome.org/app/org.gnome.Totem/) | Play movies. |

</center>


## Other popular apps

Previous chapters explained how to install popular apps such as [Firefox](https://gofoss.net/firefox/), [Tor](https://gofoss.net/tor/), [Protonmail & Tutanota](https://gofoss.net/encrypted-emails/), [Signal](https://gofoss.net/encrypted-messages/), [Keepass](https://gofoss.net/passwords/), or [Veracrypt & Cryptomator](https://gofoss.net/encrypted-files/). Many more applications are available for Ubuntu. Below a selection of popular apps you might find useful. There are two easy ways to install them:

* **The Software Center**, which was presented in the [previous chapter on Ubuntu](https://gofoss.net/ubuntu/). It's an app store to search, install, update or remove software. Click on the `Activities` button on the top left, search for `Software` and open it. Find an application, read through the description page, hit the `Install` button and enter the administrator (root) password to proceed.

* **The terminal** is another way to install, update or remove software. Open it with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. For example, the following terminal command installs Firefox: `sudo apt install firefox`. Voilà!

<center>

| Ubuntu apps | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu mousepad" width="50px"></img> | [Mousepad](https://github.com/codebrainz/mousepad/) | `Text editor`: simple, easy-to-use and fast. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install mousepad` |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu onlyoffice" width="50px"></img> | [OnlyOffice](https://www.onlyoffice.com/en/) | `Office suite`: free and open source software to process word documents, spreadsheets, presentations, etc. Fully compatible with Microsoft's formats (.doc, .xls and .ppt). Install via the Ubuntu Software Center or follow [OnlyOffice's instructions](https://helpcenter.onlyoffice.com/installation/desktop-install-ubuntu.aspx?_ga=2.123053453.764533964.1595236576-1157782750.1587541027/).|
| <img src="../../assets/img/maps.svg" alt="GNOME maps" width="50px"></img> | [Gnome Maps](https://apps.gnome.org/app/org.gnome.Maps/) | `Maps`: free open-source maps. Uses the collaborative OpenStreetMap database. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install gnome-maps`|
| <img src="../../assets/img/vlc.svg" alt="Ubuntu vlc" width="50px"></img> | [VLC](https://www.videolan.org/vlc/) | `Videos`: free open-source multimedia player. Supports most file formats and streaming protocols. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install vlc`|
| <img src="../../assets/img/totem.svg" alt="Ubuntu mplayer" width="50px"></img> | [Mplayer](http://mplayerhq.hu/) | `Videos`: free open-source media player. Supports most file formats. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install mplayer`|
| <img src="../../assets/img/ffmpeg.svg" alt="Ubuntu ffmpeg" width="50px"></img> | [Ffmpeg](https://ffmpeg.org/) | `Media processing`: open source tool to convert and modify different file formats such as .mp4, .avi, .mov, .mkv, .mp3, .wav, etc. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install ffmpeg`|
| <img src="../../assets/img/gimp.svg" alt="Ubuntu gimp" width="50px"></img> | [Gimp](https://www.gimp.org/) | `Image processing`: free and open source alternative to Adobe Photoshop, with plenty of add-ins for additional features. Supports file formats such as .bmp, .gif, .jpeg, .mng, .pcx, .pdf, .png, .ps, .psd, .svg, .tiff, .tga, .xpm, etc. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install gimp gimp-data`|
| <img src="../../assets/img/gimp.svg" alt="Ubuntu krita" width="50px"></img> | [Krita](https://krita.org/en/) | `Image processing`: free and open source painting program for concept art, texture & matte painters, illustrations and comics. Install via the Ubuntu Software Center or the following terminal command:<br> `$ sudo add-apt-repository ppa:kritalime/ppa && sudo apt update && sudo apt install krita`|
| <img src="../../assets/img/audacity.svg" alt="Ubuntu tenacity" width="50px"></img> | [Audacity](https://www.audacityteam.org/) | `Audio processing`: free and open source software to record, sample, edit, convert, analyse and add effects to sound files. *Caution*: [discussions around adding telemetry to Audacity](https://github.com/audacity/audacity/pull/835/) led to a fork called [Tenacity](https://tenacityaudio.org/), might be worthwhile to check out. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install audacity`|
| <img src="../../assets/img/openshot.svg" alt="Ubuntu openshot" width="50px"></img> | [OpenShot](https://www.openshot.org/) | `Video processing`: free and open source video editor to resize, scale, trim, cut, zoom, transition and add effects to video files. Install via the Ubuntu Software Center or the following terminal commands:<br> `sudo add-apt-repository ppa:openshot.developers/ppa && sudo apt update && sudo apt install openshot-qt` |
| <img src="../../assets/img/openshot.svg" alt="Ubuntu blender" width="50px"></img> | [Blender](https://www.blender.org/) | `Video processing`: free and open source 3D creation suite, including modeling, animation, simulation, rendering, motion tracking and video editing. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install snapd && sudo snap install blender --classic`|
| <img src="../../assets/img/games.svg" alt="Ubuntu steam" width="50px"></img> | [Steam](https://store.steampowered.com/linux/) | `Gaming`: distribution platform for video games. Play thousands of top rated games on your Linux computer. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install steam-installer`|
| <img src="../../assets/img/empathy.svg" alt="Ubuntu empathy" width="50px"></img> | [Empathy](https://wiki.gnome.org/action/show/Attic/Empathy?action=show&redirect=Apps%2FEmpathy) | `Chat`: chat per text, voice & video messages. Proprietary protocols are supported: Google Talk, MSN, IRC, AIM, Facebook, Yahoo, ICQ, etc. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install empathy` |
| <img src="../../assets/img/pidgin.svg" alt="Ubuntu pidgin" width="50px"></img> | [Pidgin](https://pidgin.im/) | `Chat`: free and open source messaging client. Supports many protocols including MSN, AIM, Yahoo, Jabber, IRC, IRQ, etc. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install pidgin`|
| <img src="../../assets/img/money.svg" alt="Ubuntu gnucash" width="50px"></img> | [GNUcash](https://gnucash.org/) | `Finance`: free and open source accounting software for personal and small-business finances. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install gnucash`|
| <img src="../../assets/img/deluge.svg" alt="Ubuntu deluge" width="50px"></img> | [Deluge](https://www.deluge-torrent.org/) | `Torrent`: free and lightweight torrent client. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install deluge`|
| <img src="../../assets/img/transmission.svg" alt="Ubuntu transmission" width="50px"></img> | [Transmission](https://transmissionbt.com/) |`Torrent`: transmit and receive files using the BitTorrent protocol. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install transmission` |
| <img src="../../assets/img/simplefilemanager.svg" alt="Ubuntu filezilla" width="50px"></img> | [FileZilla](https://filezilla-project.org/) | `FTP`: free FTP client. Install via the Ubuntu Software Center or the following terminal command:<br> `sudo apt install filezilla`|
| <img src="../../assets/img/unzip.svg" alt="Ubuntu rar" width="50px"></img> | [Rar, Unrar and Unzip](https://itsfoss.com/use-rar-ubuntu-linux/) | `File compression`: three small programs to compress or extract archives (.zip, .rar, .tar, etc.). Install via the Ubuntu Software Center or type the following terminal command:<br> `sudo apt install rar unrar unzip`|
| <img src="../../assets/img/bleachbit.svg" alt="Ubuntu bleachbit" width="50px"></img> | [Bleachbit](https://www.bleachbit.org/) | `Maintenance`: open source tool to keep the computer clutter free. Free up cache, delete cookies, clear Internet history, shred temporary files, delete logs, and discard junk. Install via the Ubuntu Software Center or type the following terminal command:<br> `sudo apt install bleachbit`|

</center>


<br>

<center> <img src="../../assets/img/separator_games.svg" alt="Ubuntu games" width="150px"></img> </center>

## Games

Linux is not perfect. From time to time, you'll run into limitations. For instance, many games aren't released for or compatible with Linux. Fortunately, there are some options.

### Steam

[Steam](https://store.steampowered.com/linux/) offers thousands of top rated games for Linux, as long as you meet the minimum requirements: 1GHz Pentium 4 or AMD Opteron processor, 512 MB memory, 5 GB hard drive space, an Internet connection and a [graphic card with latest drivers installed](https://gofoss.net/ubuntu/). To install Steam, click on the `Activities` button on the top left of the screen, open the Ubuntu Software Centre, search for `Steam` and click `Install`.

### Lutris

[Lutris](https://lutris.net/) is a free and [open source](https://github.com/lutris/) gaming platform for Linux. It allows to install thousands of games with little fuss, making Linux gaming a smooth experience. Before you install Lutris, make sure to [have the latest graphic drivers](https://gofoss.net/ubuntu/) installed. Then, follow the detailed instructions below.

??? tip "Show me a step-by-step guide"

    Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Run the commands below to install [Wine](https://www.winehq.org/), a program used to run Windows applications in Linux:

    ```bash
    sudo dpkg --add-architecture i386
    wget -nc https://dl.winehq.org/wine-builds/winehq.key
    sudo apt-key add winehq.key
    sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ jammy main'
    sudo apt update
    sudo apt install --install-recommends winehq-stable
    ```

    Now, install Lutris:

    ```bash
    sudo add-apt-repository ppa:lutris-team/lutris
    sudo apt update
    sudo apt install lutris
    ```

    Once installed, open Lutris, [register](https://lutris.net/user/register/) or login, [browse through the catalogue](https://lutris.net/games/) and install new games.


<br>

<center> <img src="../../assets/img/separator_virtualbox.svg" alt="Ubuntu virtualbox" width="150px"></img> </center>

## Windows Software

At times, a particular piece of software might not behave as expected on your Ubuntu machine. Running it in Windows might solve the issue. [VirtualBox](https://www.virtualbox.org/) is a great way to achieve this, as it basically runs a small Windows computer inside your Ubuntu machine. More details below.

??? tip "Show me a step-by-step guide"

    ### Install VirtualBox

    Make sure your Ubuntu device meets the minimum requirements: 4 GB RAM, 20 GB of free drive space. Click on the `Activities` button on the top left of the screen, open the Ubuntu Software Centre, search for `VirtualBox` and click `Install`. Or just open a terminal and run the command `sudo apt install virtualbox`.


    ### Configure VirtualBox

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Download Windows | Download a [Windows .iso](https://www.microsoft.com/en-gb/software-download/). Choose the 32 or 64 bit version, depending on your computer architecture. |
    | Create a Virtual Machine | Start VirtualBox and click on the button `New`. |
    | Name and operating system | Give your Virtual Machine (VM) a name, for example `Windows VM`. Also select the Operating System (`Microsoft Windows`) and the version, for example `Windows 10 (64-bit)`. |
    | Memory size | Choose how much RAM to allocate to Windows. 2 GB are recommended, 3-4 GB are even better. |
    | Hard disk | Select `Create a virtual hard disk now` to add a virtual hard disk. |
    | Hard disk file type | Choose the `VDI` format for the hard disk file type. |
    | Storage on physical hard disk | Choose `Dynamically allocated` for the hard disk file size. |
    | File location and size | Choose where to create the virtual disk. Also, decide how much disk space to allocate to Windows. 32 GB are recommended, more is even better. |
    | Optical Disk Selector |Back on the main screen, click on `Start` to launch the Virtual Windows Machine. In the pop-up dialogue, click on the icon to choose a virtual optical disk file. Click on `Add` and navigate to the location of the downloaded Windows `.iso` image. Click on `Open`, `Choose` and `Start`. |

    </center>

    ### Install Windows in VirtualBox

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Install Windows | After the boot phase, the Windows installation wizard will show up. Follow the on-screen instructions: select a language and keyboard layout, provide a Windows key, accept the terms of use, and so on. |
    | Start working with Windows | Log in to the first Windows session in the Virtual Machine. Going forward, you can launch Windows by clicking on the `Start` button on VirtualBox's main screen. |

    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu support" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [Ubuntu's documentation](https://help.ubuntu.com/), [Ubuntu's tutorials](https://ubuntu.com/tutorials), [Ubuntu's wiki](https://wiki.ubuntu.com/) or [Ubuntu's community](https://askubuntu.com/)

* [Steam's support](https://help.steampowered.com/en/), or the [Lutris Reddit community](https://teddit.net/r/lutris/)


Further suggestions for free and open source software can be found here:

* [Free Software Directory](https://directory.fsf.org/wiki/Main_Page)

* [Open Source Software Directory](https://opensourcesoftwaredirectory.com)

<br>

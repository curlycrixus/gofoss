---
template: main.html
title: Le guide du voyageur Fediverse
description: Comment fonctionne le Fediverse ? Mastodon vs Twitter. PeerTube vs YouTube. PixelFed vs Instagram. Friendica vs Facebook. Lemmy vs Reddit. Funkwhale.
---

# Le guide du voyageur Fediverse

!!! level "Dernière mise à jour: mai 2022. Destiné aux débutants. Aucune compétence technique particulière n'est requise."

<div align="center">
<img src="../../assets/img/fediverse.png" alt="Fediverse" width="400px"></img>
</div>

L'univers fédéré, ou « Fediverse », est une alternative libre et open source aux médias sociaux commerciaux.

Le Fediverse se compose de divers réseaux sociaux et services de microblogage interconnectés, où les utilisatrices et utilisateurs peuvent librement communiquer et partager des photos, des vidéos, de la musique et bien plus encore. Au moment d'écrire ces lignes, [le Fediverse comptait plus de 5 millions d'utilisateurs et d'utilisatrices](https://the-federation.info/). Le Fediverse est :

* **Décentralisé** : au lieu d'une seule entreprise, plusieurs [fournisseurs alternatifs](https://gofoss.net/fr/cloud-providers/) offrent un accès au Fediverse. Tout le monde peut gérer un serveur, aussi appelé « instance », et ainsi fournir des services à la communauté. C'est comme si n'importe quel internaute ou organisme pouvait gérer son propre serveur Twitter, Facebook, YouTube ou Instagram et inviter ses ami·e·s. à le rejoindre.
* **Fédéré** : les utilisatrices et utilisateurs de différents services peuvent communiquer entre eux, grâce au protocole [ActivityPub](https://activitypub.rocks/). C'est comme si les usagers de Twitter, Facebook, Instagram ou YouTube pouvaient interagir librement les un·e·s avec les autres.
* **Bienveillant** : le Fediverse n'est pas optimisé pour capter l'attention des internautes, créer des profils d'usagers et afficher autant de publicités et de contenu payant que possible. [Contrairement](https://quitsocialmedia.club/why) à Twitter, Facebook, YouTube, Instagram, Reddit et consorts.
* **Proche de l'humain** : il n'y a pas de filtres qui décident quels contenus les internautes peuvent voir dans leur chronologie et quels contenus seront censurés. Aucun algorithme n'impose aux internautes des contenus controversés pour les faire « réagir ». Il n'y a pas de modèles sombres (en anglais, « dark pattern ») pour influencer le comportement des internautes. Une fois de plus, c'est ce qui différencie la Fediverse des GAFAM.
* **Émancipateur** : nul besoin de céder vos droits d'auteur. Pas de conditions de service ou de politiques de modération arbitraires et opaques. Aucun mur numérique entre les réseaux sociaux. Contrairement aux plateformes commerciales gérées par les géants de la tech.


<br>


<center> <img src="../../assets/img/separator_mastodon.svg" alt="Mastodon" width="150px"></img> </center>

## Mastodon

<img src="../../assets/img/fediverse_mastodon.jpg" alt="Mastodon" width="300px" align="left"></img> [Mastodon](https://joinmastodon.org/) est l'alternative du Fediverse à Twitter. C'est un service de microblogage où les utilisatrices et utilisateurs suivent des comptes, publient des messages de 500 caractères (appelés *pouets* ou *« toots »*), retransmettent des contenus (appelés *« boosts »*) et ainsi de suite. Créé en 2016, [Mastodon est entièrement open source](https://github.com/mastodon/mastodon) et peut être auto-hébergé. Au moment d'écrire ces lignes, Mastodon compte près de 2 millions d'utilisateurs et d'utilisatrices.

<br>

??? tip "Lancez-vous avec Mastodon !"

    | Instructions | Description |
    | ------ | ------ |
    |Créez un compte |Chaque utilisatrice et utilisateur de Mastodon a un identifiant unique, qui fonctionne comme une adresse électronique et ressemble à ceci : `@nom@instance.social`. Créez un nouveau compte auprès de l'instance de votre choix, ou hébergez vous-même une instance. Voici une sélection d'instances actives : <br>• [Communautés Mastodon](https://joinmastodon.org/communities) <br>• [Liste d'instances Mastodon](https://instances.social/) |
    |Connectez-vous |Connectez-vous à votre instance via l'appli web de Mastodon, ou via l'un des clients Mastodon. Vous aurez ainsi accès à votre chronologie, où sont affichés les messages des utilisateurs et utilisatrices de votre instance ou d'autres instances. |
    |Rédigez un message |• Interface Web : utilisez la boîte de messages affichée dans le panneau de gauche <br>• Applications client : cliquez sur l'icône `crayon` <br>• Rédigez votre pouet <br>• Vous pouvez également ajouter des liens, fichiers multimédias, mentions, émojis, sondages ou avertissements relatifs au contenu <br>• Définissez qui peut voir votre message |
    |Paramètres de confidentialité |• Les messages `publics` sont visibles par tou·tes <br>• Les messages `non listés` ne sont visibles que par vos abonné·es, les usagers mentionnés ou les usagers consultant votre profil <br>• Les messages `réservés aux abonné·es` ne sont visibles que par vos abonné·es ou les usagers mentionnés <br>• Les messages `privés` ne sont visibles que par les usagers mentionnés <br>• Indépendamment des paramètres de confidentialité, les administrateurs du serveur peuvent accéder à tous vos messages, puisqu'ils ne sont pas chiffrés |
    |Avertissements relatifs au contenu |Du contenu sensible peut être masqué tant que les usagers ne cliquent sur un lien dans votre message. Cela peut être utile si vous partagez du contenu sensible avec vos abonné·es. |
    |« Hashtags » |Vous pouvez ajouter des « hashtags » `#` à vos pouets, ou cliquer sur des mots préfixés d'un « hashtags » `#` pour trouver du contenu similaire. |
    |Clients |[Pinafore](https://pinafore.social/) et [Halcyon](https://www.halcyon.social/) sont des clients web ressemblant à Twitter. En outre, plusieurs clients [mobiles ou de bureau sont disponibles pour Mastodon](https://joinmastodon.org/apps). Fonctionne également sur les téléphones dégooglés tournant sous [CalyxOS](https://gofoss.net/fr/calyxos/) ou [LineageOS](https://gofoss.net/fr/lineageos/). |


<br>


<center> <img src="../../assets/img/separator_peertube.svg" alt="PeerTube" width="150px"></img> </center>

## PeerTube

<img src="../../assets/img/fediverse_peertube.png" alt="PeerTube" width="300px" align="left"></img> [PeerTube](https://joinpeertube.org/fr) est l'alternative du Fediverse à YouTube, Vimeo ou Dailymotion. Il s'agit d'une plateforme de diffusion multimédia où les utilisatrices et utilisateurs visionnent, aiment ou téléchargent des vidéos. Créé en 2015, [PeerTube est entièrement open source](https://github.com/Chocobozzz/PeerTube) et peut être auto-hébergé. Au moment d'écrire ces lignes, PeerTube compte près de 200 000 utilisateurs et utilisatrices.

<br>

??? tip "Lancez-vous avec PeerTube !"

    | Instructions | Description |
    | ------ | ------ |
    |Créez un compte |Créez un nouveau compte auprès de l'instance de votre choix, ou hébergez vous-même une instance. Voici une sélection d'instances actives : <br>• [Intances PeerTube](https://instances.joinpeertube.org/instances) <br>• [FediDB](https://fedidb.org/network/?s=peertube) |
    |Connectez-vous |Connectez-vous à votre instance via l'appli web de PeerTube, ou l'un des clients PeerTube. Vous aurez accès à votre profil et à de nombreuses vidéos. |
    |Regardez des vidéos |L'interface est assez intuitive : lisez et mettez en pause des vidéos, ajustez le volume, modifiez la qualité et la vitesse de la vidéo, activez les sous-titres, passez en mode plein écran, téléchargez et partagez les vidéos, etc. |
    |Publiez des vidéos |<br>• Cliquez sur le bouton `Publier` <br>• Téléversez un fichier vidéo, depuis votre appareil, via une URL ou un lien torrent <br>• Importez facilement des vidéos de YouTube, Vimeo, Dailymotion, etc. en fournissant l'URL correspondante dans l'onglet `Importer avec un URL` <br>• PeerTube prend en charge différents formats : mp4, ogv, webm, flv, mov, avi, mkv, mp3, ogg & flac <br>• Ajoutez des informations à votre téléversement : titre, balises, descriptions, catégorie, licence, etc. <br>• Sélectionnez une chaîne <br>• Ajustez les paramètres de confidentialité <br>• Cliquez sur le bouton `Publier` <br>• Cliquez sur le bouton `Partager` pour faire connaître la vidéo à vos ami·es et contacts <br>• Cliquez sur `Partager ‣ Intégration` pour copier le lien et intégrer la vidéo à votre blog ou site web |
    |Créez des chaînes |PeerTube propose aussi des « chaînes », qui permettent de regrouper vos vidéos par thème. Gérez vos chaînes sous `Ma bibliothèque ‣ Chaînes`. |
    |Créez des listes de lecture |PeerTube propose également des « listes de lecture », permettant d'ajouter des vidéos à votre collection. Ces listes de lecture peuvent être partagées avec d'autres personnes ou rester privées. Gérez vos listes de lecture sous `Ma bibliothèque ‣ Listes de lecture`. |
    |Abonnez-vous |Cliquez sur le bouton `S'abonner` pour suivre des chaînes ou des listes de lecture d'autres utilisateurs et utilisatrices. Vous pouvez vous abonner avec votre compte local, d'autres comptes (comme Mastodon) ou via RSS. |
    |Recherchez des vidéos |Recherchez des vidéos, des chaînes ou des listes de lecture en saisissant des mots-clés dans la barre de recherche. En fonction des paramètres de vos instances, les résultats peuvent afficher des vidéos provenant de plusieurs instances, ou uniquement de votre propre instance. Vous pouvez également utiliser un moteur de recherche comme [Sepia Search](https://search.joinpeertube.org/fr/). |
    |Paramètres de confidentialité |• Les vidéos `publiques` sont visibles par tou·tes <br>• Les vidéos `internes` ne sont visibles que par les usagers connectées à votre instance <br>• Les vidéos `non listées` ne sont visibles que par les usagers en possession du lien privé <br>• Les vidéos `privées` ne sont visibles que par vous |
    |Autres paramètres |Reportez-vous à la section `Mon compte ‣ Paramètres` pour mettre à jour votre profil, modifier le mot de passe, consulter l'espace de stockage, spécifier le mode d'affichage des contenus sensibles, sélectionner une langue, gérer les notifications, supprimer votre compte, etc. |
    |Clients |Plusieurs clients [mobiles ou de bureau sont disponibles pour PeerTube](https://docs.joinpeertube.org/use-third-party-application). |


<br>


<center> <img src="../../assets/img/separator_pixelfed.svg" alt="Pixelfed" width="150px"></img> </center>

## PixelFed

<img src="../../assets/img/fediverse_pixelfed.png" alt="Pixelfed" width="200px" align="left"></img> [PixelFed](https://pixelfed.org/) est l'alternative du Fediverse à Instagram. Il s'agit d'un réseau social où les utilisatrices et utilisateurs partagent, aiment et commentent des photos et des vidéos. Créé en 2018, [Pixelfed est entièrement open source](https://github.com/pixelfed/pixelfed) et peut être auto-hébergé. Au moment d'écrire ces lignes, Pixelfed compte plus de 50 000 utilisateurs et utilisatrices.

<br>

??? tip "Lancez-vous avec PixelFed !"

    | Instructions | Description |
    | ------ | ------ |
    |Créez un compte |Créez un nouveau compte auprès de l'instance de votre choix, ou hébergez vous-même une instance. Voici une sélection d'[instances PixelFed actives](https://fedidb.org/software/pixelfed). |
    |Connectez-vous |• Connectez-vous à votre instance via l'appli web de PixelFed, ou l'un des clients PixelFed <br>• Cliquez sur `Menu ‣ Paramètres ‣ Compte` pour modifier votre photo de profil, votre nom, votre bio, votre mot de passe, etc. |
    |Chronologie |• Cliquez sur `Menu ‣ My Feed` pour accéder aux messages des utilisateurs et utilisatrices que vous suivez <br>• Cliquez sur `Menu ‣ Public Feed` pour accéder aux messages des autres utilisateurs et utilisatrices de votre instance <br>• Cliquez sur `Menu ‣ Network Feed` pour accéder aux messages d'autres instances |
    |Publiez vos photos et vidéos |• Cliquez sur le bouton `New` pour téléverser une ou plusieurs photos ou vidéos <br>• PixelFed supporte différents formats : jpeg, gif, png, mp4 ou webp <br>• Facultatif : ajoutez des légendes, avertissements sur le contenu, balises, licences ou emplacements <br>• Définissez qui peut voir vos photos <br>• Les photos et vidéos `publiques` sont visibles par tou·tes <br>• Les photos et vidéos `non listées` ne sont visibles que par les usagers qui vous suivent et sur votre profil <br>• Les photos et vidéos `réservées aux abonné·es` ne sont visibles que par vos abonné·es |
    |Découvrez des personnes et du contenu |Cliquez sur `Menu ‣ Découvrir` pour explorer les sujets d'actualité et le contenu recommandé. |
    |Messages directs |Cliquez sur le bouton `Direct` pour envoyer des messages directs à d'autres utilisateurs et utilisatrices. Ces messages peuvent contenir des photos, vidéos, messages, texte, etc. |
    |« Hashtags » |Vous pouvez ajouter des « hashtags » `#` aux libellés de vos propres messages, et suivre des « hashtags » pour rester informé des sujets qui vous intéressent. |
    |« Stories »|Cliquez sur `New ‣ New Story` pour partager des moments spéciaux avec vos abonné·es pendant 24 heures. |
    |Collections |Cliquez sur `New ‣ New Collection` pour regrouper des messages dans une collection. |
    |Paramètres de confidentialité |• Cliquez sur `Menu ‣ Paramètres ‣ Confidentialité` <br>• Cochez la case `Compte privé` pour limiter l'accès à vos photos et vidéos à vos abonné·es <br>• Cochez la case `Refuser l'indexation par les moteurs de recherche` pour masquer vos photos et vidéos aux moteurs de recherche |
    |Authentification à deux facteurs (2FA) |• Cliquez sur `Menu ‣ Paramètres ‣ Sécurité` <br>• Saisissez votre mot de passe <br>• Cliquez sur `Activer l'authentification à deux facteurs` <br>• Scannez le code QR avec votre appli mobile 2FA, par exemple [andOTP](https://gofoss.net/fr/passwords/#clients-2fa) <br>• Saisissez le code à 6 chiffres dans le formulaire web de PixelFed <br>• [Conservez les clés de sauvegarde](https://gofoss.net/fr/passwords/#keepass) fournies par PixelFed. Elles permettent de récupérer l'accès à PixelFed sans votre téléphone |
    |Clients |[PixelDroid](https://f-droid.org/fr/packages/org.pixeldroid.app/) est un client mobile pour Android. |


<br>


<center> <img src="../../assets/img/separator_lemmy.svg" alt="Lemmy" width="150px"></img> </center>

## Lemmy

<img src="../../assets/img/fediverse_lemmy.webp" alt="Lemmy" width="300px" align="left"></img> [Lemmy](https://join-lemmy.org/) est l'alternative du Fediverse à Reddit. Il s'agit d'un réseau social où les utilisatrices et utilisateurs rejoignent des communautés et publient, aiment ou commentent des messages et des photos. Créé en 2019, [Lemmy est entièrement open source](https://github.com/LemmyNet/lemmy) et peut être auto-hébergé. Au moment d'écrire ces lignes, Lemmy compte prêt de 20 000 utilisateurs et utilisatrices.

<br>

??? tip "Lancez-vous avec Lemmy !"

    | Instructions | Description |
    | ------ | ------ |
    |Créez un compte |Créez un nouveau compte auprès de l'instance de votre choix, ou hébergez vous-même une instance. Voici une sélection d'instances actives : <br>• [Serveurs Lemmy](https://join-lemmy.org/instances) <br>• [FediDB](https://fedidb.org/software/lemmy) |
    |Connectez-vous |Connectez-vous à votre instance via l'appli web de Lemmy, ou l'un des clients Lemmy. |
    |Abonnez-vous |• Cliquez sur `Menu ‣ Communautés` <br>• Abonnez-vous à toute communauté qui vous intéresse |
    |Publiez |• Cliquez sur `Menu ‣ Créer une publication` <br>• Ajoutez une URL, une image, un titre ou un texte <br>• Sélectionnez une communauté <br>• Facultatif : fournissez un avertissement sur le contenu <br>• Appuyez sur `Créer` |
    |Autres paramètres |Reportez-vous à la section `Menu ‣ Nom d'utilisateur ‣ Paramètres` pour mettre à jour votre profil, changer le mot de passe, spécifier comment afficher les contenus sensibles, supprimer votre compte, etc. |
    |Clients |Plusieurs clients [mobiles ou de bureau sont disponibles pour Lemmy](https://join-lemmy.org/apps). |


<br>


<center> <img src="../../assets/img/separator_friendica.svg" alt="Friendica" width="150px"></img> </center>

## Friendica

<img src="../../assets/img/fediverse_friendica.png" alt="Mastodon" width="300px" align="left"></img> [Friendica](https://friendi.ca/) est l'alternative du Fediverse à Facebook. Il s'agit d'un réseau social où les utilisatrices et utilisateurs s'inscrivent à des forums pour poster, voter et commenter des liens et des discussions. Créé en 2010, [Friendica est entièrement open source](https://github.com/friendica/friendica) et peut être auto-hébergé. Au moment d'écrire ces lignes, Friendica compte plus de 10 000 utilisateurs et utilisatrices.

<br>

??? tip "Lancez-vous avec Friendica !"

    | Instructions | Description |
    | ------ | ------ |
    |Créez un compte |Créez un nouveau compte auprès de l'instance de votre choix, ou hébergez vous-même une instance. Voici une sélection d'instances actives : <br>• [Répertoire Friendica](https://dir.friendica.social/servers) <br>• [FediDB](https://fedidb.org/network/?s=friendica) |
    |Connectez-vous |Connectez-vous à votre instance via l'appli web de Friendica, ou l'un des clients Friendica. Vous aurez accès à votre page de profil, où sont affichés les statuts et messages. |
    |Publiez votre statut |• Cliquez sur l'icône `crayon` ou sur la case `Partager` <br>• Saisissez votre statut <br>• Facultatif : ajoutez des liens, fichiers multimédias ou un emplacement <br>• Définissez qui peut voir votre message |
    |Onglet réseau |C'est ici que vous pouvez voir, commenter ou « liker » les publications de vos ami·es ou groupes. |
    |Rapprochez-vous des gens |Trouvez d'autres gens dans le [répertoire des utilisateurs et utilisatrices de Friendica](http://dir.friendica.social/). Friendica fera également des suggestions d'amitiés. |
    |Groupes, forums et pages communautaires |Si vous ne connaissez personne sur Friendica, rejoignez l'un des nombreux groupes et forums, ou l'une des pages communautaires. Vous pourrez trouver des gens partageant vos intérêts et entrer en contact. |
    |Connecteurs |En fonction des paramètres de votre instance, vous pourrez peut-être [interagir avec des services extérieurs à Friendica](https://forum.friendi.ca/help/Connectors), tels que Twitter, Diaspora, GNU Social ou des flux RSS. |
    |Clients |Plusieurs clients [mobiles ou de bureau sont disponibles pour Friendica](https://friendi.ca/resources/mobile-clients/). |


<br>


<center> <img src="../../assets/img/separator_funkwhale.svg" alt="Funkwhale" width="150px"></img> </center>

## Funkwhale

<img src="../../assets/img/fediverse_funkwhale.png" alt="Funkwhale" width="300px" align="left"></img> [Funkwhale](https://funkwhale.audio/) est l'alternative du Fediverse à YouTube, Spotify ou SoundCloud. Il s'agit d'un réseau social où les utilisatrices et utilisateurs écoutent, partagent ou téléchargent de la musique, des podcasts et des émissions de radio. Créé en 2015, [Funkwhale est entièrement open source](https://dev.funkwhale.audio/funkwhale/funkwhale) et peut être auto-hébergé. Au moment d'écrire ces lignes, Funkwhale compte prêt de 5 000 utilisateurs et utilisatrices.

<br>

??? tip "Lancez-vous avec Funkwhale !"

    | Instructions | Description |
    | ------ | ------ |
    |Étiquetage |Avant d'utiliser Funkwhale, assurez-vous que votre bibliothèque musicale est correctement étiquetée. Un bon étiquetage permet à Funkwhale d'afficher plus facilement des informations utiles telles que les pochettes d'album ou les données sur l'artiste. [Picard](https://picard.musicbrainz.org/?l=fr) est un outil d'étiquetage multiplateforme qui vous aide à gérer votre bibliothèque musicale. |
    |Créez un compte |Créez un nouveau compte auprès de l'instance de votre choix, ou hébergez vous-même une instance. Voici une sélection d'instances actives : <br>• [Serveurs Funkwhale](https://network.funkwhale.audio/dashboards/d/overview/funkwhale-network-overview?orgId=1&refresh=2h) <br>• [FediDB](https://fedidb.org/network/?s=funkwhale) |
    |Téléversez du contenu |• Connectez-vous à votre instance via l'appli web de Funkwhale, ou l'un des clients Funkwhale <br>• Cliquez sur l'icône `Ajouter du contenu` <br>• Sélectionnez une chaîne (pour publier de la musique) ou une bibliothèque (pour vos collections publiques et/ou privées) |
    |Créez des chaînes |Funkwhale propose des chaînes, qui permettent de gérer votre musique publique. Ces chaînes peuvent être suivies à partir de différentes instances de Funkwhale, ainsi que d'autres services Fediverse (par exemple, Mastodon). Pour suivre une chaîne d'un autre utilisateur ou d'une autre utilisatrice, cliquez simplement sur `Abonner`. Pour gérer vos propres chaînes, cliquez sur `Télécharger ‣ Publier votre travail sur une chaîne ‣ Se lancer`. |
    |Créez des bibliothèques |Funkwhale propose également des bibliothèques, qui permettent de gérer vos collections de musique publiques et privées. Gérez vos bibliothèques sous `Télécharger ‣ Télécharger du contenu tiers dans une bibliothèque ‣ Se lancer`. Les bibliothèques présentent différents niveaux de confidentialité : <br>• Les bibliothèques `publiques` sont visibles par tou·tes <br>• Les bibliothèques `locales` sont visibles par les autres usagers de votre instance <br>•  Les bibliothèques `privées` ne sont visibles que par vous <br>• Indépendamment du niveau de confidentialité, le lien d'une bibliothèque peut être partagé avec des usagers de votre choix afin de leur accorder l'accès <br><br> **Attention** : notez que vous devez toujours détenir les droits d'auteur appropriés, en particulier si vous utilisez des bibliothèques publiques ou locales. |
    |Créez des listes de lecture |Funkwhale propose également des listes de lecture, qui peuvent contenir votre musique téléversée ainsi que du contenu d'autres usagers. Gérez vos listes de lecture sous `Musique ‣ Listes de lecture`. |
    |Clients |Plusieurs clients [mobiles ou de bureau sont disponibles pour Funkwhale](https://funkwhale.audio/fr_FR/apps/). |


<br>


<center> <img src="../../assets/img/separator_permissions.svg" alt="Assitance" width="150px"></img> </center>

## Assistance

Pour davantage de précisions ou en cas de questions, consultez :

* la [documentation Mastodon (en anglais)](https://docs.joinmastodon.org/) ou le [forum Mastodon (en anglais)](https://discourse.joinmastodon.org/)
* la [documentation PeerTube (en anglais)](https://docs.joinpeertube.org/), la [foire à question](https://joinpeertube.org/faq) ou le [forum PeerTube](https://framacolibri.org/c/peertube/38)
* la [documentation PixelFed (en anglais)](https://docs.pixelfed.org/) ou la [page d'assistance PixelFed](https://pixelfed.social/site/help)
* la [documentation Friendica (en anglais)](https://friendi.ca/resources/use-it/) ou [la page de support Friendica (en anglais)](https://forum.friendi.ca/help/)
* la [documentation Lemmy (en anglais)](https://join-lemmy.org/docs/en/index.html)
* la [documentation Funkwhale (en anglais)](https://docs.funkwhale.audio/) ou le [forum Funkwhale (en anglais)](https://forum.funkwhale.audio/)

<center>
<img align="center" src="https://imgs.xkcd.com/comics/social_media.png" alt="Fediverse"></img>
</center>

<br>
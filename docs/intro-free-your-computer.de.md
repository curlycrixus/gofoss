---
template: main.html
title: So installiert Ihr Linux auf Eurem Computer
description: Was ist Linux? Welche Linux Version ist die beste? Ist Linux besser als Windows? Ist Linux besser als macOS? Was ist Ubuntu?
---

# Ubuntu & Linux Mint – <br> Beste Linux-Distros für Einsteiger

!!! level "Letzte Aktualisierung: März 2022"

<center>
<html>
<embed src="../../assets/echarts/os_stats.html" style="width: 100%; height:520px">
</html>
</center>

## Was ist Linux?

[Linux](https://de.wikipedia.org/wiki/Linux) ist ein Open-Source-Betriebssystem. Es handelt sich um eine großartige Alternative für all diejenigen, die ihre Privatsphäre nicht den Datenkraken anvertrauen wollen. Im Vergleich zu seinen proprietären Konkurrenten bietet Linux deutliche Vorteile: Linux ist frei und quelloffen, respektiert die Privatsphäre der NutzerInnen, läuft auch auf älterer Hardware, verfügt über eine tolle Nutzergemeinschaft und lässt sich leicht auf dem neuesten Stand halten. Linux, das lange Zeit ein Nischendasein fristete, ist mündig geworden: Linux gibts nun sogar auf dem Mars, dank des Perseverance Rover der Nasa! Und Linux gibt's in vielerlei Varianten, wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) und dessen Derivate.

## Solltet Ihr zu Linux wechseln?

Durchschnittliche BenutzerInnen verbringen [zwischen zwei und sechs Stunden pro Tag](https://www.bondcap.com/report/it19/) am Computer. Auf den meisten dieser Geräte läuft Windows. Manchmal auch macOS, ganz selten ChromeOS. Folglich teilen die NutzerInnen (un)freiwillig eine Menge Daten mit [Microsoft](https://privacy.microsoft.com/de-de/privacystatement/), [Apple](https://www.apple.com/legal/privacy/de-ww/) oder [Google](https://policies.google.com/privacy?hl=de&gl=DE/). Unvermeidlich? Nein — das Schöne an Computern ist, dass sie [grundsätzlich](https://de.wikipedia.org/wiki/Turing-Vollst%C3%A4ndigkeit) dazu in der Lage sind, alle Arten von Betriebssystemen einzusetzen. Inklusive Linux!



??? info "Hier erfahrt Ihr mehr über Bedenken mit Windows, macOS und ChromeOS"

    <center>

    | Bedenken | Beschreibung |
    | ------ | ------ |
    |Datenschutzverletzungen |Microsoft, Apple und Google sammeln, verwerten, übermitteln und veröffentlichen Nutzerdaten, manchmal anonymisiert, manchmal nicht. |
    |Proprietäre Software |Windows, macOS und ChromeOS sind, verwenden oder fördern Software, deren Quellcode geheim gehalten wird. Es gibt damit keine Möglichkeit herauszufinden, was genau auf dem Computer der NutzerInnen vor sich geht. |
    |Digitale Handschellen (DRM) | Microsoft, Apple und Google schränken mit digitalen Handschellen (auf Englisch "DRM") die Möglichkeiten der NutzerInnen ein, ihre Rechner frei zu verwenden. |
    |Geplante Obsoleszenz |Microsofts, Apples und Googles Aktualisierungspolitik zwingt die NutzerInnen, funktionsfähige Soft- und Hardware regelmäßig zu erneuern. Auf Kosten der NutzerInnen, und auf Kosten der Umwelt. |
    |Fehlende Interoperabilität |Microsoft, Apple und Google entwickeln Dienstleistungen, die mit keinen anderen Alternativen kompatibel sind. Dadurch versuchen sie, ihre NutzerInnen an sich zu binden. |
    |Sicherheitslücken |Obwohl Windows, macOS und ChromeOS von Großunternehmen gewartet werden, weisen diese Betriebssysteme immer wieder erhebliche Schwachstellen auf und werden zum Ziel von Spähprogrammen, Schadprogrammen und Viren. |

</center>



??? question "Ist Linux DIE Lösung?"

    Nein. Linux ist nicht für jede oder jeden geeignet. Alles hängt von Euren Bedürfnissen, Eurer Hardware und Eurer Bereitschaft zur Veränderung ab. Ein Umstieg auf Linux ist jedoch mit Sicherheit einen Versuch wert. [Ubuntu](https://ubuntu.com/) oder [Linux Mint](https://linuxmint.com) sind zwei empfehlenswerte Einsteigeroptionen. Sie bieten eine [reiche Auswahl an tollen Apps](https://gofoss.net/de/ubuntu-apps/), alles funktioniert mehr oder weniger sofort nach dem ersten Start, und beide Linux-Varianten werden von aktiven Gemeinschaften unterstützt:

    * **Ubuntu**: schaut mal im [Benutzerforum](https://ubuntuusers.de/) vorbei, oder in diesem [englischsprachigen Forum](https://ubuntuforums.org/), oder aber im [Wiki](https://wiki.ubuntuusers.de/Startseite/)
    * **Linux Mint**: schaut mal im [Benutzerforum](https://forums.linuxmint.com/viewforum.php?f=64) vorbei, oder werft einen Blick in die [Dokumentation](https://linuxmint.com/documentation.php)

    Linux gibt es in vielerlei Varianten, die auch "Distros" genannt werden. Jede Distro hat ihre Eigenheiten:

    <center>

    | Distro | Beschreibung |
    | ------ | ------ |
    | [Distro chooser](https://distrochooser.de/de/) | Tolle Webseite zur Orientierungshilfe. |
    | [Debian](https://www.debian.org/index.de.html) | Eine der weitverbreiteten Distros, die vollständig quelloffen ist (es besteht die Möglichkeit, proprietäre Elemente hinzuzufügen). Offener und transparenter Umgang mit Sicherheitslücken. Bietet aktive Unterstützung, z.B. in diesem [Forum](https://debianforum.de/forum/) oder im [Wiki](https://wiki.debian.org/de/FrontPage). |
    | [Fedora](https://getfedora.org/de/) | Innovative, freie und quelloffene Plattform für Hardware, die Cloud und Container. |
    | [Arch](https://archlinux.org/) | Ressourcenarme und flexible Linux-Variante, die versucht Dinge einfach zu halten (KISS-Prinzip). Arch Linux verwendet keine grafische Oberfläche zur Installation oder Konfiguration des Betriebssystems. Die Arch-Gemeinschaft bietet aktive Unterstützung an, z.B. in diesem [Forum](https://forum.archlinux.de/) oder im [Wiki](https://wiki.archlinux.de/title/Hauptseite). |
    | [Qubes OS](https://www.qubes-os.org/) | Quelloffenes Betriebssystem, das auf hohe Sicherheit ausgelegt ist. Wird u.a. von Edward Snowden genutzt. |
    | [Tails](https://tails.boum.org/index.de.html) | Live-Betriebssystem, das auf fast jedem Computer von einem USB-Stick oder einer DVD aus läuft. Zielt darauf ab, Datenschutz und Anonymität zu gewährleisten oder Zensuren zu umgehen, indem Internetverbindungen über das Tor-Netzwerk erzwungen werden. |
    | [Knoppix](http://www.knopper.net/knoppix/index.html) | Bootfähiges Live-Betriebssystem auf CD, DVD oder USB-Stick. |
    | [Pure OS](https://pureos.net/) | Benutzerfreundliches, sicheres und datenschutzfreundliches Betriebssystem für den täglichen Gebrauch. |
    | [MX Linux](https://mxlinux.org/) | Elegante, konfigurierbare, stabile, leistungsfähige und ressourcenarme Linux-Variante. Die MX-Linux_Gemeinschaft bietet aktive Unterstützung an, z.B. in diesem [Forum](https://forum.mxlinux.org/viewforum.php?f=136) oder im [Wiki](https://wiki.mxlinuxusers.de/wiki/Hauptseite). |
    | [Manjaro](https://manjaro.org/) | Professionelles Betriebssystem, geeigneter Ersatz für Windows oder macOS. Enthält proprietäre Elemente, wie z.B. Multimedia-Codecs. Die Manjaro-Gemeinschaft bietet aktive Unterstützung an, z.B. in diesem [Forum](https://www.manjaro-forum.de/) oder im [Wiki](https://wiki.manjaro.org/index.php/Main_Page/de). |
    | [Trisquel](https://trisquel.info/de) | Freies Betriebssystem für PrivatnutzerInnen, kleine Unternehmen oder Bildungseinrichtungen. Basiert auf Ubuntu. Frei von proprietärer Software. |
    | [Gentoo](https://www.gentoo.org/) | Hochgradig flexibles, quellbasierte Linux-Distro. |
    | [Alpine Linux](https://www.alpinelinux.org/) | Sicherheitsorientierte, ressourcenarme Linux-Distro, basiert auf musl libc und busybox. |
    | [Parrot](https://parrotsec.org/) | Freie und quelloffene Linux-Distro für Sicherheitsexperten, Entwickler und datenschutzbewusste NutzerInnen. |

    </center>


??? info "Lass mich dich kurz unterbrechen...."

    Was du als Linux bezeichnest, ist in Wirklichkeit GNU/Linux, oder, wie ich es in letzter Zeit nenne, GNU plus Linux. Linux ist in sich selbst kein Betriebssystem, sondern eine weitere, freie Komponente eines voll funktionellen GNU-Systems, das erst durch die GNU Kernbibs, Muschel-Betriebsmittel sowie vitale Systemkomponenten, die ein volles BS nach PBSSX - Standard ausmachen, nützlich gemacht wird.

    Viele Rechner -Nutzer verwenden eine modifizierte Version des GNU-Systems jeden Tag, ohne es zu merken. Durch eine seltsame Wendung wird die GNU-Version, die größte Verbreitung findet, oft Linux genannt, und vielen Nutzern ist nicht bewusst, dass es grundsätzlich das GNU-System, entwickelt vom GNU-Projekt, ist. Es gibt wirklich ein Linux, und diese Leute verwenden es, aber es ist nur ein Teil des Systems, das sie nutzen.

    Linux ist der Kern: Das Programm im System, das die Reichtümer der Maschine den anderen laufenden Programmen zuweist. Der Kern ist ein essenzieller Teil eines Betriebssystems, aber alleine ist er nutzlos; er kann nur im Kontext eines kompletten Betriebssystems funktionieren. Linux wird normalerweise in Kombination mit dem GNU-Betriebssystem verwendet: Das ganze System ist im Wesentlichen GNU, zu dem Linux hinzugefügt wurde, oder GNU/Linux. All die sogenannten Linux-Distributionen sind tatsächlich GNU/Linux-Distributionen!

    Schaut bei [https://gnu.org](https://www.gnu.org/gnu/incorrect-quotation.html) vorbei, um mehr über Linux und GNU zu erfahren.


<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/linux_user_at_best_buy.png" alt="Befreit Eure Computer"></img>
</div>

<br>

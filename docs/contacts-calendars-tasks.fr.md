---
template: main.html
title: Auto-hébergez vos contacts, calendriers & tâches
description: Auto-hébergez vos services cloud. Comment installer Radicale. Qu'est-ce que Radicale ? Qu'est-ce que CalDAV ou CarDAV ? Qu'est-ce que DAVx5 ? Comment installer Radicale ? Radicale est-il sûr ?
---

# Radicale – Auto-hébergez vos contacts, calendriers & tâches

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/radicale.png" alt="Calendrier Radicale" width="550px"></img>
</div>

<br>

[Radicale](https://radicale.org/) est une solution auto-hébergée pour la gestion de contacts, calendriers et tâches sur votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/). Elle ne se targue pas de proposer un large éventail de fonctionnalités, mais elle excelle dans ce qu'elle fait. Radicale est FOSS et supporte de nombreux clients.

<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Installation de Radicale" width="150px"></img> </center>

## Installez Radicale

Suivez les instructions ci-dessous pour résoudre toutes les dépendances et installer Radicale sur votre serveur.

??? tip "Montrez-moi le guide pas-à-pas"

    ### Prérequis

    Radicale requiert Python pour fonctionner. Connectez-vous au serveur et installez les packages suivants :

    ```bash
    sudo apt install python3 python3-pip
    ```

    ### Installation

    Installez Radicale :

    ```bash
    sudo -H python3 -m pip install --upgrade radicale
    ```

    Ensuite, créez un utilisateur système qui lancera Radicale. Dans le cadre de ce tutoriel, nous appellerons cet utilisateur système `radicaleadmin`. Évidemment, vous pouvez choisir n'importe quel nom, assurez-vous simplement d'ajuster les commandes en conséquence :


    ```bash
    sudo useradd --system --home-dir / --shell /sbin/nologin radicaleadmin
    ```

    Pas besoin d'une base de données, créez simplement un dossier où les données de Radicale seront stockées en fichiers bruts :

    ```bash
    sudo mkdir -p /var/lib/radicale/collections
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/102824be-fd90-4384-ae76-e973fdaa77c5" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Configurer Radicale" width="150px"></img> </center>

## Configurez Radicale

Après avoir installé Radicale avec succès, nous allons configurer un certain nombre de paramètres tels que le support multi-utilisateurs, les permissions, le démarrage automatique, etc. Des instructions plus détaillées sont disponibles ci-dessous.

??? tip "Montrez-moi le guide pas-à pas"

    ### Utilisateurs et utilisatrices

    Radicale supporte des utilisateurs multiples. Chacun.e d'eux peut avoir son propre lot de calendriers, contacts, et tâches. Les accès peuvent être sécurisés par des identifiants utilisateur, qui sont stockés dans un [fichier chiffré](https://httpd.apache.org/docs/current/programs/htpasswd.html) sur le serveur.

    Installez les packages suivants pour activer le chiffrement :

    ```bash
    sudo -H python3 -m pip install --upgrade passlib bcrypt
    ```

    Ensuite, créez un répertoire pour stocker les identifiants utilisateur :

    ```bash
    sudo mkdir /var/www/radicale
    ```

    Enfin, créez un premier compte utilisateur avec la commande ci-dessous, en utilisant le flag `-c`. Remplacez `$USER` par le vrai nom d'utilisateur. Lorsque vous y êtes invité, entrez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) :

    ```bash
    sudo htpasswd -B -c /var/www/radicale/users $USER
    ```

    En guise d'illustration, créons trois utilisateurs — Georg, Lenina et Tom :

    ```bash
    sudo htpasswd -B -c /var/www/radicale/users Georg
    sudo htpasswd -B /var/www/radicale/users Lenina
    sudo htpasswd -B /var/www/radicale/users Tom
    ```

    Notez que nous avons utilisé l'option `-c` pour créer le premier utilisateur Georg, puis nous avons enlevé l'option pour créer les utilisateurs supplémentaires Lenina et Tom.

    Enfin, vérifiez que tous les identifiants utilisateur ont été ajoutés correctement :

    ```bash
    sudo cat /var/www/radicale/users
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    Georg:$2y$05$Zsb8MYlKjRr2InVi/IQNtustqfdnHWN14uG9jNNhx1g rF7a/8Cg6
    Lenina:$2y$05$Hh2Y8nrikm 9f1Pz8LrOFe K5NR HlL0Qz3Yvv gupSBgIr4lT0bi
    Tom:$2y$05$1BnHnXCqNHXZtWMiFhnxBOpYM9xeVnZXm70SoTclEfsrI JPSvDVy
    ```


    ### Paramètres

    Créez un répertoire contenant tous les paramètres :

    ```bash
    sudo mkdir /etc/radicale
    ```

    Ouvrez la configuration :

    ```bash
    sudo vi /etc/radicale/config
    ```

    Ajoutez ou modifiez les lignes suivantes :

    ```bash
    [server]
    hosts               = 0.0.0.0:5232, [::]:5232
    max_connections     = 20
    max_content_length  = 10000000
    timeout             = 10

    [auth]
    type		        = http_x_remote_user
    htpasswd_filename   = /var/www/radicale/users
    htpasswd_encryption = bcrypt
    delay               = 5

    [storage]
    filesystem_folder   = /var/lib/radicale/collections
    ```

    Quelques informations supplémentaires à propos de ces paramètres :
    <center>

    | Paramètre | Description |
    | ------ | ------ |
    | `hosts` | Permet l'accès à distance aux calendriers et contacts. |
    | `max_connections` | Limite le nombre de connexions en parallèle à 20. |
    | `max_content_length` | Limite la taille de fichier à 1 Mo. Important pour les photos de contact lourdes. |
    | `timeout` | Termine les connexions après 10 secondes. |
    | `type` | Permet l’authentification avec le reverse proxy. Radicale utilise le nom d'utilisateur fourni dans l'en-tête HTTP de X-Remote-User. |
    | `htpasswd_filename` | Fournit l'emplacement où les noms d'utilisateur et les mots de passe sont stockés. Pour cette installation, c'est `/var/www/radicale/users`. |
    | `htpasswd_encryption` | Fournit la méthode utilisée pour chiffrer les noms d'utilisateur et mots de passe. Pour cette installation, c'est `bcrypt`. |
    | `delay` | Fixe à 5 secondes le délai après une tentative d'ouverture de session infructueuse, pour éviter les attaques par force brute. |
    | `filesystem_folder` | Fournit l'emplacement où Radicale stocke les données. Pour cette installation, c'est `/var/lib/radicale/collections`. |

    </center>


    ### Permissions

    Nous allons limiter à l'utilisateur `radicaleadmin` l'accès aux répertoires `/var/lib/radicale` (contient les données), `/var/www/radicale` (contient les identifiants utilisateur chiffrés) et `/etc/radicale` (contient les paramètres) :

    ```bash
    sudo chown -R radicaleadmin:radicaleadmin /var/lib/radicale
    sudo chmod -R 700 /var/lib/radicale

    sudo chown -R radicaleadmin:radicaleadmin /var/www/radicale
    sudo chmod -R 755 /var/www/radicale

    sudo chown -R radicaleadmin:radicaleadmin /etc/radicale
    sudo chmod -R 700 /etc/radicale
    ```


    ### Démarrage automatique

    Assurez-vous que Radicale se lance automatiquement à chaque démarrage. Créez un fichier de configuration :

    ```bash
    sudo vi /etc/systemd/system/radicale.service
    ```

    Ajoutez le contenu suivant (ajustez en conséquence) :

    ```bash
    [Unit]
    Description=A simple CalDAV (calendar) and CardDAV (contact) server
    After=network.target
    Requires=network.target

    [Service]
    ExecStart=/usr/bin/env python3 -m radicale
    Restart=on-failure
    User=radicaleadmin
    Group=radicaleadmin
    PrivateTmp=true
    ProtectSystem=strict
    ProtectHome=true
    PrivateDevices=true
    ProtectKernelTunables=true
    ProtectKernelModules=true
    ProtectControlGroups=true
    NoNewPrivileges=true
    ReadWritePaths=/var/lib/radicale/collections

    [Install]
    WantedBy=multi-user.target
    ```

    Vérifions que Radicale se lance automatiquement à chaque fois que le serveur démarre :

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable radicale
    ```

    Redémarrez le serveur :

    ```bash
    sudo reboot
    ```

    Assurez-vous que Radicale est en état de marche (le statut devrait être "Actif") :

    ```bash
    sudo systemctl status radicale
    ```

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/401e2339-44c8-4bea-835e-c7e4b52ce866" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Interface web de Radicale" width="150px"></img> </center>

## Interface web

Nous allons configurer un hôte virtuel Apache comme proxy inverse pour accéder à l'interface web de Radicale. Continuez votre lecture ci-dessous pour plus de détails.

??? tip "Montrez-moi le guide étape par étape"

    Créez un fichier de configuration Apache :

    ```bash
    sudo vi /etc/apache2/sites-available/myradicale.gofoss.duckdns.org.conf
    ```

    Ajoutez le contenu suivant et assurez-vous de régler les paramètres selon votre propre configuration, comme les noms de domaines (`myradicale.gofoss.duckdns.org`), chemins vers les clés SSL, adresses IP et ainsi de suite :

    ```bash
    <VirtualHost *:80>

    ServerName              myradicale.gofoss.duckdns.org
    ServerAlias             www.myradicale.gofoss.duckdns.org
    Redirect permanent /    https://myradicale.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

    ServerName              myradicale.gofoss.duckdns.org
    ServerAlias             www.myradicale.gofoss.duckdns.org
    ServerSignature         Off

    SecRuleEngine           Off
    SSLEngine               On
    SSLProxyEngine          On
    SSLProxyCheckPeerCN     Off
    SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
    SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem

    <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
    </Location>

    RewriteEngine On
    RewriteRule ^/radicale$ /radicale/ [R,L]

    <Location "/radicale/">
        AuthType      Basic
        AuthName      "Radicale - Password Required"
        AuthUserFile  "/var/www/radicale/users"
        Require       valid-user

        ProxyPass        http://localhost:5232/ retry=0
        ProxyPassReverse http://localhost:5232/
        RequestHeader    set X-Script-Name /radicale/
    RequestHeader    set X-Remote-User expr=%{REMOTE_USER}
    </Location>

    ErrorLog ${APACHE_LOG_DIR}/myradicale.gofoss.duckdns.org-error.log
    CustomLog ${APACHE_LOG_DIR}/myradicale.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Une fois que le contenu est ajouté, sauvez et quittez le fichier (`:wq!`).

    Notez comment nous avons activé le chiffrement SSL pour Radicale avec l'instruction `SSLEngine On`, et utilisé le certificat SSL `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` ainsi que la clé privée SSL `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, que nous avions créés précédemment.

    Notez également comment nous avons désactivé ModSecurity dans le fichier de configuration d'Apache avec l'instruction `SecRuleEngine Off`, car Radicale et ModSecurity ne font pas bon ménage.

    Maintenant, activez l'hôte virtuel d'Apache et redémarrez Apache :

    ```bash
    sudo a2ensite myradicale.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configurez Pi-Hole pour résoudre l'adresse locale de Radicale. Naviguez vers `https://mypihole.gofoss.duckdns.org` et connectez-vous à l'interface web de Pi-Hole (ajustez en conséquence). Naviguez vers l'entrée `Local DNS Record` et ajoutez la combinaison domaine/IP suivante (ajustez en conséquence) :

    ```bash
    DOMAIN:      myradicale.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Naviguez jusqu'à [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) et connectez-vous avec des identifiants valides. Créez alors des calendrier, des répertoires et des listes de tâches selon vos besoins.


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    Dans cette vidéo, nous configurons une interface web, nous créons un carnet d'adresses et un calendrier pour Georg, et une liste de tâches pour Lenina.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/11026915-b199-44f1-abd9-916167fc805b" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Clients Radicale" width="150px"></img> </center>

## Clients

Sous Android, [DAVx⁵](https://www.davx5.com/) s'intègre nativement aux applications de calendrier, de contact et de tâches pour les synchroniser avec Radicale. Sous Windows, macOS ou Linux (Ubuntu), Radicale est compatible avec Thunderbird. Plus d'instructions sont données ci-après.

=== "Android"

    ??? tip "Montrez-moi le guide pas-à-pas pour Android"

        Ouvrez F-Droid et installez [DAVx⁵](https://f-droid.org/fr/packages/at.bitfire.davdroid/). Ou alors, vous pouvez trouver l'appli sur [Aurora Store](https://auroraoss.com/) ou [Google's Play Store](https://play.google.com/store/apps/details?id=at.bitfire.davdroid&hl=fr_FR&gl=FR).

        <center>

        | Paramètres | Description |
        | ------ | ------ |
        | Lancer DAVx⁵ | Ouvrir l'application DAVx⁵ et désactiver l'optimisation de la batterie en cas d'invite. |
        | Créer un nouveau compte | Taper sur le signe `+` pour créer un nouveau compte. Sélectionner `Connexion avec une URL et un nom d'utilisateur`. |
        | URL de base | Fournir l'adresse du serveur Radicale. Dans notre exemple, c'est [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/), ajustez en conséquence. |
        | Utilisateur & Mot de passe | Fournir des identifiants utilisateur valides. Dans notre exemple, les utilisateurs valides sont Georg, Lenina ou Tom. Ajustez en conséquence. |
        | Méthode de groupe de contacts | Sur l'écran suivant, sélectionner `Les groupes sont des VCards distincts`. |
        | Créer un compte | Cliquer sur `Créer un compte`. |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide pas-à-pas pour Windows, macOS & Linux (Ubuntu)"

        Reportez-vous au [chapitre précédent sur les clients mail](https://gofoss.net/fr/encrypted-emails/#desktop-clients) pour des instructions détaillées. Une fois installé, ouvrez Thunderbird, naviguez jusqu'à `Outils ‣ Suppléments` et installez l'extension [Cardbook](https://addons.thunderbird.net/fr/thunderbird/addon/cardbook/). Enfin, redémarrez Thunderbird.


<div style="margin-top:-20px">
</div>

??? warning "Utilisateurs et utilisatrices ont besoin d'un accès VPN"

    Tout.e.s les utilisateurs et utilisatrices doivent être connecté.e.s via [VPN](https://gofoss.net/fr/secure-domain/) pour accéder à Radicale.



<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Importez vos contacts dans Radicale" width="150px"></img> </center>

## Importez vos contacts

Vos contacts existants sont probablement dispersés un peu partout. Suivez les instructions ci-dessous pour les importer dans Radicale.

??? tip "Montrez-moi un guide étape par étape"

    <center>

    | Étapes | Description |
    | ------ | ------ |
    | Étape 1 | Allez sur les appareils ou services du cloud où vous stockez vos contacts (Google, Microsoft, Apple, etc.). Exportez-les en format vCard (`.vcf`).|
    | Étape 2 | [Faites une sauvegarde des fichiers `.vcf`](https://gofoss.net/fr/backups/). |
    | Étape 3 | Transférez les fichiers `.vcf` sur votre téléphone Android. |
    | Étape 4 | Ouvrez l'application `Contacts` de votre téléphone. Si vous utilisez une autre application pour gérer vos contacts, les instructions pourront être légèrement différentes. |
    | Étape 5 | Allez à `Paramètres ‣ Importer`. |
    | Étape 6 | Sélectionner `Importer les contacts à partir d'un fichier .vcf`. |
    | Étape 7 | Cherchez l'emplacement des fichiers `.vcf` et sélectionnez-en un. |
    | Étape 8 | Sélectionnez `Sauvegarder les contacts importés dans DAVx⁵` et choisissez le bon répertoire d'adresses dans Radicale. |
    | Étape 9 | Répétez l'opération jusqu'à ce que tous les contacts aient été importés. |

    </center>


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Radicale synchronisez vos contacts" width="150px"></img> </center>

## Synchronisez vos contacts

Lors de la synchronisation de vos contacts, toutes les modifications effectuées sur vos appareils Android, Windows, macOS ou Linux (Ubuntu) seront répliquées sur le serveur Radicale et les autres appareils connectés, et vice versa. Plus d'instructions sont données ci-après.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        | Étape 1 | Sur votre appareil Android, ouvrez l'application DAVx⁵ et sélectionnez un compte. |
        | Étape 2 | Naviguez jusqu'à l'ongle `CardDAV` et sélectionnez le carnet d'adresse de Radicale que vous souhaitez synchroniser avec votre téléphone. |
        | Étape 3 | Cliquez sur le bouton de synchronisation. |
        | Étape 4 | Assurez-vous que tout s'est bien passé. Ouvrez l'application `Contacts` de votre téléphone  et cliquez sur `Paramètres ‣ Comptes`. Le carnet d'adresse de Radicale devrait s'afficher. |

        </center>

=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        | Étape 1 | Connectez-vous dans [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (adaptez en conséquence). |
        | Étape 2 | Naviguez jusqu'au carnet d'adresses que vous souhaitez synchroniser, et copiez l'URL du lien. |
        | Étape 3 | Ouvrez Thunderbird et naviguez jusqu'à `Cardbook ‣ Carnet d'adresses ‣ Nouveau carnet d'adresse`. |
        | Étape 4 | Suivez les instructions de l'assistant : <br>• `Emplacement` : `Distant` <br>• `Type`: `CardDAV`<br>• `URL`:`Collez le lien de l'URL vers le carnet d'adresses que vous avez copié précédemment. `<br>• `Nom d'utilisateur & Mot de passe`: `Saisissez vos identifiants utilisateurs de Radicale`<br>• `Propriétés`: `Fournissez le nom du carnet d'adresse, la couleur, la disponibilité hors ligne, ...`|
        | Étape 5 | Assurez-vous que tout s'est bien passé. Le carnet d'adresse de Radicale devrait s'afficher dans l'onglet `Cardbook`. |

        </center>


<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Radicale importez vos calendriers" width="150px"></img> </center>

## Importez vos calendriers

Vos calendriers existants sont probablement répartis dans différents endroits. Suivez les instructions ci-dessous pour les importer dans Radicale.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Description |
    | ------ | ------ |
    | Étape 1 | Allez sur les appareils ou services du cloud où vous stockez vos calendriers (Google, Microsoft, Apple, etc.). Exportez-les en format iCalendar fomat (`.ics`).|
    | Étape 2 | [Faites une sauvegarde des fichiers `.ics`](https://gofoss.net/fr/backups/). |
    | Étape 3 | Transférez les fichiers `.ics` sur votre téléphone Android. |
    | Étape 4 | Sur votre téléphone, ouvrez F-Droid et installez l'[application d'importation/exportation de calendriers Calendar](https://f-droid.org/fr/packages/org.sufficientlysecure.ical/). Vous pouvez aussi récupérer cette application sur [Google's Play Store](https://play.google.com/store/apps/details?id=org.sufficientlysecure.ical/) |
    | Étape 5 | Ouvrez l'application Calendar et cliquez sur `Importer des fichiers`. |
    | Étape 6 | Cherchez l'emplacement des fichiers `.ics` et sélectionnez-en un. |
    | Étape 7 | Importez-le dans DAVx⁵ et choisissez le bon calendrier Radicale. |
    | Étape 8 | Répétez l'opération jusqu'à ce que tous les calendriers aient été importés. |

    </center>



<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Radicale synchronisez vos calendriers" width="150px"></img> </center>

## Synchronisez vos calendriers

Lors de la synchronisation de vos calendriers, toutes les modifications effectuées sur vos appareils Android, Windows, macOS ou Linux (Ubuntu) seront répliquées sur le serveur Radicale et les autres appareils connectés, et vice versa. Plus d'instructions sont données ci-après.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        | Étape 1 | Sur votre téléphone Android, affichez l'écran principal de DAVx⁵ et choisissez un compte. |
        | Étape 2 | Allez sur l'onglet `CalDAV` et sélectionnez les calendriers Radicale que vous souhaitez synchroniser avec votre téléphone. |
        | Étape 3 | Cliquez sur le bouton de synchronisation. |
        | Étape 4 | Installez et ouvrez l'[application Simple Calendar](https://f-droid.org/fr/packages/com.simplemobiletools.calendar.pro/) sur votre téléphone. Si vous utilisez une autre application pour gérer vos calendriers, les instructions qui suivent pourront être légèrement différentes. |
        | Étape 5 | Cliquez sur `Paramètres` et vérifiez que l'option `CalDAV sync` est bien active. |
        | Étape 6 | Cliquez sur `Gérer les calendriers synchronisés`. Les calendriers Radicale devraient apparaître ici. |
        | Étape 7 | Sélectionnez les calendriers Radicale et cliquez sur `OK`. |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        | Étape 1 | Connectez-vous à [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (ajuster en conséquence). |
        | Étape 2 | Allez au calendrier que vous souhaitez synchroniser et copier son adresse URL. |
        | Étape 3 | Ouvrez Thunderbird et allez sur `Fichier ‣ Nouveau ‣ Agenda`. |
        | Étape 4 | Suivez les instructions d'installation qui s'affichent: <br>• `Créer un nouvel agenda`: `Sur le réseau` <br>• `Format`: `CalDAV` (*ne pas* choisir `iCalendar (ICS)`!)<br>• `Nom d'utilisateur`: `Indiquez un nom d'utilisateur Radicale valide`<br>• `Emplacement`:`Collez le lien vers le calendrier que vous avez copié précédemment`<br>• `Prise en charge du mode hors connexion`: `Cochez la case`<br>• `Personnaliser votre agenda`: `Donnez un nom et une couleur à votre agenda, cochez Afficher les alarmes, associez une adresse de messagerie pour gérer les invitations, etc.`|
        | Étape 5 | Vérifiez que tout s'est bien déroulé. Le calendrier Radicale devrait s'afficher dans l'onglet `Agenda`. |

        </center>



<br>

<center> <img src="../../assets/img/separator_tasks.svg" alt="Radicale synchronisez vos tâches" width="150px"></img> </center>

## Synchronisez vos tâches

Lors de la synchronisation de vos tâches, toutes les modifications effectuées sur vos appareils Android, Windows, macOS ou Linux (Ubuntu) seront répliquées sur le serveur Radicale et les autres appareils connectés, et vice versa. Plus d'instructions sont données ci-après.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        | Étape 1 | Sur votre téléphone Android, affichez l'écran principal de DAVx⁵ et choisissez un compte. |
        | Étape 2 | Allez sur l'onglet `CalDAV` et sélectionnez les listes de tâches Radicale que vous souhaitez synchroniser avec votre téléphone. |
        | Étape 3 | Cliquez sur le bouton de synchronisation. |
        | Étape 4 | Installez et ouvrez l'[application [Tasks.org](https://f-droid.org/fr/packages/org.tasks/) sur votre téléphone. Si vous utilisez une autre application pour gérer vos listes de tâches, les instructions qui suivent pourront être légèrement différentes. |
         | Étape 5 | Cliquez sur `Paramètres` et sélectionnez `Listes affichées`. Les listes de tâches Radicale devraient apparaître ici.|

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

        <center>

        | Étapes | Description |
        | ------ | ------ |
        | Étape 1 | Connectez-vous à [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (ajustez en conséquence). |
        | Étape 2 | Allez à la liste de tâches que vous souhaitez synchroniser et copier son adresse URL. |
        | Étape 3 | Ouvrez Thunderbird et allez sur `Fichier ‣ Nouveau ‣ Agenda`. |
        | Étape 4 | Suivez les instructions d'installation qui s'affichent: <br>• `Créer un nouvel agenda`: `Sur le réseau` <br>• `Format`: `CalDAV` (*ne pas* choisir `iCalendar (ICS)`!)<br>• `Nom d'utilisateur`: `Indiquez un nom d'utilisateur Radicale valide` <br>• `Emplacement`: `Collez le lien vers le calendrier que vous avez copié précédemment`<br>• `Prise en charge du mode hors connexion`: `Cochez la case` <br>• `Personnaliser votre agenda`: `Donnez un nom et une couleur à votre liste de tâches, Cochez Afficher les alarmes. Il n'est pas nécessaire d'ajouter une adresse de messagerie.` |
        | Étape 5 | Vérifiez que tout s'est bien déroulé. La liste de tâches Radicale devrait s'afficher dans l'onglet `Agenda`. |

        </center>



<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Sauvegarde Radicale" width="150px"></img> </center>

## Sauvegarde

[Faites régulièrement des sauvegardes](https://gofoss.net/fr/backups/) pour éviter de perdre vos données ! Radicale est très bien mais n'est pas totalement exempte de toute erreur. Les contacts, calendriers et listes de tâches restent synchronisés tant que vos appareils restent connectés au serveur via le VPN. Il peut parfois y avoir des problèmes de synchronisation, notamment lorsque les modifications ne sont pas correctement sauvegardées ou que les appareils se déconnectent. L'utilisateur peut par ailleurs supprimer des données par inadvertance : dans ce cas, une fois la synchronisation faite entre le serveur et les appareils, les données sont définitivement perdues.

??? tip "Montrez-moi le guide étape par étape"

    Si vous souhaitez faire des sauvegardes manuellement, connectez-vous à [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) (ajustez en conséquence) et cliquez sur les adresses URL des carnets d'adresses, calendriers et listes de tâches pour télécharger vos données.

    Vous pouvez aussi [effectuer des sauvegardes automatiques](https://gofoss.net/fr/server-backups/) des répertoires `/var/lib/radicale`, `/var/www/radicale` et`/etc/radicale`.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Radicale" width="150px"></img> </center>

## Assistance

Pour davantage de précisions, consultez les documentations de [Radicale](https://radicale.org/), [DAVx⁵](https://www.davx5.com/manual/) ou [Thunderbird](https://support.mozilla.org/fr/products/thunderbird).


<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/time_management.png" width="550px" alt="Radicale"></img>
</div>

<br>
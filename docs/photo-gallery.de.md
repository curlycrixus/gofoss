---
template: main.html
title: So hostet Ihr Eure Fotos selbst
description: Cloud-Dienste selbst hosten. Wie installiert man Piwigo? Was ist Piwigo? Piwigo vs Photoprism? Piwigo vs Photoview? Ist Piwigo sicher?
---

# Piwigo, eine selbst gehostete Fotogalerie

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/piwigo.png" alt="Piwigo" width="550px"></img>
</center>

<br>

[Piwigo](https://de.piwigo.org/) ist eine Fotogalerie, die Ihr selbst auf Eurem [Ubuntu-Server](https://gofoss.net/de/ubuntu-server/) hosten könnt. Mit Piwigo könnt Ihr von überall auf Eure Erinnerungen zugreifen, sie organisieren und mit anderen teilen! Piwigo unterstützt verschachtelte Alben, Batch-Bearbeitung, Mehrbenutzer, Tags, Plugins, Themen und vieles mehr.

??? question "Gibt es selbst gehostete Alternativen zu Piwigo?"

    Je nach Euren Bedürfnissen könnt Ihr aus einer Vielzahl von Alternativen wählen:

    <center>

    | |[Piwigo](https://de.piwigo.org/) |[Photoprism](https://photoprism.app/) |[Photoview](https://photoview.github.io/) |[Pigallery2](http://bpatrik.github.io/pigallery2/) |[Lychee](https://lycheeorg.github.io/) |
    | ------ | :------: | :------: | :------: | :------: | :------: |
    |Erstellungsdatum |2002 |2018 |2020 |2017 |2018 |
    |Programmiersprache |PHP |Go |Go |TypeScript |PHP |
    |Mindestanforderungen |Apache oder nginx, PHP |2 Kernprozessoren, 4GB RAM |-- |-- |Apache oder nginx |
    |Datenbank |MySQL, MariaDB |MySQL, MariaDB SQLite |MySQL, Postgres, SQLite |SQL oder ohne Datenbank |MySQL, PostgreSQL oder SQLite |
    |Installation |Direkte Installation |Docker |Docker, Direkte Installation |Docker, Direkte Installation |Docker, Direkte Installation |
    |Benutzeroberfläche |Einfach |Modern |Modern |Einfach |Modern |
    |Geschwindigkeit |Schnell |Schnell |Schnell |Schnell |Schnell |
    |Alben, verschachtelte Alben |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Tags, Labels |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Auto-Tags |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Gesichtserkennung |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> <br>(automatisch) |<span style="color:green">✔</span> <br>(manuell) |<span style="color:red">✗</span> |
    |Massenbearbeitung |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |
    |Erkennung von Duplikaten |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |? |
    |Suchfunktion |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Kalender/Zeitstrahl |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Standorte/Karten |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |
    |Mehrbenutzer |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Berechtigungen |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Teilen |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Kommentare |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Löschen |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |
    |Unterstützung des RAW Formats |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Video-Untestützung |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Responsive-Weboberfläche |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Mobile Apps |Android & iOS, keine Auto-Hochlade-Funktion |Android & iOS, experimentelle Auto-Hochlade-Funktion |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Unterstützung von Ordnerstrukturen^1^ |<span style="color:green">✔</span> |<span style="color:green">✔</span><br> (indexing) |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |
    |WebDAV- und FTP-Unterstützung^2^ |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |? |<span style="color:red">✗</span> |

    </center>

    1. *Das Programm verweist auf eine vorhandene Ordnerstruktur, die Fotos/Videos enthält, ohne dass diese geändert oder kopiert werden müssen. Eine separate Kopie der Dateien ist nicht erforderlich. Die Fotos/Videos bleiben unberührt, wenn das Programm deinstalliert wird.*
    2. *WebDAV- / FTP-Clients können eine direkte Verbindung zu Ordnern mit Fotos/Videos herstellen und diese als Laufwerk einbinden. Dies erlaubt, Fotos/Videos auf Client-Geräten hinzuzufügen, zu entfernen oder zu ändern.*


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Piwigo MySQL" width="150px"></img> </center>

## Vorbereitung der Datenbank

Piwigo kann mit MySQL oder MariaDB betrieben werden. In diesem Tutorial werden wir mit MySQL arbeiten, um die von Piwigo benötigte Datenbank zu generieren. Untenstehend eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Meldet Euch als Root-Benutzer auf dem Server an:

    ```bash
    sudo mysql -u root -p
    ```

    Legt den MySQL-Benutzer `piwigoadmin` an (passt den Benutzernamen entsprechend an). Ersetzt dabei die Zeichenfolge `SicheresPasswort` mit einem [sicheren, individuellen Passwort](https://gofoss.net/de/passwords/):

    ```bash
    CREATE USER 'piwigoadmin'@localhost IDENTIFIED BY 'SicheresPasswort';
    ```

    Erstellt anschließend die von Piwigo benötigte Datenbank und erteilt die richtigen Berechtigungen:

    ```bash
    CREATE DATABASE piwigo;
    GRANT ALL ON piwigo.* TO 'piwigoadmin'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;
    ```

    Meldet Euch erneut bei MySQL als `piwigoadmin` an (passt den Benutzernamen entsprechend an):

    ```bash
    sudo mysql -u piwigoadmin -p
    ```

    Vergewissert Euch, dass die `piwigo`-Datenbank korrekt angelegt wurde:

    ```bash
    SHOW DATABASES;
    ```

    Das Ergebnis sollte in etwa folgendermaßen aussehen:

    ```bash
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | piwigo             |
    +--------------------+
    2 rows in set (0.01 sec)
    ```

    Verlasst MySQL:

    ```bash
    EXIT;
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/fc962cb5-0be4-42bf-bd7c-456dbc095a27" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Piwigo Installation" width="150px"></img> </center>

## Piwigo Installation

Folgt den untenstehenden Anweisungen, um alle [Abhängigkeiten](https://piwigo.org/guides/install/requirements) zu prüfen und Piwigo auf dem Server zu installieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Voraussetzungen

    PHP ist zur Ausführung von Piwigo erforderlich. [Ein vorheriges Kapitel zur Serversicherheit](https://gofoss.net/de/server-hardening-basics/) hat sich bereits mit der Installation und Absicherung von PHP beschäftigt. Dies sollte bereits einige wichtige Voraussetzungen von Piwigo erfüllen, wie z.B. `php8.1-{common,mysql,curl,xmlrpc,gd,mbstring,xml,intl,cli,zip}`.

    Zudem solltet Ihr die folgenden zusätzlichen PHP-Module installieren:

    ```bash
    sudo apt install php8.1-{cgi,soap,ldap,readline,imap,tidy}
    sudo apt install libapache2-mod-php8.1
    ```

    ### Installation

    [Sucht nach der aktuellsten Version des Piwigo-Pakets zum selbst hosten](https://de.piwigo.org/piwigo-bekommen). Zum Zeitpunkt der Abfassung dieses Textes war das Version 12.2.0. Führt die folgenden Befehle aus, um das Paket herunterzuladen und zu entpacken:

    ```bash
    wget http://piwigo.org/download/dlcounter.php?code=latest -O /tmp/piwigo.zip
    sudo unzip /tmp/piwigo.zip 'piwigo/*' -d /var/www
    ```

    Legt die richtigen Berechtigungen fest:

    ```bash
    sudo chown -R www-data:www-data /var/www/piwigo/
    sudo chmod -R 755 /var/www/piwigo/
    sudo ls -al /var/www/
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/65b649cf-9b0b-4448-8823-0162e51fd9f0" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Piwigo Web-Oberfläche" width="150px"></img> </center>

## Web-Oberfläche

Als nächstes richten wir einen Apache Virtual Host als Reverse Proxy ein, um auf die Piwigo-Weboberfläche zuzugreifen. Untenstehend eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Erstellt eine Apache-Konfigurationsdatei:

    ```bash
    sudo vi /etc/apache2/sites-available/myphotos.gofoss.duckdns.org.conf
    ```

    Fügt den folgenden Inhalt hinzu und passt die Einstellungen an Eure eigene Konfiguration an, z. B. den Domain-Namen (`myphotos.gofoss.duckdns.org`), den Pfad zu den SSL-Schlüsseln, die IP-Adressen und so weiter:

    ```bash
    <VirtualHost *:80>

    ServerName              myphotos.gofoss.duckdns.org
    ServerAlias             www.myphotos.gofoss.duckdns.org
    Redirect permanent /    https://myphotos.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

    ServerName              myphotos.gofoss.duckdns.org
    ServerAlias             www.myphotos.gofoss.duckdns.org
    ServerSignature         Off

    SecRuleEngine           Off
    SSLEngine               On
    SSLProxyEngine          On
    SSLProxyCheckPeerCN     Off
    SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
    SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
    DocumentRoot            /var/www/piwigo

    <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
    </Location>

    <Directory /var/www/piwigo/>
        Options +FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/myphotos.gofoss.duckdns.org-error.log
    CustomLog ${APACHE_LOG_DIR}/myphotos.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Speichert und schließt die Datei (`:wq!`).

    Beachtet, dass die SSL-Verschlüsselung für Piwigo mit der Anweisung `SSLEngine On` aktiviert und das zuvor erstellte SSL-Zertifikat `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` sowie der private SSL-Schlüssel `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem` verwendet wurden.

    Beachtet ebenfalls, dass ModSecurity in der Apache-Konfigurationsdatei mit der Anweisung `SecRuleEngine Off` deaktiviert wurde, da Piwigo und ModSecurity nicht gut miteinander harmonieren.
Save visits in history
    Aktiviert als nächstes den Apache Virtual Host und ladet Apache neu:

    ```bash
    sudo a2ensite myphotos.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Konfiguriert Pi-Hole, um die lokale Adresse von Piwigo nutzen zu können. Ruft dazu `https://mypihole.gofoss.duckdns.org` auf und meldet Euch über die Pi-Hole-Weboberfläche an (passt die URL entsprechend an). Öffnet den Menüeintrag `Locale DNS Records` und fügt die folgende Domain/IP-Kombination hinzu (passt wiederum die URL sowie IP entsprechend an):

    ```bash
    DOMAIN:      myphotos.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/252127f0-8931-4a5e-a987-7f6502e55322" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Piwigo Konfiguration" width="150px"></img> </center>

## Konfiguration

Nach der erfolgreichen Installation von Piwigo sind eine Reihe von Einstellungen vorzunehmen: maximale Upload-Dateigröße, Galerietitel, Benutzerregistrierung, usw. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Maximale Fotogröße

    Öffnet die auf dem Server gespeicherte Apache-PHP-Konfigurationsdatei

    ```bash
    sudo vi /etc/php/8.1/apache2/php.ini
    ```

    Ändert die folgenden Parameter oder fügt diese hinzu, um die maximale Upload-Dateigröße auf 20 MB zu erhöhen:

    ```bash
    upload_max_filesize = 20M
    post_max_size = 20M
    ```

    Öffnet die PHP-Konfigurationsdatei der Kommandozeilen-Schnittstelle (auf Englisch, *command line interface* oder CLI):

    ```bash
    sudo vi /etc/php/8.1/cli/php.ini
    ```

    Ändert die folgenden Parameter oder fügt diese hinzu, um die maximale Upload-Dateigröße auf 20 MB zu erhöhen:

    ```bash
    upload_max_filesize = 20M
    post_max_size = 20M
    ```

    ### Nachbereitung

    Ruft [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) auf (passt die URL entsprechend an) und folgt dem Konfigurationsassistenten:

    <center>

    | Eingabefeld | Beschreibung |
    | ------ | ------ |
    | Host | Der Standardwert ist `localhost`. |
    | Benutzer | Gebt den Namen des MySQL-Benutzers an. In unserem Beispiel ist das `piwigoadmin`, passt den Namen entsprechend an. |
    | Passwort | Gebt das Passwort des MySQL-Benutzers an. |
    | Name der Datenbank | Gebt den Namen der MySQL-Datenbank an. In unserem Beispiel ist das `piwigo`, passt den Namen entsprechend an. |
    | Präfix der Datenbanktabellen | Der Standardwert ist der Name der MySQL-Datenbank, gefolgt von einem Unterstrich. In unserem Beispiel ergibt das `piwigo_`, passt den Wert entsprechend an. |
    | Webmaster-Benutzername | Erstellt ein Webmaster-Konto für Piwigo. Im Rahmen dieses Tutorials nennen wir den Webmaster `piwigoadmin@gofoss.net`. Natürlich ist jeglicher andere Name möglich. Achtet nur darauf, dass Ihr die Befehle entsprechend anpasst. |
    | Webmaster-Passwort | Gebt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) für das Webmaster-Konto an. |
    | Webmaster-Email | Gebt eine E-Mail-Adresse für das Webmaster-Konto an. |

    </center>

    Sobald die Konfiguration abgeschlossen ist, klickt auf die Schaltflächen `Start der Installation`, `Galerie ansehen` und `Fotos hinzufügen`. Das war's, Piwigo ist einsatzbereit!

    Ihr könnt noch ein paar grundlegende Einstellungen unter `Konfiguration ‣ Optionen ‣ Allgemein` vornehmen:

    * ändert Titel und Banner der Galerie
    * deaktiviert die Benutzerregistrierung (Häkchen bei `Neuregistrierung von Benutzern erlauben`)
    * verfolgt, wer sich bei Piwigo anmeldet (wählt `Besuche in der Historie speichern` für Gäste, registrierte Besucher und/oder Administratoren)


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/006c7186-00ea-4c0e-b804-6a7b8629f3e0" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Übergangslösung für Ubuntu 22.04"

    Zum Zeitpunkt des Verfassens dieser Zeilen ist Piwigo 12.2.0 nicht vollständig mit PHP 8.1 kompatibel, das mit Ubuntu 22.04 ausgeliefert wird. Aus diesem Grund zeigt die Piwigo-Oberfläche nach der Installation verschiedene `Deprecated` und `Warning`-Meldungen an. Als vorübergehende Abhilfe könnt Ihr diese Meldungen allerdings unterdrücken:

    * Öffnet [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) und meldet Euch als Webmaster `piwigoadmin@gofoss.net` an (passt die URL und den Webmaster-Namen entsprechend an)
    * Ruft den Menüeintrag `Plugins ‣ Liste der Plugins` auf
    * Aktiviert das Plugin `LocalFiles Editor`
    * Ruft den Menüeintrag `Plugins ‣ Aktiviert ‣ LocalFiles Editor ‣ Einstellungen ‣ Lokale Konfiguration` auf
    * Fügt im erscheinenden `local/config/config.inc.php`-Fenster die folgende Zeile ein und klick dann auf `Datei speichern`:

    ```bash
    <?php

    /* The file does not exist until some information is entered
    below. Once information is entered and saved, the file will be created. */

    $conf['show_php_errors'] = E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING;

    ?>
    ```


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Piwigo BenutzerInnen anlegen" width="150px"></img> </center>

## BenutzerInnen anlegen

Piwigo unterscheidet zwischen [drei Benutzertypen](https://piwigo.org/doc/doku.php?id=user_documentation:use:features:user_status/):

* **Webmaster** haben volle Zugriffsrechte auf Piwigo und können Photos, Alben, BenutzerInnen, Gruppen usw. hinzufügen, bearbeiten oder entfernen. Darüber hinaus können Webmaster Plugins und Themen installieren, die Webseite pflegen und aktualisieren usw.
* **Administratoren** haben volle Zugriffsrechte auf Piwigo und können Photos, Alben, BenutzerInnen, Gruppen usw. hinzufügen, bearbeiten oder entfernen.
* **BenutzerInnen** haben nur begrenzte Zugriffsrechte auf Piwigo. Sie können Alben und Fotos ansehen, falls sie über die entsprechenden Berechtigungen verfügen.


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Schritt 1 | Öffnet [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) und meldet Euch als Webmaster `piwigoadmin@gofoss.net` an (passt die URL und den Webmaster-Namen entsprechend an). |
    |Schritt 2 | Ruft den Menüeintrag `Benutzer ‣ Verwaltung ‣ Benutzer hinzufügen` auf. |
    |Schritt 3 | Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) ein. Klickt anschließend auf `Benutzer hinzufügen`. |
    |Schritt 4 | Legt den Status der neuen BenutzerInnen an, z.B. `Benutzer` oder `Administrator`.|

    </center>

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    In diesem Beispiel fügen wir Georg als Administrator hinzu, sowie die Benutzer Lenina, Tom und RandomUser.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ba6374ef-d524-43e7-97eb-d1bc81d6f6cb" frameborder="0" allowfullscreen></iframe>
    </center>

??? warning "Administratoren & BenutzerInnen benötigen einen VPN-Zugang"

    BenutzerInnen müssen über [VPN](https://gofoss.net/de/secure-domain/) mit dem Server verbunden sein, um auf Piwigo zugreifen zu können.


<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Piwigo Fotos hinzufügen" width="150px"></img> </center>

## Fotos hinzufügen

=== "Webformular"

    Zum Hochladen von Fotos über das Webformular benötigt Ihr lediglich einen Browser. Diese Methode eignet sich am besten, wenn Ihr eine überschaubare Anzahl von Bildern oder einzelnen Alben hinzufügen möchtet. Dateien werden hierbei in das Serververzeichnis `/var/www/piwigo/upload` hochgeladen. Beachtet, dass lediglich Administratoren Fotos hinzufügen, bearbeiten oder entfernen können. Untenstehend findet Ihr eine detaillierte Anleitung zum [Hinzufügen von Fotos über das Webformular](https://piwigo.org/doc/doku.php?id=user_documentation:learn:add_picture/).


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Anmeldung | Meldet Euch mit einem Administrator-Konto bei der Piwigo-Weboberfläche an. |
        |Webformular | Ruft den Menüeintrag `Admin ‣ Fotos ‣ Hinzufügen ‣ Webformular` auf. <br><br><center> <img src="../../assets/img/piwigo_web_form.png" alt="Piwigo Webformular" width="300px"></img> </center> |
        |Album erstellen | Erstellt ein neues Album, oder wählt ein bestehendes aus. |
        |Fotos hinzufügen | Klickt auf die Schaltfläche `Fotos hinzufügen`, oder zieht Eure Fotos per Drag & Drop in den angegebenen Bereich. |
        |Hochladen | Klickt auf `Upload starten`. |

        </center>

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        Georg ist ein Administrator. Er lädt acht Fotos über das Webformular hoch:

        * vier Fotos von einem kürzlichen Wanderausflug mit Lenina: `tree.png`, `lake.png`, `moutain.png` und `sunset.png`
        * vier Fotos von einem Arbeitsprojekt: `logo.png`, `georg.png`, `lenina.png` und `tom.png`

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/73abb50e-805b-4619-aebf-5c49644a32e4" frameborder="0" allowfullscreen></iframe>
        </center>


=== "FTP-Client"

    Diese Methode erfordert einen FTP-Client wie z.B. [FileZilla](https://filezilla-project.org/). Sie eignet sich am besten, wenn Ihr große Dateimengen oder ganze Ordnerstrukturen auf einmal hochladen wollt. Dateien müssen in das Serververzeichnis `/var/www/piwigo/galleries` hochgeladen werden. Die ursprüngliche Ordnerstruktur bleibt dabei erhalten. Beachtet, dass lediglich Administratoren Fotos hinzufügen, bearbeiten oder entfernen können. Untenstehend findet Ihr eine detaillierte Anleitung zum [Hinzufügen von Fotos per FTP](https://piwigo.org/doc/doku.php?id=user_documentation:learn:add_picture/).

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Anmeldung | Meldet Euch auf Eurem Ubuntu/Linux-Rechner mit dem `gofossadmin`-Konto an (passt den Konto-Namen entsprechend an). Dies ist das Konto mit SSH-Fernzugriff auf den Server, wie in den Kapiteln [Ubuntu Server](https://gofoss.net/de/ubuntu-server/) und [Server-Sicherheit (Teil 1)](https://gofoss.net/de/server-hardening-basics/) beschrieben. |
        |Vorbereitung | Sortiert nun Eure Fotos auf dem Ubuntu/Linux-Rechner und legt eine Ordnerstruktur Eurer Wahl an. Jeder Ordner wird zu einem Piwigo-Album. Die Tiefe der verschachtelten Ordner ist dabei unbegrenzt. Achtet jedoch auf die Einhaltung der Namenskonventionen: Ordner- und Dateinamen dürfen nur Buchstaben, Zahlen, Bindestriche, Unterstriche oder Punkte enthalten. Leerzeichen oder Sonderzeichen sind nicht erlaubt. |
        |FTP-Installation |Falls Filezilla noch nicht installiert sein sollte, öffnet ein Terminal mit der Tastenkombination `STRG + ALT + T` und führt den Befehl `sudo apt install filezilla` aus. |
        |FTP-Vorbereitung |Öffnet FileZilla und gebt die richtigen Anmeldedaten ein: <br><br> <center> <img src="../../assets/img/filezilla.png" alt="Piwigo Filezilla" height="90px"></img> </center> <br> • `Server`: `sftp://192.168.1.100` (passt die Server-IP-Adresse entsprechend an) <br><br> • `Benutzername`: `gofossadmin` (passt den Benutzernamen mit SSH-Fernzugriff entsprechend an) <br><br> • `Passwort`: `kennwort_zur_erstellung_der_ssh_verbindung_mit_dem_server` <br><br> • `Port`: `2222` (wie im Kapitel [Server-Sicherheit (Teil 1)](https://gofoss.net/de/server-hardening-basics/) festgelegt, passt den Port entsprechend an) |
        |FTP-Verbindung | Noch immer in FileZilla, klickt auf `Verbinden`. Der Rechner sollte Euch erneut nach dem SSH-Kennwort fragen. |
        |Dateiübertragung | FileZilla sollte nun das lokale Dateisystem des Rechners im linken und das Server-Dateisystem im rechten Fenster anzeigen. Kopiert die Ordner mit Euren Fotos in das folgende Server-Verzeichnis: `/var/www/piwigo/galleries`. |
        |Anmeldung | Meldet Euch nun mit einem Administrator-Konto bei der Piwigo-Weboberfläche an. |
        |Simulation | Ruft den Menü-Eintrag `Admin ‣ Werkzeuge ‣ Synchronisieren` auf. Wählt die folgenden Simulations-Einstellungen aus, bevor Ihr mit der eigentlichen Synchronisierung beginnt (passt die Berechtigungen unter `Wer soll diese Fotos sehen können?` je nach Euren Bedürfnissen an): <br><br> <center> <img src="../../assets/img/synchronise.png" alt="Piwigo-Synchronisierung" width="300px"></img> </center> <br> Klickt abschließend auf `Absenden`. Piwigo sollte die Anzahl der neuen Alben sowie Fotos anzeigen, die der Datenbank hinzugefügt oder aus ihr entfernt werden. Piwigo sollte Euch auch warten, falls Fehler zu erwarten sind. Ihr könnt fortfahren soweit alles in Ordnung ist. |
        |Synchronisierung | Noch immer unter `Admin ‣ Werkzeuge ‣ Synchronisieren`, deaktiviert den Eintrag `Nur Simulation durchführen`:<br><br> <center> <img src="../../assets/img/synchronise_2.png" alt="Piwigo-Synchronisierung" width="300px"></img> </center> <br> Klickt abschließend auf `Absenden`. Dies kann je nach Datenmenge eine Weile dauern. Setzt einen Kaffee auf!|

        </center>

    ??? warning "Ein paar Ratschläge"

        Verzeichnisse und Dateien sollten nicht mehr verschoben werden, sobald sie auf den Server hochgeladen wurden. Andernfalls gehen Euch bei der nächsten Synchronisierung sämtliche zugehörigen Daten verloren (z.B. Kommentare, Bewertungen, usw.).


=== "Drittanbieter-Apps"

    Diese Methode erfordert Anwendungen von Drittanbietern wie [digiKam](https://www.digikam.org/), [Shotwell](https://shotwell-project.org/doc/html/), [Lightroom](https://lightroom.adobe.com/) oder Piwigos [Android-](https://f-droid.org/de/packages/org.piwigo.android/) und [iOS](https://apps.apple.com/de/app/piwigo/id472225196)-Apps. Sie eignet sich am besten, wenn Ihr große Mengen unstrukturierter Fotos hinzufügen wollt. Dateien werden dabei in das Serververzeichnis `/var/www/piwigo/upload` hochgeladen. Beachtet, dass lediglich Administratoren Fotos hinzufügen, bearbeiten oder entfernen können. Untenstehend findet Ihr eine detaillierte Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        Anweisungen zum Hinzufügen von Fotos findet Ihr in der Dokumentation der jeweiligen Drittanbieter-Anwendungen. Für die meisten unter ihnen sind Anmeldedaten erforderlich, um eine Verbindung mit dem Server herzustellen:

        <center>

        | Einstellungen | Beschreibung |
        | ------ | ------ |
        |Server| Gebt die Adresse der Fotogalerie an. In unserem Beispiel ist das [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/), passt die URL entsprechend an. |
        |Benutzer | Gebt den Namen eines Piwigo-Administrators oder -Benutzers an. |
        |Passwort | Gebt das Passwort des obigen Piwigo-Administrators oder -Benutzers an. |

        </center>

        <center><img align="center" src="../../assets/img/piwigo_app_2.jpg" alt="Piwigo Handy-App" width="150px"></img>
        </center>



<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Piwigo Fotos bearbeiten" width="150px"></img> </center>

## Fotos bearbeiten

=== "Einzelne Fotos bearbeiten"

    Einzelne Fotos können bearbeitet werden, um deren Titel, Autor, Erstellungsdatum, Alben, Schlüsselwörter, Beschreibung, Datenschutz, Standort usw. zu ändern. Ihr könnt auch den aussagekräftigsten Bereich eines Fotos festlegen. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        Meldet Euch als Administrator an und ruft den Menü-Eintrag `Fotos ‣ Stapelverarbeitung ‣ Übersicht` auf. Wendet einen oder mehrere Filter an, um das zu bearbeitende Foto zu finden (mehr zu Filterattributen weiter unten). Bewegt den Mauszeiger über das Foto und klickt auf `Foto bearbeiten`.

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        Georg ändert die Autorenangaben des Fotos `logo.png`.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/90ef6f40-27bd-4d8d-aa3c-234d056b396a" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Mehrere Fotos bearbeiten"

    Die Bearbeitung mehrerer Fotos auf einmal wird auch als Stapelverarbeitung bezeichnet. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        Meldet Euch als Administrator an und ruft den Menü-Eintrag `Fotos ‣ Stapelverarbeitung ‣ Übersicht` auf. Wendet einen oder mehrere Filter an, um die zu bearbeitenden Fotos zu finden (mehr zu Filterattributen weiter unten). Ihr könnt alle Fotos auf einmal aus- oder abwählen, indem Ihr auf `Alles` oder `Nichts` klickt. Legt schließlich fest, welche Aktion durchgeführt werden soll: löschen, einem oder mehreren Alben zuordnen, in ein Album verschieben oder von einem Album trennen, Schlüsselwörter, Autor, Titel, Erstellungsdatum oder Geotags festlegen und so weiter.

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        Georg erstellt Bilder in verschiedenen Größen für die Fotos seiner Wandertour.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/6407514b-20e2-4d55-bba4-52e4ed5fa703" frameborder="0" allowfullscreen></iframe>
        </center>


<div style="    margin-top: -20px;">
</div>

??? info "Hier erfahrt Ihr mehr über Filterattribute"

    <center>

    | Filterattribute | Beschreibung |
    | ----- | ----- |
    |Voreingestellte Filter | Filtert alle Fotos, alle Videos, doppelte Fotos, zuletzt importierte Fotos, mit Geotags versehene Fotos, Fotos ohne zugehöriges Album (Waisen), Fotos ohne Schlagwörter, Lieblingsfotos und so weiter. |
    |Album | Filtert Fotos aus einem bestimmten Album. |
    |Schlagwörter | Fotos nach Schlüsselwörtern filtern. |
    |Datenschutzstufe | Filtert Fotos, die für Alle, für Kontakte, für Freunde, für Familie oder für Administratoren sichtbar sind. |
    |Abmessungen | Fotos nach ihren Abmessungen filtern. |
    |Dateigröße | Fotos nach ihrer Dateigröße filtern. |
    |Suchen | Filtert Fotos anhand von erweiterten Suchergebnissen: Titel, Tag, Dateiname, Autor, Erstellungsdatum, Veröffentlichung, Breite, Höhe, Dateigröße, Verhältnis und so weiter. |

    </center>


<br>

<center> <img src="../../assets/img/separator_simplefilemanager.svg" alt="Piwigo Alben verwalten" width="150px"></img> </center>

## Alben verwalten


Über Piwigos Weboberfläche könnt Ihr neue Alben hinzufügen oder bestehende bearbeiten: Album-Name und Beschreibung festlegen, übergeordnetes Album definieren, Album sperren (nur noch für Administratoren sichtbar), Fotos hinzufügen,  Unteralben verwalten, automatische oder manuelle Darstellungsreihenfolge festlegen und so weiter. Ihr könnt Alben ebenfalls veröffentlichen, so dass sie für all diejenigen mit dem richtigen Link sichtbar sind. Oder aber Alben als privat kennzeichnen, so dass sie nur für angemeldete BenutzerInnen mit den entsprechenden Rechten sichtbar sind. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Meldet Euch als Administrator an und ruft den Menü-Eintrag `Admin ‣ Alben ‣ Verwaltung ‣ Liste` auf. Klickt auf `Album hinzufügen`, um ein neues Album anzulegen. Oder bewegt die Maus über ein bestehendes Album und klickt auf `Bearbeiten`. Ihr könnt nun die Eigenschaften des Albums, die Sortierreihenfolge oder die Berechtigungen ändern.

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    In diesem Beispiel fügt der Administrator Georg dem Album *Holidays* eine Beschreibung hinzu.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/eda37f42-f7bd-463b-96c6-109de612e88e" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="Piwigo Berechtigungen verwalten" width="150px"></img> </center>

## Berechtigungen verwalten

Piwigo verfügt über zwei Systeme zur Verwaltung von Zugriffsberechtigungen. Diese können miteinander kombiniert oder unabhängig voneinander verwendet werden:

* **Benutzer-/Gruppenberechtigungen** gelten für Alben, BenutzerInnen und Gruppen
* **Datenschutzstufen** gelten für Fotos und BenutzerInnen

Dieses recht komplexe Berechtigungssystem eröffnet Möglichkeiten zur Feinabstimmung der Zugriffsrechte für mehrere Benutzer. Falls Ihr Piwigo nicht mit anderen teilt, haltet es einfach: kennzeichnet all Eure Alben als privat und beschränkt den Zugriff auf Euch selbst.


=== "Benutzer-/Gruppenberechtigungen"

    Beginnen wir mit den Berechtigungen auf Album-Ebene. Standardmäßig sind Alben `öffentlich` und können von allen BenutzerInnen aufgerufen werden. Administratoren können Alben als `privat` kennzeichnen und den Zugriff auf bestimmte BenutzerInnen und/oder Gruppen beschränken. Untenstehend findet Ihr weitere Anweisungen zur [Verwaltung von Benutzer-/Gruppenberechtigungen](https://piwigo.org/doc/doku.php?id=user_documentation:permissions_management/).

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        Um ein Album `privat` zu machen:

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 | Meldet Euch als Administrator an. |
        |Schritt 2 | Ruft den Menü-Eintrag `Alben ‣ Verwaltung ‣ Liste ‣ Bearbeiten ‣ Zugriffsrechte` auf. |
        |Schritt 3 | Stellt den Zugang von `öffentlich` auf `privat` um. |

        Um den Zugriff auf `private` Alben auf bestimmte `BenutzerInnen` zu beschränken:

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 | Meldet Euch als Administrator an. |
        |Schritt 2 | Ruft entweder den Menü-Eintrag `Alben ‣ Verwaltung ‣ Liste ‣ Bearbeiten ‣ Zugriffsrechte` auf und tragt BenutzerInnen, die das private Album aufrufen können, in das Feld ` Zugriff für Benutzer erlaubt` ein.<br><br>  Oder ruft den Menü-Eintrag `Benutzer ‣ Verwalten ‣ Benutzer bearbeiten ‣ Zugriffsrechte` auf und legt fest, welche privaten Alben von den BenutzerInnen aufgerufen werden können (`Erlaubt`) oder nicht (`Nicht erlaubt`). |

        Zugriffsrechte können auch für eine `Gruppe` von BenutzerInnen festgelegt werden. Dies erleichtert die Verwaltung von Berechtigungen für mehrere Benutzer. Um den Zugriff auf `private` Alben auf bestimmte `Gruppen` zu beschränken:


        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Schritt 1 | Erstellt Gruppen, indem Ihr den Menü-Eintrag `Benutzer ‣ Gruppen ‣ Gruppe hinzufügen` aufruft. |
        |Schritt 2 | Fügt BenutzerInnen zu Gruppen hinzu, indem Ihr den Menü-Eintrag `Benutzer ‣ Verwaltung ‣ Benutzer bearbeiten ‣ Gruppen` aufruft. |
        |Schritt 3 | Legt schließlich Zugriffsrechte für ganze Gruppen fest, indem Ihr den Menü-Eintrag `Benutzer ‣ Gruppen ‣ Zugriffsrechte` aufruft. |


=== "Datenschutzstufen"

    Datenschutzstufen werden pro Foto und pro BenutzerIn definiert. Damit lässt sich genau festlegen, welche BenutzerInnen auf welche Fotos zugreifen können. Im Folgenden findet Ihr weitere Details zur [Verwaltung der Datenschutzstufen](https://piwigo.org/doc/doku.php?id=user_documentation:permissions_management/).

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        Piwigo verwaltet Berechtigungen mit fünf `Datenschutzstufen`. Das funktioniert so:

        * jedes Foto hat eine Datenschutzstufe
        * jeder Benutzer hat eine Datenschutzstufe
        * eine Benutzerin muss über eine Datenschutzstufe verfügen, die größer oder gleich der des Fotos ist, das sie ansehen möchte. Oder anders ausgedrückt: Je höher die Datenschutzstufe eines Nutzers ist, desto mehr Fotos kann er sehen

        Die Datenschutzstufe von `Fotos` kann festgelegt werden, indem Ihr Euch als Administrator anmeldet und den Menü-Eintrag `Fotos ‣ Stapelverwaltung ‣ Übersicht` auswählt. Wendet bei Bedarf Filter an und wählt die entsprechenden Fotos aus. Wählt die Aktion `Wer soll diese Fotos sehen können?` und legt die gewünschte `Datenschutzstufe` fest:

        <center>

        | Datenschutzstufe | Beschreibung |
        | :------: | ------ |
        |1 | Jeder |
        |2 | Administratoren, Familie, Freunde, Kontakte |
        |3 | Administratoren, Familie, Freunde |
        |4 | Administratoren, Familie |
        |5 | Administratoren |

        </center>

        Die Datenschutzstufe von `BenutzerInnen` kann festgelegt werden, indem Ihr Euch als Administrator anmeldet und den Menü-Eintrag `Benutzer ‣ Verwaltung ‣ Benutzer bearbeiten ‣ Datenschutzstufe` aufruft:

        <center>

        | Datenschutzstufe | Beschreibung |
        | :------: | ------ |
        |1 | --- |
        |2 | Kontakte |
        |3 | Freunde |
        |4 | Familie |
        |5 | Administratoren |

        </center>


=== "Beispiel"

    Lasst uns mit einem konkreten Beispiel abschließen, in dem alle bisher erläuterten Konzepte angewendet werden. Beachtet, dass das selbe Ergebnis auch auf andere Weise hätte erreicht werden können. Piwigo ist sehr flexibel, wählt Euren bevorzugten Ansatz.

    ??? tip "Hier geht's zum Beispiel"

        <center>
        <img align="center" src="../../assets/img/piwigo_permissions.png" alt="Piwigo Zugriffsrechte" width="700px"></img>
        </center>

        <center>

        | Kreis | Beschreibung |
        | ------ | ------ |
        |1 | Georgs Benutzerstatus ist `Administrator`. Das bedeutet, dass er Fotos, Alben, BenutzerInnen, Gruppen und so weiter hinzufügen, bearbeiten und löschen kann. Georgs Datenschutzstufe ist auf `Administratoren` eingestellt. Georg hat somit Zugriff auf alle Alben und Fotos. |
        |2 | Georg hat vier Fotos von einem kürzlichen Wanderausflug in das Album *Holidays* hochgeladen: `tree.png`, `lake.png`, `moutain.png` und `sunset.png`. Er möchte diese Fotos mit niemandem außer Lenina teilen. Lenina hat den Status `Benutzer`: Sie kann daher lediglich `öffentliche` Alben oder Dateien, für die sie die richtigen Zugriffsrechte erhält, einsehen. Georg definiert daher das Album *Holidays* als `privat` und beschränkt den Zugriff auf eine Gruppe namens `Hiking`, der nur er und Lenina angehören. |
        |3 | Georg hat ebenfalls vier Bilder in das Album *Gofoss* hochgeladen: `logo.png`, `georg.png`, `lenina.png` und `tom.png`. Da es sich um ein `öffentliches` Album handelt, können alle auf die Bilder zugreifen, d.h. auch auf `logo.png`. |
        |4 | Allerdings hat Georg die Bilder`georg.png`, `lenina.png` und `tom.png` auf die Datenschutzstufe `Familie` gesetzt. Damit haben lediglich BenutzerInnen mit der Datenschutzstufe `Familie` oder höher (`Administratoren`) Zugriff auf diese Bilder. In diesem Beispiel wären das Georg (`Administratoren`), Lenina (`Familie`) und Tom (`Familie`). RandomUser hat keinen Zugriff auf diese Bilder, das seine Datenschutzstufe `Freunde` ist. |

        </center>


    ??? tip "Hier geht's zum 4-minütigen Zusammenfassungsvideo"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/fe8fdbb7-c18a-407a-b3a7-689026d38eaf" frameborder="0" allowfullscreen></iframe>
        </center>



<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Piwigo add plugins and themes" width="150px"></img> </center>

## Plugins und Themen hinzufügen

=== "Plugins"

    Piwigos Funktionalitäten können mit über [350 Plugins](https://piwigo.org/ext/) erweitert werden. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Plugins anzeigen | Um alle installierten Plugins anzuzeigen müsst Ihr Euch als Webmaster `piwigoadmin@gofoss.net` anmelden (passt den Webmaster-Namen entsprechend an) und den Menü-Eintrag `Admin ‣ Plugins ‣ Liste der Plugins` aufrufen:<br><br><center> <img src="../../assets/img/piwigo_plugins.png" alt="Piwigo Plugins" width="300px"></img></center><br>• `Aktivierte Plugins` sind installiert und laufen derzeit <br><br>• `Deaktivierte Plugins` sind installiert, aber derzeit inaktiv <br><br>• Beachtet, dass beim Deaktivieren eines Plugins die meisten Einstellungen gespeichert bleiben, während beim Löschen eines Plugins alle Einstellungen (inklusive Dateien, Konfigurationen, usw.) gelöscht werden |
        |Plugins aktualisieren | Ruft den Eintrag `Admin ‣ Plugins ‣ Auf Aktualisierungen überprüfen` auf. |
        |Plugins hinzufügen | Ruft den Eintrag `Admin ‣ Plugins ‣ Weitere Plugins` auf. Sucht nach einem Plugin und klickt auf `Installieren`. Ruft den Eintrag `Admin ‣ Plugins ‣ Liste der Plugins` auf und aktiviert das soeben installierte Plugin. |

        </center>

        Hier ein paar beliebte Plugins:

        <center>

        | Plugin | Beschreibung |
        | ------ | ------ |
        | [Piwigo-Videojs](https://piwigo.org/ext/extension_view.php?eid=610/) | Unterstützt Videos in Piwigo: mp4, m4v, ogg, ogv, webm, webmv, usw. Weitere Infos findet Ihr auf der [Wiki-Seite](https://github.com/Piwigo/piwigo-videojs/wiki). |
        | [Fotorama](https://piwigo.org/ext/extension_view.php?eid=727/) | Vollbild-Diashow. |
        | [Batch downloader](https://piwigo.org/ext/extension_view.php?eid=616/) | Ladet eine Fotoauswahl als Zip-Datei herunter. |
        | [Piwigo-Openstreetmap](https://piwigo.org/ext/extension_view.php?eid=701/) | Geolokalisiert Eure Bilder. Weitere Infos findet Ihr auf der [Wiki-Seite](https://github.com/Piwigo/piwigo-openstreetmap/wiki). |
        | [Grum Plugin Classes](https://piwigo.org/ext/extension_view.php?eid=199/) | Erforderlich für die Ausführung einiger anderer Plugins. |
        | [AStat](https://piwigo.org/ext/extension_view.php?eid=172/) | Erweitert die von Piwigo erstellten Statistiken, z.B. welche Seiten oder Fotos wie lange besucht wurden, von welcher IP-Adresse aus, usw. |
        | [EXIF view](https://piwigo.org/ext/extension_view.php?eid=155/) | Fügt EXIF-Metadaten zu Euren Fotos hinzu. |

        </center>

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b52c041b-c4b6-46cd-9e5f-741c3c382cdc" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Themen"

    Das Layout von Piwigo kann mit über [140 Themen](https://piwigo.org/ext/) angepasst werden. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        Meldet Euch als Webmaster `piwigoadmin@gofoss.net` an (passt den Webmaster-Namen entsprechend an) und ruft den Menü-Eintrag `Admin ‣ Konfiguration ‣ Themen` auf. Ladet Themen Eurer Wahl herunter, und aktiviert oder konfiguriert diese. Ein paar beliebte Themen:

        <center>

        | Themen | Beschreibung |
        | ------ | ------ |
        | [Modus](https://piwigo.com/blog/2019/06/25/new-modus-theme-on-piwigo-com/) | Standardthema, wird in verschiedenen Farbtönen geliefert. |
        | [Bootstrap Darkroom](https://piwigo.com/blog/2020/03/12/bootstrap-darkroom-new-theme-piwigo-com/) | Ein weiteres modernes, funktionsreiches und mobilfreundliches Thema. |
        | [SimpleNG](https://piwigo.org/ext/extension_view.php?eid=602/) | An Bootstrap angelehntes, mobilfreundliches Thema. |

        </center>

    ??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a14ba8f8-6ee9-497a-81e3-86418c261eec" frameborder="0" allowfullscreen></iframe>
        </center>



<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Piwigo Upgrade" width="150px"></img> </center>

## Upgrade

Das Upgraden von Piwigo ist [ziemlich einfach](https://piwigo.org/guides/update/automatic). Befolgt einfach die unten stehenden Anweisungen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Beginnt damit, [eine Sicherungskopie Eurer Fotos](https://gofoss.net/de/backups/) anzulegen, falls während des Upgrades etwas schief laufen sollte. Die Sicherungskopie kann entweder mit Hilfe einer FTP-Verbindung (FileZilla) zum Server erstellt werden, wie zuvor beschrieben. Sichert hierzu die Verzeichnisse `/var/www/galleries` und `/var/www/upload`.

    Ansonsten könnt Ihr ebenfalls Piwigos Datenbank mit [MySQL dump](https://linuxize.com/post/how-to-back-up-and-restore-mysql-databases-with-mysqldump/) oder einem [Server Backup](https://gofoss.net/de/server-backups/) sichern. Die MySQL-Datenbank von Piwigo wird normalerweise in `/var/lib/mysql` gespeichert.

    Meldet Euch schließlich als Webmaster `piwigoadmin@gofoss.net` an (passt den Webmaster-Namen entsprechend an) und ruft den Eintrag `Admin ‣ Werkzeuge ‣ Aktualisierungen ‣ Piwigo-Aktualisierung` auf. Klickt auf `Aktualisierung zu Piwigo xx.x.x`` und bestätigt Eure Wahl.

??? tip "Hier geht's zum 30-sekündigen Zusammenfassungsvideo"

    <center> <img src="../../assets/img/piwigo_upgrade.gif" alt="Piwigo Upgrade" width="600px"></img> </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Piwigo Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr in der [Piwigo Dokumentation](https://piwigo.org/doc/doku.php). Ihr könnt auch gerne die [Piwigo-Gemeinschaft](https://piwigo.org/forum/) um Unterstützung bitten.

<br>
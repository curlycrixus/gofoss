---
template: main.html
title: Comment chiffrer vos fichiers
description: Chiffrement des données. Est-ce que VeraCrypt est sûr ? Comment utiliser VeraCrypt ? Est-ce que Cryptomator est sûr ? Comment utiliser Cryptomator ? Cryptomator vs VeraCrypt ?
---

# Chiffrez vos données avec VeraCrypt & Cryptomator

!!! level "Dernière mise à jour: mars 2022. Destiné aux utilisateurs expérimentés. Certaines compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/cryptomator_veracrypt.png" alt="Chiffrement de fichier" width="400px"></img>
</div>

Qu'est-ce que le chiffrement de fichiers ? Il s'agit d'un moyen de rendre vos données et [sauvegardes](https://gofoss.net/fr/backups/) illisibles, même en cas d'intrusion. Il existe un large choix de logiciels gratuits pour chiffrer vos fichiers. Dans ce chapitre, nous nous pencherons sur VeraCrypt et Cryptomator, qui comptent parmi les outils de chiffrement les plus sûrs. VeraCrypt est particulièrement adapté au chiffrement de fichiers dans des dossiers volumineux (appelés « conteneurs ») stockés sur un périphérique local, sans révéler d'informations sur la structure, le nombre ou la taille des fichiers. Cryptomator, quant à lui, chiffre chaque fichier individuellement, ce qui le rend plus adapté au stockage en nuage. Cryptomator révèle toutefois des méta-informations non chiffrées, telles que l'horodatage, le nombre ou la taille des fichiers.

<br>

<center> <img src="../../assets/img/separator_veracrypt.svg" alt="VeraCrypt" width="150px"></img> </center>

## VeraCrypt

[VeraCrypt](https://www.veracrypt.fr/en/) est un programme libre et [open source](https://veracrypt.fr/code/) permettant de chiffrer des fichiers ou des systèmes de fichiers entiers. C'est le successeur de [TrueCrypt](https://fr.wikipedia.org/wiki/TrueCrypt), et a été publié pour la première fois en 2013. Vous trouverez des instructions d'installation ci-dessous (au moment de la rédaction de ces lignes, aucune version Android de VeraCrypt n'a été publiée).

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        Téléchargez la dernière version de l'[installateur VeraCrypt pour Windows](https://www.veracrypt.fr/en/Downloads.html) et suivez les instructions à l'écran.


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        MacOS a besoin de l'application compagnon OSXFUSE pour exécuter VeraCrypt. [Téléchargez la dernière version de l'installateur](https://github.com/osxfuse/osxfuse/releases/), ouvrez le fichier `.dmg` téléchargé et suivez les instructions à l'écran. Assurez-vous de sélectionner la couche de compatibilité `MacFUSE`.

        Téléchargez ensuite la dernière version de l'[installateur VeraCrypt pour macOS](https://www.veracrypt.fr/en/Downloads.html) et suivez les instructions à l'écran. Pour un accès facile, ouvrez le dossier Applications et faites glisser l'icône VeraCrypt vers le dock.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        Téléchargez la dernière version de l'[installateur VeraCrypt générique pour Linux](https://www.veracrypt.fr/en/Downloads.html). Le fichier devrait s'appeler `veracrypt-X.XX-UpdateX-setup.tar.bz2`. Supposons que le fichier ait été téléchargé dans le dossier `/home/gofoss/Downloads` et doive être installé dans le dossier `/home/gofoss/veracrypt`. Ouvrez un terminal et exécutez les commandes suivantes (assurez-vous d'ajuster ces chemins de fichiers en fonction de votre propre configuration) :

        ```bash
        mkdir /home/gofoss/veracrypt
        cd /home/gofoss/Downloads
        tar -xvf veracrypt-X.XX-UpdateX-setup.tar.bz2 -C /home/gofoss/veracrypt
        ```

        Lancez le programme d'installation et suivez les instructions à l'écran :

        ```bash
        cd /home/gofoss/veracrypt
        ./veracrypt-1.XX-UpdateX-setup-gui-x64
        ```

        Pour désinstaller VeraCrypt, exécutez le script `/usr/bin/veracrypt-uninstall.sh`.


### Chiffrez vos fichiers avec VeraCrypt

Une fois VeraCrypt installé, vous êtes en mesure de créer des conteneurs chiffrés et de dissimuler vos fichiers. Notez que l'interface en français n'est disponible que sous Windows. Les utilisateurs de Linux et de macOS doivent actuellement travailler avec l'interface en anglais.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Instructions |
    | ------ | ------ |
    | Créer un volume |Lancez VeraCrypt et cliquez sur `Créer un volume ‣ Créer un fichier conteneur chiffré ‣ Suivant`. |
    | Type de volume |Sélectionnez `Volume VeraCrypt standard ‣ Suivant`. |
    | Emplacement du volume chiffré |Cliquez sur `Fichier...`, puis spécifiez le chemin vers l'emplacement où le conteneur VeraCrypt doit être stocké. Cliquez sur `Sauvegarder ‣ Suivant`|
    | Option de chiffrement |Choisissez `AES` comme algorithme de chiffrement, et `SHA-512` comme algorithme de hachage, puis cliquez sur `Suivant`. <br><br>*Remarque*: VeraCrypt prend en charge un certain nombre d'algorithmes de chiffrement, notamment AES, Serpent, Twofish, Camellia et Kuznyechik, ainsi que différentes combinaisons d'algorithmes en cascade. VeraCrypt prend également en charge quatre fonctions de hachage, dont SHA-512, Whirlpool, SHA-256 et Streebog. |
    | Taille du volume chiffré |Spécifiez la taille voulue de votre conteneur VeraCrypt. Puis cliquez sur `Suivant`. |
    | Mot de passe du volume chiffré |Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour votre conteneur VeraCrypt. C'est la clé de vos données. <br><br>*Remarque*: VeraCrypt propose des options supplémentaires, telles que l'activation de fichiers de clés ou d'un multiplicateur d'itérations personnelles (PIM). |
    | Options de format |Sélectionnez un système de fichiers, par exemple `FAT`. Puis, cliquez sur `Suivant`. |
    | Formater le volume chiffré |Déplacez votre souris de manière aussi aléatoire que possible pendant au moins 30 secondes, idéalement jusqu'à ce que la barre de l'indicateur atteigne le maximum. Cliquez ensuite sur `Formater` pour créer le conteneur. En fonction de sa taille et de la puissance de calcul disponible, cela peut prendre BEAUCOUP de temps. Prenez une tasse de café ou allez déjeuner. Et ne débranchez surtout pas votre appareil... |
    | Terminé ! |Une fois le volume créé, cliquez sur `OK ‣ Quitter`. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/6d68b947-67ee-402b-bdcd-9d292f792f57" frameborder="0" allowfullscreen></iframe>

    </center>


### Accédez à vos fichiers chiffrés avec VeraCrypt

Les fichiers chiffrés par VeraCrypt ne sont accessibles qu'avec le bon mot de passe.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Instructions |
    | ------ | ------ |
    | Sélectionnez un emplacement de lecteur |Ouvrez l'application VeraCrypt et sélectionnez un emplacement de lecteur sur lequel le conteneur VeraCrypt sera monté. |
    | Monter le conteneur |Cliquez sur le bouton `Fichier...` et naviguez jusqu'à votre conteneur chiffré. Puis cliquez sur `Ouvrir ‣ Monter`. |
    | Saisissez le mot de passe |Saisissez le mot de passe pour le conteneur VeraCrypt et cliquez sur `OK`. Ensuite, fournissez votre mot de passe administrateur. |
    | Ouvrez le conteneur |Le conteneur sera monté en tant que disque virtuel, qui se comporte comme n'importe quel lecteur de votre ordinateur. Naviguez sur le disque virtuel et créez, ouvrez, modifiez, enregistrez, copiez ou supprimez des fichiers. |
    | Démonter le conteneur |Une fois que vous avez terminé, fermez le conteneur en cliquant sur `Démonter` dans la fenêtre principale de VeraCrypt. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/1eeb5668-4800-4326-9da1-1b20d8a53edb" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_cryptomator.svg" alt="Cryptomator" width="150px"></img> </center>

## Cryptomator

[Cryptomator](https://cryptomator.org/) est un logiciel [open source](https://github.com/cryptomator/) qui chiffre vos données avant de les téléverser sur le nuage. Les données chiffrées ne sont accessibles qu'avec le bon mot de passe. Cela réduit le risque que les fournisseurs de services en nuage ou tout autre tiers accèdent à vos données en ligne. Vous trouverez des instructions d'installation ci-dessous.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        Téléchargez le dernier [installateur Cryptomator pour Windows](https://cryptomator.org/downloads/win/thanks/) et suivez les instructions à l'écran.


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        Téléchargez le dernier [installateur Cryptomator pour macOS](https://cryptomator.org/downloads/mac/thanks/), ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Cryptomator sur le dossier d'Applications. Pour un accès facile, ouvrez le dossier d'Applications et faites glisser l'icône Cryptomator vers le dock.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        Ouvrez un terminal, ajoutez le dépôt correct et installez Cryptomator :

        ```bash
        sudo add-apt-repository ppa:sebastian-stenzel/cryptomator
        sudo apt update
        sudo apt install cryptomator -y
        ```

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        Achetez et téléchargez Cryptomator depuis le [Google Play Store](https://play.google.com/store/apps/details?id=org.cryptomator&hl=fr/), depuis [le site web de Cryptomator](https://cryptomator.org/android/) ou depuis [Aurora Store](https://auroraoss.com/).


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        Achetez et téléchargez Cryptomator depuis l'[App Store](https://apps.apple.com/fr/app/cryptomator/id953086535/).


### Chiffrez vos fichiers avec Cryptomator

=== "Clients de bureau"

    Une fois Cryptomator installé sur votre environnement de bureau, vous êtes en mesure de créer des dossiers chiffrés (appelés « coffres ») qui dissimulent vos fichiers en nuage.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Instructions |
        | ------ | ------ |
        | Créez un nouveau coffre |Lancez Cryptomator et sélectionnez `+ Ajouter un coffre ‣ Créer un nouveau coffre`. |
        | Choisissez un nom |Donnez un nom à votre coffre, puis cliquez sur `Suivant`. |
        | Choisissez un emplacement de stockage |Choisissez l'emplacement où stocker les fichiers chiffrés. Il doit s'agir d'un répertoire synchronisé avec le nuage. Cliquez sur `Ouvrir ‣ Suivant`. |
        | Choisissez un mot de passe |Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). C'est la clé de vos données. |
        | Créez une clé de secours |Facultatif : vous pouvez créer une clé de secours. Cette clé doit être sauvegardée en toute sécurité, car elle sera nécessaire si vous perdez votre mot de passe. |
        | Créez le coffre |Cliquez sur `Créer un coffre`. |

        </center>

        *Remarque*: Vous pouvez également ajouter un coffre existant au client de bureau. Pour cela, lancez Cryptomator, sélectionnez `+ Ajouter un coffre ‣ Ouvrir un coffre existant`, naviguez vers le dossier contenant le coffre et sélectionnez le fichier `masterkey.cryptomator`.

    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        <center>

        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/dd51ffbc-d347-4319-8543-a63e21fd3023" frameborder="0" allowfullscreen></iframe>

        </center>


=== "Clients mobiles"

    Une fois Cryptomator installé sur votre appareil mobile, vous êtes en mesure de créer des dossiers chiffrés (appelés « coffres ») qui dissimulent vos fichiers en nuage.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Instructions |
        | ------ | ------ |
        | Créez un nouveau coffre |Lancez Cryptomator et tapez sur `+ ‣ Ajouter un coffre`. |
        | Choisissez un fournisseur de nuage |Sélectionnez le fournisseur de nuage auprès duquel vous souhaitez stocker vos fichiers chiffrés. Si ce n'est pas déjà fait, fournissez les informations d'identification de votre fournisseur de stockage en nuage. |
        | Choisissez un nom |Donnez un nom à votre coffre, puis cliquez sur `Suivant`. |
        | Choisissez un emplacement de stockage |Choisissez l'emplacement où stocker les fichiers chiffrés. Il doit s'agir d'un répertoire synchronisé avec le nuage. Cliquez sur `Placer ici`. |
        | Choisissez un mot de passe |Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). C'est la clé de vos données. Cliquez sur `Créer`. |

        </center>

        *Remarque*: Vous pouvez également ajouter un coffre existant au client mobile. Pour cela, lancez Cryptomator, sélectionnez `+ ‣ Ajouter un coffre existant`, sélectionnez le fournisseur de stockage en nuage, naviguez vers le dossier contenant le coffre et sélectionnez le fichier `masterkey.cryptomator`.


### Accédez à vos fichiers chiffrés avec Cryptomator

=== "Clients de bureau"

    Les fichiers chiffrés par Cryptomator ne sont accessibles qu'avec le bon mot de passe.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Instructions |
        | ------ | ------ |
        | Déverrouillez le coffre |Ouvrez Cryptomator, sélectionnez un coffre existant et cliquez sur `Déverrouiller...`. Saisissez votre mot de passe et cliquez sur `Déverrouiller`. |
        | Monter le coffre |Cliquez sur `Révéler le coffre`. Le coffre sera monté et se comportera comme n'importe quel disque de votre ordinateur. Créez, ouvrez, modifiez, enregistrez, copiez ou supprimez des fichiers. |
        | Verrouillez le coffre |Une fois que vous avez terminé, fermez le coffre en cliquant sur `Verrouiller` dans la fenêtre principale de Cryptomator. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/e7937c30-f60c-40f1-abdb-798400e5cbcb" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Clients mobiles"

    Les fichiers chiffrés par Cryptomator ne sont accessibles qu'avec le bon mot de passe.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Étapes | Instructions |
        | ------ | ------ |
        | Déverrouillez le coffre |Ouvrez l'application Cryptomator, sélectionnez un coffre existant, saisissez votre mot de passe et cliquez sur `Déverrouiller...`. |
        | Verrouillez le coffre |Créez, ouvrez, modifiez, enregistrez, copiez ou supprimez des fichiers. Une fois que vous avez terminé, fermez le coffre en cliquant sur `Verrouiller` dans la liste des coffres. |

        </center>

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Cryptomator et Veracrypt support" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions :

* référez-vous à la [documentation de VeraCrypt](https://veracrypt.fr/en/Documentation.html) ou demandez de l'aide à la [communauté VeraCrypt](https://sourceforge.net/p/veracrypt/discussion/).

* référez-vous à la [documentation de Cryptomator](https://docs.cryptomator.org/en/latest/) ou demandez de l'aide à la [communauté Cryptomator](https://community.cryptomator.org/).


<br>

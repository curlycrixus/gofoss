---
template: main.html
title: What is FOSS? | 7 Steps To Cyber Security And Privacy
description: Want to find out about online privacy? Check out our free privacy guide to the best open source privacytools!
---

# Protect Your Online Privacy <br> With Free & Open Source Software

!!! level "Last updated: March 2022"

[gofoss.net](https://gofoss.net/) wants to make free and open source (FOSS) privacy tools accessible to all. This includes people with limited time or no inclination to tinker, as well as experts keen to learn more about cyber security & privacy.

This guide is 100% free and open source – no ads, no tracking, no sponsored content. It should be of interest to those of you who:

* own an iPhone, Android device or a computer running Windows, macOS or ChromeOS
* browse the web with Chrome, Safari or Edge
* socialise on Facebook, Twitter, WhatsApp, Tiktok or Snapchat

<center><img src="../../assets/img/closed_ecosystem.png" alt="Online privacy tools" width="500px"></img></center>

## What is Surveillance Capitalism?

Did you know that all companies behind the above mentioned services are part of the so-called [Surveillance Capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism)? In a nutshell, this term describes an economic system centred around technology monopolies which harvest private user data to maximize profit.

Don't see any issues with that? Well, Surveillance Capitalism actually threatens the very core of our societies. It gives rise to mass surveillance, polarizes the political debate, interferes with electoral processes, drives uniformity of thought, facilitates censorship and promotes planned obsolescence. Tackling those issues requires comprehensive changes to our legal systems and social conventions.


<center><img src="../../assets/img/foss_ecosystem.png" alt="What is FOSS?" width="500px"></img></center>

## How to be safe online with FOSS?

Meanwhile, there's a lot you can do to own your data, protect your privacy online and claim your right to repair. For example, start using free and open source software, also referred to as [FOSS](https://fsfe.org/freesoftware/freesoftware.en.html). This empowers you to browse, chat and work online without anyone collecting, monetising or censoring your data. Or forcing you to buy new devices every other year.

There is no one-click solution, switching to free software is a process. This online privacy guide takes one step at a time, starting with the basics before diving into more advanced stuff. It systematically favours free and open source software which excels at one specific job over proprietary all-in-one bloatware. Take a look under the hood and adopt, reject or customise any privacytools that fit your needs.

<center>

| Steps | Description |
| ------ | ------ |
|[1. Safely browse the Internet](https://gofoss.net/intro-browse-freely/) |• Switch to [Firefox](https://gofoss.net/firefox/) or [Tor](https://gofoss.net/tor/) <br>• Use a [VPN](https://gofoss.net/vpn/) against online security threats |
|[2. Keep your conversations private](https://gofoss.net/intro-speak-freely/) |• Use [encrypted messengers](https://gofoss.net/encrypted-messages/) <br>• Use [encrypted emails](https://gofoss.net/encrypted-emails/) |
|[3. Protect your data](https://gofoss.net/intro-store-safely/) |• Choose [secure passwords & two-factor authentication](https://gofoss.net/passwords/) <br>• Create a [3-2-1 backup strategy](https://gofoss.net/backups/) <br>• [Encrypt](https://gofoss.net/encrypted-files/) your sensitive data |
|[4. Stay mobile & free](https://gofoss.net/intro-free-your-phone/) |• Use [free & open source mobile apps](https://gofoss.net/foss-apps/) <br>• Free your phone from Google & Apple with [CalyxOS](https://gofoss.net/calyxos/) or [LineageOS](https://gofoss.net/lineageos/) |
|[5. Unlock your computer's potential](https://gofoss.net/intro-free-your-computer/) |• Switch to [Linux](https://gofoss.net/ubuntu/) <br>• Favour [free & open source apps](https://gofoss.net/ubuntu-apps/) on your computer |
|[6. Own your cloud](https://gofoss.net/intro-free-your-cloud/) |• Join the [Fediverse](https://gofoss.net/fediverse/), leave commercial social media behind <br>• Use [alternative cloud providers](https://gofoss.net/cloud-providers/) <br>• Set up your [own private cloud](https://gofoss.net/ubuntu-server/) <br>• Secure your [server](https://gofoss.net/server-hardening-basics/) & [cloud services](https://gofoss.net/secure-domain/) <br>• Access a blazing fast [cloud storage](https://gofoss.net/cloud-storage/) <br>• Organise and share your [photo collection](https://gofoss.net/photo-gallery/) <br>• Manage your [contacts, calendars & tasks](https://gofoss.net/contacts-calendars-tasks/) <br>• Stream your [movies, TV shows & music](https://gofoss.net/media-streaming/) <br>• [Backup your server](https://gofoss.net/server-backups/) |
|[7. Learn, connect & get involved](https://gofoss.net/nothing-to-hide/) |There is much to be said and learned about online privacy and secuity, data exploitation, filter bubbles, surveillance, censorship and planned obsolescence. Learn more about the project, get involved and spread the word. |

</center>

<br>
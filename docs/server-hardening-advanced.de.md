---
template: main.html
title: Server-Sicherheit (Teil 2)
description: Selbst hosten (Teil 2). Sichert Euren Server ab. Was ist eine Firewall? Was ist NTP? Linux Schadprogramme erkennen. ClamAV. Was ist ein Kernel-Dump? Was sind Kernel-Module?
---

# Server-Sicherheit (Teil 2)

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

!!! warning "Warnhinweis"

    **Kein System ist sicher**. Dies ist der [zweite Teil zum Thema Serversicherheit](https://gofoss.net/de/server-hardening-basics/). Es legt zwar fortgeschrittenere Maßnahmen zum Schutz Eures [Ubuntu-Servers](https://gofoss.net/de/ubuntu-server/) vor unmittelbaren Bedrohungen dar. Aber geschickte Hacker oder eine Organisation mit ausreichenden Ressourcen können vermutlich trotzdem einen Weg in Euer System finden.


## Sudo-Privilegien

Privilegierter Zugang zu Eurem Server sollte wie nachfolgend beschrieben auf BenutzerInnen einer bestimmten Gruppe beschränkt werden.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Erstellt eine Gruppe namens `sudousers` und fügt die Benutzer `gofossroot` und `gofossadmin` hinzu (passt die Benutzernamen entsprechend an):

    ```bash
    sudo groupadd sudousers
    sudo usermod -a -G sudousers gofossroot
    sudo usermod -a -G sudousers gofossadmin
    ```

    Legt eine Sicherungskopie der `sudoers`-Konfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --archive /etc/sudoers /etc/sudoers-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/sudoers
    ```

    Fügt die folgende Zeile ein, falls nicht bereits vorhanden:

    ```bash
    %sudousers   ALL=(ALL:ALL) ALL
    ```

    Speichert und schließt die Datei (`:wq!`). Beschränkt schließlich mit dem folgenden Befehl den Zugang zu privilegierten Rechten auf die `sudousers`-Gruppe:

    ```bash
    sudo dpkg-statoverride --update --add root sudousers 4750 /bin/su
    ```

    Überprüft die Berechtigungen mit dem folgenden Befehl, die Zeichenfolge `sudousers` sollte angezeigt werden:

    ```bash
    ls -lh /bin/su
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/102e7dac-ef23-4ac4-a4ca-e179dbabddd7" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_userinfo.svg" alt="Geschützte Prozesse" width="150px"></img> </center>

## Geschützte Prozesse

Das `/proc`-Verzeichnis enthält unter Umständen schützenswerte Prozessinformationen. Standardmäßig können alle BenutzerInnen auf dieses Verzeichnis zugreifen. Mit den nachstehenden Anweisungen könnt Ihr Euren Server weiter absichern und vertrauliche Daten vor unbefugten Nutzern oder Programmen verbergen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Legt eine Sicherungskopie der `fstab`-Konfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --preserve /etc/fstab /etc/fstab-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/fstab
    ```

    Fügt folgende Zeile am Ende der Datei ein, um Prozessinformationen auszublenden:

    ```bash
    proc    /proc   proc    defaults,hidepid=2  0   0
    ```

    Speichert und schließt die Datei (`:wq!`). Hängt schließlich das `/proc`-Verzeichnis mit folgendem Befehl erneut ein:

    ```bash
    sudo mount -o remount,hidepid=2 /proc
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252b3f-93b0-4f64-a016-af4c436f60e7" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Passwortsicherheit" width="150px"></img> </center>

## Passwortsicherheit

Standardmäßig können BenutzerInnen jedes beliebige Passwort wählen. Hier erfahrt Ihr mehr darüber, wie Ihr sicherere Passwörter erzwingen bzw. Qualitätsanforderungen definieren könnt:

* wie lange ein Passwort maximal verwendet werden darf
* nach wie vielen Tagen ein Passwort frühestens abgeändert werden darf
* die Mindestlänge eines Passworts
* die Anzahl der zu verwendenden Groß- und Kleinbuchstaben sowie Ziffern

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Kontoparameter konfigurieren

    Legt eine Sicherungskopie der `login.defs`-Konfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --preserve /etc/login.defs /etc/login.defs-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/login.defs
    ```

    Ändert bzw. ergänzt die folgenden Parameter:

    ```bash
    PASS_MAX_DAYS   365
    PASS_MIN_DAYS   1
    PASS_WARN_AGE   30
    SHA_CRYPT_MIN_ROUNDS 1000000
    SHA_CRYPT_MAX_ROUNDS 1000000
    ```

    Speichert und schließt die Datei (`:wq!`).

    Dieselben Einstellungen sollten auch auf die Administratoren `gofossroot` und `gofossadmin` angewendet werden, die vor der Verschärfung der Passwortregeln erstellt wurden (passt die Benutzernamen entsprechend an):

    ```bash
    sudo chage -m 1 -M 365 -W 30 gofossroot
    sudo chage -m 1 -M 365 -W 30 gofossadmin
    ```

    Vergewissert Euch, dass alle Änderungen korrekt übernommen wurden:

    ```bash
    sudo chage -l gofossroot
    sudo chage -l gofossadmin
    ```

    Ein paar Zusatzinformationen zu diesen Einstellungen:

    <center>

    | Einstellungen | Beschreibung |
    | ------ | ------ |
    | `PASS_MAX_DAYS` |Maximale Anzahl von Tagen, die ein Passwort verwendet werden darf. |
    | `PASS_MIN_DAYS` |Mindestanzahl von Tagen zwischen Passwortänderungen. |
    | `PASS_WARN_AGE` |Anzahl der Tage vor Ablauf eines Passworts, in denen eine Warnung ausgegeben wird. |
    | `SHA_CRYPT_MIN_ROUNDS & SHA_CRYPT_MAX_ROUNDS` |Standardmäßig speichert Linux das Administratorkennwort mit einem einzelnen, gesicherten SHA-512-Hash. Nutzt mehrere Hash-Runden, um das Kennwort besser zu schützen. |

    </center>


    ### PAM konfigurieren

    [Linux Pluggable Authentication Modules (PAM)](https://en.wikipedia.org/wiki/Linux_PAM) ist ein umfangreiches Benutzerauthentifizierung-Verwaltungssystem. Das folgende PAM-Modul ermöglicht die Überprüfung der Passwortstärke:

    ```bash
    sudo apt install libpam-cracklib
    ```

    Legt eine Sicherungskopie der `pam.d`-Konfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --archive /etc/pam.d/common-password /etc/pam.d/common-password-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/pam.d/common-password
    ```

    Ändert bzw. ergänzt die folgenden Parameter:

    ```bash
    password requisite pam_cracklib.so retry=3 minlen=12 ucredit=-1 lcredit=-1 dcredit=-1 difok=3 gecoschec
    ```

    Fügt in folgender Zeile die Optionen `shadow` sowie eine erhöhte Anzahl von `rounds` hinzu:

    ```bash
    password        [success=1 default=ignore]      pam_unix.so obscure use_authtok try_first_pass sha512 shadow rounds=1000000
    ```

    Speichert und schließt die Datei (`:wq!`).

    Ein paar Zusatzinformationen zu diesen Einstellungen:

    <center>

    | Einstellung | Beschreibung |
    | ------ | ------ |
    | `retry = 3` |BenutzerInnen werden 3 Mal gefragt, bevor eine Fehlermeldung angezeigt wird. |
    | `minlen = 10` |Mindestlänge des Passworts. |
    | `ucredit = -1` |Das Passwort muss mindestens einen Großbuchstaben enthalten. |
    | `lcredit = -1` |Das Passwort muss mindestens einen Kleinbuchstaben enthalten. |
    | `dcredit = -1` |Das Passwort muss mindestens eine Ziffer enthalten. |
    | `difok = 3` |Mindestens 3 Zeichen des neuen Passworts dürfen nicht im alten Passwort verwendet worden sein. |
    | `gecoschec` |Passwörter, die das Benutzerkonto beinhalten, sind nicht zulässig. |
    | `shadow rounds=1000000` |Standardmäßig speichert das `pam_unix`-Modul das Administratorkennwort mit einem einzelnen, gesicherten SHA-512-Hash. Nutzt mehrere Hash-Runden, um das Kennwort besser zu schützen. |

    </center>


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a893ced7-b4a8-4e68-83a8-8dc195c2150c" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Netzwerksicherheit" width="150px"></img> </center>

## Netzwerksicherheit

Der Systemverwaltungsbefehl `sysctl` ermöglicht eine Feinabstimmung des Kernels und kann zur Verbesserung der Netzwerksicherheit verwendet werden. Mehr Details zum Thema findet Ihr im folgenden.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Legt eine Sicherungskopie der `sysctl`-Konfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --archive /etc/sysctl.conf /etc/sysctl.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/sysctl.conf
    ```

    Löscht deren Inhalt durch Eingabe der Zeichenfolge `:%d`. Gebt anschließend die folgenden Zeilen ein bzw. kopiert/fügt diese in die Konfigurationsdatei ein:

    ```bash
    # IP Spoofing protection
    net.ipv4.conf.all.rp_filter = 1
    net.ipv4.conf.default.rp_filter = 1

    # Ignore ICMP broadcast requests
    net.ipv4.icmp_echo_ignore_broadcasts = 1

    # Disable source packet routing
    net.ipv4.conf.all.accept_source_route = 0
    net.ipv6.conf.all.accept_source_route = 0
    net.ipv4.conf.default.accept_source_route = 0
    net.ipv6.conf.default.accept_source_route = 0

    # Block SYN attacks
    net.ipv4.tcp_syncookies = 1
    net.ipv4.tcp_max_syn_backlog = 2048
    net.ipv4.tcp_synack_retries = 2
    net.ipv4.tcp_syn_retries = 5

    # Log Martians
    net.ipv4.conf.all.log_martians = 1
    net.ipv4.icmp_ignore_bogus_error_responses = 1

    # Ignore ICMP redirects
    net.ipv4.conf.all.send_redirects = 0
    net.ipv4.conf.default.send_redirects = 0
    net.ipv4.conf.all.accept_redirects = 0
    net.ipv6.conf.all.accept_redirects = 0
    net.ipv4.conf.default.accept_redirects = 0
    net.ipv6.conf.default.accept_redirects = 0

    # Ignore Directed pings
    net.ipv4.icmp_echo_ignore_all = 1
    ```

    Speichert und schließt die Datei (`:wq!`). Ladet schließlich die neuen Einstellungen:

    ```bash
    sudo sysctl -p
    ```

    Ein paar Zusatzinformationen zu diesen Sicherheitseinstellungen:

    <center>

    |Einstellung |Beschreibung |
    | ------ | ------ |
    | `Enable IP spoofing protection` |Schützt Euch vor [IP-Spoofing](https://de.wikipedia.org/wiki/IP-Spoofing), bei dem Angreifer ihre IP-Adresse verschleiern, typischerweise bei [Denial of Service (DoS)](https://de.wikipedia.org/wiki/Denial_of_Service) Angriffen. |
    | `Ignore ICMP broadcast requests` |Vermeidet, dass Euer Server an [Smurf-Angriffen](https://de.wikipedia.org/wiki/Smurf-Angriff) teilnimmt. |
    | `Ignore ICMP redirects` |Schützt Euch vor gefälschten ICMP-Umleitungspaketen, die von Angreifern verwendet werden, um einen [Denial of Service (DoS)](https://de.wikipedia.org/wiki/Denial_of_Service) Angriff auszuführen. |
    | `Disable source packet routing` |Schützt Euch vor quell-geleiteten Paketen, die von Angreifern zur Verschleierung ihrer Identität und ihres Standorts verwendet werden |
    | `Block SYN attacks` |Schützt Euch vor SYN-Angriffen, einer Form von [Denial of Service (DoS)](https://de.wikipedia.org/wiki/Denial_of_Service) Angriffen. |
    | `Log martians` |Schützt Euch vor sogenannten Mars-Paketen. Dies sind gefälschte Pakete mit einer offensichtlich falschen Quelladresse. Sie können bei [Denial of Service (DoS)](https://de.wikipedia.org/wiki/Smurf-Angriff) Angriffen zum Einsatz kommen. |
    | `Ignore directed pings` |Der Ping-Befehl kann verwendet werden, um den Verbindungsstatus mit Eurem Server zu überprüfen. Er kann aber auch zur Durchführung von Angriffen missbraucht werden. |

    </center>

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/06be3f02-f2c2-4998-b355-64d5fcb9c608" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="Schutz vor Schadprogrammen und Viren" width="150px"></img> </center>

## Schutz vor Schadprogrammen und Viren

Linux Malware Detect ([LMD](https://www.rfxn.com/projects/linux-malware-detect/)) und [ClamAV](https://www.clamav.net/) sind quelloffene Antivirus- und Schadprogramm-Lösungen. Folgt den nachstehenden Anweisungen, um sie auf Eurem Server zu installieren und zu konfigurieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Schadprogramm-Scanner (LMD)

    LMD kann mit den folgenden Befehlen installiert werden:

    ```bash
    sudo apt install wget
    sudo wget http://www.rfxn.com/downloads/maldetect-current.tar.gz
    sudo tar -xvf maldetect-current.tar.gz
    cd maldetect-1.6.4
    sudo ./install.sh
    ```

    Legt eine Sicherungskopie der `maldet`-Konfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --archive /usr/local/maldetect/conf.maldet /usr/local/maldetect/conf.maldet-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /usr/local/maldetect/conf.maldet
    ```

    Ändert bzw. ergänzt die folgenden Parameter (passt den Administrator-Namen `gofossadmin` entsprechend an):

    ```bash

    #Sendet Ergebnisse der Schadprogramm-Prüfung an den lokalen Systemadministrator gofossadmin (entsprechend anpassen) in /var/mail/gofossadmin

    email_alert="1"
    email_addr="gofossadmin@localhost"

    #In Quarantäne verschieben und Alarm schlagen, wenn Schadprogramme entdeckt werden
    quarantine_hits="1"

    #Saubere, auf Zeichenketten basierende Schadprogramm-Injektionen werden nicht automatisch gelöscht (vermeidet das Löschen von Dateien im Falle von Fehlalarmen)
    quarantine_clean="0"

    #Ermöglicht die Sperrung von Nutzern, die von Schadprogrammen betroffen sind
    quarantine_suspend_user="1"

    #Nutzt ClamAVs Suchmaschine für schnellere Ergebnisse
    scan_clamscan="1"

    #Root-Dateien durchsuchen
    scan_ignore_root="0"
    ```

    Speichert und schließt die Datei (`:wq!`). Bringt LMD auf den neuesten Stand und ladet die aktuellsten Schadprogramm-Definitionen herunter:

    ```bash
    sudo maldet --update-ver
    sudo maldet --update
    ```


    ### Antivirus (ClamAV)

    ClamAV kann mit den folgenden Befehlen installiert werden:

    ```bash
    sudo apt install clamav
    ```

    Vergewissert Euch, ob das Programm erfolgreich installiert wurde:

    ```bash
    clamscan --version
    ```

    Aktualisiert die Virensignaturdatenbank:

    ```bash
    sudo systemctl stop clamav-freshclam
    sudo freshclam
    sudo systemctl start clamav-freshclam
    ```

    Ermöglicht ClamAV nach jedem Neustart automatisch zu laden:

    ```bash
    sudo systemctl enable clamav-freshclam
    ```


    ### LMD und ClamAV testen

    Lasst uns den Schadprogramm-Scanner und das Antivirus-Programm testen. Dazu verwenden wir Schadprogramm-Attrappen,  die von der Europäischen Expertengruppe für IT-Sicherheit ([EICAR](https://www.eicar.org/?page_id=3950/)) zur Verfügung gestellt werden. Ladet die Dateien wie folgt herunter:

    ```bash
    cd /var/www/html
    sudo wget http://www.eicar.org/download/eicar.com
    sudo wget http://www.eicar.org/download/eicar.com.txt
    sudo wget http://www.eicar.org/download/eicar_com.zip
    sudo wget http://www.eicar.org/download/eicarcom2.zip
    ```

    Verwendet ClamAV, um das Verzeichnis `/var/www/html` und alle Unterverzeichnisse rekursiv nach Viren zu durchsuchen:

    ```bash
    sudo clamscan --recursive --infected /var/www/html
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    /var/www/html/eicar_com.zip: Win.Test.EICAR_HDB-1 FOUND
    /var/www/html/eicar.com: Win.Test.EICAR_HDB-1 FOUND
    /var/www/html/eicarcom2.zip: Win.Test.EICAR_HDB-1 FOUND
    /var/www/html/eicar.com.txt: Win.Test.EICAR_HDB-1 FOUND

    ----------- SCAN SUMMARY -----------
    Known viruses: 6880642
    Engine version: 0.102.2
    Scanned directories: 1
    Scanned files: 5
    Infected files: 4
    Data scanned: 0.01 MB
    Data read: 0.01 MB (ratio 1.00:1)
    Time: 54.632 sec (0 m 54 s)
    ```

    Untersucht anschließend das gleiche Verzeichnis mit LMD auf Schadprogramme:

    ```bash
    sudo maldet --scan-all /var/www/html
    ```

    Die Ausgabe sollte eine Scan-ID enthalten, z.B. `200510-1954.2127`. Der detaillierte Bericht kann mit dem folgenden Befehl eingesehen werden (passt die Scan-ID entsprechend an):

    ```bash
    sudo maldet --report 200510-1954.2127
    ```

    Eine Liste aller Scanberichte kann mit folgendem Befehl aufgerufen werden:

    ```bash
    sudo maldet -e list
    ```

    Entfernt schließlich alle unter Quarantäne gestellten Dateien:

    ```bash
    sudo ls /usr/local/maldetect/quarantine/
    sudo rm -rf /usr/local/maldetect/quarantine/*
    ```


    ### LMD-Scans planen

    Der Schadprogramm-Scanner LMD ist so konfiguriert, dass er täglich einen Cron-Job ausführt (`/etc/cron.daily/maldet`). Dieser Cron-Job aktualisiert die Schadprogramm-Datenbank und untersucht alle Heim-Verzeichnisse. Dies sollte für die meisten Anwendungsfälle ausreichend sein. Deaktiviert die Echtzeitüberwachung, um die Serverlast gering zu halten und zu vermeiden, dass alle 10 Minuten Scans laufen.

    ```bash
    sudo rm /etc/cron.d/maldet_pub
    sudo maldet -k
    ```


    ### ClamAV-Scans planen

    Die Virensignaturdatenbank wird durch den `freshclam`-Dienst aktualisiert. Standardmäßig geschieht dies stündlich, Ihr könnt die Häufigkeit in der Datei `/etc/clamav/freshclam.conf` ändern und den Dienst mit `sudo service clamav-freshclam restart` neu starten.

    Damit Antiviren-Scans geplant werden können, muss erst einmal eine leere Protokolldatei angelegt:

    ```bash
    sudo touch /var/log/clamav/clamscan.log
    ```

    Erstellt nun einen Cron-Job, um jeden Tag nach Viren zu suchen:

    ```bash
    sudo vi /etc/cron.daily/clamav
    ```

    Fügt die folgenden Zeilen in die Datei ein:

    ```bash
    #!/bin/sh

    MYLOG=/var/log/clamav/clamscan.log
    echo "Scanning for viruses at `date`" >> $MYLOG
    clamscan --recursive --infected --max-filesize=100M --max-scansize=100M --exclude=/boot / >> $MYLOG 2>&1
    ```

    Speichert und schließt die Datei (`:wq!`). Macht schließlich das Skript ausführbar:

    ```bash
    sudo chmod +x /etc/cron.daily/clamav
    ```

    Folgende Optionen zur Ausführung von `clamscan`-Skript können an Eure eigenen Bedürfnisse angepasst werden:

    <center>

    |Einstellungen |Beschreibung |
    | ------ | ------ |
    | `--recursive` | Verzeichnisse und Unterverzeichnisse werden rekursiv auf Viren geprüft. |
    | `--infected` | Nur infizierte Dateien werden in den Scanbericht aufgenommen, nicht alle gescannten Dateien. |
    | `--max-filesize=100M`, `--max-scansize=100M` | Dateien von bis zu 100 MB werden gescannt. Standardmäßig scannt ClamAV keine Dateien, die größer als 20 MB sind. |
    | `--exclude=/boot` | Im obigen Beispiel haben wir das Verzeichnis `/boot` von der Überprüfung ausgeschlossen. Dies kann an Eure eigenen Bedürfnisse angepasst werden. Die Option kann auch mehrfach verwendet werden, um mehrere Verzeichnisse auszuschließen. |
    | `/` | Dies ist das Verzeichnis, das ClamAV nach Viren durchsuchen soll. Im obigen Beispiel haben wir das gesamte Stammverzeichnis `/` gescannt. Das Zielverzeichnis könnt Ihr natürlich nach Euren eigenen Bedürfnissen anpassen. |

    </center>

    Testet den Cron-Job. Habt ein wenig Geduld, je nach Größe der Zielverzeichnisse kann der Scan eine Weile dauern:

    ```bash
    sudo bash /etc/cron.daily/clamav
    sudo cat /var/log/clamav/clamscan.log
    ```

    Die Terminalausgabe sollte in etwa so aussehen:

    ```bash
    ----------- SCAN SUMMARY -----------
    Infected files: 0
    Time: 54.052 sec (0 m 54 s)
    Start Date: 2021:05:22 21:05:29
    End Date:   2021:05:22 21:06:23
    ```


??? tip "Hier geht's zum 4-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a9b938db-0d32-4c07-8520-40fb98b07e52" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Berechtigungen" width="150px"></img> </center>

## Datei- und Verzeichnisberechtigungen

Nachfolgend wird erklärt, wie Ihr unbefugten Zugriff auf System-Dateien einschränkt.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"´

    ```bash
    sudo chmod 600 /etc/crontab
    sudo chmod 600 /etc/ssh/sshd_config
    sudo chmod 700 /etc/cron.d
    sudo chmod 700 /etc/cron.daily
    sudo chmod 700 /etc/cron.hourly
    sudo chmod 700 /etc/cron.weekly
    sudo chmod 700 /etc/cron.monthly
    sudo chmod 750 /home/gofossroot
    sudo chmod 750 /home/gofossadmin
    ```


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/50edc3be-5fc6-43c2-a283-166ebdc54b78" frameborder="0" allowfullscreen></iframe>
    </center>


??? info "Hier erfahrt Ihr mehr zu Linux-Berechtigungen"

    [umask](https://en.wikipedia.org/wiki/Umask) schränkt den Benutzerzugriff auf neu erstellte Dateien oder Verzeichnisse ein. Es gibt **drei Benutzerklassen**:

    <center>

    |Benutzerklassen |Beschreibung |
    | ------ | ------ |
    | User |EigentümerIn der Datei oder des Verzeichnisses. |
    | Group |BenutzerInnen, die zu der Gruppe der Datei oder des Verzeichnisses gehören. |
    | Others |BenutzerInnen, die weder Eigentümer noch Mitglied der Gruppe der Datei oder des Verzeichnisses sind. |

    </center>

    Zudem gibt es **drei Zugangsbeschränkungen**:

    <center>

    |Zugangsbeschränkung |Beschreibung |Dezimalwert |
    | ------ | ------ | :------: |
    | Read ( r ) |Datei oder Verzeichnis lesen bzw. anzeigen.| 4 |
    | Write ( w ) |Datei oder Verzeichnis bearbeiten bzw. ändern.| 2 |
    | Execute ( x ) |Datei oder Verzeichnis ausführen.| 1 |

    </center>

    `umask` schränkt den Zugriff auf neu erstellte Dateien oder Verzeichnisse ein, indem die drei Dezimalwerte addiert werden:

    <center>

    |umask |Wert |Zugangsbeschränkung |
    | :------: | :------: | ------ |
    | --- | 0 = 0 + 0 + 0 |Lesen, schreiben und ausführen erlaubt. |
    | --x | 1 = 0 + 0 + 1 |Lesen und schreiben erlaubt, ausführen nicht. |
    | -w- | 2 = 0 + 2 + 0 |Lesen und ausführen erlaubt, schreiben nicht. |
    | -wx | 3 = 0 + 2 + 1 |Lesen erlaubt, schreiben und ausführen nicht. |
    | r-- | 4 = 4 + 0 + 0 |Schreiben und ausführen erlaubt, lesen nicht. |
    | r-x | 5 = 4 + 0 + 1 |Schreiben erlaubt, lesen und ausführen nicht. |
    | rw- | 6 = 4 + 2 + 0 |Ausführen erlaubt, lesen und schreiben nicht. |
    | rwx | 7 = 4 + 2 + 1 |Weder lesen, schreiben noch ausführen sind erlaubt. |

    </center>

    `umask` besteht aus drei Werten, einen für jede Benutzerklasse. Wenn Ihr also zum Beispiel `umask` von `022` auf `027` ändert, wird verhindert, dass "andere (others)" Dateien und Verzeichnisse lesen, schreiben oder ausführen können:

    <center>

    |Umask |Beschränkungen für BenutzerInnen |Beschränkungen für Gruppen |Beschränkungen für Andere |Resultierende Berechtigungen |
    | :------: | :------: | :------: | :------: | ------ |
    | `022` | --- | -w- | -w- |• Der Eigentümer der Datei oder des Verzeichnisses kann lesen, schreiben und ausführen. <br>• Gruppenmitglieder der Datei oder des Verzeichnisses können lesen und ausführen, aber nicht schreiben. <br>• BenutzerInnen, die weder Eigentümer noch Mitglied der Gruppe der Datei oder des Verzeichnisses sind, können lesen und ausführen, aber nicht schreiben. <br>• Mit anderen Worten: Der Eigentümer der Datei oder des Verzeichnisses hat Vollzugriff, Gruppenmitglieder und Andere können nur lesen und ausführen. |
    | `027` | --- | -w- | rwx |• Der Eigentümer der Datei oder des Verzeichnisses kann lesen, schreiben und ausführen. <br>• Gruppenmitglieder der Datei oder des Verzeichnisses können lesen und ausführen, aber nicht schreiben. <br>• BenutzerInnen, die weder Eigentümer noch Mitglied der Gruppe der Datei oder des Verzeichnisses sind, können weder lesen, schreiben noch ausführen. <br>• Mit anderen Worten: Der Eigentümer der Datei oder des Verzeichnisses hat Vollzugriff, Gruppenmitglieder können nur lesen und ausführen, und andere haben keinen Zugriff. |

    </center>

    In mehreren Kapiteln wird der `chmod`-Befehl verwendet, um Zugriffsrechte für Dateien oder Verzeichnisse festzulegen. Im Gegensatz zu `umask`, das die Standard-*Einschränkungen* für *neu erstellte* Dateien oder Verzeichnisse festlegt, definiert der `chmod`-Befehl *Berechtigungen* für *vorhandene* Dateien oder Verzeichnisse. Daher sind die Werte zwischen `umask` und `chmod` in gewisser Hinsicht symmetrisch.


<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Kerndumps" width="150px"></img> </center>

## Kerndumps deaktivieren

Ein Kerndump ("core dump") ist eine Datei, die den Speicherzustand eines Programms im Falle eines Absturzes beinhaltet. Das ist z.B. für die Fehlersuche nützlich, kann aber gegebenenfalls sensible Daten preisgeben. Falls Ihr keine Fehlersuche betreibt empfehlen wir daher, Kerndumps zu deaktivieren. Untenstehend findet Ihr detaillierte Anweisungen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Legt Sicherungskopien der Konfigurationsdateien an:

    ```bash
    sudo cp --archive /etc/security/limits.conf /etc/security/limits.conf-COPY-$(date +"%Y%m%d%H%M%S")

    sudo cp --archive /etc/sysctl.conf /etc/sysctl.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die erste Konfigurationsdatei:

    ```bash
    sudo vi /etc/security/limits.conf
    ```

    Fügt folgende Zeilen am Ende der Datei ein:

    ```bash
    * soft core 0
    * hard core 0
    ```

    Speichert und schließt die Datei (`:wq!`).

    Öffnet die zweite Konfigurationsdatei:

    ```bash
    sudo vi /etc/sysctl.conf
    ```

    Fügt folgende Zeilen am Ende der Datei ein:

    ```bash
    # Disable coredumps
    fs.suid_dumpable=0
    ```

    Speichert und schließt die Datei (`:wq!`). Übernehmt schließlich alle Änderungen:

    ```bash
    sudo sysctl -p
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/14ad0c9b-d605-4b2b-813b-2a59ddb1487a" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Kernel-Module" width="150px"></img> </center>

## Ungenutzte Kernel-Module deaktivieren

Der Linux-Kernel ist das Herzstück des Betriebssystems. Er steuert die Hardware, den Speicher, Aufgaben und Prozesse usw. Kernel-Module sind Programmbausteine, die die Funktionalität des Kernels erweitern, zum Beispiel um auf neue Hardware zuzugreifen. Allerdings können Kernel-Module auch missbraucht werden, um Schadprogramme zu laden oder sich unbefugten Zugriff auf den Server zu verschaffen. Um dies zu vermeiden, könnt Ihr das Aufrufen von nicht verwendeten Kernel-Modulen unterbinden.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Legt eine Sicherungskopie der Konfigurationsdatei an:

    ```bash
    sudo cp --archive /etc/modprobe.d/blacklist.conf /etc/modprobe.d/blacklist.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/modprobe.d/blacklist.conf
    ```

    Fügt folgende Zeilen am Ende der Datei ein::

    ```bash
    # Instruct modprobe to force inactive modules to always fail loading
    install cramfs /bin/false
    install freevxfs /bin/false
    install hfs /bin/false
    install hfsplus /bin/false
    install jffs2 /bin/false
    install udf /bin/false
    ```

    Speichert und schließt die Datei (`:wq!`).


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/66cc82c1-f44c-41ee-8ae2-8e06066a5bae" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/incident.png" width="550px" alt="Ubuntu server security"></img>
</div>

<br>
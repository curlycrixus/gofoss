---
template: main.html
title: Tragt zum gofoss.net Projekt bei
description: Tragt zum gofoss.net Projekt bei. Schließt Euch der gofoss.net-Gemeinschaft an. Macht Vorschläge. Verbessert den Inhalt. Helft bei Übersetzungen. Meldet Probleme auf GitLab.
---

# Beteiligt Euch am gofoss.net Projekt!

<div align="center">
<img src="../../assets/img/contributing.png" alt="Zum gofoss.net Projekt beitragen" width="375px"></img>
</div>

Herzlich willkommen! [gofoss.net](https://gofoss.net/de/) ist ein freies und quelloffenes Projekt. Alle haben somit die Gelegenheit, den Code der Website zu überprüfen und Verbesserungen vorzuschlagen. Beteiligt Euch, es gibt zahlreiche Möglichkeiten, um mitzumachen!

* Verbessert den Inhalt, fügt neue Funktionen hinzu, behebt Fehler
* Erhöht die Benutzerfreundlichkeit, entwerft neue Grafiken
* Helft uns beim Übersetzen
* Moderiert Diskussionen
* Pflegt den Code

<div style="margin-top:-40px">
</div>


## Leitlinien für Beiträge

??? tip "Nehmt Kontakt auf"

     Falls Ihr Fragen habt oder diskutieren möchtet, besteht kein Bedarf ein sogenanntes *Issue* auf GitLab anzulegen. Meldet Euch einfach über die sozialen Medien bei uns:

     &nbsp;&nbsp; :material-twitter: [Folgt @gofoss_today auf Twitter](https://nitter.net/gofoss_today)

     &nbsp;&nbsp; :material-mastodon: [Folgt @don_atoms auf Mastodon](https://hostux.social/@don_atoms/)

     &nbsp;&nbsp; :material-reddit: [Schreibt @gofosstoday auf Reddit an](https://teddit.net/u/gofosstoday/)

     &nbsp;&nbsp; :fontawesome-solid-users: [Schreibt @gofoss auf Lemmy an](https://lemmy.ml/u/gofoss)

     &nbsp;&nbsp; :material-matrix: [Tretet unserem Matrix-Chat bei](https://matrix.to/#/#gofoss-community:matrix.org/)

     &nbsp;&nbsp; :material-email: [Schickt uns eine E-Mail](mailto:gofoss@protonmail.com)


??? tip "Öffnet eine GitLab-Issue"

	<center>

	| Anweisungen | Beschreibung |
	| ------ | ------ |
	| Stellt ein paar Nachforschungen an |• Sucht nach ähnlichen Beiträgen, bevor Ihr selbst einen einreicht <br>• Es ist gut möglich, dass jemand anderes bereits das gleiche Problem oder den gleichen Funktionsvorschlag hatte <br>• [Nehmt Kontakt mit der gofoss.net-Gemeinschaft auf](#nehmt-kontakt-auf) <br>• [Sucht im sogenannten *Issue Tracker*](https://gitlab.com/curlycrixus/gofoss/-/issues) <br>• [Schaut Euch die Roadmap an](https://gofoss.net/de/roadmap/) |
	| Legt ein Issue an |• Falls Eure Nachforschungen erfolglos bleiben, könnt Ihr ein Issue anlegen <br>• [Sign in to GitLab](https://gitlab.com/users/sign_in) <br>• Öffnet den gofoss.net [Issue Tracker](https://gitlab.com/curlycrixus/gofoss/-/issues) <br>• Klickt auf die `New Issue`-Schaltfläche <br>• Gebt einen Titel an <br>• Wählt die richtige Vorlage (auf Englisch, *Template*) aus: <br><br>`content_request`: Vorschläge für neue oder verbesserte Inhalte, z. B. neue Software, fehlende oder falsche Informationen, Rechtschreib- oder Grammatikfehler, Verbesserung der Lesbarkeit usw. <br><br>`feature_request`: Vorschläge für neue Webseiten-Funktionen, z. B. verbesserte Benutzerführung, verbessertes Erscheinungsbild, neue Graphiken, Dunkelmodus usw. <br><br>`bug_report`: unerwartetes Verhalten der Webseite, z. B. fehlerhafte Links, Probleme beim Laden auf bestimmten Geräten usw. |
    | Nehmt am Überprüfungsverfahren teil |• Alle Issues werden öffentlich diskutiert und überprüft <br>• Verfolgt aufmerksam die Diskussionen zu Euren Issues <br>• Beantwortet mögliche Fragen und überprüft Beiträge anderer <br>• Das ist genauso wertvoll wie ein Eigenbeitrag <br>• [Richtet E-Mail-Benachrichtigungen ein](https://docs.gitlab.com/ee/user/profile/notifications.html), um auf dem Laufenden zu bleiben |
	| Warten, bis das Issue gelöst oder geschlossen ist |• Wir tun unser Bestes, um Issues schnell zu schließen <br>• Im Gegenzug bitten wir Euch, für klärende Fragen oder Korrekturen verfügbar zu sein <br>• Sollten wir keinerlei Rückmeldung erhalten, behalten wir uns das Recht vor offene Issues zu schließen |


	</center>


??? tip "Bearbeitet laufende GitLab-Issues"

    ### Arbeitsabläufe für Verzweigungen und Issues

    <div align="center">
    <img src="../../assets/img/issue_branching.png" alt="Issue-Verzweigung" width="700px"></img>
    </div>

	<center>

	| Verzweigung | Grundsätze |
	| ------ | ------ |
    |`main` |• Der `main`-Zweig (auf Englisch *branch*) ist der Produktionszweig <br>• Jegliche Änderung (auf Englisch *commit*) am `main`-Zweig wird automatisch von GitLab an Netlify übermittelt <br>• Der `main`-Zweig ist daher geschützt <br>• Er ist ausschließlich für gofoss.net Team-Mitglieder, die sogenannten `Maintainer`, zugänglich |
    |`dev` |• Der `dev`-Zweig wird aus dem `main`-Zweig erzeugt <br>• Hier werden neue Inhalte, Funktionen sowie Fehlerbehebungen entwickelt und getestet <br>• Der `dev`-Zweig ist ausschließlich für `maintainers` im gofoss.net-Team zugänglich <br>• Der `dev`-Zweig wird mit dem `main`-Zweig zusammengeführt (auf Englisch *merged*), wenn er für die Veröffentlichung bereit ist |
    |`issue` |• Jedem Issue sollte ein eigener Zweig zugewiesen werden <br>• `Issue`-Zweige werden aus dem `dev`-Zweig erzeugt <br>• Sobald die Arbeit an einem Issue abgeschlossen ist, wird der `Issue`-Zweig wieder mit dem `dev`-Zweig zusammengeführt <br>• `Issue`-Zweige interagieren daher nie direkt mit dem `main`-Zweig <br>• Sowohl gofoss.net-`Maintainer` als auch externe Mitwirkende können an `Issue`-Zweigen arbeiten |

	</center>


    ### Arbeitsabläufe für Gabelungen

    <div align="center">
    <img src="../../assets/img/forking_workflow.png" alt="Arbeitsabläufe für Gabelungen" width="700px"></img>
    </div>

	<center>

	| Anweisungen | Beschreibung |
	| ------ | ------ |
	| Wählt ein Issue, an dem Ihr arbeiten möchtet |• [Sucht im *Tracker*](https://gitlab.com/curlycrixus/gofoss/-/issues) nach Issues, die mit `accepting_merge_requests` vermerkt sind <br> • Diese Issues sind für externe Beiträge offen <br> • Setzt Euch mit dem gofoss.net-Team in Verbindung, bevor Ihr mit der Arbeit an einem Issue beginnt <br> • Hinterlasst entweder einen Kommentar zu dem Issue, an dem Ihr arbeiten möchtet <br> • Oder schickt uns eine Nachricht auf [Mastodon](https://hostux.social/@don_atoms/), [Twitter](https://nitter.net/gofoss_today) oder [email](mailto:gofoss@protonmail.com) <br> • Es gibt auch einen [Matrix-Chat](https://matrix.to/#/#gofoss-dev:matrix.org/), in dem sich die gofoss.net-Entwickler aufhalten (nur mit Einladung) <br> • Wartet, bis jemand aus dem gofoss.net-Team Eure Beitragsanfrage bestätigt hat |
    | Gabelt (*forkt*), klont und synchronisiert das gofoss.net-Repository |• [Meldet Euch bei GitLab an](https://gitlab.com/users/sign_in) <br>• Ruft das [gofoss.net Projekt](https://gitlab.com/curlycrixus/gofoss) auf <br> • Klickt auf die `Fork`-Schaltfläche <br>• Dadurch wird eine Kopie des gofoss.net-Projekts in Eurem eigenen GitLab-Repository erstellt <br> • Öffnet auf Eurem lokalen Rechner ein Terminal und legt einen Projektordner an: <br>`mkdir ~/git_projects` <br>• Klont Euer GitLab repository: <br>`cd ~/git_projects` <br>`git clone https://gitlab.com/<YOUR_USERNAME>/gofoss.git` <br>• Wechselt zur lokalen Kopie Eures Repositorys: <br>`cd ~/git_projects/gofoss` <br> • Erstellt eine Verknüpfung mit dem vorgelagerten gofoss.net-Projekt, um Euren *Fork* auf dem neuesten Stand zu halten: <br>`git remote add upstream https://gitlab.com/curlycrixus/gofoss.git` <br>• Gleicht Euch regelmäßig mit dem `dev`-Zweig des vorgelagerten Projekts ab: <br>`cd ~/git_projects/gofoss` <br>`git fetch upstream` <br>`git checkout dev` <br>`git pull upstream dev` <br>`git push origin dev` <br>• Es kann sonst vorkommen, dass Eure Zusammenführungsanfrage aufgrund eines veralteten *Forks* abgelehnt wird |
    | Richtet die Entwicklungsumgebung ein |• Gebt Eure GitLab-Daten an: <br>`git config user.email "<USER>@example.com"` <br>`git config user.name "<USERNAME>"` <br>• Installiert die aktuellste Python-Version sowie Abhängigkeiten: <br>`sudo apt install python3 python3-pip` <br>• Stellt sicher, dass die Python-Version 3 installiert ist: <br>`python3 --version` <br>• Installt Mkdocs: <br>`sudo apt install mkdocs` <br>• Installt Material für Mkdocs: <br>`pip install mkdocs-material` <br>• Installt die [Mehrsprachen-Erweiterung](https://ultrabug.github.io/mkdocs-static-i18n/en/): <br>`pip install mkdocs-static-i18n` <br>• Installiert Abhängigkeiten: <br>`pip install -r requirements.txt` |
	| *Committet* lokal und *pusht* an das Remote-Repository |• Erstellt einen neuen `Issue`-Zweig unter Beachtung der Namenskonvention `[content/feature/bug]-[#IssueID]-[Kurzbeschreibung]`<br>• Hier ein Beispiel: <br>`git checkout -b feature-#72-add-dark-mode` <br>• Wechselt in den neuen Zweig: <br>`git checkout feature-#72-add-dark-mode` <br>• Beginnt zu Programmieren: fügt Inhalte und Funktionen hinzu oder behebt Fehler <br>• Lokale Änderungen können im Browser unter `localhost:8000` mit folgendem Befehl angezeigt werden: <br> `mkdocs serve` <br>• Sobald alles wie erwartet funktioniert, könnt Ihr den Code in den sogenannten Staging-Bereich einfügen: <br>`git add .` <br>• Bestätigt die Änderungen und fügt einen eindeutigen Kommentar hinzu: <br>`git commit -m "<CLEAR COMMENT>"` <br>• Übermittelt abschließen den `Issue`-Zweig an Euer Remote-Repository: <br>`git push -u origin feature-#72-add-dark-mode` |
    | Öffnet eine Zusammenführungsanfrage (*Merge Request*) |• [Meldet Euch bei GitLab an](https://gitlab.com/users/sign_in) <br>• Ruft `https://gitlab.com/<YOUR_USERNAME>/gofoss/-/merge_requests` auf <br>• Klickt auf die `New merge request`-Schaltfläche <br>• Wählt den Quellzweig aus: `<USERNAME>/gofoss/feature-#72-add-dark-mode` <br>• Wählt den Zielzweig aus: `curlycrixus/gofoss/dev` <br>• Klickt auf die `Compare branches and continue`-Schaltfläche <br>• Wählt die `merge_request` Vorlage <br>• Gebt eindeutige Titel und Beschreibungen für Euren Zusammenführungsantrag an <br>• Wählt die Option `Delete source branch when merge request is accepted` <br>• Klickt auf die `Create merge request`-Schaltfläche |
    | Nehmt am Überprüfungsverfahren teil |• Alle Zusammenführungsanfragen werden öffentlich diskutiert und geprüft <br>• Verfolgt aufmerksam die Diskussionen zu Eurer Zusammenführungsanfrage <br>• Beantwortet mögliche Fragen <br>• [Richtet E-Mail-Benachrichtigungen ein](https://docs.gitlab.com/ee/user/profile/notifications.html), um auf dem Laufenden zu bleiben |
	| Wartet auf die Bestätigung |• Ein gofoss.net-`Maintainer` wird Euren `Issue`-Zweig in sein lokales Repository kopieren (*pullen*) <br>• Euer Beitrag wird dann geprüft  <br>• Falls der `Maintainer` Kommentare haben sollte, solltet Ihr Eure Zusammenführungsanfrage anpassen <br>• Falls alles in Ordnung ist, führt der gofoss.net-Maintainer Euren `Issue`-Zweig mit seinem lokalen `dev`-Zweig zusammen und übermittelt die Änderungen an das gofoss.net-Repository <br>• Damit ist Euer Beitrag Teil des gofoss.net-Projekts! <br>• Die anderen EntwicklerInnen sollten sich nun mit dem gofoss.net-Repository synchronisieren |
	| Entfernt den Zweig |• Nachdem Eure Zusammenführungsanfrage übernommen wurde, sollte der `Issue`-Zweig `feature-#72-add-dark-mode` gelöscht worden sein <br>• Falls nicht, solltet Ihr ihn händisch entfernen <br>• Ihr könnt die Änderungen nun sicher aus dem vorgelagerten Repository übernehmen (*pullen*)` |
    | Lizenz |Indem Ihr zu gofoss.net beitragt, stimmt Ihr zu, dass Eure Beiträge unter der [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt) veröffentlicht werden. |

	</center>


    ### Verzeichnisstruktur

	Achtet bitte darauf, die Verzeichnisstruktur beizubehalten:

	```bash
	.
	├─ .gitlab/             # Vorlagen für *Issues* und Zusammenführungsanfragen (*Merge Requests*)
	│
	├─ docs/
	│  │
	│  ├─ assets/
	│  │  └─ css/			# Stylesheets (.css), Schriftarten (.ttf) & Symboldateien (.svg)
	│  │  └─ echarts/		# E-Chart Dateien (.html, .min.js)
	│  │  └─ img/			# Bilddateien (.svg, .png, etc.)
	│  │
	│  └─ *.xyz.md			# mehrsprachige Markdown-Dateien (.en.md, .fr.md, .de.md, etc.)
	│
	├─ overrides/			# Themenerweiterungen (home_xyz.html, main.html, overrides.xyz.min.css)
	│
	├─ LICENSE			    # Mkdocs-Material-Lizenz
	│
	├─ netlify.toml		    # Anleitung zur Netlify-Implementierung
	│
	├─ requirements.toml	# Abhängigkeiten
	│
	├─ runtime.txt			# Python-Version für die Netlify-Implementierung
	│
	└─ mkdocs.yml			# Mkdocs-Konfigurationsdatei
	```


??? tip "Helft mit beim Übersetzen"

	<center>

	| Anweisungen | Beschreibung |
	| ------ | ------ |
    | Tretet einer Verteilerliste bei |Falls Ihr eine oder mehrere der veröffentlichten Sprachen korrekt beherrscht und über Grundkenntnisse der Markdown-Syntax verfügt, könnt Ihr gerne einer der folgenden Verteilerlisten beitreten: <br>• [EN ‣ FR Übersetzungsteam (Framasoft-Gemeinschaft)](https://framalistes.org/sympa/info/framalang/) <br>• [EN ‣ DE Übersetzungsteam](https://framalistes.org/sympa/info/gofosslang_de/) <br>• [EN ‣ ES Übersetzungsteam](https://framalistes.org/sympa/info/gofosslang_es/) <br>• [EN ‣ PL Übersetzungsteam](https://framalistes.org/sympa/info/gofosslang_pl) |
    | Tauscht Euch mit anderen ÜbersetzerInnen aus | Es gibt ebenfalls einen [Matrix-Chat](https://matrix.to/#/#gofoss-translation:matrix.org/), in dem sich die ÜbersetzerInnen austauschen, koordinieren und beraten. |
    | Erhaltet Zugang zu den Übersetzungsdateien |Alle ÜbersetzerInnen erhalten einen Link, um auf die Projektdateien zugreifen zu können. |
    | Arbeitet mit Framapad |• Die Übersetzungsarbeit wird mit dem kollaborativen Online-Texteditor Framapad verwaltet <br>• Einzelbeiträge sind durch einen Farbcode gekennzeichnet <br>• Änderungen erscheinen in Echtzeit auf dem Bildschirm <br>• Bitte lest die LIES-MICH-Datei, sie enthält zusätzliche Richtlinien. |
    | Verwendet das Glossar |Verwendet bei der Übersetzungsarbeit die GLOSSAR-Datei. |
    | Erhaltet den Originaltext |Fügt Übersetzungen direkt nach dem Originaltext ein, und behaltet dabei den Originaltext unverändert bei. |
    | Behaltet Code-Abschnitte bei |Stellt sicher, dass alle Markdown- und HTML-Formatierungselemente erhalten bleiben. |
    | Respektiert die Arbeit anderer ÜbersetzerInnen |• Respektiert bitte die Arbeit anderer Mitwirkender <br>• Falls Ihr mit einer Übersetzung nicht einverstanden seid, könnt Ihr gerne eigenen Vorschläge einbringen <br>• Aber bitte ohne bestehende Übersetzungen zu ändern oder zu löschen |
    | Missbraucht Eure Befugnisse nicht |Bitte Missbraucht Eure Befugnisse nicht. Übersetzungsarbeit basiert auf Vertrauen. |
    | Wartet auf die Freigabe |Übersetzungen werden auf der Webseite veröffentlicht, sobald sie zu 100 % fertiggestellt sind und vom Projektteam geprüft wurden. |

	</center>


??? tip "Haltet Euch an die Benimmregeln"

	<center>

	| Anweisungen | Beschreibung |
	| ------ | ------ |
    |Regel #1 |Ihr erklärt Euch bereit, mit Menschen aus aller Welt zusammenzuarbeiten, unabhängig von Geschlecht, Alter, Staatsangehörigkeit, ethnischer Zugehörigkeit, Aussehen, Bildung, sozialem Hintergrund oder Religion. |
    |Regel #2 |Bitte bleibt in der Diskussion stets freundlich, respektvoll, konstruktiv und beim Thema. Ungehobeltes Verhalten wird nicht toleriert. Keine Flame-Wars; kein Trolling; keine Beleidigungen; keine sexualisierte Sprache; keine öffentliche oder persönliche Belästigung; kein Doxing; keine persönlichen, rassistischen, politischen oder religiösen Angriffe. |
    |Regel #3 |Respektiert bitte die Entscheidungen des Projektteams. Wir versuchen, Fragen, Probleme und Zusammenführungsanträge so schnell wie möglich zu lesen, zu prüfen und zu beantworten – aber vergesst nicht, unsere Zeit ist begrenzt. Das Team begrüßt auch Vorschläge, Empfehlungen und Beiträge – was jedoch nicht bedeutet, dass jede Entscheidung im Gruppenkonsens getroffen werden muss. |
    |Regel #4 |Bitte vermeidet private Diskussionen über das Projekt, wenn es keinen guten Grund dafür gibt. Solche Diskussionen sollten öffentlich bleiben, und auf GitLab oder über die offiziellen Kommunikationskanäle des Projekts geführt werden. |
    |Regel #5 |Reicht mögliche Beschwerden bei [gofoss@protonmail.com](mailto:gofoss@protonmail.com) ein. Verstöße gegen Regel #1 bis #4 können zu angemessenen Maßnahmen führen. Dazu gehören freundliche Ermahnungen, schriftliche Verwarnungen, sowie die Löschung, Bearbeitung oder Zurückweisung von unangemessenen Beiträgen, Kommentaren, *Commits*, Programmiercodes, *Issues* oder Zusammenführungsanträgen. Als letztes Mittel kann eine Verbannung aus dem Projekt ausgesprochen werden. |

	</center>


## Ihr wollt spenden?

Vielen Dank für Euer Interesse!

Die meisten "kostenlosen" Webseiten verkaufen Werbung, Daten ihrer Nutzer oder kostenpflichtige Inhalte. gofoss.net ist hingegen wirklich *frei* (wie in *Freibier*): frei in der Nutzung, frei im Quellcode, frei von Werbung, frei von Tracking, frei von gesponserten oder kostenpflichtigen Inhalten, frei von Affiliate-Links.

gofoss.net ist ein ehrenamtlich geführtes, gemeinnütziges Projekt. Alle Betriebskosten, Entwicklungen und Inhalte werden ausschließlich durch Spenden der NutzerInnen finanziert. Wenn Ihr spenden oder uns einen Kaffee ausgeben möchtet, klickt bitte auf den Knopf unten :)


<center>

<a href="https://liberapay.com/gofoss/donate"><img alt="Mit Liberapay spenden" src="https://liberapay.com/assets/widgets/donate.svg"></a>

</center>

Erwägt ebenfalls die Unterstützung anderer [Open-Source-Entwickler](https://gofoss.net/de/thanks/). Oder unterstützt Aktivistengruppen und Software-Stiftungen, die sich für bessere Datenschutzgesetze, Nutzerrechte und Innovationsfreiheit einsetzen:

* die [Digitale Gesellschaft](https://digitalegesellschaft.de/)
* [Digitalcourage](https://digitalcourage.de)
* den [Chaos Computer Club](https://www.ccc.de/)
* die [Free Software Foundation Europe](https://fsfe.org/index.de.html)
* und viele mehr

<div align="center">
<img src="https://imgs.xkcd.com/comics/fixing_problems.png" alt="Zu gofoss.net beitragen"></img>
</div>

<br>

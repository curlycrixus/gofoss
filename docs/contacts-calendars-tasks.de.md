---
template: main.html
title: Hostet Eure Kontakte, Kalender und Aufgaben selbst
description: Hostet Eure Cloud-Dienste selbst. Installiert Radicale. Was ist Radicale? Was ist CalDAV oder CarDAV? Was ist DAVx5? Wie installiert man Radicale? Ist Radicale sicher?
---

# Radicale, selbst gehostete Kontakte, Kalender & Aufgaben

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/radicale.png" alt="Radicale Kalender" width="550px"></img>
</div>

<br>

[Radicale](https://radicale.org/) ist eine Lösung zur Verwaltung von Kontakten, Kalendern und Aufgaben, die Ihr selbst auf Eurem [Ubuntu-Server](https://gofoss.net/de/ubuntu-server/) hosten könnt.  Wenngleich der Funktionsumfang überschaubar bleibt, zeichnet sich Radicale durch seine hocheffiziente, plattformübergreifende Datensynchronisation aus. Radicale ist frei, quelloffen und verfügt über Client-Anwendungen für zahlreiche Plattformen.

<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Radicale Installation" width="150px"></img> </center>

## Radicale Installation

Folgt den untenstehenden Anweisungen, um alle Abhängigkeiten zu prüfen und Radicale auf Eurem Server zu installieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Voraussetzungen

    Python ist zur Ausführung von Radicale erforderlich, installiert folgende Pakete auf dem Server:

    ```bash
    sudo apt install python3 python3-pip
    ```

    ### Installation

    Installiert Radicale:

    ```bash
    sudo -H python3 -m pip install --upgrade radicale
    ```

    Legt als nächstes einen Systembenutzer an, der Radicale ausführen soll. Für die Zwecke dieses Tutorials nennen wir diesen Systembenutzer `radicaleadmin`. Natürlich könnt Ihr jeden beliebigen Namen wählen, stellt dabei nur sicher, dass Ihr die Befehle entsprechend anpasst:


    ```bash
    sudo useradd --system --home-dir / --shell /sbin/nologin radicaleadmin
    ```

    Es wird keine Datenbank benötigt, erstellt einfach einen Ordner, in dem die Daten von Radicale abgespeichert werden:

    ```bash
    sudo mkdir -p /var/lib/radicale/collections
    ```

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/102824be-fd90-4384-ae76-e973fdaa77c5" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Radicale Konfiguration" width="150px"></img> </center>

## Konfiguration

Nach der erfolgreichen Installation von Radicale sind eine Reihe von Einstellungen vorzunehmen: Mehrbenutzerunterstützung, Berechtigungen, Autostart beim Booten, usw. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### BenutzerInnen

    Radicale unterstützt mehrere BenutzerInnen. Jeder Nutzer kann über eigene Kalender, Kontakte und Aufgabenlisten verfügen. Der Zugriff kann durch Zugangsdaten gesichert werden, die in einer [verschlüsselten Datei](https://httpd.apache.org/docs/current/programs/htpasswd.html) auf dem Server gespeichert werden.

    Installiert die folgenden Pakete, um die Verschlüsselung zu aktivieren:

    ```bash
    sudo -H python3 -m pip install --upgrade passlib bcrypt
    ```

    Erstellt nun ein Verzeichnis für die Zugangsdaten:

    ```bash
    sudo mkdir /var/www/radicale
    ```

    Legt schließlich ein erstes Benutzerkonto mit dem folgenden Befehl an, der unter anderem den `-c` Parameter übergibt. Ersetzt den Parameter `$USER` durch den tatsächlichen Benutzernamen. Wenn Ihr dazu aufgefordert werdet, gebt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) ein:

    ```bash
    sudo htpasswd -B -c /var/www/radicale/users $USER
    ```

    Zur Veranschaulichung legen wir drei Benutzer an — Georg, Lenina und Tom:

    ```bash
    sudo htpasswd -B -c /var/www/radicale/users Georg
    sudo htpasswd -B /var/www/radicale/users Lenina
    sudo htpasswd -B /var/www/radicale/users Tom
    ```

    Beachtet, dass der `-c` Parameter lediglich zur Erstellung des ersten Benutzers übergeben werden muss, in diesem Fall "Georg". Zur Erstellung weiterer BenutzerInnen kann der Parameter hingegen entfernt werden, in diesen Fall "Lenina" sowie "Tom".

    Überprüft abschließend, ob alle Zugangsdaten korrekt erstellt wurden:

    ```bash
    sudo cat /var/www/radicale/users
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    Georg:$2y$05$Zsb8MYlKjRr2InVi/IQNtustqfdnHWN14uG9jNNhx1g rF7a/8Cg6
    Lenina:$2y$05$Hh2Y8nrikm 9f1Pz8LrOFe K5NR HlL0Qz3Yvv gupSBgIr4lT0bi
    Tom:$2y$05$1BnHnXCqNHXZtWMiFhnxBOpYM9xeVnZXm70SoTclEfsrI JPSvDVy
    ```


    ### Einstellungen

    Erstellt ein Verzeichnis, in dem alle Einstellungen abgelegt werden:

    ```bash
    sudo mkdir /etc/radicale
    ```

    Öffnet die Konfigurationsdatei:

    ```bash
    sudo vi /etc/radicale/config
    ```

    Ergänzt oder ändert die folgenden Zeilen:

    ```bash
    [server]
    hosts               = 0.0.0.0:5232, [::]:5232
    max_connections     = 20
    max_content_length  = 10000000
    timeout             = 10

    [auth]
    type		        = http_x_remote_user
    htpasswd_filename   = /var/www/radicale/users
    htpasswd_encryption = bcrypt
    delay               = 5

    [storage]
    filesystem_folder   = /var/lib/radicale/collections
    ```

    Hier ein paar Zusatzinformationen zu diesen Einstellungen:

    <center>

    | Einstellung | Beschreibung |
    | ------ | ------ |
    | `hosts` | Ermöglicht den Fernzugriff auf Kalender und Kontakte. |
    | `max_connections` | Begrenzt die Anzahl der parallelen Verbindungen auf 20. |
    | `max_content_length` | Begrenzt die Dateigröße auf 1 MB. Wichtig für umfangreiche Kontaktfotos. |
    | `timeout` | Beendet die Verbindung nach 10 Sekunden. |
    | `type` | Aktiviert die Authentifizierung mit dem Reverse-Proxy. Radicale verwendet den im X-Remote-User HTTP-Header angegebenen Benutzernamen. |
    | `htpasswd_filename` | Gibt den Ordnerpfad an, unter dem Benutzernamen und Passwörter gespeichert werden. Im vorliegenden Fall handelt es sich um `/var/www/radicale/users`. |
    | `htpasswd_encryption` | Gibt die Methode an, die zur Verschlüsselung von Benutzernamen und Passwörtern verwendet werden soll. Im vorliegenden Fall handelt es sich um `bcrypt`. |
    | `delay` | Setzt die Wartezeit nach einem fehlgeschlagenen Anmeldeversuch auf 5 Sekunden, um sogenannte Brute-Force-Angriffe abzuwehren. |
    | `filesystem_folder` | Gibt den Ordnerpfad an, unter dem Radicale Daten speichert. Im vorliegenden Fall handelt es sich um `/var/lib/radicale/collections`. |

    </center>


    ### Zugriffsrechte

    Zugriffsrechte auf die Verzeichnisse `/var/lib/radicale` (enthält Daten), `/var/www/radicale` (enthält verschlüsselte Zugangsdaten) und `/etc/radicale` (enthält Einstellungen) werden auf den Nutzer `radicaleadmin` beschränkt:

    ```bash
    sudo chown -R radicaleadmin:radicaleadmin /var/lib/radicale
    sudo chmod -R 700 /var/lib/radicale

    sudo chown -R radicaleadmin:radicaleadmin /var/www/radicale
    sudo chmod -R 755 /var/www/radicale

    sudo chown -R radicaleadmin:radicaleadmin /etc/radicale
    sudo chmod -R 700 /etc/radicale
    ```


    ### Autostart

    Stellt sicher, dass Radicale bei jedem Hochfahren des Servers automatisch gestartet wird. Erstellt eine Konfigurationsdatei:

    ```bash
    sudo vi /etc/systemd/system/radicale.service
    ```

    Gebt den folgenden Text ein und passt den Inhalt (Benutzernamen, usw.) entsprechend an:

    ```bash
    [Unit]
    Description=A simple CalDAV (calendar) and CardDAV (contact) server
    After=network.target
    Requires=network.target

    [Service]
    ExecStart=/usr/bin/env python3 -m radicale
    Restart=on-failure
    User=radicaleadmin
    Group=radicaleadmin
    PrivateTmp=true
    ProtectSystem=strict
    ProtectHome=true
    PrivateDevices=true
    ProtectKernelTunables=true
    ProtectKernelModules=true
    ProtectControlGroups=true
    NoNewPrivileges=true
    ReadWritePaths=/var/lib/radicale/collections

    [Install]
    WantedBy=multi-user.target
    ```

    Stellt sicher, dass Radicale bei jedem Boot-Vorgang automatisch gestartet wird:

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable radicale
    ```

    Startet den Server neu:

    ```bash
    sudo reboot
    ```

    Vergewissert Euch, dass Radicale aktiv ist und läuft (der Status sollte "Activ" sein):

    ```bash
    sudo systemctl status radicale
    ```

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/401e2339-44c8-4bea-835e-c7e4b52ce866" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Radicale Weboberfläche" width="150px"></img> </center>

## Web-Oberfläche

Wir werden einen Apache Virtual Host als Reverse Proxy einrichten, um auf die Radicale-Weboberfläche zuzugreifen. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Erstellt eine Apache-Konfigurationsdatei:

    ```bash
    sudo vi /etc/apache2/sites-available/myradicale.gofoss.duckdns.org.conf
    ```

    Fügt den folgenden Inhalt hinzu und stellt sicher, dass Ihr die Einstellungen an Eure eigene Konfiguration anpasst, wie z. B. den Domain-Namen (`myradicale.gofoss.duckdns.org`), den Pfad zu SSL-Schlüsseln, die IP-Adressen und so weiter:

    ```bash
    <VirtualHost *:80>

    ServerName              myradicale.gofoss.duckdns.org
    ServerAlias             www.myradicale.gofoss.duckdns.org
    Redirect permanent /    https://myradicale.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

    ServerName              myradicale.gofoss.duckdns.org
    ServerAlias             www.myradicale.gofoss.duckdns.org
    ServerSignature         Off

    SecRuleEngine           Off
    SSLEngine               On
    SSLProxyEngine          On
    SSLProxyCheckPeerCN     Off
    SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
    SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem

    <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
    </Location>

    RewriteEngine On
    RewriteRule ^/radicale$ /radicale/ [R,L]

    <Location "/radicale/">
        AuthType      Basic
        AuthName      "Radicale - Password Required"
        AuthUserFile  "/var/www/radicale/users"
        Require       valid-user

        ProxyPass        http://localhost:5232/ retry=0
        ProxyPassReverse http://localhost:5232/
        RequestHeader    set X-Script-Name /radicale/
    RequestHeader    set X-Remote-User expr=%{REMOTE_USER}
    </Location>

    ErrorLog ${APACHE_LOG_DIR}/myradicale.gofoss.duckdns.org-error.log
    CustomLog ${APACHE_LOG_DIR}/myradicale.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Speichert und schließt die Datei (`:wq!`).

    Man beachte, dass die SSL-Verschlüsselung für Radicale mit der Anweisung `SSLEngine On` aktiviert und das zuvor erstellte SSL-Zertifikat `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` sowie der private SSL-Schlüssel `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem` verwendet wurden.

    Man beachte des Weiteren, dass ModSecurity in der Apache-Konfigurationsdatei mit der Anweisung `SecRuleEngine Off` deaktiviert wurde, da Radicale und ModSecurity nicht gut miteinander harmonieren.

    Aktiviert als nächstes den Apache Virtual Host und ladet Apache neu:

    ```bash
    sudo a2ensite myradicale.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Konfiguriert Pi-Hole, um die lokale Adresse von Radicale nutzen zu können. Ruft `https://mypihole.gofoss.duckdns.org` auf und meldet Euch über die Pi-Hole-Weboberfläche an (passt die URL entsprechend an). Öffnet den Menüeintrag `Locale DNS Records` und fügt die folgende Domain/IP-Kombination hinzu (passt die URL sowie IP entsprechend an):

    ```bash
    DOMAIN:      myradicale.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Ruft [https://myradicale.gofoss.duckdns.org](https://myfiles.gofoss.duckdns.org/) auf und meldet Euch an. Erstellt je nach Bedarf Kalender, Adressbücher und Aufgabenlisten.


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    In diesem Video richten wir die Radicale-Weboberfläche ein, erstellen ein Adressbuch und einen Kalender für Georg sowie eine Aufgabenliste für Lenina.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/11026915-b199-44f1-abd9-916167fc805b" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Radicale Clients" width="150px"></img> </center>

## Clients

Unter Android integriert sich [DAVx⁵](https://www.davx5.com/) nativ mit den Kalender-, Kontakt- und Aufgaben-Apps Eures Geräts, um diese mit Radicale zu synchronisieren. Und unter Windows, macOS oder Linux (Ubuntu) funktioniert Radicale gut mit den Desktop-Clients von Thunderbird. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Öffnet F-Droid und installiert [DAVx⁵](https://f-droid.org/de/packages/at.bitfire.davdroid/). Alternativ findet Ihr die App im [Aurora Store](https://auroraoss.com/de/) oder im [Play Store von Google](https://play.google.com/store/apps/details?id=at.bitfire.davdroid&hl=de&gl=DE).

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | DAVx⁵ starten |Öffnet die DAVx⁵-App und deaktiviert die Akkuoptimierung, sobald Ihr dazu aufgefordert werdet. |
        | Neues Konto erstellen | Tippt auf das `+`-Zeichen, um ein neues Konto zu erstellen. Wählt `Mit URL und Benutzername anmelden`. |
        | Basis-URL | Gebt die Adresse des Radicale-Servers an. In unserem Beispiel ist das [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/). Passt die URL entsprechend an. |
        | Nutzer & Kennwort | Gebt gültige Zugangsdaten ein. In unserem Beispiel sind gültige Benutzer Georg, Lenina oder Tom. |
        | Kontaktgruppen-Methode | Wählt im nächsten Dialogfenster `Gruppen sind eigene vCards`. |
        | Konto erstellen | Klickt auf `Konto erstellen`. |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        Eine ausführliche Installationsanleitung für Thunderbird findet Ihr im [vorherigen Kapitel über E-Mail-Clients](https://gofoss.net/de/encrypted-emails/#desktop-clients).  Öffnet Thunderbird und ruft den Menüeintrag `Tools ‣ Add-ons` auf. Installiert die Erweiterung [Cardbook](https://addons.thunderbird.net/de/thunderbird/addon/cardbook/). Startet abschließend Thunderbird neu.


<div style="margin-top:-20px">
</div>

??? warning "Clients benötigen einen VPN-Zugang"

    Clients müssen über [VPN](https://gofoss.net/de/secure-domain/) mit dem Server verbunden sein, um auf Radicale zugreifen zu können.



<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Radicale Kontakte importieren" width="150px"></img> </center>

## Kontakte importieren

Eure vorhandenen Kontaktdaten sind wahrscheinlich über mehrere Geräte und Dienste verstreut. Untenstehend findet Ihr eine ausführliche Anleitung, um Eure Kontakte in Radicale zu importieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Schritt 1 | Exportiert Eure vorhandenen Kontakte von Euren Geräten oder Cloud-Diensten (Google, Microsoft, Apple usw.) im vCard-Format (`.vcf`). |
    | Schritt 2 | [Legt eine Sicherungskopie Eurer `.vcf` Dateien an](https://gofoss.net/de/backups/). |
    | Schritt 3 | Übertragt die verschiedenen `.vcf`-Dateien auf Euer Android-Telefon. |
    | Schritt 4 | Öffnet die Standard-Kontakt-App auf Eurem Telefon. Solltet Ihr eine andere Kontakt-App verwenden, weichen die nachstehenden Anweisungen möglicherweise ab. |
    | Schritt 5 | Ruft den Menüeintrag `Einstellungen ‣ Importieren` auf. |
    | Schritt 6 | Wählt den Menüeintrag `VCF-Datei`. |
    | Schritt 7 | Sucht den Speicherort der `.vcf`-Dateien und wählt eine Datei aus. |
    | Schritt 8 | Tippt auf `Importierte Kontakte speichern unter: DAVx⁵-Adressbuch` und wählt das entsprechende Radicale-Adressbuch. |
    | Schritt 9 | Wiederholt diesen Vorgang, bis alle Kontakte importiert wurden. |

    </center>


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Radicale Kontakte synchronisieren" width="150px"></img> </center>

## Kontakte synchronisieren

Bei der Kontakt-Synchronisierung werden sämtliche Änderungen zwischen den miteinander verbundenen Android-, Windows-, macOS- oder Linux (Ubuntu)-Geräten und dem Radicale-Server gespiegelt. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Schritt 1 | Öffnet auf Eurem Android-Telefon den DAVx⁵-Hauptbildschirm und wählt ein Konto aus. |
        | Schritt 2 | Ruft den `CardDAV` Menüeintrag auf, und wählt die Radicale-Adressbücher aus, die mit dem Telefon synchronisiert werden sollen. |
        | Schritt 3 | Klickt auf die Synchronisation-Taste. |
        | Schritt 4 | Überprüft, ob alles funktioniert hat. Öffnet die Kontakte-App Eures Telefons und wählt den Menüeintrag `Einstellungen ‣ Konten`. Hier sollten nun die Radicale-Adressbücher angezeigt werden. |

        </center>

=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Schritt 1 | Meldet Euch bei [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) an (passt die URL entsprechend an). |
        | Schritt 2 | Wählt das Adressbuch aus, das Ihr synchronisieren möchtet, und kopiert dessen URL-Link. |
        | Schritt 3 | Öffnet Thunderbird und wählt den Menüeintrag `Cardbook ‣ Adressbuch ‣ Neues Adressbuch`. |
        | Schritt 4 | Folgt dem Einrichtungsassistenten: <br>• `Ort des neuen Adressbuchs`: `Im Netzwerk` <br>• `Art`: `CardDAV`<br>• `URL`: `Fügt den URL-Link des Radicale-Adressbuchs ein, den Ihr zuvor kopiert habt`<br>• `Benutzername & Passwort`: `Gebt gültige Radicale-Zugangsdaten an`<br>• `Eigenschaften`: `Name, Farbe, Offline-Verfügbarkeit des Adressbuchs, usw.`|
        | Schritt 5 | Überprüft, ob alles funktioniert hat. Das Radicale-Adressbuch sollte nun in Thunderbirds `CardBook`-Reiter erscheinen. |

        </center>


<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Radicale Kalender importieren" width="150px"></img> </center>

## Kalender importieren

Eure vorhandenen Kalender sind wahrscheinlich über mehrere Geräte und Dienste verstreut. Untenstehend findet Ihr eine ausführliche Anleitung, um Eure Kalender in Radicale zu importieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Schritt 1 | Exportiert Eure vorhandenen Kalender aus Euren Geräten oder Cloud-Diensten (Google, Microsoft, Apple usw.) im iCalendar-Format (`.ics`).|
    | Schritt 2 | [Legt eine Sicherungskopie Eurer `.ics` Dateien an](https://gofoss.net/de/backups/). |
    | Schritt 3 | Übertragt die verschiedenen `.ics`-Dateien auf Euer Android-Telefon. |
    | Schritt 4 | Öffnet F-Droid auf Eurem Telefon und installiert die App [Calendar Import-Export](https://f-droid.org/de/packages/org.sufficientlysecure.ical/). Alternativ findet Ihr die App auf [Googles Play Store](https://play.google.com/store/apps/details?id=org.sufficientlysecure.ical&hl=de&gl=DE). |
    | Schritt 5 | Öffnet die Calendar Import-Export App und klickt auf `Dateien importieren`. |
    | Schritt 6 | Sucht den Speicherort der `.ics`-Dateien und wählt eine Datei aus. |
    | Schritt 7 | Importiert die `.ics`-Datei in DAVx⁵ und wählt den entsprechenden Radicale-Kalender. |
    | Schritt 8 | Wiederholt diesen Vorgang, bis alle Kalender importiert wurden. |

    </center>



<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Radicale Kalender synchronisieren" width="150px"></img> </center>

## Kalender synchronisieren

Bei der Kalender-Synchronisierung werden sämtliche Änderungen zwischen den miteinander verbundenen Android-, Windows-, macOS- oder Linux (Ubuntu)-Geräten und dem Radicale-Server gespiegelt. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Schritt 1 | Öffnet auf Eurem Android-Telefon den DAVx⁵-Hauptbildschirm und wählt ein Konto aus. |
        | Schritt 2 | Ruft den `CardDAV` Menüeintrag auf, und wählt die Radicale-Kalender aus, die mit dem Telefon synchronisiert werden sollen. |
        | Schritt 3 | Klickt auf die Synchronisation-Taste. |
        | Schritt 4 | Installiert und öffnet die [Simple Calendar app](https://f-droid.org/de/packages/com.simplemobiletools.calendar.pro/) auf Eurem Telefon. Solltet Ihr eine andere Kalender-App verwenden, weichen die nachstehenden Anweisungen möglicherweise ab. |
        | Schritt 5 | Wählt den Menüeintrag `Einstellungen` und stellt sicher, dass die Option `CalDAV-Synchronisierung` aktiv ist. |
        | Schritt 6 | Wählt den Menüeintrag `Synchronisierte Kalender verwalten`. Die Radicale-Kalender sollten nun hier auftauchen. |
        | Schritt 7 | Wählt die Radicale-Kalender und klickt auf `OK`.|

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Schritt 1 | Meldet Euch bei [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) an (passt die URL entsprechend an). |
        | Schritt 2 | Wählt den Kalender aus, den Ihr synchronisieren möchtet, und kopiert dessen URL-Link. |
        | Schritt 3 | Öffnet Thunderbird und wählt den Menüeintrag `Neu ‣ Kalender`. |
        | Schritt 4 | Folgt dem Einrichtungsassistenten: <br>• `Ort Ihres Kalenders feststellen`: `Im Netzwerk` <br>• `Format`: `CalDAV` (wählt *nicht* `iCalendar (ICS)`!) <br>• `Benutzername`: `Gebt einen gültigen Radicale-Benutzernamen an`<br>• `Adresse`: `Fügt den URL-Link des Radicale-Kalenders ein, den Ihr zuvor kopiert habt`<br>• `Offline-Unterstützung`: `Markiert das Kästchen`<br>• `Eigenschaften`: `Legt den Namen und die Farbe des Kalenders fest, aktiviert Erinnerungen, ordnet eine E-Mail-Adresse zu um Einladungen zu verwalten, usw.`|
        | Schritt 5 | Überprüft, ob alles funktioniert hat. Der Radicale-Kalender sollte nun in Thunderbirds `Kalender`-Reiter erscheinen. |

        </center>



<br>

<center> <img src="../../assets/img/separator_tasks.svg" alt="Radicale Aufgaben synchronisieren" width="150px"></img> </center>

## Aufgaben synchronisieren

Bei der Aufgaben-Synchronisierung werden sämtliche Änderungen zwischen den miteinander verbundenen Android-, Windows-, macOS- oder Linux (Ubuntu)-Geräten und dem Radicale-Server gespiegelt. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Schritt 1 | Öffnet auf Eurem Android-Telefon den DAVx⁵-Hauptbildschirm und wählt ein Konto aus. |
        | Schritt 2 | Ruft den `CalDAV` Menüeintrag auf, und wählt die Radicale-Aufgabenlisten aus, die mit dem Telefon synchronisiert werden sollen. |
        | Schritt 3 | Klickt auf die Synchronisation-Taste. |
        | Schritt 4 | Installiert und öffnet die [Tasks.org App](https://f-droid.org/de/packages/org.tasks/) auf Eurem Telefon. Solltet Ihr eine andere Kalender-App verwenden, weichen die nachstehenden Anweisungen möglicherweise ab. |
        | Schritt 5 | Wählt den Menüeintrag `Einstellungen`. Die Radicale-Aufgabenlisten sollten nun hier auftauchen. |
        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | Schritt 1 | Meldet Euch bei [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) an (passt die URL entsprechend an). |
        | Schritt 2 | Wählt die Aufgabenliste aus, die Ihr synchronisieren möchtet, und kopiert den dazugehörigen URL-Link. |
        | Schritt 3 | Öffnet Thunderbird und wählt den Menüeintrag `Neu ‣ Kalender`. |
        | Schritt 4 | Folgt dem Einrichtungsassistenten: <br>• `Ort Ihres Kalenders feststellen`: `Im Netzwerk` <br>• `Format`: `CalDAV` (wählt *nicht* `iCalendar (ICS)`!)<br>• `Benutzername`: `Gebt einen gültigen Radicale-Benutzernamen an`<br>• `Adresse`: `Fügt den URL-Link der Radicale-Aufgabenliste ein, den Ihr zuvor kopiert habt`<br>• `Offline-Unterstützung`: `Markiert das Kästchen`<br>• `Eigenschaften`: `Legt den Namen und die Farbe der Aufgabenliste fest und aktiviert Erinnerungen. Eine E-Mail-Adresse ist nicht notwendig.` |
        | Schritt 5 | Überprüft, ob alles funktioniert hat. Die Radicale-Aufgabenliste sollte nun in Thunderbirds `Kalender`-Reiter erscheinen. |

        </center>



<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Radicale Datensicherung" width="150px"></img> </center>

## Datensicherung

[Führt regelmäßig Datensicherungen durch](https://gofoss.net/de/backups/), um Datenverluste zu vermeiden! Obwohl Radicale sehr gut funktioniert, sind Fehler nie vollständig auszuschließen. Kontakte, Kalender und Aufgabenlisten bleiben synchronisiert, solange Eure Geräte über VPN mit dem Server verbunden sind. Aber manchmal kann es zu Synchronisationsfehlern kommen, z.B. wenn Änderungen nicht richtig gespeichert werden oder die Verbindung zu den Geräten unterbrochen wird. Es kann auch vorkommen, dass BenutzerInnen versehentlich Informationen löschen, und sobald dies auf den Server und alle Geräte gespiegelt wurde, sind die Daten für immer verloren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Um manuelle Datensicherungen durchzuführen, meldet Euch bei [https://myradicale.gofoss.duckdns.org/radicale](https://myradicale.gofoss.duckdns.org/radicale/) an (passt die URL entsprechend an) und klickt auf die URL-Links von Adressbüchern, Kalendern und Aufgabenlisten, um Eure Daten herunterzuladen.

    Ihr könnt ebenfalls [automatische Sicherungen](https://gofoss.net/de/server-backups/) der Verzeichnisse `/var/lib/radicale`, `/var/www/radicale` und `/etc/radicale` durchführen.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Radicale Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Informationen finden Sie in der Dokumentation zu [Radicale](https://radicale.org/), [DAVx⁵](https://www.davx5.com/manual/) oder [Thunderbird](https://support.mozilla.org/de/products/thunderbird).


<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/time_management.png" width="550px" alt="Radicale"></img>
</div>

<br>
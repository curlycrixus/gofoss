---
template: main.html
title: Home Server | How To Install Ubuntu Server
description: Ubuntu Desktop vs Server? Follow this home server setup guide to learn how to install Ubuntu Server, use a static ip address, SSH remote login, etc.
---

# Set Up Your Very Own Ubuntu Home Server <br> (A Step-by-Step Guide)

!!! level "Last updated: May 2022. For advanced users. Solid tech skills required."

<div align="center">
<img src="../../assets/img/server_2.png" alt="Ubuntu server vs desktop" width="700px"></img>
</div>

In this chapter, we're going to explain how to install an Ubuntu server on an unused computer or cheap hardware at home. What is this Ubuntu Server used for? It can be used to host a variety of services, including your [cloud-storage](https://gofoss.net/cloud-storage/), [pictures](https://gofoss.net/photo-gallery/), [contacts, calendars and tasks](https://gofoss.net/contacts-calendars-tasks/) or [movies and TV series](https://gofoss.net/media-streaming/).

Three additional chapters present measures to protect your home server from most immediate threats:

* [security basics](https://gofoss.net/server-hardening-basics/) like firewalls, remote access and updates
* [advanced techniques](https://gofoss.net/server-hardening-advanced/) including network security, antivirus, permissions and so on
* [backup instructions](https://gofoss.net/server-backups/) to perform scheduled backups of your data


??? question "Should I host my home server or rent a VPS?"

    Both options have pros and cons. It's a trade-off between privacy, security and convenience. Although this guide explains how to host your server at home, you can also rent a [virtual private server (VPS)](https://en.wikipedia.org/wiki/Virtual_private_server) from providers such as [Hetzner](https://www.hetzner.com/cloud/), [Digital Ocean](https://www.digitalocean.com/pricing/), [OVH](https://www.ovh.com/world/vps/), [Contabo](https://contabo.com/en/), [Scaleway](https://www.scaleway.com/en/virtual-instances/development/) and others.

    <center>

    | | Home server | VPS |
    | ------ | :------: | :------: |
    | Better privacy | <span style="color:green">✔</span> |  |
    | Lower upfront costs |  | <span style="color:green">✔</span> |
    | Lower recurring costs | <span style="color:green">✔</span> | |
    | Easier to setup |  | <span style="color:green">✔</span> |
    | Easier to maintain | | <span style="color:green">✔</span> |
    | Lower security risks | | <span style="color:green">✔</span> |
    | Lower risk of data loss | | <span style="color:green">✔</span> |
    | Lower downtime risk | | <span style="color:green">✔</span> |
    | Fewer latency issues | <span style="color:green">✔</span> |  |
    | Fewer bandwidth limitations | <span style="color:green">✔</span> | |

    </center>


??? question "What are the minimum hardware requirements?"

    It will depend on the planned usage. How many users or visits do you expect per day? Do you plan on streaming video or storing large amounts of data? The following minimum specs should be enough for a handful of services/users:

    * One or more 2 GHz processors
    * 2 to 4 GB RAM
    * 10 to 25 GB storage (or more for large file collections)
    * Fast Internet connection

    [Here a list of mini PCs](https://forums.servethehome.com/index.php?threads/tiny-mini-micro-pc-experiences.30230/), which are well suited as servers due to their small form factor and low price range.


??? question "How can I edit files within the terminal?"

    Interacting with the server requires quite heavy usage of the terminal. Configuration files and settings often need to be modified with a text editor. The [vi](https://en.wikipedia.org/wiki/Vi) editor is commonly used and has three modes:

    * **Normal mode**: vi starts in normal mode, which allows to navigate through documents

    * **Insert mode**: by hitting the `i` key, vi switches to insert mode, which allows to edit and modify documents

    * **Command mode**: by hitting the colon key `:`, vi switches to command mode, which allows for example to save changes or find text within the document

    The following commands should be sufficient to follow all instructions provided on this website:

    <center>

    |Command |Description |
    | ----- | ----- |
    | `vi file.txt` | Open the document "file.txt" with the vi text editor. |
    | `i` | Switch to "Insert mode" and start modifying the file. |
    | `ESC` | Exit the "Insert mode" once modifications are done. |
    | `:w` | Save all changes made to the file, while keeping it open. |
    | `:wq` or `:x` | Save all changes made to the file, and exit vi. |
    | `:q!` | Exit vi, without saving any changes made to the file. |
    | `/findword` | Search for the word "findword" (replace with the actual word you're looking for). |
    | `n` | After starting the search for "findword", find the next occurrence of "findword" in the file. |

    </center>

    Numerous tutorials exist to dive deeper in the world of vi:

    * [Cheat sheet from Santa Clara University](https://speicher.systemausfall.org/f/265c5adc9a7b4ddba7fd/)
    * [Cheat sheet from WorldTimZone](https://speicher.systemausfall.org/f/2efedff727764690bda4/)
    * [Cheat sheet from Daniel Gryniewicz](https://speicher.systemausfall.org/f/7be6da8705c9443288be/)


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Install Ubuntu server from USB" width="150px"></img> </center>

## How to install Ubuntu Server

Perform some preliminary checks and follow the installation instructions below for your home server setup.

??? tip "Show me the list of preliminary checks"

    <center>

    | Checklist | Description |
    | ------ | ------ |
    | Is my hardware compatible with Linux? |• [Test it](https://gofoss.net/ubuntu/#ubuntu-live-usb-virtualbox) with a Live USB or VirtualBox  <br>• Check the [compatibility database](https://ubuntu.com/certified) <br>• [Ask](https://search.disroot.org/) the Internet <br>• Buy a Linux compatible computer, e.g. at [Linux Preloaded](https://linuxpreloaded.com/) or the [Ministry of Freedom](https://minifree.org/) |
    | Does my hardware fulfill the minimum requirements? |• 2 GHz dual core processor <br>• 4 GB system memory (RAM) <br>• 25 GB of free storage space |
    | Is my hardware plugged in? | If you install Ubuntu Server 22.04 on a laptop, make sure it is plugged in. |
    | Is the installation medium accessible? | Check if your hardware has either a DVD drive or a free USB port. |
    | Is my hardware connected to the internet? | Check if the Internet connection is up and running. |
    | Have I backed up my data? | [Back up your data](https://gofoss.net/backups/), since everything stored on the hardware will be erased during the installation process! |
    | Have I downloaded the latest Ubuntu Server version? | Download the [latest long-term support (LTS) version of Ubuntu Server](https://ubuntu.com/download/server), which is supported for 5 years, including security and maintenance updates. At the time of writing, the latest LTS was Ubuntu Server 22.04. Check out the [most recent release cycle](https://ubuntu.com/about/release-cycle) for more information. |
    | Have I prepared a bootable device? | Use a [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) or [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) machine to burn the downloaded `.iso` file to a DVD. Alternatively, use a [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) or [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) machine to create a bootable USB drive. |

    </center>


??? tip "Show me the step-by-step guide"

    <center>

    | Instruction | Description |
    | ------ | ------ |
    |Boot |Insert the bootable DVD or USB drive and power on the hardware. Most machines will boot automatically from the DVD or USB drive. If that's not the case, try repeatedly hitting `F12`, `ESC`, `F2` or `F10` when the hardware starts up. This should provide access to the boot menu, where you can select the DVD or USB drive. |
    |GRUB |Select the menu entry `Try or Install Ubuntu Server`. |
    |Welcome |Select a language. |
    |Installer update |This screen appears if the server is already connected to the Internet. Select the menu entry `Continue without updating`. |
    |Keyboard layout |Select a keyboard layout. |
    |Type of installation |Select the default Ubuntu Server install option. |
    |Network connections |Make sure to set up a working Internet connection via Ethernet or WiFi. This is important for the rest of the process. Select `Info` to find out more about a given network. |
    |Proxy |These settings are optional. Provide the required information if you need to set up network proxies, else just skip this step. |
    |Ubuntu archive |Choose the Ubuntu repository. Default options should work fine. |
    |Storage configuration |Choose how to partition your disk. In this tutorial, we will use the entire disk: <br>• Select `Set up this disk as an LVM group` <br>• Select `Encrypt the LVM group with LUKS` to encrypt your server <br>• Provide a [strong, unique security key](https://gofoss.net/passwords/) and confirm <br><br> *Caution*: This step will erase all data and format the hard drive! Carefully review all settings! Make sure you [backed up your data](https://gofoss.net/backups/)! |
    |Profile setup |Setup the user and server profile. For the purpose of this tutorial, we'll choose the following configuration (adapt accordingly): <br>• Your name (this is the name of the system root user): `gofossroot` <br>• Your server's name (this is the name of your server): `gofossserver` <br>• Pick a username (same name as the system root user): `gofossroot` <br>• Password: provide a [strong, unique password](https://gofoss.net/passwords/) and confirm |
    |SSH setup |These settings are optional. Just leave this section blank and continue. |
    |Featured Server Snaps |These settings are optional. Just leave this section blank and continue. |
    |Wrap up |Wait for the installation to finish. Once Ubuntu Server is installed, remove the installation medium and press `ENTER` when prompted. |

    </center>

??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7f4a0a65-7c82-4bd1-9f14-2ff5ced911c6" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Linux get ip address" width="150px"></img> </center>

## What is LAMP Stack?

The acronym [LAMP](https://en.wikipedia.org/wiki/LAMP_%28software_bundle%29) stands for [Linux](https://en.wikipedia.org/wiki/Ubuntu) (the operating system), [Apache](https://en.wikipedia.org/wiki/Apache_HTTP_Server) (the server software), [MySQL](https://en.wikipedia.org/wiki/MySQL)(the database management system) and [PHP](https://en.wikipedia.org/wiki/PHP)/[Perl](https://en.wikipedia.org/wiki/Perl)/[Python](https://en.wikipedia.org/wiki/Python_(programming_language)) (the programming languages).

After the reboot, log into the Ubuntu Server. Provide the security key to decrypt the server partition, and enter the root username and password. Then follow the instructions below to install the LAMP stack.

??? tip "Show me the step-by-step guide"

    Update system packages and install the LAMP stack:

    ```bash
    sudo apt update
    sudo apt upgrade
    sudo apt install lamp-server^
    ```

    Mind the trailing caret in the command `sudo apt install lamp-server^`. It indicates that `lamp-server` is a meta package, which installs Apache, MySQL, and PHP along with other packages and dependencies.

    Verify that Apache has been successfully installed (the latest version number should appear), automatically start Apache after each reboot, and check Apache's current status (which should be `Active`):

    ```bash
    apachectl -V
    sudo systemctl enable apache2
    sudo systemctl status apache2
    ```

    Same procedure for MySQL:

    ```bash
    mysql -V
    sudo systemctl enable mysql
    sudo systemctl status mysql
    ```

    Make sure PHP has been successfully installed (the latest version number should appear):

    ```bash
    php -v
    ```

    Finally, some clean up:

    ```bash
    sudo apt autoremove && sudo apt clean
    ```

??? tip "Show me the 2-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/d9c1baa7-68ec-47c7-83dd-c338c5e55751" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ip.svg" alt="How to set static ip" width="150px"></img> </center>

## Set static IP address

It's recommended to assign a static IP address to the server. For the purpose of this tutorial, we are going to assign the static IP address `192.168.1.100`. Of course, any other suitable address will do. Just make sure to adapt the relevant commands accordingly in the instructions provided below.

??? tip "Show me the step-by-step guide"

    ### Network interface

    First, let's find out the name of the server's network interface:

    ```bash
    ip link show
    ```

    A list of all network interfaces should be displayed:

    * The first entry is most likely labelled `lo`, the loopback interface
    * The Ethernet interface should be labelled something like `enpXsY`, for example `enp0s1` or `enp1s3`
    * The WiFi interface should be labelled something like `wlpXsY`, for example `wlp0s1` or `wlp2s3`

    For the purpose of this tutorial, let's assume that the server's network interface is labelled `enp0s3` (adjust accordingly).

    ### Default gateway

    Time to find out the name of the default gateway:

    ```bash
    ip route show
    ```

    The terminal should display a line containing the IP address of the default gateway, something similar to `default via 192.168.1.1 dev enp0s3 proto dhcp`. For the purpose of this tutorial, let's assume that the address of the standard gateway is `192.168.1.1` (adjust accordingly).

    ### Backup

    Back up the current network configuration file. In case something goes wrong, you can always restore the default configuration:

    ```bash
    sudo cp --archive /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    ### Network configuration

    Open the network configuration file:

    ```bash
    sudo vi /etc/netplan/00-installer-config.yaml
    ```

    The file's content should look something like:

    ```bash
    network:
      ethernets:
        enp0s3:
          dhcp4: true
      version: 2
    ```

    The entry `dhcp4: true` tells us that the server is assigned a dynamic IP address. Modify the file's content:

    * specify the correct network interface: in this case `enp0s3` (adjust accordingly)
    * attribute a static IP address to the server: in this case `192.168.1.100` (adjust accordingly)
    * specify the default gateway: in this case `192.168.1.1`  (adjust accordingly)
    * select a DNS provider: in this case [UncensoredDNS](https://blog.uncensoreddns.org/) (select the DNS provider of your choice)

    Pay attention to the correct indentation of each line in the network configuration file. Also, make sure to adjust all settings according to your own setup. For example:

    * your Ethernet interface might be called `enp0s1` instead of `enp0s3`
    * or you might use a WiFi interface, let's say `wlp0s1`: in this case you need to replace the line `ethernets:` with `wifis` and the line `enp0s3` with `wlp0s1`
    * your default gateway might be labelled `192.168.0.1` instead of `192.168.1.1`
    * you might also want to choose another static IP address or DNS provider

    ```bash
    network:
      ethernets:
        enp0s3:
          dhcp4: false
          addresses: [192.168.1.100/24]
          routes:
            - to: default
              via: 192.168.1.1
          nameservers:
            addresses: [89.233.43.71, 91.239.100.100]
      version: 2
    ```

    Save and close the file by hitting `ESC` and typing `:wq`. Finally, apply and verify all changes:

    ```bash
    sudo netplan apply
    ip add
    ```


??? tip "Show me the 2-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ef1b78f7-bab2-4ace-8de0-ddb1c9715338" frameborder="0" allowfullscreen></iframe>
    </center>

??? question "Static IP vs Dynamic IP?"

    What is a static IP address? By default, the so-called [DHCP protocol](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) assigns a unique IP address to each device in your home network, including your server. After each reboot or network connection loss, there is a chance that your server is attributed with a new IP address. Which in turn would require re-configuring various settings. This can be avoided by assigning a static IP address to your server.

    Which static IP address you can assign to the server depends on the network configuration. Verify the settings of your other devices or router to find out which IP addresses are used throughout your home network. Commonly used IP ranges are `192.168.0.xx` or `192.168.1.xx`.

    There are two ways to assign a static IP address to the server:

    * **Network configuration**: directly change the network configuration on the server, as shown above.

    * **DHCP reservation**: alternatively, configure a so-called DHCP reservation on the router, which will always assign the same IP address to the server. Refer to the router's manual for more information.


??? question "Are there other privacy respecting DNS providers?"

    <center>

    | DNS provider | Country | DNS #1 | DNS #2 |Privacy Policy |
    | ------ | ------ | ------  |------ |------ |
    | [Digitalcourage](https://digitalcourage.de/en) | Germany | 5.9.164.112 | -- |[Privacy Policy](https://digitalcourage.de/datenschutz-bei-digitalcourage) |
    | [Dismail](https://dismail.de/info.html#dns/) | Germany |80.241.218.68 | 159.69.114.157 |[Privacy Policy](https://dismail.de/datenschutz.html#dns) |
    | [DNS Watch](https://dns.watch/) | Germany | 84.200.69.80 | 84.200.70.40 | -- |
    | [FDN](https://www.fdn.fr/actions/dns/) | France | 80.67.169.12 | 80.67.169.40 | -- |
    | [OpenNIC](https://servers.opennic.org/) | Various | Various | Various | Various |

    </center>



<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="What is static IP" width="150px"></img> </center>

## SSH remote login

It's possible to remotely connect to the *server* from another computer, called the *client*. This way, the server can remain *headless* — no need to hook it up to a screen, keyboard, mouse or any other peripheral. The remote connection is protected by [Secure Shell (SSH)](https://en.wikipedia.org/wiki/Secure_Shell), which provides strong authentication and encryption. Here is how it works in a nutshell, more detailed instructions below.

<div align="center">
<img src="../../assets/img/ssh_connection.png" alt="Ubuntu set static IP" width="500px"></img>
</div>

1. The *client* initiates the connection to the *server*
2. The *server* sends its public key to the *client*
3. The *server's* public key is saved in the *client’s* known hosts file
4. The *client* and the *server* agree upon the encryption to be used for their communication and establish the connection


??? tip "Show me the step-by-step guide"

    ### Server setup

    Let's start with the *server*. Run the following commands to install OpenSSH, to automatically start it after each boot, and to verify its current status (which should be `Active`):

    ```bash
    sudo apt install openssh-server
    sudo systemctl enable ssh
    sudo systemctl status ssh
    ```

    Create an administrator user with privileges to remotely connect to the server. For the purpose of this tutorial, we'll call this user `gofossadmin`. Any other name will do, just make sure to adjust the relevant commands accordingly. When prompted, provide a [strong, unique password](https://gofoss.net/passwords/):

    ```bash
    sudo adduser gofossadmin
    sudo usermod -a -G sudo gofossadmin
    ```

    Switch to the new account and test its `sudo` powers by updating the system:

    ```bash
    su - gofossadmin
    sudo apt update
    ```

    ### Client setup

    Now, let's set up the *client*. For the purpose of this tutorial, we assume the client machine runs a GNU/Linux distribution, for example Ubuntu. If your client runs Windows or macOS, use available system tools or install a SSH client such as [PuTTY](https://www.putty.org/).

    Open a terminal on the Linux client machine with the `CTRL + ALT + T` shortcut, or click on the `Activities` button on the top left and search for `Terminal`. Now create the same administrator user `gofossadmin` as on the server (adjust the name according to your own setup). Provide a [strong, unique password](https://gofoss.net/passwords/) when prompted:

    ```bash
    sudo adduser gofossadmin
    ```

    Log into the new administrator account, create a hidden directory where SSL keys will be stored and provide this directory with the right permissions:

    ```bash
    su - gofossadmin
    mkdir /home/gofossadmin/.ssh
    chmod 755 /home/gofossadmin/.ssh
    ```

    Generate a public/private key pair which secures the remote login to the server. Run below commands, follow the on-screen instructions and, when prompted, provide a [strong, unique passphrase](https://gofoss.net/passwords/):

    ```bash
    cd /home/gofossadmin/.ssh
    ssh-keygen -t RSA -b 4096
    ```

    The output should look something like this:

    ```bash
    Generating public/private RSA key pair.
    Enter file in which to save the key (/home/gofossadmin/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase): *******
    Enter same passphrase again: ********
    Your identification has been saved in /home/gofossadmin/.ssh/id_rsa.
    Your public key has been saved in /home/gofossadmin/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:erdBxn9i/ORLIJ4596DvfUIIPOyfFnSMQ4SieLIbSuxI gofossadmin@gofossclient
    The key's randomart image is:
    +---[RSA 4096]----+
    |   .+.+=     o   |
    |       ..oo.+ o  |
    | .S+..     %..   |
    |         o+o. o  |
    |        So*o o = |
    |   ..oo.+     A +|
    | ..+ooo       .oo|
    |  .o...+  .E..   |
    |         .. o==.%|
    +----[SHA256]-----+
    ```

    Transfer the public key from the client to the server. Make sure to replace the administrator name and IP address according to your own setup:

    ```bash
    ssh-copy-id gofossadmin@192.168.1.100
    ```

    That's it! From now on you can remotely log into the server from the client machine. Just open a terminal, switch to the administrator account and connect to the server using your passphrase. Don't forget to adjust the administrator name and IP address as required:

    ```bash
    su - gofossadmin
    ssh gofossadmin@192.168.1.100
    ```

??? tip "Show me the 3-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/f201ed55-5b1c-49b8-9698-73940c05aab2" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu static IP" width="150px"></img> </center>

## Support

For further details or questions on Ubuntu Server, refer to [Ubuntu's documentation](https://help.ubuntu.com/), [Ubuntu's tutorials](https://ubuntu.com/tutorials), [Ubuntu's wiki](https://wiki.ubuntu.com/) or ask [Ubuntu's community](https://askubuntu.com/) for help.

<br>

---
template: main.html
title: Applis FOSS sur un smartphone dégooglelé
description: Dégoogleler. Qu'est-ce que LineageOS? Qu'est-ce que GrapheneOS? Qu'est-ce que microG? Comment installer CalyxOS? Qu'est-ce que F-Droid?
---

# Libérez votre smartphone de Google et Apple

Dans ce chapitre, nous allons voir comment:

* privilégier les [applications pour mobile libres et open source](https://gofoss.net/fr/foss-apps/), sans traqueurs et avec des permissions réduites au minimum
* libérer votre smartphone de Google et Apple, en utilisant des systèmes d'exploitation mobiles tels que [CalyxOS](https://gofoss.net/fr/calyxos/) ou [LineageOS](https://gofoss.net/fr/lineageos/)


## Les applis sans traqueurs

<center>
<html>
<embed src="../../assets/echarts/app_stats.html" style="width: 100%; height:520px">
</html>
</center>

Les applications mobiles de Google, Facebook, Samsung ou Microsoft sont téléchargées des milliards de fois. Souvent elles sont déjà pré-installées sans l'accord explicite de l'utilisateur.

Cela pose de sérieuses questions sur la vie privée. Nombre de ces applis demandent l'accès à votre localisation, microphone, appareil photo, contacts etc. Elles contiennent également des traqueurs pour collecter des informations sur vous. En moyenne,  les [applis Android du top 50](https://en.wikipedia.org/wiki/List_of_most-downloaded_Google_Play_applications) (plus de 100 milliards de téléchargements) contiennent **2 à 3 traqueurs** et demandent **36 autorisations**, comme illustré ci-dessus.


??? info "Dites m'en plus sur les traqueurs"

    Un traqueur est un morceau de logiciel qui rassemble de l'information sur comment les applis et les smartphones sont utilisés. Il peut s'agir de divers aspects, comme par exemple la possibilité de vous localiser, de scanner vos contacts, d'accéder à vos numéros de carte de crédit ou de lire le contenu de votre presse-papiers. Vérifiez bien les traqueurs et les autorisations quand vous installez une appli, par exemple en utilisant [εxodus](https://reports.exodus-privacy.eu.org/fr/).

    <center>

    | Type de traqueur | Description |
    | ------ | ------ |
    | Analytique | Collecte des données, par exemple quels sites vous visitez, pour combien de temps, quelle partie du site, etc. |
    | Profilage | Construit un profil virtuel en regardant votre historique de navigation, les applis installées, etc. |
    | Identification | Détermine qui vous êtes en se reportant à votre nom ou pseudonymes, localisation, etc. |
    | Publicitaire | Identifie qui utilise votre appareil pour envoyer des pubs ciblées. |
    | Localisation | Détermine votre position en regardant GPS, tours de télécommunications, réseaux WiFi alentours, etc. |
    | Rapports d'incident | Informe les développeurs si l'appli a eu un problème. |

    </center>

<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Dégoogler votre smartphone" width="150px"></img> </center>

## Smartphones sans Google et Apple

<center>
<html>
<embed src="../../assets/echarts/mobileos_stats.html" style="width: 100%; height:520px">
</html>
</center>

En 2020, la planète a compté [3,5 milliards d'utilisateurs de smartphone](https://www.statista.com/statistics/330695/number-of-smartphone-users-worldwide/). C'est presque la moitié de la population mondiale. Trois quarts de ces téléphones faisaient tourner Android de Google, et le reste iOS d'Apple. Et [3,3 milliards de personnes](https://www.statista.com/statistics/264810/number-of-monthly-active-facebook-users-worldwide/) utilisaient au moins un des produits phares de Facebook — Facebook, WhatsApp, Instagram ou Messenger.

La vie privée n'a jamais été au cœur des préoccupations de ces entreprises. Bien au contraire : les smartphones "partagent" constamment des données privées avec Google, Facebook et Apple, qui sont ensuite vendues à une armée de marketeurs et de revendeurs de données: agences de publicité, forces de l'ordre, comités d'action politique, institutions financières, compagnies d'assurance, etc. Les smartphones Android par exemple [envoient 12 megabytes de données à Google chaque jour](https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf). Même au repos, ils communiquent leur position à Google 14 fois par heure. De façon similaire, les iPhones envoient 6 mégabytes de données à Google et 1 mégabyte à Apple, chaque jour.


??? question "Je veux une confidentialité renforcée sur mon téléphone — quelles sont mes options ?"

    <center>

    | Option | Description |
    | ------ | ------ |
    |1. Jetez votre téléphone | Les téléphones n'ont pas été construits en prenant en compte la confidentialité. Si vous faites vraiment attention à votre privée, vous ne devriez pas avoir sur vous un téléphone mobile. Pour la plupart d'entre-nous, cette option est cependant trop radicale. |
    |2. Utilisez un OS mobile FOSS | L'option suivante serait de trouver un téléphone avec des logiciels entièrement open-source (et si possible de même pour le matériel). Il y a quelques projets en cours comme [Librem 5](https://puri.sm/products/librem-5/), [Pinephone](https://www.pine64.org/pinephone/), [Postmarket OS](https://postmarketos.org/), [Ubuntu Touch](https://ubuntu-touch.io/fr/) ou [Sailfish OS](https://sailfishos.org/) (ce dernier n'étant pas entièrement FOSS). Encore une fois, ces solutions à la pointe de la technologie ne sont pas pour tout le monde. |
    |3. Libérez Android de Google | En principe, Android est open-source. Se débarrasser des applications Google et des logiciels propriétaires est le meilleur compromis pour l'instant. Cela peut par exemple se faire en passant à CalyxOS ou LineageOS pour MicroG. |

    </center>

??? question "Devrais-je choisir LineageOS pour microG, ou CalyxOS, ou quelque chose d'autre ?"

    Cela dépend du modèle de votre téléphone, et aussi de votre [modèle de menace](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf). LineageOS pour microG est compatible avec de nombreux modèles de téléphones, aux dépens de la sécurité améliorée. CalyxOS est plus sécurisé, mais compatible avec moins de téléphones. Voici une vue générale des deux systèmes d'exploitations mobiles :

    <center>

    | Fonctions| LineageOS pour microG | CalyxOS |
    | ------ | ------ | ------ |
    |Sans Google^(1)^ |99% |99% |
    |Ergonomie^(2)^ |Bonne|Bonne|
    |Performance^(3)^ |Bonne|Bonne|
    |Mises à jour de sécurité automatiques^(4)^ |Limitées |Oui |
    |Démarrage sécurisé^(5)^ |Non |Oui |
    |Usurpation de signature ^(6)^ |Oui |Oui (si vous utilisez microG) |
    |Appareils supportés |[Disponible pour des centaines d'appareils](https://wiki.lineageos.org/devices/) |[Uniquement la lignée des Google Pixel ](https://calyxos.org/get/) |
    |F-Droid |Installé |Installé |
    |Facilité d'installation^(7)^ |Difficile|Moyenne |
    |Documentation |Bonne |Moyenne|

    </center>

    *(1)* Bien que pas entièrement open-source, LineageOS pour microG et CalyxOS vous débarrassent des applications Google et limitent la quantité de code propriétaire au strict minimum.

    *(2)* Concernant les aspects comme la compatibilité des applications, les notifications instantanées, l'accès aux cartes et ainsi de suite. Alors que la plupart des applications fonctionnent très bien avec LineageOS pour microG ou CalyxOS, [certains logiciels s'en sortent moins bien](https://github.com/microg/GmsCore/wiki/Implementation-Status). Aussi, l'utilisation d'application payantes sans utiliser le Google Play Store peut être un peu délicat.

    *(3)* Concernant des aspects comme la batterie, le stockage et l'utilisation du processeur. microG ne prend que 4 Mo, comparativement aux plus de 700 Mo de la pile entière des applications Google

    *(4)* CalyxOS reçoit automatiquement les mises à jour de sécurité. LineageOS déploie des mises à jour de sécurité manuelles : alors que celles-ci incluent régulièrement les patches pour Android, les patches pour les noyaux des appareils ou les drivers ne sont pas uniformes.

    *(5)* CalyxOS peut verrouiller le logiciel de démarrage. Cela conserve la possibilité d'un démarrage sécurisé, conformément au modèle de sécurité d'Android. LineageOS de son côté tourne avec une séquence de démarrage déverrouillée. C'est un problème de sécurité, qui peut être exploité par un attaquant qui aurait un accès physique au téléphone, ou par des attaques répétées capables de survivre à un redémarrage, par exemple des applications infectées ou des attaques via les navigateurs.

    *(6)* microG utilise l'usurpation de signature, ce qui peut présenter des failles de sécurité. Sur CalyxOS, l'usurpation de signature est implémentée de manière très restrictive.

    *(7)* Installer LineageOS pour microG peut sembler compliqué et long. Vous pouvez rencontrer des problèmes inopinés, surtout si c'est la première fois.


<br>
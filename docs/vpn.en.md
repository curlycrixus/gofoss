---
template: main.html
title: How To Hide My Location | Learn how to use VPN to change location & more
description: Looking for a free VPN for iPhone? Or a free VPN for Netflix? This site explains how to use VPN.
---

# How To Use VPN <br> (Hide Your Location & More)

!!! level "Last updated: March 2022. For intermediate users. Some tech skills required."

<div align="center">
<img src="../../assets/img/vpn_connection.png" alt="Hide my location iPhone" width="500px"></img>
</div>

The term [Virtual Private Network](https://en.wikipedia.org/wiki/Virtual_private_network) (VPN) refers to a form of transport-layer encryption. It brings all sorts of benefits, such as hiding your location to access geoblocked websites. So how does VPN work? When connecting to a VPN, traffic sent out or received by your device travels through a sort of encrypted tunnel. If someone eavesdrops, for example your Internet Service Provider (ISP) or an attacker, they can only tell you are transmitting data. They won't know its destination or content. However, the online providers you interact with — such as search engines, banks or email providers — are able to inspect, store and modify your data. So does your VPN provider.

Choose a trustworthy VPN provider to protect your online activity. You'll find many online benchmarks and guides, for support refer to [Reddit's VPN community](https://teddit.net/r/VPN/). The following providers have a good reputation in terms of location, logging, payment, security level, availability, pricing and ethics.

??? warning "Will VPN hide my location? And can VPN be traced?"

    Like with [Tor](https://gofoss.net/tor/), using VPN for privacy doesn't necessarily mean being anonymous or secure. For example, while some websites explain how to hide your location on iPhone, your device still can be exposed to browser fingerprinting, man in the middle attacks, correlation attacks or other exploits. Also note that [in a later chapter, we'll explain how to set up OpenVPN to communicate with your private server](https://gofoss.net/secure-domain/). Such a setup is not meant to protect your online identity.

<br>

<center> <img src="../../assets/img/separator_protonvpn.svg" alt="ProtonVPN privacy" width="150px"></img> </center>

## ProtonVPN

[ProtonVPN](https://protonvpn.com/) claims to be the only free VPN with no ads and no speed limits. It offers a large range of features, such as a "kill switch" and DNS leak protection, split tunneling or traffic encryption over two locations.

ProtonVPN is based in Switzerland, outside the [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) countries. The company claims to be a [no-logs VPN provider](https://protonvpn.com/privacy-policy), except for keeping a timestamp of the last successful login attempt, to protect user accounts from password brute force attacks.

The [client software is open source](https://github.com/ProtonVPN/), anyone can review it. The Android app [contains no trackers and requires 7 permissions](https://reports.exodus-privacy.eu.org/en/reports/ch.protonvpn.android/latest/). ProtonVPN reportedly uses [dedicated servers](https://protonvpn.com/vpn-servers/).

There is a free version, however with some limitations: 1 VPN connection, severs in only 3 countries and medium speed. Access to additional features requires a paid account, which started at around €4/month at the time of writing. ProtonVPN accepts anonymous payments.


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Mullvad VPN review" width="150px"></img> </center>

## Mullvad VPN

[Mullvad](https://mullvad.net/en/) claims to be a fast, trustworthy and easy-to-use VPN. Mullvad also powers [Mozilla's VPN solution](https://www.mozilla.org/en-US/products/vpn/).

The company is based in Sweden, which is part of the [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) countries. Mullvad claims to be a [no-logs VPN provider](https://mullvad.net/en/help/no-logging-data-policy/).

The [client software is open source](https://github.com/mullvad/), anyone can review it. The Android app [contains no trackers and requires 4 permissions](https://reports.exodus-privacy.eu.org/en/reports/net.mullvad.mullvadvpn/latest/). [Mullvad servers are operated all over the world](https://mullvad.net/en/servers/), most of them are however rented.

At the time of writing, subscriptions started at around €5/month. Mullvad accepts anonymous payments and requires no email to sign up.


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="Is using VPN illegal?" width="150px"></img> </center>

## RiseupVPN

[RiseupVPN](https://riseup.net/en/vpn) claims to offer censorship circumvention, location anonymization and traffic encryption.

RiseUp is a volunteer-run collective, created in 1999. The Seattle based organisation is located in a [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) country. RiseUp claims to be a [no-logs VPN provider](https://riseup.net/en/privacy-policy).

RiseupVPN’s clients are based on the open source software [Bitmask](https://bitmask.net/). The Android app [contains no trackers and requires 8 permissions](https://reports.exodus-privacy.eu.org/en/reports/se.leap.riseupvpn/latest/).

At the time of writing, RiseupVPN was free of charge. The service is entirely funded through donations.


<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="Best VPN for privacy" width="150px"></img> </center>

## CalyxVPN

[CalyxVPN](https://calyx.net/) is made available by the Calyx Institute, a non-profit organisation founded in 2010. It is located in a [Fourteen Eyes](https://en.wikipedia.org/wiki/Five_Eyes) country.

The [privacy policy](https://calyxinstitute.org/legal/privacy-policy/) of the Calyx Institute claims that IP addresses may be logged, but deleted on a regular basis.

CalyxVPN's clients are based on the open source software [Bitmask](https://bitmask.net/). The Android app [contains no trackers and requires 8 permissions](https://reports.exodus-privacy.eu.org/en/reports/org.calyxinstitute.vpn/latest/).

At the time of writing, CalyxVPN was free of charge.


<br>

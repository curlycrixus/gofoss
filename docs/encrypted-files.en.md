---
template: main.html
title: File Encryption | Cryptomator And VeraCrypt Download Guide
description: Wondering which file encryption software to use? Read on to learn how to use VeraCrypt or Cryptomator, how to encrypt an external hard drive, and more!
---

# File Encryption With VeraCrypt & Cryptomator

!!! level "Last updated: March 2022. For intermediate users. Some tech skills required."

<div align="center">
<img src="../../assets/img/cryptomator_veracrypt.png" alt="File encryption" width="400px"></img>
</div>

What is file encryption? It's a way of making your data and [backups](https://gofoss.net/backups/) unreadable, even in the event of a breach. There's a broad choice of free file encryption software. In this chapter, we're going to focus on VeraCrypt and Cryptomator, which range among the most secure encryption tools. VeraCrypt is best suited to encrypt files in large containers on a local device, without revealing information about file structure, number or size. Cryptomator on the other hand encrypts each file individually, which makes it more suitable for cloud storage. Cryptomator does however reveal non-encrypted meta information, such as timestamps, number or size of files.

<br>

<center> <img src="../../assets/img/separator_veracrypt.svg" alt="VeraCrypt vs TrueCrypt" width="150px"></img> </center>

## VeraCrypt review

[VeraCrypt](https://www.veracrypt.fr/en/) is a free and [open source](https://veracrypt.fr/code/) program to encrypt files or entire file systems. It's the successor of [TrueCrypt](https://en.wikipedia.org/wiki/TrueCrypt), and was first released in 2013. More detailed installation instructions below (at the time of writing, no VeraCrypt Android version has been released).

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        Download the latest [VeraCrypt installer for Windows](https://www.veracrypt.fr/en/Downloads.html) and follow the on-screen instructions.


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        MacOS needs the companion app OSXFUSE to run VeraCrypt. [Download the latest installer](https://github.com/osxfuse/osxfuse/releases/), open the downloaded `.dmg` file and follow the on-screen instructions. Be sure to select the `MacFUSE Compatibility Layer`.

        Next, download the latest [VeraCrypt Installer for macOS](https://www.veracrypt.fr/en/Downloads.html) and follow the on-screen instructions. For easy access, open the Applications folder and drag the VeraCrypt icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Download the latest [VeraCrypt Generic Installer for Linux](https://www.veracrypt.fr/en/Downloads.html). The file should be named something like `veracrypt-X.XX-UpdateX-setup.tar.bz2`. For the purpose of this tutorial, let's suppose the file was downloaded to the folder `/home/gofoss/Downloads` and needs to be installed in the folder `/home/gofoss/veracrypt`. Make sure to adjust these file paths according to your own setup. Open a terminal and run the following commands:

        ```bash
        mkdir /home/gofoss/veracrypt
        cd /home/gofoss/Downloads
        tar -xvf veracrypt-X.XX-UpdateX-setup.tar.bz2 -C /home/gofoss/veracrypt
        ```

        Install VeraCrypt Linux by following the on-screen instructions:

        ```bash
        cd /home/gofoss/veracrypt
        ./veracrypt-1.XX-UpdateX-setup-gui-x64
        ```

        To uninstall VeraCrypt, run the `/usr/bin/veracrypt-uninstall.sh` script.


### How to use VeraCrypt

Once VeraCrypt is installed, you're able to create encrypted containers and conceal your files.

??? tip "Show me the step-by-step guide"

    <center>

    | Steps | Instructions |
    | ------ | ------ |
    | Create a volume |Launch VeraCrypt and click on `Create Volume ‣ Create an encrypted file container ‣ Next`. |
    | Volume type |Select `Standard VeraCrypt volume ‣ Next`. |
    | Volume location |Click on `Select File`, then specify the path to the location where the VeraCrypt container should be stored. Then click on `Save ‣ Next`|
    | Encryption tools |Choose `AES` as encryption algorithm, and `SHA-512` as hash algorithm, then click `Next`. <br><br>*Is VeraCrypt safe?*: VeraCrypt supports a number of secure encryption algorithms including AES, Serpent, Twofish, Camellia, and Kuznyechik, as well as different combinations of cascaded algorithms. VeraCrypt also supports four hash functions including SHA-512, Whirlpool, SHA-256 and Streebog. |
    | Volume size |Specify the file size of your VeraCrypt container. Then click `Next`. |
    | Volume password |Provide a [strong and unique password](https://gofoss.net/passwords/) for your VeraCrypt container. It's the key to your data. <br><br>*Remark*: VeraCrypt provides additional options, such as enabling key files or a Personal Iterations Multiplier (PIM). |
    | Format options |Select a file system, for example `FAT`. Then click `Next`. |
    | Volume format |Move your mouse as randomly as possible for at least 30 seconds, ideally until the randomness indicator bar reaches the maximum. Then click `Format` to create the container. Depending on its size and the available computing power, this can take a LONG time. Grab a cup of coffee or get lunch. And don't unplug your device... |
    | Exit |Once the volume has been created, click `OK ‣ Exit`. |

    </center>

??? tip "Show me the 1-minute summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/6d68b947-67ee-402b-bdcd-9d292f792f57" frameborder="0" allowfullscreen></iframe>

    </center>


### VeraCrypt – decrypt a volume

Files encrypted by VeraCrypt can only be accessed with the correct password.

??? tip "Show me the step-by-step guide"

    <center>

    | Steps | Instructions |
    | ------ | ------ |
    | Choose a drive slot |Open the VeraCrypt app and select a drive slot to which the VeraCrypt container will be mounted. |
    | Mount the container |Click `Select File` and browse to your encrypted container. Then click `Open ‣ Mount`. |
    | Provide a password |Enter the password for the VeraCrypt container and click `OK`. Then, provide your administrator password. |
    | Open the container |The container will mount as a virtual disk, which behaves like any drive on your computer. Navigate to the virtual disk and create, open, modify, save, copy or delete files. |
    | Dismount the container |Once you're finished, close the container by clicking on `Dismount` in the main VeraCrypt window. |

    </center>

??? tip "Show me the 1-minute summary video"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/1eeb5668-4800-4326-9da1-1b20d8a53edb" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_cryptomator.svg" alt="Cryptomator vs Boxcryptor" width="150px"></img> </center>

## Cryptomator review

[Cryptomator](https://cryptomator.org/) is [open source](https://github.com/cryptomator/) software which encrypts your data before it's uploaded to the cloud. The encrypted data can only be accessed with the right password. This reduces the risk that cloud providers or any third party get access to your online data. More detailed installation instructions below.

=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        Download the latest [Cryptomator installer for Windows](https://cryptomator.org/downloads/win/thanks/) and follow the on-screen instructions.


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        Download the latest [Cryptomator installer for macOS](https://cryptomator.org/downloads/mac/thanks/), open the downloaded `.dmg` file and drag the Cryptomator icon on top of the Application folder. For easy access, open the Applications folder and drag the Cryptomator icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Open a terminal, add the correct repository and install Cryptomator:

        ```bash
        sudo add-apt-repository ppa:sebastian-stenzel/cryptomator
        sudo apt update
        sudo apt install cryptomator -y
        ```

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        Purchase and download Cryptomator from [Google's Play Store](https://play.google.com/store/apps/details?id=org.cryptomator&hl=en/), from [Cryptomator's website](https://cryptomator.org/android/) or from [Aurora Store](https://auroraoss.com/).


=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

        Purchase and download Cryptomator from the [App Store](https://apps.apple.com/app/cryptomator/id953086535/).


### How to use Cryptomator

=== "Desktop clients"

    Once Cryptomator is installed on your desktop environment, you're able to create encrypted vaults which conceal your cloud files. For example, you could use Cryptomator with Google Drive, iCloud or OneDrive.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Instructions |
        | ------ | ------ |
        | Create a new vault |Launch Cryptomator and select `+ Add Vault ‣ Create New Vault`. |
        | Choose a name |Give your vault a name, then click `Save`. |
        | Choose a storage location |Choose a location where you want encrypted files to be stored. This should be a directory which is synchronised with the cloud. Click on `Open ‣ Next`. |
        | Choose a password |Provide a [strong and unique password](https://gofoss.net/passwords/). It's the key to your data.|
        | Create a recovery key |Optionally, create a recovery key. This key should be safely backed up, as it will be needed if you loose your password. |
        | Create the vault |Click `Create Vault`. |

        </center>

        *Remark*: You can also add an existing vault to the desktop client. In this case, launch Cryptomator, select `+ Add Vault ‣ Open Existing Vault`, navigate to the folder containing the vault and select the file `masterkey.cryptomator`.

    ??? tip "Show me the 1-minute summary video"

        <center>

        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/dd51ffbc-d347-4319-8543-a63e21fd3023" frameborder="0" allowfullscreen></iframe>

        </center>


=== "Mobile clients"

    Once Cryptomator is installed on your mobile device, you're able to create encrypted vaults which conceal your cloud files.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Instructions |
        | ------ | ------ |
        | Create a new vault |Launch Cryptomator and click on `+ ‣ Create new vault`. |
        | Choose your cloud provider |Select the cloud provider where you want your encrypted files to be stored. If not already done, provide the credentials for your cloud storage provider.|
        | Choose a name |Give your vault a name, then click `Create`. |
        | Choose a storage location |Choose a location on your cloud storage where you want encrypted files to be stored. Click on `Place here`. |
        | Choose a password |Provide a [strong and unique password](https://gofoss.net/passwords/). It's the key to your data. Click on `Done`.|

        </center>

        *Remark*: You can also add an existing vault to the mobile app. In this case, open the Cryptomator mobile app, select `+ ‣ Add existing vault`, choose your cloud provider, navigate to the folder containing the vault and select the file `masterkey.cryptomator`.


### Cryptomator – decrypt a volume

=== "Desktop clients"

    Files encrypted by Cryptomator can only be accessed with the correct password.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Instructions |
        | ------ | ------ |
        | Unlock the vault |Open Cryptomator, select an existing vault and click on `Unlock...` Provide your password and click on `Unlock`. |
        | Mount the vault |Click on `Reveal Vault`. The vault will mount, and behave like any drive on your computer. Create, open, modify, save, copy or delete files. |
        | Lock the vault |Once you're finished, close the vault by clicking on `Lock` in the main Cryptomator window. |

        </center>

    ??? tip "Show me the 1-minute summary video"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/e7937c30-f60c-40f1-abdb-798400e5cbcb" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Mobile clients"

    Files encrypted by Cryptomator can only be accessed with the correct password.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Steps | Instructions |
        | ------ | ------ |
        | Unlock the vault |Open the Cryptomator app, select an existing vault, provide your password and click on `Unlock`. |
        | Lock the vault |Create, open, modify, save, copy or delete files. Once you're finished, close the vault by clicking on `Lock` button in the vault list. |

        </center>

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Boxcryptor vs Cryptomator" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [VeraCrypt's documentation](https://veracrypt.fr/en/Documentation.html) or ask the [community](https://sourceforge.net/p/veracrypt/discussion/).

* [Cryptomator's documentation](https://docs.cryptomator.org/en/latest/) or [ask the community](https://community.cryptomator.org/).

<br>

---
template: main.html
title: Datenschutzerklärung, Haftungsausschluss & Lizenz
description: Datenschutzerklärung. Welche Daten wir sammeln. Eure Rechte. Rechtlicher Hinweis. Lizenz-Zusammenfassung. DSGVO. Was wir niemals mit Euren Daten tun werden.
---

# Datenschutzerklärung, Haftungsausschluss & Lizenz

## Unsere Datenschutzerklärung

<center> <img src="../../assets/img/disclaimer.png" alt="Schutz der Privatsphäre und Datenschutz im Internet" width="700px"></img> </center>

v1.1 - Oktober 2021

gofoss.net engagiert sich für den Datenschutz seiner NutzerInnen. Wie alles, was wir tun, haben wir auch diese Datenschutzerklärung so gestaltet, dass sie einfach und für alle zugänglich ist. 

Dieses Dokument wurde ursprünglich in englischer Sprache verfasst. Die englische Fassung ist die einzige Version, für die gofoss.net Verantwortung übernimmt.

### Definitionen

* *DSGVO*: Datenschutz-Grundverordnung, [EU 2016/679](https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679&from=EN)
* *Daten*: Nach der DSGVO sind Daten alle Informationen, die zur Identifizierung einer Person verwendet werden können, entweder direkt (echter Name, Telefonnummer, IP-Adresse usw.) oder indirekt (jede Kombination der vorgenannten plus Geräte-Fingerabdrücke, Cookies usw.). Im spezifischen Kontext der Nutzung dieser Webseite handelt es sich um die Mindestinformationen, die für den ordnungsgemäßen Betrieb der Webseite erforderlich sind.
* *Dienste*: die Gesamtheit der verschiedenen Programme, Protokolle und Standards, die für den Datenaustausch zwischen Webanwendungen verwendet werden.
* *BenutzerInnen* oder *Ihr*: jede Person oder Drittpartei, die auf gofoss.net zugreift.
* *gofoss.net*, *wir* oder *uns*: https://gofoss.net
* *Host*: diese Webseite wird von [Netlify](https://www.netlify.com/) gehostet.

### Anwendungsbereich

Diese Datenschutzerklärung gilt für gofoss.net und dessen Sub-Domains. Sie erstreckt sich nicht auf Webseiten- oder Dienste, auf die von dieser Webseite aus zugegriffen werden kann. Dies beinhaltet ebenfalls föderierte Dienste und soziale Netzwerke, ohne sich auf diese zu beschränken. Diese Dienste verwenden Protokolle, die notwendigerweise Daten zwischen verschiedenen Anbietern austauschen oder übertragen. Daher liegen solche Wechselbeziehungen außerhalb des Geltungsbereichs dieser Datenschutzerklärung.

### Zustimmung

Indem Ihr auf unsere Webseite zugreift, akzeptiert Ihr unsere Datenschutzbestimmungen und Nutzungsbedingungen.

### Welche Daten werden von uns gesammelt?

* Wir verwenden keine Cookies.
* Wir spionieren Euch nicht mit Hilfe von Skripten aus.
* Wir setzen keine sogenannten *Tracker* auf unserer Webseite ein, um Euer Verhalten zu analysieren.
* Wir sammeln keine persönlich identifizierbaren Informationen von Euch, weder über die Eingabe von Formularen noch durch andere Methoden.
* Wir haben einen Online-Anbieter gewählt, der dafür bekannt ist, [die Privatsphäre seiner NutzerInnen zu respektieren](https://www.netlify.com/gdpr-ccpa), und der mit Rechtsexperten in Europa und den USA zusammenarbeitet um sicherzustellen, dass seine Produkte und vertraglichen Verpflichtungen mit den DSGVO-Vorschriften übereinstimmen.
* Der Anbieter sammelt Zugriffsprotokolle einschließlich der IP-Adressen der Webseiten-Besucher. Diese werden weniger als 30 Tage gespeichert.

### Was wir niemals mit Euren Daten machen werden

* Wir schlagen keinen Profit aus Euren Daten.
* Wir erheben keine anderen Daten als jene, die für den Betrieb der Webseite erforderlich sind.
* Wir verarbeiten oder analysieren in keinster Weise Euer Verhalten oder persönliche Merkmale, um Profile über Euch zu erstellen.
* Wir schalten keine Werbung und pflegen keinerlei Geschäftsbeziehungen zu Werbetreibenden.
* Wir verkaufen Eure Daten nicht an Dritte.
* Wir verlangen keine zusätzlichen Informationen, die für den Betrieb der Webseite nicht notwendig wären (wir fragen nicht nach Telefonnummern, privaten persönlichen Daten, Wohnanschrift und so weiter).

### Eure Rechte

Laut *DSGVO* habt Ihr eine Reihe von Rechten in Bezug auf Eure personenbezogenen Daten:

* *Recht auf Zugang* - Das Recht, jederzeit (I) Kopien Eurer persönlichen Daten oder (II) Zugang zu den von Euch eingereichten und von uns gespeicherten Informationen anzufordern.
* *Recht auf Berichtigung* - Das Recht, Eure Daten zu berichtigen, wenn diese ungenau oder unvollständig sind.
* *Recht auf Löschung* - Das Recht, die Löschung oder Entfernung Eurer Daten von unserer Webseite zu verlangen.
* *Recht auf Einschränkung der Nutzung Ihrer Daten* - Das Recht, die Verarbeitung einzuschränken oder die Art und Weise, wie wir Eure Daten nutzen, zu begrenzen.
* *Recht auf Datenübertragbarkeit* - Das Recht, Eure Daten zu migrieren, zu kopieren oder zu übertragen.
* *Widerspruchsrecht* - Das Recht, der Nutzung Eurer Daten durch uns zu widersprechen.

### Änderungen dieser Datenschutzerklärung

Alle Änderungen an dieser *Datenschutzerklärung* sind öffentlich zugänglich. Wir empfehlen Euch, Euch regelmäßig über etwaige Änderungen dieser Erklärung zu informieren.

### Ansprechpartner

Falls Ihr Fragen zu dieser Datenschutzerklärung oder zu den Vorgehensweisen dieser Webseite habt, wendet Euch bitte per E-Mail an [gofoss@protonmail.com](mailto:gofoss@protonmail.com).

<br>

## Rechtlicher Hinweis

Alle Informationen auf [https://gofoss.net/](https://gofoss.net/) werden nach bestem Wissen und Gewissen veröffentlicht und dienen ausschließlich der allgemeinen Information. [https://gofoss.net](https://gofoss.net/) übernimmt keine Gewähr für die Vollständigkeit, Zuverlässigkeit und Richtigkeit dieser Informationen.

Es werden keine Garantien oder Gewährleistungen gegeben oder impliziert. Jede Handlung, die Ihr aufgrund der auf [https://gofoss.net](https://gofoss.net/) gefundenen Informationen vornehmt, geschieht ausschließlich auf Euer eigenes Risiko. BenutzerInnen übernehmen alle Risiken für eventuell auftretende Schäden, einschließlich, aber nicht beschränkt auf Datenverluste, Schäden an der Hardware oder entgangene Geschäftsgewinne. [https://gofoss.net](https://gofoss.net/) haftet nicht für Verluste und/oder Schäden im Zusammenhang mit der Nutzung dieser Webseite. Sofern nicht ausdrücklich durch die Garantie für Euer Gerät erlaubt, solltet Ihr davon ausgehen, dass jegliche Garantie erlischt, wenn Ihr die Systemsoftware oder die Hardware manipuliert.

Von unserer Webseite aus könnt Ihr andere Webseiten besuchen, indem Ihr Hyperlinks zu solchen externen Seiten folgt. Obwohl wir uns bemühen, nur hochwertige Links zu nützlichen und ethischen Webseiten anzubieten, haben wir keine Kontrolle über den Inhalt und die Natur dieser Webseiten. Links zu anderen Webseiten stellen keine Empfehlung für den gesamten Inhalt dieser Seiten dar. Die Eigentümer der Webseiten und deren Inhalte können sich ohne Vorankündigung ändern und es kann vorkommen, dass ein Link "mangelhaft" wird, bevor wir diesen entfernen können. Bitte beachtet auch, dass andere Webseiten unterschiedliche Datenschutzrichtlinien und -bedingungen haben können, die außerhalb unserer Kontrolle liegen. Bitte informiert Euch über die Datenschutzrichtlinien und Nutzungsbedingungen dieser Webseiten, bevor Ihr Geschäfte tätigt oder Informationen hochladet.

Durch die Nutzung unserer Webseite stimmt Ihr unserem Haftungsausschluss zu und erklärt Euch mit den Bedingungen einverstanden. Sollten wir dieses Dokument aktualisieren, ergänzen oder ändern, werden diese Änderungen an prominenter Stelle veröffentlicht.

<br>

## Lizenz (Zusammenfassung, englische Version)

Wenn Ihr diese Seite verwendet, denkt bitte daran, [https://gofoss.net](https://gofoss.net/) zu nennen und auf die Lizenz zu verweisen.

Copyright (c) 2020-2022 Georg Jerska <gofoss@protonmail.com>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A full copy of the GNU Affero General Public License version 3 is available at <https://www.gnu.org/licenses/agpl-3.0.txt>.

This program incorporates work covered by the following copyright and permission notice:

> Copyright (c) 2016-2022 Martin Donath <martin.donath@squidfunk.com>
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

---
template: main.html
title: End To End Encryption & Transport Layer Security
description: What is TLS? Learn more about transport layer protocols & end-to-end encryption. Adopt encrypted messaging apps & email encryption.
---

# Speak Freely With End To End Encryption

In this chapter, we'll discuss how to [use encrypted messaging apps](https://gofoss.net/encrypted-messages/). We'll also discuss how to progressively [adopt email encryption](https://gofoss.net/encrypted-emails/).

<center>
<html>
<embed src="../../assets/echarts/email_stats.html" style="width: 100%; height:420px">
</html>
</center>


## Encrypted messaging apps & email encryption

Maybe you've already heard that most email providers and messenger apps:

* [sell private conversations to advertisers](https://www.wsj.com/articles/yahoo-bucking-industry-scans-emails-for-data-to-sell-advertisers-1535466959/)
* [take part in mass surveillance programs](https://www.nytimes.com/2016/10/06/technology/yahoo-email-tech-companies-government-investigations.html/)
* [suffer severe data breaches](https://en.wikipedia.org/wiki/Yahoo!_data_breaches)
* [track their user's behavior](https://thenextweb.com/news/google-tracks-nearly-all-your-online-and-some-of-your-offline-purchases-see-it-for-yourself-here)

Nevertheless, the vast majority continues to entrust their communication to incumbent service providers. Main reason? Changing provider seems daunting. After all, many will argue that their current app is "free" of charge, known by everybody and tied to all their accounts and subscriptions. Fortunately, changing provider is not as hard as it seems.


## Transport layer security vs. end to end encryption

Nowadays, several messenger and email apps are encrypted to protect your communication:

**Transport layer security (TLS)** makes sure your communication can't be decoded while transiting on the Internet or between cell towers. However, with TLS encryption, intermediaries can access your data while passing it along or processing it. This includes many messenger services, social networks, search engines, banks, and so on.

<div align="center">
<img src="../../assets/img/tls.png" alt="TLS 1.2" width="550px"></img>
</div>

**End-to-end encryption (E2EE)** protects your communication all the way. No one except the final recipient can decode it. Not even intermediaries passing along the data, such as for example an [end-to-end encrypted messenger](https://gofoss.net/encrypted-messages/) or [email provider](https://gofoss.net/encrypted-emails/).

<div align="center">
<img src="../../assets/img/e2ee.png" alt="TLS vs SSL" width="550px"></img>
</div>

??? tip "Tell me more about how encryption can help avoid online surveillance"

    <center>

    <iframe src="https://archive.org/embed/tips-to-help-avoid-online-surveillance" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Courtesy of the [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


<br>

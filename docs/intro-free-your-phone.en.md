---
template: main.html
title: Degoogled Phones, Linux Phones & FOSS Apps
description: Want a degoogled phone? What are the best phones for privacy? Learn more about degoogled Android, open source phone systems & sustainable phones
---

# Degoogled Phones –<br> Free Your Phone From Google And Apple

Want a degoogled phone? In this chapter, we'll discuss how to break away from Android and iOS by:

* privileging [free and open source apps (FOSS apps)](https://gofoss.net/foss-apps/) without trackers and with minimal permissions
* using a customised open source phone OS such as [CalyxOS](https://gofoss.net/calyxos/) or [LineageOS](https://gofoss.net/lineageos/)


## Tracker-free FOSS apps

<center>
<html>
<embed src="../../assets/echarts/app_stats.html" style="width: 100%; height:520px">
</html>
</center>

Mobile apps from Google, Facebook, Samsung or Microsoft are downloaded by the billions. Often, they come pre-installed without the user's explicit consent.

This raises serious privacy concerns. Many of those apps request access to your location, microphone, camera, contacts and so on. They also contain trackers to collect information about you. On average, the [top 50 Android apps](https://en.wikipedia.org/wiki/List_of_most-downloaded_Google_Play_applications) (over 100 billion downloads) contain **2 to 3 trackers** and require **36 permissions**, as illustrated above.

??? info "Tell me more about trackers"

    A tracker is a piece of software gathering information on how applications and smartphones are being used. This can include various aspects, such as let's say the ability to track your location, scan your contacts, access your credit card numbers or read your clipboard contents. Carefully check for trackers and permissions when you install an app, for example using [εxodus](https://reports.exodus-privacy.eu.org/en/).

    <center>

    | Tracker type | Description |
    | ------ | ------ |
    | Analytics | Collects data, for example which websites you visit, for how long, which part of the website, and so on. |
    | Profiling | Builds a virtual profile by looking at your browsing history, installed apps, and so on. |
    | Identification | Determines who you are by referring to your name or pseudonyms, location, and so on. |
    | Advertisement | Identifies who is using your device, to serve targeted ads. |
    | Location | Determines your position by checking GPS, cell towers, WiFi networks nearby, and so on. |
    | Crash reports | Informs developers if applications encountered an issue. |

    </center>

<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Best Linux phone" width="150px"></img> </center>

## Degoogled phone & Linux phone

<center>
<html>
<embed src="../../assets/echarts/mobileos_stats.html" style="width: 100%; height:520px">
</html>
</center>

In 2020, the world counted [3.5 billion smartphone users](https://www.statista.com/statistics/330695/number-of-smartphone-users-worldwide/). That's almost half of the world's population. Three out of four of these phones were running Google's Android, the rest Apple's iOS. And [3.3 billion people](https://www.statista.com/statistics/264810/number-of-monthly-active-facebook-users-worldwide/) were using at least one of Facebook's core products — Facebook, WhatsApp, Instagram or Messenger.

Privacy has never been the main focus of these companies. Quite the opposite, smartphones are constantly "sharing" private data with Google, Facebook or Apple, which is then sold to an army of marketers and data brokers: advertising agencies, law enforcement, political action committees, financial institutions, insurance companies and so on. Android phones for example [send 12 megabytes of data to Google every day](https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf). Even when idle, they communicate their location to Google 14 times per hour. Likewise, iPhones push 6 megabytes of data to Google and 1 megabyte to Apple — every day.


??? question "What are the best privacy phones?"

    <center>

    | Option | Description |
    | ------ | ------ |
    |1. Dump your phone | Phones haven't been designed with privacy in mind. If you really care about privacy, you shouldn't be carrying a mobile phone. For most of us, this option is however too radical. |
    |2. Go for a FOSS phone | The next best option would be to find a Linux phone OS or a phone with fully open source mobile OS (and possibly hardware). There are some ongoing projects such as the [Librem 5](https://puri.sm/products/librem-5/), [Pinephone](https://www.pine64.org/pinephone/), [Postmarket OS](https://postmarketos.org/), [Ubuntu Touch](https://ubuntu-touch.io/) or [Sailfish OS](https://sailfishos.org/) (the latter is not entirely FOSS). Again, these bleeding-edge solutions are not for everybody.  |
    |3. Free Android from Google | In principle, Android is open-source. Getting rid of Google's applications and proprietary software is the best compromise solution as of now. This can for example be achieved by switching to CalyxOS or LineageOS for microG, two de googled Android alternatives. |

    </center>

??? question "Should I choose LineageOS for microG or CalyxOS, or something else?"

    It depends on your phone model, as well as your [threat model](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf). LineageOS for microG is compatible with many phone models, at the cost of enhanced security. CalyxOS is more secure, but compatible with less phone. Here an overview of both mobile operating systems:

    <center>

    | Features | LineageOS for microG | CalyxOS |
    | ------ | ------ | ------ |
    |De googled phones^(1)^ |99% |99% |
    |Usability^(2)^ |Good |Good |
    |Performance^(3)^ |Good |Good |
    |Automatic security updates^(4)^ |Limited |Yes |
    |Verified boot^(5)^ |No |Yes |
    |Signature spoofing^(6)^ |Yes |Yes (if using microG) |
    |Supported devices |[Available to hundreds of devices](https://wiki.lineageos.org/devices/) |[Google's Pixel line only](https://calyxos.org/get/) |
    |F-Droid |Installed |Installed |
    |Ease of installation^(7)^ |Hard |Medium |
    |Documentation |Good |Medium |

    </center>

    *(1)* While not entirely open-source, LineageOS for microG and CalyxOS get rid of Google apps and limit the amount of proprietary code to a strict minimum.

    *(2)* Considering aspects such as app compatibility, push notifications, access to maps, and so on. While most apps work just fine with LineageOS for microG or CalyxOS, [some programs don't play nice](https://github.com/microg/GmsCore/wiki/Implementation-Status). Also, using paid apps without Google's Play Store can be a little tricky.

    *(3)* Considering aspects such as battery, storage and CPU usage. microG only takes up 4 MB, compared to over 700 MB for the full Google Apps stack.

    *(4)* CalyxOS automatically receives security updates. LineageOS rolls out manual security updates: while these regularly include patches for Android, patches to the devices's kernel or drivers are not consistent.

    *(5)* CalyxOS can lock the bootloader. This maintains the ability for verified boot, in line with Android's security model. LineageOS on the other hand runs with the bootloader unlocked. This is a security issue, which can be exploited by an attacker with physical access to the phone, or by persistent exploits able to survive a reboot, e.g. from malicious apps or browser exploits.

    *(6)* microG uses signature spoofing, which can present certain security vulnerabilities. On CalyxOS, signature spoofing is implemented in a very restrictive manner.

    *(7)* Setting up LineageOS for microG can seem quite complex and lengthy. You might run into unforeseen issues, especially if it's the first time.

<br>
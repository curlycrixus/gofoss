---
template: main.html
title: Comment auto-héberger vos photos
description: Auto-hébergez vos services en nuage. Comment installer Piwigo ? Qu'est-ce que Piwigo ? Piwigo versus Photoprism ? Piwigo versus Photoview ? Est-ce que Piwigo est sûr ?
---

# Piwigo, une photothèque auto-hébergée

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises."

<center>
<img align="center" src="../../assets/img/piwigo.png" alt="Piwigo" width="550px"></img>
</center>

<br>

[Piwigo](https://fr.piwigo.org/) est une photothèque auto-hébergée pour votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/). Accédez, organisez et partagez vos souvenirs depuis partout. Piwigo prend en charge les albums imbriqués, l'édition par lot, les utilisateurs multiples, les étiquettes, les extensions, les thèmes et plus encore.

??? question "Existe-t-il des alternatives à Piwigo ?"

    Il existe de nombreuses alternatives, choisissez un logiciel qui répond à vos besoins :

    <center>

    | |[Piwigo](https://fr.piwigo.org/) |[Photoprism](https://photoprism.app/) |[Photoview](https://photoview.github.io/) |[Pigallery2](http://bpatrik.github.io/pigallery2/) |[Lychee](https://lycheeorg.github.io/) |
    | ------ | :------: | :------: | :------: | :------: | :------: |
    |Date de création |2002 |2018 |2020 |2017 |2018 |
    |Langue de programmation |PHP |Go |Go |TypeScript |PHP |
    |Exigences minimales |Apache ou nginx, PHP |2 cœurs, 4 Go de RAM |-- |-- |Apache ou nginx |
    |Base de donnée |MySQL, MariaDB |MySQL, MariaDB SQLite |MySQL, Postgres, SQLite |SQL ou sans base de donnée |MySQL, PostgreSQL ou SQLite |
    |Installation |Installation directe |Docker |Docker, installation directe |Docker, installation directe |Docker, installation directe |
    |Interface utilisateur |Basique |Moderne |Moderne |Basique |Moderne |
    |Réactivité |Rapide |Rapide |Rapide |Rapide |Rapide |
    |Albums, albums imbriqués |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Étiquettes, balises |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Étiquettes automatiques |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Reconnaissance faciale |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> <br>(automatique) |<span style="color:green">✔</span> <br>(manuelle) |<span style="color:red">✗</span> |
    |Édition par lot |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |
    |Détection des doublons |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |? |
    |Recherche |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Calendrier, Chronologie |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Lieux, cartes |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |
    |Utilisateurs multiples |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Autorisations |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Partage |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Commentaires |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Suppression |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |
    |Prise en charge du format RAW |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Prise en charge des vidéos |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Interface web mobile |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Applis mobiles |Android & iOS, pas de téléversement automatique |Android & iOS, téléversement automatique experimental |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Prise en charge de la structure de répertoires^1^ |<span style="color:green">✔</span> |<span style="color:green">✔</span><br> (indexing) |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |
    |Prise en charge de WebDAV et/ou FTP^2^ |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |? |<span style="color:red">✗</span> |

    </center>

    1. *Le logiciel pointe vers une structure de répertoires existante contenant des photos/vidéos, sans qu'il soit nécessaire de les modifier ou de les dupliquer. Il n'est donc pas nécessaire d'avoir une copie supplémentaire des fichiers. Les photos/vidéos restent intactes si le logiciel est désinstallé.*
    2. *Les clients WebDAV / FTP peuvent se connecter directement aux répertoires contenant des photos/vidéos et les intégrer comme un lecteur réseau. De cette façon, les photos/vidéos peuvent être ajoutées, supprimées et modifiées sur les appareils clients.*


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Piwigo MySQL" width="150px"></img> </center>

## Préparation de la base de données

Piwigo peut être déployé avec MySQL ou MariaDB. Dans ce tutoriel, nous allons générer la base de donnée MySQL requise par les composants serveur de Piwigo. Des instructions plus détaillées sont disponibles ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Connectez-vous au serveur et accédez à MySQL en tant qu'utilisateur root :

    ```bash
    sudo mysql -u root -p
    ```

    Lancez la commande ci-dessous pour créer l'utilisateur MySQL `piwigoadmin` (ajustez en conséquence). Assurez-vous de remplacer la chaîne de charactères `MotDePasseFort` par un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) :

    ```bash
    CREATE USER 'piwigoadmin'@localhost IDENTIFIED BY 'MotDePasseFort';
    ```

    Générez ensuite la base de donnée requise par Piwigo et accordez lui les bonnes permissions :

    ```bash
    CREATE DATABASE piwigo;
    GRANT ALL ON piwigo.* TO 'piwigoadmin'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;
    ```

    Connectez-vous de nouveau à MySQL en tant qu'utilisateur `piwigoadmin` (ajustez en conséquence) :

    ```bash
    sudo mysql -u piwigoadmin -p
    ```

    Assurez-vous que la base de donnée `piwigo` ait bien été créée :

    ```bash
    SHOW DATABASES;
    ```

    L'affichage devrait être similaire à ceci :

    ```bash
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | piwigo             |
    +--------------------+
    2 rows in set (0.01 sec)
    ```

    Quittez MySQL:

    ```bash
    EXIT;
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/fc962cb5-0be4-42bf-bd7c-456dbc095a27" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Installation de Piwigo" width="150px"></img> </center>

## Installation de Piwigo

Suivez les instructions ci-dessous pour résoudre toutes les [dépendances](https://piwigo.org/guides/install/requirements) et installer Piwigo sur votre serveur.

??? tip "Montrez-moi le guide étape par étape"

    ### Pré-requis

    Seafile requiert PHP pour fonctionner. La façon de déployer et de sécuriser PHP a été présentée dans l'un des [chapitres sur la sécurité des serveurs](https://gofoss.net/fr/server-hardening-basics/). Cela devrait satisfaire les principales dépendances logicielles de Piwigo, y compris `php8.1-{common,mysql,curl,xmlrpc,gd,mbstring,xml,intl,cli,zip}`.

    Installez par ailleurs les modules PHP additionnels suivants :

    ```bash
    sudo apt install php8.1-{cgi,soap,ldap,readline,imap,tidy}
    sudo apt install libapache2-mod-php8.1
    ```

    ### Installation

    [Vérifiez la dernière version auto-hébergée de Piwigo](https://fr.piwigo.org/obtenir-piwigo). Au moment d'écrire ces lignes, il s'agit de la version 12.2.0. Téléchargez et décompressez le paquet avec les commandes suivantes :

    ```bash
    wget http://piwigo.org/download/dlcounter.php?code=latest -O /tmp/piwigo.zip
    sudo unzip /tmp/piwigo.zip 'piwigo/*' -d /var/www
    ```

    Définissez et vérifiez les bonnes permissions :

    ```bash
    sudo chown -R www-data:www-data /var/www/piwigo/
    sudo chmod -R 755 /var/www/piwigo/
    sudo ls -al /var/www/
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/65b649cf-9b0b-4448-8823-0162e51fd9f0" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Interface web de Piwigo" width="150px"></img> </center>

## Interface web

Nous allons configurer un hôte virtuel Apache comme proxy inverse pour accéder à l'interface web de Piwigo. Des instructions plus détaillées sont disponibles ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Créez un fichier de configuration Apache:

    ```bash
    sudo vi /etc/apache2/sites-available/myphotos.gofoss.duckdns.org.conf
    ```

    Ajoutez le contenu ci-dessous et assurez-vous d'ajuster les paramètres à vos propres réglages, comme les noms de domaine (`myphotos.gofoss.duckdns.org`), chemins vers les clés SSL, l'adresse IP, et ainsi de suite:

    ```bash
    <VirtualHost *:80>

    ServerName              myphotos.gofoss.duckdns.org
    ServerAlias             www.myphotos.gofoss.duckdns.org
    Redirect permanent /    https://myphotos.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

    ServerName              myphotos.gofoss.duckdns.org
    ServerAlias             www.myphotos.gofoss.duckdns.org
    ServerSignature         Off

    SecRuleEngine           Off
    SSLEngine               On
    SSLProxyEngine          On
    SSLProxyCheckPeerCN     Off
    SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
    SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
    DocumentRoot            /var/www/piwigo

    <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
    </Location>

    <Directory /var/www/piwigo/>
        Options +FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/myphotos.gofoss.duckdns.org-error.log
    CustomLog ${APACHE_LOG_DIR}/myphotos.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Une fois que ce contenu est ajouté, sauvez et fermez le fichier (`:wq!`).

    Notez comment nous avons activé le chiffrement SSL pour Piwigo avec l'instruction `SSLEngine On`, et utilisé le certificat SSL `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` ainsi que la clé privée SSL `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, que nous avions créés précédemment.

    Notez également comment nous avons désactivé ModSecurity dans le fichier de configuration Apache avec l'instruction `SecRuleEngine Off`, car Piwigo et ModSecurity ne font pas bon ménage.

    Ensuite, activez l'hôte virtuel Apache et redémarrez Apache :

    ```bash
    sudo a2ensite myphotos.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configurez Pi-Hole pour résoudre l'adresse locale de Piwigo. Naviguez vers `https://mypihole.gofoss.duckdns.org` et connectez-vous à l'interface web de Pi-Hole (ajustez en conséquence). Naviguez vers l'entrée `Local DNS Record` et ajoutez la combinaison domaine/IP suivante (ajustez en conséquence) :

    ```bash
    DOMAIN:      myphotos.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/252127f0-8931-4a5e-a987-7f6502e55322" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Configuration de Piwigo" width="150px"></img> </center>

## Configuration

Après avoir installé Piwigo avec succès, nous allons configurer plusieurs paramètres tels que la taille maximale de téléversement, le titre de la photothèque, l'enregistrement et la connexion des utilisateurs, etc. Des instructions plus détaillées sont disponibles ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Taille maximale des photos

    Sur le serveur, ouvrez le fichier de configuration PHP d'Apache :

    ```bash
    sudo vi /etc/php/8.1/apache2/php.ini
    ```

    Modifiez ou ajoutez les paramètres suivants pour augmenter la taille maximale de téléversement à 20 Mo :

    ```bash
    upload_max_filesize = 20M
    post_max_size = 20M
    ```

    Ouvrez le fichier de configuration PHP de l'interface de ligne de commande (en anglais, « command line interface » ou CLI) :

    ```bash
    sudo vi /etc/php/8.1/cli/php.ini
    ```

    Modifiez ou ajoutez les paramètres suivants pour augmenter la taille maximale de téléversement à 20 Mo :

    ```bash
    upload_max_filesize = 20M
    post_max_size = 20M
    ```

    ### Dernière étape

    Allez sur [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) (à ajuster en conséquence) et suivez le guide de configuration :

    <center>

    | Champ | Description |
    | ------ | ------ |
    | Hôte | La valeur par défaut est `localhost`. |
    | Identifiant de connexion | Saisissez le nom de l'utilisateur MySQL. Dans notre exemple, c'est `piwigoadmin`, ajustez en conséquence. |
    | Mot de passe | Saisissez le mot de passe de l'utilisateur MySQL. |
    | Nom de la base de données | Saisissez le nom de la base de données MySQL. Dans notre exemple, c'est `piwigo`, ajustez en conséquence. |
    | Préfixe des tables de Piwigo | La valeur par défaut est le nom de la base de données MySQL suivi d'un trait de soulignement. Dans notre exemple, c'est `piwigo_`, ajustez en conséquence. |
    | Identifiant du compte webmestre | Créez un compte webmestre pour Piwigo. Dans ce tutoriel, nous appellerons le webmestre `piwigoadmin@gofoss.net`, ajustez en conséquence. |
    | Mot de passe du compte webmestre | Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour le compte du webmestre. |
    | Adresse électronique du compte webmestre | Saisissez une adresse électronique pour le compte du webmestre. |

    </center>

    Une fois la configuration terminée, cliquez sur les boutons `Start installation`, `Visit Piwigo` et `I want to add photos`. C'est tout, Piwigo est prêt à l'emploi !

    Commencez à ajuster certains paramètres de base dans `Configuration ‣ Options ‣ Principale` :

    * changez le titre de la photothèque et la bannière
    * désactivez l'enregistrement d'utilisateurs (décochez `Permettre l'enregistrement des utilisateurs`)
    * suivez qui se connecte à Piwigo (cochez `Historiser les visites` pour les visiteurs, les utilisateurs enregistrés et/ou les administrateurs)


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/006c7186-00ea-4c0e-b804-6a7b8629f3e0" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Solution de contournement temporaire pour Ubuntu 22.04"

    Au moment d'écrire ces lignes, Piwigo 12.2.0 ne gère pas correctement PHP 8.1, qui est fourni avec Ubuntu 22.04. Pour cette raison, l'interface Piwigo affiche divers messages `Deprecated` et `Warning` après l'installation. En guise de solution de contournement temporaire, vous pouvez supprimer ces messages :

    * Allez sur [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) et connectez-vous en tant que webmestre `piwigoadmin@gofoss.net` (à ajuster en conséquence)
    * Allez dans `Plugins ‣ Liste des plugins ‣ Tout`
    * Activez le plugin `LocalFiles Editor`
    * Allez dans `Plugins ‣ Activé ‣ LocalFiles Editor ‣ Configuration ‣ Configuration locale`
    * Ajoutez la ligne suivante dans la fenêtre apparaissant `local/config/config.inc.php`, puis cliquez sur `Enregistrer le fichier` :

    ```bash
    <?php

    /* The file does not exist until some information is entered
    below. Once information is entered and saved, the file will be created. */

    $conf['show_php_errors'] = E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING;

    ?>
    ```


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ajouter des utilisateurs à Piwigo" width="150px"></img> </center>

## Ajouter des utilisatrices et utilisateurs

Piwigo différencie [trois types d'utilisateurs](https://fr.piwigo.org/doc/doku.php?id=utiliser:admin:menus:utilisateurs:gerer#cadre_statut):

* **Le webmestre** a un accès complet à Piwigo. Il peut ajouter, modifier et supprimer des photos, des albums, des utilisateurs, des groupes, etc. En outre, le webmestre peut installer des plugins et des thèmes, maintenir et mettre à jour le site, etc.
* **Les administrateurs** ont un accès complet à Piwigo. Ils peuvent ajouter, modifier et supprimer des photos, des albums, des utilisateurs, des groupes, etc.
* **Les visiteurs** ont un accès limité à Piwigo. Ils peuvent voir les albums et les photos à condition d'avoir les bonnes permissions.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    |Étape 1 | Allez sur [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) et connectez-vous en tant que webmestre `piwigoadmin@gofoss.net` (à ajuster en conséquence). |
    |Étape 2 | Allez dans `Utilisateurs ‣ Gérer ‣ Ajouter un utilisateur`. |
    |Étape 3 | Saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Puis, cliquez sur `Ajouter l'utilisateur`. |
    |Étape 4 | Renseignez le statut du nouvel utilisateur, par exemple `visiteur` or `administrateur`. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    Dans cet exemple, nous ajoutons un nouvel administrateur Georg, et les visiteurs Lenina, Tom et RandomUser.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ba6374ef-d524-43e7-97eb-d1bc81d6f6cb" frameborder="0" allowfullscreen></iframe>
    </center>

??? warning "Administrateurs, utilisateurs et utilisatrices ont besoin d'un accès VPN"

    Tout.e.s les utilisateurs et utilisatrices doivent être connecté.e.s via [VPN](https://gofoss.net/fr/secure-domain/) pour accéder à Piwigo.


<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Ajouter des photos à Piwigo" width="150px"></img> </center>

## Ajouter des photos

=== "Formulaire web"

    Le formulaire web ne nécessite qu'un navigateur. Il est mieux adapté pour ajouter une quantité modérée de photos, ou des albums individuels. Les fichiers sont téléversés dans le répertoire `/var/www/piwigo/upload` sur le serveur. Notez que seuls les administrateurs peuvent ajouter, modifier et supprimer des photos. Les instructions pour [ajouter des photos via le formulaire web](https://fr.piwigo.org/doc/doku.php?id=utiliser:admin:menus:photos:ajouter#onglet_formulaire_web) sont décrites ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Connexion | Connectez-vous à l'interface web de Piwigo avec un compte administrateur. |
        |Formulaire web | Allez dans `Administration ‣ Photos ‣ Ajouter ‣ Formulaire web`. <br><br><center> <img src="../../assets/img/piwigo_web_form.png" alt="Piwigo web form" width="300px"></img> </center> |
        |Album | Créez un nouvel album, ou sélectionnez un album existant. |
        |Ajouter des photos | Ajoutez des photos en cliquant sur le bouton `Choisir des fichiers`, ou en les faisant glisser et en les déposant dans la zone dédiée. |
        |Téléverser | Cliquez sur `Démarrer le transfert`. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        Georg est administrateur. Il téléverse huit photos via la formulaire web :

        * quatre photos d'une récente randonnée avec Lenina : `tree.png`, `lake.png`, `moutain.png` and `sunset.png`
        * quatre photos pour un projet sur lequel il travaille : `logo.png`, `georg.png`, `lenina.png` and `tom.png`

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/73abb50e-805b-4619-aebf-5c49644a32e4" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Client FTP"

    Cette méthode nécessite un client FTP, tel que [FileZilla](https://filezilla-project.org/). C'est la méthode la plus adaptée pour ajouter de grandes quantités de fichiers, ou pour téléverser une structure de répertoires entière en une seule fois. Les fichiers doivent être téléversés dans le répertoire `/var/www/piwigo/galleries` sur le serveur. La structure des répertoires sera préservée. Notez que seuls les administrateurs peuvent ajouter, modifier et supprimer des photos. Les instructions pour [ajouter des photos via FTP](https://fr.piwigo.org/doc/doku.php?id=utiliser:admin:menus:photos:ajouter#onglet_ftp_synchronisation) sont décrites ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Connexion | Sur votre ordinateur Ubuntu/Linux, connectez-vous avec le compte `gofossadmin` (ajustez en conséquence). Il s'agit du compte possédant un accès SSH distant au serveur, tel que configuré dans les chapitres [Hébergement de serveur](https://gofoss.net/fr/ubuntu-server/) et [Sécurisation basique du serveur](https://gofoss.net/fr/server-hardening-basics/). |
        |Préparation | Toujours sur votre ordinateur Ubuntu/Linux, classez les photos dans une structure de répertoires de votre choix. Chaque répertoire deviendra un album dans Piwigo. Le nombre de sous-album est illimité. Veillez toutefois à respecter les conventions de nomenclature : les répertoires et les fichiers ne peuvent contenir que des lettres, des chiffres, des tirets, des traits de soulignement ou des points. Les espaces vides et les caractères spéciaux ne sont pas autorisés. |
        |Installation du client FTP |Si FileZilla n'est pas déjà installé, ouvrez un terminal avec le raccourci `CTRL + ALT + T` et exécutez la commande `sudo apt install filezilla`. |
        |Préparation du client FTP |Ouvrez FileZilla et saisissez les informations d'identification correctes : <br><br> <center> <img src="../../assets/img/filezilla.png" alt="Piwigo Filezilla" height="90px"></img> </center> <br> • `Hôte`: `sftp://192.168.1.100` (l'adresse IP du serveur, adjustez en conséquence) <br><br> • `Identifiant`: `gofossadmin` (ajustez en conséquence) <br><br> • `Mot de passe`: `mot_de_passe_pour_se_connecter_au_serveur` <br><br> • `Port`: `2222` (port SSH configuré dans le chapitre [Sécurisation basique du serveur](https://gofoss.net/fr/server-hardening-basics/), ajustez en conséquence) |
        |Connexion avec le client FTP | Toujours dans FileZilla, cliquez sur `Connexion rapide`. Il vous sera demandé une fois de plus de saisir le mot de passe SSH. |
        |Copier les fichiers | FileZilla devrait afficher le système de fichiers de votre ordinateur local dans le volet de gauche, et le système de fichiers du serveur dans le volet de droite. Copiez les répertoires avec vos photos dans le répertoire suivant sur le serveur : `/var/www/piwigo/galleries`. |
        |Connexion | Connectez-vous à l'interface web de Piwigo avec un compte administrateur. |
        |Simulation | Allez dans `Administration ‣ Outils ‣ Synchroniser`. Avant de synchroniser quoi que ce soit, sélectionnez les paramètres suivants (adaptez les permissions `Qui peut voir ces photos?` à vos besoins) : <br><br> <center> <img src="../../assets/img/synchronise.png" alt="Piwigo synchroniser" width="300px"></img> </center> <br> Puis cliquez sur `Valider`. Piwigo devrait afficher le nombre de nouveaux albums et photos qui seront ajoutés ou supprimés de la base de données, et si des erreurs sont à prévoir. Continuez si tout est en ordre. |
        |Synchronisation | Toujours dans `Administration ‣ Outils ‣ Synchroniser`, décochez `Simuler uniquement`:<br><br> <center> <img src="../../assets/img/synchronise_2.png" alt="Piwigo synchroniser" width="300px"></img> </center> <br> Puis, cliquez sur `Valider`. Cela peut prendre un certain temps, en fonction de la quantité de données. Servez-vous un café ! |

        </center>

    ??? warning "Quelques conseils"

        Sachez que les répertoires et les fichiers ne doivent pas être déplacés une fois qu'ils ont été téléversés sur le serveur. Sinon, toutes les données associées seront perdues lors de la prochaine synchronisation (comme les commentaires, les évaluations, etc.).


=== "Applications"

    Cette méthode nécessite des applications tierces telles que [digiKam](https://www.digikam.org/), [Shotwell](https://shotwell-project.org/doc/html/), [Lightroom](https://lightroom.adobe.com/) ou les applications [Android](https://f-droid.org/fr/packages/org.piwigo.android/) et [iOS](https://apps.apple.com/fr/app/piwigo/id472225196) de Piwigo. C'est la méthode la plus adaptée pour ajouter de grandes quantités de photos non structurées, qui seront téléversées dans le répertoire `/var/www/piwigo/upload` sur le serveur. Notez que seuls les administrateurs peuvent ajouter, modifier et supprimer des photos. Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        Reportez-vous à la documentation de l'application tierce concernée pour obtenir des instructions sur l'ajout de photos. La plupart des applications tierces nécessitent des informations d'identification pour se connecter au serveur :

        <center>

        | Réglages | Description |
        | ------ | ------ |
        |Serveur | Saisissez l'adresse de la photothèque. Dans note exemple, il s'agit de [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/), ajustez en conséquence. |
        |Identifiant | Saisissez un identifiant administrateur ou visiteur de Piwigo. |
        |Mot de passe | Saisissez le mot de passe associé à l'administrateur ou au visiteur ci-dessus. |

        </center>

        <center><img align="center" src="../../assets/img/piwigo_app_2.jpg" alt="Application mobile Piwigo" width="150px"></img>
        </center>



<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Modifier des photos dans Piwigo" width="150px"></img> </center>

## Modifier des photos

=== "Modifier une seule photo"

    Vous pouvez éditer des photos individuelles pour en modifier le titre, l'auteur, la date de création, les albums associés, les mots-clés, la description, le niveau de confidentialité, la géolocalisation, etc. Vous pouvez également définir la zone la plus significative de la photo. Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        Connectez-vous en tant qu'administrateur et allez dans `Photos ‣ Gestion par lot ‣ Mode global`. Appliquez un ou plusieurs filtres pour trouver la photo que vous souhaitez modifier (plus d'informations sur les attributs des filtres sont fournies ci-dessous). Passez la souris sur la photo et cliquez sur `Éditer`. Vous êtes maintenant en mesure de modifier la photo.

    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        Georg modifie l'auteur de l'image `logo.png`.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/90ef6f40-27bd-4d8d-aa3c-234d056b396a" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Modifier plusieurs photos"

    La modification de plusieurs photos à la fois est également appelée gestion par lot. Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        Connectez-vous en tant qu'administrateur et allez dans `Photos ‣ Gestion par lot ‣ Mode global`. Appliquez un ou plusieurs filtres pour trouver les photos que vous souhaitez modifier (plus d'informations sur les attributs des filtres sont fournies ci-dessous). Vous pouvez sélectionner ou désélectionner toutes les photos à la fois en cliquant sur `Tout` ou `Rien`. Enfin, précisez l'action à effectuer : supprimer, associer à un ou plusieurs albums, déplacer vers un album ou dissocier d'un album, définir des mots-clés, un auteur, un titre, une date de création ou des géotags, etc.


    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        Georg génère des images de taille multiple à partir de ses photos de randonnée.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/6407514b-20e2-4d55-bba4-52e4ed5fa703" frameborder="0" allowfullscreen></iframe>
        </center>


<div style="    margin-top: -20px;">
</div>

??? info "Dites-m'en plus sur les attributs des filtres"

    <center>

    | Attributs des filtres | Description |
    | ----- | ----- |
    |Filtres prédéfinis | Filtrez toutes les photos, toutes les vidéos, les photos dupliquées, les dernières photos importées, les photos géolocalisées, les photos sans album attribué (orphelines), les photos sans mots-clés, les photos favorites, etc. |
    |Album | Filtrez les photos d'un album spécifique. |
    |Mots clés | Filtrez les photos en fonction de mots-clés. |
    |Niveau de confidentialité | Filtrez les photos visibles par toutes et tous, par les contacts, par les amies et amis, par la famille ou par les administrateurs. |
    |Dimension | Filtrez les photos en fonction de leur dimension. |
    |Taille du fichier | Filtrez les photos en fonction de la taille des fichiers. |
    |Recherche | Filtrez les photos en fonction d'une recherche avancée portant sur le titre, l'étiquette, le nom du fichier, l'auteur, la date de création, la date de publication, la largeur, la hauteur, la taille du fichier, le ratio, etc. |

    </center>


<br>

<center> <img src="../../assets/img/separator_simplefilemanager.svg" alt="Gérer les albums Piwigo" width="150px"></img> </center>

## Gérer les albums

Vous pouvez ajouter de nouveaux albums ou modifier des albums existants via l'interface web de Piwigo. Cela inclut la modification du nom et de la description de l'album, la définition d'un album parent, le verrouillage de l'album afin qu'il ne soit visible que par les administrateurs, l'ajout de photos, la gestion des sous-albums, le réglage du classement automatique ou la définition d'un classement manuel, etc. Vous pouvez également rendre un album public afin qu'il soit consultable par toute personne en possession du lien, ou privé afin qu'il ne soit consultable que par les utilisateurs connectés possédant les autorisations appropriées. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Connectez-vous en tant qu'administrateur et allez dans `Administration ‣ Album ‣ Gérer ‣ Liste`. Pour créer un nouvel album, cliquez sur `Créer un nouvel album`. Pour modifier un album existant, passez la souris sur celui-ci et cliquez sur `Éditer`. Vous êtes maintenant en mesure de modifier les propriétés de l'album, l'ordre de tri ou les permissions.

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    Dans cet exemple, l'administrateur Georg ajoute une description à l'album « Holidays ».

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/eda37f42-f7bd-463b-96c6-109de612e88e" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="Gérer les autorisations dans Piwigo" width="150px"></img> </center>

## Gérer les permissions

Piwigo dispose de deux systèmes pour gérer les autorisations d'accès, qui peuvent être utilisés indépendamment ou combinés :

* **Les permissions pour les utilisateurs et groupes** s'appliquent aux albums, aux utilisateurs et aux groupes
* **Les niveaux de confidentialité** s'appliquent aux photos et aux utilisateurs

Ce système de permission plutôt complexe offre de nombreuses possibilités pour affiner les droits d'accès pour des utilisateurs multiples. Si vous êtes seul(e) à utiliser Piwigo, faites au plus simple : rendez tous vos albums privés et limitez l'accès à vous-même.

=== "Permissions pour les utilisateurs et groupes"

    Commençons par les permissions au niveau des albums. Par défaut, les albums sont `publics` et peuvent être vus par n'importe quel utilisateur. Les administrateurs peuvent rendre les albums `privés` et limiter l'accès à certains utilisateurs et/ou groupes. Les instructions sur la façon de [gérer les permissions des utilisateurs/groupes](https://fr.piwigo.org/doc/doku.php?id=utiliser:admin:menus:utilisateurs:gerer:permissions) sont décrites ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        Afin de rendre un album `privé`:

        | Instructions | Description |
        | ------ | ------ |
        |Étape 1 | Connectez-vous en tant qu'administrateur. |
        |Étape 2 | Allez dans `Album ‣ Gérer ‣ Liste ‣ Éditer ‣ Permissions`. |
        |Étape 3 | Changez le type d'accès de `public` à `privé`. |

        Pour limiter l'accès aux albums `privés` à certains `utilisateurs`:

        | Instructions | Description |
        | ------ | ------ |
        |Étape 1 | Connectez-vous en tant qu'administrateur. |
        |Étape 2 | Allez dans `Album ‣ Gérer ‣ Liste ‣ Éditer ‣ Permissions` et ajoutez les utilisateurs qui peuvent consulter l'album privé dans la case `Permission accordée pour les utilisateurs`.<br><br>  Ou allez dans `Utilisateurs ‣ Gérer ‣ Éditer l'utilisateur ‣ Permissions` et définissez quels albums privés l'utilisateur en question peut consulter (`Autorisés`) ou non (`Interdits`).|

        Les droits d'accès peuvent également être définis pour un ensemble d'utilisateurs, appelés `groupe`. Cela permet de définir des permissions pour des groupes d'utilisateurs, ce qui facilite la gestion en cas d'utilisateurs nombreux. Pour limiter l'accès aux albums `privés` à certains `groupes` :

        | Instructions | Description |
        | ------ | ------ |
        |Étape 1 | Créez des groupes en allant dans `Utilisateurs ‣ Groupes ‣ Ajouter un groupe`.|
        |Étape 2 | Ajoutez des utilisateurs à un groupe en allant dans `Utilisateurs ‣ Gérer ‣ Éditer l'utilisateur ‣ Groupes`. |
        |Étape 3 | Enfin, définissez les droits d'accès aux albums privés pour des groupes en allant dans `Utilisateurs ‣ Groupes ‣ Permissions`. |


=== "Niveau de confidentialité"

    Les niveaux de confidentialité sont définis au niveau de chaque photo et de chaque utilisateur. Ils permettent de définir précisément quel utilisateur peut accéder à quelles photos. Vous trouverez ci-dessous des instructions pour [gérer les niveaux de confidentialité](https://fr.piwigo.org/doc/doku.php?id=utiliser:utilisation:fonctionnalites:gerer_perm:images).

    ??? tip "Montrez-moi le guide étape par étape"

        Piwigo gère les permissions avec cinq `niveaux de confidentialité`. Voici comment cela fonctionne :

        * chaque photo a un niveau de confidentialité
        * chaque utilisateur a un niveau de confidentialité
        * un utilisateur doit avoir un niveau de confidentialité supérieur ou égal à celui des photos qu'il souhaite consulter. Autrement dit, plus le niveau de confidentialité d'un utilisateur est élevé, plus il peut consulter de photos.

        <center>

        | Niveau de confidentialité | Description |
        | :------: | ------ |
        |1 | Tout le monde |
        |2 | Admins, Famille, Amis, Contacts |
        |3 | Admins, Famille, Amis |
        |4 | Admins, Famille |
        |5 | Admins |

        </center>

        Le niveau de confidentialité d'un `utilisateur` peut être défini en se connectant en tant qu'administrateur et allant dans `Utilisateurs ‣ Gérer ‣ Éditer l'utilisateur ‣ Niveau de confidentialité` :

        <center>

        | Niveau de confidentialité | Description |
        | :------: | ------ |
        |1 | Aucun |
        |2 | Contacts |
        |3 | Amis |
        |4 | Famille |
        |5 | Admins |

        </center>


=== "Exemple"

    Terminons par un exemple concret, en appliquant tous les concepts abordés jusqu'ici. Notez qu'il peut exister plusieurs manières pour atteindre le même résultat. Piwigo est plutôt flexible, choisissez votre approche préférée.

    ??? tip "Montrez-moi l'exemple"

        <center>
        <img align="center" src="../../assets/img/piwigo_permissions.png" alt="Permissions Piwigo" width="700px"></img>
        </center>

        <center>

        | Encadré | Description |
        | ------ | ------ |
        |1 | Le statut d'utilisateur de Georg est défini comme `administrateur`. Cela signifie qu'il peut ajouter, modifier et supprimer des photos, des albums, d'autres utilisateurs, des groupes, etc. Le niveau de confidentialité de Georg est défini comme `admins`. Georg a donc accès à tous les albums et photos. |
        |2 | Georg a téléversé quatre photos d'une récente randonnée dans l'album « Holidays » : `tree.png`, `lake.png`, `moutain.png` et `sunset.png`. Il ne veut partager ces photos avec personne d'autre que Lenina. Le statut de Lenina étant défini comme `visiteur`, cela signifie qu'elle ne peut voir que les albums `public`, ou les fichiers pour lesquels elle obtient les permissions requises. Georg rend donc l'album « Holidays » `privé`, et limite l'accès à un groupe appelé « Holidays », auquel seuls lui et Lenina sont affectés. |
        |3 | Georg a également téléversé quatre photos dans l'album « Gofoss » : `logo.png`, `georg.png`, `lenina.png` et `tom.png`. Comme il s'agit d'un album `public`, tout le monde peut accéder aux images, y compris `logo.png`. |
        |4 | Toutefois, Georg a attribué le niveau de confidentialité `Famille` aux photos `georg.png`, `lenina.png` et `tom.png`. Cela signifie que seuls les utilisateurs ayant le niveau de confidentialité `Famille` et plus (`Admins`) ont accès à ces photos. Dans cet exemple, il s'agit de Georg (`Admins`), Lenina (`Famille`) et Tom (`Famille`). RandomUser par contre n'a pas accès à ces photos, car son niveau de confidentialité est défini comme `Amis`. |

        </center>


    ??? tip "Montrez-moi une vidéo récapitulative (4min)"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/fe8fdbb7-c18a-407a-b3a7-689026d38eaf" frameborder="0" allowfullscreen></iframe>
        </center>



<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Ajouter des extensions et thèmes à Piwigo" width="150px"></img> </center>

## Ajouter des extensions et thèmes

=== "Extensions"

    Les fonctionnalités de Piwigo peuvent être étendues avec plus de [350 extensions (en anglais, « plugins »)](https://fr.piwigo.org/ext/). Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Afficher les extensions | Pour afficher toutes les extensions installées, connectez-vous en tant que webmestre `piwigoadmin@gofoss.net` (ajustez en conséquence) et allez dans `Administration ‣ Plugins ‣ Liste des plugins` :<br><br><center> <img src="../../assets/img/piwigo_plugins.png" alt="Extensions Piwigo" width="300px"></img></center><br>• Les `plugins activés` sont installés et opérationnels <br><br>• Les `plugins désactivés` sont installés mais désactivés <br><br>• Notez qu'une extension désactivée conservera la plupart de sa configuration, tandis que la suppression d'une extension en effacera toute trace (fichiers, configurations, etc.) |
        |Mettre à jour les extensions | Allez dans `Administration ‣ Plugins ‣ Rechercher les mises à jour`. |
        |Ajouter des extensions | Allez dans `Administration ‣ Plugins ‣ Autres plugins disponibles`. Choisissez une extension et cliquez sur `Installer`. Allez dans `Administration ‣ Plugins ‣ Liste de plugins` et activez l'extension installée. |

        </center>

        Quelques extensions populaires :

        <center>

        | Extension | Description |
        | ------ | ------ |
        | [Piwigo-Videojs](https://piwigo.org/ext/extension_view.php?eid=610/) | Affichez des vidéos dans Piwigo. Les formats suivants sont pris en charge : mp4, m4v, ogg, ogv, webm, webmv, etc. Plus d'informations sont disponibles sur [la page wiki](https://github.com/Piwigo/piwigo-videojs/wiki). |
        | [Fotorama](https://piwigo.org/ext/extension_view.php?eid=727/) | Diaporama plein écran. |
        | [Batch downloader](https://piwigo.org/ext/extension_view.php?eid=616/) | Téléchargez des sélections de photos sous forme de fichier zip. |
        | [Piwigo-Openstreetmap](https://piwigo.org/ext/extension_view.php?eid=701/) | Géolocalisez vos photos. Plus d'informations disponibles sur [la page wiki](https://github.com/Piwigo/piwigo-openstreetmap/wiki). |
        | [Grum Plugin Classes](https://piwigo.org/ext/extension_view.php?eid=199/) | Nécessaire pour exécuter certaines autres extensions. |
        | [AStat](https://piwigo.org/ext/extension_view.php?eid=172/) | Étend le nombre de statistiques générées par Piwigo. Par exemple : pages ou photos visitées, durée de la visite, origine de l'adresse IP, etc. |
        | [EXIF view](https://piwigo.org/ext/extension_view.php?eid=155/) | Ajoutez des métadonnées EXIF à vos photos. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b52c041b-c4b6-46cd-9e5f-741c3c382cdc" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Thèmes"

    La mise en page de Piwigo peut être personnalisée avec plus de [140 thèmes](https://piwigo.org/ext/). Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        Connectez-vous en tant que webmestre `piwigoadmin@gofoss.net` (ajustez en conséquence) et allez dans `Administration ‣ Configuration ‣ Thèmes`. Téléchargez, activez ou configurez les thèmes de votre choix. Quelques thèmes populaires :

        <center>

        | Thèmes | Description |
        | ------ | ------ |
        | [Modus](https://piwigo.com/blog/2019/06/25/new-modus-theme-on-piwigo-com/) | Thème par défaut, livré en plusieurs variantes. |
        | [Bootstrap Darkroom](https://piwigo.com/blog/2020/03/12/bootstrap-darkroom-new-theme-piwigo-com/) | Un autre thème moderne, riche en fonctionnalités, convivial et adapté aux appareils mobiles. |
        | [SimpleNG](https://piwigo.org/ext/extension_view.php?eid=602/) | Basé sur Bootstrap, adapté aux appareils mobiles. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative (1min)"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a14ba8f8-6ee9-497a-81e3-86418c261eec" frameborder="0" allowfullscreen></iframe>
        </center>



<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Mettre à jour Piwigo" width="150px"></img> </center>

## Mises à jour

La mise à jour de Piwigo est assez [simple](https://fr.piwigo.org/guides/mise-a-jour/automatique). Il suffit de suivre les instructions ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Commencez par [sauvegarder vos photos](https://gofoss.net/fr/backups/) pour parer à tout problème lors de la mise à jour. Pour cela, vous pouvez vous connecter au serveur via FTP (FileZilla) comme décrit précédemment et sauvegarder les dossiers `/var/www/galleries` et `/var/www/upload`.

    Vous pouvez également sauvegarder la base de données de Piwigo en utilisant [MySQL dump](https://linuxize.com/post/how-to-back-up-and-restore-mysql-databases-with-mysqldump/) ou effectuer une [sauvegarde du server](https://gofoss.net/fr/server-backups/). La base de données MySQL de Piwigo est habituellement stockée dans `/var/lib/mysql`.

    Connectez-vous ensuite en tant que webmestre `piwigoadmin@gofoss.net` (ajustez en conséquence) et allez dans `Administration ‣ Outils ‣ Mise à jour ‣ Mise à jour de Piwigo`. Cliquez sur `Mise à jour vers Piwigo xx.x.x`` et confirmez.

??? tip "Montrez-moi une vidéo récapitulative (30sec)"

    <center> <img src="../../assets/img/piwigo_upgrade.gif" alt="Mise à jour Piwigo" width="600px"></img> </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Piwigo" width="150px"></img> </center>

## Assistance

Pour davantage de précisions, consultez la [documentation de Piwigo](https://fr.piwigo.org/doc/doku.php) ou demandez de l'aide à la [communauté Piwigo](https://fr.piwigo.org/forum/).

<br>
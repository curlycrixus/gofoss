---
template: main.html
title: How to browse privately with Firefox, Tor and VPN
description: Firefox vs Chrome. Is Firefox safer than Google? How do I install Firefox? What about Firefox on Android? What is a hardened Firefox?
---

# The Net Is Vast And Infinite — Browse Freely

<center>
<html>
<embed src="../../assets/echarts/browser_stats.html" style="width: 100%; height:520px">
</html>
</center>

In this chapter, we'll explain how to browse the Internet in a more privacy respecting manner:

* with [Firefox](https://gofoss.net/firefox/)
* with the [Tor Browser](https://gofoss.net/tor/)
* with a [VPN](https://gofoss.net/vpn/)

The browser is the gateway to the Internet. It has become so ubiquitous in our everyday lives that we overlook its vast importance.

Most of us rely on Google Chrome to browse the web. Others on Microsoft's Edge or Apple's Safari. All of these browsers have a poor track record in terms of privacy. They come up with an infinite variety of ways to harvest our data: page visits, content views, search history, user names and passwords, locations, voice records, purchases, and so on.

There are some privacy band aids for Chrome, Edge and Safari. But ultimately, these browsers are designed to scrutinise the user's online behaviour and turn it into profits.

<center>
<a href="https://contrachrome.com/"><img align="center" src="../../assets/img/contrachrome_en.png" alt="Contra Chrome"></img></a>
</center>

<br>

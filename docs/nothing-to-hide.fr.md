---
template: main.html
title: Pourquoi la liberté numérique est importante
description: Pourquoi la liberté numérique est-elle importante ? La vie privée est un droit humain ? Comment combattre capitalisme de surveillance et obsolescence programmée ?
---

# Pourquoi la liberté numérique est importante

!!! level "Dernière mise à jour: mars 2022."

<center> <img src="../../assets/img/privacy.png" alt="protection de la vie privée et confidentialité sur internet" width="700px"></img> </center>

## La vie privée est un droit fondamental

*« Si tu n'as rien à cacher, tu n'as rien à craindre »* — nous avons tous entendu cette phrase. De manière assez intéressante, cet argument est fallacieux. Il sous-entend que la volonté de garder privés certains aspects de nos vies signifie forcément que nous dissimulons des méfaits.

Évidemment, ce n'est pas vrai. Le fait de ne pas donner notre smartphone déverrouillé au premier venu ne fait pas de nous des criminels. Il y a plus d'un aspect de notre vie que nous aimerions garder privé, même sans avoir commis de délit. Que ce soit pour nous exprimer librement et explorer notre personnalité sans être jugés ou stigmatisés. Ou pour nos protéger de la surveillance, de la censure, de la manipulation et de l'usurpation d'identité, ou pire. C'est pour cela que les rideaux ont été inventés. Et des choses comme le secret bancaire, le privilège avocat-client, le secret de la correspondance, le vote à bulletin secret, le secret de la confession ou le secret médical.

Nous avons tendance à oublier que la vie privée est un droit fondamental humain. Elle est entérinée par le Conseil des droits de l'homme des Nations unies, le Pacte international relatif aux droits civils et politiques et un certain nombre de traités nationaux et internationaux.

Nous devons empêcher les entreprises et les gouvernements d'enregistrer de manière permanente nos conversations, nos souvenirs, notre localisation, notre historique médical, et beaucoup d'autres choses. Pour paraphraser [Geoffrey A. Fowler](https://www.washingtonpost.com/technology/2019/12/31/how-we-survive-surveillance-apocalypse/) du Washington Post : *« La confidentialité en ligne n'est pas morte, mais il faut être en colère et assez patient pour l'obtenir »*.

??? question "Sur les traces du roman d'Upton Sinclair de 1917 "Les bénéfices de la religion""

    *« Non seulement mon propre courrier fut ouvert, mais aussi le courrier de tous mes proches et amis – des gens résidant à des endroits aussi éloignés que la Californie et la Floride. Il me souvient du sourire poli de l'agent du gouvernement auprès de qui je me plaignais de ce sujet: "Si vous n'avez rien à cacher, vous n'avez rien à craindre." Ma réponse avait été que l'étude de nombreux cas de législation industrielle (ancêtre du droit du travail) m'avait enseigné les méthodes de l'agent provocateur. Il est très enclin à prendre de vraies preuves s'il peut en trouver; mais si tel n'est pas le cas, s'étant familiarisé avec les affaires de sa victime, il peut fabriquer des preuves qui seront convaincantes lorsqu'exploitées par la presse jaune (les tabloïds de l'époque). »*

??? question "Confidentialité, anonymat, sécurité — quelles différences ?"

    <center>

    | Concept | Description |
    | ------ | ------ |
    | **Confidentialité** |Il s'agit de ce qui maintient privés vos contenus confidentiels. Les autres peuvent savoir qui vous êtes, mais pas ce que vous pensez, dites ou faites. |
    | **Anonymat** |Il s'agit de ce qui dissimule votre identité. Les autres peuvent savoir ce que vous dites, pensez et faites, mais pas qui vous êtes. |
    | **Sécurité** |Il s'agit de l'usage d'outils destinés à protéger votre vie privée et votre anonymat. |

    </center>

    La sécurité renforcée implique souvent davantage de respect de la vie privée et de l'anonymat, mais implique aussi moins de confort. [Le bon équilibre dépend du modèle de menace qui vous convient](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf). Ce guide devrait vous être utile si votre objectif est un niveau raisonnable de confidentialité. Pas vraiment si vous souhaitez devenir un espion qui se dissimule de puissants adversaires ou si vous cherchez à enfreindre les lois.


<br>

<center> <img src="../../assets/img/separator_money.svg" alt="Les GAFAM et le capitalisme de surveillance" width="150px"></img> </center>

## L'ère des GAFAM

Les GAFAM excellent dans l'art de transformer leurs utilisateurs en produits. Les données privées sont un ingrédient essentiel de leur modèle commercial, qui est généralement construit autour de services « gratuits », de monopole, de pistage et de publicités ciblées.

Google et Facebook ont eu beaucoup de succès à ce jeu. Et tandis qu'Apple, Amazon, et Microsoft n'ont pas (entièrement) construit leur modèle autour de l’espionnage, ils sont loin d'être du côté de la confidentialité. En fait, ils ont énormément à gagner à faire affaire avec des courtiers en données et autres délinquants de la confidentialité. Ces cinq entreprises — souvent appelées les GAFAM — sont régulièrement confrontées à ou condamnées pour [mauvaises pratiques en matière de collecte et de suivi des données](https://www.gizmodo.com.au/2021/05/google-location-services-lawsuit/), [surveillance civile](https://www.theguardian.com/commentisfree/2021/may/18/amazon-ring-largest-civilian-surveillance-network-us), [violations des règles de protection de la vie privée](https://edps.europa.eu/press-publications/press-news/press-releases/2021/edps-opens-two-investigations-following-schrems_en), [évasion fiscale](https://www.theguardian.com/technology/2019/jan/03/google-tax-haven-bermuda-netherlands), [soucis d'antitrust](https://www.statista.com/chart/14752/eu-antitrust-fines-against-tech-companies/), [érosion des normes d'éthique](https://fr.wikipedia.org/wiki/Critiques_de_Facebook), [rapports de mauvaises conditions de travail](https://www.theatlantic.com/technology/archive/2019/11/amazon-warehouse-reports-show-worker-injuries/602530/), etc.

<center>
  <html>
   <embed src="../../assets/echarts/gafam_stats.html" style="width: 100%; margin-top:20px; height:400px">
  </html>
</center>

Pourtant, en dépit de – ou grâce à – ces pratiques douteuses, la valeur marchande des GAFAM flambe. En 2021, la valeur de ces entreprises a atteint près de 8000 milliards d'euros. Si les GAFAM étaient une nation, elles seraient la 3e plus grande économie du monde, juste après les États-Unis et la Chine. La valeur des GAFAM est désormais supérieure au produit intérieur brut de l'Allemagne et du Royaume-Uni – réunis !

*« L'information c'est le pouvoir. Mais comme pour tous les pouvoirs, il y a ceux qui veulent le garder pour eux tout seuls »*. Dès 2008, Aaron Swartz condamnait les entreprises privées qui centralisaient, numérisaient et verrouillaient l'information dans son controversé [« Guerilla Open Access Manifesto »](https://archive.org/details/GuerillaOpenAccessManifesto/). Les choses ne se sont pas arrangées.


??? question "Quelles informations les entreprises de la tech collectent-elles sur moi ?"

    <center> <img src="../../assets/img/what_info_tech_companies_collect.png" alt="data collection." width="100%"></img> </center>

    Publié par [TruePeopleSearch.net](https://truepeoplesearch.net/).


<br>

<center> <img src="../../assets/img/separator_compatibility.svg" alt="Surveillance de masse" width="150px"></img> </center>


## Luttez contre la surveillance de masse

De nombreux gouvernements réduisent les libertés individuelles. Issues de la peur des manifestations, du terrorisme ou des maladies, des lois d'urgence sont mises en place et demeurent. De plus en plus de pratiques intrusives dans la vie privée sont déployées à très grande échelle.

Pourtant, au lieu de protéger l'intérêt public, les données récoltées par des entreprises et gouvernements sont détournées pour surveiller la population, polariser le débat politique, et diviser l'opinion publique. Cette évolution affaiblit la Démocratie. L'Histoire nous a enseigné encore et encore que la surveillance de masse ne fait pas du monde un endroit plus sûr. Bien au contraire ! La surveillance, l'ingérence électorale et la censure sont les composantes de base de l'oppression et de la violence étatique.

Il est temps de lutter contre le capitalisme de surveillance et de préserver notre coexistence dans un monde de plus en plus numérisé. Benjamin Franklin serait d'accord: *« Ceux qui peuvent abandonner la liberté fondamentale pour obtenir un peu de sécurité temporaire ne méritent ni la liberté, ni la sécurité »*.


??? info "Dites-m'en plus sur la surveillance de masse"

    <center>

    | Menaces pour la Démocratie | Description |
    | ------ | ------ |
    | Programmes de surveillance mondiale |Suite à la guerre des États-Unis contre le terrorisme, plusieurs lanceurs d'alerte ont révélé l'existence de [programmes de surveillance mondiaux](https://fr.wikipedia.org/wiki/R%C3%A9v%C3%A9lations_d%27Edward_Snowden). Avec le soutien plus ou moins volontaire de méga-entreprises, les gouvernements à travers le monde ont commencé à collecter, stocker et partager des informations sur leurs citoyens : activité en ligne, appels téléphoniques, SMS, historique de position, etc. |
    |PRISM | PRISM, le programme de surveillance de la NSA, a été révélé pour la première fois par le Guardian et le Washington Post en 2013. Comme l'explique [The Verge](https://www.theverge.com/2013/7/17/4517480/nsa-spying-prism-surveillance-cheat-sheet/), c'est un outil pour collecter des données privées depuis les GAFAM et autres organisations. <br><br> <center><img src="../../assets/img/prism.png" align="center" alt="Prism" width="500px"></img></center> |
    | XKeyscore| Le programme de la NSA Xkeyscore a été révélé pour la première fois par le [Guardian](https://www.theguardian.com/world/2013/jul/31/nsa-top-secret-program-online-data) en 2013. Comme l'explique [The Intercept](https://theintercept.com/2015/07/01/nsas-google-worlds-private-communications/), c'est un outil pour la surveillance de masse, alimenté par le trafic Internet provenant des câbles de fibre optique. Pour citer Edward Snowden: *« Moi, assis à mon bureau, je pourrais mettre sur écoute n'importe qui, de vous ou votre comptable, à un juge fédéral ou même le président, si j'avais un email personnel »*. <br><br> <center><img src="../../assets/img/xkeyscore.jpeg" align="center" alt="Xkeyscore" width="500px"></img></center> |
    | Boundless Informant | Boundless Informant est l'un des outils d'analyse de mégadonnées de la NSA. En 2013, le [Guardian a publié la carte suivante](https://www.theguardian.com/world/2013/jun/08/nsa-boundless-informant-global-datamining), sur laquelle les pays varient du vert (surveillance moindre) au jaune et à l'orange, jusqu'au rouge (surveillance forte). L'[Atlantic Wire](https://www.theatlantic.com/national/archive/2013/06/nsa-datacenters-size-analysis/314364/) a estimé qu'en mars 2013 seulement, la NSA a récupéré et stocké 9,7 pétaoctets données. Il est difficile de se faire une idée de ce que représentent de tels chiffres, mais imaginez seulement que cette quantité de données correspond plus ou moins à l'ensemble de la programmation télévisuelle de la dernière décennie.. <br><br> <center><img src="../../assets/img/boundlessinformant.png" align="center" alt="Boundless informant" width="500px"></img></center> |
    | Cambridge Analytica |Les révélations sur le rôle de [Cambridge Analytica](https://fr.wikipedia.org/wiki/Cambridge_Analytica) lors de la campagne présidentielle aux États-Unis en 2016 ou le référendum au Royaume-Uni sur l'appartenance à l'Union Européenne, illustrent comment l'utilisation abusive des données privées et les algorithmes biaisés influencent le processus démocratique et alimentent le matraquage politique. <br><br> <center><img src="../../assets/img/cambridge_analytica.png" align="center" alt="Cambridge Analytica" width="250px"></img></center> |
    | Pister la pandémie |De nombreux pays ont commencé à tracer (ou pister) les citoyens en se basant sur la localisation de leur téléphone, pour contenir l'épidémie de [COVID-19](https://www.top10vpn.com/research/covid-19-digital-rights-tracker/). Les données sont collectées par le biais d'applications, ou fournies par les opérateurs télécoms ou des firmes de la tech comme [Google et Apple](https://www.theverge.com/2020/4/10/21216715/apple-google-coronavirus-covid-19-contact-tracing-app-details-use/). Derrière ce qui apparaît comme une noble cause, se trouve une technologie qui permet le pistage numérique, la surveillance physique et la censure. En fait, il n'a pas fallu longtemps avant que les [premières failles de confidentialité ne soient découvertes](https://themarkup.org/privacy/2021/04/27/google-promised-its-contact-tracing-app-was-completely-private-but-it-wasnt).|

    </center>


<center>
<img align="center" src="https://imgs.xkcd.com/comics/privacy_opinions.png" width="550px" alt="Pourquoi la vie privée est importante"></img>
</center>

<br/>

---
template: main.html
title: Comment chiffrer vos messages ?
description: Protégez vos messages grâce au chiffrage de bout en bout. Choisissez Signal, Element, Jami, ou Briar.
---

# Chiffrez vos messages (Perfect Forward Secrecy, Zero Knowledge Encryption & plus)

!!! level "Dernière mise à jour: mars 2022. Destiné aux débutants et utilisateurs expérimentés. Certaines compétences techniques peuvent être requises."

<div align="center">
<img src="../../assets/img/email_2.png" alt="Applications de messagerie chiffrées" width="400px"></img>
</div>

Choisissez des applications de messagerie dignes de confiance. Évaluez soigneusement des aspects tels que les fonctionnalités disponibles, les technologies de chiffrement ou l'emplacement des serveurs — en effet, la législation sur la protection de la vie privée change d'un pays à l'autre. Ce chapitre donne un (bref) aperçu des applications de messagerie qui respectent la vie privée.

??? tip "Montrez-moi une comparaison d'applications de messagerie respectant la vie privée."

    <center>

    | | Signal | Element | Jami | Briar |
    | ------ | :------: | :------: | :------: | :------: |
    | Date de création | 2014 | 2016 | 2005/2017^1^ | 2018 |
    | Juridiction | US | UK | Canada |N/A^2^ |
    | Financement | Diverses fondations^3^ | Matrix Foundation, New Vector Limited | Free Software Foundation^4^ |Dons |
    | Architecture | Centralisée | Décentralisée | De pair à pair | De pair à pair |
    | Open source | [Oui](https://github.com/signalapp/)^17^ | [Oui](https://github.com/vector-im/) | [Oui](https://git.jami.net/savoirfairelinux) |[Oui](https://code.briarproject.org/briar/briar/) |
    | Protocole | Signal | Matrix | SIP/OpenDHT |Bramble |
    | Chiffrement de bout en bout | <span style="color:green">✔</span> (par défault)^5^ | <span style="color:green">✔</span> (pas par défault)^6^ | <span style="color:green">✔</span> (par défault)^7^ |<span style="color:green">✔</span> (par défault)^8^ |
    | « Perfect forward secrecy » | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:orange">?</span>^10^ |
    | Security audit | [2016](https://eprint.iacr.org/2016/1013.pdf), [2017](https://hal.inria.fr/hal-01575923/file/KobeissiBhargavanBlanchetEuroSP17.pdf) | <span style="color:red">✗</span>^11^ | <span style="color:red">✗</span> |[2017](https://briarproject.org/raw/BRP-01-report.pdf) |
    | Pas de collecte de données | <span style="color:green">✔</span> | <span stylShow me the step-by-step guide for e="color:orange">?</span>^12^ | <span style="color:red">✗</span>^13^ |<span style="color:green">✔</span> |
    | Conformité avec le GDPR | <span style="color:red">✗</span>^14^ | <span style="color:orange">?</span>^15^ | <span style="color:orange">?</span> | <span style="color:orange">?</span> |
    | Inscription anonyme | <span style="color:red">✗</span>^16^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:green">✔</span> |
    | Messages instantanés | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:green">✔</span> |
    | Appels vidéo/vocaux | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:red">✗</span> |
    | Client Windows | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Client macOS | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Client Linux | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Client Android | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Client iOS | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    </center>

    1. *Jami s'appuie sur GNU/Ring et SFLphone, qui existent respectivement depuis 2015 et 2005.*
    2. *Briar repose sur une architecture de pair à pair, où les utilisateurs se connectent directement sans passer par des serveurs.*
    3. *Signal Foundation, Freedom of the Press Foundation, Shuttleworth Foundation, Knight Foundation, Open Technology Fund.*
    4. *En novembre 2016, Jami a été intégré au projet GNU, qui est financé par la Free Software Foundation.*
    5. *Signal, AES-256, Curve25519, HMAC-SHA256.*
    6. *Olm/Megaolm, AES-256, Curve25519, HMAC-SHA256.*
    7. *TLS 1.3 avec clés RSA et certificats X.509.*
    8. *Bramble, AES-256.*
    9. *Signal/Axolotl, AES-256.*
    10. *Bien que Briar offre une confidentialité de transmission parfaite (en anglais, « perfect forward secrecy »), il faut noter que le « déni plausible » n'est pas assuré pour les communications entre plusieurs personnes, comme les discussions de groupe.*
    11. *Au moment d'écrire ces lignes, aucun audit de sécurité complet n'a été réalisé sur Element. En 2016, NCC Group Security Services [a examiné l'algorithme de chiffrement de Matrix](https://www.nccgroup.com/globalassets/our-research/us/public-reports/2016/november/ncc_group_olm_cryptogrpahic_review_2016_11_01.pdf).*
    12. *Cela dépend du serveur Matrix. Les utilisatrices et utilisateurs peuvent choisir entre plusieurs serveurs indépendants, certains enregistrent les adresses IP, d'autres non. Gardez à l'esprit que l'administrateur du serveur Matrix peut accéder à presque toutes les données des usagers (par exemple, l'identifiant, le nom d'utilisateur, les mots de passe, l'e-mail, le numéro de téléphone, des informations sur les périphériques, le profil d'utilisation, l'adresse IP, etc.), même si le contenu du message reste chiffré.*
    13. *Les adresses IP peuvent être dévoilées si elles ne sont pas soigneusement protégées par [Tor](https://gofoss.net/fr/tor/) ou [VPN](https://gofoss.net/fr/vpn/).*
    14. *Selon Signal, il s'agit d'un [« processus en évolution »](https://support.signal.org/hc/fr/articles/360007059412-Signal-and-the-General-Data-Protection-Regulation-GDPR-).*
    15. *Cela dépend du serveur Matrix, assurez-vous de vérifier la politique de confidentialité. Gardez à l'esprit que l'administrateur du serveur Matrix peut accéder à presque toutes les données des usagers (par exemple, l'identifiant, le nom d'utilisateur, les mots de passe, l'e-mail, le numéro de téléphone, des informations sur les périphériques, le profil d'utilisation, l'adresse IP, etc.), même si le contenu du message reste chiffré.*
    16. *Un numéro de téléphone portable est requis pour s'inscrire.*
    17. *Contient des bibliothèques propriétaires. La publication du code source côté serveur [a été retardée](https://github.com/signalapp/Signal-Android/issues/11101) dans le passé.*


??? question "Qu'en est-il de XMPP ?"

    [XMPP](https://xmpp.org/) est génial ! Nous avons cependant choisi de ne pas couvrir les clients XMPP dans ce guide pour plusieurs raisons :

    * *Le chiffrement de bout en bout n'est pas activé par défaut* : bien que XMPP puisse en théorie chiffrer votre communication, cette fonctionnalité n'est pas activée par défaut. En raison de la diversité des serveurs et des clients utilisés, votre communication peut donc être chiffrée ou non. En outre, les discussions de groupe ne prennent souvent pas en charge le chiffrement. Par défaut, les utilisateurs de XMPP doivent donc supposer que leurs discussions ne sont pas chiffrées.

    * *XMPP collecte des méta-données* : comme Matrix, XMPP ne poursuit pas le principe de « zéro connaissance » et collecte des méta-données. En effet, la gestion des comptes utilisateurs est assurée par les administrateurs du serveur XMPP. En utilisant un serveur XMPP public, vous confiez donc certaines de vos données à ces administrateurs. Même si les communications peuvent être chiffrées, les administrateurs sont potentiellement en mesure de conserver une copie de toutes vos communications (chiffrées), de consigner les adresses IP ou d'accéder aux données (non chiffrées) telles que les noms d'utilisateur, les mots de passe, les adresses électroniques, les numéros de téléphone, les fichiers multimédias, les informations sur les périphériques, les listes de contacts, les profils d'utilisation, les adhésions à des groupes, etc. Même si vous auto-hébergez votre propre serveur, les administrateurs des serveurs XMPP participant à une conversation peuvent accéder à ces métadonnées.

    Ceci étant dit, il existe une communauté XMPP très active qui maintient plusieurs clients open source. Ceux-ci reposent sur l'architecture décentralisée de XMPP, qui permet aux utilisatrices et aux utilisateurs de choisir entre différents serveurs ou même d'auto-héberger leur propre instance :

    * [Gajim](https://gajim.org/fr/), une application de messagerie et d'appel vidéo, disponible sur tous les environnements de bureau
    * [Conversations](https://conversations.im/), une application de messagerie et d'appel vidéo pour Android



<br>

<center> <img src="../../assets/img/separator_signal.svg" alt="Signal" width="150px"></img> </center>

## Signal

[Signal](https://signal.org/fr/) est une application simple et puissante intégrant messagerie instantanée et appels vocaux/vidéos. Signal est disponible sur tous les ordinateurs de bureau et les appareils mobiles. L'application est gratuite, [open source](https://github.com/signalapp/) et repose sur une architecture centralisée, gérée par la Signal Foundation. Signal offre un chiffrement de bout en bout pour les contenus textes, voix, vidéos et images, ainsi que pour les fichiers. Cela signifie l'application ne peut accéder au contenu des messages. Signal adhère également au principe de « zéro connaissance », ce qui signifie que l'application ne cherche pas à accéder au réseau social ou au profil d'utilisation. Vous trouverez ci-dessous des instructions plus détaillées.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        ### Installation & inscription

        | Instructions | Description |
        | ------ | ------ |
        |Installez Signal |Installez Signal à partir du : <br>• [Google Play Store](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=fr)<br>• [Aurora Store](https://auroraoss.com/)<br>• [site web de Signal](https://signal.org/android/apk/). |
        |Aucun traqueur |L'application [contient 0 traqueur et nécessite 67 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/org.thoughtcrime.securesms/latest/). À titre de comparaison:<br>• TikTok: 16 traqueurs, 76 autorisations<br>• Snapchat: 2 traqueurs, 44 autorisations<br>• WhatsApp: 1 traqueur, 57 autorisations |
        |Pas de GCM |Notez que Signal sur Android peut gérer les notifications sans le « Google Cloud Messaging (GCM) » et se reporte sur les « websockets » si GCM n'est pas disponible. C'est très pratique si vous cherchez à [dé-googler votre téléphone](https://gofoss.net/fr/intro-free-your-phone/). |
        |Inscrivez-vous |• Lancez l'appli et suivez les instructions à l'écran <br>• Lorsque vous y êtes invité, saisissez un numéro de téléphone et appuyez sur `Enregistrer` <br>• Vous recevrez un code de vérification par SMS, il devrait être détecté automatiquement <br>• Sinon, vous pouvez demander à être appelé. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Touchez la `zone de saisie de texte` et rédigez votre message <br>• Appuyez sur le bouton bleu `Envoyer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat en tête-à-tête <br>• Appuyez sur le nom de votre contact <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère |
        |Textos et messages multimédias | • Sur Android, Signal peut également envoyer des textos ou messages multimédias non chiffrés par l'intermédiaire de votre fournisseur de services mobiles <br>• Allez dans `Paramètres ‣ Conversations ‣ Textos et messages multimédias ‣ Utiliser comme appli de textos par défaut` <br>• Appuyez longuement sur le bouton `Envoyer` dans n'importe quelle conversation <br>• Sélectionnez le bouton gris pour envoyer un texto ou un message multimédia non chiffrés |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat de groupe |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez `Nouveau groupe` <br>• Ajoutez des contacts existants ou saisissez de nouveaux numéros <br>• Appuyez sur le bouton `Suivant` <br>• Saisissez un nom pour le groupe <br>• Appuyez sur le bouton `Créer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat de groupe <br>• Appuyez sur le nom du groupe <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère |
        |Textos et messages multimédias |• Sur Android, Signal peut également créer des groupes avec des messages multimédias non chiffrés <br>• Ces messages sont envoyés par votre fournisseur de téléphonie mobile <br>• Le bouton `Envoyer` apparaîtra gris au lieu de bleu |
        |Lancez un appel vocal ou vidéo en groupe |• Lancez l'appli <br>• Créez un nouveau groupe de discussion ou ouvrez un groupe existant <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur <br>• Sélectionnez `Démarrez un appel` ou `Joignez un appel` <br>• Pour passer en appel vocal, désactivez la caméra |

        ### Authentification, NIP Signal et sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Authentification |• Assurez-vous que vous communiquez avec les bonnes personnes <br>• Signal peut authentifier vos contreparties à partir d'une empreinte numérique <br>• Lancez l'appli <br>• Ouvrez un tchat existant <br>• Appuyez sur le nom de votre contact <br>• Sélectionnez `Afficher le numéro de sécurité` <br>• Comparez la séquence de chiffres avec celle qui s'affiche sur le téléphone de votre contact. S'ils correspondent, appuyez sur `Marquer comme confirmé` <br>• Vous pouvez également scanner le code QR qui apparaît sur le téléphone de votre contact <br>• Après une vérification réussie, une coche s'affiche sous le nom de votre contact <br>• En cas de modification du numéro de vérification, par exemple si vous ou votre contact changez de téléphone ou réinstallez l'application, une notification sera envoyée et le processus d'authentification devra être répété |
        |NIP Signal |• Vous pouvez définir un code NIP pour récupérer votre profil, vos paramètres ou vos contacts en cas de perte ou de changement d'appareil <br>• Le tout sans révéler votre réseau social à Signal <br>• Allez dans `Paramètres ‣ Compte ‣ Changer votre NIP` pour définir et gérer votre NIP <br>• Veillez à choisir un [code NIP fort et unique](https://gofoss.net/fr/passwords/) et conservez-le en toute sécurité <br>• Signal ne peut pas réinitialiser le code NIP pour vous. Si vous l'oubliez, certaines informations de votre compte ne pourront être rétablies. En outre, vous devrez potentiellement attendre jusqu'à 7 jours avant de créer un nouveau compte |
        |Sauvegarde des tchats |• Vos messages sont chiffrés et ne sont stockés que sur vos appareils <br>• Si vous perdez votre appareil ou effacez vos messages par erreur, ils seront perdus à jamais <br>• Afin de pouvoir rétablir vos messages ou les transférer vers un autre appareil, vous devez créer des sauvegardes <br><br>*Sauvegarde*<br>• Allez dans `Paramètres ‣ Conversations ‣ Sauvegardes de conversations ‣ Activer` <br>• Choisissez un répertoire où enregistrer le fichier de sauvegarde <br>• [Stockez en toute sécurité](https://gofoss.net/fr/passwords/) la phrase de passe à 30 chiffres affichée ; elle est requise pour rétablir les sauvegardes <br>• Sélectionnez `Activez les sauvegardes` <br>• Cliquez sur `Créer une sauvegarde` <br>• Déplacez le fichier de sauvegarde ainsi généré vers vos [supports de sauvegarde](https://gofoss.net/fr/backups/) <br><br>*Rétablissement*• Déplacez le fichier de sauvegarde sur votre appareil <br>• (Ré-)installez Signal <br>• Sélectionnez `Restaurer une sauvegarde` <br>• Saisissez votre phrase de passe de 30 caractères <br>• Saisissez votre numéro de téléphone pour l'inscription |
        |Sauvegarde des fichiers multimédias |• Les photos, les vidéos, les fichiers audio, etc., sont chiffrés et stockés uniquement sur vos appareils <br>• Si vous perdez vos appareils ou effacez vos fichiers par erreur, ils seront perdus à jamais <br>• [Créez des sauvegardes](https://gofoss.net/fr/backups/)! <br>• Allez dans `Paramètres ‣ Données et mémoire ‣ Gérer la mémoire ‣ État de la mémoire` <br>• Sélectionnez les fichiers que vous souhaitez sauvegarder <br>• Appuyez sur le bouton `Enregistrer` et sélectionnez `Oui` pour déchiffrer et exporter vos fichiers multimédias de Signal <br>• Déplacez les fichiers multimédias ainsi exportés vers vos [supports de sauvegarde](https://gofoss.net/fr/backups/) |


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        ### Installation & inscription

        |Installez Signal |Installez Signal à partir du [App Store](https://apps.apple.com/fr/app/signal-private-messenger/id874139669/). |
        |Inscrivez-vous |• Lancez l'appli et suivez les instructions à l'écran <br>• Lorsque vous y êtes invité, saisissez un numéro de téléphone et appuyez sur `Enregistrer` <br>• Vous recevrez un code de vérification par SMS, il devrait être détecté automatiquement <br>• Sinon, vous pouvez demander à être appelé. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Touchez la `zone de saisie de texte` et rédigez votre message <br>• Appuyez sur le bouton bleu `Envoyer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat en tête-à-tête <br>• Appuyez sur le nom de votre contact <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère. |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat de groupe |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez `Nouveau groupe` <br>• Ajoutez des contacts existants ou saisissez de nouveaux numéros <br>• Appuyez sur le bouton `Suivant` <br>• Saisissez un nom pour le groupe <br>• Appuyez sur le bouton `Créer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat de groupe <br>• Appuyez sur le nom du groupe <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère |
        |Lancez un appel vocal ou vidéo en groupe |• Lancez l'appli <br>• Créez un nouveau groupe de discussion ou ouvrez un groupe existant <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur <br>• Sélectionnez `Démarrez un appel` ou `Joignez un appel` <br>• Pour passer en appel vocal, désactivez la caméra |

        ### Authentification, NIP Signal et sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Authentification |• Assurez-vous que vous communiquez avec les bonnes personnes <br>• Signal peut authentifier vos contreparties à partir d'une empreinte numérique <br>• Lancez l'appli <br>• Ouvrez un tchat existant <br>• Appuyez sur le nom de votre contact <br>• Sélectionnez `Afficher le numéro de sécurité` <br>• Comparez la séquence de chiffres avec celle qui s'affiche sur le téléphone de votre contact. S'ils correspondent, appuyez sur `Marquer comme confirmé` <br>• Vous pouvez également scanner le code QR qui apparaît sur le téléphone de votre contact <br>• Après une vérification réussie, une coche s'affiche sous le nom de votre contact <br>• En cas de modification du numéro de vérification, par exemple si vous ou votre contact changez de téléphone ou réinstallez l'application, une notification sera envoyée et le processus d'authentification devra être répété |
        |NIP Signal |• Vous pouvez définir un code NIP pour récupérer votre profil, vos paramètres ou vos contacts en cas de perte ou de changement d'appareil <br>• Le tout sans révéler votre réseau social à Signal <br>• Allez dans `Paramètres ‣ Compte ‣ Changer votre NIP` pour définir et gérer votre NIP <br>• Veillez à choisir un [code NIP fort et unique](https://gofoss.net/fr/passwords/) et conservez-le en toute sécurité <br>• Signal ne peut pas réinitialiser le code NIP pour vous. Si vous l'oubliez, certaines informations de votre compte ne pourront être rétablies. En outre, vous devrez potentiellement attendre jusqu'à 7 jours avant de créer un nouveau compte |
        |Sauvegarde des fichiers multimédias |• Les photos, les vidéos, les fichiers audio, etc., sont chiffrés et stockés uniquement sur vos appareils <br>• Si vous perdez vos appareils ou effacez vos fichiers par erreur, ils seront perdus à jamais <br>• [Créez des sauvegardes](https://gofoss.net/fr/backups/)! <br>• Ouvrez un tchat <br>• Appuyez sur le nom du contact dans le menu supérieur pour afficher les paramètres <br>• Sélectionnez `Tous les médias` <br>• Sélectionnez les fichiers que vous souhaitez sauvegarder <br>• Appuyez sur le bouton `Partager` et sélectionnez `Enregistrer l'image` ou `Enregistrer la vidéo` pour déchiffrer et exporter vos fichiers multimédias de Signal <br>• Déplacez les fichiers multimédias ainsi exportés vers vos [supports de sauvegarde](https://gofoss.net/fr/backups/) |


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        ### Installation & établissement de lien

        | Instructions | Description |
        | ------ | ------ |
        |Installation |• Assurez-vous que Signal soit bien installé sur votre smartphone <br>• Téléchargez le [client Windows Desktop](https://signal.org/fr/download/windows/) de signal, qui est basé sur Electron <br>• Cliquez sur le bouton `Exécuter` <br>• Suivez l'assistant d'installation |
        |Établissement de lien |• Lancez l'appli Signal sur votre smartphone <br>• Allez dans `Paramètres ‣ Appareils reliés` <br>• Appuyez sur le bouton `+` (Android) ou `Lier un nouvel appareil` (iPhone) <br>• Utilisez l'appli Signal de votre téléphone pour scanner le code QR affiché sur votre ordinateur Windows <br>• Saisissez un nom pour le périphérique lié <br>• Cliquez sur `Terminer` |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Lancez une nouvelle conversation ou sélectionnez un tchat existant <br>• Rédigez votre message <br>• Cliquez sur `ENTRER` pour envoyer votre message |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat en tête-à-tête <br>• Appuyez sur le nom de votre contact <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère. |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran : lors d'un appel vidéo, cliquez sur le bouton `Démarrer la présentation` et sélectionnez votre écran entier ou une fenêtre spécifique |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat de groupe |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez `Nouveau groupe` <br>• Ajoutez des contacts existants ou saisissez de nouveaux numéros <br>• Appuyez sur le bouton `Suivant` <br>• Saisissez un nom pour le groupe <br>• Appuyez sur le bouton `Créer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat de groupe <br>• Appuyez sur le nom du groupe <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère |
        |Lancez un appel vocal ou vidéo en groupe |• Lancez l'appli <br>• Créez un nouveau groupe de discussion ou ouvrez un groupe existant <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur <br>• Sélectionnez `Démarrez un appel` ou `Joignez un appel` <br>• Partage d'écran : lors d'un appel vidéo, cliquez sur le bouton `Démarrer la présentation` et sélectionnez votre écran entier ou une fenêtre spécifique <br>• Pour passer en appel vocal, désactivez la caméra |

        ### Authentification & sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Authentification |• Assurez-vous que vous communiquez avec les bonnes personnes <br>• Signal peut authentifier vos contreparties à partir d'une empreinte numérique <br>• Lancez l'appli <br>• Ouvrez un tchat existant <br>• Appuyez sur le nom de votre contact <br>• Sélectionnez `Afficher le numéro de sécurité` <br>• Comparez la séquence de chiffres avec celle qui s'affiche sur le téléphone de votre contact <br>• S'ils correspondent, appuyez sur `Marquer comme confirmé` <br>• Après une vérification réussie, une coche s'affiche sous le nom de votre contact <br>• En cas de modification du numéro de vérification, par exemple si vous ou votre contact changez de téléphone ou réinstallez l'application, une notification sera envoyée et le processus d'authentification devra être répété |
        |Sauvegarde des fichiers multimédias |• Les photos, les vidéos, les fichiers audio, etc., sont chiffrés et stockés uniquement sur vos appareils <br>• Si vous perdez vos appareils ou effacez vos fichiers par erreur, ils seront perdus à jamais <br>• [Créez des sauvegardes](https://gofoss.net/fr/backups/)! <br>• Ouvrez un tchat <br>• Appuyez sur le nom du contact dans le menu supérieur pour afficher les paramètres <br>• Sélectionnez `Voir les médias récents` <br>• Sélectionnez les fichiers que vous souhaitez sauvegarder <br>• Appuyez sur le bouton `Enregistrer` pour déchiffrer et exporter vos fichiers multimédias de Signal <br>• Déplacez les fichiers multimédias ainsi exportés vers vos [supports de sauvegarde](https://gofoss.net/fr/backups/) |


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        ### Installation & établissement de lien

        | Instructions | Description |
        | ------ | ------ |
        |Installation |• Assurez-vous que Signal soit bien installé sur votre smartphone <br>• Téléchargez le [client macOS Desktop](https://signal.org/fr/download/macos/) de signal, qui est basé sur Electron <br>• Le programme devrait s'ouvrir tout seul et monter un nouveau volume contenant l'appli Signal <br>• Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Signal vers le dossier Application <br>• Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Signal vers le dock |
        |Établissement de lien |• Lancez l'appli Signal sur votre smartphone <br>• Allez dans `Paramètres ‣ Appareils reliés` <br>• Appuyez sur le bouton `+` (Android) ou `Lier un nouvel appareil` (iPhone) <br>• Utilisez l'appli Signal de votre téléphone pour scanner le code QR affiché sur votre appareil macOS <br>• Saisissez un nom pour le périphérique lié <br>• Cliquez sur `Terminer` |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Lancez une nouvelle conversation ou sélectionnez un tchat existant <br>• Rédigez votre message <br>• Cliquez sur `ENTRER` pour envoyer votre message |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat en tête-à-tête <br>• Appuyez sur le nom de votre contact <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère. |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran : lors d'un appel vidéo, cliquez sur le bouton `Démarrer la présentation` et sélectionnez votre écran entier ou une fenêtre spécifique |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat de groupe |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez `Nouveau groupe` <br>• Ajoutez des contacts existants ou saisissez de nouveaux numéros <br>• Appuyez sur le bouton `Suivant` <br>• Saisissez un nom pour le groupe <br>• Appuyez sur le bouton `Créer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat de groupe <br>• Appuyez sur le nom du groupe <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère |
        |Lancez un appel vocal ou vidéo en groupe |• Lancez l'appli <br>• Créez un nouveau groupe de discussion ou ouvrez un groupe existant <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur <br>• Sélectionnez `Démarrez un appel` ou `Joignez un appel` <br>• Partage d'écran : lors d'un appel vidéo, cliquez sur le bouton `Démarrer la présentation` et sélectionnez votre écran entier ou une fenêtre spécifique <br>• Pour passer en appel vocal, désactivez la caméra |

        ### Authentification & sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Authentification |• Assurez-vous que vous communiquez avec les bonnes personnes <br>• Signal peut authentifier vos contreparties à partir d'une empreinte numérique <br>• Lancez l'appli <br>• Ouvrez un tchat existant <br>• Appuyez sur le nom de votre contact <br>• Sélectionnez `Afficher le numéro de sécurité` <br>• Comparez la séquence de chiffres avec celle qui s'affiche sur le téléphone de votre contact <br>• S'ils correspondent, appuyez sur `Marquer comme confirmé` <br>• Après une vérification réussie, une coche s'affiche sous le nom de votre contact <br>• En cas de modification du numéro de vérification, par exemple si vous ou votre contact changez de téléphone ou réinstallez l'application, une notification sera envoyée et le processus d'authentification devra être répété |
        |Sauvegarde des fichiers multimédias |• Les photos, les vidéos, les fichiers audio, etc., sont chiffrés et stockés uniquement sur vos appareils <br>• Si vous perdez vos appareils ou effacez vos fichiers par erreur, ils seront perdus à jamais <br>• [Créez des sauvegardes](https://gofoss.net/fr/backups/)! <br>• Ouvrez un tchat <br>• Appuyez sur le nom du contact dans le menu supérieur pour afficher les paramètres <br>• Sélectionnez `Voir les médias récents` <br>• Sélectionnez les fichiers que vous souhaitez sauvegarder <br>• Appuyez sur le bouton `Enregistrer` pour déchiffrer et exporter vos fichiers multimédias de Signal <br>• Déplacez les fichiers multimédias ainsi exportés vers vos [supports de sauvegarde](https://gofoss.net/fr/backups/) |


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        ### Installation & établissement de lien

        | Instructions | Description |
        | ------ | ------ |
        |Installation |• Assurez-vous que Signal soit bien installé sur votre smartphone <br>• Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal` <br>• Installez la clé de signature : <br>`wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg` <br>`cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null` <br>• Ajoutez Signal à la liste des dépôts : <br> `echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list` <br>• Mettez à jour la base de données des paquets et installez Signal : <br> `sudo apt update && sudo apt install signal-desktop` |
        |Établissement de lien |• Lancez l'appli Signal sur votre smartphone <br>• Allez dans `Paramètres ‣ Appareils reliés` <br>• Appuyez sur le bouton `+` (Android) ou `Lier un nouvel appareil` (iPhone) <br>• Utilisez l'appli Signal de votre téléphone pour scanner le code QR affiché sur votre appareil Linux <br>• Saisissez un nom pour le périphérique lié <br>• Cliquez sur `Terminer` |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Lancez une nouvelle conversation ou sélectionnez un tchat existant <br>• Rédigez votre message <br>• Cliquez sur `ENTRER` pour envoyer votre message |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat en tête-à-tête <br>• Appuyez sur le nom de votre contact <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère. |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez un contact ou saisissez un numéro <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran : lors d'un appel vidéo, cliquez sur le bouton `Démarrer la présentation` et sélectionnez votre écran entier ou une fenêtre spécifique |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat de groupe |• Lancez l'appli <br>• Appuyez sur le bouton `Rédiger` <br>• Sélectionnez `Nouveau groupe` <br>• Ajoutez des contacts existants ou saisissez de nouveaux numéros <br>• Appuyez sur le bouton `Suivant` <br>• Saisissez un nom pour le groupe <br>• Appuyez sur le bouton `Créer` |
        |Messages éphémères |• Signal permet de définir un délai après lequel les messages disparaissent de vos appareils <br>• Pour ceci, ouvrez un tchat de groupe <br>• Appuyez sur le nom du groupe <br>• Activez l'option `Messages éphémères` <br>• Définissez un délai pouvant aller jusqu'à une semaine <br>• Une fois activé, une icône de minuterie sera visible à côté de chaque message éphémère |
        |Lancez un appel vocal ou vidéo en groupe |• Lancez l'appli <br>• Créez un nouveau groupe de discussion ou ouvrez un groupe existant <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur <br>• Sélectionnez `Démarrez un appel` ou `Joignez un appel` <br>• Partage d'écran : lors d'un appel vidéo, cliquez sur le bouton `Démarrer la présentation` et sélectionnez votre écran entier ou une fenêtre spécifique <br>• Pour passer en appel vocal, désactivez la caméra |

        ### Authentification & sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Authentification |• Assurez-vous que vous communiquez avec les bonnes personnes <br>• Signal peut authentifier vos contreparties à partir d'une empreinte numérique <br>• Lancez l'appli <br>• Ouvrez un tchat existant <br>• Appuyez sur le nom de votre contact <br>• Sélectionnez `Afficher le numéro de sécurité` <br>• Comparez la séquence de chiffres avec celle qui s'affiche sur le téléphone de votre contact <br>• S'ils correspondent, appuyez sur `Marquer comme confirmé` <br>• Après une vérification réussie, une coche s'affiche sous le nom de votre contact <br>• En cas de modification du numéro de vérification, par exemple si vous ou votre contact changez de téléphone ou réinstallez l'application, une notification sera envoyée et le processus d'authentification devra être répété |
        |Sauvegarde des fichiers multimédias |• Les photos, les vidéos, les fichiers audio, etc., sont chiffrés et stockés uniquement sur vos appareils <br>• Si vous perdez vos appareils ou effacez vos fichiers par erreur, ils seront perdus à jamais <br>• [Créez des sauvegardes](https://gofoss.net/fr/backups/)! <br>• Ouvrez un tchat <br>• Appuyez sur le nom du contact dans le menu supérieur pour afficher les paramètres <br>• Sélectionnez `Voir les médias récents` <br>• Sélectionnez les fichiers que vous souhaitez sauvegarder <br>• Appuyez sur le bouton `Enregistrer` pour déchiffrer et exporter vos fichiers multimédias de Signal <br>• Déplacez les fichiers multimédias ainsi exportés vers vos [supports de sauvegarde](https://gofoss.net/fr/backups/) |


<div style="margin-top:-20px">
</div>

??? warning "Signal est chiffré, mais pas anonyme"

    Signal met en place plusieurs mesures pour assurer le principe de « zéro connaissance » et collecter le moins de métadonnées possible : [« Private-Contact-Discovery](https://signal.org/blog/private-contact-discovery/), [« Sealed-Sender »](https://signal.org/blog/sealed-sender/), [« Signal-PIN »](https://signal.org/blog/signal-pins/), [« Secure Value Recovery »](https://signal.org/blog/secure-value-recovery/), etc. N'oubliez cependant pas que malgré ces mesures de confidentialité, l'utilisation de Signal n'est pas anonyme puisque l'application accède à votre numéro de téléphone.

<br>

<center> <img src="../../assets/img/separator_element.svg" alt="Element" width="150px"></img> </center>

## Element

[Element](https://element.io/) est une application de messagerie instantanée et d'appels vocaux/vidéos. Element est [open source](https://github.com/vector-im/) et disponible sur tous les ordinateurs de bureau et appareils mobiles. L'application est basée sur le [protocole Matrix](https://matrix.org/) et s'appuie sur une architecture décentralisée. Aucun numéro de téléphone n'est requis pour s'inscrire. Les utilisateurs peuvent se connecter à plusieurs serveurs publics ou même auto-héberger leur propre serveur. Element prend en charge le chiffrement de bout en bout, ce qui signifie que le contenu de vos messages ne peut être consulté par des intermédiaires. Vous trouverez ci-dessous des instructions plus détaillées.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Installer Element |Installez Element à partir: <br>• du [Google Play Store](https://play.google.com/store/apps/details?id=im.vector.app&hl=fr)<br>• d'[Aurora Store](https://auroraoss.com/)<br>• de [F-Droid](https://f-droid.org/fr/packages/im.vector.app/) |
        |Aucun traqueur |L'appli [contient 0 traqueur et nécessite 37 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/im.vector.app/latest/). À titre de comparaison:<br>• TikTok: 16 traqueurs, 76 autorisations<br>• Snapchat: 2 traqueurs, 44 autorisations <br>• WhatsApp: 1 traqueur, 57 autorisations |
        |Pas de GCM |Notez que la version F-Droid de l'appli Element peut gérer les notifications sans le « Google Cloud Messaging (GCM) ». C'est très pratique si vous cherchez à [dé-googler votre téléphone](https://gofoss.net/fr/intro-free-your-phone/). |

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Chaque utilisatrice et utilisateur de Matrix dispose d'un identifiant unique, qui fonctionne de manière similaire à une [adresse électronique](https://gofoss.net/fr/encrypted-emails/) et ressemble à ceci : `@utilisatrice:nomduserveur.net`. Vous pouvez créer un nouvel identifiant Matrix auprès du fournisseur de votre choix, ou même devenir votre propre fournisseur en auto-hébergeant Matrix. |
        |Ouvrez Element |Ouvrez l'appli et cliquez sur `Démarrer`. |
        |Sélectionnez un serveur |Choisissez le serveur par défaut [matrix.org](https://matrix.org), ou tout autre serveur public de votre choix. Une sélection peut être trouvée ici : <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Inscrivez-vous |Cliquez sur `S'inscrire` et saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Ces informations d'identification sont nécessaires pour se connecter à Element. |
        |Politique de confidentialité |Vérifiez la politique de confidentialité du serveur et cliquez sur `Accepter`. |
        |Vérification de votre adresse électronique |Saisissez une adresse électronique existante ou une [adresse électronique jetable](https://gofoss.net/fr/cloud-providers/#other-cloud-services) afin d'authentifier votre compte. Ouvrez le lien dans le courriel de confirmation pour finaliser l'inscription. |

        ### Clé de sécurité

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |*Cette étape n'est requise qu'après la première connexion*. <br> Element utilise un chiffrement de bout en bout. Pour être sûr de toujours avoir accès à vos messages depuis n'importe quel appareil, vous devez générer une clé dite de sécurité. |
        |Paramétrage |Allez dans `Paramètres ‣ Sécurité et vie privée ‣ Sauvegarde sécurisée ‣ Activer la sauvegarde sécurisée`. |
        |Phrase de sécurité |• Sélectionnez `Saisir une phrase de sécurité` et entrez une [phrase de sécurité forte et unique](https://gofoss.net/fr/passwords/) <br>• Cette phrase de sécurité protégera votre clé de sécurité. <br>• *Veillez à ne pas utiliser le mot de passe de votre compte Element !* <br>• Appuyez sur `Continuer`, confirmez votre phrase de sécurité et appuyez de nouveau sur `Continuer`. |
        |Clé de sécurité |Element va maintenant générer une clé de sécurité et en sauvegarder une copie chiffrée sur le serveur Matrix. |
        |Stockage |Veillez à stocker la phrase de sécurité ainsi que la clé de sécurité dans un endroit sûr, comme un [gestionnaire de mots de passe](https://gofoss.net/fr/passwords/) ! Trois éléments doivent être stockés : <br>• votre mot de passe, nécessaire pour vous connecter à Element <br>• une phrase de sécurité, qui protège votre clé de sécurité <br>• une clé de sécurité, qui est nécessaire pour accéder à vos messages chiffrés |
        |Récupération |Si jamais vous ne parvenez plus à lire vos messages sur votre appareil Android, c'est que celui-ci ne dispose pas de la bonne clé de sécurité. Dans ce cas, allez dans `Paramètres ‣ Sécurité et vie privée ‣ Gestion des clés cryptographiques ‣ Récupération des messages chiffrés`, cliquez sur `Restaurer depuis la sauvegarde` et saisissez votre phrase de sécurité. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez sur le bouton `+` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `CRÉER` <br>• Commencez la discussion |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Message vocal` se trouve à côté du champ de saisie de texte <br>• Maintenez le bouton `Message vocal` enfoncé pour enregistrer votre message, puis relâchez-le pour l'envoyer (ou glissez vers la gauche pour annuler l'opération) <br>• Appuyez et faites glisser le bouton `Message vocal` vers le haut pour enregistrer un message vocal plus long. Vous pouvez le consulter avant d'appuyer sur le bouton `Envoyer` (ou sur le bouton `Corbeille` pour supprimer le message vocal) |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez sur le bouton `+` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `CRÉER` <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez sur le bouton `+` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `CRÉER` <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Rejoignez un salon publique |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Appuyez sur le bouton `#+` <br>• Parcourez le répertoire des salons <br>• Ou bien appuyez sur la loupe pour rechercher une salle correspondant à vos intérêts <br>• Vous pouvez également consulter d'autres serveurs pour trouver des salons additionnels <br>• Appuyez sur `REJOINDRE` pour vous joindre à une communauté |
        |Créez un nouveau salon |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Appuyez sur le bouton `#+` <br>• Appuyez sur `CRÉER UN NOUVEAU SALON` <br>• Saisissez un nom pour le salon <br>• Choisissez si le salon est privé ou publique (dans ce dernier cas, une adresse doit être fournie) <br>• Activez ou désactivez le chiffrement <br>• Facultatif: bloquez les utilisateurs d'autres serveurs Matrix <br>• Appuyez sur `CRÉER` <br>• Appuyez sur le bouton `Ajouter des personnes` et invitez d'autres personnes à vous rejoindre en utilisant leur nom d'utilisateur ou leur adresse électronique <br>• Facultatif: il est possible de définir des paramètres très spécifiques, tels que les aperçus d'URL, l'accès à la salle, l'historique des messages, les permissions des utilisateurs, etc. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal en groupe |
        |Lancez un appel vidéo en groupe |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo en groupe |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Paramètres de notification globaux |• Lancez l'appli <br>• Allez dans `Paramètres ‣ Notifications` et configurez les notifications globales à votre convenance <br>• Activez ou désactivez les notifications pour votre compte ou la session <br>• Activez ou désactivez les notifications pour les messages contenant votre nom, pour les tchats en tête-à-tête ou en groupe, pour les invitations, etc. <br>• Configurez la couleur des LED, les vibrations et les sons |
        |Paramètres de notification des conversations privées |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez longuement sur une session de tchat existante <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom |
        |Paramètres de notification des salons |• Lancez l'application <br>• Sélectionnez l'onglet `Salons` <br>• Appuyez longuement sur un salon existant <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom |

        ### Authentification & vérification

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Dans certaines circonstances, Element peut vouloir vérifier votre identité. Par exemple, si vous vous connectez avec un nouvel appareil ou ouvrez une nouvelle session. Ou si vous vous connectez avec plusieurs appareils en même temps. Dans de tels cas, une notification apparaîtra : `Vérifier cette session` ou `Nouvelle connexion. Est-ce vous ?`. Plusieurs options existent pour prouver votre identité. |
        |Phrase ou clé de sécurité |La solution la plus simple consiste à saisir soit votre phrase de sécurité, soit votre clé de sécurité. |
        |Autre appareil |Vous pouvez également utiliser un autre appareil qui est déjà connecté à votre compte. Pour cela, cliquez sur la demande de vérification qui apparaît. Maintenant, vous pouvez soit scanner le code QR, soit comparer un code Emoji. |
        |Vérification réussie |Si vous réussissez à vérifier votre identité, vous pourrez accéder à l'intégralité de vos messages et apparaîtrez comme digne de confiance aux yeux des autres. |
        |Sessions actives |Vous pouvez vérifier toutes les sessions actives et vérifiées en allant dans `Paramètres ‣ Sécurité et vie privée ‣ Sessions actives`. |
        |Vérification d'autres utilisateurs |Pour davantage de sécurité, vous pouvez également vérifier l'identité des autres utilisateurs avec lesquels vous discutez. Ouvrez un tchat (chiffré), allez dans `Paramètres ‣ Personnes`, sélectionnez l'utilisateur concerné et cliquez sur `Vérifier ‣ Commencer la vérification`. Attendez que l'autre utilisateur accepte la demande de vérification. Maintenant, vous pouvez soit scanner les codes QR, soit comparer un code Emoji. |



=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        ### Installation

        Installez Element à partir de l'[App Store](https://apps.apple.com/fr/app/vector/id1083446067/).

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Chaque utilisatrice et utilisateur de Matrix dispose d'un identifiant unique, qui fonctionne de manière similaire à une adresse électronique et ressemble à ceci : `@utilisatrice:nomduserveur.net`. Vous pouvez créer un nouvel identifiant Matrix auprès du fournisseur de votre choix, ou même devenir votre propre fournisseur en auto-hébergeant Matrix. |
        |Ouvrez Element |Ouvrez l'appli et cliquez sur `Démarrer`. |
        |Sélectionnez un serveur |Choisissez le serveur par défaut [matrix.org](https://matrix.org), ou tout autre serveur public de votre choix. Une sélection peut être trouvée ici : <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Inscrivez-vous |Cliquez sur `S'inscrire` et saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Ces informations d'identification sont nécessaires pour se connecter à Element. |
        |Politique de confidentialité |Vérifiez la politique de confidentialité du serveur et cliquez sur `Accepter`. |
        |Vérification de votre adresse électronique |Saisissez une adresse électronique existante ou une [adresse électronique jetable](https://gofoss.net/fr/cloud-providers/#other-cloud-services) afin d'authentifier votre compte. Ouvrez le lien dans le courriel de confirmation pour finaliser l'inscription. |

        ### Clé de sécurité

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |*Cette étape n'est requise qu'après la première connexion*. <br> Element utilise un chiffrement de bout en bout. Pour être sûr de toujours avoir accès à vos messages depuis n'importe quel appareil, vous devez générer une clé dite de sécurité. |
        |Paramétrage |Allez dans `Paramètres ‣ Sécurité et vie privée ‣ Sauvegarde sécurisée ‣ Activer la sauvegarde sécurisée`. |
        |Phrase de sécurité |• Sélectionnez `Saisir une phrase de sécurité` et entrez une [phrase de sécurité forte et unique](https://gofoss.net/fr/passwords/) <br>• Cette phrase de sécurité protégera votre clé de sécurité. <br>• *Veillez à ne pas utiliser le mot de passe de votre compte Element !* <br>• Appuyez sur `Continuer`, confirmez votre phrase de sécurité et appuyez de nouveau sur `Continuer`. |
        |Clé de sécurité |Element va maintenant générer une clé de sécurité et en sauvegarder une copie chiffrée sur le serveur Matrix. |
        |Stockage |Veillez à stocker la phrase de sécurité ainsi que la clé de sécurité dans un endroit sûr, comme un [gestionnaire de mots de passe](https://gofoss.net/fr/passwords/) ! Trois éléments doivent être stockés : <br>• votre mot de passe, nécessaire pour vous connecter à Element <br>• une phrase de sécurité, qui protège votre clé de sécurité <br>• une clé de sécurité, qui est nécessaire pour accéder à vos messages chiffrés |
        |Récupération |Si jamais vous ne parvenez plus à lire vos messages sur votre appareil iOS, c'est que celui-ci ne dispose pas de la bonne clé de sécurité. Dans ce cas, allez dans `Paramètres ‣ Sécurité et vie privée ‣ Gestion des clés cryptographiques ‣ Récupération des messages chiffrés`, cliquez sur `Restaurer depuis la sauvegarde` et saisissez votre phrase de sécurité. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez sur le bouton `+` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `CRÉER` <br>• Commencez la discussion |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Message vocal` se trouve à côté du champ de saisie de texte <br>• Maintenez le bouton `Message vocal` enfoncé pour enregistrer votre message, puis relâchez-le pour l'envoyer (ou glissez vers la gauche pour annuler l'opération) <br>• Appuyez et faites glisser le bouton `Message vocal` vers le haut pour enregistrer un message vocal plus long. Vous pouvez le consulter avant d'appuyer sur le bouton `Envoyer` (ou sur le bouton `Corbeille` pour supprimer le message vocal) |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez sur le bouton `+` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `CRÉER` <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez sur le bouton `+` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `CRÉER` <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Rejoignez un salon publique |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Appuyez sur le bouton `#+` <br>• Parcourez le répertoire des salons <br>• Ou bien appuyez sur la loupe pour rechercher une salle correspondant à vos intérêts <br>• Vous pouvez également consulter d'autres serveurs pour trouver des salons additionnels <br>• Appuyez sur `REJOINDRE` pour vous joindre à une communauté |
        |Créez un nouveau salon |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Appuyez sur le bouton `#+` <br>• Appuyez sur `CRÉER UN NOUVEAU SALON` <br>• Saisissez un nom pour le salon <br>• Choisissez si le salon est privé ou publique (dans ce dernier cas, une adresse doit être fournie) <br>• Activez ou désactivez le chiffrement <br>• Facultatif: bloquez les utilisateurs d'autres serveurs Matrix <br>• Appuyez sur `CRÉER` <br>• Appuyez sur le bouton `Ajouter des personnes` et invitez d'autres personnes à vous rejoindre en utilisant leur nom d'utilisateur ou leur adresse électronique <br>• Facultatif: il est possible de définir des paramètres très spécifiques, tels que les aperçus d'URL, l'accès à la salle, l'historique des messages, les permissions des utilisateurs, etc. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal en groupe |
        |Lancez un appel vidéo en groupe |• Lancez l'appli <br>• Sélectionnez l'onglet `Salons` <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo en groupe |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Paramètres de notification globaux |• Lancez l'appli <br>• Allez dans `Paramètres ‣ Notifications` et configurez les notifications globales à votre convenance <br>• Activez ou désactivez les notifications pour votre compte ou la session <br>• Activez ou désactivez les notifications pour les messages contenant votre nom, pour les tchats en tête-à-tête ou en groupe, pour les invitations, etc. <br>• Configurez la couleur des LED, les vibrations et les sons |
        |Paramètres de notification des conversations privées |• Lancez l'appli <br>• Sélectionnez l'onglet `Conversations privées` <br>• Appuyez longuement sur une session de tchat existante <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom |
        |Paramètres de notification des salons |• Lancez l'application <br>• Sélectionnez l'onglet `Salons` <br>• Appuyez longuement sur un salon existant <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom |

        ### Authentification & vérification

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Dans certaines circonstances, Element peut vouloir vérifier votre identité. Par exemple, si vous vous connectez avec un nouvel appareil ou ouvrez une nouvelle session. Ou si vous vous connectez avec plusieurs appareils en même temps. Dans de tels cas, une notification apparaîtra : `Vérifier cette session` ou `Nouvelle connexion. Est-ce vous ?`. Plusieurs options existent pour prouver votre identité. |
        |Phrase ou clé de sécurité |La solution la plus simple consiste à saisir soit votre phrase de sécurité, soit votre clé de sécurité. |
        |Autre appareil |Vous pouvez également utiliser un autre appareil qui est déjà connecté à votre compte. Pour cela, cliquez sur la demande de vérification qui apparaît. Maintenant, vous pouvez soit scanner le code QR, soit comparer un code Emoji. |
        |Vérification réussie |Si vous réussissez à vérifier votre identité, vous pourrez accéder à l'intégralité de vos messages et apparaîtrez comme digne de confiance aux yeux des autres. |
        |Sessions actives |Vous pouvez vérifier toutes les sessions actives et vérifiées en allant dans `Paramètres ‣ Sécurité et vie privée ‣ Sessions actives`. |
        |Vérification d'autres utilisateurs |Pour davantage de sécurité, vous pouvez également vérifier l'identité des autres utilisateurs avec lesquels vous discutez. Ouvrez un tchat (chiffré), allez dans `Paramètres ‣ Personnes`, sélectionnez l'utilisateur concerné et cliquez sur `Vérifier ‣ Commencer la vérification`. Attendez que l'autre utilisateur accepte la demande de vérification. Maintenant, vous pouvez soit scanner les codes QR, soit comparer un code Emoji. |


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        ### Installation

        Téléchargez et exécutez [l'installateur Element pour Windows](https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe).

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Chaque utilisatrice et utilisateur de Matrix dispose d'un identifiant unique, qui fonctionne de manière similaire à une adresse électronique et ressemble à ceci : `@utilisatrice:nomduserveur.net`. Vous pouvez créer un nouvel identifiant Matrix auprès du fournisseur de votre choix, ou même devenir votre propre fournisseur en auto-hébergeant Matrix. |
        |Ouvrez Element |Lancez l'appli et cliquez sur `Créez un compte`. |
        |Sélectionnez un serveur |Choisissez le serveur par défaut [matrix.org](https://matrix.org), ou tout autre serveur public de votre choix. Une sélection peut être trouvée ici : <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Inscrivez-vous |Saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Ces informations d'identification sont nécessaires pour se connecter à Element. |
        |Vérification de votre adresse électronique |Saisissez une adresse électronique existante ou une [adresse électronique jetable](https://gofoss.net/fr/cloud-providers/#other-cloud-services) afin d'authentifier votre compte. Cliquez sur `S'inscrire` et acceptez les conditions générales. Ouvrez le lien dans le courriel de confirmation pour finaliser l'inscription. |

        ### Clé de sécurité

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |*Cette étape n'est requise qu'après la première connexion*. <br> Element utilise un chiffrement de bout en bout. Pour être sûr de toujours avoir accès à vos messages depuis n'importe quel appareil, vous devez générer une clé dite de sécurité. |
        |Paramétrage |Après s'être connecté, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Sauvegarde sécurisée ‣ Configurer`. |
        |Phrase de sécurité |• Sélectionnez `Saisir une phrase de sécurité` et entrez une [phrase de sécurité forte et unique](https://gofoss.net/fr/passwords/) <br>• Cette phrase de sécurité protégera votre clé de sécurité. <br>• *Veillez à ne pas utiliser le mot de passe de votre compte Element !* <br>• Appuyez sur `Continuer`, confirmez votre phrase de sécurité et appuyez de nouveau sur `Continuer`. |
        |Clé de sécurité |Element va maintenant générer une clé de sécurité et en sauvegarder une copie chiffrée sur le serveur Matrix. |
        |Stockage |Veillez à stocker la phrase de sécurité ainsi que la clé de sécurité dans un endroit sûr, comme un [gestionnaire de mots de passe](https://gofoss.net/fr/passwords/) ! Trois éléments doivent être stockés : <br>• votre mot de passe, nécessaire pour vous connecter à Element <br>• une phrase de sécurité, qui protège votre clé de sécurité <br>• une clé de sécurité, qui est nécessaire pour accéder à vos messages chiffrés |
        |Récupération |Si jamais vous ne parvenez plus à lire vos messages sur votre ordinateur Windows, c'est que celui-ci ne dispose pas de la bonne clé de sécurité. Dans ce cas, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Chiffrement ‣ Sauvegarde sécurisée`, cliquez sur `Restaurer depuis la sauvegarde` et saisissez votre phrase de sécurité. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Commencez la discussion |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Message vocal` se trouve à côté du champ de saisie de texte <br>• Cliquez dessus pour enregistrer votre message <br>• Quand vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Ou cliquez sur le bouton `Corbeille` pour effacer votre message vocal |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Rejoignez un salon publique |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Parcourir les salons publiques` <br>• Parcourez le répertoire des salons <br>• Ou bien recherchez une salle correspondant à vos intérêts <br>• Vous pouvez également consulter d'autres serveurs pour trouver des salons additionnels <br>• Appuyez sur `Rejoindre` pour vous joindre à une communauté |
        |Créez un nouveau salon |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Créer un nouveau salon` <br>• Saisissez un nom pour le salon <br>• Choisissez si le salon est privé ou publique (dans ce dernier cas, une adresse doit être fournie) <br>• Activez ou désactivez le chiffrement <br>• Facultatif: bloquez les utilisateurs d'autres serveurs Matrix <br>• Appuyez sur `Créer un salon` <br>• Appuyez sur le bouton `Inviter dans ce salon` et invitez d'autres personnes à vous rejoindre en utilisant leur nom d'utilisateur ou leur adresse électronique <br>• Facultatif: il est possible de définir des paramètres très spécifiques, tels que les aperçus d'URL, l'accès à la salle, l'historique des messages, les permissions des utilisateurs, etc. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal en groupe |
        |Lancez un appel vidéo en groupe |• Lancez l'appli <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo en groupe |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Paramètres de notification globaux |• Lancez l'appli <br>• Allez dans `Menu utilisateur ‣ Paramètres de notification` et configurez les notifications globales à votre convenance <br>• Activez ou désactivez les notifications pour votre compte <br>• Activez ou désactivez les notifications de bureau pour la session <br>• Activez ou désactivez les notifications pour les courriels de notification <br>• Activez ou désactivez les notifications pour les tchats en tête-à-tête ou de groupe <br>• Activez ou désactivez les notifications pour des messages mentionnant votre nom ou des mots clés <br>• Activez ou désactivez les notifications pour les invitations etc. |
        |Paramètres de notification des conversations privées |• Lancez l'appli <br>• Survolez une session de tchat dans la section `Personnes` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |
        |Paramètres de notification des salons |• Lancez l'application <br>• Survolez un salon dans la section `Salons` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |

        ### Authentification & vérification

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Dans certaines circonstances, Element peut vouloir vérifier votre identité. Par exemple, si vous vous connectez avec un nouvel appareil ou ouvrez une nouvelle session. Ou si vous vous connectez avec plusieurs appareils en même temps. Dans de tels cas, une notification apparaîtra : `Vérifier cette session` ou `Nouvelle connexion. Est-ce vous ?`. Plusieurs options existent pour prouver votre identité. |
        |Phrase ou clé de sécurité |La solution la plus simple consiste à saisir soit votre phrase de sécurité, soit votre clé de sécurité. |
        |Autre appareil |Vous pouvez également utiliser un autre appareil qui est déjà connecté à votre compte. Pour cela, cliquez sur la demande de vérification qui apparaît. Maintenant, vous pouvez soit scanner le code QR, soit comparer un code Emoji. |
        |Vérification réussie |Si vous réussissez à vérifier votre identité, vous pourrez accéder à l'intégralité de vos messages et apparaîtrez comme digne de confiance aux yeux des autres. |
        |Sessions actives |Vous pouvez vérifier toutes les sessions actives et vérifiées en allant dans `Menu utilisateur ‣ Sécurité et vie privée`. |
        |Vérification d'autres utilisateurs |Pour davantage de sécurité, vous pouvez également vérifier l'identité des autres utilisateurs avec lesquels vous discutez. Ouvrez un tchat (chiffré), allez dans `Informations sur le salon ‣ Personnes`, sélectionnez l'utilisateur concerné et cliquez sur `Vérifier ‣ Commencer la vérification`. Attendez que l'autre utilisateur accepte la demande de vérification. Maintenant, vous pouvez soit scanner les codes QR, soit comparer un code Emoji. |


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        ### Installation

        Téléchargez l'[image disque d'Element](https://packages.riot.im/desktop/install/macos/Element.dmg), ouvrez-la et faites glisser l'icône au-dessus du dossier d'application. Pour y accéder facilement, ouvrez le dossier d'applications et faites glisser l'icône Element vers le dock.

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Chaque utilisatrice et utilisateur de Matrix dispose d'un identifiant unique, qui fonctionne de manière similaire à une adresse électronique et ressemble à ceci : `@utilisatrice:nomduserveur.net`. Vous pouvez créer un nouvel identifiant Matrix auprès du fournisseur de votre choix, ou même devenir votre propre fournisseur en auto-hébergeant Matrix. |
        |Ouvrez Element |Lancez l'appli et cliquez sur `Créez un compte`. |
        |Sélectionnez un serveur |Choisissez le serveur par défaut [matrix.org](https://matrix.org), ou tout autre serveur public de votre choix. Une sélection peut être trouvée ici : <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Inscrivez-vous |Saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Ces informations d'identification sont nécessaires pour se connecter à Element. |
        |Vérification de votre adresse électronique |Saisissez une adresse électronique existante ou une [adresse électronique jetable](https://gofoss.net/fr/cloud-providers/#other-cloud-services) afin d'authentifier votre compte. Cliquez sur `S'inscrire` et acceptez les conditions générales. Ouvrez le lien dans le courriel de confirmation pour finaliser l'inscription. |

        ### Clé de sécurité

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |*Cette étape n'est requise qu'après la première connexion*. <br> Element utilise un chiffrement de bout en bout. Pour être sûr de toujours avoir accès à vos messages depuis n'importe quel appareil, vous devez générer une clé dite de sécurité. |
        |Paramétrage |Après s'être connecté, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Sauvegarde sécurisée ‣ Configurer`. |
        |Phrase de sécurité |• Sélectionnez `Saisir une phrase de sécurité` et entrez une [phrase de sécurité forte et unique](https://gofoss.net/fr/passwords/) <br>• Cette phrase de sécurité protégera votre clé de sécurité. <br>• *Veillez à ne pas utiliser le mot de passe de votre compte Element !* <br>• Appuyez sur `Continuer`, confirmez votre phrase de sécurité et appuyez de nouveau sur `Continuer`. |
        |Clé de sécurité |Element va maintenant générer une clé de sécurité et en sauvegarder une copie chiffrée sur le serveur Matrix. |
        |Stockage |Veillez à stocker la phrase de sécurité ainsi que la clé de sécurité dans un endroit sûr, comme un [gestionnaire de mots de passe](https://gofoss.net/fr/passwords/) ! Trois éléments doivent être stockés : <br>• votre mot de passe, nécessaire pour vous connecter à Element <br>• une phrase de sécurité, qui protège votre clé de sécurité <br>• une clé de sécurité, qui est nécessaire pour accéder à vos messages chiffrés |
        |Récupération |Si jamais vous ne parvenez plus à lire vos messages sur votre appareil macOS, c'est que celui-ci ne dispose pas de la bonne clé de sécurité. Dans ce cas, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Chiffrement ‣ Sauvegarde sécurisée`, cliquez sur `Restaurer depuis la sauvegarde` et saisissez votre phrase de sécurité. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Commencez la discussion |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Message vocal` se trouve à côté du champ de saisie de texte <br>• Cliquez dessus pour enregistrer votre message <br>• Quand vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Ou cliquez sur le bouton `Corbeille` pour effacer votre message vocal |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Rejoignez un salon publique |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Parcourir les salons publiques` <br>• Parcourez le répertoire des salons <br>• Ou bien recherchez une salle correspondant à vos intérêts <br>• Vous pouvez également consulter d'autres serveurs pour trouver des salons additionnels <br>• Appuyez sur `Rejoindre` pour vous joindre à une communauté |
        |Créez un nouveau salon |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Créer un nouveau salon` <br>• Saisissez un nom pour le salon <br>• Choisissez si le salon est privé ou publique (dans ce dernier cas, une adresse doit être fournie) <br>• Activez ou désactivez le chiffrement <br>• Facultatif: bloquez les utilisateurs d'autres serveurs Matrix <br>• Appuyez sur `Créer un salon` <br>• Appuyez sur le bouton `Inviter dans ce salon` et invitez d'autres personnes à vous rejoindre en utilisant leur nom d'utilisateur ou leur adresse électronique <br>• Facultatif: il est possible de définir des paramètres très spécifiques, tels que les aperçus d'URL, l'accès à la salle, l'historique des messages, les permissions des utilisateurs, etc. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal en groupe |
        |Lancez un appel vidéo en groupe |• Lancez l'appli <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo en groupe |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Paramètres de notification globaux |• Lancez l'appli <br>• Allez dans `Menu utilisateur ‣ Paramètres de notification` et configurez les notifications globales à votre convenance <br>• Activez ou désactivez les notifications pour votre compte <br>• Activez ou désactivez les notifications de bureau pour la session <br>• Activez ou désactivez les notifications pour les courriels de notification <br>• Activez ou désactivez les notifications pour les tchats en tête-à-tête ou de groupe <br>• Activez ou désactivez les notifications pour des messages mentionnant votre nom ou des mots clés <br>• Activez ou désactivez les notifications pour les invitations etc. |
        |Paramètres de notification des conversations privées |• Lancez l'appli <br>• Survolez une session de tchat dans la section `Personnes` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |
        |Paramètres de notification des salons |• Lancez l'application <br>• Survolez un salon dans la section `Salons` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |

        ### Authentification & vérification

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Dans certaines circonstances, Element peut vouloir vérifier votre identité. Par exemple, si vous vous connectez avec un nouvel appareil ou ouvrez une nouvelle session. Ou si vous vous connectez avec plusieurs appareils en même temps. Dans de tels cas, une notification apparaîtra : `Vérifier cette session` ou `Nouvelle connexion. Est-ce vous ?`. Plusieurs options existent pour prouver votre identité. |
        |Phrase ou clé de sécurité |La solution la plus simple consiste à saisir soit votre phrase de sécurité, soit votre clé de sécurité. |
        |Autre appareil |Vous pouvez également utiliser un autre appareil qui est déjà connecté à votre compte. Pour cela, cliquez sur la demande de vérification qui apparaît. Maintenant, vous pouvez soit scanner le code QR, soit comparer un code Emoji. |
        |Vérification réussie |Si vous réussissez à vérifier votre identité, vous pourrez accéder à l'intégralité de vos messages et apparaîtrez comme digne de confiance aux yeux des autres. |
        |Sessions actives |Vous pouvez vérifier toutes les sessions actives et vérifiées en allant dans `Menu utilisateur ‣ Sécurité et vie privée`. |
        |Vérification d'autres utilisateurs |Pour davantage de sécurité, vous pouvez également vérifier l'identité des autres utilisateurs avec lesquels vous discutez. Ouvrez un tchat (chiffré), allez dans `Informations sur le salon ‣ Personnes`, sélectionnez l'utilisateur concerné et cliquez sur `Vérifier ‣ Commencer la vérification`. Attendez que l'autre utilisateur accepte la demande de vérification. Maintenant, vous pouvez soit scanner les codes QR, soit comparer un code Emoji. |


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Ouvrez un terminal |Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), utilisez le raccourci `Ctrl+Alt+T` ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. |
        |Activez l'accès sécurisé aux dépôts |`sudo apt install -y wget apt-transport-https` |
        |Ajoutez les clés de signature |`sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg` |
        |Ajoutez le dépôt à la liste des sources apt |`echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list` |
        |Mettez à jour le cache apt local |`sudo apt update` |
        |Installez le client Element |`sudo apt install element-desktop`|

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Chaque utilisatrice et utilisateur de Matrix dispose d'un identifiant unique, qui fonctionne de manière similaire à une adresse électronique et ressemble à ceci : `@utilisatrice:nomduserveur.net`. Vous pouvez créer un nouvel identifiant Matrix auprès du fournisseur de votre choix, ou même devenir votre propre fournisseur en auto-hébergeant Matrix. |
        |Ouvrez Element |Lancez l'appli et cliquez sur `Créez un compte`. |
        |Sélectionnez un serveur |Choisissez le serveur par défaut [matrix.org](https://matrix.org), ou tout autre serveur public de votre choix. Une sélection peut être trouvée ici : <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Inscrivez-vous |Saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Ces informations d'identification sont nécessaires pour se connecter à Element. |
        |Vérification de votre adresse électronique |Saisissez une adresse électronique existante ou une [adresse électronique jetable](https://gofoss.net/fr/cloud-providers/#other-cloud-services) afin d'authentifier votre compte. Cliquez sur `S'inscrire` et acceptez les conditions générales. Ouvrez le lien dans le courriel de confirmation pour finaliser l'inscription. |

        ### Clé de sécurité

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |*Cette étape n'est requise qu'après la première connexion*. <br> Element utilise un chiffrement de bout en bout. Pour être sûr de toujours avoir accès à vos messages depuis n'importe quel appareil, vous devez générer une clé dite de sécurité. |
        |Paramétrage |Après s'être connecté, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Sauvegarde sécurisée ‣ Configurer`. |
        |Phrase de sécurité |• Sélectionnez `Saisir une phrase de sécurité` et entrez une [phrase de sécurité forte et unique](https://gofoss.net/fr/passwords/) <br>• Cette phrase de sécurité protégera votre clé de sécurité. <br>• *Veillez à ne pas utiliser le mot de passe de votre compte Element !* <br>• Appuyez sur `Continuer`, confirmez votre phrase de sécurité et appuyez de nouveau sur `Continuer`. |
        |Clé de sécurité |Element va maintenant générer une clé de sécurité et en sauvegarder une copie chiffrée sur le serveur Matrix. |
        |Stockage |Veillez à stocker la phrase de sécurité ainsi que la clé de sécurité dans un endroit sûr, comme un [gestionnaire de mots de passe](https://gofoss.net/fr/passwords/) ! Trois éléments doivent être stockés : <br>• votre mot de passe, nécessaire pour vous connecter à Element <br>• une phrase de sécurité, qui protège votre clé de sécurité <br>• une clé de sécurité, qui est nécessaire pour accéder à vos messages chiffrés |
        |Récupération |Si jamais vous ne parvenez plus à lire vos messages sur votre appareil Linux/Ubuntu, c'est que celui-ci ne dispose pas de la bonne clé de sécurité. Dans ce cas, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Chiffrement ‣ Sauvegarde sécurisée`, cliquez sur `Restaurer depuis la sauvegarde` et saisissez votre phrase de sécurité. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Commencez la discussion |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Message vocal` se trouve à côté du champ de saisie de texte <br>• Cliquez dessus pour enregistrer votre message <br>• Quand vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Ou cliquez sur le bouton `Corbeille` pour effacer votre message vocal |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Rejoignez un salon publique |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Parcourir les salons publiques` <br>• Parcourez le répertoire des salons <br>• Ou bien recherchez une salle correspondant à vos intérêts <br>• Vous pouvez également consulter d'autres serveurs pour trouver des salons additionnels <br>• Appuyez sur `Rejoindre` pour vous joindre à une communauté |
        |Créez un nouveau salon |• Lancez l'appli <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Créer un nouveau salon` <br>• Saisissez un nom pour le salon <br>• Choisissez si le salon est privé ou publique (dans ce dernier cas, une adresse doit être fournie) <br>• Activez ou désactivez le chiffrement <br>• Facultatif: bloquez les utilisateurs d'autres serveurs Matrix <br>• Appuyez sur `Créer un salon` <br>• Appuyez sur le bouton `Inviter dans ce salon` et invitez d'autres personnes à vous rejoindre en utilisant leur nom d'utilisateur ou leur adresse électronique <br>• Facultatif: il est possible de définir des paramètres très spécifiques, tels que les aperçus d'URL, l'accès à la salle, l'historique des messages, les permissions des utilisateurs, etc. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal en groupe |
        |Lancez un appel vidéo en groupe |• Lancez l'appli <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo en groupe |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Paramètres de notification globaux |• Lancez l'appli <br>• Allez dans `Menu utilisateur ‣ Paramètres de notification` et configurez les notifications globales à votre convenance <br>• Activez ou désactivez les notifications pour votre compte <br>• Activez ou désactivez les notifications de bureau pour la session <br>• Activez ou désactivez les notifications pour les courriels de notification <br>• Activez ou désactivez les notifications pour les tchats en tête-à-tête ou de groupe <br>• Activez ou désactivez les notifications pour des messages mentionnant votre nom ou des mots clés <br>• Activez ou désactivez les notifications pour les invitations etc. |
        |Paramètres de notification des conversations privées |• Lancez l'appli <br>• Survolez une session de tchat dans la section `Personnes` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |
        |Paramètres de notification des salons |• Lancez l'application <br>• Survolez un salon dans la section `Salons` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |

        ### Authentification & vérification

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Dans certaines circonstances, Element peut vouloir vérifier votre identité. Par exemple, si vous vous connectez avec un nouvel appareil ou ouvrez une nouvelle session. Ou si vous vous connectez avec plusieurs appareils en même temps. Dans de tels cas, une notification apparaîtra : `Vérifier cette session` ou `Nouvelle connexion. Est-ce vous ?`. Plusieurs options existent pour prouver votre identité. |
        |Phrase ou clé de sécurité |La solution la plus simple consiste à saisir soit votre phrase de sécurité, soit votre clé de sécurité. |
        |Autre appareil |Vous pouvez également utiliser un autre appareil qui est déjà connecté à votre compte. Pour cela, cliquez sur la demande de vérification qui apparaît. Maintenant, vous pouvez soit scanner le code QR, soit comparer un code Emoji. |
        |Vérification réussie |Si vous réussissez à vérifier votre identité, vous pourrez accéder à l'intégralité de vos messages et apparaîtrez comme digne de confiance aux yeux des autres. |
        |Sessions actives |Vous pouvez vérifier toutes les sessions actives et vérifiées en allant dans `Menu utilisateur ‣ Sécurité et vie privée`. |
        |Vérification d'autres utilisateurs |Pour davantage de sécurité, vous pouvez également vérifier l'identité des autres utilisateurs avec lesquels vous discutez. Ouvrez un tchat (chiffré), allez dans `Informations sur le salon ‣ Personnes`, sélectionnez l'utilisateur concerné et cliquez sur `Vérifier ‣ Commencer la vérification`. Attendez que l'autre utilisateur accepte la demande de vérification. Maintenant, vous pouvez soit scanner les codes QR, soit comparer un code Emoji. |


=== "Navigateur"

    ??? tip "Montrez-moi le guide étape par étape"

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Chaque utilisatrice et utilisateur de Matrix dispose d'un identifiant unique, qui fonctionne de manière similaire à une adresse électronique et ressemble à ceci : `@utilisatrice:nomduserveur.net`. Vous pouvez créer un nouvel identifiant Matrix auprès du fournisseur de votre choix, ou même devenir votre propre fournisseur en auto-hébergeant Matrix. |
        |Ouvrez Element |Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/). Si votre navigateur vous y invite, autorisez l'accès au stockage permanent. Cela permet à Element de stocker des clés, des messages, etc. dans la session du navigateur. Puis, cliquez sur `Créer un compte`. |
        |Sélectionnez un serveur |Choisissez le serveur par défaut [matrix.org](https://matrix.org), ou tout autre serveur public de votre choix. Une sélection peut être trouvée ici : <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Inscrivez-vous |Saisissez un nom d'utilisateur ainsi qu'un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Ces informations d'identification sont nécessaires pour se connecter à Element. |
        |Vérification de votre adresse électronique |Saisissez une adresse électronique existante ou une [adresse électronique jetable](https://gofoss.net/fr/cloud-providers/#other-cloud-services) afin d'authentifier votre compte. Cliquez sur `S'inscrire` et acceptez les conditions générales. Ouvrez le lien dans le courriel de confirmation pour finaliser l'inscription. |

        ### Clé de sécurité

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |*Cette étape n'est requise qu'après la première connexion*. <br> Element utilise un chiffrement de bout en bout. Pour être sûr de toujours avoir accès à vos messages depuis n'importe quel appareil, vous devez générer une clé dite de sécurité. |
        |Paramétrage |Après s'être connecté, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Sauvegarde sécurisée ‣ Configurer`. |
        |Phrase de sécurité |• Sélectionnez `Saisir une phrase de sécurité` et entrez une [phrase de sécurité forte et unique](https://gofoss.net/fr/passwords/) <br>• Cette phrase de sécurité protégera votre clé de sécurité. <br>• *Veillez à ne pas utiliser le mot de passe de votre compte Element !* <br>• Appuyez sur `Continuer`, confirmez votre phrase de sécurité et appuyez de nouveau sur `Continuer`. |
        |Clé de sécurité |Element va maintenant générer une clé de sécurité et en sauvegarder une copie chiffrée sur le serveur Matrix. |
        |Stockage |Veillez à stocker la phrase de sécurité ainsi que la clé de sécurité dans un endroit sûr, comme un [gestionnaire de mots de passe](https://gofoss.net/fr/passwords/) ! Trois éléments doivent être stockés : <br>• votre mot de passe, nécessaire pour vous connecter à Element <br>• une phrase de sécurité, qui protège votre clé de sécurité <br>• une clé de sécurité, qui est nécessaire pour accéder à vos messages chiffrés |
        |Récupération |Si jamais vous ne parvenez plus à lire vos messages sur votre navigateur, c'est que celui-ci ne dispose pas de la bonne clé de sécurité. Dans ce cas, allez dans `Menu utilisateur ‣ Sécurité et vie privée ‣ Chiffrement ‣ Sauvegarde sécurisée`, cliquez sur `Restaurer depuis la sauvegarde` et saisissez votre phrase de sécurité. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Commencez la discussion |
        |Envoyez un message vocal |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Message vocal` se trouve à côté du champ de saisie de texte <br>• Cliquez dessus pour enregistrer votre message <br>• Quand vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Ou cliquez sur le bouton `Corbeille` pour effacer votre message vocal |
        |Lancez un appel vocal en tête-à-tête |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Cliquez sur le bouton `+` à côté de la rubrique `Personnes` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur ou de leur adresse électronique <br>• Cliquez sur `C'est parti` <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Rejoignez un salon publique |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Parcourir les salons publiques` <br>• Parcourez le répertoire des salons <br>• Ou bien recherchez une salle correspondant à vos intérêts <br>• Vous pouvez également consulter d'autres serveurs pour trouver des salons additionnels <br>• Appuyez sur `Rejoindre` pour vous joindre à une communauté |
        |Créez un nouveau salon |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Cliquez sur le bouton `+` à côté de la rubrique `Salons` <br>• Cliquez sur `Créer un nouveau salon` <br>• Saisissez un nom pour le salon <br>• Choisissez si le salon est privé ou publique (dans ce dernier cas, une adresse doit être fournie) <br>• Activez ou désactivez le chiffrement <br>• Facultatif: bloquez les utilisateurs d'autres serveurs Matrix <br>• Appuyez sur `Créer un salon` <br>• Appuyez sur le bouton `Inviter dans ce salon` et invitez d'autres personnes à vous rejoindre en utilisant leur nom d'utilisateur ou leur adresse électronique <br>• Facultatif: il est possible de définir des paramètres très spécifiques, tels que les aperçus d'URL, l'accès à la salle, l'historique des messages, les permissions des utilisateurs, etc. |
        |Lancez un appel vocal en groupe |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal en groupe |
        |Lancez un appel vidéo en groupe |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Sélectionnez un salon existant ou créez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo en groupe |

        ### Notifications

        | Instructions | Description |
        | ------ | ------ |
        |Paramètres de notification globaux |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Allez dans `Menu utilisateur ‣ Notifications` et configurez les notifications globales à votre convenance <br>• Activez ou désactivez les notifications pour votre compte <br>• Activez ou désactivez les notifications de bureau pour la session <br>• Activez ou désactivez les notifications pour les courriels de notification <br>• Activez ou désactivez les notifications pour les tchats en tête-à-tête ou de groupe <br>• Activez ou désactivez les notifications pour des messages mentionnant votre nom ou des mots clés <br>• Activez ou désactivez les notifications pour les invitations etc. |
        |Paramètres de notification des conversations privées |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Survolez une session de tchat dans la section `Personnes` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |
        |Paramètres de notification des salons |• Ouvrez votre navigateur et allez sur [https://app.element.io](https://app.element.io/) <br>• Survolez un salon dans la section `Salons` et cliquez sur l'icône `cloche` pour accéder aux paramètres de notification <br>• Activez ou désactivez les notifications pour l'ensemble des messages, ou pour les messages mentionnant votre nom ou des mots clés |

        ### Authentification & vérification

        | Instructions | Description |
        | ------ | ------ |
        |Observations préliminaires |Dans certaines circonstances, Element peut vouloir vérifier votre identité. Par exemple, si vous vous connectez avec un nouvel appareil ou ouvrez une nouvelle session. Ou si vous vous connectez avec plusieurs appareils en même temps. Dans de tels cas, une notification apparaîtra : `Vérifier cette session` ou `Nouvelle connexion. Est-ce vous ?`. Plusieurs options existent pour prouver votre identité. |
        |Phrase ou clé de sécurité |La solution la plus simple consiste à saisir soit votre phrase de sécurité, soit votre clé de sécurité. |
        |Autre appareil |Vous pouvez également utiliser un autre appareil qui est déjà connecté à votre compte. Pour cela, cliquez sur la demande de vérification qui apparaît. Maintenant, vous pouvez soit scanner le code QR, soit comparer un code Emoji. |
        |Vérification réussie |Si vous réussissez à vérifier votre identité, vous pourrez accéder à l'intégralité de vos messages et apparaîtrez comme digne de confiance aux yeux des autres. |
        |Sessions actives |Vous pouvez vérifier toutes les sessions actives et vérifiées en allant dans `Menu utilisateur ‣ Sécurité et vie privée`. |
        |Vérification d'autres utilisateurs |Pour davantage de sécurité, vous pouvez également vérifier l'identité des autres utilisateurs avec lesquels vous discutez. Ouvrez un tchat (chiffré), allez dans `Informations sur le salon ‣ Personnes`, sélectionnez l'utilisateur concerné et cliquez sur `Vérifier ‣ Commencer la vérification`. Attendez que l'autre utilisateur accepte la demande de vérification. Maintenant, vous pouvez soit scanner les codes QR, soit comparer un code Emoji. |


<div style="margin-top:-20px">
</div>

??? warning "Element est chiffré, mais requiert une certaine confiance"

    Element ne poursuit pas le principe de « zéro connaissance » et [collecte des métadonnées](https://gitlab.com/libremonde-org/papers/research/privacy-matrix.org/-/blob/master/part1/README.md/). Comme indiqué dans [la politique de confidentialité d'Element](https://element.io/privacy) : *[...] Nous sommes susceptibles de profiler des métadonnées relatives à la configuration et à la gestion des serveurs hébergés afin d'améliorer nos produits et services*.

    En effet, la gestion des comptes utilisateurs est assurée par les administrateurs du serveur Matrix. En utilisant un serveur Matrix public, vous confiez donc certaines de vos données à ces administrateurs. Même si les communications peuvent être chiffrées, les administrateurs sont potentiellement en mesure de conserver une copie de toutes vos communications (chiffrées), de consigner les adresses IP ou d'accéder aux données (non chiffrées) telles que les noms d'utilisateur, les mots de passe, les adresses électroniques, les numéros de téléphone, les fichiers multimédias, les informations sur les périphériques, les listes de contacts, les profils d'utilisation, les adhésions à des groupes, etc.

    Même si vous auto-hébergez votre propre serveur, les administrateurs des serveurs Matrix participant à une conversation peuvent accéder à ces métadonnées. Selon la formulation d'un [responsable du projet](https://teddit.net/r/privacy/comments/da219t/im_project_lead_for_matrixorg_the_open_protocol/f20r6vp/): *"[...] Si vous invitez dans votre salon de discussion un utilisateur qui se trouve sur un serveur auquel vous ne faites pas confiance, l'historique sera transmis à ce serveur. Si la salle est chiffrée de bout en bout, le serveur ne pourra pas voir les messages, mais il pourra voir les métadonnées indiquant qui a parlé à qui et quand (mais pas de quoi). [...]"*.


<br>

<center> <img src="../../assets/img/separator_jami.svg" alt="Jami" width="150px"></img> </center>

## Jami

[Jami](https://jami.net/) est une application de messagerie instantanée et d'appels vocaux/vidéos. Jami est [open source](https://git.jami.net/savoirfairelinux/) et disponible sur tous les ordinateurs de bureau et appareils mobiles. L'application s'appuie sur une architecture de pair à pair, où les utilisateurs se connectent directement sans passer par des serveurs. Toutes les communications sont chiffrées de bout en bout, avec une confidentialité de transmission parfaite (en anglais, « perfect forward secrecy »). Vous trouverez ci-dessous des instructions plus détaillées.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Installation |Installez Jami à partir: <br>• du [Google Play Store](https://play.google.com/store/apps/details?id=cx.ring&hl=fr)<br>• de l'[Aurora Store](https://auroraoss.com/)<br>• de [F-Droid](https://f-droid.org/fr/packages/cx.ring/) |
         |Aucun traqueur |L'appli [contient 0 traqueur et nécessite 21 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/cx.ring/latest/). À titre de comparaison:<br>• TikTok: 16 traqueurs, 76 autorisations<br>• Snapchat: 2 traqueurs, 44 autorisations <br>• WhatsApp: 1 traqueur, 57 autorisations |
        |Notifications instantanées |Notez que si la version F-Droid de l'application Jami est exempte de Google, [seule la version Google Play Store peut gérer les notifications instantanées](https://git.jami.net/savoirfairelinux/jami-client-android/-/issues/781), en utilisant le « Google Cloud Messaging (GCM) ». |

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Créez un compte |Ouvrez l'appli et cliquez sur `Créer un compte Jami`. Saisissez un nom d'utilisateur. |
        |Saisissez un mot de passe |Facultatif: Chiffrez votre compte en saisissant un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Veillez à conserver ce mot de passe en lieu sûr, il est impossible de le récupérer. |
        |Configurez votre profil |Facultatif: vous pouvez également définir un nom d'utilisateur visible par les autres utilisatrices et utilisateurs, ainsi qu'une photo de profil. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur `Démarrer une conversation` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur <br>• Appuyez sur `Ajouter dans les contacts`. Ceci enverra une invitation à l'autre utilisateur <br>• Vous pouvez commencer à discuter dès que l'invitation est acceptée |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Enregistrer un message audio` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Partager` et envoyez le message à votre contact |
        |Envoyez un message vidéo |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Enregistrer un message vidéo` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Partager` et envoyez le message à votre contact |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran: lors d'un appel vidéo, cliquez sur le bouton `Partager` |
        |Envoyez un fichier |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer le fichier` se trouve à côté du champ de saisie de texte <br>• Sélectionnez le fichier à envoyer |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en groupe |Au moment d'écrire ces lignes, Jami [travaille toujours à la mise en œuvre de la fonctionnalité de tchat de groupe](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Aussi appelés « Swarms », ces conversations textuelles sont entièrement distribuées et de pair à pair, avec un nombre potentiellement illimité de participants. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal <br>• Une fois l'appel lancé, appuyez sur l'icône `Ajouter`  |
        |Lancez un appel vidéo en groupe |Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Une fois l'appel lancé, appuyez sur l'icône `Ajouter` <br>• Partage d'écran: lors d'un appel vidéo de groupe, cliquez sur le bouton `Partager` |

        ### Sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Créez des sauvegardes |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Assurez-vous de [créer des sauvegardes régulières](https://gofoss.net/fr/backups/) en allant dans `Réglages ‣ Compte ‣ Compte ‣ Sauvegarder votre compte`. Il vous sera demandé de saisir votre mot de passe, si vous en avez créé un lors de l'inscription. Votre sauvegarde sera enregistrée dans un fichier d'archive du format `.gz`. |
        |Rétablir une sauvegarde |Après avoir réinstallé Jami, cliquez sur `Se connecter à partir d'une sauvegarde`. Accédez au fichier d'archive qui contient la sauvegarde de votre compte. Si nécessaire, saisissez votre mot de passe et cliquez sur `Se connecter à partir d'une sauvegarde`. |


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        ### Installation

        Installez Jami à partir de l'[App Store](https://apps.apple.com/fr/app/ring-a-gnu-package/id1306951055).

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Créez un compte |Ouvrez l'appli et cliquez sur `Créer un compte Jami`. Saisissez un nom d'utilisateur. |
        |Saisissez un mot de passe |Facultatif: vous pouvez également saisir un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour chiffrer votre compte. Veillez à conserver ce mot de passe en lieu sûr, il est impossible de le récupérer. |
        |Configurez votre profil |Facultatif: vous pouvez également définir un nom d'utilisateur visible par les autres utilisatrices et utilisateurs, ainsi qu'une photo de profil. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Appuyez sur `Démarrer une conversation` <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur <br>• Appuyez sur `Ajouter dans les contacts`. Ceci enverra une invitation à l'autre utilisateur <br>• Vous pouvez commencer à discuter dès que l'invitation est acceptée |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Enregistrer un message audio` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Partager` et envoyez le message à votre contact |
        |Envoyez un message vidéo |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Enregistrer un message vidéo` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Partager` et envoyez le message à votre contact |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran: lors d'un appel vidéo, cliquez sur le bouton `Partager` |
        |Envoyez un fichier |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer le fichier` se trouve à côté du champ de saisie de texte <br>• Sélectionnez le fichier à envoyer |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en groupe |Au moment d'écrire ces lignes, Jami [travaille toujours à la mise en œuvre de la fonctionnalité de tchat de groupe](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Aussi appelés « Swarms », ces conversations textuelles sont entièrement distribuées et de pair à pair, avec un nombre potentiellement illimité de participants. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal <br>• Une fois l'appel lancé, appuyez sur l'icône `Ajouter`  |
        |Lancez un appel vidéo en groupe |Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Une fois l'appel lancé, appuyez sur l'icône `Ajouter` <br>• Partage d'écran: lors d'un appel vidéo de groupe, cliquez sur le bouton `Partager` |

        ### Sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Créez des sauvegardes |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Assurez-vous de [créer des sauvegardes régulières](https://gofoss.net/fr/backups/) en allant dans `Réglages ‣ Compte ‣ Compte ‣ Sauvegarder votre compte`. Il vous sera demandé de saisir votre mot de passe, si vous en avez créé un lors de l'inscription. Votre sauvegarde sera enregistrée dans un fichier d'archive du format `.gz`. |
        |Rétablir une sauvegarde |Après avoir réinstallé Jami, cliquez sur `Se connecter à partir d'une sauvegarde`. Accédez au fichier d'archive qui contient la sauvegarde de votre compte. Si nécessaire, saisissez votre mot de passe et cliquez sur `Se connecter à partir d'une sauvegarde`. |


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        ### Installation

        Téléchargez et exécutez l'[installateur Jami pour Windows](https://jami.net/download-jami-windows/).

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Créez un compte |Ouvrez l'appli et cliquez sur `Créer un compte Jami`. Saisissez un nom d'utilisateur. |
        |Saisissez un mot de passe |Facultatif: vous pouvez également saisir un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour chiffrer votre compte. Veillez à conserver ce mot de passe en lieu sûr, il est impossible de le récupérer. |
        |Configurez votre profil |Facultatif: vous pouvez également définir un nom d'utilisateur visible par les autres utilisatrices et utilisateurs, ainsi qu'une photo de profil. |
        |Créez une première sauvegarde |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Lors de l'inscription, vous pouvez [créer une première sauvegarde](https://gofoss.net/fr/backups/) de votre compte. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur <br>• Appuyez sur `Ajouter aux conversations`. Ceci enverra une invitation à l'autre utilisateur <br>• Vous pouvez commencer à discuter dès que l'invitation est acceptée |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer un message vocal` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Sinon, quittez l'enregistrement pour annuler votre message |
        |Envoyez un message vidéo |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer un message vidéo` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Sinon, quittez l'enregistrement pour annuler votre message  |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran: lors d'un appel vidéo, cliquez sur `Menu ‣ Partager votre écran` |
        |Envoyez un fichier |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer le fichier` se trouve à côté du champ de saisie de texte <br>• Sélectionnez le fichier à envoyer <br>• Vous pouvez aussi simplement glisser un fichier dans la fenêtre de Jami |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en groupe |Au moment d'écrire ces lignes, Jami [travaille toujours à la mise en œuvre de la fonctionnalité de tchat de groupe](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Aussi appelés « Swarms », ces conversations textuelles sont entièrement distribuées et de pair à pair, avec un nombre potentiellement illimité de participants. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal <br>• Une fois l'appel lancé, appuyez sur `Menu ‣ Ajouter des participants`  |
        |Lancez un appel vidéo en groupe |Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Une fois l'appel lancé, appuyez sur `Menu ‣ Ajouter des participants` <br>• Partage d'écran: lors d'un appel vidéo de groupe, cliquez sur `Menu ‣ Partager votre écran` |

        ### Sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Créez des sauvegardes |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Assurez-vous de [créer des sauvegardes régulières](https://gofoss.net/fr/backups/) en allant dans `Ouvrir les paramètres ‣ Compte ‣ Sauvegarder le compte dans un fichier .gz`. Il vous sera demandé de saisir votre mot de passe, si vous en avez créé un lors de l'inscription. Votre sauvegarde sera enregistrée dans un fichier d'archive du format `.gz`. |
        |Rétablir une sauvegarde |Après avoir réinstallé Jami, cliquez sur `Créer un compte à partir d'une sauvegarde`. Accédez au fichier d'archive qui contient la sauvegarde de votre compte. Si nécessaire, saisissez votre mot de passe et cliquez sur `Créer un compte à partir d'une sauvegarde`. |


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        ### Installation

        Téléchargez l'[image disque de Jami](https://jami.net/download-jami-macos/), ouvrez-la et faites glisser l'icône au-dessus du dossier d'application. Pour y accéder facilement, ouvrez le dossier d'applications et faites glisser l'icône Jami vers le dock.

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Créez un compte |Ouvrez l'appli et cliquez sur `Créer un compte Jami`. Saisissez un nom d'utilisateur. |
        |Saisissez un mot de passe |Facultatif: vous pouvez également saisir un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour chiffrer votre compte. Veillez à conserver ce mot de passe en lieu sûr, il est impossible de le récupérer. |
        |Configurez votre profil |Facultatif: vous pouvez également définir un nom d'utilisateur visible par les autres utilisatrices et utilisateurs, ainsi qu'une photo de profil. |
        |Créez une première sauvegarde |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Lors de l'inscription, vous pouvez [créer une première sauvegarde](https://gofoss.net/fr/backups/) de votre compte. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur <br>• Appuyez sur `Ajouter aux conversations`. Ceci enverra une invitation à l'autre utilisateur <br>• Vous pouvez commencer à discuter dès que l'invitation est acceptée |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer un message vocal` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Sinon, quittez l'enregistrement pour annuler votre message |
        |Envoyez un message vidéo |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer un message vidéo` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Sinon, quittez l'enregistrement pour annuler votre message  |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran: lors d'un appel vidéo, cliquez sur `Menu ‣ Partager votre écran` |
        |Envoyez un fichier |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer le fichier` se trouve à côté du champ de saisie de texte <br>• Sélectionnez le fichier à envoyer <br>• Vous pouvez aussi simplement glisser un fichier dans la fenêtre de Jami |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en groupe |Au moment d'écrire ces lignes, Jami [travaille toujours à la mise en œuvre de la fonctionnalité de tchat de groupe](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Aussi appelés « Swarms », ces conversations textuelles sont entièrement distribuées et de pair à pair, avec un nombre potentiellement illimité de participants. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal <br>• Une fois l'appel lancé, appuyez sur `Menu ‣ Ajouter des participants`  |
        |Lancez un appel vidéo en groupe |Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Une fois l'appel lancé, appuyez sur `Menu ‣ Ajouter des participants` <br>• Partage d'écran: lors d'un appel vidéo de groupe, cliquez sur `Menu ‣ Partager votre écran` |

        ### Sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Créez des sauvegardes |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Assurez-vous de [créer des sauvegardes régulières](https://gofoss.net/fr/backups/) en allant dans `Ouvrir les paramètres ‣ Compte ‣ Sauvegarder le compte dans un fichier .gz`. Il vous sera demandé de saisir votre mot de passe, si vous en avez créé un lors de l'inscription. Votre sauvegarde sera enregistrée dans un fichier d'archive du format `.gz`. |
        |Rétablir une sauvegarde |Après avoir réinstallé Jami, cliquez sur `Créer un compte à partir d'une sauvegarde`. Accédez au fichier d'archive qui contient la sauvegarde de votre compte. Si nécessaire, saisissez votre mot de passe et cliquez sur `Créer un compte à partir d'une sauvegarde`. |


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        ### Installation

        | Instructions | Description |
        | ------ | ------ |
        |Ouvrez le terminal |Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), utilisez le raccourci `Ctrl+Alt+T` ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. |
        |Installez les dépendances |`sudo apt install gnupg dirmngr ca-certificates curl --no-install-recommends` |
        |Ajoutez les clés de signature |`curl -s https://dl.jami.net/public-key.gpg | sudo tee /usr/share/keyrings/jami-archive-keyring.gpg > /dev/null` |
        |Ajoutez le dépôt à la liste des sources apt |`sudo sh -c "echo 'deb [signed-by=/usr/share/keyrings/jami-archive-keyring.gpg] https://dl.jami.net/nightly/ubuntu_20.04/ jami main' > /etc/apt/sources.list.d/jami.list"` |
        |Mettez à jour le cache apt local |`sudo apt update` |
        |Installez le client Jami |`sudo apt install jami`|

        ### Inscription

        | Instructions | Description |
        | ------ | ------ |
        |Créez un compte |Ouvrez l'appli et cliquez sur `Créer un compte Jami`. Saisissez un nom d'utilisateur. |
        |Saisissez un mot de passe |Facultatif: vous pouvez également saisir un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour chiffrer votre compte. Veillez à conserver ce mot de passe en lieu sûr, il est impossible de le récupérer. |
        |Configurez votre profil |Facultatif: vous pouvez également définir un nom d'utilisateur visible par les autres utilisatrices et utilisateurs, ainsi qu'une photo de profil. |
        |Créez une première sauvegarde |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Lors de l'inscription, vous pouvez [créer une première sauvegarde](https://gofoss.net/fr/backups/) de votre compte. |

        ### Tchats, appels vocaux et appels vidéos en tête-à-tête

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en tête-à-tête |• Lancez l'appli <br>• Recherchez des contacts à l'aide de leur nom d'utilisateur <br>• Appuyez sur `Ajouter aux conversations`. Ceci enverra une invitation à l'autre utilisateur <br>• Vous pouvez commencer à discuter dès que l'invitation est acceptée |
        |Envoyez un message vocal |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer un message vocal` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Sinon, quittez l'enregistrement pour annuler votre message |
        |Envoyez un message vidéo |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer un message vidéo` se trouve à côté du champ de saisie de texte <br>• Appuyez dessus pour enregistrer votre message <br>• Lorsque vous êtes prêt, cliquez sur le bouton `Envoyer` <br>• Sinon, quittez l'enregistrement pour annuler votre message  |
        |Lancez un appel vocal en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal |
        |Lancez un appel vidéo en tête-à-tête |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Partage d'écran: lors d'un appel vidéo, cliquez sur `Menu ‣ Partager votre écran` |
        |Envoyez un fichier |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Le bouton `Envoyer le fichier` se trouve à côté du champ de saisie de texte <br>• Sélectionnez le fichier à envoyer <br>• Vous pouvez aussi simplement glisser un fichier dans la fenêtre de Jami |

        ### Tchats, appels vocaux et appels vidéos en groupe

        | Instructions | Description |
        | ------ | ------ |
        |Lancez un tchat en groupe |Au moment d'écrire ces lignes, Jami [travaille toujours à la mise en œuvre de la fonctionnalité de tchat de groupe](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Aussi appelés « Swarms », ces conversations textuelles sont entièrement distribuées et de pair à pair, avec un nombre potentiellement illimité de participants. |
        |Lancez un appel vocal en groupe |• Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Téléphone` dans le menu supérieur pour lancer un appel vocal <br>• Une fois l'appel lancé, appuyez sur `Menu ‣ Ajouter des participants`  |
        |Lancez un appel vidéo en groupe |Lancez l'appli <br>• Ouvrez un tchat existant ou commencez-en un nouveau <br>• Appuyez sur l'icône `Caméra` dans le menu supérieur pour lancer un appel vidéo <br>• Une fois l'appel lancé, appuyez sur `Menu ‣ Ajouter des participants` <br>• Partage d'écran: lors d'un appel vidéo de groupe, cliquez sur `Menu ‣ Partager votre écran` |

        ### Sauvegardes

        | Instructions | Description |
        | ------ | ------ |
        |Créez des sauvegardes |Les informations de votre compte sont stockées localement sur votre appareil. Elles seront définitivement effacées si vous perdez votre appareil ou si vous désinstallez l'application. Assurez-vous de [créer des sauvegardes régulières](https://gofoss.net/fr/backups/) en allant dans `Ouvrir les paramètres ‣ Compte ‣ Sauvegarder le compte dans un fichier .gz`. Il vous sera demandé de saisir votre mot de passe, si vous en avez créé un lors de l'inscription. Votre sauvegarde sera enregistrée dans un fichier d'archive du format `.gz`. |
        |Rétablir une sauvegarde |Après avoir réinstallé Jami, cliquez sur `Créer un compte à partir d'une sauvegarde`. Accédez au fichier d'archive qui contient la sauvegarde de votre compte. Si nécessaire, saisissez votre mot de passe et cliquez sur `Créer un compte à partir d'une sauvegarde`. |



<br>

<center> <img src="../../assets/img/separator_briar.svg" alt="Briar" width="150px"></img> </center>

## Briar

[Briar](https://briarproject.org/) est une application de messagerie [open source](https://code.briarproject.org/briar/briar/tree/master/) pour Android. Elle s'adresse en particulier aux personnes dont le modèle de menace est élevé, comme les activistes ou les journalistes. Briar est indépendant de « Google Cloud Messaging (GCM) » et repose sur une architecture de pair à pair, où les utilisateurs se connectent directement sans passer par des serveurs.

Par défaut, toutes les communications sont chiffrées de bout en bout et envoyées sur le [réseau Tor](https://gofoss.net/fr/tor/). Briar prend en charge la confidentialité de transmission parfaite (en anglais, « perfect forward secrecy »). Toutes les données sont stockées en local sur l'appareil, aucune inscription via un numéro de téléphone ou une adresse électronique n'est nécessaire. L'application peut également fonctionner sans connexion Internet, via WiFi ou Bluetooth. Notez toutefois que les utilisateurs doivent être en ligne pour pouvoir discuter entre eux. Vous trouverez ci-dessous des instructions plus détaillées.

??? tip "Montrez-moi le guide étape par étape pour Android"

    ### Installation

    | Instructions | Description |
    | ------ | ------ |
    |Installez Briar |Téléchargez l'appli: <br><br> • du [Google Play Store](https://play.google.com/store/apps/details?id=org.briarproject.briar.android&hl=fr) <br> • de [F-Droid](https://f-droid.org/fr/packages/org.briarproject.briar.android/) <br> • de l'[Aurora Store](https://auroraoss.com/) |
    |Aucun traqueur |L'application [contient 0 traqueur et nécessite 11 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/org.briarproject.briar.android/latest/). À titre de comparaison:<br>• TikTok: 16 traqueurs, 76 autorisations<br>• Snapchat: 2 traqueurs, 44 autorisations<br>• WhatsApp: 1 traqueur, 57 autorisations |
    |Pas de GCM |Notez que Briar peut gérer les notifications sans « Google Cloud Messaging (GCM) ». C'est très pratique si vous cherchez à [dé-googler votre téléphone](https://gofoss.net/fr/intro-free-your-phone/). |

    ### Inscription

    | Instructions | Description |
    | ------ | ------ |
    |Créez un compte |Lancez l'appli et saisissez un nom d'utilisateur. |
    |Saisissez un mot de passe |Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Veillez à conserver ce mot de passe en lieu sûr : si vous l'oubliez, votre compte sera perdu à jamais ! |
    |Activer les connexions en arrière-plan |Suivez les instructions pour désactiver l'optimisation de la batterie et pour permettre à Briar de travailler en arrière-plan. |
    |Créez un compte |Finalement, appuyez sur le bouton `Créer un compte`. |

    ### Tchats en tête-à-tête

    | Instructions | Description |
    | ------ | ------ |
    |Rencontrez-vous en personne |• Rencontrez votre contact en personne et ouvrez l'appli <br>• Appuyez sur le bouton `+` et sélectionnez `Ajouter un contact à proximité` <br>• Appuyez sur `Poursuivre` <br>• Vous devez tous les deux scanner les codes QR qui apparaissent sur vos écrans <br>• De cette façon, votre communication est authentifiée dès le départ <br>• Vous pouvez commencer à discuter |
    |Faites vous présenter |• Si vous ne pouvez pas vous rencontrer en personne, faites-vous présenter par un·e ami·e commun·e en qui vous avez confiance <br>• Cet·te ami·e doit ouvrir Briar sur son téléphone <br>• Ensuite il ou elle doit appuyer sur votre nom dans sa liste des contacts <br>• Il ou elle devra ensuite sélectionner `Menu ‣ Faire les présentations` <br>• Puis choisir l'autre contact auquel vous voulez être présenté(e) <br>• Pour finir, il ou elle devra appuyer sur `Faire les présentations` <br>• Vous recevrez une demande d'acceptation <br>• Une fois que les deux parties ont accepté l'introduction, vous pouvez commencer à tchatter |
    |Ajoutez des contacts à distance |• Si vous ne pouvez pas vous rencontrer en personne, ajoutez votre contact à distance <br>• Appuyez sur le bouton `+` et sélectionnez `Ajouter un contact éloigné` <br>• Partagez le lien qui s'affiche avec votre contact en utilisant un canal sécurisé (par exemple, un courriel chiffré, Signal, etc.). <br>• De même, demandez à votre contact de partager son lien avec vous <br> Vérifiez que le lien est authentique avant de le saisir dans l'appli (par exemple, en passant un appel, en discutant sur un tchat préalablement authentifié, ou en se rencontrant) <br>• Une fois les deux liens ajoutés, appuyez sur `Continuer` et choisissez un surnom pour votre nouveau contact <br>• Dès que votre contact accepte l'invitation, vous pouvez commencer à chatter |

    ### Tchats en groupe, forums, blogues et flux RSS

    | Instructions | Description |
    | ------ | ------ |
    |Lancez un tchat en groupe |• Lancez l'appli <br>• Appuyez sur `Menu ‣ Groupes privés` <br>• Appuyez sur le bouton `+` pour créer un nouveau groupe <br>• Choisissez un nom, appuyez sur `Créer un groupe` et invitez vos contacts <br>• Commencez à tchatter |
    |Lancez un forum |• Un forum est une conversation publique <br>• Contrairement aux discussions de groupe privées, tout le monde peut inviter des contacts additionnels <br>• Lancez l'appli <br>• Appuyez sur `Menu ‣ Forums` <br>• Appuyez sur le bouton `+` pour créer un nouveau forum <br>• Choisissez un nom et appuyez sur `Créer un forum` <br>• Ouvrez le nouveau forum et appuyez sur l'icône `Partager` pour inviter vos contacts |
    |Lancez un blogue |• Le blogue permet de partager des nouvelles avec vos contacts <br>• Lancez l'appli <br>• Appuyez sur `Menu ‣ Blogues` <br>• Appuyez sur l'icône `stylo` pour rédiger un nouveau billet, puis appuyez sur `Publier` |
    |Lisez des flux RSS |• Suivez des blogues ou des sites d'information à partir de Briar ! <br>• Lancez l'appli <br>• Appuyez sur `Menu ‣ Blogues` <br>• Appuyez sur `Menu ‣ Fils RSS ‣ + ‣ Importer un fil RSS` <br>• Entrez l'URL et appuyez sur `Importer` |

    ### Sauvegardes et verrouillage d'écran

    | Instructions | Description |
    | ------ | ------ |
    |Sauvegardes |• Pour des raisons de sécurité, votre compte sera définitivement supprimé si vous perdez l'accès à votre appareil, oubliez votre mot de passe ou désinstallez Briar <br>• Cela inclut vos identités, vos contacts et vos messages <br>• Vous devrez ajouter et vérifier à nouveau vos contacts après avoir réinstallé l'appli <br>• En outre, il n'y a pas de possibilité de migrer votre compte vers un autre appareil |
    |Verrouillage de l'écran |• Briar peut être verrouillé sans se déconnecter <br>• Appuyez sur `Menu ‣ Paramètres ‣ Sécurité` <br>• Activez `Verrou de l'appli` <br>• Briar se verrouille désormais automatiquement lorsque l'appli est inactive (par exemple après 5 minutes) ou lorsque vous appuyez sur `Menu ‣ Verrouiller l'appli` <br>• Pour déverrouiller Briar, utilisez le système de déverrouillage de votre appareil (code NIP, motif, mot de passe) |



??? warning "Briar est chiffré, mais pas anonyme"

    Briar met en place plusieurs mesures pour assurer la « non-associativité ». Malgré cela, [Briar ne peut garantir l'anonymat](https://code.briarproject.org/briar/briar/-/wikis/FAQ#does-briar-provide-anonymity). Si personne ne peut découvrir qui sont vos contacts, vos contacts peuvent très bien être en mesure de découvrir qui vous êtes. En effet, Briar partage votre adresse Bluetooth ainsi que vos adresses IPv4 et IPv6 récentes de l'interface WiFi, afin de se connecter à vos contacts.

<br>


<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions :

* référez-vous à la [documentation de Signal](https://support.signal.org/hc/fr/) ou demandez de l'aide à la [communauté Signal](https://community.signalusers.org/).

* référez-vous à la [documentation d'Element](https://element.io/help) ou demandez de l'aide à la [communauté Element](https://teddit.net/r/elementchat/).

* référez-vous à la [documentation de Jami](https://jami.net/help/) ou demandez de l'aide à la [communauté Jami](https://forum.jami.net/).

* référez-vous à la [documentation de Briar](https://briarproject.org/manual/fr/) ou demandez de l'aide sur le [chat Matrix](https://matrix.to/#/#freenode_#briar:matrix.org).


<br>

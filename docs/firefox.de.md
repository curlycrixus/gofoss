---
template: main.html
title: So macht Ihr Firefox sicherer
description: Mozilla Firefox oder Chrome. Ist Firefox sicherer als Google? Wie kann ich Firefox installieren? Wie installiert man Firefox auf Android? Wie macht man Firefox sicherer?
---

# Firefox, ein freier und quelloffener Browser

!!! level "Letzte Aktualisierung: Mai 2022. Für Anfänger und erfahrenere BenutzerInnen. Technische Kenntnisse können erforderlich sein."


<center>
<img align="center" src="../../assets/img/firefox_logo.png" alt="Mozilla Firefox browser" width ="150px"></img>
</center>

Der [Firefox](https://www.mozilla.org/de/firefox/new/) Browser verbindet erfolgreich Datenschutz, Sicherheit und Nutzungskomfort. Firefox wurde erstmals im Jahr 2004 von der Mozilla-Gemeinschaft veröffentlicht. Firefox ist ein freier und quelloffener Browser, allerdings enthält die Android-Version proprietäre Bibliotheken, wie Google Analytics. Der Firefox Browser ist in hohem Maße konfigurierbar, blockiert standardmäßig Cookies zur seitenübergreifenden Aktivitätenverfolgung und funktioniert reibungslos auf nahezu jedem Gerät. Untenstehend findet Ihr eine ausführliche Anleitung.


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        Ladet das [Firefox-Installationsprogramm für Windows](https://www.mozilla.org/de/firefox/windows/) herunter und führt es aus.


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        Ladet das [Firefox-Installationsprogramm für macOS](https://www.mozilla.org/de/firefox/mac/) herunter, öffnet die heruntergeladene `.dmg`-Datei und zieht das Firefox-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das Firefox-Symbol in das Andockmenü.

=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, öffnet das Terminal mit der Tastenkombination `Strg+Alt+T` oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`.

        ```bash
        sudo apt install firefox
        ```

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        Ladet Firefox aus dem [App Store](https://play.google.com/store/apps/details?id=org.mozilla.firefox&hl=de&gl=DE) herunter, oder ruft von Eurem Android-Gerät aus die [Firefox-Download-Seite](https://www.mozilla.org/de/firefox/browsers/mobile/) auf. Ihr habt ebenfalls die Möglichkeit, Firefox herunterzuladen oder zu aktualisieren ohne ein Google-Konto zu nutzen: [Aurora Store](https://auroraoss.com/de/)! Wir erklären in einem [späteren Kapitel](https://gofoss.net/de/foss-apps/) wie Ihr alternative App-Stores nutzen könnt.


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        Ladet die Firefox-App vom [App Store](https://apps.apple.com/de/app/firefox-private-safe-browser/id989804926/) herunter und installiert sie.

<div style="margin-top:-20px">
</div>

??? warning "Mozilla, der (nicht ganz so) weiße Ritter in Googles Gnaden"

    In einer von Google dominierten Browserlandschaft scheint Firefox derzeit der einzige erwähnenswerte und unabhängige Konkurrent zu sein. Deshalb empfehlen wir Firefox als freie und quelloffene Alternative zu Chrome-basierten Browsern. Gleichwohl teilen wir einige der Kritikpunkte an Mozilla. Dazu gehören fragwürdige technische Entscheidungen, wie die Integration des teilweise [proprietären Pocket](https://github.com/Pocket/extension-save-to-pocket/issues/75) oder das standardmäßige Zuschalten von [gesponsorten Inhalten](https://support.mozilla.org/de/kb/anpassung-der-einstellungen-von-firefox-suggest) und [Telemetrie](https://support.mozilla.org/de/kb/telemetriedaten-sammeln-und-loeschen). Zum Glück können diese Einstellungen wie auf dieser Seite beschrieben angepasst werden.

    Es gibt jedoch weitergehende Bedenken hinsichtlich der langfristigen Finanzierung der Mozilla Corporation. Das ist das Unternehmen, das hinter der Entwicklung, dem Vertrieb und der Förderung von Firefox, Thunderbird und anderen Anwendungen steht. Darüber hinaus handelt es sich bei der *Mozilla Corporation* um ein wirtschaftliches Unternehmen, welches der gemeinnützigen Organisation namens *Mozilla Foundation* untersteht. Wie es der Zufall will, erwirtschaftet die besagte Mozilla Corporation den größten Teil ihrer Einnahmen durch ein Abkommen mit... Google, ihrem Hauptkonkurrenten! Diese ungewöhnliche Allianz, die auf das Jahr 2006 zurückgeht, wurde bis 2023 verlängert und sichert [400 Millionen Euro](https://www.zdnet.com/article/sources-mozilla-extends-its-google-search-deal/) an jährlichen Einnahmen, indem Google als Standardsuchmaschine für Firefox festgelegt wird. Einerseits ist es durchaus legitim, dass Mozilla nach Finanzierungsquellen für die Entwicklung sucht. Allerdings bezeichnen manche diese Geschäftsbeziehung auch als Interessenkonflikt. Man kann sich in der Tat fragen, ob Google Mozilla nicht bezahlt, um Konkurrenz sowie Regulierungsbehörden in Schach zu halten. Ein Fuchs beißt nicht die Hand, die ihn füttert...


??? question "Gibt es andere datenschutzfreundliche Browser?"

    Natürlich, einige davon sind in der nachstehenden Tabelle aufgeführt. Aus Datenschutzgründen raten wir von der Nutzung von Chrome oder [Chromium](https://www.chromium.org/)-basierten Browsern ab. Leah Elliotts Comic [Contra Chrome](https://contrachrome.com/comic/279/) fasst bestens zusammen, warum der Google-Browser zu einer Bedrohung für die Privatsphäre der Nutzer geworden ist:

    * Chrome aber auch Chromium-basierte Browser beruhen auf Code, der letztlich von Google kontrolliert wird. Sie stärken damit Googles Browser-Monopol
    * Chrome sowie einige andere Chromium-basierte Browser sind das Tor zu Googles Ökosystem, einschließlich GMail, YouTube, Google Photos usw.
    * Googles Browser und Dienste sammeln systematisch private Nutzerdaten, meist ohne Zustimmung, um sie dann an den Meistbietenden zu verkaufen


    <div class="table-noscroll">

    <center>

    | | [Tor](https://gofoss.net/de/tor/) | [Librewolf](https://librewolf-community.gitlab.io/) | [FOSS Browser](https://github.com/scoute-dich/browser/) | [Mull](https://github.com/Divested-Mobile/mull/) | [Fennec F-Droid](https://f-droid.org/de/packages/org.mozilla.fennec_fdroid/) | [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium/) | [Brave](https://brave.com) | [Bromite](https://www.bromite.org/) |
    |------|:------:|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
    | Quelloffen | <span style="color:orange">✔</span>^1^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^1^ | <span style="color:green">✔</span> |
    | Desktop-Version | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Android-Version | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Daten-Isolierung (Tracking-Schutz) | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^2^ | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^2^ | <span style="color:orange">✔</span>^2^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Prozess-Isolierung (Sandbox) | <span style="color:orange">✔</span>^3^ | <span style="color:green">✔</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Fingerprinting-Schutz | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^4^ |
    | Inhaltsfilter | <span style="color:green">✔</span>^5^ | <span style="color:green">✔</span>^5^ | <span style="color:orange">✔</span>^6^ | <span style="color:green">✔</span>^5^ | <span style="color:green">✔</span>^5^ | <span style="color:red">✗</span> | <span style="color:green">✔</span> | <span style="color:orange">✔</span>^6^ |
    | Benutzer-definierte Suchmaschine | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Regelmäßige Sicherheits-Updates | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | Telemetrie- & Tracker-frei | <span style="color:red">✗</span>^7^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span>^8^ | <span style="color:green">✔</span>^9^ | <span style="color:green">✔</span> | <span style="color:red">✗</span>^10^ | <span style="color:green">✔</span> |

    </center>

    </div>

    1. *Enthält proprietäre Bibliotheken.*

    2. *Nur Basis-Tracking-Schutz.*

    3. *Nur auf dem Desktop verfügbar. In der Android-Version werden Webseiten nicht in einem eigenen Prozess isoliert, was die Anfälligkeit für Sicherheitslücken begrenzen würde: Weder [Prozessisolierung pro Website](https://wiki.mozilla.org/Project_Fission) noch [Sandboxing](https://bugzilla.mozilla.org/show_bug.cgi?id=1565196) sind implementiert.*

    4. *Nur Basis-Fingerprinting-Schutz.*

    5. *Mit uBlock Origin kompatibel.*

    6. *Nur Basis-Inhaltsfilter.*

    7. *Tor Browser für Android enthält [3 Tracker (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/de/reports/org.torproject.torbrowser/latest/)*

    8. *Exodus Privacy meldet [2 Tracker (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/de/reports/us.spotco.fennec_dos/latest/) für Mull. Nach Angaben des Entwicklerteams handelt es sich dabei jedoch um einen [Fehlalarm](https://forum.f-droid.org/t/classyshark3exydus-found-five-trackers-inside-tor-browser/13453/20): Die Tracker-Bibliotheken wurden durch sogenannte "Stubs" ersetzt, also durch wirkungsfreien Code.*

    9. *Exodus Privacy meldet [2 Tracker (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/de/reports/org.mozilla.fennec_fdroid/latest/) für Fennec F-Droid. Nach Angaben des Entwicklerteams handelt es sich dabei jedoch um einen [Fehlalarm](https://forum.f-droid.org/t/welcome-a-new-fennec-f-droid/11113): Die Tracker-Bibliotheken wurden durch sogenannte "Stubs" ersetzt, also durch wirkungsfreien Code.*

    10. *Brave wird von einem gewinnorientierten, Risikokapital-finanzierten Unternehmen betrieben. Verfügt über ein (Opt-in)-Werbesystem und wurde in der Vergangenheit für die Einbindung von Affiliate-Links kritisiert, von denen es profitiert. Überträgt standardmäßig Telemetriedaten an einen Analysedienst, dies kann abgewählt werden.*


<br>

<center> <img src="../../assets/img/separator_ublockorigin.svg" alt="uBlock Origin" width="150px"></img> </center>

## Datenschutz-Erweiterungen

=== "Windows, macOS & Linux"

    Installiert [uBlock Origin](https://de.wikipedia.org/wiki/UBlock_Origin) für Firefox. Es handelt sich um einen hervorragenden freien und quelloffenen Inhaltsfilter! [Begebt Euch auf Mozillas Addon Store](https://addons.mozilla.org/de/firefox/addon/ublock-origin/) und klickt auf die Schaltfläche `Zu Firefox hinzufügen`. Verschiedene Einstellungen ermöglichen es, den Datenschutz zu erhöhen. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        * Klickt auf das uBlock Origin Symbol in der Symbolleiste von Firefox
        * Vergewissert Euch, dass uBlock Origin aktiviert ist, der große Schaltknopf sollte blau eingefärbt sein
        * Klickt auf das Übersicht-Symbol
        * Wählt den Reiter `Filterlisten`
        * Aktiviert die in der untenstehenden Tabelle aufgeführten Kontrollkästchen und klickt auf `Änderungen anwenden`

        <center>

        | Rubrik | Kontrollkästchen |
        | ------ | ------ |
        | Werbung | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Privatsphäre | ☑ AdGuard Tracking Protection <br> ☑ EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Belästigungen | ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br> ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>


=== "Android"

    Installiert [HTTPS Everywhere](https://www.eff.org/https-everywhere/) für Firefox. Es handelt sich um eine Datenschutzerweiterung, die standardmäßig das sichere Hypertext-Übertragungsprotokoll ([HTTPS](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol_Secure)) aktiviert, ein Verschlüsselungsverfahren zum Schutz des Internetverkehrs. Öffnet hierzu `Menü ‣ Add-ons` und klickt auf das `+`-Symbol neben dem HTTPS Everywhere Eintrag.

    Installiert ebenfalls [uBlock Origin](https://de.wikipedia.org/wiki/UBlock_Origin). Es handelt sich um einen hervorragenden freien und quelloffenen Inhaltsfilter! Öffnet hierzu `Menü ‣ Add-ons` und klickt auf das `+`-Symbol neben dem uBlock Origin Eintrag. Verschiedene Einstellungen ermöglichen es, den Datenschutz zu erhöhen. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        * Öffnet `Menü ‣ Add-ons ‣ uBlock Origin ‣ Einstellungen`
        * Wählt den Reiter `Filterlisten`
        * Aktiviert die in der untenstehenden Tabelle aufgeführten Kontrollkästchen und klickt auf `Änderungen anwenden`

        <center>

        | Rubrik | Kontrollkästchen |
        | ------ | ------ |
        | Werbung | ☑ AdGuard Base <br> ☑ AdGuard Mobile Ads <br> ☑ EasyList <br> |
        | Privatsphäre | ☑ AdGuard Tracking Protection <br> ☑ EasyPrivacy <br> ☑ Fanboy's Enhanced Tracking List <br> |
        | Belästigungen | ☑ AdGuard Annoyances <br> ☑ AdGuard Social Media <br> ☑ Anti-Facebook <br> ☑ EasyList Cookie <br> ☑ Fanboy's Annoyance <br> ☑ Fanboy's Social <br> ☑ uBlock filters - Annoyances <br> |

        </center>

<div style="margin-top:-20px">
</div>

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ae911b43-b4bd-4059-8447-891e82bd2367" frameborder="0" allowfullscreen></iframe>

    </center>

??? info "Weitere Hinweise zu Datenschutzerweiterungen"

    Geht **sparsam** mit Erweiterungen um. Browser-Erweiterungen erleichtern das Erfassen von [digitalen Fingerabdrücken](https://en.m.wikipedia.org/wiki/Device_fingerprint). Diese Praxis dient dazu, Informationen zu sammeln, Suchgewohnheiten zu erfassen und gezielt Werbung zu platzieren. Je mehr Erweiterungen Ihr nutzt, desto individueller Euer Fingerabdruck und desto größer die Angriffsfläche. Wollt Ihr wissen, wie einfach es ist Euren Browser zu identifizieren und zu überwachen? Schaut Euch mal [EFF's Cover Your Tracks](https://coveryourtracks.eff.org/) an.

    Genießt Erweiterungen mit **Vorsicht**. Manche Browser-Erweiterungen beeinträchtigen die korrekte Darstellung von Webseiten. Fügt neue Erweiterungen schrittweise hinzu und deaktiviert sie im Falle negativer Auswirkungen. Das richtige Verhältnis zwischen Datenschutz und Benutzerfreundlichkeit zu finden kann herausfordernd sein.

    Ein paar abschließende Ratschläge für BenutzerInnen **sozialer Netzwerke**. Aktiviert kein Kontrollkästchen in der Rubrik `Belästigungen` der Filterliste von uBlock Origin, wenn Ihr öfters die `Teilen`-Knöpfe von Facebook, Twitter und Co. verwendet.


    | Erweiterungen | Beschreibung |
    | ------ | ------ |
    | [Clear URLs](https://addons.mozilla.org/de/firefox/addon/clearurls/) | Entfernt Tracking-Elemente aus URLs. |
    | [Cookie autodelete](https://addons.mozilla.org/de/firefox/addon/cookie-autodelete/) | Löscht ungenutzte Cookies automatisch beim Schließen von Tabs. |
    | [I don't care about cookies](https://addons.mozilla.org/de/firefox/addon/i-dont-care-about-cookies/) | Beseitigt Cookie-Warnungen. |
    | [Miner block](https://addons.mozilla.org/de/firefox/addon/minerblock-origin/) | Blockt Kryptowährungsschürfer. |
    | [Cloud firewall](https://addons.mozilla.org/de/firefox/addon/cloud-firewall/) | Blockt Verbindungen zu Cloud-Diensten von Google, Amazon, Facebook, Microsoft, Apple und Cloudflare. |
    | [CSS exfil protection](https://addons.mozilla.org/de/firefox/addon/css-exfil-protection/) | Schützt Euren Browser vor Datendiebstahl auf Webseiten, die CSS verwenden. |
    | [Disconnect](https://addons.mozilla.org/de/firefox/addon/disconnect/) | Visualisiert und blockiert Web-Tracking. |
    | [Noscript](https://noscript.net/) | Erlaubt ausschließlich vertrauenswürdigen Webseiten JavaScript, Java, Flash oder andere Plugins auszuführen. |
    | [HTTPS everywhere](https://addons.mozilla.org/de/firefox/addon/https-everywhere/) | Verschlüsselt den Internetverkehr und macht das Surfen im Web sicherer. |
    | [Privacy badger](https://addons.mozilla.org/de/firefox/addon/privacy-badger17/) | Stoppt Werbebetreiber und andere Drittanbieter, Euch heimlich auszuspionieren. |
    | [Decentraleyes](https://addons.mozilla.org/de/firefox/addon/decentraleyes/) | Blockt das Tracking über sogenannte *Content Delivery Networks*, die von Drittparteien betrieben werden. |
    | [Terms of service; didn't read](https://addons.mozilla.org/de/firefox/addon/terms-of-service-didnt-read/) | Erleichter das Verständnis von Datenschutzrichtlinien verschiedener Webseiten, inklusive Bewertung und Zusammenfassung. |


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Firefox sichern" width="150px"></img> </center>

## Datenschutz- und Sicherheitseinstellungen

=== "Windows, macOS & Linux"

    Öffnet eine neue Tab-Seite in Firefox. Entfernt überflüssige Elemente, wie z.B. `Neueste Aktivität` oder `Verknüpfungen`. Gebt anschließend `about:preferences` in die Adressleiste ein, um auf die Datenschutz- und Sicherheitseinstellungen von Firefox zuzugreifen. Untenstehend findet Ihr eine ausführliche Anleitung.


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        *Warnhinweis*: Wendet die folgenden Einstellungen mit Vorsicht an, manche von ihnen beeinträchtigen möglicherweise die korrekte Darstellung von Webseiten. Fügt neue Einstellungen schrittweise hinzu und deaktiviert sie im Falle negativer Auswirkungen.

        <center>

        | Menü | Einstellung |
        | ------ | ------ |
        | Allgemein | Deaktiviert im Abschnitt `Sprache` das Kontrollkästchen `Rechtschreibung während der Eingabe überprüfen`. |
        | Allgemein | Deaktiviert im Abschnitt `Surfen` die beiden Kontrollkästchen `Erweiterungen während des Surfens empfehlen` und `Funktionen während des Surfens empfehlen`. |
        | Startseite | Deaktiviert im Abschnitt `Inhalte des Firefox-Startbildschirms` die Kontrollkästchen `Verknüpfungen`, `Neueste Aktivität` und `Kurzinformationen`. |
        | Suche | Deaktiviert im Abschnitt `Suchvorschläge` das Kontrollkästchen `Suchvorschläge anzeigen`. |
        | Suche | Entfernt im Abschnitt `Suchmaschinen-Schlüsselwörter` die Einträge Google, Bing, Amazon und Ebay. |
        | Suche | Begebt Euch auf die Seite von [Disroot Searx](https://search.disroot.org/) und fügt sie zu den Standardsuchmaschinen hinzu. Führt hierzu einen Rechtsklick oder einen Klick auf das 3-Punkte-Aktionsmenü in der Adressleiste aus. <br><br> *Hinweis*: Am Ende dieses Abschnitts findet Ihr weitere Vorschläge zu datenschutzfreundlichen Suchmaschinen. |
        | Suche | Begebt Euch wieder in den Abschnitt `Standardsuchmaschine` in den Firefox-Einstellungen und wählt den Eintrag Disroot SearX aus. |
        | Datenschutz & Sicherheit | Der Abschnitt `Verbesserter Schutz vor Aktivitätenverfolgung` enthält Einstellungen, um Drittparteien davon abzuhalten Euch über verschiedene Webseiten hinweg zu verfolgen. Wählt zwischen den Einstellungen `Standard` und `Streng`. Bei strengeren Einstellungen werden mehr Tracker und Werbung blockiert, aber es kann sein, dass manche Webseiten nicht korrekt dargestellt werden können. |
        | Datenschutz & Sicherheit | Aktiviert im Abschnitt `Verbesserter Schutz vor Aktivitätenverfolgung` ebenfalls das Kontrollkästchen `Websites immer eine Do Not Track-Information senden`.|
        | Datenschutz & Sicherheit | Aktiviert im Abschnitt `Cookies und Website-Daten` das Kontrollkästchen `Cookies und Website-Daten beim Beenden von Firefox löschen`. Klickt dann auf `Daten entfernen` und löscht alle von Firefox gespeicherten Cookies sowie Webseitendaten. |
        | Datenschutz & Sicherheit | Deaktiviert im Abschnitt `Zugangsdaten und Passwörter` das Kontrollkästchen `Fragen, ob Zugangsdaten und Passwörter für Websites gespeichert werden sollen`. |
        | Datenschutz & Sicherheit | Aktiviert im Abschnitt `Chronik` das Kontrollkästchen `Firefox wird eine Chronik nach benutzerdefinierten Einstellungen anlegen`. <br><br>Deaktiviert die Kontrollkästchen `Besuchte Seiten und Download-Chronik speichern` und `Eingegebene Suchbegriffe und Formulardaten speichern`. <br><br>Aktiviert stattdessen das Kontrollkästchen `Die Chronik löschen, wenn Firefox geschlossen wird`. Klickt dann auf `Chronik löschen` und löscht alle von Firefox gespeicherten Daten. <br><br>*Hinweis*: dies ist ein Notbehelf, denn aus irgendeinem unerfindlichen Grund macht der Eintrag `Firefox wird eine Chronik niemals anlegen` viele Add-ons unbrauchbar. |
        | Datenschutz & Sicherheit | Deaktiviert im Abschnitt `Datenerhebung durch Firefox und deren Verwendung` alle Einträge. |
        | Datenschutz & Sicherheit | Deaktiviert im Abschnitt `Sicherheit` den Eintrag `Gefährliche und betrügerische Inhalte blockieren`. Diese Einstellung hilft zwar bei der Erkennung von Phishing und Malware, aber nur, indem sie [eine Verbindung zu Googles Servern herstellt](https://support.mozilla.org/de/kb/wie-funktioniert-schutz-vor-betrugsversuchen-und-schadprogrammen). |
        | Datenschutz & Sicherheit | Aktiviert im Abschnitt `Nur-HTTPS-Modus` das Kontrollkästchen `Nur-HTTPS-Modus in allen Fenstern aktivieren`. Dadurch wird standardmäßig das sichere Hypertext-Übertragungsprotokoll ([HTTPS](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol_Secure)) aktiviert, ein Verschlüsselungsverfahren zum Schutz des Internetverkehrs. Jedes Mal, wenn Ihr eine Webseite aufruft, sollte in der Adressleiste von Firefox ein (grünes) Schloss angezeigt werden. |
        | Datenschutz & Sicherheit | Deaktiviert im Abschnitt `Adressleiste` die Einträge `Kontextbezogene Vorschläge` und `Gelegentlich gesponserte Vorschläge einbeziehen`. |
        | Allgemein | (Optional) Aktiviert das Kontrollkästchen `Immer überprüfen, ob Firefox der Standardbrowser ist` und klickt auf `Als Standard festlegen`. |

        </center>


=== "Android"

    Startet Firefox. Scrollt auf dem Begrüßungsbildschirm nach unten und klickt auf `Lossurfen`. Entfernt überflüssige Elemente von der Tabseite, wie z.B. `Google` oder `Top Artikel`. Begebt Euch anschließend auf `Menü ‣ Einstellungen` , um auf die Datenschutz- und Sicherheitseinstellungen von Firefox zuzugreifen. Untenstehend findet Ihr eine ausführliche Anleitung.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        *Warnhinweis*: Wendet die folgenden Einstellungen mit Vorsicht an, manche von ihnen beeinträchtigen möglicherweise die korrekte Darstellung von Webseiten. Fügt neue Einstellungen schrittweise hinzu und deaktiviert sie im Falle negativer Auswirkungen.

        <center>

        | Menü | Einstellungen |
        | ------ | ------ |
        | Suchen | Entfernt im Abschnitt `Standardsuchmaschine` die Einträge Google, Bing, Amazon, Qwant und Ebay. |
        | Suchen | Klickt im Abschnitt `Standardsuchmaschine` auf `+ Suchmaschine hinzufügen`. Gebt unter `Andere` Folgendes an:<br><br> • `Name`: `Disroot SearX`<br><br> • `Zu verwendender Such-String`: `https://search.disroot.org/search?q=%s` <br><br>Klickt dann auf `🗸` um alle Änderungen zu übernehmen. <br><br>*Hinweis*: Am Ende dieses Abschnitts findet Ihr weitere Vorschläge zu datenschutzfreundlichen Suchmaschinen. |
        | Suchen | Wählt im Abschnitt `Standardsuchmaschine` den Eintrag `Disroot SearX`. |
        | Suchen | Deaktiviert im Abschnitt `Adressleiste` die Optionen `Autovervollständigung von Adressen`, `Vorschläge aus der Zwischenablage anzeigen`, `Browser-Chronik durchsuchen` und `Suchvorschläge anzeigen`. |
        | Anpassen | Deaktiviert im Abschnitt `Startseite` den Eintrag `Meistbesuchte Seiten anzeigen`. |
        | Zugangsdaten und Passwörter | Ändert den Eintrag `Zugangsdaten und Passwörter speichern` in `Nie speichern` um. |
        | Zugangsdaten und Passwörter | Deaktiviert `Automatisches Ausfüllen in Firefox`. |
        | Verbesserter Schutz vor Aktivitätenverfolgung | Stellt sicher, dass die Option `Verbesserter Schutz vor Aktivitätenverfolgung` aktiviert ist, damit Drittparteien Euch nicht über verschiedene Webseiten hinweg verfolgen können. Wählt zwischen den Einstellungen `Standard` und `Streng`. Bei strengeren Einstellungen werden mehr Tracker und Werbung blockiert, aber es kann sein, dass manche Webseiten nicht korrekt dargestellt werden können. |
        | Browser-Daten beim Beenden löschen | Aktiviert den Eintrag `Browser-Daten beim Beenden löschen`. Wenn Ihr offenen Tabs nach dem Schließen von Firefox wiederhergestellt haben wollt, deaktiviert das Kontrollkästchen `Offene Tabs`. |
        | Datenerhebung | Deaktiviert `Nutzungs- und technische Daten`, `Marketing-Daten` sowie `Studien`. |
        | Allgemein | (Optional) Aktiviert `Als Standardbrowser festlegen`. |

        </center>

<div style="margin-top: -20px;">
</div>

??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7e4abf0d-c96b-4455-bb44-6951e1b0619c" frameborder="0" allowfullscreen></iframe>

    </center>


??? info "Hier erfahrt Ihr mehr über datenschutzfreundliche Suchmaschinen"

    <center>

    | Suchmaschine | Beschreibung |
    | ------ | ------ |
    | [Searx](https://asciimoo.github.io/searx/) | Open-Source-Metasuchmaschine. Aggregiert anonyme Ergebnisse von verschiedenen Suchmaschinen. [Verschiedene Online-Instanzen sind zugänglich](https://searx.space), zum Beispiel von [Disroot](https://search.disroot.org/). Kann auch selbst gehostet werden. |
    | [Duckduckgo](https://duckduckgo.com/) | Metasuchmaschine mit Sitz in den USA, die hauptsächlich Ergebnisse von Bing/Yahoo aggregiert. |
    | [Ecosia](https://www.ecosia.org/) | Deutsche Metasuchmaschine, bietet hauptsächlich Bing-Ergebnisse an und pflanzt Bäume. |
    | [Swisscows](https://swisscows.com/) | Schweizer Metasuchmaschine, bietet hauptsächlich Bing-Ergebnisse an. |
    | [Mojeek](https://www.mojeek.com/) | Suchmaschine mit Sitz im Vereinigten Königreich. |
    | [Metager](https://metager.de/) | Deutsche Open-Source-Metasuchmaschine. |
    | [Qwant](https://www.qwant.com/) | Französische Metasuchmaschine, Risikokapital-finanziert (das Axel-Springer Unternehmen ist eines der Investoren). |
    | [Startpage](https://www.startpage.com/) | Niederländische Metasuchmaschine, bietet hauptsächlich Google-Ergebnisse an. System 1, ein Werbeunternehmen, ist seit Oktober 2019 Gesellschafter. |

    </center>


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Firefox sichern" width="150px"></img> </center>

## User.js

Firefox bietet eine ganze Reihe von erweiterten Datenschutz- und Sicherheitseinstellungen. Auf Desktop-Geräten kann man auf diese zugreifen, indem man `about:config` in die Adressleiste eingibt und eine Sicherheitswarnung bestätigt. Angesichts der schieren Menge an möglichen Einstellungen kann dies jedoch schnell mühsam werden. Es ist viel einfacher, eine einzige `user.js`-Datei zu installieren. Diese kleine JavaScript-Datei enthält eine Reihe von vorkonfigurierten Einstellungen und wird jedes Mal geladen, wenn Ihr Firefox startet. Untenstehend findet Ihr eine ausführliche Anleitung zur Installation der `user.js`-Datei!

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

    *Hinweis*: Wendet `user.js`-Dateien mit Vorsicht an. Je strenger die Datenschutz-Einstellungen, desto wahrscheinlicher ist es, dass manche Webseiten möglicherweise nicht korrekt dargestellt werden können!

    Legt eine Sicherungskopie Eurer aktuellen Konfiguration an, die in einer Datei names `pref.js` gespeichert ist:

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Schritt 1 | Gebt `about:support` in die Firefox-Adressleiste ein. |
    | Schritt 2 | Begebt Euch in den Abschnitt `Allgemeine Informationen`. |
    | Schritt 3 | Klickt auf `Ordner öffnen`. |
    | Schritt 4 | Legt eine Sicherungskopie der `pref.js`-Datei an. |

    </center>

    Ladet nun Eure bevorzugte `user.js`-Vorlage herunter. Am Ende dieses Abschnitts erfahrt Ihr mehr über die verfügbaren Vorlagen. Speichert die heruntergeladene `user.js`-Datei im selben Ordner wie die `pref.js`-Datei. Je nach Browser und Betriebssystem sollte dieser Ordner hier zu finden sein:


    <center>

    | Betriebssystem | Ordnerpfad |
    | ------ | ------ |
    | Windows | `%APPDATA%\Mozilla\Firefox\Profiles\XXXXXXXX.your_profile_name\user.js` |
    | macOS | `~/Library/Application Support/Firefox/Profiles/XXXXXXXX.your_profile_name` |
    | Linux (Ubuntu) | `~/.mozilla/firefox/XXXXXXXX.default-release/user.js` |

    </center>

    Falls Ihr zu irgendeinem Zeitpunkt zu Euren ursprünglichen Einstellungen zurückkehren wollt, könnt Ihr einfach die gesicherte `pref.js`-Datei wiederherstellen und die `user.js`-Datei löschen.


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/4b9255d5-3d9f-4319-bd3a-8214a396df18" frameborder="0" allowfullscreen></iframe>

    </center>


??? question "Hier findet Ihr user.js-Vorlagen"

    * [Arkenfox](https://github.com/arkenfox/user.js) schafft ein gutes Gleichgewicht zwischen Datenschutz und Benutzerfreundlichkeit. Weitere Informationen auf der [Wiki-Seite](https://github.com/arkenfox/user.js/wiki)
    * [Pyllyukko](https://github.com/pyllyukko/user.js/)
    * [Relaxed Pyllyukko](https://github.com/pyllyukko/user.js/tree/relaxed/)
    * [Privacy Handbuch, minimale Konfiguration](https://www.privacy-handbuch.de/download/minimal/user.js)
    * [Privacy Handbuch, moderate Konfiguration](https://www.privacy-handbuch.de/download/moderat/user.js)
    * [Privacy Handbuch, strenge Konfiguration](https://www.privacy-handbuch.de/download/streng/user.js)
    * [Ein Werkzeug zur Erstellung eigener user.js Dateien](https://ffprofile.com/)
    * [Erläuterung der gängigen user.js Einstellungen](http://kb.mozillazine.org/About:config_Entries/)


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Firefox Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten finden Ihr:

* in der [Firefox Dokumentation](https://support.mozilla.org/de/products/firefox/get-started). Ihr könnt auch gerne die [Firefox Gemeinschaft](https://support.mozilla.org/de/questions/new) um Unterstützung bitten.

* in der [uBlock Dokumentation](https://github.com/gorhill/uBlock/wiki/) oder einem der vielen [Online-Tutorials](https://www.maketecheasier.com/ultimate-ublock-origin-superusers-guide/), falls Ihr an einer fortgeschrittenen Nutzung interessiert seid.

<center>
<img align="center" src="https://imgs.xkcd.com/comics/perspective.png" alt="Firefox"></img>
</center>

<br>

---
template: main.html
title: Das Projektteam
description: gofoss.net ist ein ehrenamtlich geführtes Projekt. Lernt unsere Mannschaft kennen. Georg Jerska. Lenina Helmholtz. Tom Parsons.
---

# Das gofoss.net-Team

## Georg Jerska

<img src="../../assets/img/georg.png" alt="Georg" width="150px" align="left"></img> Georg arbeitet im Bereich der darstellenden Künste. Er verbrachte einen Teil seiner Karriere in Deutschland und hat ein besonderes Interesse am Schutz der Privatsphäre der Bürger.

<br><br>

## Lenina Helmholtz

<img src="../../assets/img/lenina.png" alt="Georg" width="150px" align="left"></img> Lenina hat einen technischen Werdegang und hält Hochschulvorlesungen. Sie ist immer an neuen FOSS-Tools interessiert.

<br><br>

## Tom Parsons

<img src="../../assets/img/tom.png" alt="Georg" width="150px" align="left"></img> Tom ist sehr sozial und glaubt an ein besseres Internet. Er hofft, seinen Beitrag zum Projekt leisten zu können.

<br><br>
---
template: main.html
title: So verschlüsselt Ihr Eure E-Mails
description: Datenschutzfreundliche E-Mail-Anbieter. Ist Protonmail sicherer als Gmail? Protonmail vs. Gmail? Tutanota vs. Protonmail? Ist Tutanota sicher?
---


# Verschlüsselt Eure E-Mails

!!! level "Letzte Aktualisierung: Mai 2022. Für Anfänger und erfahrenere BenutzerInnen. Technische Kenntnisse können erforderlich sein."

<div align="center">
<img src="../../assets/img/https5.png" alt="E-Mail-Verschlüsselung" width="250px"></img>
</div>

Wählt einen vertrauenswürdigen E-Mail-Anbieter. Prüft sorgfältig welche Funktionen verfügbar sind, welche Verschlüsselungstechnologien genutzt werden oder an welchen Standorten sich die Server befinden, denn Datenschutzgesetze ändern sich von Land zu Land. Dieses Kapitel bietet einen kurzen Überblick über beliebte, datenschutzfreundliche E-Mail-Anbieter. Es wird zudem erklärt, wie man PGP-Verschlüsselung für E-Mails verwendet.


<br>


<center> <img src="../../assets/img/separator_protonmail.svg" alt="Protonmail" width="150px"></img> </center>

## Protonmail

[Protonmail](https://protonmail.com/) gibt an, der weltweit größte sichere E-Mail-Dienst zu sein, der darüber hinaus durch Schweizer Datenschutzgesetze abgesichert ist. Protonmail wird unter anderem von US-Investoren (Charles River Ventures) und der Europäischen Union finanziert. Während die Apps von Protonmail [quelloffen](https://github.com/ProtonMail/) sind, gilt dies nicht für die Serverseite.

Zum Zeitpunkt der Abfassung dieses Textes umfasst das Einzelbenutzerkonto 500 MB kostenlosen Speicherplatz. Für 4 bis 24 EUR/Monat erhaltet Ihr Zugang zu weiteren BenutzerInnen und Speicherplatz sowie einer Fülle von Funktionen: Kalender, Kontakt- und E-Mail-Import, Bitcoin-Zahlungen, [VPN](https://gofoss.net/de/vpn/) und mehr.

??? warning "Einige Anmerkungen zur Verschlüsselung"

    <center>

    | Emails | Verschlüsselung |
    | ------ | ------ |
    | **Email-Versand zwischen Protonmail-NutzerInnen** |Nachrichtentext und Anhänge sind durchgängig verschlüsselt. Betreffzeilen und Empfänger-/Absenderadressen sind hingegen unverschlüsselt. |
    | **Email-Versand von Protonmail-NutzerInnen an andere Anbieter** |Nachrichtentext und Anhänge werden nur dann durchgängig verschlüsselt, wenn der Protonmail-Nutzer die Option `Für Außenstehende verschlüsseln` auswählt. Andernfalls wird nur TLS-Verschlüsselung angewendet, soweit der empfangende Mailserver dies unterstützt (dies bedeutet auch, dass der empfangende Anbieter die Nachricht mitlesen kann). In jedem Fall bleiben Betreffzeilen und Empfänger-/Absenderadressen unverschlüsselt.|
    | **Email-Empfang durch Protonmail-NutzerInnen von anderen Anbietern** |Nachrichtentext und Anhänge werden nur mit TLS verschlüsselt, wenn der Mailserver des Absenders dies unterstützt. Betreffzeilen und Empfänger-/Absenderadressen bleiben unverschlüsselt. |

    </center>


### Protonmail Clients

Neben dem [Webmail-Zugang](https://account.protonmail.com/login/) bietet Protonmail mobile Apps für Android und iOS an. In Desktop-Umgebungen unterstützt Protonmail [Thunderbird](https://www.thunderbird.net/de/), über eine sogenannte Bridge-Anwendung. Diese Funktion ist jedoch nur für kostenpflichtige Abonnements verfügbar. Alternativ dazu ist [ElectronMail](https://github.com/vladimiry/ElectronMail) ein freier und quelloffener Desktop-Client für Protonmail. Beachtet jedoch, dass ElectronMail eine inoffizielle Anwendung ist. Untenstehend findet Ihr eine ausführliche Anleitung

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

         Ladet einfach die Protonmail-App von [Googles Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=de&gl=DE) oder [Aurora Store](https://auroraoss.com/de/) herunter. Die App [enthält 0 Tracker und benötigt 14 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/ch.protonmail.android/latest/). Zum Vergleich: für Gmail sind es 1 Tracker und 55 Berechtigungen, für Outlook sind es 13 Tracker und 49 Berechtigungen und für Hotmail sind es 4 Tracker und 31 Berechtigungen.


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

         Ladet einfach die Protonmail-App vom [App Store](https://apps.apple.com/de/app/protonmail-encrypted-email/id979659905/) herunter.


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für ElectronMail in Windows (kein kostenpflichtiges Abonnement erforderlich)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | ElectronMail herunterladen |Ladet das [ElectronMail-Installationsprogramm für Windows](https://github.com/vladimiry/ElectronMail/releases) herunter und führt es aus. |
        | Master-Kennwort erstellen |Öffnet ElectronMail und gebt ein [sicheres, individuelles Master-Kennwort](https://gofoss.net/de/passwords/) ein, um Eure E-Mails zu schützen. |
        | Anmelden |Meldet Euch mit Eurem Protonmail-Konto an. Falls aktiviert, führt ebenfalls die Zwei-Faktor-Authentifizierung durch. |
        | Domäne |Wählt eine Domäne aus der Liste. Es gibt sogar eine `Onion`-Option, mit der Ihr Tor verwenden könnt. Klickt anschließend auf `Schließen`. |

        </center>


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Thunderbird in Windows (kostenpflichtiges Abonnement erforderlich)"

        ### Thunderbird in Windows installieren

        Ruft die [Thunderbird-Download-Seite](https://www.thunderbird.net/de/) auf und klickt auf die Schaltfläche `Kostenloser Download`. Sobald das Installationsprogramm heruntergeladen ist, klickt auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten.

        ### Protonmail Bridge in Windows installieren

        Thunderbird lässt sich gut mit Protonmail integrieren und stellt sicher, dass eingehende und versandte E-Mails verschlüsselt bleiben. Dies wird von der sogenannten Bridge-Anwendung gehandhabt, die nur für zahlende NutzerInnen verfügbar ist. Ladet hierzu [Protonmail Bridge für Windows](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/) herunter. Sobald das Installationsprogramm heruntergeladen ist, klickt auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten.

        ### Protonmail Bridge in Windows konfigurieren

        Öffnet die soeben installierte Protonmail Bridge-Anwendung und folgt dem Einrichtungsassistenten:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Meldet Euch mit Eurem Protonmail-Konto an. |
        | 2 | Klickt auf Euren Kontonamen und dann auf die Schaltfläche `Mailbox Configuration`. |
        | 3 | Ein Fenster mit dem Titel `Protonmail Bridge Mailbox Configuration` sollte sich einblenden. Hier werden die IMAP- und SMTP-Einstellungen angezeigt, einschließlich eines Passworts, das später für die Konfiguration von Thunderbird benötigt wird. |

        </center>

        ### Thunderbird in Windows konfigurieren

        Startet nun Thunderbird, ruft den Menüeintrag `Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Einrichtungsassistenten:

        <center>

        | Einstellung | Beschreibung |
        | ------ | ------ |
        | Euer Name | Gebt den Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure Protonmail E-Mail-Adresse ein. |
        | Passwort | Kopiert das Passwort aus dem Fenster `Protonmail Bridge Mailbox Configuration` und fügt es hier ein (*Gebt nicht Euer Protonmail-Passwort ein*, das wird nicht funktionieren). |
        | Passwort speichern | Setzt ein Häkchen bei `Kennwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Manuell einrichten | Klickt auf die Schaltfläche `Manuell einrichten`, und fügt die IMAP- und SMTP-Einstellungen ein, die im Fenster `Protonmail Bridge Mailbox Configuration` angezeigt werden (wählt für die Authentifizierung `Normales Passwort`). |
        | Erneut testen | Klickt auf die Schaltfläche `Erneut testen`, um Eure Verbindungseinstellungen zu überprüfen. |
        | Erweiterte Einstellungen | Klickt auf die Schaltfläche `Erweiterte Einstellungen`. Ein neues Fenster wird aufgerufen. Klickt einfach auf die Schaltfläche `OK`, ändert keine Einstellungen in diesem Fenster. |
        | Sicherheitsausnahme hinzufügen | Klickt im Pop-up-Fenster auf die Schaltfläche `Sicherheitsausnahme hinzufügen'. Damit wird bestätigt, dass Euer Computer (`127.0.0.1`) die Bridge-Anwendung ausführen kann. Möglicherweise müsst Ihr eine zweite Sicherheitsausnahme bestätigen, sobald Ihr Eure erste E-Mail versenden möchtet. |

        </center>

    ??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo für Thunderbird (kostenpflichtiges Abonnement erforderlich)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Quelle: Protonmail. Anweisungen sollten in ähnlicher Weise für macOS oder Linux gelten.*

        </center>


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für ElectronMail in macOS (kein kostenpflichtiges Abonnement erforderlich)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | ElectronMail herunterladen |Ladet das [ElectronMail Disk Image](https://github.com/vladimiry/ElectronMail/releases) herunter, öffnet es und zieht das ElectronMail-Symbol auf den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das ElectronMail-Symbol in das Andockmenü. |
        | Master-Kennwort erstellen |Öffnet ElectronMail und gebt ein [sicheres, individuelles Master-Kennwort](https://gofoss.net/de/passwords/) ein, um Eure E-Mails zu schützen. |
        | Anmelden |Meldet Euch mit Eurem Protonmail-Konto an. Falls aktiviert, führt ebenfalls die Zwei-Faktor-Authentifizierung durch. |
        | Domäne |Wählt eine Domäne aus der Liste. Es gibt sogar eine `Onion`-Option, mit der Ihr Tor verwenden könnt. Klickt anschließend auf `Schließen`. |

        </center>


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Thunderbird in macOS (kostenpflichtiges Abonnement erforderlich)"

        ### Thunderbird in macOS installieren

        Ruft die [Thunderbird-Download-Seite](https://www.thunderbird.net/de/) auf und klickt auf die Schaltfläche `Kostenloser Download`. Sobald das Installationsprogramm heruntergeladen ist, sollte es sich von selbst öffnen und ein neues Laufwerk einbinden, das die Thunderbird-Anwendung enthält. Sollte dies nicht der Fall sein, öffnet die heruntergeladene Thunderbird `dmg`-Datei und verschiebt das erscheinende Thunderbird-Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Thunderbird-Symbol in das Andockmenü.

        ### Protonmail Bridge in macOS installieren

        Thunderbird lässt sich gut mit Protonmail integrieren und stellt sicher, dass eingehende und versandte E-Mails verschlüsselt bleiben. Dies wird von der sogenannten Bridge-Anwendung gehandhabt, die nur für zahlende NutzerInnen verfügbar ist. Ladet hierzu [Protonmail Bridge für macOS](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/) herunter. Sobald das Installationsprogramm heruntergeladen ist, sollte es sich von selbst öffnen und ein neues Laufwerk einbinden, das die Protonmail-Anwendung enthält. Sollte dies nicht der Fall sein, öffnet die heruntergeladene Protonmail-Bridge `dmg`-Datei und verschiebt das erscheinende Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Protonmail-Symbol in das Andockmenü.

        ### Protonmail Bridge in macOS konfigurieren

        Öffnet die soeben installierte Protonmail Bridge-Anwendung und folgt dem Einrichtungsassistenten:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Meldet Euch mit Eurem Protonmail-Konto an. |
        | 2 | Klickt auf Euren Kontonamen und dann auf die Schaltfläche `Mailbox Configuration`. |
        | 3 | Ein Fenster mit dem Titel `Protonmail Bridge Mailbox Configuration` sollte sich einblenden. Hier werden die IMAP- und SMTP-Einstellungen angezeigt, einschließlich eines Passworts, das später für die Konfiguration von Thunderbird benötigt wird. |

        </center>

        ### Thunderbird in macOS konfigurieren

        Startet nun Thunderbird, ruft den Menüeintrag `Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Einrichtungsassistenten:

        <center>

        | Einstellung | Beschreibung |
        | ------ | ------ |
        | Euer Name | Gebt den Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure Protonmail E-Mail-Adresse ein. |
        | Passwort | Kopiert das Passwort aus dem Fenster `Protonmail Bridge Mailbox Configuration` und fügt es hier ein (*Gebt nicht Euer Protonmail-Passwort ein*, das wird nicht funktionieren). |
        | Passwort speichern | Setzt ein Häkchen bei `Kennwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Manuell einrichten | Klickt auf die Schaltfläche `Manuell einrichten`, und fügt die IMAP- und SMTP-Einstellungen ein, die im Fenster `Protonmail Bridge Mailbox Configuration` angezeigt werden (wählt für die Authentifizierung `Normales Passwort`). |
        | Erneut testen | Klickt auf die Schaltfläche `Erneut testen`, um Eure Verbindungseinstellungen zu überprüfen. |
        | Erweiterte Einstellungen | Klickt auf die Schaltfläche `Erweiterte Einstellungen`. Ein neues Fenster wird aufgerufen. Klickt einfach auf die Schaltfläche `OK`, ändert keine Einstellungen in diesem Fenster. |
        | Sicherheitsausnahme hinzufügen | Klickt im nun erscheinenden Fenster auf die Schaltfläche `Sicherheitsausnahme hinzufügen'. Damit wird bestätigt, dass Euer Computer (`127.0.0.1`) die Bridge-Anwendung ausführen kann. Möglicherweise müsst Ihr eine zweite Sicherheitsausnahme bestätigen, sobald Ihr Eure erste E-Mail versenden möchtet. |

        </center>

    ??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo für Thunderbird (kostenpflichtiges Abonnement erforderlich)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Quelle: Protonmail. Anweisungen sollten in ähnlicher Weise für macOS oder Linux gelten.*

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für ElectronMail in Ubuntu Linux (kein kostenpflichtiges Abonnement erforderlich)"

        <center>

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        | ElectronMail herunterladen |Ladet das aktuelle [ElectronMail .deb-Paket](https://github.com/vladimiry/ElectronMail/releases) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `electron-mail-X-XX-X-linux-amd64.deb`. Für den Zweck dieses Tutorials nehmen wir an, dass die Datei in den Ordner `/home/gofoss/Downloads` heruntergeladen wurde. Passt den Dateipfad an Eure eigenen Einstellungen an. Öffnet nun das Terminal mit der Tastenkombination `STRG+ALT+T` oder klickt auf die Schaltfläche `Anwendungen` oben links und sucht nach `Terminal`. Führt schließlich die folgenden Befehle aus:<br><br> `cd /home/gofoss/Downloads` <br>`sudo dpkg -i electron-mail-X-XX-X-linux-amd64.deb`
        |
        | Master-Kennwort erstellen |Öffnet ElectronMail und gebt ein [sicheres, individuelles Master-Kennwort](https://gofoss.net/de/passwords/) ein, um Eure E-Mails zu schützen. |
        | Anmelden |Meldet Euch mit Eurem Protonmail-Konto an. Falls aktiviert, führt ebenfalls die Zwei-Faktor-Authentifizierung durch.  |
        | Domäne |Wählt eine Domäne aus der Liste. Es gibt sogar eine `Onion`-Option, mit der Ihr Tor verwenden könnt. Klickt anschließend auf `Schließen`. |

        </center>


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Thunderbird in Ubuntu Linux (kostenpflichtiges Abonnement erforderlich)"

        ### Thunderbird in Linux installieren

        Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt oben links auf die Schaltfläche `Aktivitäten` und sucht nach `Terminal`. Führt den folgenden Befehl aus, um Thunderbird zu installieren:

        ```bash
        sudo apt install thunderbird
        ```

        ### Protonmail Bridge in Linux installieren

        Thunderbird lässt sich gut mit Protonmail integrieren und stellt sicher, dass eingehende und versandte E-Mails verschlüsselt bleiben. Dies wird von der sogenannten Bridge-Anwendung gehandhabt, die nur für zahlende NutzerInnen verfügbar ist. Ladet hierzu [Protonmail Bridge für Linux](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `Protonmail-Bridge_X.X.X-X_amd64.deb`. Nehmen wir an, diese Datei wurde in den Ordner `/home/gofoss/Downloads` heruntergeladen. Öffnet das Terminal mit dem Tastaturkürzel `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal`. Führt dann die folgenden Befehle aus (vergesst nicht, den Dateinamen sowie den Pfad des Download-Ordners entsprechend anzupassen):

        ```bash
        sudo apt install gdebi
        cd /home/gofoss/Downloads
        sudo gdebi protonmail-bridge_X.X.X-X_amd64.deb
        ```

        ### Protonmail Bridge in Linux konfigurieren

        Öffnet die Bridge-Anwendung mit dem Terminalbefehl `protonmail-bridge`, oder klickt auf die Schaltfläche `Aktivitäten` oben links, und sucht nach `ProtonMail Bridge`. Folgt anschließend dem Einrichtungsassistenten:

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Meldet Euch mit Eurem Protonmail-Konto an. |
        | 2 | Klickt auf Euren Kontonamen und dann auf die Schaltfläche `Mailbox Configuration`. |
        | 3 | Ein Fenster mit dem Titel `Protonmail Bridge Mailbox Configuration` sollte sich einblenden. Hier werden die IMAP- und SMTP-Einstellungen angezeigt, einschließlich eines Passworts, das später für die Konfiguration von Thunderbird benötigt wird. |

        </center>

        ### Thunderbird in Linux konfigurieren

        Startet nun Thunderbird, ruft den Menüeintrag `Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Einrichtungsassistenten:

        <center>

        | Einstellung | Beschreibung |
        | ------ | ------ |
        | Euer Name | Gebt Euren Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure Protonmail E-Mail-Adresse ein. |
        | Passwort | Kopiert das Passwort aus dem Fenster `Protonmail Bridge Mailbox Configuration` und fügt es hier ein (*Gebt nicht Euer Protonmail-Passwort ein*, das wird nicht funktionieren). |
        | Passwort speichern | Setzt ein Häkchen bei `Kennwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Manuell einrichten | Klickt auf die Schaltfläche `Manuell einrichten`, und fügt die IMAP- und SMTP-Einstellungen ein, die im Fenster `Protonmail Bridge Mailbox Configuration` angezeigt werden (wählt für die Authentifizierung `Normales Passwort`). |
        | Erneut testen | Klickt auf die Schaltfläche `Erneut testen`, um Eure Verbindungseinstellungen zu überprüfen. |
        | Erweiterte Einstellungen | Klickt auf die Schaltfläche `Erweiterte Einstellungen`. Ein neues Fenster wird aufgerufen. Klickt einfach auf die Schaltfläche `OK`, ändert keine Einstellungen in diesem Fenster. |
        | Sicherheitsausnahme hinzufügen | Klickt im nun erscheinenden Fenster auf die Schaltfläche `Sicherheitsausnahme hinzufügen'. Damit wird bestätigt, dass Euer Computer (`127.0.0.1`) die Bridge-Anwendung ausführen kann. Möglicherweise müsst Ihr eine zweite Sicherheitsausnahme bestätigen, sobald Ihr Eure erste E-Mail versenden möchtet. |

        </center>


    ??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo für Thunderbird (kostenpflichtiges Abonnement erforderlich)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Quelle: Protonmail. Anweisungen sollten in ähnlicher Weise für macOS oder Linux gelten.*

        </center>


<br>


<center> <img src="../../assets/img/separator_tutanota.svg" alt="Tutanota" width="150px"></img> </center>

## Tutanota

[Tutanota](https://tutanota.com/de/) ist ein in Deutschland registrierter, Freemium-basierter und sicherer E-Mail-Dienst. Alle Daten sind durchgängig verschlüsselt. Tutanota verwendet seinen eigenen Verschlüsselungsstandard und unterstützt nicht PGP. Während die Apps von Tutanota [quelloffen](https://github.com/tutao/tutanota/) sind, gilt dies nicht für die Serverseite.

Zum Zeitpunkt der Abfassung dieses Textes umfasst das Basiskonto 1 GB kostenlosen Speicherplatz. Für 1 bis 6 EUR/Monat erhaltet Ihr Zugang zu weiteren BenutzerInnen und Speicherplatz sowie einer Fülle von Funktionen: benutzerdefinierte Domains, unbegrenzte Suchfunktion, mehrere Kalender, Posteingangsregeln, *Whitelabel*, Kalenderfreigabe usw. E-Mail-Importe und anonyme Zahlung werden derzeit nicht unterstützt.


### Tutanota Clients

Neben dem [Webmail-Zugang](https://mail.tutanota.com/?r=/login/) bietet Tutanota mobile Apps für Android und iOS an. Für Desktop-Umgebungen hat Tutanota einen eigenen Client entwickelt. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

         Ladet einfach die Tutanota-App aus dem [Google Play Store](https://play.google.com/store/apps/details?id=de.tutao.tutanota&hl=de&gl=DE) oder [Aurora Store](https://auroraoss.com/de/) herunter. Tutanota ist auch auf [F-Droid](https://f-droid.org/de/packages/de.tutao.tutanota/) verfügbar. Alternativ könnt Ihr die [Download-Seite](https://tutanota.com/de/#download)) oder die [Github-Paketquelle](https://github.com/tutao/tutanota/releases/) von Tutanota besuchen, um die `.apk`-Datei herunterzuladen und zu installieren. Die App [enthält 0 Tracker und benötigt 9 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/de.tutao.tutanota/latest/). Zum Vergleich: für Gmail sind es 1 Tracker und 55 Berechtigungen; für Outlook sind es 13 Tracker und 49 Berechtigungen; und für Hotmail sind es 4 Tracker und 31 Berechtigungen.

=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

         Ladet einfach die Tutanota aus dem [App Store](https://apps.apple.com/de/app/tutanota/id922429609) herunter.


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        Ladet einfach [das Installationsprogramm](https://mail.tutanota.com/desktop/tutanota-desktop-win.exe) herunter, klickt dann auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten.


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        Ladet einfach [das Installationsprogramm](https://mail.tutanota.com/desktop/tutanota-desktop-mac.dmg) herunter, das sich von selbst öffnen und ein neues Laufwerk mit der Tutanota-Anwendung einbinden sollte. Falls dies nicht der Fall sein sollte, öffnet die heruntergeladene Tutanota `.dmg`-Datei und verschiebt das erscheinende Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Tutanota-Symbol in das Andockmenü.


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        Ladet einfach [das Installationsprogramm](https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage) herunter. Der Dateiname sollte in etwa folgendermaßen lauten: `tutanota-desktop-linux.AppImage`.. Nehmen wir an, diese Datei wurde in den Ordner `/home/gofoss/Downloads` heruntergeladen. Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal`. Führt dann die folgenden Befehle aus (vergesst nicht, den Dateinamen sowie den Pfad zum Download-Ordner entsprechend anzupassen):

        ```bash
        cd /home/gofoss/Downloads
        chmod +x tutanota-desktop-linux.AppImage
        ```

    ??? tip "Tipps um Tutanota im Ubuntu-Dock anzuzeigen"

        Es ist nicht ganz einfach, aber Tutanotas Launcher kann dem Ubuntu-Anwendungsmenü hinzugefügt und an das Dock angeheftet werden. Öffnet hierzu das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal`. Führt den folgenden Befehl aus:

        ```bash
        sudo gedit /usr/share/applications/tutanota.desktop
        ```

        Fügt den folgenden Inhalt in die neu erstellte Datei ein. Stellt sicher, dass der `Exec`-Pfad auf den Ordner verweist, der die heruntergeladene AppImage-Datei enthält:

        ```
        #!/usr/bin/env xdg-open
        [Desktop Entry]
        Version=1.0
        Type=Application
        Terminal=false
        Exec=/home/gofoss/Downloads/tutanota-desktop-linux.AppImage
        Name=Tutanota
        ```

        Macht die Datei ausführbar:

        ```bash
        sudo chmod +x /usr/share/applications/tutanota.desktop
        ```

        Meldet Euch von der Ubuntu-Sitzung ab und dann gleich wieder an. Ihr solltet nun in der Lage sein, Tutanota aus dem Anwendungsmenü zu starten und es an das Dock zu heften.


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Email providers" width="150px"></img> </center>

## Weitere Email-Anbieter

=== "Disroot"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[disroot.org](https://disroot.org/de/services/email/) |
    |Preisangaben | Das Basiskonto ist kostenlos (1 GB Speicherplatz); zusätzlichen Speicherplatz gibt's für 0,15 EUR pro GB pro Monat. |
    |Funktionalitäten |Disroot bietet Online-Dienste an, die auf den Prinzipien von Freiheit, Datenschutz, Gemeinschaft und Dezentralisierung basieren. Der Anbieter befindet sich in den Niederlanden. Bitcoin und Faircoin werden akzeptiert. Vollständige Festplatten- und E-Mail-Verschlüsselung. Mobile App. |
    |Schwachstellen | Kann potenziell Benutzerdaten entschlüsseln, da E-Mails Berichten zufolge im Klartext gespeichert werden. |


=== "Mailbox"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[mailbox.org](https://mailbox.org/de/) |
    |Preisangaben | 1 EUR/Monat, 2 GB Speicherplatz. |
    |Funktionalitäten |Deutscher, quelloffener E-Mail-Anbieter, dessen Server in Berlin stehen. Bietet Sicherheitsfunktionen wie Verschlüsselung im Ruhezustand, PGP, DANE, SPF und DKIM an. Bietet des weiteren Zwei-Faktor-Authentifizierung, Volltextsuche, Kalender, Adressbücher, Aufgabenlisten, CalDAV- und CardDAV-Synchronisation an. |
    |Schwachstellen | Keine eigene mobile App, Drittanbieter-Clients können genutzt werden. |


=== "Posteo"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[posteo.de](https://posteo.de/de/) |
    |Preisangaben | 1 EUR/Monat, 2 GB Speicherplatz. |
    |Funktionalitäten |Deutscher, quelloffener E-Mail-Anbieter, eigen-finanziert, Verschlüsselung im Ruhezustand, [Zwei-Faktor-Authentifizierung](https://gofoss.net/de/passwords/), Kalender, Adressbücher, CalDAV- und CardDAV-Synchronisation. |
    |Schwachstellen | Kein Spam-Ordner, keine Test- oder Gratis-Version. |


=== "Kolab Now"

    | Info | Beschreibung |
    | :-----: | ----- |
    |Webseite |[kolabnow.com](https://kolabnow.com/) |
    |Preisangaben | 5 EUR/Monat, 2 GB Speicherplatz. |
    |Funktionalitäten |Schweizer, quelloffener E-Mail-Anbieter, Textsuche und Tagging, Filter, Adressbücher, Kalender, CalDAV- und CardDAV-Synchronisation. |
    |Schwachstellen | Keine durchgängige Verschlüsselung, keine Verschlüsselung im Ruhezustand. |


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="E-Mail Weiterleitung" width="150px"></img> </center>

## Übergangszeit

=== "E-Mails weiterleiten"

    Die Umstellung auf ein neues E-Mail-Konto kann einige Zeit in Anspruch nehmen, ähnlich wie beim [Wechsel zu einer neuen Messaging-App](https://gofoss.net/de/encrypted-messages/). Vermutlich solltet Ihr Eure alten Konten noch eine Weile beibehalten, um sicherzustellen, dass Ihr nichts verpasst. Leitet einfach alle eingehenden Nachrichten an das neue Konto weiter. Weitere Anweisungen zur Weiterleitung von E-Mails findet Ihr auf den Dokumentationsseiten von [Gmail](https://support.google.com/mail/answer/10957?hl=de), [Outlook](https://support.microsoft.com/de-de/office/erstellen-beantworten-oder-weiterleiten-von-e-mail-nachrichten-in-outlook-im-web-ecafbc06-e812-4b9e-a7af-5074a9c7abd0?redirectsourcepath=%252fen-us%252farticle%252fforward-email-from-office-365-to-another-email-account-1ed4ee1e-74f8-4f53-a174-86b748ff6a0e), [iCloud](https://www.idownloadblog.com/2019/01/28/set-up-icloud-email-forwarding/), [Yahoo](https://www.wikihow.com/Forward-Yahoo-Mail), usw.


=== "Abonnements aktualisieren"

    Nutzt die Übergangszeit, um Eure alten E-Mail-Konten auf laufende Abonnements zu überprüfen und diesen Eure neue E-Mail-Adresse zu übermitteln!


=== "Kontakte informieren"

    Vergesst nicht, die neue E-Mail-Adresse Euren privaten und beruflichen Kontakten, Eurer Bank, Eurer Versicherung, dem Finanzamt und so weiter mitzuteilen. Möglicherweise solltet Ihr auch eine automatische Antwortnachricht auf Eurem alten Konto einrichten, um Eure Kontakte über die Adressänderung zu informieren.


=== "Alte E-Mail-Konten schließen"

    Mit der Zeit werden immer weniger E-Mails in Euren alten Posteingängen landen. Irgendwann werden diese inaktiv. Das ist der richtige Zeitpunkt, um Eure alten E-Mail-Konten zu schließen.


<br>

<center> <img src="../../assets/img/separator_protonmail.svg" alt="Warning unprotected private key file" width="150px"></img> </center>

## PGP-Verschlüsselung für E-Mails

Niemand in Eurem Bekanntenkreis nutzt Protonmail oder Tutanota? Beide Dienstleister sind nicht so Euer Ding? Dann verschlüsselt Eure E-Mails doch einfach mit [OpenPGP](https://www.openpgp.org/)! Dieses Verschlüsselungsprotokoll ist frei, quelloffen und [unterstützt eine große Anzahl von Apps](https://www.openpgp.org/software/). Im Folgenden erklären wir, wie man OpenPGP auf dem Telefon oder Rechner einrichtet, einen PGP-Schlüsselgenerator verwendet, Sicherungskopien von PGP-Schlüsseln anlegt und E-Mails ver- bzw. entschlüsselt.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        ### K-9 Mail & OpenKeychain installieren

        <center>

        | Schritte | Beschreibung |
        | ----- | ----- |
        | K-9 Mail | K-9 Mail ist eine von mehreren [Android-E-Mail Apps](https://www.openpgp.org/software/), die OpenPGP unterstützen. Ladet die App einfach aus dem [Play Store](https://play.google.com/store/apps/details?id=com.fsck.k9&hl=de&gl=DE) oder von [F-Droid](https://f-droid.org/de/packages/com.fsck.k9/) herunter. |
        | OpenKeychain | OpenKeychain ist eine freie und quelloffene App, die sich in K-9 Mail integriert und eine durchgängige Verschlüsselung Eurer E-Mails ermöglicht. Ladet die App aus dem [Play Store](https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain&hl=de&gl=DE) oder von [F-Droid](https://f-droid.org/de/packages/org.sufficientlysecure.keychain/) herunter. |

        </center>


        ### PGP-Schlüssel mit OpenKeychain verwalten

        Um verschlüsselte E-Mails senden oder lesen zu können, benötigt Ihr ein individuelles Schlüsselpaar für Eure E-Mail Adresse:

        * **Öffentlicher Schlüssel**: Kontakte verschlüsseln an Euch gerichtete E-Mails mit Eurem öffentlichen Schlüssel. Ihr könnt Euren öffentlichen Schlüssel daher mit beliebig vielen Personen teilen.
        * **Privater Schlüssel**: eingehende verschlüsselte E-Mails werden mit Eurem privaten Schlüssel entziffert. Ihr solltet diesen daher unbedingt für Euch behalten, und unter keinerlei Umständen weitergeben!

        <center>

        | Schritte | Beschreibung |
        | ----- | ----- |
        | Vorhandene PGP-Schlüssel importieren |• Erzeugt kein neues Schlüsselpaar, wenn bereits eines für Eure E-Mail-Adresse existiert <br>• Startet OpenKeychain <br>• Tippt auf `Menü ‣ Meine Schlüssel verwalten ‣ Schlüssel aus Datei importieren` <br>• Gebt falls erforderlich den Sicherungscode und/oder das Schlüsselpasswort ein |
        | Neue PGP-Schlüssel erzeugen |• Erzeugt ein neues Schlüsselpaar, wenn noch keines für Eure E-Mail-Adresse existiert <br>• Startet OpenKeychain <br>• Tippt auf `Menü ‣ Meine Schlüssel verwalten ‣ Meinen Schlüssel erzeugen` <br>• Gebt Namen sowie E-Mail-Adresse an <br>• Tippt auf `Menü ‣ Schlüsselkonfiguration ändern` <br>• Gebt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an <br>• Deaktiviert `Auf Schlüsselservern veröffentlichen` <br>• Tippt auf `Schlüssel erzeugen` |
        | PGP-Schlüsselpaar sichern |• Solltet Ihr Eure Schlüssel verlieren, verliert Ihr den Zugriff auf all Eure E-Mails! <br>• Falls Ihr gerade ein neues Schlüsselpaar erzeugt habt, legt am besten gleich eine Sicherungskopie an <br>• Startet OpenKeychain <br>• Tippt auf Euren Schlüssel <br>• Wählt `Menü ‣ Schlüssel sichern` <br> • Gebt das Schlüsselkennwort ein <br>• [Speichert den 45-stelligen Sicherungscode](https://gofoss.net/de/passwords/), er ist zur Wiederherstellung des Schlüssels nötig! <br> • Speichert zudem die Sicherungsdatei auf Eurem Telefon oder besser noch an einem [sicheren Ort](https://gofoss.net/de/backups/) |
        | Öffentliche Schlüssel teilen |Bevor Ihr verschlüsselte E-Mails mit Euren Kontakten austauschen könnt, müsst Ihr Eure jeweiligen öffentlichen Schlüssel miteinander teilen. Nachfolgend einige gängige Methoden zum Teilen öffentlicher Schlüssel. <br><br> **Öffentliche Schlüssel an Eure Kontakte senden**: <br>• Startet OpenKeychain <br>• Tippt auf Euren Schlüssel <br>• Tippt auf das `Teilen`-Symbol & verschickt Euren Schlüssel <br>• Eure Kontakte können Euren Schlüssel nun mit ihrer bevorzugten App importieren <br><br> **Öffentliche Schlüssel auf einen Schlüsselserver hochladen**: <br>• Startet OpenKeychain <br>• Tippt auf Euren Schlüssel <br>• Tippt auf `Menü ‣ Erweitert ‣ Teilen ‣ Auf Schlüsselserver veröffentlichen` <br>• Eure Kontakte können nun den öffentlichen PGP-Schlüssel vom Schlüsselserver herunterladen <br>• Optional könnt Ihr den Download-Link sowie den Fingerabdruck des Schlüssels zu Eurer E-Mail-Signatur hinzufügen <br><br> **Öffentliche Schlüssel Eurer Kontakte importieren**: <br>• Bittet Eure Kontakte, Euch ihre öffentlichen Schlüssel per E-Mail, Messenger usw. zuzuschicken <br>• Startet OpenKeychain <br>• Tippt auf `Menü ‣ Meine Schlüssel verwalten ‣ Schlüssel aus Datei importieren` <br><br> **Öffentliche Schlüssel Eurer Kontakte von einem Schlüsselserver herunterladen**: <br>• Startet OpenKeychain <br>• Tippt auf `+ ‣ Schlüsselsuche` <br>• Sucht nach E-Mail-Adresse, Namen oder Fingerabdruck Eurer Kontakte <br>• Tippt auf `Import` |

        </center>


        ### E-Mails mit K-9 Mail verschlüsseln

        <center>

        | Schritte | Beschreibung |
        | ----- | ----- |
        | Konto & Verschlüsselung einrichten |• Startet die K-9 Mail App <br>• Legt Euer Konto an: gebt Eure E-Mail-Adresse sowie Passwort ein <br>• Konfiguriert die IMAP/POP3/SMTP-Einstellungen, falls diese nicht automatisch erkannt werden <br>• Wählt `Menü ‣ Einstellungen ‣ Konto ‣ Ende-zu-Ende Verschlüsselung ‣ OpenPGP-Unterstützung aktivieren` <br>• Wählt `Menü ‣ Einstellungen ‣ Konto ‣ Ende-zu-Ende Verschlüsselung ‣ Configure end-to-end key` <br>• Wählt Euren Schlüssel aus |
        | E-Mails verschlüsseln |• Öffnet die K-9 Mail App <br>• Tippt in der Posteingang-Ansicht auf das `Stift`-Symbol <br>• Verfasst Eure Nachricht & gebt die E-Mail-Adresse Eures Kontakts an <br>• Soweit Ihr zuvor den öffentlichen Schlüssel Eures Kontakts importiert habt, sollte am oberen Bildschirmrand ein `Schloss`-Symbol angezeigt werden <br>• Wenn Ihr auf das Symbol tippt, sollte es grün werden: die Verschlüsselung ist nun aktiv <br>• Tippt auf `Senden` <br><br>*Achtung*: der Betreff Eurer E-Mail wird unverschlüsselt übertragen! |
        | E-Mails entziffern |• K-9 Mail/OpenKeychain entziffert automatisch Nachrichten, die mit Eurem öffentlichen Schlüssel verschlüsselt wurden <br>• Dazu wird einmalig das Passwort Eures privaten Schlüssels abgefragt <br>• Ein `Schloss`-Symbol sollte über der entzifferten Nachricht erscheinen |

        </center>


        ### Probiert es aus!

        Edward ist ein von der Free Software Foundation entwickeltes Programm zum Testen der E-Mail-Verschlüsselung. Das funktioniert so:

        * Zuerst teilt Ihr Euren öffentlichen Schlüssel mit Edward
        * Edward verwendet Euren öffentlichen Schlüssel, um Euch eine verschlüsselte E-Mail zu senden
        * Nur Ihr seid in der Lage, diese E-Mail mit Eurem privaten Schlüssel zu entziffern
        * Anschließend ladet Ihr Euch Edwards öffentlichen Schlüssel herunter, um eine verschlüsselte E-Mail zu senden
        * Lediglich Edward kann Eure Nachricht mit seinem privaten Schlüssel entschlüsseln
        * Edward antwortet und bestätigt, dass Eure vorherige E-Mail sowohl verschlüsselt als auch signiert war

        <center>

        | Schritte | Beschreibung |
        | ----- | ----- |
        | Öffentlichen Schlüssel an Edward senden |• Öffnet OpenKeychain <br>• Tippt auf Euren Schlüssel <br>• Tippt auf das `Teilen`-Symbol <br>• Wählt K-9 Mail & verfasst eine E-Mail an `edward-de@fsf.org` <br>• Fügt einen Betreff und eine kurze Nachricht hinzu <br>• Tippt auf `Menü` & vergewissert Euch, dass die Verschlüsselung `deaktiviert` ist <br>• Tippt auf `Versenden` |
        | Edwards Nachricht entschlüsseln |• Öffnet K-9 Mail & wartet auf Edwards Antwort <br>• Diese sollte mit Eurem öffentlichen Schlüssel verschlüsselt sein <br>• Entziffert die E-Mail mit dem Passwort Eures privaten Schlüssels <br>• Ein oranges `Schloss`-Symbol sollte am oberen Nachrichtenrand erscheinen  |
        | Edwards öffentlichen Schlüssel importieren |• Tippt auf das orange `Schloss`-Symbol <br>• Tippt auf `Schlüssel suchen` <br>• Tippt auf `Import` <br>•  Das `Schloss`-Symbol sollte nun grün aufleuchten |
        | Edward eine verschlüsselte und signierte E-Mail schicken |• Tippt auf `Antworten` <br>• Verfasst eine kurze Antwort an `edward-de@fsf.org` <br>• Tippt auf `Menü` & vergewissert Euch, dass die Verschlüsselung `aktiviert` ist <br>• Tippt auf `Versenden` |
        | Edwards Antwort entschlüsseln |• Wartet auf Edwards Antwort <br>• Vergewissert Euch, dass das grüne `Schloss`-Symbol weiterhin angezeigt wird <br>• Edward sollte bestätigen, dass Eure Nachricht entschlüsselt & Eure Signatur überprüft werden konnte |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows, macOS & Linux (Ubuntu)"

        ### Thunderbird 78 (oder neuer) installieren

        <center>

        | OS | Beschreibung |
        | ----- | ----- |
        | Windows | Ruft die [Thunderbird-Download-Seite](https://www.thunderbird.net/de/) auf und klickt auf die Schaltfläche `Kostenloser Download`. Sobald das Installationsprogramm heruntergeladen ist, klickt auf die Schaltfläche `Ausführen` und folgt dem Installationsassistenten. |
        | macOS | Ruft die [Thunderbird-Download-Seite](https://www.thunderbird.net/de/) auf und klickt auf die Schaltfläche `Kostenloser Download`. Sobald das Installationsprogramm heruntergeladen ist, sollte es sich von selbst öffnen und ein neues Laufwerk einbinden, das die Thunderbird-Anwendung enthält. Sollte dies nicht der Fall sein, öffnet die heruntergeladene Thunderbird `dmg`-Datei und verschiebt das erscheinende Thunderbird-Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Thunderbird-Symbol in das Andockmenü. |
        | Linux (Ubuntu) | Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt oben links auf die Schaltfläche `Aktivitäten` und sucht nach `Terminal`. Führt den folgenden Befehl aus, um Thunderbird zu installieren: `sudo apt install thunderbird` |

        </center>


        ### Thunderbird einrichten

        Startet Thunderbird, ruft `Menü ‣ Neu ‣ Bestehendes E-Mail-Konto` auf und folgt dem Installationsassistenten:

        <center>

        | Einstellungen | Beschreibung |
        | ------ | ------ |
        | Name | Gebt den Namen ein, der für andere sichtbar sein soll. |
        | E-Mail-Adresse | Gebt Eure E-Mail-Adresse ein. |
        | Passwort | Gebt Eure E-Mail-Passwort ein. |
        | Passwort speichern | Setzt ein Häkchen bei `Passwort speichern`, um zu vermeiden, dass Ihr das Kennwort bei jedem Start von Thunderbird erneut eingeben müsst. |
        | Automatische vs. manuelle Einrichtung | Sobald Ihr Eure Anmeldedaten eingegeben habt, klickt auf die Schaltfläche `Weiter`. Thunderbird wird versuchen, die IMAP/POP3/SMTP-Einstellungen automatisch zu konfigurieren. Sollte dies nicht gelingen, konfiguriert diese Einstellungen manuell (Informationen hierzu findet Ihr bei Eurem E-Mail-Anbieter). |

        </center>


        ### PGP-Schlüssel mit Thunderbird verwalten

        Um verschlüsselte E-Mails senden oder lesen zu können, benötigt Ihr ein individuelles Schlüsselpaar für Eure E-Mail Adresse:

        * **Öffentlicher Schlüssel**: Kontakte verschlüsseln an Euch gerichtete E-Mails mit Eurem öffentlichen Schlüssel. Ihr könnt Euren öffentlichen Schlüssel daher mit beliebig vielen Personen teilen.
        * **Privater Schlüssel**: eingehende verschlüsselte E-Mails werden mit Eurem privaten Schlüssel entziffert. Ihr solltet diesen daher unbedingt für Euch behalten, und unter keinerlei Umständen weitergeben!

        <center>

        | Schritte | Beschreibung |
        | ----- | ----- |
        | Vorhandene PGP-Schlüssel importieren |**Schlüssel importieren**: <br>• Erzeugt kein neues Schlüsselpaar, wenn bereits eines für Eure E-Mail-Adresse existiert <br>• Startet Thunderbird <br>• Ruft `Menü ‣ Konten-Einstellungen ‣ Ende-zu-Ende-Verschlüsselung ‣ Schlüssel hinzufügen` auf <br>• Wählt `Bestehenden OpenPGP-Schlüssel importieren` & klickt auf `Weiter` <br>• Klickt auf `Datei für den Import auswählen` & ruft die Schlüsseldatei auf <br>• Gebt falls erforderlich den Sicherungscode und/oder das Schlüsselpasswort ein <br><br> **Verschlüsselte Sicherungskopie importieren**: <br>• Manche Sicherungskopien sind verschlüsselt (z.B. OpenKeychain) <br>• Diese können nicht direkt in Thunderbird importiert werden <br>• Öffnet das Terminal <br>• Entziffert die PGP-Datei:<br>`gpg --decrypt backup_YYYY-MM-DD.sec.pgp | gpg --import` <br>• Gebt falls erforderlich den Sicherungscode und/oder das Schlüsselpasswort ein <br>• Zeigt die Liste der Schlüssel an:<br>`gpg --list-keys` <br>• Schreibt  die `UID` des zu importierenden Schlüssels auf <br>• Speichert den Schlüssel im richtigen Format (ersetzt `UID` entsprechend): <br>`gpg --export-secret-keys UID > decrypted_backup_key.asc` <br>• Gebt falls erforderlich das Schlüsselpasswort ein <br>• Öffnet Thunderbird <br>• Ruft `Menü ‣ Konten-Einstellungen ‣ Ende-zu-Ende-Verschlüsselung ‣ Schlüssel hinzufügen` auf <br>• Wählt `Bestehenden OpenPGP-Schlüssel importieren` & klickt auf `Weiter` <br>• Klickt auf `Datei für den Import auswählen` & ruft die `.asc`-Schlüsseldatei auf <br>• Gebt falls erforderlich das Schlüsselpasswort ein |
        | Neue PGP-Schlüssel erzeugen |• Erzeugt ein neues Schlüsselpaar, wenn noch keines für Eure E-Mail-Adresse existiert <br>• Startet Thunderbird <br>• Ruft `Menü ‣ Konten-Einstellungen ‣ Ende-zu-Ende-Verschlüsselung ‣ Schlüssel hinzufügen` auf <br>• Wählt `Neuen OpenPGP-Schlüssel erzeugen` & klickt auf `Weiter` <br>• Wählt die entsprechende E-Mail-Adresse aus <br>• Legt ein Ablaufdatum zwischen 1-3 Jahren fest (kann jederzeit verlängert werden) <br>• Wählt `Schlüsseltyp: RSA` & `Schlüsselgröße: 4096`  <br>• Klickt auf `Schlüssel erzeugen ‣ Bestätigen` |
        | PGP-Schlüsselpaar sichern |• Solltet Ihr Eure Schlüssel verlieren, verliert Ihr den Zugriff auf all Eure E-Mails! <br>• Falls Ihr gerade ein neues Schlüsselpaar erzeugt habt, legt am besten gleich eine Sicherungskopie an <br><br>**Sicherungskopie des privaten Schlüssel anlegen:** <br>• Öffnet Thunderbird <br>• Ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf <br>• Klickt auf den gewünschten Schlüssel <br>• Wählt `Datei ‣ Sicherheitskopie für geheime(n) Schlüssel erstellen` <br>• Gebt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) für die Sicherheitskopie ein <br>• Hebt dieses Passwort gut auf, es ist zur Wiederherstellung des privaten Schlüssels nötig! <br>• Speichert zudem die `.asc`-Sicherungskopie des privaten Schlüssels auf Eurem Rechner oder besser noch an einem [sicheren Ort](https://gofoss.net/de/backups/) <br><br>**Sicherungskopie des öffentlichen Schlüssel anlegen:** <br>• Öffnet Thunderbird <br>• Ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf <br>• Rechts-klickt auf den gewünschten Schlüssel <br>• Wählt `Schlüssel in Datei exportieren` <br>• Speichert die `asc`-Sicherungskopie des öffentlichen Schlüssels auf Eurem Rechner oder besser noch an einem [sicheren Ort](https://gofoss.net/de/backups/)|
        | Öffentliche Schlüssel teilen |Bevor Ihr verschlüsselte E-Mails mit Euren Kontakten austauschen könnt, müsst Ihr Eure jeweiligen öffentlichen Schlüssel miteinander teilen. Nachfolgend einige gängige Methoden zum Teilen öffentlicher Schlüssel. <br><br> **Öffentliche Schlüssel an Eure Kontakte senden**: <br>• Öffnet Thunderbird <br>• Ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf <br>• Rechts-klickt auf den gewünschten Schlüssel <br>• Wählt `Öffentliche Schlüssel per E-Mail senden` <br>• Eure Kontakte können Euren Schlüssel nun mit ihrer bevorzugten App importieren <br><br> **Öffentliche Schlüssel auf einen Schlüsselserver hochladen**: <br>• Öffnet Thunderbird <br>• Ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf <br>• Rechts-klickt auf den gewünschten Schlüssel <br>• Wählt `Schlüssel in Datei exportieren` <br>• Ruft die [OpenPGP Key Repository](https://keys.openpgp.org/upload) Webseite auf <br>• Wählt die exportierte öffentliche Schlüsseldatei und klickt auf `Upload` <br>• Eure Kontakte können nun den öffentlichen PGP-Schlüssel vom Schlüsselserver herunterladen <br>• Optional könnt Ihr den Download-Link sowie den Fingerabdruck des Schlüssels zu Eurer E-Mail-Signatur hinzufügen <br><br> **Öffentliche Schlüssel Eurer Kontakte importieren**: <br>• Bittet Eure Kontakte, Euch ihre öffentlichen Schlüssel per E-Mail, Messenger usw. zuzuschicken <br>• Öffnet Thunderbird <br>• Falls Ihr einen öffentlichen Schlüssel per E-Mail erhalten habt, klickt auf die `OpenPGP`-Schaltfläche, um diesen zu importieren <br>• Falls Ihr den öffentlichen Schlüssel auf Euren Rechner heruntergeladen habt, ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf und klickt auf `Datei ‣ Öffentliche(n) Schlüssel aus Datei importieren` <br><br> **Öffentliche Schlüssel Eurer Kontakte von einem Schlüsselserver herunterladen**: <br>• Öffnet Thunderbird <br>• Ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf <br>• Klickt auf `Schlüsselserver` <br>• Sucht nach E-Mail-Adresse, Namen oder Fingerabdruck Eurer Kontakte <br>• Klickt auf `OK` |

        </center>


        ### E-Mails mit Thunderbird verschlüsseln

        <center>

        | Schritte | Beschreibung |
        | ----- | ----- |
        | Verschlüsselung einrichten |• Öffnet Thunderbird <br>• Ruft `Menü ‣ Konten-Einstellungen ‣ Ende-zu-Ende-Verschlüsselung` auf <br>• Stellt sicher, dass der richtige Schlüssel mit Eurer E-Mail-Adresse verknüpft ist |
        | E-Mails verschlüsseln |• Öffnet Thunderbird <br>• Klickt in der Posteingang-Ansicht auf die `Verfassen`-Schaltfläche <br>• Verfasst Eure Nachricht & gebt die E-Mail-Adresse Eures Kontakts an <br>• Klickt auf das Aufklappsymbol neben der `Sicherheit`-Schaltfläche <br>• Wählt `Nur mit Verschlüsselung senden` <br>• Ein `OpenPGP`-Symbol sollte in der Fußzeile des Fensters angezeigt werden <br>• Klickt auf die `Sicherheit`-Schaltfläche <br>• Soweit Ihr zuvor den öffentlichen Schlüssel Eures Kontakts importiert habt, sollte `OK` neben der E-Mail-Adresse Eures Kontakts erscheinen <br>• Klickt auf `Senden` |
        | E-Mails entziffern |• Thunderbird entziffert automatisch Nachrichten, die mit Eurem öffentlichen Schlüssel verschlüsselt wurden <br>• Dazu wird einmalig das Passwort Eures privaten Schlüssels abgefragt <br>• Ein `OpenPGP Schloss`-Symbol mit einem grünen Häkchen sollte am oberen Rand der entzifferten Nachricht erscheinen |

        </center>


        ### Probiert es aus!

        Edward ist ein von der Free Software Foundation entwickeltes Programm zum Testen der E-Mail-Verschlüsselung. Das funktioniert so:

        * Zuerst teilt Ihr Euren öffentlichen Schlüssel mit Edward
        * Edward verwendet Euren öffentlichen Schlüssel, um Euch eine verschlüsselte E-Mail zu senden
        * Nur Ihr seid in der Lage, diese E-Mail mit Eurem privaten Schlüssel zu entziffern
        * Anschließend ladet Ihr Euch Edwards öffentlichen Schlüssel herunter, um eine verschlüsselte E-Mail zu senden
        * Lediglich Edward kann Eure Nachricht mit seinem privaten Schlüssel entschlüsseln
        * Edward antwortet und bestätigt, dass Eure vorherige E-Mail sowohl verschlüsselt als auch signiert war

        <center>

        | Schritte | Description |
        | ----- | ----- |
        | Öffentlichen Schlüssel an Edward senden |• Öffnet Thunderbird <br>• Ruft `Menü ‣ Extras ‣ OpenPGP-Schlüssel verwalten` auf <br>• Rechts-klickt auf den gewünschten Schlüssel <br>• Wählt `Öffentliche Schlüssel per E-Mail senden` <br>• Richtet die E-Mail an `edward-de@fsf.org` <br>• Fügt einen Betreff und eine kurze Nachricht hinzu <br>• Klickt auf das Aufklappsymbol neben der `Sicherheit`-Schaltfläche <br>• Stellt sicher, dass `Nicht verschlüsseln` ausgewählt ist <br>• Klickt auf `Senden` |
        | Edwards Nachricht entschlüsseln |• Wartet auf Edwards Antwort <br>• Diese sollte mit Eurem öffentlichen Schlüssel verschlüsselt sein <br>• Stellt sicher, dass ein `OpenPGP Schloss`-Symbol mit einem grünen Häkchen am oberen Nachrichtenrand erscheint |
        | Edwards öffentlichen Schlüssel importieren |• Klickt in Edwards Antwort auf die E-Mail-Adresse `edward-de@fsf.org` <br>• Klickt auf `OpenPGP-Schlüssel suchen` <br>• Klickt auf `Akzeptiert (nicht verifiziert)` <br>•  Klickt auf `OK` |
        | Edward eine verschlüsselte und signierte E-Mail schicken |• Klickt auf `Antworten` <br>• Verfasst eine kurze Antwort an `edward-de@fsf.org` <br>• Klickt auf das Aufklappsymbol neben der `Sicherheit`-Schaltfläche <br>• Stellt sicher, dass `Nur mit Verschlüsselung senden` ausgewählt ist <br>• Klickt auf die `Sicherheit`-Schaltfläche <br>• `OK` sollte neben Edwards E-Mail-Adresse erscheinen <br>• Klickt auf `Senden` |
        | Edwards Antwort entschlüsseln |• Wartet auf Edwards Antwort <br>• Stellt sicher, dass das `OpenPGP Schloss`-Symbol mit dem grünen Häkchen weiterhin angezeigt wird <br>• Edward sollte bestätigen, dass Eure Nachricht entschlüsselt & Eure Signatur überprüft werden konnte |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Protonmail & Tutanota Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten findet Ihr in:

* [Protonmails Hilfestellung](https://protonmail.com/support/) oder [Thunderbirds Handbuch](https://support.mozilla.org/de/products/thunderbird). Ihr könnt auch gerne die [Protonmail Gemeinschaft](https://teddit.net/r/ProtonMail) oder die [Thunderbird Gemeinschaft](https://support.mozilla.org/de/questions/new/thunderbird) um Hilfe bitten.

* [Tutanotas Handbuch](https://tutanota.com/howto/#install-desktop) oder auf Nachfrage bei der [Tutanota Gemeinschaft](https://teddit.net/r/tutanota).

* [K9 Mails Handbuch](https://docs.k9mail.app).

<br>

<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/public_key.png" width="600px" alt="Sichere E-Mails"></img>
</div>

<br>
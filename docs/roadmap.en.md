---
template: main.html
title: A glimpse into the future
description: The gofoss.net Roadmap and Changelog. A glimpse into the future. A look in the mirror. New features. Upgrades. Content updates. Issue fixes.
---

# gofoss.net Roadmap and Changelog

<center> <img src="../../assets/img/roadmap.png" alt="Roadmap" width="500px"></img> </center>

We'll continue adding new content and features, creating localised versions and keeping everything tidy and up to date. There will be minor releases for corrections and maintenance, and more spaced out major releases to introduce new features and content.


##Roadmap

* Add Spanish version ([#50](https://gitlab.com/curlycrixus/gofoss/-/issues/50))
* Add news or announcement section ([#68](https://gitlab.com/curlycrixus/gofoss/-/issues/68))
* Upgrade [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/upgrading/)
* Upgrade [E-Charts](https://echarts.apache.org/en/index.html)
* Update and clarify guidelines for contributors & maintainers ([#9](https://gitlab.com/curlycrixus/gofoss/-/issues/9), [#10](https://gitlab.com/curlycrixus/gofoss/-/issues/10))
* Add Session to encrypted messengers ([#29](https://gitlab.com/curlycrixus/gofoss/-/issues/29))
* Add Delta Chat to encrypted messengers ([#67](https://gitlab.com/curlycrixus/gofoss/-/issues/67))
* Review iOS, macOS & Windows instructions for encrypted messengers ([#65](https://gitlab.com/curlycrixus/gofoss/-/issues/65))
* Add GPG to email section ([#62](https://gitlab.com/curlycrixus/gofoss/-/issues/62))
* Add Protonmail privacy notice to email section ([#66](https://gitlab.com/curlycrixus/gofoss/-/issues/66))
* Add Bitwarden to password section ([#30](https://gitlab.com/curlycrixus/gofoss/-/issues/30))
* Replace Yubikey in password section ([#63](https://gitlab.com/curlycrixus/gofoss/-/issues/63))
* Add RaivoOTP to password section ([#71](https://gitlab.com/curlycrixus/gofoss/-/issues/71))
* Add PinePhone to phone section ([#41](https://gitlab.com/curlycrixus/gofoss/-/issues/41))
* Add GrapheneOS to phone section ([#48](https://gitlab.com/curlycrixus/gofoss/-/issues/48))
* Add /e/ to phone section ([#53](https://gitlab.com/curlycrixus/gofoss/-/issues/53))
* Curate FOSS app list ([#64](https://gitlab.com/curlycrixus/gofoss/-/issues/64))
* Update Ubuntu instructions to LTS 22.04 ([#69](https://gitlab.com/curlycrixus/gofoss/-/issues/69))
* Add Linux Mint & Pop OS! to computer section ([#49](https://gitlab.com/curlycrixus/gofoss/-/issues/49))
* Update server hardening section ([#70](https://gitlab.com/curlycrixus/gofoss/-/issues/70))
* Update cron jobs screencasts ([#17](https://gitlab.com/curlycrixus/gofoss/-/issues/17))
* Add Nextcloud to cloud section ([#51](https://gitlab.com/curlycrixus/gofoss/-/issues/51))
* Improve readability & legibility ([#15](https://gitlab.com/curlycrixus/gofoss/-/issues/15))
* Fix broken links
* Remove mkdocs-static-i18n plugin ([#5](https://gitlab.com/curlycrixus/gofoss/-/issues/5))
* Fix multi-language search ([#4](https://gitlab.com/curlycrixus/gofoss/-/issues/4))


##Changelog

###January 2022

* Migrated to [https://gofoss.net](https://gofoss.net) ([#80](https://gitlab.com/curlycrixus/gofoss/-/issues/80))
* Published source code on Gitlab ([#57](https://gitlab.com/curlycrixus/gofoss/-/issues/57))
* Changed license to AGPLv3 ([#72](https://gitlab.com/curlycrixus/gofoss/-/issues/72))
* Overhauled layout: landing page, fonts, icons, admonitions, png files, etc. ([#7](https://gitlab.com/curlycrixus/gofoss/-/issues/7), [#14](https://gitlab.com/curlycrixus/gofoss/-/issues/14), [#79](https://gitlab.com/curlycrixus/gofoss/-/issues/79))
* Upgraded Material for MkDocs ([#34](https://gitlab.com/curlycrixus/gofoss/-/issues/34))
* Upgraded E-Charts ([#14](https://gitlab.com/curlycrixus/gofoss/-/issues/14))
* Migrated Reddit & Twitter links to Teddit & Nitter ([#13](https://gitlab.com/curlycrixus/gofoss/-/issues/13))
* Added dark mode ([#3](https://gitlab.com/curlycrixus/gofoss/-/issues/3))
* Added multi-language support & localised versions: English, French & German ([#2](https://gitlab.com/curlycrixus/gofoss/-/issues/2))
* Added README ([#36](https://gitlab.com/curlycrixus/gofoss/-/issues/36))
* Added contribution guidelines ([#8](https://gitlab.com/curlycrixus/gofoss/-/issues/8), [#73](https://gitlab.com/curlycrixus/gofoss/-/issues/73), [#74](https://gitlab.com/curlycrixus/gofoss/-/issues/74), [#77](https://gitlab.com/curlycrixus/gofoss/-/issues/77))
* Added Issue & Merge Request templates ([#45](https://gitlab.com/curlycrixus/gofoss/-/issues/45))
* Added browser overview to Firefox section ([#12](https://gitlab.com/curlycrixus/gofoss/-/issues/12), [#22](https://gitlab.com/curlycrixus/gofoss/-/issues/22))
* Added VPN providers: RiseupVPN, CalyxVPN ([#26](https://gitlab.com/curlycrixus/gofoss/-/issues/26))
* Added encrypted messengers: Element, Jami, Briar ([#24](https://gitlab.com/curlycrixus/gofoss/-/issues/24), [#27](https://gitlab.com/curlycrixus/gofoss/-/issues/27), [#33](https://gitlab.com/curlycrixus/gofoss/-/issues/33), [#42](https://gitlab.com/curlycrixus/gofoss/-/issues/42))
* Added FreeFileSync to backup section ([#21](https://gitlab.com/curlycrixus/gofoss/-/issues/21))
* Added cloud providers: Chatons, Digitalcourage, Picasoft ([#19](https://gitlab.com/curlycrixus/gofoss/-/issues/19), [#25](https://gitlab.com/curlycrixus/gofoss/-/issues/25), [#31](https://gitlab.com/curlycrixus/gofoss/-/issues/31))
* Added Fediverse: Mastodon, PeerTube, PixelFed, Friendica, Lemmy, Funkwhale, etc. ([#40](https://gitlab.com/curlycrixus/gofoss/-/issues/40))
* Added Electronmail to Protonmail ([#28](https://gitlab.com/curlycrixus/gofoss/-/issues/28))
* Added PhotoPrism to photo gallery ([#59](https://gitlab.com/curlycrixus/gofoss/-/issues/59))
* Added Xiaomi Mi A2 to CalyxOS ([#61](https://gitlab.com/curlycrixus/gofoss/-/issues/61))
* Updated permissions for server hardening ([#11](https://gitlab.com/curlycrixus/gofoss/-/issues/11))
* Updated secure domain section ([#18](https://gitlab.com/curlycrixus/gofoss/-/issues/18), [#44](https://gitlab.com/curlycrixus/gofoss/-/issues/44))
* Updated roadmap & changelog ([#58](https://gitlab.com/curlycrixus/gofoss/-/issues/58), [#38](https://gitlab.com/curlycrixus/gofoss/-/issues/38), [#75](https://gitlab.com/curlycrixus/gofoss/-/issues/75))
* Curated FOSS apps list ([#23](https://gitlab.com/curlycrixus/gofoss/-/issues/23), [#47](https://gitlab.com/curlycrixus/gofoss/-/issues/47))
* Curated Ubuntu apps list ([#39](https://gitlab.com/curlycrixus/gofoss/-/issues/39), [#43](https://gitlab.com/curlycrixus/gofoss/-/issues/43), [#46](https://gitlab.com/curlycrixus/gofoss/-/issues/46), [#56](https://gitlab.com/curlycrixus/gofoss/-/issues/56))
* Curated thanks section ([#37](https://gitlab.com/curlycrixus/gofoss/-/issues/37))
* Fixed cron jobs for dehydrated & ClamAV ([#16](https://gitlab.com/curlycrixus/gofoss/-/issues/16))
* Fixed Digitalcourage DNS ([#20](https://gitlab.com/curlycrixus/gofoss/-/issues/20))
* Fixed broken links & deprecated services ([#6](https://gitlab.com/curlycrixus/gofoss/-/issues/6))
* Fixed broken table headings ([#35](https://gitlab.com/curlycrixus/gofoss/-/issues/35))


###May 2021

* Migrated from Docsify to Material for MkDocs
* Added screencasts
* Added social media links ([#13](https://gitlab.com/curlycrixus/gofoss/-/issues/13))
* Overhauled layout: landing page, navigation, tables, colors, fonts, sections, etc.
* Upgraded E-Charts
* Re-designed 3-star system for beginners, intermediate, advanced users ([#54](https://gitlab.com/curlycrixus/gofoss/-/issues/54))
* Updated all website sections
* Added Arkenfox to user.js section
* Provided specifics on Protonmail's encryption ([#52](https://gitlab.com/curlycrixus/gofoss/-/issues/52))
* Curated Ubuntu apps list
* Added Secure domain section
* Curated thanks section
* Added Roadmap
* Removed links to CDNs
* Removed NordVPN ([#32](https://gitlab.com/curlycrixus/gofoss/-/issues/32))


###December 2020

* Overhauled visual identity
* Updated E-Charts
* Updated website content


###August 2020

* Initial release of [https://gofoss.today](https://gofoss2020.netlify.app/#/)


<br>
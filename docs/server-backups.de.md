---
template: main.html
title: So erstellt Ihr Sicherungskopien von Eurem Server
description: Erstellt Backups von Eurem Server. Server-Backup-Software. Was ist rsnapshot? Wie installiert man rsnapshot? Rsnapshot Ubuntu. Rsnapshot wiederherstellen.
---

# Plant Server-Sicherungen mit rsnapshot

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/server_2.png" alt="Server Backups" width="600px"></img>
</center>

[rsnapshot](https://rsnapshot.org/) ist ein Programm zur Planung von Sicherungskopien Eures [Ubuntu-Servers](https://gofoss.net/de/ubuntu-server/). Es ist frei und quelloffen, zuverlässig und ressourcenarm. Der geringe Speicherbedarf ist darauf zurückzuführen, dass rsnapshot inkrementelle Sicherungen durchführt: Nach einer ersten vollständigen Sicherungskopie werden nur noch geänderte Dateien gesichert.

<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="rsnapshot Installation" width="150px"></img> </center>

## Installation

Falls mal was schiefgehen sollte, ist es hilfreich den Server wieder in einen vorherigen Zustand zurückversetzen zu können. Installiert dazu rsnapshot mit dem Befehl `sudo apt install rsnapshot` auf Eurem Server.


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="rsnapshot Konfiguration" width="150px"></img> </center>

## Konfiguration

Nach der erfolgreichen Installation von rsnapshot sind eine Reihe von Einstellungen vorzunehmen: Speicherort der Sicherungsdateien, externe Abhängigkeiten, Sicherungsintervalle, Protokollierung, usw. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Sicherungsverzeichnis

    Öffnet die rsnapshot-Konfigurationsdatei:

    ```bash
    sudo vi /etc/rsnapshot.conf
    ```

    Fügt die folgenden Zeilen hinzu oder passt sie an, um den Speicherort für die Server-Sicherungskopien festzulegen. Ersetzt dabei die Zeichenfolge `$PATH` durch den Pfad zum eigentlichen Sicherungs-Verzeichnis. Dieses kann sich direkt auf der Festplatte des Servers, auf einem mit dem Server verbundenen externen Laufwerk oder an einem Remote-Speicherort befinden. Setzt auch das Attribut `no_create_root`, um zu verhindern, dass rsnapshot automatisch ein Sicherungs-Verzeichnis erstellt:

    ```bash
    snapshot_root	$PATH
    no_create_root	1
    ```

    ### Abhängigkeiten

    Kommentiert die folgenden Zeilen in der rsnapshot-Konfigurationsdatei `/etc/rsnapshot.conf` aus, um externe Abhängigkeiten anzugeben:

    ```bash
    cmd_rsync           /usr/bin/rsync
    cmd_du              /usr/bin/du
    cmd_rsnapshot_diff	/usr/bin/rsnapshot-diff
    ```
    Führt die folgenden Befehle aus, um sicherzustellen, dass rsnapshot auf die richtigen Binärdateien verweist. Überprüft, ob alle Pfade korrekt sind:

    ```bash
    whereis rsync
    whereis du
    whereis rsnapshot-diff
    ```

    ### Sicherungsintervalle

    Die folgenden Einstellungen in der rsnapshot-Konfigurationsdatei `/etc/rsnapshot.conf` legen fest, in welchen Abständen Sicherungskopien angelegt werden, und wie viele Sicherungskopien angelegt werden sollen. Dabei werden stündliche, tägliche, wöchentliche und monatliche Sicherungskopien unterstützt. Im folgenden Beispiel werden die 3 letzten täglichen Sicherungskopien, die 7 letzten wöchentlichen Sicherungskopien und die 2 letzten monatlichen Sicherungskopien aufbewahrt. Passt diese Einstellungen an Eure eigene Sicherungsstrategie an:

    ```bash
    retain	daily	3
    retain	weekly	6
    retain	monthly	2
    ```

    ### Protokollierung

    Gegebenenfalls könnt Ihr die Protokollierung zu Debugging-Zwecken aktivieren. Erstellt dazu eine Protokolldatei:

    ```bash
    sudo touch /var/log/rsnapshot.log
    ```

    Aktiviert die Protokollierung in der rsnapshots-Konfigurationsdatei `/etc/rsnapshot.conf`:

    ```bash
    verbose     5
    loglevel    5
    logfile     /var/log/rsnapshot.log
    ```


    ### Datei-Sicherung

    Ihr müsst festlegen, welche Serverdateien gesichert werden sollen. Gebt dazu in der Konfigurationsdatei ein oder mehrere Verzeichnisse an. Mit dem Attribut `exclude` könnt Ihr auch bestimmte Unterverzeichnisse ausschließen. Achtet darauf, für jedes Verzeichnis eine neue Zeile zu verwenden und am Ende jedes Verzeichnispfads ein abschließendes `/` hinzuzufügen. In folgenden Beispiel wird das gesamte Root-Dateisystem `/` gesichert, mit Ausnahme der Ordner `tmp`, `proc`, `mnt` und `sys`:

    ```bash
    backup	/	.	exclude=tmp,exclude=proc,exclude=mnt,exclude=sys
    ```

    ### Test

    Der folgende Befehl prüft die Syntax der Konfigurationsdatei (das Terminal sollte `Syntax OK` anzeigen):

    ```bash
    sudo rsnapshot configtest
    ```

    Testet anschließend Eure stündlichen, täglichen, wöchentlichen oder monatlichen Sicherungsstrategien:

    ```bash
    sudo rsnapshot -t hourly
    sudo rsnapshot -t daily
    sudo rsnapshot -t weekly
    sudo rsnapshot -t monthly
    ```

<br>

<center> <img src="../../assets/img/separator_alarm.svg" alt="rsnapshot geplante Sicherungskopien" width="150px"></img> </center>

## Geplante Sicherungskopien

Mit rsnapshot könnt Ihr automatisch Sicherungskopien in bestimmten Abständen planen. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Führt die kürzeste Sicherungsstrategie aus, um eine erste vollständige Sicherungskopie anzulegen. In unserem Beispiel ist das die tägliche Sicherung (passt dies entsprechend an):

    ```bash
    sudo rsnapshot daily
    ```

    Öffnet die `cron`-Konfigurationsdatei, um Sicherungskopien in bestimmten Abständen zu planen:

    ```bash
    sudo vi /etc/cron.d/rsnapshot
    ```

    Im folgenden Beispiel planen wir tägliche Sicherungskopien um 1:00 Uhr, wöchentliche Sicherungskopien jeden Montag um 18:30 Uhr und monatliche Sicherungskopien am zweiten Tag jedes Monats um 7:00 Uhr. Definiert je nach Bedarf Eure eigenen Zeitpläne, der [crontab-Guru](https://crontab.guru/) kann Euch dabei behilflich sein:

    ```bash
    #0 */4 * * *    root    /usr/bin/rsnapshot hourly
    0 1  * * *      root    /usr/bin/rsnapshot daily
    30 18 * * 1     root    /usr/bin/rsnapshot weekly
    0 7 2 * *       root    /usr/bin/rsnapshot monthly
    ```


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="rsnapshot Wiederherstellung" width="150px"></img> </center>

## Wiederherstellung

Sollte mal was schief gehen, könnt Ihr unter Umständen einen vorherigen Zustand des Servers wiederherstellen, indem Ihr eine der rsnapshot-Sicherungskopien wiederherstellt. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Der folgende Befehl listet alle vorhandenen Sicherungskopien auf. Ersetzt die Zeichenfolge `$PATH` durch den Pfad zum Sicherungsverzeichnis, den Ihr eingangs festgelegt habt:

    ```bash
    ls -l $PATH
    ```

    Je nach Sicherungsstrategie könnte die Ausgabe in etwa folgendermaßen aussehen:

    ```bash
    /path/to/rsnapshot/backup/folder/daily.0
    /path/to/rsnapshot/backup/folder/daily.1
    /path/to/rsnapshot/backup/folder/daily.2
    /path/to/rsnapshot/backup/folder/weekly.0
    /path/to/rsnapshot/backup/folder/weekly.1
    /path/to/rsnapshot/backup/folder/weekly.2
    /path/to/rsnapshot/backup/folder/weekly.3
    /path/to/rsnapshot/backup/folder/weekly.4
    /path/to/rsnapshot/backup/folder/weekly.5
    /path/to/rsnapshot/backup/folder/monthly.0
    /path/to/rsnapshot/backup/folder/monthly.1
    ```

    Mit dem folgenden Befehl wird der Server in einen vorherigen Zustand zurückversetzt:

    ```bash
    sudo rsync -avr $SOURCE_PATH $DESTINATION_PATH
    ```

    Versichert Euch, dass Ihr den richtigen `$SOURCE_PATH` (d.h. das Sicherungs-Verzeichnis, das wiederhergestellt werden soll) und `$DESTINATION_PATH` (d.h. das Zielverzeichnis auf dem Server, in das die Sicherungskopie wiederhergestellt werden soll) angebt. Der folgende Befehl stellt zum Beispiel die wöchentliche Sicherungskopie des gesamten Dateisystems von vor 3 Wochen im Server-Stammverzeichnis `/` wieder her:

    ```bash
    sudo rsync -avr /path/to/rsnapshot/backup/folder/weekly.3/ /
    ```

!!! warning "Vorsicht, Drachen!"

    Seid bei der Systemwiederherstellung äußerst vorsichtig. Daten können unwiderruflich überschrieben oder für immer verloren werden.


<br>
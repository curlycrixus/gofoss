---
template: main.html
title: Comment chiffrer sa communication
description: Qu'est ce qu'une couche de transport chiffrée ? Qu'est ce que le chiffrage de bout en bout ? Algorithmes de chiffrements ? Clé privée vs clé publique ?
---

# Parlez librement, chiffrez vos conversations

Dans ce chapitre, nous allons parler de [comment chiffrer vos messages et appels](https://gofoss.net/fr/encrypted-messages/). Nous allons également vous montrer comment progressivement [migrer vos courriels vers un fournisseur plus respectueux de la vie privée](https://gofoss.net/fr/encrypted-emails/).

<center>
<html>
<embed src="../../assets/echarts/email_stats.html" style="width: 100%; height:420px">
</html>
</center>


## Messageries chiffrées et courriels chiffrés

Peut-être avez vous déjà entendu dire que la plupart des fournisseurs de messageries et de courriels:

* [vendent vos conversations privées aux publicitaires](https://www.wsj.com/articles/yahoo-bucking-industry-scans-emails-for-data-to-sell-advertisers-1535466959/)
* [participent à des programmes de surveillance globale](https://www.nytimes.com/2016/10/06/technology/yahoo-email-tech-companies-government-investigations.html/)
* [souffrent de fuites de données sévères](https://fr.wikipedia.org/wiki/Yahoo!#Vol_de_donn%C3%A9es)
* [tracent le comportement de leurs utilisateurs et utilisatrices](https://thenextweb.com/news/google-tracks-nearly-all-your-online-and-some-of-your-offline-purchases-see-it-for-yourself-here)

Néanmoins, la vaste majorité des personnes continue de placer la sécurité de leur communication entre les mains de ces fournisseurs. La raison principale ? Changer de fournisseur est intimidant. Après tout, beaucoup argumentent que leur service actuel est "gratuit", connu par tout le monde et lié à tous leurs comptes et souscriptions. Heureusement, changer de fournisseur de courriel n'est pas aussi compliqué qu'il ne le semble.


## Chiffrement de la couche de transport vs chiffrement de bout en bout

De nos jours, plusieurs applications mobile sont chiffrées pour protéger vos communications:

Le **Chiffrement de la couche de transport (TLS)** s'assure que votre communication ne peut être déchiffrée lorsqu'elle transite sur internet ou entre des antennes relais. Cependant, des intermédiaires peuvent accéder à vos données lorsqu'elles transitent chez eux et les traiter. Cela inclut beaucoup de services de messageries, de réseaux sociaux, de moteurs de recherches, de banques et bien plus encore.

<div align="center">
<img src="../../assets/img/tls.png" alt="Chiffrement de la couche de transport" width="550px"></img>
</div>

Le **Chiffrement de bout en bout** protège vos communications pendant tout le trajet. Personne à l'exception de votre destinataire final ne peut la déchiffrer. Même pas les intermédiaires qui font transiter les données, comme les applications de [messagerie chiffrée](https://gofoss.net/fr/encrypted-messages/) ou bien votre [fournisseur de courriel](https://gofoss.net/fr/encrypted-emails/).

<div align="center">
<img src="../../assets/img/e2ee.png" alt="Chiffrement de bout en bout" width="550px"></img>
</div>

??? tip "Comment le chiffrement peut aider à éviter la surveillance en ligne ?"

    <center>

    <iframe src="https://archive.org/embed/tips-to-help-avoid-online-surveillance" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Publié par la [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


<br>
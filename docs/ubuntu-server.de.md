---
template: main.html
title: So setzt Ihr einen Ubuntu-Server auf
description: Selbst hosten. Ubuntu-Server. Linux, Apache, MySQL und PHP/Perl/Python. Warum solltet Ihr selbst hosten? Was ist LAMP? Was ist eine Fernanmeldung? Was ist SSH?
---

# Die Ubuntu-Server Anleitung

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/server_2.png" alt="Ubuntu-Server selbst hosten" width="700px"></img>
</div>

In diesem Kapitel wird erklärt, wie man einen Ubuntu-Server auf einem unbenutzten Rechner oder billiger Hardware zu Hause einrichtet. Dieser Server kann für eine Vielzahl von Diensten genutzt werden, z. B. als [Cloud-Speicher](https://gofoss.net/de/cloud-storage/), als [Fotogalerie](https://gofoss.net/de/photo-gallery/), zur [Kontakt-, Kalender- und Aufgabenverwaltung](https://gofoss.net/de/contacts-calendars-tasks/) oder zum Streamen von [Filmen und Fernsehserien](https://gofoss.net/de/media-streaming/).

In drei gesonderten Kapiteln werden weitere Schutzmaßnahmen für Euren Server vorgestellt:

* [Sicherheitsgrundlagen](https://gofoss.net/de/server-hardening-basics/) wie die Firewall, der Fernzugriff und Updates
* [fortgeschrittenere Techniken](https://gofoss.net/de/server-hardening-advanced/) wie Netzwerksicherheit, Antivirus, Berechtigungen usw.
* [Sicherungsanweisungen](https://gofoss.net/de/server-backups/), um regelmäßig Sicherungskopien Eurer Daten anzulegen


??? question "Sollte man seinen Server zu Hause hosten oder einen VPS mieten?"

    Beide Optionen haben Vor- und Nachteile. Es ist eine Abwägung zwischen Privatsphäre, Sicherheit und Benutzerfreundlichkeit. Obwohl auf dieser Webseite erklärt wird, wie Ihr Euren Server zu Hause hostet, könnt Ihr natürlich auch einen [Virtual Private Server (VPS)](https://en.wikipedia.org/wiki/Virtual_private_server) bei Anbietern wie [Hetzner](https://www.hetzner.com/cloud/), [Digital Ocean](https://www.digitalocean.com/pricing), [OVH](https://www.ovhcloud.com/de/vps/), [Contabo](https://contabo.com/de/), [Scaleway](https://www.scaleway.com/de/virtual-instances/development/) usw. anmieten.

    <center>

    | | Heim-Server | VPS |
    | ------ | :------: | :------: |
    | Höherer Datenschutz | <span style="color:green">✔</span> |  |
    | Geringere Einstiegskosten |  | <span style="color:green">✔</span> |
    | Geringere laufende Kosten | <span style="color:green">✔</span> | |
    | Einfacher aufzusetzen |  | <span style="color:green">✔</span> |
    | Einfacher zu warten | | <span style="color:green">✔</span> |
    | Geringere Sicherheitsrisiken | | <span style="color:green">✔</span> |
    | Geringeres Datenverlust-Risiko | | <span style="color:green">✔</span> |
    | Geringeres Ausfall-Risiko | | <span style="color:green">✔</span> |
    | Weniger Latenzprobleme | <span style="color:green">✔</span> |  |
    | Weniger Bandbreitenbeschränkungen | <span style="color:green">✔</span> | |

    </center>


??? question "Was sind die Mindestanforderungen an die Hardware?"

    Das hängt von der geplanten Nutzung ab. Wie viele NutzerInnen oder Besuche erwartet Ihr pro Tag? Plant Ihr, Videos zu streamen oder große Datenmengen zu speichern? Die folgenden Mindestspezifikationen sollten für eine Handvoll Dienste/BenutzerInnen ausreichend sein:

    * Ein oder mehrere 2-GHz-Prozessoren
    * 2 bis 4 GB Arbeitsspeicher
    * 10 bis 25 GB Festplattenspeicher (oder mehr für große Dateisammlungen)
    * Schnelle Internetverbindung

    [Hier eine Liste von Mini-PCs](https://forums.servethehome.com/index.php?threads/tiny-mini-micro-pc-experiences.30230/), die aufgrund ihres kleinen Formfaktors und ihres niedrigen Preises gut als Server geeignet sind.

??? question "Wie bearbeitet man Dateien innerhalb des Terminals?"

    Die Interaktion mit dem Server erfordert eine recht intensive Nutzung des Terminals. Konfigurationsdateien und Einstellungen müssen oft mit einem Texteditor angepasst werden. Der Editor [vi](https://de.wikipedia.org/wiki/Vi) wird häufig verwendet und verfügt über drei Modi:

    **Normaler Modus**: vi startet im normalen Modus, in dem Dokumente betrachtet werden können

    **Einfügemodus**: durch Drücken der Taste `i` schaltet vi in den Einfügemodus, der das Bearbeiten und Ändern von Dokumenten ermöglicht

    **Befehlsmodus**: durch Drücken der Doppelpunkttaste `:` wechselt vi in den Befehlsmodus, der es z.B. erlaubt, Änderungen zu speichern oder Text im Dokument zu finden

    Die folgenden Befehle sollten ausreichen, um alle Anweisungen auf dieser Website zu befolgen:

    <center>

    |Befehl |Beschreibung |
    | ----- | ----- |
    | `vi file.txt` | Öffnet das Dokument "file.txt" mit dem vi-Texteditor. |
    | `i` | Wechselt in den "Einfügemodus", um die Datei zu ändern. |
    | `ESC` | Verlasst den "Einfügemodus", sobald die Änderungen abgeschlossen sind. |
    | `:w` | Speichert alle Änderungen, die Ihr an der Datei vorgenommen habt, und lasst diese geöffnet. |
    | `:wq` oder `:x` | Speichert alle Änderungen an der Datei, und schließt vi. |
    | `:q!` | Beendet vi, ohne die Änderungen an der Datei zu speichern. |
    | `/findewort` | Sucht nach dem Wort "findewort" (ersetzt die Zeichenfolge durch das tatsächliche Wort, das Ihr sucht). |
    | `n` | Nachdem Ihr die Suche nach "findewort" gestartet habt, sucht das nächste vorkommende Wort "findewort" in der Datei. |

    </center>

    Es gibt zahlreiche Tutorials, um noch tiefer in die Welt von vi einzutauchen:

    * [Spickzettel der Santa Clara University](https://speicher.systemausfall.org/f/265c5adc9a7b4ddba7fd/)
    * [Spickzettel von WorldTimZone](https://speicher.systemausfall.org/f/2efedff727764690bda4/)
    * [Spickzettel von Daniel Gryniewicz](https://speicher.systemausfall.org/f/7be6da8705c9443288be/)


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Ubuntu-Server" width="150px"></img> </center>

## Ubuntu-Server installieren

Führt ein paar Vorabprüfungen durch und folgt den nachstehenden Installationsanweisungen.

??? tip "Ein paar Vorabprüfungen"

    <center>

    | Vorabprüfungen | Beschreibung |
    | ------ | ------ |
    | Ist mein Gerät Linux-kompatibel? |• [Testet Ubuntu](https://gofoss.net/de/ubuntu/#ubuntu-live-usb-virtualbox) mit einem Live-USB-Laufwerk oder mit VirtualBox <br>• Überprüft die [Kompatibilitätsdatenbank](https://ubuntu.com/certified) <br>• [Fragt](https://search.disroot.org/) im Internet nach <br>• Kauft einen Linux-kompatiblen Rechner, z.B. über [Linux Preloaded](https://linuxpreloaded.com/) oder [Ministry of Freedom](https://minifree.org/) |
    | Erfüllt mein Gerät die Mindestanforderungen? |• 2 GHz Dual-Core-Prozessor <br>• 4 GB Arbeitsspeicher (RAM) <br>• 25 GB freier Festplattenspeicher |
    | Ist mein Gerät eingestöpselt? | Falls Ihr Ubuntu Server 22.04 auf einem mobilen Gerät wie z.B. einem Laptop installiert, sollte dieses an die Stromversorgung angeschlossen sein. |
    | Ist das Installationsmedium zugänglich? | Prüft, ob Euer Rechner über ein DVD-Laufwerk oder einen freien USB-Anschluss verfügt. |
    | Hat das Gerät Zugriff aufs Internet? | Prüft, ob die Internetverbindung funktioniert. |
    | Sind Eure Daten gesichert? | [Sichert Eure Daten](https://gofoss.net/de/backups/), da ein (geringes, aber reales) Risiko des Datenverlustes während des Installationsprozesses besteht! |
    | Ist die aktuelle Ubuntu-Server-Version heruntergeladen? | Ladet die aktuellste [Ubuntu Server Long-Term-Support (LTS)-Version](https://ubuntu.com/download/server) herunter. Diese wird 5 Jahre lang unterstützt, einschließlich Sicherheits- und Wartungsupdates. Zum Zeitpunkt der Abfassung dieses Textes war die letzte Ubuntu-Server-LTS-Version 22.04. Nähere Informationen findet Ihr im [aktuellen Veröffentlichungszyklus](https://ubuntu.com/about/release-cycle). |
    | Ist ein bootfähiges Medium vorbereitet? | Verwendet einen [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) oder [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) Rechner, um die heruntergeladene `.iso`-Datei auf eine DVD zu brennen. Alternativ könnt Ihr auch einen [Windows](hhttps://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) oder [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) Rechner verwenden, um ein bootfähiges USB-Laufwerk zu erstellen. |

    </center>


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Booten |Legt die DVD oder das bootfähige USB-Laufwerk ein und startet Euren Rechner neu. Die meisten Rechner booten automatisch von der DVD oder dem USB-Laufwerk. Sollte dies nicht der Fall sein, drückt beim Hochfahren Eures Computers wiederholt auf die Tasten `F12`, `ESC`, `F2` oder `F10`. Dadurch solltet Ihr Zugang zum Boot-Menü Eures Rechners erhalten, in dem Ihr dann die DVD bzw. das USB-Laufwerk auswählen könnt. |
    |GRUB |Wählt den Menü-Eintrag `Try or Install Ubuntu Server`. |
    |Willkommen |Wählt eine Sprache. |
    |Installer-Aktualisierung verfügbar |Dieser Bildschirm erscheint, wenn Euer Rechner bereits mit dem Internet verbunden ist. Wählt den Menüeintrag `Ohne Aktualisierung fortfahren`. |
    |Tastatur-Konfiguration |Wählt ein Tastaturlayout. |
    |Art der Installation |Wählt die Standardinstallationsoption für Ubuntu Server. |
    |Netzwerkverbindungen |Stellt eine funktionierende Internetverbindung über Ethernet oder WiFi her. Dies ist für den Rest des Prozesses wichtig. Wählt `Info`, um mehr über ein bestimmtes Netzwerk zu erfahren. |
    |Proxy |Diese Einstellungen sind optional. Gebet die erforderlichen Informationen ein, wenn Ihr Netzwerk-Proxys einrichten müsst, andernfalls überspringt diesen Schritt einfach. |
    |Ubuntu-Archiv |Wählt die Ubuntu-Paketquelle. Die Standardoptionen sollten gut funktionieren. |
    |Speicherplatzkonfiguration |Legt fest, wie Eure Festplatte partitioniert werden soll. In diesem Tutorial werden wir die gesamte Festplatte verwenden <br>• Wählt `Diese Festplatte als LVM-Gruppe konfigurieren` <br>• Wählt `Die LVM-Gruppe mit LUKS verschlüsseln`, um Euren Server zu verschlüsseln <br>• Gebt ein [starkes, individuelles Kennwort](https://gofoss.net/de/passwords/) an und bestätigt Eure Eingabe <br><br> *Vorsicht*: Dieser Schritt löscht alle Daten und formatiert die Festplatte! Prüft sorgfältig alle Einstellungen! Vergewissert Euch, dass Ihr [eine Sicherungskopie Eurer Daten](https://gofoss.net/de/backups/) erstellt habt! |
    |Profileinrichtung |Richtet das Benutzer- und Serverprofil ein. Im Rahmen dieses Tutorials wählen wir die folgende Konfiguration (passt dies entsprechend an): <br>• Ihr Name (dies ist der Name des System-Root-Benutzers): `gofossroot` <br>• Name Ihres Servers (dies ist der Name Eures Servers): `gofossserver` <br>• Bitte Benutzername auswählen (selber Name wie der des System-Root-Benutzers): `gofossroot` <br>• Passwort: gebt ein [starkes, individuelles Passwort](https://gofoss.net/de/passwords/) ein und klickt auf `Erledigt` |
    |SSH-Einrichtung |Diese Einstellungen sind optional. Lasst diesen Abschnitt einfach leer und fahrt fort. |
    |Featured Server Snaps |Diese Einstellungen sind optional. Lasst diesen Abschnitt einfach leer und fahrt fort. |
    |Letzter Schritt |Wartet ab, bis die Installation abgeschlossen ist. Sobald Ubuntu Server installiert ist, entfernt das Installationsmedium und drückt `ENTER`, wenn Ihr dazu aufgefordert werdet. |

    </center>

??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7f4a0a65-7c82-4bd1-9f14-2ff5ced911c6" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="LAMP Stack" width="150px"></img> </center>

## LAMP Stack

Das Akronym [LAMP](https://de.wikipedia.org/wiki/LAMP_(Softwarepaket)) steht für [Linux](https://de.wikipedia.org/wiki/Ubuntu) (das Betriebssystem), [Apache](https://de.wikipedia.org/wiki/Apache_HTTP_Server) (die Server-Software), [MySQL](https://de.wikipedia.org/wiki/MySQL) (das Datenbankverwaltungssystem) und [PHP](https://de.wikipedia.org/wiki/PHP)/[Perl](https://de.wikipedia.org/wiki/Perl_(Programmiersprache))/[Python](https://de.wikipedia.org/wiki/Python_(Programmiersprache)) (die Programmiersprachen).

Meldet Euch nach dem Neustart am Ubuntu-Server an. Entschlüsselt die Serverpartition und gebt anschließend den Root-Benutzernamen und das Root-Passwort ein. Folgt schließlich den untenstehenden Anweisungen, um den LAMP-Stack zu installieren.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Aktualisiert die Systempakete und installiert den LAMP-Stack::

    ```bash
    sudo apt update
    sudo apt upgrade
    sudo apt install lamp-server^
    ```

    Achtet auf das nachgestellte Zirkumflex-Zeichen im Befehl `sudo apt install lamp-server^`. Es gibt an, dass es sich bei `lamp-server` um ein Meta-Paket handelt, das Apache, MySQL und PHP zusammen mit anderen Paketen und Abhängigkeiten installiert.

    Mit den folgenden Befehlen könnt Ihr Euch vergewissern, dass Apache erfolgreich installiert wurde (die neueste Versionsnummer sollte angezeigt werden), dass Apache nach jedem Neustart automatisch ausgeführt wird und dass der aktuelle Status von Apache `Active` ist:

    ```bash
    apachectl -V
    sudo systemctl enable apache2
    sudo systemctl status apache2
    ```

    Wiederholt daselbe für MySQL:

    ```bash
    mysql -V
    sudo systemctl enable mysql
    sudo systemctl status mysql
    ```

    Vergewissert Euch ebenfalls, dass PHP erfolgreich installiert wurde (die neueste Versionsnummer sollte angezeigt werden):

    ```bash
    php -v
    ```

    Und zum Schluss noch ein paar Aufräumarbeiten:

    ```bash
    sudo apt autoremove && sudo apt clean
    ```

??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/d9c1baa7-68ec-47c7-83dd-c338c5e55751" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ip.svg" alt="Ubuntu-Server statische IP" width="150px"></img> </center>

## Statische IP-Adresse

Es wird empfohlen, dem Server eine statische IP-Adresse zuzuweisen. Im Rahmen dieses Tutorial verwenden wir die statische IP-Adresse `192.168.1.100`. Natürlich könnt Ihr jegliche andere geeignete Adresse auswählen. Achtet lediglich darauf, die Befehle in den nachfolgenden Anweisungen entsprechend anzupassen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Netzwerkschnittstelle

    Zunächst solltet Ihr den Namen der Netzwerkchnittstelle des Servers herausfinden:

    ```bash
    ip link show
    ```

    Es sollte eine Liste aller Netzwerkschnittstellen angezeigt werden:

    * Der erste Eintrag ist höchstwahrscheinlich `lo`, also die sogenannte Loopback-Schnittstelle
    * Die Ethernet-Schnittstelle sollte mit `enpXsY` gekennzeichnet sein, also z.B. `enp0s1` oder `enp1s3`
    * Die WiFi-Schnittstelle sollte mit `wlpXsY` gekennzeichnet sein, also z.B. `wlp0s1` oder `wlp2s3`

    Für die Zwecke dieses Tutorials nehmen wir an, dass die Netzwerkschnittstelle des Servers mit `enp0s3` bezeichnet ist (passt dies gemäß Eurer eigenen Konfiguration entsprechend an).

    ### Standard-Gateway

    Ermittelt als nächstes den Namen des Standard-Gateways:

    ```bash
    ip route show
    ```

    Das Terminal sollte eine Zeile mit der IP-Adresse des Standard-Gateways anzeigen. Diese sollte in etwa so aussehen: `default via 192.168.1.1 dev enp0s3 proto dhcp`. Für die Zwecke dieses Tutorials nehmen wir an, dass die Adresse des Standard-Gateways `192.168.1.1` lautet (passt dies gemäß Eurer eigenen Konfiguration entsprechend an).

    ### Sicherungskopie

    Legt eine Sicherungskopie der Netzwerkkonfigurationsdatei an. Sollte etwas schief gehen, könnt Ihr die ursprünglichen Einstellungen wiederherstellen:

    ```bash
    sudo cp --archive /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    ### Netzwerkkonfiguration

    Öffnet die Netzwerkkonfigurationsdatei:

    ```bash
    sudo vi /etc/netplan/00-installer-config.yaml
    ```

    Der Inhalt der Datei sollte etwa so aussehen:

    ```bash
    network:
      ethernets:
        enp0s3:
          dhcp4: true
      version: 2
    ```

    Der Eintrag `dhcp4: true` besagt, dass dem Server eine dynamische IP-Adresse zugewiesen ist. Ändert den Inhalt der Datei:

    * gebt die richtige Netzwerkschnittstelle an: in unserem Beispiel `enp0s3` (passt dies entsprechend an)
    * weist dem Server eine statische IP-Adresse zu: in unserem Beispiel `192.168.1.100` (passt dies entsprechend an)
    * gebt das Standardgateway an: in unserem Beispiel `192.168.1.1` (passt dies entsprechend an)
    * wählt einen DNS-Anbieter aus: in unserem Beispiel ist das [UncensoredDNS](https://blog.uncensoreddns.org/) (gebt den DNS-Anbieter Eurer Wahl an)

    Achtet auf die korrekte Einrückung der einzelnen Zeilen der Netzwerkkonfigurationsdatei. Und stellt unbedingt sicher, dass Ihr alle Einstellungen an Eure eigene Konfiguration anpasst. Zum Beispiel:

    * könnte Eure Ethernet-Schnittstelle `enp0s1` statt `enp0s3` heißen
    * oder Ihr könntet eine eine WiFi-Schnittstelle verwenden, beispielsweise `wlp0s1`: in diesem Fall müsst Ihr die Zeile `ethernets:` durch `wifis` und die Zeile `enp0s3` durch `wlp0s1` ersetzen
    * Euer Standard-Gateway könnte `192.168.0.1` statt `192.168.1.1` lauten
    * Ihr könnt auch eine andere statische IP-Adresse oder einen anderen DNS-Anbieter wählen

    ```bash
    network:
      ethernets:
        enp0s3:
          dhcp4: false
          addresses: [192.168.1.100/24]
          routes:
            - to: default
              via: 192.168.1.1
          nameservers:
            addresses: [89.233.43.71, 91.239.100.100]
      version: 2
    ```

    Speichert und schließt die Datei, indem Ihr erst auf `ESC` drückt und dann `:wq` eintippt. Übernehmt schließlich alle Änderungen und überprüft diese:

    ```bash
    sudo netplan apply
    ip add
    ```


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ef1b78f7-bab2-4ace-8de0-ddb1c9715338" frameborder="0" allowfullscreen></iframe>
    </center>

??? question "Warum braucht der Server eine statische IP-Adresse?"

    Standardmäßig weist das so genannte [DHCP-Protokoll](https://de.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) jedem Gerät in Eurem Heimnetzwerk, einschließlich Eures Servers, eine eindeutige IP-Adresse zu. Nach jedem Neustart oder Verlust der Netzwerkverbindung besteht die Möglichkeit, dass Eurem Server eine neue IP-Adresse zugewiesen wird. Das wiederum würde eine Neukonfiguration verschiedener Einstellungen erfordern. Um dies zu vermeiden, wird Eurem Server eine statische IP-Adresse zugewiesen.

    Welche statische IP-Adresse Ihr dem Server zuweisen könnt, hängt von der Netzwerkkonfiguration ab. Überprüft die Einstellungen Eurer anderen Geräte oder Eures Routers, um herauszufinden, welche IP-Adressen in Eurem Heimnetzwerk verwendet werden. Häufig verwendete IP-Adressbereiche sind `192.168.0.xx` oder `192.168.1.xx`.

    Es gibt zwei Möglichkeiten, dem Server eine statische IP-Adresse zuzuweisen:

    * **Netzwerkkonfiguration**: Ändert direkt die Netzwerkkonfiguration auf dem Server, wie oben dargestellt.

    * **DHCP-Reservierung**: Konfiguriert alternativ eine sogenannte DHCP-Reservierung auf dem Router, die dem Server immer die gleiche IP-Adresse zuweist. Weitere Informationen hierzu findet Ihr im Handbuch Eures Routers.


??? question "Gibt es weitere datenschutzfreundliche DNS-Anbieter?"

    <center>

    | DNS-Anbieter | Land | DNS #1 | DNS #2 |Datenschutz |
    | ------ | ------ | ------  |------ |------ |
    | [Digitalcourage](https://digitalcourage.de/support/zensurfreier-dns-server) | Deutschland | 5.9.164.112 | -- |[Datenschutz](https://digitalcourage.de/datenschutz-bei-digitalcourage) |
    | [Dismail](https://dismail.de/info.html#dns/) | Deutschland |80.241.218.68 | 159.69.114.157 |[Datenschutz](https://dismail.de/datenschutz.html#dns) |
    | [DNS Watch](https://dns.watch/) | Deutschland | 84.200.69.80 | 84.200.70.40 | -- |
    | [FDN](https://www.fdn.fr/actions/dns/) | Frankreich | 80.67.169.12 | 80.67.169.40 | -- |
    | [OpenNIC](https://servers.opennic.org/) | Verschiedentlich | Verschiedentlich | Verschiedentlich | Verschiedentlich |

    </center>



<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Ubuntu-Server Fernanmeldung" width="150px"></img> </center>

## Fernanmeldung

Ihr könnt von einem anderen *Client*-Rechner aus eine Fernverbindung zum *Server* herstellen. Auf diese Weise kann der Server *kopflos* betrieben werden — an den Server angeschlossene Peripheriegeräte wie Bildschirm, Tastatur oder Maus werden dadurch überflüssig. Die Fernverbindung ist durch [Secure Shell (SSH)](https://de.wikipedia.org/wiki/Secure_Shell) geschützt, welches eine sichere Authentifizierung und Verschlüsselung bietet. Im Folgenden eine Kurzbeschreibung, wie das ganze funktioniert, eine ausführlichere Anleitung findet Ihr weiter unten.

<div align="center">
<img src="../../assets/img/ssh_connection.png" alt="Ubuntu Server SSH" width="500px"></img>
</div>

1. Der *Client* initiiert die Verbindung zum *Server*.
2. Der *Server* sendet seinen öffentlichen Schlüssel an den *Client*.
3. Der öffentliche Schlüssel des *Servers* wird in der `known hosts`-Datei des *Clients* gespeichert.
4. Der *Client* und der *Server* einigen sich auf die für ihre Kommunikation zu verwendende Verschlüsselung und stellen die Verbindung her.


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Server-Einrichtung

    Beginnen wir mit dem *Server*. Führt folgende Befehle aus, um OpenSSH zu installieren, automatisch nach jedem Neustart auszuführen und den aktuellen Status zu überprüfen (dieser sollte `Active` sein):

    ```bash
    sudo apt install openssh-server
    sudo systemctl enable ssh
    sudo systemctl status ssh
    ```

    Legt einen Administrator-Benutzer an, der über die Berechtigung verfügt, eine Fernverbindung zum Server herzustellen. Für die Zwecke dieses Tutorials nennen wir diesen Administrator `gofossadmin`. Ihr könnt selbstverständlich jeglichen anderen Namen verwenden, passt in diesem Fall allerdings die Befehle entsprechend an. Gebt auf Aufforderung ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) ein:

    ```bash
    sudo adduser gofossadmin
    sudo usermod -a -G sudo gofossadmin
    ```

    Wechselt zu dem neuen Administrator-Konto und testet seine `sudo`-Befugnisse, indem Ihr das System aktualisiert:

    ```bash
    su - gofossadmin
    sudo apt update
    ```

    ### Client-Einrichtung

    Lasst uns nun den *Client* einrichten. In dieser Anleitung gehen wir davon aus, dass auf dem Client-Rechner eine GNU/Linux-Distribution läuft, z. B. Ubuntu. Wenn Euer Client unter Windows oder macOS läuft, verwendet die verfügbaren Systemwerkzeuge oder installiert einen SSH-Client wie [PuTTY](https://www.putty.org/).

    Öffnet mit der Tastenkombination `STRG + ALT + T` ein Terminal auf dem Linux-Client, oder klickt auf `Aktivitäten` in der oberen Menüleiste und sucht nach `Terminal`. Legt nun denselben Administrator-Benutzer `gofossadmin` wie auf dem Server an (passt den Namen entsprechend an). Gebt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) ein, wenn Ihr dazu aufgefordert werdet:

    ```bash
    sudo adduser gofossadmin
    ```

    Meldet Euch mit dem neuen Administrator-Konto an, erstellt ein verstecktes Verzeichnis, in dem die SSL-Schlüssel gespeichert werden, und verseht das Verzeichnis mit den richtigen Berechtigungen:

    ```bash
    su - gofossadmin
    mkdir /home/gofossadmin/.ssh
    chmod 755 /home/gofossadmin/.ssh
    ```

    Erzeugt ein öffentliches/privates Schlüsselpaar, das die Fernanmeldung beim Server sichert. Führt dazu die folgenden Befehle aus, folgt den Anweisungen auf dem Bildschirm und gebt bei Aufforderung ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) ein:

    ```bash
    cd /home/gofossadmin/.ssh
    ssh-keygen -t RSA -b 4096
    ```

    Die Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    Generating public/private RSA key pair.
    Enter file in which to save the key (/home/gofossadmin/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase): *******
    Enter same passphrase again: ********
    Your identification has been saved in /home/gofossadmin/.ssh/id_rsa.
    Your public key has been saved in /home/gofossadmin/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:erdBxn9i/ORLIJ4596DvfUIIPOyfFnSMQ4SieLIbSuxI gofossadmin@gofossclient
    The key's randomart image is:
    +---[RSA 4096]----+
    |   .+.+=     o   |
    |       ..oo.+ o  |
    | .S+..     %..   |
    |         o+o. o  |
    |        So*o o = |
    |   ..oo.+     A +|
    | ..+ooo       .oo|
    |  .o...+  .E..   |
    |         .. o==.%|
    +----[SHA256]-----+
    ```

    Übertragt anschließend den öffentlichen Schlüssel vom Client zum Server. Stellt dabei sicher, dass Ihr den Administrator-Namen und die IP-Adresse entsprechend Eurer eigenen Einrichtung anpasst:

    ```bash
    ssh-copy-id gofossadmin@192.168.1.100
    ```

    Das war's! Von nun an könnt Ihr Euch vom Client-Rechner aus per Fernzugriff beim Server anmelden. Um Euch mit dem Server zu verbinden reicht es, ein Terminal zu öffnen, zum Administrator-Konto zu wechseln und Euer Passwort anzugeben. Vergesst nicht, den Administrator-Namen und die IP-Adresse nach Bedarf anzupassen:

    ```bash
    su - gofossadmin
    ssh gofossadmin@192.168.1.100
    ```

??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/f201ed55-5b1c-49b8-9698-73940c05aab2" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu-Server Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr in [Ubuntus Dokumentation](https://help.ubuntu.com/), [Ubuntus Tutorials](https://ubuntu.com/tutorials) oder [Ubuntus Wiki](https://wiki.ubuntu.com/). Ihr könnt auch gerne die [beginnerfreundliche Ubuntu-Gemeinschaft](https://ubuntuusers.de/) um Unterstützung bitten.

<br>

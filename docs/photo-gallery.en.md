---
template: main.html
title: Piwigo | Self-hosted Digital Media Solutions
description: How to organise photos in a privacy-friendly manner? Use an alternative to Google Photos or iCloud photo library, such as Piwigo or Photoprism.
---

# Piwigo, a Self-Hosted Google Photos Alternative

!!! level "Last updated: May 2022. For advanced users. Solid tech skills required."

<center>
<img align="center" src="../../assets/img/piwigo.png" alt="Digital media management" width="550px"></img>
</center>

<br>

Looking for the best way to organise photos in a privacy-friendly manner? [Piwigo](https://piwigo.org/) is a self-hosted photo library for your [home server](https://gofoss.net/ubuntu-server/). Access, organise and share your cloud photo storage from anywhere. Piwigo supports nested albums, batch editing, multiple users, tags, plugins, themes, and more.


??? question "Looking for a Piwigo alternative?"

    There are plenty of alternatives, choose a software which responds to your needs:

    <center>

    | |[Piwigo](https://piwigo.org/) |[Photoprism](https://photoprism.app/) |[Photoview](https://photoview.github.io/) |[Pigallery2](http://bpatrik.github.io/pigallery2/) |[Lychee](https://lycheeorg.github.io/) |
    | ------ | :------: | :------: | :------: | :------: | :------: |
    |Creation date |2002 |2018 |2020 |2017 |2018 |
    |Language |PHP |Go |Go |TypeScript |PHP |
    |Minimum requirements |Apache or nginx, PHP |2 cores, 4 GB RAM |-- |-- |Apache or nginx |
    |Database |MySQL, MariaDB |MySQL, MariaDB SQLite |MySQL, Postgres, SQLite |SQL or no database |MySQL, PostgreSQL or SQLite |
    |Installation |Direct install |Docker |Docker, Direct install |Docker, Direct install |Docker, Direct install |
    |User interface |Basic |Modern |Modern |Basic |Modern |
    |Speed |Fast |Fast |Fast |Fast |Fast |
    |Albums, nested albums |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Tags, labels |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Auto tags |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Face recognition |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> <br>(automatic) |<span style="color:green">✔</span> <br>(manual) |<span style="color:red">✗</span> |
    |Bulk edit |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |
    |Duplicate detection |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |? |
    |Search |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Calendar/Timeline |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Places/Map |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |
    |Multi users |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Permissions |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Sharing |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Comments |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Delete |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:green">✔</span> |
    |RAW file support |<span style="color:red">✗</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Video support |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Responsive web interface |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |
    |Native mobile apps |Android & iOS, no auto-upload |Android & iOS, experimental auto-upload |<span style="color:red">✗</span> |<span style="color:red">✗</span> |<span style="color:red">✗</span> |
    |Folder structure support^1^ |<span style="color:green">✔</span> |<span style="color:green">✔</span><br> (indexing) |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:red">✗</span> |
    |WebDAV, FTP support^2^ |<span style="color:green">✔</span> |<span style="color:green">✔</span> |<span style="color:green">✔</span> |? |<span style="color:red">✗</span> |

    </center>

    1. *Software points to an existing folder structure containing photos/videos, without the need to alter or replicate them. No need for a separate copy of the files. The photos/videos remain untouched if the software is uninstalled.*
    2. *WebDAV / FTP clients can directly connect to folders containing photos/videos and mount them as a network drive. This way, photos/videos can be added, removed & modified on client devices.*


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Picasa alternative" width="150px"></img> </center>

## Database preparation

Piwigo is a photo management software which can be deployed with MySQL or MariaDB. In this tutorial, we'll generate a MySQL database required by Piwigo's server components. Read on below for detailed instructions.

??? tip "Show me the step-by-step guide"

    Log into the server and access MySQL as root:

    ```bash
    sudo mysql -u root -p
    ```

    Create the MySQL user `piwigoadmin` (adjust accordingly). Make sure to replace the string `StrongPassword` with a [strong, unique password](https://gofoss.net/passwords/):

    ```bash
    CREATE USER 'piwigoadmin'@localhost IDENTIFIED BY 'StrongPassword';
    ```

    Next, generate the database required by Piwigo and grant correct permissions:

    ```bash
    CREATE DATABASE piwigo;
    GRANT ALL ON piwigo.* TO 'piwigoadmin'@'localhost';
    FLUSH PRIVILEGES;
    EXIT;
    ```

    Log back into MySQL as the user `piwigoadmin` (adjust accordingly):

    ```bash
    sudo mysql -u piwigoadmin -p
    ```

    Make sure the `piwigo` database has been created correctly:

    ```bash
    SHOW DATABASES;
    ```

    The output should look similar to this:

    ```bash
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | piwigo             |
    +--------------------+
    2 rows in set (0.01 sec)
    ```

    Exit MySQL:

    ```bash
    EXIT;
    ```

??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/fc962cb5-0be4-42bf-bd7c-456dbc095a27" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Piwigo installation" width="150px"></img> </center>

## How to install Piwigo

Follow the instructions below to resolve all [dependencies](https://piwigo.org/guides/install/requirements) and install Piwigo on your server.

??? tip "Show me the step-by-step guide"

    ### Prerequisites

    Piwigo requires PHP to run. [One of the previous chapters on server hardening](https://gofoss.net/server-hardening-basics/) covered how to deploy and secure PHP. This should fulfill Piwigo's main software dependencies, such as `php8.1-{common,mysql,curl,xmlrpc,gd,mbstring,xml,intl,cli,zip}`.

    Install some additional PHP modules:

    ```bash
    sudo apt install php8.1-{cgi,soap,ldap,readline,imap,tidy}
    sudo apt install libapache2-mod-php8.1
    ```

    ### Installation

    [Check the latest release of Piwigo's self-hosted package](https://piwigo.org/get-piwigo). At the time of writing, it was 12.2.0. Download and decompress the package with the following commands:

    ```bash
    wget http://piwigo.org/download/dlcounter.php?code=latest -O /tmp/piwigo.zip
    sudo unzip /tmp/piwigo.zip 'piwigo/*' -d /var/www
    ```

    Set and verify the right permissions:

    ```bash
    sudo chown -R www-data:www-data /var/www/piwigo/
    sudo chmod -R 755 /var/www/piwigo/
    sudo ls -al /var/www/
    ```

??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/65b649cf-9b0b-4448-8823-0162e51fd9f0" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Image library software" width="150px"></img> </center>

## Web interface

We are going to set up an Apache Virtual Host as a Reverse Proxy to access Piwigo's web interface. Read on below for more details on how to set this up.

??? tip "Show me the step-by-step guide"

    Create an Apache configuration file:

    ```bash
    sudo vi /etc/apache2/sites-available/myphotos.gofoss.duckdns.org.conf
    ```

    Add the following content and make sure to adjust the settings to your own setup, such as domain names (`myphotos.gofoss.duckdns.org`), path to SSL keys, IP addresses and so on:

    ```bash
    <VirtualHost *:80>

    ServerName              myphotos.gofoss.duckdns.org
    ServerAlias             www.myphotos.gofoss.duckdns.org
    Redirect permanent /    https://myphotos.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

    ServerName              myphotos.gofoss.duckdns.org
    ServerAlias             www.myphotos.gofoss.duckdns.org
    ServerSignature         Off

    SecRuleEngine           Off
    SSLEngine               On
    SSLProxyEngine          On
    SSLProxyCheckPeerCN     Off
    SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
    SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
    DocumentRoot            /var/www/piwigo

    <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
    </Location>

    <Directory /var/www/piwigo/>
        Options +FollowSymlinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/myphotos.gofoss.duckdns.org-error.log
    CustomLog ${APACHE_LOG_DIR}/myphotos.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Once the content is added, save and close the file (`:wq!`).

    Note how we enable SSL encryption for Piwigo with the instruction `SSLEngine On`, and use the SSL certicate `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` as well as the private SSL key `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem`, which were created earlier on.

    Also note how we disabled ModSecurity in the Apache configuration file with the instruction `SecRuleEngine Off`, as Piwigo and ModSecurity don't play well together.

    Next, enable the Apache Virtual Host and restart Apache:

    ```bash
    sudo a2ensite myphotos.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Configure Pi-Hole to resolve Piwigo's local address. Browse to `https://mypihole.gofoss.duckdns.org` (adjust accordingly) and log into Pi-Hole's web interface. Navigate to the menu entry `Local DNS Records` and add the following domain/IP combination (adjust accordingly):

    ```bash
    DOMAIN:      myphotos.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

??? tip "Show me the 1-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/252127f0-8931-4a5e-a987-7f6502e55322" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Piwigo facial recognition" width="150px"></img> </center>

## Setting up the photo library

After successfully installing Piwigo, we're going to configure a number of settings such as the maximum upload size, the gallery title, user registration and logging, and so on. More details below.

??? tip "Show me the step-by-step guide"

    ### Maximum photo size

    On the server, open the Apache PHP configuration file:

    ```bash
    sudo vi /etc/php/8.1/apache2/php.ini
    ```

    Modify or add the following parameters to increase the maximum upload file size to 20 MB:

    ```bash
    upload_max_filesize = 20M
    post_max_size = 20M
    ```

    Open the command line interface (CLI) PHP configuration file:

    ```bash
    sudo vi /etc/php/8.1/cli/php.ini
    ```

    Modify or add the following parameters to increase the maximum upload file size to 20 MB:

    ```bash
    upload_max_filesize = 20M
    post_max_size = 20M
    ```

    ### Wrap up

    Browse to [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) (adjust accordingly) and follow the configuration guide:

    <center>

    | Field | Description |
    | ------ | ------ |
    | Host | The default value is `localhost`. |
    | User | Provide the name of the MySQL user. In our example, that's `piwigoadmin`, adjust accordingly. |
    | Password | Provide the password of the MySQL user. |
    |Database name | Provide the name of the MySQL database. In our example, that's `piwigo`, adjust accordingly. |
    |Database tables prefix | The default value is the name of the MySQL database followed by an underscore. In our example, that's `piwigo_`, adjust accordingly. |
    |Webmaster username | Create a webmaster account for Piwigo. For the purpose of this tutorial, we'll call the webmaster `piwigoadmin@gofoss.net`, adjust accordingly. |
    |Webmaster password | Provide a [strong, unique password](https://gofoss.net/passwords/) for the webmaster account. |
    |Webmaster email | Provide an email address for the webmaster account. |

    </center>

    Once the configuration is finished, click on the buttons `Start installation`, `Visit the gallery` and `I want to add photos`. That's it, Piwigo is ready for use!

    Start adjusting some basic settings in `Configuration ‣ Options ‣ General`:

    * change the gallery title and banner
    * disable user registration (uncheck `Allow user registration`)
    * track who logs into Piwigo (select `Save visits in history` for visitors, registered users and/or administrators)


??? tip "Show me the 2-minute summary video"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/006c7186-00ea-4c0e-b804-6a7b8629f3e0" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Temporary workaround for Ubuntu 22.04"

    At the time of writing, Piwigo 12.2.0 doesn't correctly handle PHP 8.1, which ships with Ubuntu 22.04. For this reason, the Piwigo interface will display various `Deprecated` and `Warning` messages after the installation. As a temporary workaround, you can suppress those messages:

    * Browse to [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) and log in as webmaster `piwigoadmin@gofoss.net` (adjust accordingly)
    * Go to `Plugins ‣ Plugins list ‣ All`
    * Activate the plugin `LocalFiles Editor`
    * Go to `Plugins ‣ Activated ‣ LocalFiles Editor ‣ Settings ‣ Local config`
    * Add the following line in the appearing `local/config/config.inc.php` window, then click on `Save`:

    ```bash
    <?php

    /* The file does not exist until some information is entered
    below. Once information is entered and saved, the file will be created. */

    $conf['show_php_errors'] = E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING;

    ?>
    ```

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Best photo gallery app for android" width="150px"></img> </center>

## Add users

Piwigo differentiates between [three user types](https://piwigo.org/doc/doku.php?id=user_documentation:use:features:user_status/):

* **Webmasters** have full access to Piwigo and can add, edit and remove photos, albums, users, groups, etc. In addition, webmasters can install plugins and themes, maintain and update the website, and so on.
* **Administrators** have full access to Piwigo and can add, edit and remove photos, albums, users, groups, and so on.
* **Users** have limited access to Piwigo. They can view albums and photos if they have the right permissions.


??? tip "Show me the step-by-step guide"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    |Step 1 | Browse to [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/) and log in as webmaster `piwigoadmin@gofoss.net` (adjust accordingly). |
    |Step 2 | Navigate to `Users ‣ Manage ‣ Add a user`. |
    |Step 3 | Provide a user name, as well as a [strong, unique password](https://gofoss.net/passwords/). Then click on `Add user`. |
    |Step 4 | Provide the status of the new user, for example `user` or `administrator`.|

    </center>

??? tip "Show me the 1-minute summary video"

    In this example, we add a new administrator Georg, and the users Lenina, Tom and RandomUser.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ba6374ef-d524-43e7-97eb-d1bc81d6f6cb" frameborder="0" allowfullscreen></iframe>
    </center>

??? warning "How to share pictures (VPN access required)"

    All admins and users must be connected to the home photo server via [VPN](https://gofoss.net/secure-domain/) to access Piwigo.


<br>

<center> <img src="../../assets/img/separator_simplegallery.svg" alt="Photo hosting sites" width="150px"></img> </center>

## Add photos

=== "Web form"

    The web form only requires a browser. It's best suited to add a manageable amount of pictures, or single albums. Files are uploaded to the server directory `/var/www/piwigo/upload`. Note that only administrators can add, edit and remove photos. Instructions on how to [add photos via the web form](https://piwigo.org/doc/doku.php?id=user_documentation:learn:add_picture/) are outlined below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Log in | Log into Piwigo's web interface with an administrator account. |
        |Web form | Navigate to `Admin ‣ Photos ‣ Add ‣ Web Form`. <br><br><center> <img src="../../assets/img/piwigo_web_form.png" alt="Photo gallery website" width="300px"></img> </center> |
        |Album | Create a new album, or select an existing one. |
        |Add photos | Add photos by clicking on the `Add Photos` button, or dragging and dropping them into the dedicated area. |
        |Upload | Click on `Start upload`. |

        </center>

    ??? tip "Show me the 1-minute summary video"

        Georg is an administrator. He uploads eight photos via the web form:

        * four photos from a recent hiking trip with Lenina: `tree.png`, `lake.png`, `moutain.png` and `sunset.png`
        * four pictures for a project he's working on: `logo.png`, `georg.png`, `lenina.png` and `tom.png`

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/73abb50e-805b-4619-aebf-5c49644a32e4" frameborder="0" allowfullscreen></iframe>
        </center>


=== "FTP client"

    This method requires a FTP client, such as [FileZilla](https://filezilla-project.org/). It's best suited to add large amounts of files, or upload an entire folder structure at once. Files must be uploaded to the server directory `/var/www/piwigo/galleries`. The original folder structure will be preserved. Note that only administrators can add, edit and remove photos. Instructions on how to [add photos via FTP](https://piwigo.org/doc/doku.php?id=user_documentation:learn:add_picture/) are outlined below.

    ??? tip "Show me the step-by-step guide"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Login | On your Ubuntu/Linux computer, log in with the `gofossadmin` account (adjust accordingly). This is the account with remote SSH access to the server, as configured in the chapters [Ubuntu Server](https://gofoss.net/ubuntu-server/) and [Basic server hardening](https://gofoss.net/server-hardening-basics/). |
        |Preparation | Still on your Ubuntu/Linux computer, sort the photos in a folder structure of your choice. Each folder will become an album in Piwigo. The depth of the nested folder tree is unlimited. Make however sure to respect naming conventions: folders and files can only contain letters, numbers, dashes, underscores or dots. No blank spaces or special characters are allowed. |
        |FTP installation |If not already installed, open a terminal with the `CTRL + ALT + T` shortcut and run the command `sudo apt install filezilla`.|
        |FTP preparation |Open FileZilla and provide the correct credentials: <br><br> <center> <img src="../../assets/img/filezilla.png" alt="Piwigo Filezilla" height="90px"></img> </center> <br> • `Host`: `sftp://192.168.1.100` (the server's IP address, adjust accordingly) <br><br> • `Username`: `gofossadmin` (adjust accordingly) <br><br> • `Password`: `passphrase_used_to_ssh_into_the_server` <br><br> • `Port`: `2222` (SSH port configured in chapter [Basic server hardening](https://gofoss.net/server-hardening-basics/), adjust accordingly) |
        |FTP connection | Still in FileZilla, click on `Quickconnect`. You will be prompted once again for the SSH passphrase. |
        |File copy | FileZilla should display your local computer's file system in left pane, and the server's file system in the right one. Copy the folders with your pictures to the following directory on the server: `/var/www/piwigo/galleries`. |
        |Login | Log into Piwigo's web interface with an administrator account. |
        |Simulation | Navigate to `Admin ‣ Tools ‣ Synchronise`. Before synchronising anything, select the following settings (adapt the permissions `Who can see these photos?` to your needs): <br><br> <center> <img src="../../assets/img/synchronise.png" alt="Piwigo synchronise" width="300px"></img> </center> <br> Then click on `Submit`. Piwigo should display the number of new albums and photos which will be added or removed from the database, and if any errors are to be expected. Proceed if everything is OK.|
        |Synchronisation | Still in `Admin ‣ Tools ‣ Synchronise`, uncheck `perform a simulation only`:<br><br> <center> <img src="../../assets/img/synchronise_2.png" alt="Piwigo synchronise" width="300px"></img> </center> <br> Then click on `Submit`. This may take a while, depending on the amount of data. Grab a cup of coffee!|

        </center>

    ??? warning "Some words of advice"

        Be aware that directories and files shouldn't be moved once they're uploaded to the server. Else, all associated data will be lost during the next synchronisation (such as comments, ratings and so on).


=== "3rd party apps"

    This method requires third party photo applications such as [digiKam](https://www.digikam.org/), [Shotwell](https://shotwell-project.org/doc/html/), [Lightroom](https://lightroom.adobe.com/) or Piwigo's [Android](https://f-droid.org/en/packages/org.piwigo.android/) and [iOS](https://apps.apple.com/app/piwigo/id472225196) apps. It's best suited to add large amounts of unstructured photos, which are uploaded to the server directory `/var/www/piwigo/upload`. Note that only administrators can add, edit and remove photos. More detailed instructions below.

    ??? tip "Show me the step-by-step guide"

        Refer to the respective third party application documentation for instructions on the best way to organize photos. Most third party applications require credentials to connect to the server and add photos:

        <center>

        | Settings | Description |
        | ------ | ------ |
        |Server| Provide the address of the photo gallery. In our example, that's [https://myphotos.gofoss.duckdns.org](https://myphotos.gofoss.duckdns.org/), adjust accordingly. |
        |User | Provide the name of a Piwigo administrator or user. |
        |Password | Provide the password of the Piwigo administrator or user above. |

        </center>

        <center><img align="center" src="../../assets/img/piwigo_app_2.jpg" alt="Android photo backup" width="150px"></img>
        </center>



<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Online photo gallery" width="150px"></img> </center>

## Edit photos

=== "Edit a single photo"

    You can edit single photos to change the title, author, creation date, album(s), keywords, description, privacy, geolocation and so on. You can also define the most meaningful area of the photo. More detailed instructions below.

    ??? tip "Show me the step-by-step guide"

        Log in as administrator and browse to `Photos ‣ Batch Manager ‣ Global Mode`. Apply one or several filters to find the photo you want to edit (more about filter attributes below). Hover over the photo and click on `Edit`. You are now able to modify the photo.

    ??? tip "Show me the 1-minute summary video"

        Georg modifies the author of the picture `logo.png`.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/90ef6f40-27bd-4d8d-aa3c-234d056b396a" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Edit multiple photos"

    Editing multiple photos at once is also called batch editing or bulk editing. More detailed instructions below.

    ??? tip "Show me the step-by-step guide"

        Log in as administrator and browse to `Photos ‣ Batch Manager ‣ Global Mode`. Apply one or several filters to find the photos you want to edit (more about filter attributes below). You can select or de-select all photos at once by clicking on `All` or `None`. Finally, specify what action to perform: delete, associate to one or several album(s), move to an album or dissociate from an album, set keywords, author, title, creation date or geotags and so on.

    ??? tip "Show me the 1-minute summary video"

        Georg generates multiple size images for the photos from his hiking trip.

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/6407514b-20e2-4d55-bba4-52e4ed5fa703" frameborder="0" allowfullscreen></iframe>
        </center>


<div style="    margin-top: -20px;">
</div>

??? info "Tell me more about filter attributes"

    <center>

    | Filter attributes | Description |
    | ----- | ----- |
    |Predefined filters | Filter all photos, all videos, duplicated photos, latest imported photos, geotagged photos, photos without an attributed album (orphans), photos without keywords, favourite photos, and so on. |
    |Album | Filter photos from a specific album. |
    |Keywords | Filter photos based on keywords. |
    |Privacy level | Filter photos visible by everybody, contacts, friends, family or administrators. |
    |Dimensions | Filter photos based on their size. |
    |File size | Filter photos based on their file size. |
    |Search | Filter photos based on advanced search results. Can be the title, tag, file name, author, creation date, post date, width, height, file size, ratio, and so on. |

    </center>


<br>

<center> <img src="../../assets/img/separator_simplefilemanager.svg" alt="Digital asset management system" width="150px"></img> </center>

## Manage albums

You can add new albums or edit existing ones via Piwigo's web interface. This includes editing the album's name and description, defining a parent album, locking the album so that it's only visible to administrators, adding photos, managing sub-albums, setting automatic ordering or defining a manual order, and so on. You can also make an album public so that it's visible to anyone with the link, or private so that it's only visible to logged in users with appropriate permissions. More detailed instructions below.

??? tip "Show me the step-by-step guide"

    Log in as administrator and browse to `Admin ‣ Album ‣ Manage ‣ List`. To create a new album, click on `Add album`. To edit existing an album, hover the mouse over it and click on `Edit`. You are now able to modify the album's properties, sort order or permissions.

??? tip "Show me the 1-minute summary video"

    In this example, administrator Georg adds a description to the album Holidays.

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/eda37f42-f7bd-463b-96c6-109de612e88e" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="How to share photos" width="150px"></img> </center>

## Manage permissions

Piwigo has two systems to manage access permissions, which can be used independently or combined:

* **User/group permissions** apply to albums, users and groups
* **Privacy levels** apply to photos and users

This rather complex permission system opens up possibilities to fine tune access rights for multiple users. If you don't share pictures with others, keep it simple: make all your albums private and limit access to yourself.


=== "User/group permissions"

    Let's start with album-level permissions. By default, albums are `public` and can be viewed by any user. Administrators can make albums `private` and limit access to certain users and/or groups. Instructions on how to [manage user/group permissions](https://piwigo.org/doc/doku.php?id=user_documentation:permissions_management/) are outlined below.

    ??? tip "Show me a step-by-step guide"

        To make an album `private`:

        | Instructions | Description |
        | ------ | ------ |
        |Step 1 | Log in as administrator. |
        |Step 2 | Browse to `Album ‣ Manage ‣ List ‣ Edit ‣ Permissions`. |
        |Step 3 | Switch access from `public` to `private`. |

        To limit access to `private` albums to certain `users`:

        | Instructions | Description |
        | ------ | ------ |
        |Step 1 | Log in as administrator. |
        |Step 2 | Either browse to `Album ‣ Manage ‣ List ‣ Edit ‣ Permissions` and add users which can view the private album to the box `Permissions granted`.<br><br> Or browse to `Users ‣ Manage ‣ Edit User ‣ Permissions` and define which private albums the user can see (`Authorised`) or not (`Forbidden`).|

        Access rights can also be defined for a set of users, called `groups`. This allows to set permissions for entire groups, which facilitates permission management if there are many users. To limit access to `private` albums to certain `groups`:

        | Instructions | Description |
        | ------ | ------ |
        |Step 1 | Create groups by browsing to `Users ‣ Groups ‣ Add Group`.|
        |Step 2 | Add users to a group by browsing to `Users ‣ Manage ‣ Edit User ‣ Groups`. |
        |Step 3 | Finally, define album access rights for entire groups by browsing to `Users ‣ Groups ‣ Permissions`. |


=== "Privacy levels"

    Privacy levels are defined on a per-photo and per-user level. It allows to precisely define which user can access which photos. Instructions on how to [manage these privacy levels](https://piwigo.org/doc/doku.php?id=user_documentation:permissions_management/) are outlined below.

    ??? tip "Show me a step-by-step guide"

        Piwigo manages permissions with five `privacy levels`. Here is how it works:

        * each photo has a privacy level
        * each user has a privacy level
        * a user must have a privacy level greater or equal to the one of the photos he wants to view. Or put differently, the higher a users privacy level, the more photos he can see

        The privacy level of `photos` can be specified by logging in as administrator and browsing to `Photos ‣ Batch Manager ‣ Global Mode`. Apply filters as needed, and select the relevant photos. Choose the action `Who can see these photos` and define the appropriate `privacy level`:

        <center>

        | Privacy level | Description |
        | :------: | ------ |
        |1 | Everybody |
        |2 | Admins, Family, Friends, Contacts |
        |3 | Admins, Family, Friends |
        |4 | Admins, Family |
        |5 | Admins |

        </center>

        The privacy level of a `user` is defined by logging in as administrator and browsing to `Users ‣ Manage ‣ Edit User ‣ Privacy level`:

        <center>

        | Privacy level | Description |
        | :------: | ------ |
        |1 | None |
        |2 | Contacts |
        |3 | Friends |
        |4 | Family |
        |5 | Admins |

        </center>


=== "Example"

    Let's wrap up with a concrete example, applying all concept explained so far. Notice that the same result could have been achieved in a different way. Piwigo is quite flexible, choose your preferred approach.

    ??? tip "Show me the example"

        <center>
        <img align="center" src="../../assets/img/piwigo_permissions.png" alt="Google photo gallery" width="700px"></img>
        </center>

        <center>

        | Circle | Description |
        | ------ | ------ |
        |1 | Georg's user status is set to `Administrator`. This means he can add, edit and remove photos, albums, users, groups, and so on. Georg's privacy level is set to `Admins`. Georg therefore has access to all albums and photos. |
        |2 | Georg uploaded four photos from a recent hiking trip to the album `Holidays`: `tree.png`, `lake.png`, `moutain.png` and `sunset.png`. He doesn't want to share these photos with anyone but Lenina. Lenina status is set to `User`, meaning that she can only view `public` albums, or files for which she obtains the right permissions. Georg therefore makes the album `Holidays` `private`, and limits access to a group called `Hiking`, to which only himself and Lenina are assigned. |
        |3 | Georg also uploaded four pictures to the album `Gofoss`: `logo.png`, `georg.png`, `lenina.png` and `tom.png`. Since it's a `public` album, everybody can access the pictures, including `logo.png`. |
        |4 | However, Georg set the pictures `georg.png`, `lenina.png` and `tom.png` to the privacy level `Family`. This means only users with privacy levels `Family` and above (`Admins`) have access to these pictures. In this example, it would be Georg (`Admins`), Lenina (`Family`) and Tom (`Family`). RandomUser has no access to those pictures, since his privacy level is `Friends`. |

        </center>


    ??? tip "Show me the 4-minute summary video"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/fe8fdbb7-c18a-407a-b3a7-689026d38eaf" frameborder="0" allowfullscreen></iframe>
        </center>



<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Piwigo hosting" width="150px"></img> </center>

## Add plugins & themes

=== "Plugins"

    Piwigo's functionalities can be extended with over [350 plugins](https://piwigo.org/ext/). More detailed instructions below.

    ??? tip "Show me a step-by-step guide"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Display plugins | To display all installed plugins, log in as webmaster `piwigoadmin@gofoss.net` (adjust accordingly) and browse to `Admin ‣ Plugins ‣ Plugins List`:<br><br><center> <img src="../../assets/img/piwigo_plugins.png" alt="Piwigo plugins" width="300px"></img></center><br>• `Active plugins` are installed and currently running <br><br>• `Inactive plugins` are installed but currently disabled <br><br>• Note that deactivating a plugin will keep most of its configuration saved, while deleting a plugin will remove any trace (files, configurations, etc.) |
        |Update plugins | Browse to `Admin ‣ Plugins ‣ Check for updates`. |
        |Add plugins | Browse to `Admin ‣ Plugins ‣ Other plugins available`. Search for a plugin and click on `Install`. Browse to `Admin ‣ Plugins ‣ Plugins list` and activate the installed plugin. |

        </center>

        Some popular plugins:

        <center>

        | Plugin | Description |
        | ------ | ------ |
        | [Piwigo-Videojs](https://piwigo.org/ext/extension_view.php?eid=610/) | Piwigo video plugin, supports various formats: mp4, m4v, ogg, ogv, webm, webmv, etc. More info available on the [Wiki page](https://github.com/Piwigo/piwigo-videojs/wiki). |
        | [Fotorama](https://piwigo.org/ext/extension_view.php?eid=727/) | Full screen slideshow. |
        | [Batch downloader](https://piwigo.org/ext/extension_view.php?eid=616/) | Photo gallery download as zip file. |
        | [Piwigo-Openstreetmap](https://piwigo.org/ext/extension_view.php?eid=701/) | Geolocate your pictures. More info available on the [Wiki page](https://github.com/Piwigo/piwigo-openstreetmap/wiki). |
        | [Grum Plugin Classes](https://piwigo.org/ext/extension_view.php?eid=199/) | Required to run some other plugins. |
        | [AStat](https://piwigo.org/ext/extension_view.php?eid=172/) | Extends the statistics generated by Piwigo, e.g. which pages or photos have been visited, for how long, from which IP address, etc. |
        | [EXIF view](https://piwigo.org/ext/extension_view.php?eid=155/) | Add EXIF metadata to your photos. |

        </center>

    ??? tip "Show me the 1-minute summary video"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b52c041b-c4b6-46cd-9e5f-741c3c382cdc" frameborder="0" allowfullscreen></iframe>
        </center>


=== "Themes"

    Piwigo's layout can be customised with over [140 themes](https://piwigo.org/ext/). More detailed instructions below.

    ??? tip "Show me a step-by-step guide"

        Log in as webmaster `piwigoadmin@gofoss.net` (adjust accordingly) and browse to `Admin ‣ Configuration ‣ Themes`. Download, activate or configure themes of your choice. Some popular ones:

        <center>

        | Themes | Description |
        | ------ | ------ |
        | [Modus](https://piwigo.com/blog/2019/06/25/new-modus-theme-on-piwigo-com/) | Default theme, comes with several skins. |
        | [Bootstrap Darkroom](https://piwigo.com/blog/2020/03/12/bootstrap-darkroom-new-theme-piwigo-com/) | Another modern, feature-rich and mobile-friendly theme. |
        | [SimpleNG](https://piwigo.org/ext/extension_view.php?eid=602/) | Based on Bootstrap, responsive theme. |

        </center>

    ??? tip "Show me the 1-minute summary video"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a14ba8f8-6ee9-497a-81e3-86418c261eec" frameborder="0" allowfullscreen></iframe>
        </center>



<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Piwigo video" width="150px"></img> </center>

## Upgrade

Upgrading Piwigo is pretty [straight forward](https://piwigo.org/guides/update/automatic). Just follow the instructions oulined below.

??? tip "Show me a step-by-step guide"

    [Back up your photos](https://gofoss.net/backups/) in case anything goes wrong during the update. One possible way is to connect via FTP (FileZilla) as described previously and backup the folders `/var/www/galleries` and `/var/www/upload`.

    You can also backup Piwigo's database using [MySQL dump](https://linuxize.com/post/how-to-back-up-and-restore-mysql-databases-with-mysqldump/) or [server backups](https://gofoss.net/server-backups/). Piwigo's MySQL database is usually stored in `/var/lib/mysql`.

    Now, log in as webmaster `piwigoadmin@gofoss.net` (adjust accordingly) and browse to `Admin ‣ Tools ‣ Updates ‣ Piwigo Update`. Click on `Update to Piwigo xx.x.x`` and confirm.

??? tip "Show me the 30-second summary video"

    <center> <img src="../../assets/img/piwigo_upgrade.gif" alt="Google photos alternative self hosted" width="600px"></img> </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Digital asset management software" width="150px"></img> </center>

## Support

For further details, refer to [Piwigo's documentation](https://piwigo.org/doc/doku.php) or request support from the [Piwigo forums](https://piwigo.org/forum/).

<br>
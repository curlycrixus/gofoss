---
template: main.html
title: 75 services en nuage respectant la vie privée
description: Services en nuage respectueux de la vie privée. Fournisseurs alternatifs. Framasoft, Chatons, Disroot, Riseup, Systemausfall, Picasoft, Digitalcourage.
---

# 75 services en nuage respectant la vie privée

!!! level "Dernière mise à jour: avril 2022. Destiné aux débutants. Aucune compétence technique particulière n'est requise."

<div align="center">
<img src="../../assets/img/cloud_storage_2.png" alt="Alternative cloud providers" width="550px"></img>
</div>

## Les fournisseurs alternatifs

=== "Framasoft"

    <img src="../../assets/img/framasoft.png" alt="Framasoft" width="80px" align="left"></img> [Framasoft](https://framasoft.org/fr/) est une association française, créée en 2001. Elle fait la promotion des alternatives libres, [offre plusieurs services en ligne](https://degooglisons-internet.org/fr/list/) et [recommande des logiciels respectant la vie privée](https://degooglisons-internet.org/fr/alternatives/). [Les données personnelles récoltées par Framasoft sont soumises à la législation française](https://framasoft.org/fr/legals/), et les serveurs sont situés en Allemagne.


=== "Kitten"

    <img src="../../assets/img/chatons.png" alt="Chatons" width="70px" align="left"></img> [Chatons](https://www.chatons.org/) est un collectif français regroupant des fournisseurs alternatifs de services en nuage. Il promeut des [services en ligne](https://www.chatons.org/search/by-service/) transparents, ouverts, neutres et durables. [Les données personnelles récoltées par Chatons sont soumises à la législation française](https://www.chatons.org/mentions-legales/). Les serveurs de Chatons sont situés en France et en Allemagne.


=== "Disroot"

    <img src="../../assets/img/disroot.png" alt="Disroot" width="70px" align="left"></img> [Disroot](https://disroot.org/fr/) est un projet néerlandais géré par des bénévoles, créé en 2015. Le projet propose [plusieurs services en ligne](https://disroot.org/fr/#services), la plupart étant libres d'utilisation. [Disroot affirme respecter les RGPD](https://disroot.org/fr/privacy_policy), et les serveurs sont situés aux Pays-Bas. [L'application Android de Disroot](https://f-droid.org/fr/packages/org.disroot.disrootapp/) permet l'accès mobile à la plupart de ses services.


=== "Digitalcourage"

    <img src="../../assets/img/digitalcourage.png" alt="Digitalcourage" width="70px" align="left"></img> [Digitalcourage](https://digitalcourage.de/en) est une association allemande, créée en 1987. Elle propose [plusieurs services en ligne](https://digitalcourage.de/swarm-support), la plupart étant libres d'utilisation. [Digitalcourage affirme respecter les RGPD](https://digitalcourage.de/datenschutz-bei-digitalcourage), et les serveurs sont gérés par la société [freistil.it](https://www.freistil.it/) basée en Irlande.


=== "RiseUp"

    <img src="../../assets/img/riseup.png" alt="RiseUp" width="80px" align="left"></img> [RiseUp](https://riseup.net/fr) est un collectif géré par des bénévoles, basé à Seattle. Créé en 1999, il offre plusieurs services et ressources informatiques. [RiseUp affiche une politique de confidentialité transparente](https://riseup.net/fr/about-us/policy/privacy-policy).


=== "Systemausfall"

    <img src="../../assets/img/systemausfall.png" alt="Systemausfall" width="178px" align="left"></img> [Systemausfall](https://systemausfall.org/) est un collectif géré par des bénévoles, basé en Allemagne. Il a été créé en 2003. Le collectif offre plusieurs services et ressources informatiques. [Systemausfall affiche une politique de confidentialité transparente](https://systemausfall.org/wikis/hilfe/Datensicherheit/).


=== "Picasoft"

    <img src="../../assets/img/picasoft.png" alt="Picasoft" width="70px" align="left"></img> [Picasoft](https://picasoft.net/) est une association française à but non lucratif de l'Université technique de Compiègne, créee en 2016. Elle promeut un mode de vie libre, inclusif et respectueux de la vie privée. Picasoft propose différents services en ligne. [Les données personnelles collectées par Picasoft sont soumises à la loi française](https://picasoft.net/co/cgu.html). Les serveurs de Picasoft sont situés en [France](https://wiki.picasoft.net/doku.php?id=technique:resume).



## Suite bureautique

=== "Éditeurs de texte"

    <img src="../../assets/img/editor.svg" alt="Éditeur de texte en ligne" width="50px" align="left"></img> Remplacez Google Docs, Word365 et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framapad](https://framapad.org/abc/fr/) |Éditeur de texte en ligne collaboratif, basé sur [etherpad](https://etherpad.org/) |
    |[Disroot pad](https://pad.disroot.org/) |Éditeur de texte en ligne collaboratif, basé sur [etherpad](https://etherpad.org/) |
    |[Disroot cryptpad](https://cryptpad.disroot.org/pad/) |Éditeur de texte en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage cryptpad](https://cryptpad.digitalcourage.de/) |Éditeur de texte en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Riseup pad](https://pad.riseup.net/) |Éditeur de texte en ligne collaboratif, basé sur [etherpad](https://etherpad.org) |
    |[Picapad](https://pad.picasoft.net/) |Éditeur de texte en ligne collaboratif, basé sur [etherpad](https://etherpad.org/) |
    |[Zaclys cloud docs](https://www.zaclys.com/cloud-2/) |Éditeur de texte en ligne collaboratif, basé sur [nextcloud](https://nextcloud.com/) & [onlyoffice](https://www.onlyoffice.com/fr/) |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=135&title=&field_software_target_id=All&field_is_shared_value=All) |Divers éditeurs de texte collaboratifs en ligne, basés sur [cryptpad](https://cryptpad.fr), [etherpad](https://etherpad.org/) ou [hedgedoc](https://hedgedoc.org/) |


=== "Tableurs"

    <img src="../../assets/img/spreadsheet.svg" alt="Tableur en ligne" width="50px" align="left"></img> Replacez Google Spreadsheet, Excel365 et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framacalc](https://framacalc.org/abc/fr/) |Tableur en ligne collaboratif, basé sur [ethercalc](https://github.com/audreyt/ethercalc/) |
    |[Disroot calc](https://calc.disroot.org/) |Tableur en ligne collaboratif, basé sur [ethercalc](https://github.com/audreyt/ethercalc/) |
    |[Disroot sheet](https://cryptpad.disroot.org/sheet/) |Tableur en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage sheet](https://cryptpad.digitalcourage.de/) |Tableur en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Zaclys cloud sheets](https://www.zaclys.com/cloud-2/) |Tableur en ligne collaboratif, basé sur [nextcloud](https://nextcloud.com/) & [onlyoffice](https://www.onlyoffice.com/fr/) |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=136&title=&field_software_target_id=All&field_is_shared_value=All) |Divers tableurs en ligne collaboratifs, basés sur [ethercalc](https://github.com/audreyt/ethercalc/) |


=== "Disapositives"

    <img src="../../assets/img/slide.svg" alt="Éditeurs de diapositives en ligne" width="50px" align="left"></img> Remplacez Google Slides, Powerpoint365 et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Disroot slides](https://cryptpad.disroot.org/slide/) |Éditeur de diapositives en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage cryptpad](https://cryptpad.digitalcourage.de/) |Éditeur de diapositives en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Zaclys cloud slides](https://www.zaclys.com/cloud-2/) |Éditeur de diapositives en ligne collaboratif, basé sur [nextcloud](https://nextcloud.com/) & [onlyoffice](https://www.onlyoffice.com/fr/) |


=== "Calendrier"

    <img src="../../assets/img/simplecalendar.svg" alt="Calendrier en ligne" width="50px" align="left"></img> Remplacez Google Calendar, Outlook365 et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framagenda](https://framagenda.org/login/) |Calendrier en ligne collaboratif, basé sur [nextcloud](https://nextcloud.com/) |
    |[Cryptpad calendar](https://cryptpad.fr/calendar/) |Calendrier en ligne collaboratif et chiffré, basé sur [cryptpad](https://cryptpad.fr) |
    |[Zaclys cloud calendar](https://www.zaclys.com/cloud-2/)|Calendrier en ligne collaboratif, basé sur [nextcloud](https://nextcloud.com/) |


=== "Réunions"

    <img src="../../assets/img/doodle.svg" alt="Planification de réunions" width="50px" align="left"></img> Remplacez Doodle et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framadate](https://framadate.org/abc/fr/) |Service en ligne pour la planification de réunions et la prise de décision,  basé sur [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Disroot poll](https://poll.disroot.org/) |Service en ligne pour la planification de réunions et la prise de décision,  basé sur  [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Digitalcourage poll](https://nuudel.digitalcourage.de/) |Service en ligne pour la planification de réunions et la prise de décision,  basé sur [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=139&title=&field_software_target_id=All&field_is_shared_value=All) |Divers services en ligne pour la planification de réunions et la prise de décision,  basés sur [studs](https://sourcesup.cru.fr/projects/studs/) |


## Stockage et synchronisation de fichiers

=== "Stockage en nuage"

    <img src="../../assets/img/mysql.svg" alt="Stockage en nuage" width="50px" align="left"></img> Remplacez Google Drive, Dropbox, iCloud, OneDrive et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Disroot cloud](https://cloud.disroot.org/) |Solution de stockage en nuage, basée sur [nextcloud](https://nextcloud.com/) |
    |[Systemausfall drive](https://speicher.systemausfall.org/accounts/login/?next=) |Solution de stockage en nuage, basée sur [seafile](https://www.seafile.com/en/home/) |
    |[Zaclys cloud](https://www.zaclys.com/cloud-2/) |Solution de stockage en nuage, basée sur [nextcloud](https://nextcloud.com/) |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=147&title=&field_software_target_id=All&field_is_shared_value=All) |Diverses solutions de stockage en nuage, basées sur [nextcloud](https://nextcloud.com/) ou [seafile](https://www.seafile.com/en/home/)|
    |[Disroot cryptdrive](https://cryptpad.disroot.org/drive/)|Solution de stockage en nuage chiffrée, basée sur  [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage cryptdrive](https://cryptpad.digitalcourage.de/drive/)|Solution de stockage en nuage chiffrée, basée sur [cryptpad](https://cryptpad.fr) |


=== "Partage de fichiers"

    <img src="../../assets/img/davx5.svg" alt="Partage de fichiers" width="50px" align="left"></img> Remplacez WeTransfer et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Disroot upload](https://upload.disroot.org/) |Solution de partage de fichiers chiffrée, basée sur [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[Riseup share](https://share.riseup.net/) |Solution de partage de fichiers, basée sur [up1](https://github.com/Upload/Up1/)|
    |[Systemausfall share](https://teilen.systemausfall.org/login/)|Solution de partage de fichiers chiffrée, basée sur [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[PicaDrop](https://drop.picasoft.net/) |Solution de partage de fichiers chiffrée, basée sur [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[Standardnotes filesend](https://filesend.standardnotes.com)|Solution de partage de fichiers, basée sur [standardnotes](https://github.com/standardnotes/filesend)|
    |[Zaclys share](https://www.zaclys.com/envoi/) |Solution de partage de fichiers |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=148&title=&field_software_target_id=All&field_is_shared_value=All) |Diverses solutions de partage de fichiers, basées sur [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/), [plik](https://github.com/root-gg/plik/), [file2link](https://framagit.org/kepon/file2link/) ou [firefox send (fork)](https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/) |


## Chats et listes de diffusion

=== "Appels vidéo et vocaux"

    <img src="../../assets/img/jitsi.svg" alt="Jitsi" width="50px" align="left"></img> Remplacez Skype, Google Hangouts, Zoom et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framatalk](https://framatalk.org/accueil/) |Solution de vidéoconférence et d'audioconférence sécurisée, basée sur [jitsi meet](https://meet.jit.si/). Prend en charge les [appels vidéo chiffrés de bout en bout](https://jitsi.org/e2ee-in-jitsi/) |
    |[Disroot calls](https://calls.disroot.org/) |Solution de vidéoconférence et d'audioconférence sécurisée, basée sur [jitsi meet](https://meet.jit.si/). Prend en charge les [appels vidéo chiffrés de bout en bout](https://jitsi.org/e2ee-in-jitsi/) |
    |[Senfcall](https://senfcall.de/)|Solution de visioconférence, basée sur [big blue button](https://bigbluebutton.org/) |
    |[Picasoft voice](https://framatalk.org/accueil/)|Solution d'audioconférence, basée sur [mumble](https://www.mumble.info/)|
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=117&title=&field_software_target_id=All&field_is_shared_value=All) |Diverses solutions de vidéoconférence et d'audioconférence sécurisées, basées sur [jitsi meet](https://meet.jit.si/), [nextcloud](https://nextcloud.com/) ou [big blue button](https://bigbluebutton.org/) |


=== "Chats en ligne"

    <img src="../../assets/img/signal.svg" alt="Chats en ligne" width="50px" align="left"></img> Remplacez Teams, Facebook Groups, Slack et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framateam](https://framateam.org/login/)  |Solution de chat en équipe, basée sur [mattermost](https://mattermost.com/) |
    |[Disroot web chat](https://webchat.disroot.org/) |Solution de chat en équipe, basée sur [xmpp](https://conversejs.org/) |
    |[Systemausfall chat](https://klax.systemausfall.org/) |Solution de chat en équipe, basée sur [matrix](https://matrix.org/) |
    |[Picateam](https://team.picasoft.net/login)|Solution de chat en équipe, basée sur [mattermost](https://mattermost.com/) |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=118&title=&field_software_target_id=All&field_is_shared_value=All) |Diverses solutions de chat en équipe, basées sur [matrix](https://matrix.org/), [mattermost](https://mattermost.com/) ou [rocket chat](https://rocket.chat/) |


=== "Listes de diffusion"

    <img src="../../assets/img/pidgin.svg" alt="Listes de diffusion" width="50px" align="left"></img> Remplacez Google Groups et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Framalistes](https://framalistes.org/sympa/) |Service de listes de diffusion, basé sur [sympa](https://www.sympa.org/)|
    |[Riseup lists](https://lists.riseup.net/) |Service de listes de diffusion, basé sur [sympa](https://www.sympa.org/)    |
    |[Systemausfall lists](https://wat.systemausfall.org/) |Service de listes de diffusion, basé sur [sympa](https://www.sympa.org/) |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=114&title=&field_software_target_id=All&field_is_shared_value=All) |Divers services de listes de diffusion, basés sur [sympa](https://www.sympa.org/) |


## Réseaux sociaux

=== "Réseaux sociaux"

    <img src="../../assets/img/permissions.svg" alt="Fediverse" width="50px" align="left"></img> Remplacez Facebook et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Diaspora](https://diasporafoundation.org/) |Réseau social décentralisé, libre et privé. Basé sur [activitypub](https://activitypub.rocks/), fait parti du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Friendica](https://friendi.ca/) |Réseau social décentralisé, privé et interopérable. Basé sur [activitypub](https://activitypub.rocks/), fait parti du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[GNU Social](https://gnusocial.network/)|Réseau social libre et open source. Basé sur [activitypub](https://activitypub.rocks/), fait parti du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Minds](https://www.minds.com/) |Réseau social gratuit et open-source, chiffré et basé sur un système de récompense |


=== "Microblogage"

    <img src="../../assets/img/tusky.svg" alt="Mastodon" width="50px" align="left"></img> Remplacez Twitter et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Mastodon](https://joinmastodon.org/) |Plateforme de microblogage décentralisée permettant de partager des messages, des images, des fichiers audio et vidéo ainsi que des sondages. Basée sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Nitter](https://nitter.net/)|Interface libre et open source pour Twitter. Pas de publicité, pas de traçage, pas de Twitter |


=== "Blogging"

    <img src="../../assets/img/apache.svg" alt="Write Freely" width="50px" align="left"></img> Remplacez Medium, WordPress et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[WriteFreely](https://writefreely.org/)|Plateforme de blogging décentralisée et open source. Basée sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |


=== "Événements"

    <img src="../../assets/img/simplecalendar.svg" alt="Mobilizon" width="50px" align="left"></img> Remplacez Facebook Events, Facebook Groups et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Mobilizon](https://joinmobilizon.org/) |Créez, trouvez et organisez des événements. Basé sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |


=== "Forums"

    <img src="../../assets/img/redreader.svg" alt="Lemmy" width="50px" align="left"></img> Remplacez Reddit et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Lemmy](https://join-lemmy.org/) |Forums de discussion en ligne. Basé sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Aether](https://getaether.net/) |Forums de discussion en ligne. Open source, pair-à-pair et éphémère |
    |[Teddit](https://teddit.net/)|Interface libre et open source pour Reddit. Pas de publicité, pas de traçage, pas de Reddit |
    |[Libreddit](https://libredd.it/)|Interface libre et open source pour Reddit. Pas de publicité, pas de traçage, pas de Reddit |


## Médias sociaux

=== "Vidéos"

    <img src="../../assets/img/totem.svg" alt="Peertube" width="50px" align="left"></img> Remplacez YouTube, Vimeo, Dailymotion et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Peertube](https://joinpeertube.org/) |Plateforme de partage vidéo décentralisée, libre et open source. Basée sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Dtube](https://d.tube/) |Plateforme de partage vidéo décentralisée, libre et open source |
    |[Invidious](https://invidious.io/)|Interface libre et open source pour YouTube. Pas de publicité, pas de traçage, pas de Google |


=== "Photos"

    <img src="../../assets/img/opencamera.svg" alt="Pixelfed" width="50px" align="left"></img> Remplacez Instagram, Flickr, Google Photos, Img.ur et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Pixelfed](https://pixelfed.org/) |Plateforme de partage de photos libre et éthique. Basée sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Bibliogram](https://bibliogram.art/) |Interface libre et open source pour Instagram. Pas de publicité, pas de traçage, pas de Facebook |
    |[Zaclys album](https://www.zaclys.com/album/) |Créez des albums photo privés ou publics et partagez-les avec vos ami.e.s |
    |[Services Chatons](https://www.chatons.org/en/search/by-service?service_type_target_id=149&title=&field_software_target_id=All&field_is_shared_value=All) |Diverses solutions de partage de photos, basées sur [lutim](https://github.com/ldidry/lutim/) |


=== "Musique"

    <img src="../../assets/img/audacity.svg" alt="Funkwhale" width="50px" align="left"></img> Remplacez Spotify, YouTube, Last.fm et consorts:

    |Alternative FOSS |Description |
    | ------ | ------ |
    |[Funkwhale](https://funkwhale.audio/) |Plateforme décentralisée libre et open source pour écouter et partager de la musique. Basée sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |
    |[Libre.fm](https://libre.fm/)|Plateforme libre et open source pour écouter et partager de la musique |
    |Internet Radio |Écoutez des stations de radio en ligne, par exemple avec [RadioDroid](https://f-droid.org/fr/packages/net.programmierecke.radiodroid2/) |
    |Podcasts |Écoutez des podcasts gratuits, par exemple avec [AntennaPod](https://f-droid.org/fr/packages/de.danoeh.antennapod/) |


=== "Livres"

    <img src="../../assets/img/books.svg" alt="Bookwyrm" width="50px" align="left"></img> Remplacez Goodreads et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Bookwyrm](https://bookwyrm.social/)|Plateforme décentralisée libre et open source pour échanger sur des livres, suivre vos lectures et en parler à vos ami.e.s. Basée sur [activitypub](https://activitypub.rocks/), fait partie du [fediverse](https://gofoss.net/fr/fediverse/) |


## Autres services en nuage

=== "Recherche"

    <img src="../../assets/img/search.svg" alt="Moteurs de recherche" width="50px" align="left"></img> Remplacez Google Search, Bing et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[SearX](https://searx.space/) |Métamoteur libre regroupant les résultats de plus de 70 services de recherche. Pas de publicité, pas de traçage, pas de Google, pas de Microsoft. Sélectionnez votre instance préférée, comme par exemple [searx.be](https://searx.be/) ou [disroot search](https://search.disroot.org/) |
    |[DuckDuckGo](https://duckduckgo.com/) |Moteur de recherche axé sur la vie privée, sans traçage. *Attention*: diffuse des publicités provenant du réseau d'alliance Yahoo-Bing et des relations d'affiliation avec Amazon et eBay |


=== "Cartes"

    <img src="../../assets/img/maps.svg" alt="Cartes en ligne" width="50px" align="left"></img> Remplacez Google Maps et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Openstreetmap](https://www.openstreetmap.org/) |Carte du monde en ligne. Libre, open source et maintenue par la communauté internet |
    |[Framacarte](https://framacarte.org/fr/) |Service en ligne libre pour créer vos propres cartes. Basé sur [umap](https://github.com/umap-project/umap/) |
    |[GoGoCarto](https://gogocarto.fr/projects) |Service en ligne libre pour créer vos propres cartes |


=== "Traductions"

    <img src="../../assets/img/empathy.svg" alt="Deepl" width="50px" align="left"></img> Remplacez Google Translations et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[LibreTranslate](https://libretranslate.com/)|Service de traduction libre et open source. Pas de publicité, pas de Google |
    |[Simply Translate](https://simplytranslate.org/)|Interface libre et open source pour Google Translate. Pas de publicité, pas de traçage, pas de Google |
    |[Deepl](https://www.deepl.com/translator)|Service de traduction gratuit. *Attention*: pas open source |


=== "Wikipedia"

    <img src="../../assets/img/fossbrowser.svg" alt="Wikipedia" width="50px" align="left"></img> Remplacez Wikipedia et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Wikiless](https://wikiless.org/)|Interface libre et open source pour Wikipédia. Pas de publicité, pas de traçage, pas de censure |


=== "Raccourcisseurs URL"

    <img src="../../assets/img/fossbrowser.svg" alt="Raccourcisseurs d'URL" width="50px" align="left"></img> Remplacez Bit.ly, Goo.gl et consorts:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Let's shorten that URL](https://lstu.fr/)|Raccourcisseur d'URL libre et open source. Permet de personnaliser les noms de liens |
    |[Kitten services](https://www.chatons.org/en/search/by-service?service_type_target_id=125&title=&field_software_target_id=All&field_is_shared_value=All) |Divers raccourcisseurs d'URL libres et open source, basés sur [lstu](https://lstu.fr/), [polr](https://github.com/cydrobolt/polr/) ou [yourls](https://github.com/YOURLS/YOURLS/) |


=== "Pastebins"

    <img src="../../assets/img/editor.svg" alt="Hastebin" width="50px" align="left"></img> Remplacez Pastebin et consorts:

    |Alternative FOSS |Description|
    | -------------------------------- | ------ |
    |[Hastebin](https://www.toptal.com/developers/hastebin/) |Service pastebin en ligne libre et open source |
    |[PicaPaste](https://paste.picasoft.net/)|Service pastebin en ligne libre, open source et chiffré. Basé sur Privatebin, cliquez ici pour [plus d'instances Privatebin ](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Instances-Directory)|
    |[GhostBin](https://ghostbin.com/) |Service pastebin en ligne libre et open source |


=== "Paiement en ligne"

    <img src="../../assets/img/money.svg" alt="Crypto-monnaie" width="50px" align="left"></img> Remplacez Paypal, Google Wallet, Apple Pay et consorts:

    |Alternative FOSS |Description|
    | --------------------------------- | ------ |
    |[LiberaPay](https://fr.liberapay.com/)|Plateforme de dons open source |
    |[Bitcoin](https://bitcoin.org/fr/) |Crypto-monnaie numérique décentralisée, lancée en 2009 |
    |[Ethereum](https://ethereum.org/fr/) |Crypto-monnaie numérique décentralisée, lancée en 2015 |
    |[Litecoin](https://litecoin.org/fr/) |Crypto-monnaie numérique décentralisée, lancée en 2011 |


=== "Transfert d'emails"

    <img src="../../assets/img/simpleemail.svg" alt="Transfert d'emails" width="50px" align="left"></img> Transférez vos courriels de façon anonyme:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Anon Addy](https://anonaddy.com/) |Service de transfert de courriel open source et anonyme |
    |[Simple Login](https://simplelogin.io/) |Service de transfert de courriel open source et anonyme |


=== "Emails jetables"

    <img src="../../assets/img/simpleemail.svg" alt="Emails jetables" width="50px" align="left"></img> Utilisez des adresses électroniques jetables:

    |Alternative FOSS |Description|
    | ------ | ------ |
    |[Spam Gourmet](https://www.spamgourmet.com/index.pl) |Service en ligne gratuit pour créer des adresses électroniques jetables |
    |[Guerrillamail](https://www.guerrillamail.com/) |Service en ligne gratuit pour créer des adresses électroniques jetables |
    |[Anonbox.net](https://anonbox.net/) |Service en ligne gratuit pour créer des adresses électroniques jetables |
    |[Jetable.org](https://jetable.org/) |Service en ligne gratuit pour créer des adresses électroniques jetables |

<br>

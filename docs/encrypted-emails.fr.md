---
template: main.html
title: Comment chiffrer vos emails ?
description: Fournisseurs email respectueux de la vie privée. Protonmail est plus sûr que Gmail ? Protonmail vs Gmail ? Tutanota vs Protomail ? Est-ce que Tutanota est sécurisé ?
---


# Chiffrez vos courriels

!!! level "Dernière mise à jour: mai 2022. Destiné aux débutants et utilisateurs expérimentés. Certaines compétences techniques peuvent être requises."

<div align="center">
<img src="../../assets/img/https5.png" alt="Chiffrement de courriels" width="250px"></img>
</div>

Choisissez un fournisseur de courriel de confiance. Considérez avec attention des aspects comme les fonctionnalités, les technologies de chiffrement, l'emplacement des serveurs — les lois sur la vie privée changent d'un pays à l'autre. Ce chapitre fournit un aperçu (incomplet) de fournisseurs populaires et respectueux de la vie privée. Nous y expliquerons également comment utiliser le chiffrement PGP pour les courriers électroniques.


<br>


<center> <img src="../../assets/img/separator_protonmail.svg" alt="Protonmail" width="150px"></img> </center>

## Protonmail

[Protonmail](https://protonmail.com/fr/) affirme être le plus grand service de courriels sécurisés au monde, protégé par les lois suisses sur la vie privée. L'entreprise a entre autre a été financée par des investissements états-uniens (Charles River Ventures) et par l'union européenne. Le code source est [disponible ici](https://github.com/ProtonMail/). Si les applications de Protonmail sont [open source](https://github.com/ProtonMail/), ce n'est en revanche pas le cas côté serveur.


Au moment d'écrire ces lignes, un compte utilisateur simple offre gratuitement 500 Mo de stockage. Pour 4 à 24 Euro/mois, vous aurez accès à plus d'utilisateurs et plus de stockage, ainsi qu'à pléthore de fonctionnalités: calendrier, contacts, import de courriels, paiement en bitcoins, [VPN](https://gofoss.net/fr/vpn/) et plus encore.

??? warning "Quelques conseils sur le chiffrement"

    <center>

    | Courriels | Chiffrement|
    | ------ | ------ |
    | **Envoyés entre utilisateurs de Protonmail** | Le corps du message et les attachements sont chiffrés de bout en bout. Le sujet et les adresses des émetteur/récepteur ne le sont pas. |
    | **Envoyés de Protonmail vers d'autres fournisseurs** | Le corps du message et les attachements ne sont chiffrés que si l'utilisateur sélectionne l'option `Chiffrez vers l'extérieur`. Faute de quoi seul le chiffrement TLS est appliqué, si le fournisseur de mail du destinataire le permet (ce qui implique également que le fournisseur destinataire peut lire le mail). Dans tous les cas, le sujet et les adresses des émetteur/récepteur ne sont pas chiffrés de bout en bout.|
    | **Reçus par les utilisateurs de Protonmail d'un autre fournisseur** | Le corps du message ainsi que les attachements sont uniquement chiffrés via TLS, si le serveur de l'émetteur le permet. Le sujet et les adresses des émetteurs/récepteur ne sont pas chiffrés de bout en bout. |

    </center>


### Les clients Protonmail

Au delà de l'accès à un [webmail](https://account.protonmail.com/login/), Protonmail offre une application mobile pour Android et iOS. Sur les environnements de bureau, Protonmail s'intègre avec [Thunderbird](https://www.thunderbird.net/fr/) à travers ce qu'on appelle une application Bridge. Cela est néanmoins limité aux comptes payants. Une alternative consiste à utiliser [ElectronMail](https://github.com/vladimiry/ElectronMail), qui est un client libre et open-source pour Protonmail. Gardez néanmoins en tête que ce n'est pas une application officielle. Vous trouverez des informations détaillées ci-dessous.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

         Téléchargez l'application Protonmail depuis le [Google Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=fr&gl=FR) ou [Aurora Store](https://auroraoss.com/). Elle [contient 0 traqueurs et requiert 14 permissions](https://reports.exodus-privacy.eu.org/fr/reports/ch.protonmail.android/latest/). À titre de comparaison: pour Gmail, c'est 1 traqueur et 55 permissions ; pour Outlook, c'est 13 traqueurs et 49 permissions ; et pour Hotmail, c'est 4 traqueurs et 31 permissions.


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

         Téléchargez l'application Protonmail depuis l'[App Store](https://apps.apple.com/fr/app/protonmail-encrypted-email/id979659905/).


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour ElectronMail sous Windows (un compte payant n'est pas nécessaire)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Télécharger ElectronMail | Téléchargez et lancez l'[installateur ElectronMail pour Windows](https://github.com/vladimiry/ElectronMail/releases). |
        | Créer un mot de passe principal | Ouvrez ElectronMail et saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour protéger vos courriels. |
        | Login | Fournissez vos identifiants Protonmail, y compris l'authentification à deux facteurs si celle-ci est active. |
        | Domaine | Choisissez un domaine dans la liste. Il y a même une option `Onion` pour utiliser Tor. Puis cliquez sur `Fermer`. |

        </center>


    ??? tip "Montrez-moi le guide étape par étape pour Thunderbird sous Windows (pour comptes payants uniquement)"

        ### Installer Thunderbird sous Windows

        Allez sur [la page de téléchargement de Thunderbird](https://www.thunderbird.net/fr/) et cliquez sur le bouton `Téléchargement gratuit`. Une fois le programme d'installation téléchargé, cliquez sur le bouton `Exécuter` et suivez l'assistant d'installation.

        ### Installer Protonmail Bridge sous Windows

        Thunderbird s'intègre parfaitement à Protonmail, et veille à ce que vos courriels restent chiffrés lorsqu'ils entrent et sortent de votre ordinateur. Cette tâche est confiée à l'application Bridge, disponible uniquement pour les utilisateurs payants. [Téléchargez Protonmail Bridge pour Windows](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/). Une fois le programme d'installation téléchargé, cliquez sur le bouton "Exécuter" et suivez l'assistant d'installation.

        ### Configurer Protonmail Bridge sous Windows

        Ouvrez l'application Protonmail Bridge tout juste installée et suivez l'assistant de configuration :

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez-vous à votre compte Protonmail. |
        | 2 | Cliquez sur le nom de votre compte, puis sur le bouton `Mailbox configuration` (en anglais). |
        | 3 | Une fenêtre avec le titre `Protonmail Bridge Mailbox Configuration` (en anglais) devrait apparaître. Elle affiche les paramètres IMAP et SMTP, y compris un mot de passe, nécessaire plus tard pour configurer Thunderbird. |

        </center>

        ### Configurer Thunderbird sous Windows

        Finalement, lancez Thunderbird, naviguez vers `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant de configuration :

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Votre nom complet | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse Protonmail. |
        | Mot de passe | Copiez et collez le mot de passe depuis la fenêtre `Protonmail Bridge Mailbox Configuration` (n'entrez pas votre mot de passe Protonmail, cela ne fonctionnera pas). |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configurer manuellement | Cliquez sur le bouton `Configuration manuelle`, et saisissez les paramètres IMAP et SMTP fournis dans la fenêtre `Protonmail Bridge Mailbox Configuration` (pour l'authentification, sélectionnez `Mot de passe normal`). |
        | Re-tester | Cliquez sur le bouton `Re-tester` pour vérifier vos paramètres de connexion. |
        | Configuration avancée | Cliquez sur le bouton `Configuration avancée`. Une nouvelle fenêtre apparaît. Cliquez simplement sur le bouton `OK`, ne modifiez aucun paramètre dans cette fenêtre. |
        | Ajouter une exception de sécurité | Cliquez sur le bouton `Confirmer l'exception de sécurité` dans la fenêtre qui s'ouvre. Cela confirme que votre ordinateur (127.0.0.1) peut exécuter l'application Bridge. Vous devrez peut-être confirmer une deuxième exception de sécurité plus tard, lorsque vous envoyez votre premier courriel. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative pour Thunderbird (3min, pour comptes payants uniquement)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Publié par Protonmail. Les instructions devraient s'appliquer de la même manière sous macOS ou Linux.*

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour ElectronMail sous macOS (un compte payant n'est pas nécessaire)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Télécharger ElectronMail | Téléchargez l'[image disque d'ElectronMail pour macOS](https://github.com/vladimiry/ElectronMail/releases), ouvrez-la et faites glisser l'icône d'ElectronMail au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône ElectronMail vers le dock. |
        | Créer un mot de passe principal | Ouvrez ElectronMail et saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour protéger vos courriels. |
        | Login | Fournissez vos identifiants Protonmail, y compris l'authentification à deux facteurs si celle-ci est active. |
        | Domaine | Choisissez un domaine dans la liste. Il y a même une option `Onion` pour utiliser Tor. Puis cliquez sur `Fermer`.  |

        </center>


    ??? tip "Montrez-moi le guide étape par étape pour Thunderbird sous macOS (pour comptes payants uniquement)"

        ### Installer Thunderbird sous macOS

        Allez sur [la page de téléchargement de Thunderbird](https://www.thunderbird.net/fr/) et cliquez sur le bouton `Téléchargement gratuit`. Une fois le programme d'installation téléchargé, il devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Thunderbird. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Thunderbird au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Thunderbird vers le dock.

        ### Installer Protonmail Bridge sous macOS

        Thunderbird s'intègre parfaitement à Protonmail, et veille à ce que vos courriels restent chiffrés lorsqu'ils entrent et sortent de votre ordinateur. Cette tâche est confiée à l'application Bridge, disponible uniquement pour les utilisateurs payants. [Téléchargez Protonmail Bridge pour macOS](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/). Une fois le programme d'installation téléchargé, il devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Protonmail Bridge. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Protonmail Bridge au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Protonmail Bridge vers le dock.

        ### Configurer Protonmail Bridge sous macOS

        Ouvrez l'application Protonmail Bridge tout juste installée et suivez l'assistant de configuration :

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez-vous à votre compte Protonmail. |
        | 2 | Cliquez sur le nom de votre compte, puis sur le bouton `Mailbox configuration` (en anglais). |
        | 3 | Une fenêtre avec le titre `Protonmail Bridge Mailbox Configuration` (en anglais) devrait apparaître. Elle affiche les paramètres IMAP et SMTP, y compris un mot de passe, nécessaire plus tard pour configurer Thunderbird. |

        </center>

        ### Configurer Thunderbird sous macOS

        Finalement, lancez Thunderbird, naviguez vers `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant de configuration :

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Votre nom complet | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse Protonmail. |
        | Mot de passe | Copiez et collez le mot de passe depuis la fenêtre `Protonmail Bridge Mailbox Configuration` (n'entrez pas votre mot de passe Protonmail, cela ne fonctionnera pas). |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configurer manuellement | Cliquez sur le bouton `Configuration manuelle`, et saisissez les paramètres IMAP et SMTP fournis dans la fenêtre `Protonmail Bridge Mailbox Configuration` (pour l'authentification, sélectionnez `Mot de passe normal`). |
        | Re-tester | Cliquez sur le bouton `Re-tester` pour vérifier vos paramètres de connexion. |
        | Configuration avancée | Cliquez sur le bouton `Configuration avancée`. Une nouvelle fenêtre apparaît. Cliquez simplement sur le bouton `OK`, ne modifiez aucun paramètre dans cette fenêtre. |
        | Ajouter une exception de sécurité | Cliquez sur le bouton `Confirmer l'exception de sécurité` dans la fenêtre qui s'ouvre. Cela confirme que votre ordinateur (127.0.0.1) peut exécuter l'application Bridge. Vous devrez peut-être confirmer une deuxième exception de sécurité plus tard, lorsque vous envoyez votre premier courriel. |

        </center>

    ??? tip "Montrez-moi une vidéo récapitulative pour Thunderbird (3min, pour comptes payants uniquement)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Publié par Protonmail. Les instructions devraient s'appliquer de la même manière sous macOS ou Linux.*

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour ElectronMail sous Ubuntu Linux (un compte payant n'est pas nécessaire)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Télécharger ElectronMail | Téléchargez le dernier [paquet `.deb` d'ElectronMail](https://github.com/vladimiry/ElectronMail/releases). Le fichier devrait s'appeler `electron-mail-X-XX-X-linux-amd64.deb`. Supposons qu'il ait été téléchargé dans le dossier `/home/gofoss/Downloads`. Assurez-vous d'ajuster ces chemins de fichiers en fonction de votre propre configuration. Maintenant ouvrez le terminal avec le raccourci `Ctrl+Alt+T` ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Enfin, exécutez les commandes suivantes:<br><br> `cd /home/gofoss/Downloads` <br>`sudo dpkg -i electron-mail-X-XX-X-linux-amd64.deb` |
        | Créer un mot de passe principal | Ouvrez ElectronMail et saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) pour protéger vos courriels. |
        | Login | Fournissez vos identifiants Protonmail, y compris l'authentification à deux facteurs si celle-ci est active. |
        | Domaine | Choisissez un domaine dans la liste. Il y a même une option `Onion` pour utiliser Tor. Puis cliquez sur `Fermer`.  |

        </center>


    ??? tip "Montrez-moi le guide étape par étape pour Thunderbird sous Ubuntu Linux (pour comptes payants uniquement)"

        ### Installer Thunderbird sous Linux

        Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez la commande suivante pour installer Thunderbird :

        ```bash
        sudo apt install thunderbird
        ```

        ### Installer Protonmail Bridge sous Linux

        Thunderbird s'intègre parfaitement à Protonmail, et veille à ce que vos courriels restent chiffrés lorsqu'ils entrent et sortent de votre ordinateur. Cette tâche est confiée à l'application Bridge, disponible uniquement pour les utilisateurs payants. [Téléchargez Protonmail Bridge pour Linux](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/). Le fichier devrait s'appeler `protonmail-bridge_X.X.X-X_amd64.deb`. Supposons qu'il ait été téléchargé dans le dossier `/home/gofoss/Downloads`. Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes (n'oubliez pas d'ajuster le nom du fichier et le chemin du dossier de téléchargement en conséquence) :

        ```bash
        sudo apt install gdebi
        cd /home/gofoss/Downloads
        sudo gdebi protonmail-bridge_X.X.X-X_amd64.deb
        ```

        ### Configurer Protonmail Bridge sous Linux

        Ouvrez l'application Bridge avec la commande `protonmail-bridge`, ou cliquez sur le bouton `Applications` en haut à gauche, et recherchez `ProtonMail Bridge`. Suivez ensuite l'assistant d'installation :

        <center>

        | Étapes | Description |
        | :------: | ------ |
        | 1 | Connectez-vous à votre compte Protonmail. |
        | 2 | Cliquez sur le nom de votre compte, puis sur le bouton `Mailbox configuration` (en anglais). |
        | 3 | Une fenêtre avec le titre `Protonmail Bridge Mailbox Configuration` (en anglais) devrait apparaître. Elle affiche les paramètres IMAP et SMTP, y compris un mot de passe, nécessaire plus tard pour configurer Thunderbird. |

        </center>

        ### Configurer Thunderbird sous Linux

        Finalement, lancez Thunderbird, naviguez vers `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant de configuration :

        <center>

        | Paramètre | Description |
        | ------ | ------ |
        | Nom | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse Protonmail. |
        | Mot de passe | Copiez et collez le mot de passe depuis la fenêtre `Protonmail Bridge Mailbox Configuration` (n'entrez pas votre mot de passe Protonmail, cela ne fonctionnera pas). |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configurer manuellement | Cliquez sur le bouton `Configuration manuelle`, et saisissez les paramètres IMAP et SMTP fournis dans la fenêtre `Protonmail Bridge Mailbox Configuration` (pour l'authentification, sélectionnez `Mot de passe normal`). |
        | Re-tester | Cliquez sur le bouton `Re-tester` pour vérifier vos paramètres de connexion. |
        | Configuration avancée | Cliquez sur le bouton `Configuration avancée`. Une nouvelle fenêtre apparaît. Cliquez simplement sur le bouton `OK`, ne modifiez aucun paramètre dans cette fenêtre. |
        | Ajouter une exception de sécurité | Cliquez sur le bouton `Confirmer l'exception de sécurité` dans la fenêtre qui s'ouvre. Cela confirme que votre ordinateur (127.0.0.1) peut exécuter l'application Bridge. Vous devrez peut-être confirmer une deuxième exception de sécurité plus tard, lorsque vous envoyez votre premier courriel. |

        </center>


    ??? tip "Montrez-moi une vidéo récapitulative pour Thunderbird (3min, paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Publié par Protonmail. Les instructions devraient s'appliquer de la même manière sous macOS ou Linux.*

        </center>


<br>


<center> <img src="../../assets/img/separator_tutanota.svg" alt="Tutanota" width="150px"></img> </center>

## Tutanota

[Tutanota](https://tutanota.com/fr/) est un service de messagerie électronique sécurisé et "freemium", enregistré en Allemagne. Tout est chiffré de bout en bout. Tutanota utilise sa propre norme de chiffrement et ne prend pas en charge PGP. Si les applications de Tutanota sont [open source](https://github.com/tutao/tutanota/), ce n'est en revanche pas le cas côté serveur.

Au moment d'écrire ces lignes, le compte de base offre gratuitement 1 Go de stockage. Pour environ 1 à 6 Euros/mois, vous avez accès à plus d'utilisateurs et de stockage, ainsi qu'à une pléthore de fonctionnalités : domaines personnalisés, recherche illimitée, calendriers multiples, règles de boîte de réception, label blanc, partage de calendrier, etc. L'importation de courriels et le paiement anonyme ne sont actuellement pas supportés.

### Clients Tutanota

Outre l'accès au [webmail](https://mail.tutanota.com/?r=/login/), Tutanota propose des applications mobiles pour Android et iOS. Pour les environnements de bureau, Tutanota a développé son propre client. Des instructions plus détaillées sont fournies ci-dessous.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

         Téléchargez l'application Tutanota sur [Google Play Store](https://play.google.com/store/apps/details?id=de.tutao.tutanota&hl=fr&gl=FR) ou [Aurora Store](https://auroraoss.com/). Tutanota est également disponible sur [F-Droid](https://f-droid.org/fr/packages/de.tutao.tutanota/). Vous pouvez également visiter la [page de téléchargement](https://tutanota.com/fr/#download) ou le [dépôt Github](https://github.com/tutao/tutanota/releases/) de Tutanota pour télécharger et installer le fichier `.apk`. L'application [contient 0 traqueurs et nécessite 9 permissions](https://reports.exodus-privacy.eu.org/fr/reports/de.tutao.tutanota/latest/). À titre de comparaison : pour Gmail, il y a 1 traqueur et 55 permissions ; pour Outlook, il y a 13 traqueurs et 49 permissions ; et pour Hotmail, il y a 4 traqueurs et 31 permissions.


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

         Téléchargez l'application Tutanota sur l'[App Store](https://apps.apple.com/fr/app/tutanota/id922429609).


=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        Téléchargez [le programme d'installation](https://mail.tutanota.com/desktop/tutanota-desktop-win.exe), puis cliquez sur le bouton `Exécuter` pour suivre l'assistant d'installation.


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        [Téléchargez le programme d'installation](https://mail.tutanota.com/desktop/tutanota-desktop-mac.dmg), qui devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Tutanota. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Tutanota au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Tutanota vers le dock.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        [Télécharger le programme d'installation](https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage), qui devrait s'appeler `tutanota-desktop-linux.AppImage`. Supposons qu'il ait été téléchargé dans le dossier `/home/gofoss/Downloads`. Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes (n'oubliez pas d'ajuster le nom du fichier et le chemin du dossier de téléchargement en conséquence) :

        ```bash
        cd /home/gofoss/Downloads
        chmod +x tutanota-desktop-linux.AppImage
        ```

    ??? tip "Montrez-moi comment épingler Tutanota au dock Ubuntu"

        Ce n'est pas évident, mais le lanceur de Tutanota peut être ajouté au menu des applications d'Ubuntu et épinglé au dock. Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez la commande suivante :

        ```bash
        sudo gedit /usr/share/applications/tutanota.desktop
        ```

        Insérez le contenu suivant dans le fichier nouvellement créé. Assurez-vous de faire pointer le chemin `Exec` vers le dossier contenant l'AppImage téléchargé :

        ```
        #!/usr/bin/env xdg-open
        [Desktop Entry]
        Version=1.0
        Type=Application
        Terminal=false
        Exec=/home/gofoss/Downloads/tutanota-desktop-linux.AppImage
        Name=Tutanota
        ```

        Rendez le fichier exécutable :

        ```bash
        sudo chmod +x /usr/share/applications/tutanota.desktop
        ```

        Déconnectez-vous, puis reconnectez-vous à votre session Ubuntu. Vous devriez maintenant être en mesure de lancer Tutanota à partir du menu des applications et de l'épingler au dock.


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Fournisseurs de courriel" width="150px"></img> </center>

## Autres fournisseurs

=== "Disroot"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[disroot. org](https://disroot.org/fr/services/email) |
    |Prix | Le compte de base est gratuit (1 Go de stockage) ; le stockage supplémentaire coûte 0,15 Euros par Go et par mois. |
    |Fonctionnalités |Plateforme fournissant des services en ligne basés sur les principes de liberté, de confidentialité, de fédération et de décentralisation. Située aux Pays-Bas. Accepte le *bitcoin* et le *faircoin*. Chiffrement complet du disque et du courrier électronique. Application mobile. |
    |Faiblesses | Peut potentiellement déchiffrer les données de ses utilisateurs, car les courriels sont apparemment stockés en texte clair. |


=== "Mailbox"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[mailbox. org](https://mailbox.org/en/) |
    |Prix | 1 Euro/mois, 2 Go de stockage. |
    |Fonctionnalités |Fournisseur de courriel allemand et open source, dont les serveurs sont situés à Berlin. Offre des fonctions de sécurité telles que le chiffrement au repos, PGP, DANE, SPF et DKIM, ainsi que l'authentification à deux facteurs, la recherche plein texte, les calendriers, les carnets d'adresses et les listes de tâches, ainsi que la synchronisation CalDAV et CardDAV. |
    |Faiblesses| Pas de client mobile, nécessite des clients tiers. |


=== "Posteo"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[posteo.de](https://posteo.de/fr/) |
    |Prix | 1 Euro/mois, 2 Go de stockage. |
    |Fonctionnalités |Fournisseur de courriel allemand et open source. Autofinancé, chiffrement au repos, [authentification à deux facteurs](https://gofoss.net/fr/passwords/), calendriers et carnets d'adresses, synchronisation CalDAV et CardDAV. |
    |Faiblesses| Pas de dossier spam, pas de version d'essai ou gratuite. |


=== "Kolab Now"

    | Informations | Description |
    | :-----: | ----- |
    |Site web |[kolabnow.com](https://kolabnow.com/) |
    |Prix | 5 Euros/mois, 2 Go de stockage. |
    |Fonctionnalités |Fournisseur de courriel suisse et open source. Recherche de texte, étiquetage, filtres, carnets d'adresses, calendriers, synchronisation CalDAV et CardDAV. |
    |Faiblesses| Pas de chiffrement de bout en bout, pas de chiffrement au repos. |


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="Transfert d'e-mail" width="150px"></img> </center>

## Période de transition

=== "Transférez vos courriels"

    La transition vers un nouveau compte de messagerie peut prendre un certain temps, similaire au [changement d'appli de messagerie](https://gofoss.net/fr/encrypted-messages/). Il est préférable de garder vos anciens comptes en vie pendant un certain temps pour être sûr de ne rien rater. Il suffit de transférer tout message entrant vers le nouveau compte. Pour plus d'information sur le transfert de courriel, consultez les pages de documentation de [Gmail](https://support.google.com/mail/answer/10957?hl=fr/), [Outlook](https://support.microsoft.com/fr-fr/office/cr%C3%A9er-et-transf%C3%A9rer-des-messages-%C3%A9lectroniques-et-y-r%C3%A9pondre-dans-outlook-sur-le-web-ecafbc06-e812-4b9e-a7af-5074a9c7abd0?redirectsourcepath=%252fen-us%252farticle%252fforward-email-from-office-365-to-another-email-account-1ed4ee1e-74f8-4f53-a174-86b748ff6a0e%252f), [iCloud](https://www.idownloadblog.com/2019/01/28/set-up-icloud-email-forwarding/), [Yahoo](https://www.wikihow.com/Forward-Yahoo-Mail), etc.


=== "Mettez à jour vos abonnements"

    Profitez de cette période de transition pour repérer tout abonnement actif dans vos anciens comptes de messagerie, afin de mettre à jour votre nouvelle adresse électronique !


=== "Informez vos contacts"

    N'oubliez pas de communiquer votre nouvelle adresse électronique à vos contacts personnels et professionnels, à vos banques, à vos assurances, à l'administration fiscale, etc. Vous pouvez également configurer un message de réponse automatique sur votre ancien compte pour informer les gens de votre changement d'adresse.


=== "Résilier vos anciens comptes"

    Au fil du temps, de moins en moins de courriels atterriront dans votre ancienne boîte de réception. Elle finira par devenir inactive. C'est à ce moment-là que vous pourriez envisager de résilier votre ancien compte de messagerie.


<br>

<center> <img src="../../assets/img/separator_protonmail.svg" alt="Warning unprotected private key file" width="150px"></img> </center>

## Comment chiffrer les courriels avec PGP

Vous ne connaissez personne qui utilise Protonmail ou Tutanota ? Ou vous n'appréciez tout bonnement pas ces fournisseurs de services ? Chiffrez vos courriels à l'ancienne, avec [OpenPGP](https://www.openpgp.org/) ! Ce protocole de chiffrement est libre, open source et [compatible avec une multitude de clients](https://www.openpgp.org/software/). Dans cette section, nous vous expliquons comment configurer OpenPGP sur votre téléphone ou ordinateur, comment générer et sauvegarder vos clés PGP, et comment chiffrer et décoder vos courriels.

=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        ### Installer K-9 Mail & OpenKeychain

        <center>

        | Étapes | Description |
        | ----- | ----- |
        | K-9 Mail | K-9 Mail est l'un des nombreux [clients de messagerie Android](https://www.openpgp.org/software/) qui prennent en charge OpenPGP. Téléchargez l'appli depuis le [Play Store](https://play.google.com/store/apps/details?id=com.fsck.k9&hl=fr&gl=FR) ou [F-Droid](https://f-droid.org/fr/packages/com.fsck.k9/). |
        | OpenKeychain | OpenKeychain est une application libre et open source qui s'intègre à K-9 Mail pour fournir des capacités de chiffrement de bout en bout. Téléchargez l'appli depuis le [Play Store](https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain&hl=fr&gl=FR) ou [F-Droid](https://f-droid.org/fr/packages/org.sufficientlysecure.keychain/). |

        </center>


        ### Gérer les clés PGP avec OpenKeychain

        Pour pouvoir envoyer ou lire des courriels chiffrés, vous avez besoin d'une paire de clés unique pour votre adresse électronique :

        * **Clé publique**: Vos contacts utilisent votre clé publique pour chiffrer les messages électroniques qu'ils vous envoient. Vous pouvez donc partager votre clé publique avec quiconque.
        * **Clé privée**: Elle est utilisée pour décoder les courriels chiffrés que vos contacts vous envoient. Gardez votre clé privée pour vous, ne la partagez jamais et ne conservez pas de fichier de clé privée non protégé !

        <center>

        | Étapes | Description |
        | ----- | ----- |
        | Importer des clés PGP existantes |• S'il existe déjà une paire de clés pour votre adresse électronique, n'en créez pas une nouvelle <br>• Lancez OpenKeychain <br>• Allez dans `Menu ‣ Gérer mes clés ‣ Importer la clé d'un fichier` <br>• Si nécessaire, entrez le code de sauvegarde et/ou le mot de passe de la clé |
        | Générer de nouvelles clés PGP |• Si aucune paire de clés n'existe pour votre adresse électronique, créez-en une nouvelle <br>• Lancez OpenKeychain <br>• Allez dans `Menu ‣ Gérer mes clés ‣ Créer ma clé` <br>• Associez un nom et une adresse électronique <br>• Allez dans `Menu ‣ Changer la configuration de la clé` <br>• Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) <br>• Désélectionnez `Publier sur les serveurs de clés` <br>• Tapez sur `Créer la clé` |
        | Sauvegarder des clés PGP |• Si vous perdez vos clés, vous perdez l'accès à tous vos courriels ! <br>• Si vous venez de créer une nouvelle paire de clés, assurez-vous de conserver une sauvegarde <br>• Lancez OpenKeychain <br>• Sélectionnez votre clé <br>• Sélectionnez `Menu ‣ Sauvegarder la clé` <br> • Saisissez le mot de passe de la clé <br>• [Conservez le code de sauvegarde de 45 caractères](https://gofoss.net/fr/passwords/), il vous sera demandé lors du rétablissement des clés ! <br> • Conservez également le fichier de sauvegarde sur votre téléphone ou mieux, dans un [endroit sûr](https://gofoss.net/fr/backups/) |
        | Partager des clés publiques |Avant de pouvoir échanger des courriels chiffrés avec vos contacts, vous devez échanger vos clés publiques respectives. Voici quelques méthodes courantes de partage des clés publiques. <br><br> **Envoyez votre clé publique à vos contacts**: <br>• Lancez OpenKeychain <br>• Sélectionnez votre clé <br>• Tapez sur le symbole de `Partage` et envoyez votre clé <br>• Vos contacts peuvent importer votre clé avec leur application préférée <br><br> **Téléversez votre clé publique sur un serveur de clés**: <br>• Lancez OpenKeychain <br>• Sélectionnez votre clé <br>• Allez dans `Menu ‣ Avancé ‣ Partager ‣ Publier sur le serveur de clés` <br>• Vos contacts peuvent maintenant télécharger la clé publique à partir des serveurs de clés <br>• Vous pouvez également ajouter le lien de téléchargement et l'empreinte de la clé à la signature de vos courriels <br><br> **Importez les clés publiques de vos contacts**: <br>• Demandez à vos contacts de vous envoyer leur clé publique par courriel, messagerie instantanée, etc. <br>• Lancez OpenKeychain <br>• Allez dans `Menu ‣ Gérer mes clés ‣ Importer la clé d'un fichier` <br><br> **Importez les clés publiques de vos contacts depuis un serveur de clés**: <br>• Lancez OpenKeychain <br>• Tapez sur `+ ‣ Recherche de clés` <br>• Recherchez l'adresse électronique, le nom ou l'empreinte digitale de vos contacts <br>• Tapez sur `Importer` |

        </center>


        ### Chiffrer les courriels avec K-9 Mail

        <center>

        | Étapes | Description |
        | ----- | ----- |
        | Configurer le compte et le chiffrement |• Lancez K-9 Mail <br>• Ajoutez votre compte : fournissez votre adresse électronique et votre mot de passe <br>• Configurez les paramètres IMAP/POP3/SMTP, s'ils ne sont pas détectés automatiquement <br>• Allez dans `Menu ‣ Paramètres ‣ Compte ‣ Chiffrement de bout en bout ‣ Activer la prise en charge d'OpenPGP` <br>• Allez dans `Menu ‣ Paramètres ‣ Compte ‣ Chiffrement de bout en bout ‣ Configure end-to-end key` <br>• Sélectionnez votre clé |
        | Chiffrer les courriels |• Lancez K-9 Mail <br>• À partir de la boîte de réception, tapez sur l'icône `stylo` <br>• Rédigez votre message et saisissez l'adresse électronique de votre contact <br>• Si vous avez précédemment importé la clé publique de votre contact, une icône `cadenas` doit apparaître en haut du message <br>• Lorsque vous tapez dessus, l'icône doit devenir verte, ce qui indique que le chiffrement est activé <br>• Appuyez sur `Envoyer` <br><br>*Attention*: l'objet du courriel est transmis en clair ! |
        | Déchiffrer les courriels |• K-9 Mail/OpenKeychain décode automatiquement les messages qui utilisent votre clé publique <br>• Pour cela, le mot de passe de votre clé privée vous sera demandé <br>• L'icône `cadenas` doit apparaître en haut du message déchiffré |

        </center>


        ### Faites un essai !

        Edward est un programme développé par la Free Software Foundation pour tester le chiffrement des courriels. Voici comment cela fonctionne :

        * D'abord, vous partagez votre clé publique avec Edward
        * Edward utilise votre clé publique pour vous envoyer un courriel chiffré
        * Vous seul êtes capable de décoder ce courriel, en utilisant votre clé privée
        * Ensuite, vous récupérez la clé publique d'Edward pour répondre par un courriel chiffré
        * Edward seul est capable de décoder votre message, en utilisant sa clé privée
        * Edward répondra, confirmant que votre précédent courriel était à la fois chiffré et signé

        <center>

        | Étape | Description |
        | ----- | ----- |
        | Envoyer la clé publique à Edward |• Lancez OpenKeychain <br>• Sélectionnez votre clé <br>• Appuyez sur le symbole `Partager` <br>• Sélectionnez l'appli K-9 Mail & rédigez un message adressé à `edward-fr@fsf.org` <br>• Ajoutez un objet et un court message <br>• Appuyez sur `Menu` et assurez-vous que le chiffrement est bien `Désactivé` <br>• Appuyez sur `Envoyer` |
        | Déchiffrer le message d'Edward |• Lancez K-9 Mail et attendez la réponse d'Edward <br>• Celle-ci devrait être chiffrée à l'aide de votre clé publique <br>• Saisissez le mot de passe de votre clé privée pour déchiffrer le courriel <br>• Assurez-vous qu'un `cadenas` orange apparaît en haut du message |
        | Importer la clé publique d'Edward |• Appuyez sur le `cadenas` orange <br>• Appuyez sur `Touche de recherche` <br>• Sélectionnez `Importer` <br>•  Le symbole `cadenas` devrait être devenu vert |
        | Envoyer un courriel chiffré à Edward |• Appuyez sur `Répondre` <br>• Adressez un bref message de réponse à `edward-fr@fsf.org` <br>• Allez dans `Menu` et assurez-vous que le chiffrement est bien `Activé` <br>• Appuyez sur `Envoyer` |
        | Déchiffrer le message d'Edward |• Attendez la réponse d'Edward <br>• Assurez-vous que le `cadenas` vert est toujours affiché <br>• Edward devrait confirmer avoir pu décoder votre message et vérifier votre signature |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Windows, macOS & Linux (Ubuntu)"

        ### Installer Thunderbird 78 (ou plus récent)

        <center>

        | Système d'exploitation | Description |
        | ----- | ----- |
        | Windows | Allez sur [la page de téléchargement de Thunderbird](https://www.thunderbird.net/fr/) et cliquez sur le bouton `Téléchargement gratuit`. Une fois le programme d'installation téléchargé, cliquez sur le bouton `Exécuter` et suivez l'assistant d'installation. |
        | macOS | Allez sur [la page de téléchargement de Thunderbird](https://www.thunderbird.net/fr/) et cliquez sur le bouton `Téléchargement gratuit`. Une fois le programme d'installation téléchargé, il devrait s'ouvrir tout seul et monter un nouveau volume contenant l'application Thunderbird. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône Thunderbird au-dessus du dossier Applications. Pour y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Thunderbird vers le dock. |
        | Linux (Ubuntu) | Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Exécutez la commande suivante pour installer Thunderbird : `sudo apt install thunderbird` |

        </center>


        ### Configurer Thunderbird

        Lancez Thunderbird, allez dans `Menu ‣ Nouveau ‣ Compte courrier existant` et suivez l'assistant d'installation :

        <center>

        | Paramètres | Description |
        | ------ | ------ |
        | Nom | Saisissez le nom que vous voulez faire apparaître aux autres. |
        | Adresse électronique | Saisissez votre adresse électronique. |
        | Mot de passe | Saisissez le mot de passe de votre courriel. |
        | Retenir le mot de passe | Cochez la case `Retenir le mot de passe` pour éviter de ressaisir le mot de passe à chaque lancement de Thunderbird. |
        | Configuration automatique vs. manuelle | Une fois que vous avez saisi vos informations d'identification, cliquez sur le bouton `Continuer`. Thunderbird va essayer de configurer automatiquement les paramètres IMAP/POP3/SMTP. Si cela n'aboutit pas, configurez ces paramètres manuellement (consultez votre fournisseur de messagerie pour d'avantage d'informations). |

        </center>


        ### Gérer les clés PGP avec Thunderbird

        Pour pouvoir envoyer ou lire des courriels chiffrés, vous avez besoin d'une paire de clés unique pour votre adresse électronique :

        * **Clé publique**: Vos contacts utilisent votre clé publique pour chiffrer les messages électroniques qu'ils vous envoient. Vous pouvez donc partager votre clé publique avec quiconque.
        * **Clé privée**: Elle est utilisée pour décoder les courriels chiffrés que vos contacts vous envoient. Gardez votre clé privée pour vous, ne la partagez jamais et ne conservez pas de fichier de clé privée non protégé !

        <center>

        | Étapes | Description |
        | ----- | ----- |
        | Importer des clés PGP existantes |**Importer une clé de sauvegarde**: <br>• S'il existe déjà une paire de clés pour votre adresse électronique, n'en créez pas une nouvelle <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Paramètres des comptes ‣ Chiffrement de bout en bout ‣ Ajouter une clé` <br>• Sélectionnez `Importer une clé OpenPGP existante` et appuyez sur `Continuer` <br>• Cliquez sur `Sélectionner le fichier à emporter` et naviguez jusqu'au fichier clé <br>• Si nécessaire, entrez le code de sauvegarde et/ou le mot de passe de la clé <br><br> **Importer une clé de sauvegarde chiffrée**: <br>• Quelques fois, les clés de sauvegardes sont chiffrées (ex. OpenKeychain) <br>• Elles ne peuvent être importées directement dans Thunderbird <br>• Ouvrez un terminal <br>• Déchiffrez le fichier :<br>`gpg --decrypt backup_YYYY-MM-DD.sec.pgp | gpg --import` <br>• Si nécessaire, entrez le code de sauvegarde et/ou le mot de passe de la clé <br>• Affichez la liste des clés :<br>`gpg --list-keys` <br>• Notez l'identifiant `UID` de la clé à importer <br>• Sauvegardez la clé sous le bon format (replacez `l'UID` en conséquence): <br>`gpg --export-secret-keys UID > decrypted_backup_key.asc` <br>• Si nécessaire, saisissez le mot de passe de la clé <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Paramètres des comptes ‣ Chiffrement de bout en bout ‣ Ajouter une clé` <br>• Sélectionnez `Importer une clé OpenPGP existante` et appuyez sur `Continuer` <br>• Cliquez sur `Sélectionner le fichier à emporter` et naviguez jusqu'au fichier clé `.asc` <br>• Si nécessaire, saisissez le mot de passe de la clé |
        | Générer de nouvelles clés PGP |• Si aucune paire de clés n'existe pour votre adresse électronique, créez-en une nouvelle <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Paramètres des comptes ‣ Chiffrement de bout en bout ‣ Ajouter une clé` <br>• Sélectionnez `Créer une nouvelle clé OpenPGP Key` et appuyez sur `Continuer` <br>• Sélectionnez l'adresse électronique pertinente <br>• Définissez le délai d'expiration entre 1 et 3 ans (il peut être prolongé à tout moment)  <br>• Choisissez `Type de clé: RSA` et `Taille de la clé: 4096`  <br>• Cliquez sur `Générer la clé ‣ Confirmer` |
        | Sauvegarder des clés PGP |• Si vous perdez vos clés, vous perdez l'accès à tous vos courriels ! <br>• Si vous venez de créer une nouvelle paire de clés, assurez-vous de conserver une sauvegarde <br><br>**Sauvegarde de la clé privée:** <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` <br>• Cliquez sur la clé pertinente <br>• Sélectionnez `Fichier ‣ Sauvegarder une ou des clés secrètes dans un fichier` <br>• Saisissez un [code de sauvegarde fort et unique](https://gofoss.net/fr/passwords/) <br>• Conservez le code de sauvegarde dans un endroit sûr, il vous sera demandé lors du rétablissement de votre clé privée ! <br>• Conservez également le fichier de sauvegarde `.asc` de votre clé privée sur votre ordinateur ou mieux, dans un [endroit sûr](https://gofoss.net/fr/backups/) <br><br>**Sauvegarde de la clé publique:** <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` <br>• Faites un clic droit sur la clé pertinente <br>• Sélectionnez `Exporter les clés vers un fichier` <br>• Conservez le fichier de sauvegarde `.asc` de votre clé publique sur votre ordinateur ou mieux, dans un [endroit sûr](https://gofoss.net/fr/backups/) |
        | Partager des clés publiques |Avant de pouvoir échanger des courriels chiffrés avec vos contacts, vous devez échanger vos clés publiques respectives. Voici quelques méthodes courantes de partage des clés publiques. <br><br> **Envoyez votre clé publique à vos contacts**: <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` <br>• Faites un clic droit sur la clé pertinente <br>• Sélectionnez `Envoyer une ou des clés publiques par courriel` <br>• Vos contacts peuvent importer votre clé avec leur application préférée <br><br> **Téléversez votre clé publique sur un serveur de clés**: <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` <br>• Faites un clic droit sur la clé pertinente <br>• Sélectionnez `Exporter les clés vers un fichier` <br>• Allez sur le site de l'[OpenPGP Key Repository](https://keys.openpgp.org/upload) <br>• Sélectionnez le fichier de la clé publique exportée et cliquez sur `Upload` <br>• Vos contacts peuvent maintenant télécharger la clé publique à partir des serveurs de clés <br>• Vous pouvez également ajouter le lien de téléchargement et l'empreinte de la clé à la signature de vos courriels <br><br> **Importez les clés publiques de vos contacts**: <br>• Demandez à vos contacts de vous envoyer leur clé publique par courriel, messagerie instantanée, etc. <br>• Lancez Thunderbird <br>• Si vous avez reçu une clé publique par courriel, cliquez sur le bouton `OpenPGP` pour l'importer <br>• Si vous avez téléchargé une clé publique sur votre ordinateur, allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` et cliquez sur `Fichier ‣ Importer une ou des clés publiques depuis un fichier` <br><br> **Importez les clés publiques de vos contacts depuis un serveur de clés**: <br>• Lancez Thunderbird <br>• Allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` <br>• Cliquez sur `Serveur de clés` <br>• Recherchez l'adresse électronique, le nom ou l'empreinte digitale de vos contacts <br>• Cliquez sur `OK` |

        </center>


        ### Chiffrer les courriels avec Thunderbird

        <center>

        | Étapes | Description |
        | ----- | ----- |
        | Configurer le chiffrement |• Lancez Thunderbird <br>• Allez dans `Menu ‣ Paramètres des comptes ‣ Chiffrement de bout en bout` <br>• Assurez-vous que la bonne clé est associée à votre adresse électronique |
        | Chiffrer les courriels |• Lancez Thunderbird <br>• À partir de la boîte de réception, cliquez sur le bouton `Écrire` <br>• Rédigez votre message et saisissez l'adresse électronique de votre contact <br>• Cliquez sur l'icône déroulante à côté du bouton `Sécurité` <br>• Sélectionnez `Exiger le chiffrement` <br>• Une icône `OpenPGP` doit s'afficher dans le pied de page de la fenêtre <br>• Cliquez sur le bouton `Sécurité` <br>• Si vous avez précédemment importé la clé publique de votre contact,`OK` devrait s'afficher à côté de l'adresse électronique de votre contact <br>• Appuyez sur `Envoyer` une fois prêt |
        | Déchiffrer les courriels |• Thunderbird décode automatiquement les messages qui utilisent votre clé publique <br>• Pour cela, le mot de passe de votre clé privée vous sera demandé <br>• Un symbole `cadenas OpenPGP` avec une coche verte doit apparaître en haut du message déchiffré |

        </center>


        ### Faites un essai !

        Edward est un programme développé par la Free Software Foundation pour tester le chiffrement des courriels. Voici comment cela fonctionne :

        * D'abord, vous partagez votre clé publique avec Edward
        * Edward utilise votre clé publique pour vous envoyer un courriel chiffré
        * Vous seul êtes capable de décoder ce courriel, en utilisant votre clé privée
        * Ensuite, vous récupérez la clé publique d'Edward pour répondre par un courriel chiffré
        * Edward seul est capable de décoder votre message, en utilisant sa clé privée
        * Edward répondra, confirmant que votre précédent courriel était à la fois chiffré et signé

        <center>

        | Étapes | Description |
        | ----- | ----- |
        | Envoyer la clé publique à Edward |• Lancez Thunderbird <br>• Allez dans `Menu ‣ Outils ‣ Gestionnaire de clés OpenPGP` <br>• Faites un clic droit sur la clé pertinente <br>• Sélectionnez `Envoyer une ou des clés publiques par courriel` <br>• Adressez le courriel à `edward-fr@fsf.org` <br>• Ajoutez un objet et un court message <br>• Cliquez sur l'icône déroulante à côté du bouton `Sécurité` <br>• Assurez-vous que `Ne pas chiffrer` est sélectionné <br>• Appuyez sur `Envoyer` |
        | Déchiffrer le message d'Edward |• Attendez la réponse d'Edward <br>• Celle-ci devrait être chiffrée à l'aide de votre clé publique <br>• Assurez-vous qu'un symbole `cadenas OpenPGP` avec une coche verte apparaît en haut du message |
        | Importer la clé publique d'Edward |• Dans la réponse d'Edward, cliquez sur l'adresse électronique `edward-fr@fsf.org` <br>• Sélectionnez `Rechercher la clé OpenPGP` <br>• Sélectionnez `Acceptée (non vérifiée)` <br>•  Cliquez sur `OK` |
        | Envoyer un courriel chiffré à Edward |• Appuyez sur `Répondre` <br>• Adressez un bref message de réponse à `edward-fr@fsf.org` <br>• Cliquez sur l'icône déroulante à côté du bouton `Sécurité` <br>• Assurez-vous que `Exiger le chiffrement` est sélectionné <br>• Cliquez sur le bouton `Sécurité` <br>• `OK` devrait s'afficher à côté de l'adresse électronique d'Edward <br>• Appuyez sur `Envoyer` |
        | Déchiffrer le message d'Edward |• Attendez la réponse d'Edward <br>• Assurez-vous que le symbole `cadenas OpenPGP` avec une coche verte apparaît toujours <br>• Edward devrait confirmer avoir pu décoder votre message et vérifier votre signature |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Protonmail et Tutanota" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions, référez-vous à :

* la [documentation de Protonmail](https://protonmail.com/support/) ou à la [documentation de Thunderbird](https://support.mozilla.org/fr/products/thunderbird). Vous pouvez également demander de l'aide à la [communauté Protonmail](https://teddit.net/r/ProtonMail/) ou à la [communauté Thunderbird](https://support.mozilla.org/fr/questions/new/thunderbird).

* la [documentation de Tutanota](https://tutanota.com/howto/#install-desktop/) ou demandez de l'aide à la [communauté Tutanota](https://teddit.net/r/tutanota/).

* la [documentation de K9 Mail](https://docs.k9mail.app).

<br>

<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/public_key.png" width="600px" alt="Courriels sécurisés"></img>
</div>

<br>
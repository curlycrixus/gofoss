---
template: main.html
title: Les FOSS vous protègent du capitalisme de surveillance
description: Que sont les FOSS ? Avantages des logiciels libres et open source ? Qu'est-ce qu'un modèle de menace ? Différences entre vie privée, anonymat et sécurité ?
---

# Protégez vos données <br> avec des logiciels libres et open source

!!! level "Dernière mise à jour: mars 2022"

[gofoss.net](https://gofoss.net/fr/) veut rendre accessibles à tout.e.s les logiciels libres et open source respectant la vie privée. Le site s'adresse à celles et ceux qui ont peu de temps ou pas très envie de « bidouiller ». Et aux expert·e·s qui veulent en savoir plus sur la vie privée et la sécurité en ligne.

Tout contenu est 100% libre et open source - pas de publicité, pas de suivi, pas de sponsors. Et il devrait intéresser celles et ceux d'entre vous qui:

* possèdent un iPhone, un appareil Android ou un ordinateur tournant sous Windows, macOS ou ChromeOS
* naviguent sur le Web avec Chrome, Safari ou Edge
* vont sur les médias sociaux du type Facebook, Twitter, WhatsApp, Tiktok ou Snapchat


<center><img src="../../assets/img/closed_ecosystem.png" alt="Jardin clos" width="500px"></img></center>

## Qu'est-ce que le capitalisme de surveillance ?

Êtes-vous conscient que toutes les entreprises à l'origine des services mentionnés plus haut font partie de ce que l'on appelle le [capitalisme de surveillance](https://fr.wikipedia.org/wiki/%C3%89conomie_de_la_surveillance) ? En deux mots, il s'agit d'un système économique centré autour de monopoles technologiques qui récoltent les données personnelles afin de maximiser les profits.

Vous n'y voyez pas d'inconvénient ? Et bien, le capitalisme de surveillance menace le cœur même de nos sociétés. Il donne lieu à une surveillance de masse, polarise le débat politique, interfère avec les processus électoraux, pousse à l'uniformisation de la pensée, facilite la censure et favorise l'obsolescence planifiée. Pour affronter ces problèmes, il faut procéder à des changements complets de nos systèmes juridiques et nos conventions sociales.


<center><img src="../../assets/img/foss_ecosystem.png" alt="Logiciel libre et open source" width="500px"></img></center>

## Comment se protéger à l'aide des FOSS ?

En attendant, vous pouvez déjà faire beaucoup pour vous approprier vos données, protéger votre vie privée et revendiquer votre droit à la réparation. Par exemple, commencez à utiliser des logiciels libres et open source, également appelés [FOSS](https://fsfe.org/freesoftware/freesoftware.fr.html). Vous pourrez ainsi naviguer sur Internet, discuter avec vos amis et vos proches, travailler en ligne sans être soumis à la collecte de données par les gouvernements et les entreprises qui enregistrent, monétisent ou censurent vos données. Ou obligé à racheter de nouveaux appareils tous les deux ans.

Il n'existe pas de solution miracle. Le passage aux logiciels libres et open source est un processus. Ce guide franchit une étape à la fois, en commençant par les fondamentaux avant de viser des sujets plus avancées. Nous favorisons les logiciels libres et open source qui font très bien une seule chose, plutôt que des « obésiciels » propriétaires qui prétendent faire un tas de choses et le font mal. Jetez un coup d'œil dans les coulisses et adoptez, rejetez ou personnalisez tout logiciel qui vous semble utile.

<center>

| Étapes | Description |
| ------ | ------ |
|[1. Naviguez sur Internet en toute sécurité](https://gofoss.net/fr/intro-browse-freely/) |• Passez à [Firefox](https://gofoss.net/fr/firefox/) ou [Tor](https://gofoss.net/fr/tor/) <br>• Utilisez un [VPN](https://gofoss.net/fr/vpn/) pour sécuriser davantage votre présence en ligne |
|[2. Gardez vos conversations privées](https://gofoss.net/fr/intro-speak-freely/) |• Utilisez des [messageries chiffrées](https://gofoss.net/fr/encrypted-messages/) <br>• [Chiffrez vos courriels](https://gofoss.net/fr/encrypted-emails/)  |
|[3. Sécurisez vos données](https://gofoss.net/fr/intro-store-safely/) |• Choisissez des [mots de passe et une authentification sécurisés](https://gofoss.net/fr/passwords/) <br>• Créez une [stratégie de sauvegarde 3-2-1](https://gofoss.net/fr/backups/) <br>• [Chiffrez](https://gofoss.net/fr/encrypted-files/) vos données sensibles |
|[4. Restez mobiles et libres](https://gofoss.net/fr/intro-free-your-phone/) |• Utilisez [des applis mobiles libres et open source](https://gofoss.net/fr/foss-apps/) <br>• Libérez votre téléphone de Google & Apple à l'aide de [CalyxOS](https://gofoss.net/fr/calyxos/) & [LineageOS](https://gofoss.net/fr/lineageos/) |
|[5. Libérez vos ordinateurs](https://gofoss.net/fr/intro-free-your-computer/) |• Passez sous [Linux](https://gofoss.net/fr/ubuntu/) <br>• Privilégiez des [applis libres & open source](https://gofoss.net/fr/ubuntu-apps/) |
|[6. Appropriez-vous le nuage](https://gofoss.net/fr/intro-free-your-cloud/) |• Rejoignez le [Fediverse](https://gofoss.net/fr/fediverse/), tournez le dos aux médias sociaux commerciaux <br>• Utilisez des [fournisseurs de nuage alternatifs](https://gofoss.net/fr/cloud-providers/) <br>• Créez votre [propre nuage privé](https://gofoss.net/fr/ubuntu-server/) <br>• Sécurisez votre [serveur](https://gofoss.net/fr/server-hardening-basics/) et vos [services en nuage](https://gofoss.net/fr/secure-domain/) <br>• Accédez ultra-rapidement à votre [stockage en nuage](https://gofoss.net/fr/cloud-storage/) <br>• Organisez et partagez [vos collections photos](https://gofoss.net/fr/photo-gallery/) <br>• Gérez vos [contacts, calendriers et listes de tâches](https://gofoss.net/fr/contacts-calendars-tasks/) <br>• Diffusez vos [films, émissions télés et musique](https://gofoss.net/fr/media-streaming/) en « streaming » <br>• [Créez des sauvegardes de votre serveur](https://gofoss.net/fr/server-backups/)|
|[7. Apprenez, réseautez et participez](https://gofoss.net/fr/nothing-to-hide/) |Il y a beaucoup à dire et à apprendre sur la vie privée en ligne, l'exploitation des données, les bulles de filtres, la surveillance en ligne, la censure et l'obsolescence planifiée. Apprenez-en davantage sur ce projet, impliquez-vous et faites passer le mot. |

</center>

<br>
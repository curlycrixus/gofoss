---
template: main.html
title: gofoss.net. Digital freedom for all
description: Digital freedom for all. The ultimate, free and open source guide to online privacy, data ownership and durable technology.
---

# We believe in a free, open and privacy respecting Internet

<center> <img src="../../assets/img/about.png" alt="gofoss.net" width="400px"></img> </center>

[gofoss.net](https://gofoss.net/) is a free and open source guide to online privacy, data ownership and durable technology. The project was launched in 2020 by a [small team of volunteers](https://gofoss.net/team/), and is 100% non-profit – no ads, no tracking, no sponsored content, no affiliates.

We deplore that many tech companies generate profits at the expense of user privacy. We are increasingly concerned about governments intruding into the private sphere of citizens. And we see an urgent need for action to lower the environmental impact of technology.

[Freedom and privacy are fundamental human rights](https://gofoss.net/nothing-to-hide/). We believe everyone should be able to use their devices in a secure and privacy respecting manner. Browsing the Internet, using social networks, sharing media or collaborating at work — these things need to be possible without someone recording, monetizing or censoring data.

<br>
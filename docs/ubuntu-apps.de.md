---
template: main.html
title: Die 40 beliebtesten Ubuntu-Anwendungen
description: Ubuntu-Anwendungen. Wie man Ubuntu-Anwendungen herunterlädt. Wie man Ubuntu-Anwendungen installiert. Ubuntu-App-Store. Top Ubuntu Anwendungen. Ubuntu Software Center.
---

# Die 40 beliebtesten Ubuntu-Anwendungen

!!! level "Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/laptop.png" alt="Die beliebtesten Ubuntu-Anwendungen" width="300px"></img>
</div>


## Vorinstallierte Anwendungen

<center>

| Ubuntu Anwendungen | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/unzip.svg" alt="Ubuntu-Archiv-Verwalter" width="50px"></img> | [Archiv-Verwalter](https://wiki.gnome.org/Apps/FileRoller) | Komprimiert oder entpackt Archive (.zip, .rar, .tar, usw.). |
| <img src="../../assets/img/transmission.svg" alt="Ubuntu Sicherungskopien" width="50px"></img> | [Sicherungskopien](https://apps.gnome.org/de/app/org.gnome.DejaDup/) | Sichert Dateien an lokalen, entfernten oder Cloud-Speicherorten. Verschlüsselt Backups und plant inkrementelle Sicherungskopien. |
| <img src="../../assets/img/calculator.svg" alt="Ubuntu-Taschenrechner" width="50px"></img> | [Taschenrechner](https://apps.gnome.org/de/app/org.gnome.Calculator/) | Führt arithmetische, wissenschaftliche oder finanzielle Berechnungen durch. |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu-Kalender" width="50px"></img> | [Kalender](https://apps.gnome.org/de/app/org.gnome.Calendar/) | Plant Termine, behaltet den Überblick über Eure Tagesordnung. |
| <img src="../../assets/img/cheese.svg" alt="Ubuntu Cheese" width="50px"></img> | [Cheese](https://apps.gnome.org/de/app/org.gnome.Cheese/) | Nehmt Videos und Fotos mit Eurer Webcam auf. |
| <img src="../../assets/img/baobab.svg" alt="Ubuntu Baobab" width="50px"></img> | [Festplatten-Nutzungsanalyse](https://apps.gnome.org/de/app/org.gnome.baobab/) |Scannt Laufwerke oder bestimmte Verzeichnisse und zeigt die Festplattennutzung an. |
| <img src="../../assets/img/mysql.svg" alt="Ubuntu Disks" width="50px"></img> | [Disks](https://apps.gnome.org/de/app/org.gnome.DiskUtility/) | Prüft, formatiert, partitioniert und konfiguriert Festplatten. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu-Dokumentenscanner" width="50px"></img> | [Dokumentenscanner](https://apps.gnome.org/de/app/simple-scan/) | Scannt, dreht, druckt und speichert Texte und Bilder. |
| <img src="../../assets/img/evince.svg" alt="Ubuntu Evince" width="50px"></img> | [Dokumentenbetrachter](https://apps.gnome.org/de/app/org.gnome.Evince/) | Zeigt Dateiformate wie .pdf, .ps, .xps, .tiff, .djvu, .cbr, .cbz usw. an.|
| <img src="../../assets/img/simplefilemanager.svg" alt="Ubuntu Nautilus" width="50px"></img> | [Datei-Verwalter](https://apps.gnome.org/de/app/org.gnome.Nautilus/) | Durchsucht und verwaltet Dateien. |
| <img src="../../assets/img/firefox.svg" alt="Ubuntu Firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/en-US/firefox/new/) | Surft im Internet. |
| <img src="../../assets/img/simplegallery.svg" alt="Ubuntu eye of gnome" width="50px"></img> | [Bildbetrachter](https://wiki.gnome.org/Apps/EyeOfGnome) | Betrachtet Eure Bildsammlungen, Diashows im Vollbildmodus oder legt Hintergrundbilder fest. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu Libreoffice" width="50px"></img> | [LibreOffice](https://de.libreoffice.org/) | Arbeitet mit Word-Dokumenten, Tabellenkalkulationen, Präsentationen usw. Vollständig kompatibel mit den Microsoft-Formaten (.doc, .xls und .ppt). |
| <img src="../../assets/img/ssh.svg" alt="Ubuntu Passwort-Manager" width="50px"></img> | [Passwörter & Schlüssel](https://apps.gnome.org/de/app/org.gnome.seahorse.Application/) | Verwaltet Verschlüsselungscodes (PGP, SSH).. |
| <img src="../../assets/img/rhythmbox.svg" alt="Ubuntu Rhythmbox" width="50px"></img> | [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox/) | Hört Alben und Podcasts, organisiert Audiodateien, erstellt Wiedergabelisten, und greift auf Online-Medien zu. |
| <img src="../../assets/img/cheese.svg" alt="Ubuntu Bildschirmfoto" width="50px"></img> | [Bildschirmfoto](https://help.gnome.org/users/gnome-help/stable/screen-shot-record.html.de) | Speichert Bilder von Eurem Bildschirm oder einzelnen Fenstern. |
| <img src="../../assets/img/shot_well.svg" alt="Ubuntu Shotwell" width="50px"></img> | [Shotwell](https://wiki.gnome.org/Apps/Shotwell/) | Importiert, organisiert und betrachtet Eure Fotos. |
| <img src="../../assets/img/terminal.svg" alt="Ubuntu Terminal" width="50px"></img> | [Terminal](https://apps.gnome.org/fr/app/org.gnome.Console/) | Führt textbasierte Befehle aus. |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu Text-Editor" width="50px"></img> | [Text-Editor](https://apps.gnome.org/fr/app/org.gnome.TextEditor/) | Bearbeitet Textdateien. |
| <img src="../../assets/img/simpleemail.svg" alt="Ubuntu Thunderbird" width="50px"></img> | [Thunderbird](https://www.thunderbird.net/de/) | Greift auf Eure E-Mails, Kalender, Adressbücher und Aufgabenlisten zu. |
| <img src="../../assets/img/totem.svg" alt="Ubuntu Totem" width="50px"></img> | [Videos](https://apps.gnome.org/de/app/org.gnome.Totem/) | Schaut Eure Filme an. |

</center>


## Andere beliebte Anwendungen

Vorherige Kapitel haben sich mit der Installation von beliebten Anwendungen wie [Firefox](https://gofoss.net/de/firefox/), [Tor](https://gofoss.net/de/tor/), [Protonmail & Tutanota](https://gofoss.net/de/encrypted-emails/), [Signal](https://gofoss.net/de/encrypted-messages/), [Keepass](https://gofoss.net/de/passwords/) oder [Veracrypt & Cryptomator](https://gofoss.net/de/encrypted-files/) befasst. Darüber hinaus sind noch viele weitere Anwendungen für Ubuntu verfügbar. Nachfolgend findet Ihr eine Auswahl beliebter und nützlicher Anwendungen. Es gibt zwei einfache Möglichkeiten, diese zu installieren:

* **Das Software Center**, das im [vorherigen Kapitel über Ubuntu](https://gofoss.net/de/ubuntu/#software-center/) vorgestellt wurde. Es handelt sich um einen App-Store mit dem Ihr Programme suchen, installieren, aktualisieren oder entfernen könnt. Klickt hierzu in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Software`. Sucht anschließend nach Anwendungen, die Euch interessieren. Um eine Anwendung zu installieren, reicht es auf `Installieren` zu klicken und das Administrator- bzw. Root-Passwort einzugeben.

* **Das Terminal** ist eine weitere Möglichkeit, um Programme zu installieren, zu aktualisieren oder zu entfernen. Öffnet hierzu das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt in der oberen Menüleiste auf `Aktivitäten` und sucht nach dem Eintrag `Terminal`. Der folgende Terminal-Befehl installiert zum Beispiel Firefox: `sudo apt install firefox`. Voilà!

<center>

| Ubuntu Anwendungen | |Beschreibung |
| :------: | ------ | ------ |
| <img src="../../assets/img/simplecalendar.svg" alt="Ubuntu Mousepad" width="50px"></img> | [Mousepad](https://github.com/codebrainz/mousepad/) | `Text-Editor`: einfach, leicht zu bedienen und schnell. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install mousepad` |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Ubuntu Onlyoffice" width="50px"></img> | [OnlyOffice](https://www.onlyoffice.com/de/) | `Office-Paket`: freies und quelloffenes Programm zur Bearbeitung von Word-Dokumenten, Tabellenkalkulationen, Präsentationen, usw. Vollständig kompatibel mit Microsoft-Formaten (.doc, .xls und .ppt). Installiert das Programm über Ubuntus Software Center oder folgt den Anweisungen von [OnlyOffice](https://helpcenter.onlyoffice.com/installation/desktop-install-ubuntu.aspx?_ga=2.123053453.764533964.1595236576-1157782750.1587541027/).|
| <img src="../../assets/img/maps.svg" alt="GNOME Maps" width="50px"></img> | [Gnome Maps](https://apps.gnome.org/de/app/org.gnome.Maps/) | `Maps`: freie und quelloffene Karten. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install gnome-maps`|
| <img src="../../assets/img/vlc.svg" alt="Ubuntu VLC" width="50px"></img> | [VLC](https://www.videolan.org/vlc/index.de.html) | `Videos`: freier und quelloffener Multimedia-Player. Unterstützt eine Vielzahl an Dateiformaten und Streaming-Protokollen. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install vlc`|
| <img src="../../assets/img/totem.svg" alt="Ubuntu MPlayer" width="50px"></img> | [MPlayer](http://mplayerhq.hu/) | `Videos`: freier und quelloffener Multimedia-Player. Unterstützt die gängigsten Dateiformate. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install mplayer`|
| <img src="../../assets/img/ffmpeg.svg" alt="Ubuntu Ffmpeg" width="50px"></img> | [Ffmpeg](https://ffmpeg.org/) | `Medienbearbeitung`: quelloffenes Tool zum Konvertieren und Ändern verschiedener Dateiformate wie .mp4, .avi, .mov, .mkv, .mp3, .wav, usw. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install ffmpeg`|
| <img src="../../assets/img/gimp.svg" alt="Ubuntu Gimp" width="50px"></img> | [Gimp](https://www.gimp.org/) | `Bildbearbeitung`: freie und quelloffene Alternative zu Adobe Photoshop, mit zahlreichen Add-Ins für Zusatzfunktionen. Unterstützt Dateiformate wie .bmp, .gif, .jpeg, .mng, .pcx, .pdf, .png, .ps, .psd, .svg, .tiff, .tga, .xpm, usw. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install gimp gimp-data`|
| <img src="../../assets/img/gimp.svg" alt="Ubuntu Krita" width="50px"></img> | [Krita](https://krita.org/en/) | `Bildbearbeitung`: freies und quelloffenes Malprogramm für Konzeptkunst, Textur- und Mattemalerei, Illustrationen und Comics. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `$ sudo add-apt-repository ppa:kritalime/ppa && sudo apt update && sudo apt install krita`|
| <img src="../../assets/img/audacity.svg" alt="Ubuntu Tenacity" width="50px"></img> | [Audacity](https://www.audacityteam.org/) | `Audiobearbeitung`: freies und quelloffenes Programm zum Aufnehmen, Sampeln, Bearbeiten, Konvertieren und Analysieren von Audiodateien. *Warnhinweis*: [Diskussionen um das Hinzufügen von Telemetrie zu Audacity](https://github.com/audacity/audacity/pull/835/) haben zu einem Fork namens [Tenacity](https://tenacityaudio.org/) geführt, der gegebenenfalls einen Blick wert ist. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install audacity`|
| <img src="../../assets/img/openshot.svg" alt="Ubuntu Openshot" width="50px"></img> | [OpenShot](https://www.openshot.org/de/) | `Videobearbeitung`: freier und quelloffener Videoeditor zum Skalieren, Trimmen, Schneiden, Zoomen, Überblenden und Hinzufügen von Effekten zu Videodateien. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo add-apt-repository ppa:openshot.developers/ppa && sudo apt update && sudo apt install openshot-qt` |
| <img src="../../assets/img/openshot.svg" alt="Ubuntu Blender" width="50px"></img> | [Blender](https://www.blender.org/) | `Videobearbeitung`: freie und quelloffene 3D-Entwicklungssuite, einschließlich Modellierung, Animation, Simulation, Rendering, Bewegungsverfolgung und Videobearbeitung. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install snapd && sudo snap install blender --classic`|
| <img src="../../assets/img/games.svg" alt="Ubuntu Steam" width="50px"></img> | [Steam](https://store.steampowered.com/linux/) | `Videospiele`: Verteilungsplattform für Videospiele. Zockt Tausende von Top-Spielen auf Eurem Linux-Rechner. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install steam-installer`|
| <img src="../../assets/img/empathy.svg" alt="Ubuntu Empathy" width="50px"></img> | [Empathy](https://wiki.gnome.org/action/show/Attic/Empathy?action=show&redirect=Apps%2FEmpathy) | `Chat`: Text-, Sprach- und Videonachrichten. Proprietäre Protokolle werden unterstützt: Google Talk, MSN, IRC, AIM, Facebook, Yahoo, ICQ, usw. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install empathy` |
| <img src="../../assets/img/pidgin.svg" alt="Ubuntu Pidgin" width="50px"></img> | [Pidgin](https://pidgin.im/) | `Chat`: freier und quelloffener Messaging-Client. Unterstützt viele Protokolle, einschließlich MSN, AIM, Yahoo, Jabber, IRC, IRQ, usw. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install pidgin`|
| <img src="../../assets/img/money.svg" alt="Ubuntu Gnucash" width="50px"></img> | [GNUcash](https://gnucash.org/index.phtml?lang=de_DE) | `Finanzen`: freies und quelloffenes Buchhaltungsprogramm für Privatpersonen und Kleinunternehmen. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install gnucash`|
| <img src="../../assets/img/deluge.svg" alt="Ubuntu Deluge" width="50px"></img> | [Deluge](https://www.deluge-torrent.org/) | `Torrent`: freier und ressourcenarmer Torrent-Client. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install deluge`|
| <img src="../../assets/img/transmission.svg" alt="Ubuntu Transmission" width="50px"></img> | [Transmission](https://transmissionbt.com/) | `Torrent`: übertragt und empfangt Dateien über das BitTorrent-Protokoll. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install transmission` |
| <img src="../../assets/img/simplefilemanager.svg" alt="Ubuntu Filezilla" width="50px"></img> | [FileZilla](https://filezilla-project.org/) | `FTP`: freier FTP-Client. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install filezilla`|
| <img src="../../assets/img/unzip.svg" alt="Ubuntu rar" width="50px"></img> | [Rar, Unrar and Unzip](https://itsfoss.com/use-rar-ubuntu-linux/) | `Dateikomprimierung`: drei kleine Programme zum Komprimieren oder Extrahieren von Archiven (.zip, .rar, .tar, usw.). Installiert die Programme über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install rar unrar unzip`|
| <img src="../../assets/img/bleachbit.svg" alt="Ubuntu Bleachbit" width="50px"></img> | [Bleachbit](https://www.bleachbit.org/) | `Wartung`: quelloffenes Programm zur Entrümplung Eurer Rechner. Leert den Cache, beseitigt Cookies, löscht den Internetverlauf, vernichtet temporäre Dateien, löscht Protokolle und entsorgt Datenmüll. Installiert das Programm über Ubuntus Software Center oder mit dem folgenden Terminalbefehl:<br> `sudo apt install bleachbit`|

</center>


<br>

<center> <img src="../../assets/img/separator_games.svg" alt="Ubuntu Spiele" width="150px"></img> </center>

## Videospiele

Linux ist bei weitem nicht perfekt. Zuweilen stößt man auf Einschränkungen. Spiele zum Beispiel sind oft nicht für Linux verfügbar. Zum Glück gibt es einige Ausweichmöglichkeiten.

### Steam

Steam bietet Tausende Top-Spiele für Linux, sofern die Mindestanforderungen erfüllt werden: 1 GHz Pentium 4 oder AMD Opteron Prozessor, 512 MB Arbeitsspeicher, 5 GB Festplattenspeicher, eine Internetverbindung und eine [Grafikkarte mit den neuesten installierten Treibern](https://gofoss.net/de/ubuntu/#graphic-drivers/). Um Steam zu installieren reicht es in der oberen Menüleiste auf `Aktivitäten` zu klicken, das Ubuntu Software Centre aufzurufen, nach dem Eintrag `Steam` zu suchen und auf `Installieren` zu klicken.

### Lutris

[Lutris](https://lutris.net/) ist eine freie und [quelloffenen](https://github.com/lutris/) Spieleplattform für Linux. Sie ermöglicht die mühelose Einrichtung von Tausenden von Spielen und sorgt für ein reibungsloses Gaming-Erlebnis unter Linux. Stellt vor der Installation von Lutris sicher, dass Ihr [die neuesten Grafiktreiber](https://gofoss.net/de/ubuntu/#graphic-drivers/) installiert habt. Folgt anschließend den nachfolgenden Anweisungen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T` oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`. Führt die folgenden Befehle aus, um [Wine](https://www.winehq.org/) zu installieren. Es handelt sich dabei um ein Programm, mit dem Ihr Windows-Anwendungen auf Eurem Linux-Rechner ausführen könnt:

    ```bash
    sudo dpkg --add-architecture i386
    wget -nc https://dl.winehq.org/wine-builds/winehq.key
    sudo apt-key add winehq.key
    sudo apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ jammy main'
    sudo apt update
    sudo apt install --install-recommends winehq-stable
    ```

    Installiert nun Lutris:

    ```bash
    sudo add-apt-repository ppa:lutris-team/lutris
    sudo apt update
    sudo apt install lutris
    ```

    Öffnet Lutris nach der erfolgreichen Installation, [registriert Euch](https://lutris.net/user/register/) oder meldet Euch an, [stöbert durch den Katalog](https://lutris.net/games/) und installiert neue Spiele.


<br>

<center> <img src="../../assets/img/separator_virtualbox.svg" alt="Ubuntu Virtualbox" width="150px"></img> </center>

## Windows-Programme

Ab und an kommt es vor, dass ein bestimmtes Programm partout nicht auf Eurem Ubuntu-Rechner laufen will. Eine Ausführung des Programms unter Windows könnte das Problem lösen. [VirtualBox](https://www.virtualbox.org/) ist eine geeignete Lösung, da es sozusagen einen Windows-Rechner innerhalb Eures Ubuntu-Rechners simuliert. Mehr Details zum Thema findet Ihr nachfolgend.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### VirtualBox installieren

    Stellt sicher, dass Euer Ubuntu-Rechner die Mindestanforderungen erfüllt: 4 GB Arbeitsspeicher, 20 GB freier Festplattenspeicher. Klickt anschließend auf `Aktivitäten` in der oberen Menüleiste, öffnet das Ubuntu Software Centre, sucht nach `VirtualBox` und klickt auf `Installieren`. Alternativ könnt Ihr auch einfach ein Terminal öffnen und folgenden Befehl ausführen: `sudo apt install virtualbox`.


    ### VirtualBox konfigurieren

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Windows herunterladen | Ladet eine [Windows .iso-Datei](https://www.microsoft.com/de-de/software-download/) herunter. Wählt die 32- oder 64-Bit-Version, je nach Architektur Eures Rechners. |
    | Eine virtuelle Maschine erstellen | Startet VirtualBox und klickt auf die `Neu`-Schaltfläche. |
    | Name und Betriebssystem | Gebt Eurer virtuellen Maschine (VM) einen Namen, zum Beispiel `Windows VM`. Wählt ebenfalls das Betriebssystem (`Microsoft Windows`) sowie die Version aus, zum Beispiel `Windows 10 (64-bit)`. |
    | Speichergröße | Wählt aus, wie viel Arbeitsspeicher Windows zugewiesen werden soll. 2 GB sind empfohlen, 3-4 GB wären noch besser. |
    | Festplatte | Wählt den Eintrag `Festplatte erzeugen`, um eine virtuelle Festplatte hinzuzufügen. |
    | Dateityp der Festplatte | Wählt `VDI` als Dateityp für die Festplatte. |
    | Art der Speicherung | Wählt den Eintrag `dynamisch alloziert`, um die Größe der Festplatte festzulegen. |
    | Dateiname und Größe | Gebt an, wo die virtuelle Festplatte erstellt werden soll. Weist Windows genügend Speicherplatz zu, mindestens 32 GB sind empfohlen. |
    | Wahl des optischen Mediums | Klickt vom Hauptbildschirm aus auf `Start`, um die virtuelle Windows-Maschine zu starten. Klickt im erscheinenden Dialogfenster auf das Ordner-Symbol, um ein virtuelles optisches Medium auszuwählen. Ruft den Pfad auf, an dem sich die heruntergeladene Windows-`.iso`-Datei befindet. Klickt auf `Öffnen`, `Auswählen` und `Start`. |

    </center>

    ### Windows in VirtualBox installieren

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Windows installieren | Nach der Boot-Phase wird der Windows-Installationsassistent angezeigt. Folgt den Anweisungen auf dem Bildschirm: Wählt eine Sprache und ein Tastaturlayout aus, gebt einen Windows-Schlüssel an, akzeptiert die Nutzungsbedingungen und so weiter. |
    | Mit Windows arbeiten | Meldet Euch nach der erfolgreichen Installation in Eurer ersten Windows-Sitzung in der virtuellen Maschine an. Von nun an könnt Ihr Windows starten, indem Ihr auf dem VirtualBox-Hauptbildschirm auf `Start` klickt. |

    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Ubuntu Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr:

* in [Ubuntus Dokumentation](https://help.ubuntu.com/), [Ubuntus Tutorials](https://ubuntu.com/tutorials) oder [Ubuntus Wiki](https://wiki.ubuntu.com/). Ihr könnt auch gerne die [beginnerfreundliche Ubuntu-Gemeinschaft](https://ubuntuusers.de/) um Unterstützung bitten.

* auf [Steams Hilfeseite](https://help.steampowered.com/de/). Ihr könnt auch gerne die [Lutris Reddit-Gemeinschaft](https://teddit.net/r/lutris/) um Unterstützung bitten.


Weitere Vorschläge für freie und quelloffene Programme findet Ihr hier:

* [Free Software Directory](https://directory.fsf.org/wiki/Main_Page)

* [Open Source Software Directory](https://opensourcesoftwaredirectory.com)

<br>

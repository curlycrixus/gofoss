---
template: main.html
title: The project team
description: gofoss.net is a volunteer-run and not-for-profit project. Meet the team. Georg Jerska. Lenina Helmholtz. Tom Parsons.
---

# The gofoss.net team

## Georg Jerska

<img src="../../assets/img/georg.png" alt="Georg" width="150px" align="left"></img> Georg works in the performing arts. He spent part of his career in Germany, and has a special interest in protecting the privacy of citizens.

<br><br>

## Lenina Helmholtz

<img src="../../assets/img/lenina.png" alt="Georg" width="150px" align="left"></img> Lenina has a technical background and gives college lectures. She is always curious about new FOSS tools.

<br><br>

## Tom Parsons

<img src="../../assets/img/tom.png" alt="Georg" width="150px" align="left"></img> Tom is very social and believes in a better internet. He hopes to contribute good work to the project.

<br><br>
---
template: main.html
title: FOSS-Apps auf entgoogelten Telefonen
description: Entgoogeln. Was ist LineageOS? Was ist CalyxOS? Was ist GrapheneOS? Was ist microG? Wie kann ich CalyxOS installieren? Was ist F-droid ?
---

# Befreit Eure Telefone von Google und Apple

Dieses Kapitel beschäftigt sich mit:

* [Tracker-freien, quelloffenen mobilen Apps (FOSS-Apps)](https://gofoss.net/de/foss-apps/) mit möglichst wenigen Zugangsberechtigungen
* alternativen mobilen Betriebssystems wie [CalyxOS](https://gofoss.net/de/calyxos/) oder [LineageOS](https://gofoss.net/de/lineageos/)


## Tracker-freie Apps

<center>
<html>
<embed src="../../assets/echarts/app_stats.html" style="width: 100%; height:520px">
</html>
</center>

Mobile Apps von Google, Facebook, Samsung oder Microsoft werden milliardenfach heruntergeladen. Und werden oft ohne die ausdrückliche Zustimmung der NutzerInnen auf Telefonen vorinstalliert.

Dies wirft ernsthafte Bedenken hinsichtlich des Datenschutzes auf. Viele dieser Apps verlangen Zugriff auf Euren Standort, Euer Mikrofon, Eure Kamera, Eure Kontakte und so weiter. Sie enthalten zudem Tracker, um Informationen über Euch zu sammeln. Im Schnitt enthalten die [Top 50 Android-Apps](https://en.wikipedia.org/wiki/List_of_most-downloaded_Google_Play_applications) (über 100 Milliarden Downloads) **2 bis 3 Tracker** und erfordern **36 Zugangsberechtigungen**, wie oben dargestellt.

??? info "Hier erfahrt Ihr mehr über Tracker"

    Ein Tracker ist ein Programm, das Informationen darüber sammelt, wie Apps und Smartphones genutzt werden. Dies umfasst verschiedenste Aspekte, wie z.B. die Fähigkeit Euren Standort zu verfolgen, Eure Kontakte zu durchforsten, Eure Kreditkartennummern zu erfassen oder den Inhalt Eurer Zwischenablage auszulesen. Prüft daher vor der Installation sorgfältig, welche Tracker und Berechtigungen eine App nutzt. Das geht z.B. mit [εxodus](https://reports.exodus-privacy.eu.org/de/).

    <center>

    | Tracker-Typ | Beschreibung |
    | ------ | ------ |
    | Analytik | Sammelt Daten, z.B. welche Webseiten Ihr besucht, für wie lange, welche Abschnitte der Webseite, usw. |
    | Profilerstellung | Erstellt ein virtuelles Profil, mit dem Euer Browserverlauf, installierte Apps usw. ausgewertet werden. |
    | Identifizierung | Bestimmt Eure Identität anhand Eures Namen, Eurer Pseudonyme, Eures Standorts usw. |
    | Werbung | Bestimmt wer gerade ein Gerät benutzt, um gezielte Werbung zu schalten. |
    | Standort | Bestimmt Euren Standort mit Hilfe von GPS, Mobilfunkmasten, WiFi-Netzwerken in Eurer Nähe, usw. |
    | Absturzberichte | Informiert Entwickler über das Auftreten von Problemen mit einer App. |

    </center>

<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Degoogelt Eure Telefone" width="150px"></img> </center>

## Google- und Apple-freie Telefone

<center>
<html>
<embed src="../../assets/echarts/mobileos_stats.html" style="width: 100%; height:520px">
</html>
</center>

Im Jahr 2020 zählt die Welt [3.5 Milliarden Smartphone-NutzerInnen](https://www.statista.com/statistics/330695/number-of-smartphone-users-worldwide/). Das ist fast die Hälfte der Weltbevölkerung. Dreiviertel dieser Telefone werden von Googles Android betrieben, der Rest von Apples iOS. Und [3.3 Milliarden Menschen](https://www.statista.com/statistics/264810/number-of-monthly-active-facebook-users-worldwide/) nutzen mindestens eines von Facebooks Kernprodukten — Facebook, WhatsApp, Instagram oder Messenger.

Der Datenschutz stand bei diesen Unternehmen noch nie im Vordergrund. Ganz im Gegenteil, Smartphones "teilen" ständig persönliche Nutzerdaten mit Google, Facebook oder Apple, die dann an meistbietende Datenmakler veräußert werden: Werbeagenturen, Strafverfolgungsbehörden, politische Aktionsgruppen, Finanzinstitute, Versicherungsunternehmen usw. Android-Telefone zum Beispiel [senden täglich 12 Megabytes an Google](https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf). Selbst im Ruhezustand übermitteln sie ihren Standort 14 Mal pro Stunde an Google. iPhones senden ebenso 6 Megabytes an Google und 1 Megabyte an Apple — jeden Tag.


??? question "Ihr wollt datenschutzfreundlich telefonieren? Hier sind ein paar Optionen"

    <center>

    | Option | Beschreibung |
    | ------ | ------ |
    |1. Werft Euer Smartphone weg | Datenschutz stand bei der Entwicklung von Handys noch nie im Vordergrund. Wenn Euch Datenschutz wirklich am Herzen liegt, solltet Ihr auf Smartphones verzichten. Für die meisten von uns ist diese Option allerdings zu radikal. |
    |2. Wählt ein freies und quelloffenes mobiles Betriebssystem | Die nächstbeste Lösung wäre, ein Telefon mit quelloffener Soft- und idealerweise Hardware zu finden. Es gibt einige Projekte wie das [Librem 5](https://puri.sm/products/librem-5/), das [Pinephone](https://www.pine64.org/pinephone/), [Postmarket OS](https://postmarketos.org/), [Ubuntu Touch](https://ubuntu-touch.io/) oder [Sailfish OS](https://sailfishos.org/) (letzteres ist nicht vollständig quelloffen). Auch hier gilt: diese innovativen Lösungen sind nicht für jede/b geeignet. |
    |3. Befreit Android von Google | Prinzipiell ist Android ein quelloffenes Betriebssystem. Es von Google-Apps und proprietärer Software zu entledigen ist daher derzeit die beste Kompromisslösung. Dies kann z.B. durch den Wechsel zu CalyxOS oder LineageOS für microG erreicht werden. |

    </center>

??? question "Was ist die beste Wahl: LineageOS für microG, CalyxOS, oder etwas anderes?"

    Das hängt von Eurem Handymodell ab, sowie von Eurem [Bedrohungsmodell](https://www.eff.org/files/2015/11/24/3mod_threat-modeling-ssd_9-3-15.pdf). LineageOS für microG läuft auf zahlreichen Handymodellen, allerdings auf Kosten einer erhöhten Sicherheit. CalyxOS ist in dieser Hinsicht sicherer, läuft aber auf weniger Handymodellen. Hier ein Überblick:

    <center>

    | Merkmale | LineageOS für microG | CalyxOS |
    | ------ | ------ | ------ |
    |Google-frei^(1)^ |99% |99% |
    |Benutzerfreundlichkeit^(2)^ |Hoch |Hoch |
    |Leistungsfähigkeit^(3)^ |Hoch |Hoch |
    |Automatische Sicherheitsupdates^(4)^ |Eingeschränkt |Ja |
    |Verifizierter Bootvorgang^(5)^ |Nein |Ja |
    |Signaturfälschung^(6)^ |Ja |Ja (wenn microG genutzt wird) |
    |Unterstützte Geräte |[Für Hunderte von Geräten verfügbar](https://wiki.lineageos.org/devices/) |[Nur für Googles Pixel-Geräte](https://calyxos.org/get/) |
    |F-Droid |Installiert |Installiert |
    |Installation^(7)^ |Relativ kompliziert |Relativ einfach |
    |Dokumentation |Gut |Ausreichend |

    </center>

    *(1)* LineageOS für microG und CalyxOS sind zwar nicht vollständig quelloffen, verzichten aber auf Google-Apps und beschränken die Menge an proprietärem Code auf ein striktes Minimum.

    *(2)* Unter Berücksichtigung von Aspekten wie App-Kompatibilität, Push-Benachrichtigungen, Kartennavigation usw. Während die meisten Apps problemlos unter LineageOS für microG oder CalyxOS laufen, sind [einige Apps nicht kompatibel](https://github.com/microg/GmsCore/wiki/Implementation-Status). Auch die Nutzung kostenpflichtiger Apps ohne Googles Play Store kann sich etwas schwierig gestalten.

    *(3)* Unter Berücksichtigung von Aspekten wie Batterie-, Speicher- und CPU-Nutzung. microG benötigt lediglich 4 MB, verglichen mit den über 700 MB für das gesamte Google App-Paket.

    *(4)* CalyxOS erhält automatisch Sicherheitsupdates. Bei LineageOS müssen Sicherheitsupdates manuell aufgespielt werden: diese enthalten regelmäßig Android-Patches, Korrekturen für den Kernel oder für Gerätetreiber sind weniger konsistent.

    *(5)* CalyxOS kann den sogenannten Bootloader sperren. Dadurch bleibt die Fähigkeit zum verifizierten Booten erhalten, im Einklang mit Androids Sicherheitsmodell. LineageOS hingegen läuft mit einem entsperrten Bootloader. Dies ist an sich eine Sicherheitslücke, die theoretisch ausgenutzt werden kann: von einem Angreifer mit physischem Zugriff auf das Telefon zum Beispiel, oder von hartnäckigen Schadprogrammen, die einen Neustart überdauern können.

    *(6)* microG verwendet eine Signaturfälschung, die gewisse Sicherheitslücken aufweisen kann. CalyxOS implementiert ebenfalls eine Signaturfälschung, allerdings auf sehr viel restriktivere Weise.

    *(7)* Die Einrichtung von LineageOS für microG kann ziemlich komplex und langwierig erscheinen. ErstanwenderInnen können gegebenenfalls auf unvorhergesehene Schwierigkeiten stoßen.

<br>
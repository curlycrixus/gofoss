---
template: main.html
title: Comment installer Ubuntu
description: Qu'est-ce qu'Ubuntu ? Ubuntu est meilleur que Windows ? Quelle est la signification d'Ubuntu ? Qu'est-ce qu'Ubuntu LTS ? Comment installer Ubuntu ?
---

# Guide du débutant pour Ubuntu

!!! level "Dernière mise à jour: mai 2022. Destiné aux débutants et utilisateurs expérimentés. Certaines compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/ubuntu_desktop.png" alt="Comment installer Ubuntu" width="600px"></img>
</div>


## Qu'est-ce qu'Ubuntu LTS ?

Ubuntu est un mot africain qui signifie *humanité* ou *fraternité*. Il qualifie également une philosophie qui encourage l'égalité des communautés, la répartition des richesses et l'appréciation de l'unicité de chacun·e.

Dans cet esprit, la société britannique Canonical Ltd développe et publie le [système d'exploitation éponyme Ubuntu](https://ubuntu.com/), une distribution Linux open source basée sur Debian. Ubuntu est publié tous les six mois, avec des versions de support à long terme tous les deux ans (en anglais, « long-term support » ou LTS). Au moment de la rédaction de ce document, la dernière version LTS est Ubuntu 22.04, qui est prise en charge jusqu'en avril 2027.

Ubuntu se décline en de nombreuses versions officielles et non officielles, chacune proposant des environnements de bureau différents avec un aspect et une convivialité qui leur sont propres : Ubuntu GNOME (version par défaut), Ubuntu Mate, Xubuntu, Lubuntu, Kubuntu, elementaryOS, Linux Mint, Pop!_OS, Zorin OS et ainsi de suite. Dans ce chapitre, nous allons passer en revue la version officielle d'Ubuntu, avec l'environnement de bureau GNOME.


## Ubuntu Live USB & VirtualBox

La façon la plus simple de découvrir [Ubuntu](https://ubuntu.com/) est de l'installer aux côtés de votre système d'exploitation existant, par exemple en utilisant une clé USB « Live » ou un programme comme VirtualBox. S'il devait s'avérer qu'Ubuntu n'est pas à votre goût, vous pourrez ainsi facilement revenir à Windows ou macOS.

=== "USB ou DVD « Live »"

    Pour essayer Ubuntu rapidement, lancez-le à partir d'un DVD ou d'une clé USB « Live ». Plus de détails ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Téléchargez Ubuntu |Téléchargez la dernière [version d'Ubuntu bénéficiant d'un support à long terme](https://ubuntu.com/download/desktop) (en anglais, « long-term support ou LTS »). Au moment d'écrire ces lignes, la dernière version LTS était Ubuntu 22.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
        | Gravez un DVD ou préparez une clé USB |Utilisez un ordinateur [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) ou [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) pour graver le fichier `.iso` téléchargé sur un DVD. Vous pouvez également utiliser un ordinateur [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) ou [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) pour créer une clé USB « Live ». |
        | Redémarrez |Insérez le DVD ou la clé USB « Live » dans l'ordinateur sur lequel vous souhaitez tester Ubuntu, puis redémarrez-le. La plupart des ordinateurs démarrent automatiquement depuis le DVD ou la clé USB. Si ce n'est pas le cas, essayez d'appuyer plusieurs fois sur `F12`, `ESC`, `F2` ou `F10` lorsque l'ordinateur démarre. Cela devrait vous donner accès au menu de démarrage, où vous pourrez alors sélectionner le DVD ou le lecteur USB comme périphérique de démarrage. |
        | Testez Ubuntu |Une fois démarré à partir du DVD ou de la clé USB, sélectionnez `Try or Install Ubuntu` et appuyez sur la touche `ENTRÉE`. Attendez qu'Ubuntu charge à partir du périphérique de démarrage, cela peut prendre un certain temps. Lorsque l'écran de bienvenue s'affiche, choisissez l'entrée `Essayer Ubuntu`. |

        </center>


=== "VirtualBox"

    [VirtualBox](https://www.virtualbox.org/) est une autre solution pour découvrir Ubuntu tout en conservant votre système d'exploitation existant. Ce programme crée des machines virtuelles qui se lancent sur votre ordinateur Windows ou macOS. Vous trouverez des instructions plus détaillées ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape"

        ### Installer VirtualBox

        === "Windows"

            Assurez-vous que votre ordinateur répond aux exigences minimales : Processeur double cœur de 2 GHz ou plus, 2 Go de mémoire vive ou plus, 25 Go d'espace disque disponible. Téléchargez et exécutez le dernier [paquet VirtualBox pour les hôtes Windows](https://www.virtualbox.org/wiki/Downloads). Ouvrez le fichier `.exe` téléchargé et suivez le guide d'installation.

        === "macOS"

            Assurez-vous que votre ordinateur répond aux exigences minimales : Processeur double cœur de 2 GHz ou plus, 2 Go de mémoire vive ou plus, 25 Go d'espace disque disponible. Téléchargez le [paquet VirtualBox pour les hôtes OS X](https://www.virtualbox.org/wiki/Downloads). Ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône VirtualBox vers le dossier d'application. Pour y accéder facilement, ouvrez le dossier d'applications et faites glisser l'icône VirtualBox vers le dock.


        ### Configurer VirtualBox

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Téléchargez Ubuntu | Téléchargez la dernière [version d'Ubuntu Desktop bénéficiant d'un support à long terme](https://ubuntu.com/download/desktop) (en anglais, « long-term support ou LTS »). Au moment d'écrire ces lignes, la dernière version LTS était Ubuntu 22.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
        | Créez une machine virtuelle | Lancez VirtualBox et cliquez sur le bouton `Nouvelle`. |
        | Nom et système d'exploitation | Donnez un nom à votre machine virtuelle (en anglais, « virtual machine ou VM »), par exemple `Ubuntu`. Sélectionnez également le système d'exploitation (`Linux`) et la version, par exemple `Ubuntu (64-bit)`. |
        | Taille de la mémoire | Choisissez combien de mémoire vive sera allouée à Ubuntu. 2 Go sont recommandés, 3-4 Go sont encore mieux. |
        | Disque dur |  Sélectionnez `Créer un disque dur virtuel maintenant` pour ajouter un disque dur virtuel. |
        | Type de fichier de disque dur | Choisissez le format `VDI` comme type de fichier. |
        | Stockage sur disque dur physique | Choisissez `Dynamiquement alloué` comme taille du disque dur virtuel. |
        | Emplacement du fichier et taille | Choisissez l'endroit où créer le disque dur virtuel. Décidez également combien d'espace disque doit être alloué à Ubuntu. 10 Go sont recommandés, plus est encore mieux. |
        | Sélecteur de disque optique |De retour sur l'écran principal, cliquez sur `Démarrer` pour lancer la machine virtuelle `Ubuntu`. Dans la fenêtre qui s'ouvre, cliquez sur l'icône pour choisir un fichier de disque optique virtuel. Cliquez sur `Ajouter` et cherchez le fichier `.iso` Ubuntu LTS téléchargé précédemment. Cliquez sur `Ouvrir`, `Choisir` et `Démarrer`. |
        | Démarrage | Dans le menu de démarrage, sélectionnez `Try or Install Ubuntu` et appuyez sur la touche `ENTRÉE`. L'assistant d'installation d'Ubuntu apparaîtra après la phase de démarrage. |

        </center>


        ### Installer Ubuntu dans VirtualBox

        <center>

        | Instructions | Description |
        | ------ | ------ |
        |Bienvenue |Sélectionnez une langue et cliquez sur `Installer Ubuntu`. |
        |Disposition du clavier |Sélectionnez une disposition de clavier. |
        |Mises à jour et autres logiciels |Choisissez entre une installation `normale` ou `minimale`, en fonction du nombre d'applications que vous souhaitez installer dès le départ.<br><br> En option, cochez la case `Télécharger les mises à jour pendant l'installation de Ubuntu` pour accélérer la configuration après l'installation, et la case `Installer des logiciels tiers` pour bénéficier de pilotes (propriétaires) pour les graphismes, le WiFi, les fichiers multimédias, etc. Cliquez ensuite sur `Continuer`. |
        |Type d'installation & chiffrement |Cet écran permet de choisir si l'on veut supprimer le système d'exploitation existant et le remplacer par Ubuntu, ou si l'on veut installer Ubuntu en parallèle du système d'exploitation existant (ce que l'on appelle le « dual boot »). <br><br> Comme nous installons Ubuntu dans VirtualBox, aucun autre système d'exploitation n'est présent. Choisissez simplement `Effacer le disque et installer Ubuntu`, cliquez sur `Fonctions avancées` et sélectionnez `Utiliser LVM pour la nouvelle installation de Ubuntu` ainsi que `Chiffrer la nouvelle installation de Ubuntu pour la sécurité`. Cliquez ensuite sur `OK` et `Installer maintenant`. |
        |Choisir une clé de sécurité |Choisissez une [clé de sécurité forte et unique](https://gofoss.net/fr/passwords/). Elle sera nécessaire pour déchiffrer le disque à chaque démarrage de votre ordinateur. Cochez également la case `Écraser l'espace disque vide` pour plus de sécurité. Cliquez sur `Installer maintenant`. Confirmez la fenêtre de dialogue en cliquant sur `Continuer`. <br><br> *Attention*: Si vous perdez la clé de sécurité, toutes vos données seront perdues. Conservez-la en lieu sûr ! |
        |Où êtes-vous ? |Choisissez un fuseau horaire et un emplacement. Cliquez sur `Continuer`. |
        |Qui êtes-vous ? |Saisissez des informations de connexion, telles qu'un nom d'utilisateur et un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Sélectionnez `Demander mon mot de passe pour ouvrir une session`. Cliquez sur `Continuer`.|
        |Redémarrez et connectez-vous pour la première fois |Cliquez sur `Redémarrer maintenant` après l'installation. La machine virtuelle va redémarrer. Vous pouvez maintenant vous connecter à votre première session Ubuntu en saisissant la clé de sécurité et le mot de passe corrects. |
        |Découvrez Ubuntu| Parcourez la configuration initiale et commencez à explorer Ubuntu. Fermez la session une fois que vous avez terminé. A partir de maintenant, vous pouvez lancer Ubuntu en cliquant sur le bouton `Démarrer` sur l'écran principal de VirtualBox. |

        </center>


    ??? tip "Montrez-moi une vidéo récapitulative (5min)"

        <center>
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ef6ada2-116b-416d-8d53-10a817d95827" frameborder="0" allowfullscreen></iframe>
        </center>


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Installation d'Ubuntu" width="150px"></img> </center>

## Installer Ubuntu

Vous commencez à apprécier Ubuntu ? Vous voulez l'installer définitivement sur votre ordinateur ? Génial ! Il vous suffit d'effectuer quelques vérifications préliminaires et de suivre les instructions d'installation ci-dessous.


??? tip "Montrez-moi la liste des contrôles préliminaires"

    <center>

    | Vérifications | Description |
    | ------ | ------ |
    | Mon ordinateur est-il compatible avec Linux ? |• [Testez Ubuntu](https://gofoss.net/fr/ubuntu/#ubuntu-live-usb-virtualbox) en utilisant une clé USB « Live » ou VirtualBox <br>• Consultez la [base de données de compatibilité](https://ubuntu.com/certified) <br>• [Demandez](https://search.disroot.org/) sur Internet <br>• Achetez un ordinateur compatible avec Linux, par exemple sur [Linux Préinstallé](https://linuxpréinstallé.com/) ou [Ministry of Freedom](https://minifree.org/) |
    | Mon ordinateur répond-il aux exigences minimales ? |• Processeur double cœur 2 GHz <br>• 4 Go de mémoire vive (RAM) <br>• 25 Go d'espace de stockage libre (Ubuntu occupe environ 5 Go, prévoyez au moins 20 Go pour vos données) |
    | Mon ordinateur est-il branché ? | Si vous installez Ubuntu sur un appareil mobile tel qu'un ordinateur portable, assurez-vous qu'il est bien branché. |
    | Le support d'installation est-il accessible ? | Vérifiez si votre ordinateur dispose d'un lecteur de DVD ou d'un port USB libre. |
    | Mon ordinateur est-il connecté à l'Internet ? | Vérifiez si la connexion Internet est opérationnelle. |
    | Ai-je sauvegardé mes données ? | [Sauvegardez vos données](https://gofoss.net/fr/backups/), car il existe un risque (faible mais réel) de perte de données pendant le processus d'installation ! |
    | Ai-je téléchargé la dernière version d'Ubuntu ? | Téléchargez la dernière [version d'Ubuntu Desktop bénéficiant d'un support à long terme](https://ubuntu.com/download/desktop) (en anglais, « long-term support ou LTS »), qui est pris en charge pendant 5 ans, y compris les mises à jour de sécurité et de maintenance. Au moment d'écrire ces lignes, la dernière version LTS était Ubuntu 22.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
    | Ai-je préparé un périphérique de démarrage ? | Utilisez un ordinateur [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) ou [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) pour graver le fichier `.iso` téléchargé sur un DVD. Vous pouvez également utiliser un ordinateur [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) ou [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) pour créer une clé USB « Live ». |

    </center>


??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Instruction | Description |
    | ------ | ------ |
    |Démarrage |Insérez le DVD ou la clé USB « Live » dans l'ordinateur, puis redémarrez-le. La plupart des ordinateurs démarrent alors automatiquement depuis le DVD ou la clé USB. Si ce n'est pas le cas, essayez d'appuyer plusieurs fois sur `F12`, `ESC`, `F2` ou `F10` lorsque l'ordinateur démarre. Cela devrait vous donner accès au menu de démarrage, où vous pourrez alors sélectionner le DVD ou le lecteur USB comme périphérique de démarrage. <br><br>Une fois démarré à partir du DVD ou de la clé USB, sélectionnez `Try or Install Ubuntu` et appuyez sur la touche `ENTRÉE`. Attendez qu'Ubuntu charge à partir du périphérique de démarrage, cela peut prendre un certain temps. |
    |Bienvenue |L'assistant d'installation s'affiche. Sélectionnez une langue et cliquez sur `Installer Ubuntu`. |
    |Disposition du clavier |Sélectionnez une disposition de clavier. |
    |Mises à jour et autres logiciels |Choisissez entre une installation `normale` ou `minimale`, en fonction du nombre d'applications que vous souhaitez installer dès le départ. En option, cochez la case `Télécharger les mises à jour pendant l'installation de Ubuntu` pour accélérer la configuration après l'installation, et la case `Installer des logiciels tiers` pour bénéficier de pilotes (propriétaires) pour les graphismes, le WiFi, les fichiers multimédias, etc. Cliquez ensuite sur `Continuer`. |
    |Type d'installation & chiffrement |Choisissez si vous voulez : <br>• Supprimer le système d'exploitation existant et le remplacez par Ubuntu. *Attention* : ceci effacera toutes les données de votre disque dur ! Assurez-vous d'avoir [sauvegardé vos données](https://gofoss.net/fr/backups/) ! <br>• Ou installer Ubuntu en parallèle du système d'exploitation existant (ce que l'on appelle le « dual boot »). Cela ne devrait avoir aucune incidence sur la configuration existante de votre ordinateur. Veillez tout de même à [sauvegarder vos données](https://gofoss.net/fr/backups/), on ne sait jamais... <br><br> Pour chiffrer Ubuntu, cliquez sur `Fonctions avancées` et sélectionnez : <br>• `Utiliser LVM pour la nouvelle installation de Ubuntu` <br>• `Chiffrer la nouvelle installation de Ubuntu pour la sécurité` <br><br>Puis, cliquez sur `OK` et `Installer maintenant`. |
    |Choisir une clé de sécurité |Choisissez une [clé de sécurité forte et unique](https://gofoss.net/fr/passwords/). Elle sera nécessaire pour déchiffrer le disque à chaque démarrage de votre ordinateur. Cochez également la case `Écraser l'espace disque vide` pour plus de sécurité. Cliquez sur `Installer maintenant`. Confirmez la fenêtre de dialogue en cliquant sur `Continuer`. <br><br> *Attention*: Si vous perdez la clé de sécurité, toutes vos données seront perdues. Conservez-la en lieu sûr ! |
    |Où êtes-vous ? |Choisissez un fuseau horaire et un emplacement. Cliquez sur `Continuer`. |
    |Qui êtes-vous ? |Saisissez des informations de connexion, telles qu'un nom d'utilisateur et un [mot de passe fort et unique](https://gofoss.net/fr/passwords/). Sélectionnez `Demander mon mot de passe pour ouvrir une session`. Cliquez sur `Continuer`.|
    |Redémarrez et connectez-vous pour la première fois |Et voilà. Attendez que l'installation se termine, retirez la clé USB et cliquez sur `Redémarrer maintenant` lorsque vous y êtes invité. Après le redémarrage, connectez-vous à Ubuntu avec votre clé de sécurité et votre mot de passe. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>

    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252dab-255f-42c7-885e-711d9a24da85" frameborder="0" allowfullscreen></iframe>

    </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Ubuntu desktop" width="150px"></img> </center>

## Le bureau GNOME

Une fois connecté, vous verrez apparaître le [« Desktop GNOME »](https://www.gnome.org/). Ci-dessous un aperçu rapide des différents éléments du bureau GNOME.

=== "Barre supérieure"

    <center> <img src="../../assets/img/gnome_top_bar.png" alt="GNOME top bar" width="700px"></img> </center>

    La barre supérieure permet un accès rapide aux éléments suivants :

    * **Activités**: cliquez sur le bouton `Activités` en haut à gauche de l'écran pour accéder aux fenêtres et espaces de travail ouverts, ou rechercher des applications, des fichiers et des dossiers. Vous pouvez également faire glisser trois doigts vers le haut sur votre pavé tactile pour accéder à la vue d'ensemble des activités
    * **Heure, calendrier et rendez-vous**: affichez l'heure et la date actuelles, le calendrier et la liste de vos rendez-vous
    * **Propriétés du système**: gérez les propriétés du système telles que le son, la luminosité de l'écran, les connexions réseau, la gestion de l'énergie, l'état du microphone pendant les appels, la fermeture de session, etc.

    Vous pouvez également ajouter un **indicateur de pourcentage de batterie** via `Paramètres ‣ Énergie ‣ Bouton de mise en veille et extinction`.


=== "Dock"

    <center> <img src="../../assets/img/gnome_dock.png" alt="GNOME dock" width="300px"></img> </center>

    Par défaut, le « dock » apparaît sur le côté gauche du bureau :

    * **Applications épinglées**: accédez rapidement à vos applications préférées. Pour épingler une application, il suffit de faire un clic droit sur l'icône correspondante dans le tiroir d'application ou dans le « dock » et de sélectionner `Ajouter aux favoris`. Notez que les icônes du « dock » peuvent être réorganisées à votre guise
    * **Applications en cours d'exécution**: accédez aux applications en cours d'exécution. Elles sont affichées dans la moitié inférieure du dock.
    * **Périphériques & Corbeille**: accédez à vos périphériques et à la corbeille

    **Affinez l'apparence du « dock »** sous `Paramètres ‣ Apparence ‣ Dock`: réduisez la taille du « dock » en désactivant le *mode panneau*, déplacez le « dock » vers un autre emplacement sur le bureau, masquez automatiquement le « dock », modifiez la taille des icônes ou masquez les périphériques et la corbeille.

    Pour **activer la minimisation sur clic**, qui vous permet de cliquer sur l'icône d'une application dans le dock pour minimiser ou restaurer la fenêtre correspondante, il suffit d'exécuter la commande suivante dans le terminal :

    ```bash
    gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'
    ```


=== "Tiroir d'application"

    <center> <img src="../../assets/img/gnome_app_launcher.png" alt="GNOME app launcher" width="700px"></img> </center>

    Le tiroir d'application permet d'accéder à toutes les [applications installées](https://gofoss.net/fr/ubuntu-apps/):

    * **Ouvrir le tiroir d'application**: cliquez sur l'icône à neuf points en bas à gauche du bureau, ou effectuez deux balayages consécutifs vers le haut à trois doigts sur votre pavé tactile
    * **Parcourir le tiroir d'application**: faites défiler horizontalement vos applications à l'aide de la souris ou d'un balayage gauche/droite à deux doigts sur votre pavé tactile
    * **Réorganiser les icônes**: glissez et de déposez les icônes au sein du tiroir d'application pour les réorganiser à votre convenance


=== "Bureau"

    <center> <img src="../../assets/img/gnome_desktop.png" alt="GNOME desktop" width="700px"></img> </center>

    De nombreux aspects du bureau GNOME peuvent être personnalisés dans le menu `Paramètres ‣ Apparence` :

    * **Apparence des fenêtres**: choisissez un thème clair ou sombre pour l'ensemble du système, et choisissez parmi 10 couleurs d'accentuation
    * **Icônes**: réglez la taille ou la position des icônes sur le bureau, et affichez ou masquez le dossier personnel
    * **Fichiers et dossiers**: ouvrez le gestionnaire de fichiers pour glisser et déposer des fichiers ou des dossiers sur le bureau

    Vous pouvez affiner les activités multi-tâches via le menu `Paramètres ‣ Multi-tâches` :

    * **Coin actif**: touchez le coin supérieur gauche pour ouvrir la vue d'ensemble des activités
    * **Bords de l'écran actifs**: poussez les fenêtres contre les bords haut, gauche et droite pour les redimensionner
    * **Espace de travail**: choisissez entre un nombre dynamique ou fixe d'espaces de travail. Pour changer d'espace de travail, accédez à la vue d'ensemble des activités ou au tiroir d'applications comme décrit précédemment, ou faites glisser trois doigts vers la droite ou la gauche sur votre pavé tactile

    Vous pouvez également enregistrer **des images et captures d'écran** en appuyant sur le bouton `IMPRESSION D'ÉCRAN`.

    Enfin, vous pouvez **désactiver les animations** via `Paramètres ‣ Accessibilité ‣ Image`.


=== "Terminal"

    <center> <img src="../../assets/img/gnome_terminal.png" alt="GNOME terminal" width="700px"></img> </center>

    Le terminal est une interface permettant d'exécuter des commandes en mode texte. Il est parfois appelé « shell », console, ligne de commande ou « prompt ». De nombreux nouveaux utilisateurs sont rebutés par le terminal, qui est souvent associé à d'obscurs activités de piratage ou de codage. En réalité, l'utilisation du terminal n'est pas si compliquée et s'avère souvent beaucoup plus rapide que la navigation dans l'interface graphique. Ouvrez le terminal avec le raccourci clavier `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` situé sur la barre supérieure et cherchez `Terminal`. Voici quelques lectures supplémentaires pour devenir un ninja du terminal :

    * [Aide-mémoire avec les commandes de terminal les plus utilisées](https://speicher.systemausfall.org/f/28456ff12e7d4b43870e/)
    * [Autre aide-mémoire avec les commandes de terminal les plus utilisées](https://speicher.systemausfall.org/f/3ec10028a0cb482383bd/)
    * [Encore un autre aide-mémoire avec les commandes de terminal les plus utilisées](https://speicher.systemausfall.org/f/85700c3358444ed3942a/)
    * [Quelques commandes utiles pour le terminal Ubuntu](https://speicher.systemausfall.org/f/f305c26061d4472b8f9c/)
    * [Livre complet sur la ligne de commande Linux](https://speicher.systemausfall.org/f/949ad740507741a5a2b2/)


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="GNOME software center" width="150px"></img> </center>

## Supprimer Snap & installer le gestionnaire de logiciels GNOME

Depuis Ubuntu 20.04, le gestionnaire de logiciels classique a été remplacé par « Snap », une nouvelle technologie permettant de fournir des applications regroupées dans un seul fichier. Si vous préférez revenir au gestionnaire de logiciels GNOME, suivez les instructions détaillées ci-dessous.

??? tip "Comment supprimer Snap d'Ubuntu 22.04 LTS"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Puis exécutez les commandes suivantes afin d'arrêter les services « Snap » :

    ```bash
    sudo systemctl disable snapd.service
    sudo systemctl disable snapd.socket
    sudo systemctl disable snapd.seeded.service
    ```

    Listez tous les paquets « Snap » installés sur votre système :

    ```bash
    snap list
    ```

    Le terminal devrait afficher quelque chose comme :

    ```
    Name                        Version             Rev     Tracking            Publisher       Notes
    bare                        1.0                 5       latest/stable       canonical       base
    core20                      20220318            1405    latest/stable       canonical       base
    firefox                     99.0.1-1            1232    latest/stable/...   mozilla         -
    gnome-3-38-2004             0#git.1f9014a       99      latest/stable/...   canonical       -
    gtk-common-themes           0.1-79-ga83e90c     1534    latest/stable/...   canonical       -
    snap-store                  41.3-59-gf884f48    575     latest/stable/...   canonical       -
    snapd                       2.54.4              15177   latest/stable       canonical       snapd
    snapd-desktop-integration   0.1                 10      latest/stable/...   canonical       -
    ```

    Ensuite, retirez chaque paquet « Snap » comme suit :

    ```bash
    sudo snap remove --purge firefox
    sudo snap remove --purge snap-store
    sudo snap remove --purge gtk-common-themes
    sudo snap remove --purge gnome-3-38-2004
    sudo snap remove --purge snapd-desktop-integration
    sudo snap remove --purge core20
    sudo snap remove --purge bare
    ```

    Supprimez tous les fichiers restants :

    ```bash
    sudo rm -rf /var/cache/snapd/
    sudo rm -rf ~/snap
    ```

    Supprimer complètement « Snap » d'Ubuntu 22.04 LTS :

    ```bash
    sudo apt autoremove --purge snapd
    ```

    En option, si vous voulez empêcher Ubuntu d'installer les paquets « Snap » à l'avenir, ouvrez un fichier de configuration :

    ```bash
    sudo gedit /etc/apt/preferences.d/nosnap.pref
    ```

    Ajoutez les lignes suivantes, puis enregistrez le fichier :

    ```bash
    # This file prevents snapd from being installed by apt
    Package: snapd
    Pin: release a=*
    Pin-Priority: -10
    ```

??? tip "Comment installer le gestionnaire de logiciel GNOME dans Ubuntu 22.04 LTS"

    Exécutez la commande suivante pour installer le gestionnaire de logiciel GNOME :

    ```bash
    sudo apt update
    sudo apt install gnome-software
    ```

??? tip "Comment réinstaller Firefox dans Ubuntu 22.04 LTS après avoir supprimé Snap ?"

    Après avoir supprimé « Snap », l'exécution de la commande `sudo apt install firefox` peut retourner le message d'erreur `firefox : PreDepends : snapd but it is not installable`. Pour réinstaller [Firefox](https://gofoss.net/fr/firefox/), ouvrez un fichier de configuration :

    ```bash
    sudo gedit /etc/apt/preferences.d/firefox-nosnap.pref
    ```

    Ajoutez les lignes suivantes, puis enregistrez le fichier :

    ```bash
    # This file enables re-installing Firefox after removing snapd
    Package: firefox*
    Pin: release o=Ubuntu*
    Pin-Priority: -1
    ```

    Ajoutez le dépôt de l'équipe Mozilla :

    ```bash
    sudo add-apt-repository ppa:mozillateam/ppa
    sudo apt update
    ```

    Enfin, installez Firefox :

    ```bash
    sudo apt install firefox
    ```


??? tip "Comment prendre en charge les paquets « Snap » et « Flatpak » dans le gestionnaire de logiciel GNOME ?"

    Tout d'abord, vérifiez que vous n'empêchez pas Ubuntu d'installer les paquets « Snap ». Si vous avez créé le fichier `/etc/apt/preferences.d/nosnap.pref` lors d'une des étapes précédentes, supprimez-le avant de continuer :

    ```bash
    sudo rm /etc/apt/preferences.d/nosnap.pref
    ```

    Si vous voulez que le gestionnaire de logiciel GNOME prenne en charge les paquets « Snap » et « Flatpak », exécutez les commandes suivantes :

    ```bash
    sudo apt install gnome-software-plugin-snap
    sudo apt install gnome-software-plugin-flatpak
    ```

<br>

<center> <img src="../../assets/img/separator_anon.svg" alt="Ubuntu Privacy" width="150px"></img> </center>

## Paramètres de confidentialité d'Ubuntu

Sous `Paramètres ‣ Confidentialité`, les utilisateurs peuvent affiner les aspects liés à la vie privée tels que les contrôles de connectivité, les services de localisation, l'historique, l'accès aux appareils externes, le verrouillage de l'écran ou les rapports de diagnostic. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Paramètres de confidentialité | Description |
    | ------ | ------ |
    | Connectivité | Désactivez la vérification de la connectivité pour éviter les fuites de données si votre communication réseau est surveillée. |
    | Services de localisation | Désactivez les services de localisation pour empêcher les applications d'accéder à votre position. |
    | Thunderbolt | Ubuntu est capable de reconnaître les périphériques branchés sur les ports Thunderbolt de l'ordinateur, tels que les docks ou les cartes graphiques externes. Si vous n'utilisez pas ou ne souhaitez pas utiliser cette fonctionnalité, désactivez-la. |
    | Historique des fichiers & corbeille | Désactivez l'historique des fichiers pour empêcher les applications d'accéder à une trace des fichiers que vous avez utilisés. Vous pouvez également supprimer automatiquement le contenu de la corbeille et les fichiers temporaires pour protéger vos informations sensibles. |
    | Écran | Activez le verrouillage automatique de l'écran pour empêcher des tiers d'accéder à votre ordinateur en votre absence. |
    | Diagnostics | Désactivez le diagnostic pour empêcher l'envoi de rapports d'erreur à Canonical. |

    </center>


<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="Ubuntu Gnome Tweaks" width="150px"></img> </center>

## GNOME Tweaks & Extensions

Comme détaillé précédemment, Ubuntu 22.04 et le bureau GNOME sont hautement personnalisables : fonds d'écran, notifications, thèmes clairs et sombres, couleurs d'accentuation, multitâche, profils énergétique, etc. [GNOME Tweaks](https://wiki.gnome.org/Apps/Tweaks/) pousse la personnalisation encore plus loin : fond d'écran de verrouillage, emplacement des boutons de contrôle des fenêtres, applications de démarrage, etc. Les [extensions GNOME](https://extensions.gnome.org/), quant à elles, ajoutent des fonctionnalités telles que la météo, la vitesse du réseau, le découpage du bureau, etc. Vous trouverez des instructions plus détaillées ci-dessous.


??? tip "Montrez-moi le guide étape par étape"

    Pour installer GNOME Tweaks & GNOME Extensions, ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes :

    ```bash
    sudo apt install gnome-tweaks
    sudo apt install gnome-shell-extension-manager
    ```

    Vous pouvez également cliquer sur le bouton `Activités` en haut à gauche et chercher `Logiciels`. Recherchez maintenant `Gnome Tweaks` ainsi que `Extension Manager` et cliquez sur `Installer`.


<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Rapports de bogues sur Ubuntu" width="150px"></img> </center>

## Rapports de bogues

[Apport](https://wiki.ubuntu.com/Apport/) est le système de rapport de bogues d'Ubuntu. Il intercepte les plantages et archive les rapports de bogues. Vous pouvez le désactiver pour des raisons de confidentialité en suivant les instructions détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez ensuite les commandes suivantes pour supprimer complètement la fonctionnalité de rapport de bogue :

    ```bash
    sudo rm /etc/cron.daily/apport
    sudo apt purge apport
    ```

    Pour simplement désactiver le rapport automatique de bogues tout en conservant la fonctionnalité, ouvrez un terminal et tapez `sudo service apport stop`. Ouvrez ensuite le fichier de configuration avec la commande `sudo gedit /etc/default/apport` et mettez la valeur `enabled` à zéro, c'est-à-dire `enabled=0`.


<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Codecs pour Ubuntu" width="150px"></img> </center>

## Codecs Ubuntu

Les codecs indiquent à votre ordinateur comment lire les fichiers vidéo ou audio. Pour des raisons légales et éthiques, certaines distributions Linux n'incluent pas tous les codecs multimédia. Il est donc parfois nécessaire d'installer des codecs supplémentaires pour visualiser des formats de fichiers particuliers. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Exécutez les commandes suivantes pour profiter pleinement de votre expérience multimédia :

    ```bash
    sudo add-apt-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -cs) partner"
    sudo apt update
    sudo apt install ubuntu-restricted-extras
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Pilotes graphiques Ubuntu" width="150px"></img> </center>

## Pilotes graphiques Ubuntu

La grande majorité des distributions Linux sont livrées avec des pilotes graphiques « open source ». Bien que cela soit suffisant pour faire tourner des applications standard, ce n'est souvent pas assez pour les jeux. Pour installer des pilotes graphiques propriétaires, cliquez sur le bouton `Activités` en haut à gauche de l'écran et tapez `Pilotes additionnels`. Choisissez le bon pilote graphique (la plupart du temps, c'est l'option par défaut) et redémarrez votre système.


<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Mises à jour du système Ubuntu" width="150px"></img> </center>

## Mises à jour des logiciels Ubuntu

Maintenez Ubuntu à jour et installez régulièrement les derniers correctifs de sécurité, les corrections d'erreurs et les mises à niveau des applications. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Puis lancez la commande suivante :

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    La première partie de la commande `sudo apt update` vérifie si de nouvelles versions de logiciels sont disponibles. La seconde partie de la commande `sudo apt upgrade` installe les dernières mises à jour. Le `-y` à la fin de la commande autorise l'installation des nouveaux paquets.

    Si vous préférez ne pas utiliser le terminal, cliquez sur le bouton `Activités` en haut à gauche de l'écran, et recherchez `Gestionnaire de mises à jour`. Il vérifiera si des mises à jour sont disponibles et vous proposera de les installer.

    Une fois le système est à jour, supprimez les anciens paquets inutiles à l'aide des commandes suivantes :

    ```bash
    sudo apt autoremove
    sudo apt autoclean
    sudo apt clean
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Mises à jour du système Ubuntu" width="150px"></img> </center>

## Mises à niveau des versions Ubuntu

Au-delà des mises à jour logicielles, vous pouvez passer d'une version Ubuntu à l'autre, par exemple d'Ubuntu 20.04 LTS à 22.04 LTS, ou de 22.04 LTS à 22.10 non-LTS. Vous trouverez des instructions plus détaillées ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Commencez par [sauvegarder vos données](https://gofoss.net/fr/backups/). Puis, ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Mettez à jour tous les logiciels, comme décrit dans la section précédente :

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

    Vérifiez la version d'Ubuntu actuellement installée :

    ```bash
    lsb_release -a
    ```

    Activez les mises à niveau des versions Ubuntu :

    ```bash
    sudo gedit /etc/update-manager/release-upgrades
    ```

    Configurez la ligne `Prompt=normal` si vous souhaitez activer toutes les mises à niveau de versions, ou `Prompt=lts` si vous souhaitez activer uniquement les mises à niveau de versions LTS.

    Redémarrez votre ordinateur. Enfin, mettez-le à niveau avec la dernière version Ubuntu disponible :

    ```bash
    sudo do-release-upgrade -d -f DistUpgradeViewGtk3
    ```

    Suivez l'assistant de mise à niveau. Soyez patient, la mise à niveau peut prendre un certain temps. Assurez-vous que votre appareil est branché et prenez un café ! Une fois la mise à niveau terminée, le système redémarre et vous pouvez vous connecter. Notez que les logiciels tiers ont été désactivés lors de la mise à niveau et doivent être réactivés. Assurez-vous enfin que la mise à niveau a réussi en vérifiant la version d'Ubuntu dans le terminal :

    ```bash
    lsb_release -a
    ```


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Ubuntu" width="150px"></img> </center>

## Assistance

Pour davantage de précisions ou en cas de questions, consultez la [documentation d'Ubuntu](https://help.ubuntu.com/), les [tutoriels d'Ubuntu](https://ubuntu.com/tutorials), le [wiki d'Ubuntu](https://wiki.ubuntu.com/) ou demandez de l'aide à la [communauté conviviale d'Ubuntu](https://forum.ubuntu-fr.org/).

<div align="center">
<img src="https://imgs.xkcd.com/comics/sandwich.png" alt="Ubuntu Desktop"></img>
</div>

<br>

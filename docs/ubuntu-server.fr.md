---
template: main.html
title: Comment configurer un serveur Ubuntu
description: Auto-hébergement. Serveur Ubuntu. Linux, Apache, MySQL et PHP/Perl/Python. Pourquoi auto-héberger ? Qu'est-ce que LAMP ? Connexion à distance ? SSH ?
---

# Le guide pour configurer votre serveur Ubuntu

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises"

<div align="center">
<img src="../../assets/img/server_2.png" alt="Auto-hébergez votre serveur Ubuntu" width="700px"></img>
</div>

Dans ce chapitre, nous allons voir comment héberger un serveur Ubuntu à domicile, sur un ordinateur disponible ou du matériel bon marché. Ce serveur peut être utilisé pour héberger divers services, comme votre [stockage en nuage](https://gofoss.net/fr/cloud-storage/), vos [photos](https://gofoss.net/fr/photo-gallery/), vos [contacts, calendriers et tâches](https://gofoss.net/fr/contacts-calendars-tasks/) ou vos [films et séries TV](https://gofoss.net/fr/media-streaming/).

Trois autres chapitres sont dédiés aux mesures de sécurité pour protéger votre serveur contre les menaces les plus immédiates :

* des [bases de sécurité](https://gofoss.net/fr/server-hardening-basics/), comme le pare-feu, l'accès à distance et les mises à jour
* des [techniques plus avancées](https://gofoss.net/fr/server-hardening-advanced/), notamment la sécurité du réseau, les antivirus, les autorisations, etc.
* des [instructions de sauvegarde](https://gofoss.net/fr/server-backups/) pour effectuer des sauvegardes programmées de vos données


??? question "Devrais-je héberger mon serveur à domicile ou louer un VPS ?"

    Les deux options présentent des avantages et des inconvénients. Il s'agit de trouver un compromis entre confidentialité, sécurité et confort d'usage. Bien que ce tutoriel explique comment héberger votre serveur à domicile, vous pouvez également louer un [serveur dédié virtuel (en anglais, « virtual private server ou VPS »)](https://fr.wikipedia.org/wiki/Serveur_d%C3%A9di%C3%A9_virtuel) auprès de fournisseurs tels que [Hetzner](https://www.hetzner.com/?country=fr), [Digital Ocean](https://www.digitalocean.com/pricing), [OVH](https://www.ovhcloud.com/fr/vps/), [Contabo](https://contabo.com/en/), [Scaleway](https://www.scaleway.com/fr/instances-virtuelles/development/) et d'autres.

    <center>

    | | Serveur à domicile | Serveur dédié virtuel |
    | ------ | :------: | :------: |
    | Meilleure confidentialité | <span style="color:green">✔</span> |  |
    | Coûts initiaux moins élevés |  | <span style="color:green">✔</span> |
    | Coûts récurrents moins élevés | <span style="color:green">✔</span> | |
    | Plus facile à mettre en place |  | <span style="color:green">✔</span> |
    | Plus facile à maintenir | | <span style="color:green">✔</span> |
    | Moins de risques de sécurité | | <span style="color:green">✔</span> |
    | Moins de risque de pertes de données | | <span style="color:green">✔</span> |
    | Moins de risque de temps d'arrêt | | <span style="color:green">✔</span> |
    | Moins de problèmes de latence | <span style="color:green">✔</span> |  |
    | Moins de limitations de la bande passante | <span style="color:green">✔</span> | |

    </center>


??? question "Quelle est la configuration minimale requise pour le matériel utilisé ?"

    Cela dépend de l'utilisation prévue. Combien d'utilisateurs ou de visites prévoyez-vous par jour ? Prévoyez-vous de diffuser des vidéos ou de stocker de grandes quantités de données ? Les spécifications minimales suivantes devraient être suffisantes pour une poignée de services/utilisateurs :

    * Un ou plusieurs processeurs de 2 GHz
    * 2 à 4 Go de mémoire vive
    * 10 à 25 Go de stockage (ou plus pour les grandes collections de fichiers)
    * Connexion Internet rapide

    [Voici une liste de mini PC](https://forums.servethehome.com/index.php?threads/tiny-mini-micro-pc-experiences.30230/), qui sont bien adaptés aux serveurs en raison de leur petit facteur de forme et de leur faible prix.


??? question "Comment puis-je modifier des fichiers dans le terminal ?"

    L'interaction avec le serveur nécessite une utilisation assez intensive du terminal. Les fichiers de configuration et les réglages doivent souvent être modifiés à l'aide d'un éditeur de texte. L'éditeur [vi](https://fr.wikipedia.org/wiki/Vi) est couramment utilisé et dispose de trois modes :

    **Mode normal** : vi démarre en mode normal, qui permet de parcourir les documents.

    **Mode insertion** : en appuyant sur la touche `i`, vi passe en mode insertion, qui permet d'éditer et de modifier les documents.

    **Mode commande** : en appuyant sur la touche deux-points `:`, vi passe en mode commande, qui permet par exemple d'enregistrer des modifications ou de rechercher du texte dans le document.

    Les commandes suivantes devraient être suffisantes pour suivre toutes les instructions fournies sur notre site :

    <center>

    |Commande |Description |
    | ----- | ----- |
    | `vi fichier.txt` | Ouvrez le document « fichier.txt » avec l'éditeur de texte vi. |
    | `i` | Passez en mode « insertion » et commencez à modifier le fichier. |
    | `ESC` | Quittez le mode « insertion » une fois les modifications terminées. |
    | `:w` | Enregistrez toutes les modifications apportées au fichier, tout en le gardant ouvert. |
    | `:wq` ou `:x` | Enregistrez toutes les modifications apportées au fichier, et quittez vi. |
    | `:q!` | Quittez vi, sans enregistrer les modifications apportées au fichier. |
    | `/trouverunmot` | Recherchez le mot « trouverunmot » (remplacez-le par le mot que vous recherchez). |
    | `n` | Après avoir lancé la recherche de « trouverunmot », repérez la prochaine occurrence de « trouverunmot » dans le fichier. |

    </center>

    De nombreux tutoriels existent pour plonger plus profondément dans le monde de vi :

    * [Aide-mémoire de l'Université de Santa Clara](https://speicher.systemausfall.org/f/265c5adc9a7b4ddba7fd/)
    * [Aide-mémoire de WorldTimZone](https://speicher.systemausfall.org/f/2efedff727764690bda4/)
    * [Aide-mémoire de Daniel Gryniewicz](https://speicher.systemausfall.org/f/7be6da8705c9443288be/)


<br>

<center> <img src="../../assets/img/separator_ubuntu.svg" alt="Serveur Ubuntu" width="150px"></img> </center>

## Serveur Ubuntu

Effectuez quelques vérifications préliminaires et suivez les instructions d'installation ci-dessous.

??? tip "Montrez-moi la liste des contrôles préliminaires"

    <center>

    | Vérifications | Description |
    | ------ | ------ |
    | Mon ordinateur est-il compatible avec Linux ? |• [Testez Ubuntu](https://gofoss.net/fr/ubuntu/#ubuntu-live-usb-virtualbox) en utilisant une clé USB « Live » ou VirtualBox <br>• Consultez la [base de données de compatibilité](https://ubuntu.com/certified) <br>• [Demandez](https://search.disroot.org/) sur Internet <br>• Achetez un ordinateur compatible avec Linux, par exemple sur [Linux Préinstallé](https://linuxpréinstallé.com/) ou [Ministry of Freedom](https://minifree.org/) |
    | Mon ordinateur répond-il aux exigences minimales ? |• Processeur double cœur 2 GHz <br>• 4 Go de mémoire vive (RAM) <br>• 25 Go d'espace de stockage libre |
    | Mon ordinateur est-il branché ? | Si vous installez le serveur Ubuntu 22.04 sur un appareil mobile tel qu'un ordinateur portable, assurez-vous qu'il est bien branché. |
    | Le support d'installation est-il accessible ? | Vérifiez si votre ordinateur dispose d'un lecteur de DVD ou d'un port USB libre. |
    | Mon ordinateur est-il connecté à l'Internet ? | Vérifiez si la connexion Internet est opérationnelle. |
    | Ai-je sauvegardé mes données ? | [Sauvegardez vos données](https://gofoss.net/fr/backups/) : tout ce qui est stocké sur l'appareil sera effacé pendant le processus d'installation ! |
    | Ai-je téléchargé la dernière version d'« Ubuntu Server » ? | Téléchargez la dernière [version d'« Ubuntu Server » bénéficiant d'un support à long terme](https://ubuntu.com/download/server) (en anglais, « long-term support ou LTS »), qui est pris en charge pendant 5 ans, y compris les mises à jour de sécurité et de maintenance. Au moment d'écrire ces lignes, la dernière version LTS était « Ubuntu Server » 22.04. Consultez le [cycle de publication le plus récent](https://ubuntu.com/about/release-cycle) pour plus d'informations. |
    | Ai-je préparé un périphérique de démarrage ? | Utilisez un ordinateur [Windows](https://ubuntu.com/tutorials/burn-a-dvd-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/burn-a-dvd-on-macos#1-overview) ou [Ubuntu Linux](https://ubuntu.com/tutorials/burn-a-dvd-on-ubuntu#1-getting-started) pour graver le fichier `.iso` téléchargé sur un DVD. Vous pouvez également utiliser un ordinateur [Windows](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows#1-overview), [macOS](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview) ou [Ubuntu](https://ubuntu.com/tutorials/create-a-usb-stick-on-ubuntu#1-overview) pour créer une clé USB « Live ». |

    </center>


??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Instructions | Description |
    | ------ | ------ |

    |Démarrage |Insérez le DVD ou la clé USB « Live » dans l'ordinateur, puis redémarrez-le. La plupart des ordinateurs démarrent alors automatiquement depuis le DVD ou la clé USB. Si ce n'est pas le cas, essayez d'appuyer plusieurs fois sur `F12`, `ESC`, `F2` ou `F10` lorsque l'ordinateur démarre. Cela devrait vous donner accès au menu de démarrage, où vous pourrez alors sélectionner le DVD ou le lecteur USB comme périphérique de démarrage. |
    |GRUB |Sélectionnez `Try or Install Ubuntu Server`. |
    |Bienvenue |Sélectionnez une langue. |
    |Mise à jour de l'installateur |Cet écran peut apparaître si le serveur est déjà connecté à Internet. Sélectionnez `Continuer sans mettre à jour`. |
    |Disposition du clavier |Sélectionnez une disposition de clavier. |
    |Type d'installation |Sélectionnez l'option d'installation par défaut d'Ubuntu Server. |
    |Connections réseau |Assurez-vous de mettre en place une connexion Internet opérationnelle via Ethernet ou WiFi. C'est important pour la suite du processus. Sélectionnez `Info` pour en savoir plus sur un réseau donné. |
    |Proxy |Ces réglages sont facultatifs. Saisissez les informations requises si vous devez configurer des proxies réseau, sinon passez cette étape. |
    |Dépôt |Choisissez le dépôt Ubuntu. Les options par défaut devraient fonctionner correctement. |
    |Partitionnement disque |Choisissez comment partitionner votre disque. Dans ce tutoriel, nous utiliserons l'intégralité du disque <br>• Sélectionnez `Configurer ce disque comme un groupe LVM` <br>• Sélectionnez `Chiffrer le groupe LVM avec LUKS` pour chiffrer votre serveur <br>• Saisissez une [clé de sécurité forte et unique](https://gofoss.net/fr/passwords/) et confirmez <br><br> *Attention :* Cette étape effacera toutes les données et formatera le disque dur ! Vérifiez soigneusement tous les paramètres ! Assurez-vous d'avoir [sauvegardé vos données](https://gofoss.net/fr/backups/) ! |
    |Configuration du profil |Configurez le profil de l'utilisateur et du serveur. Pour ce tutoriel, nous choisirons la configuration suivante (à adapter en conséquence) : <br>• Votre nom (il s'agit du nom de l'utilisateur « root » du système): `gofossroot` <br>• Nom du serveur (il s'agit du nom de votre serveur): `gofossserver` <br>• Nom d'utilisateur (c'est le même que celui de l'utilisateur « root »): `gofossroot` <br>• Mot de passe: saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) et confirmez |
    |Configuration SSH |Ces réglages sont facultatifs. Laissez cette section vide et continuez. |
    |Featured Server Snaps |Ces réglages sont facultatifs. Laissez cette section vide et continuez. |
    |Dernière étape |Attendez que l'installation se termine. Une fois « Ubuntu Server » installé, retirez le support d'installation et appuyez sur `ENTRÉE` lorsque vous y êtes invité. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/7f4a0a65-7c82-4bd1-9f14-2ff5ced911c6" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_linux.svg" alt="Architecture LAMP" width="150px"></img> </center>

## Architecture LAMP

L'acronyme [« LAMP »](https://fr.wikipedia.org/wiki/LAMP) correspond à [Linux](https://fr.wikipedia.org/wiki/Ubuntu_(syst%C3%A8me_d%27exploitation)) (le système d'exploitation), [Apache](https://fr.wikipedia.org/wiki/Apache_HTTP_Server) (le logiciel du serveur), [MySQL](https://fr.wikipedia.org/wiki/MySQL) (le système de gestion de la base de données) et [PHP](https://fr.wikipedia.org/wiki/PHP)/[Perl](https://fr.wikipedia.org/wiki/Perl_(langage))/[Python](https://fr.wikipedia.org/wiki/Python_(langage)) (les langages de programmation).


Après le redémarrage, connectez-vous au serveur Ubuntu. Fournissez la clé de sécurité pour déchiffrer la partition du serveur, puis entrez le nom de l'utilisateur « root » et le mot de passe associé. Suivez ensuite les instructions ci-dessous pour installer l'architecture LAMP.

??? tip "Montrez-moi le guide étape par étape"

    Mettez à jour les paquets système et installez l'architecture LAMP :

    ```bash
    sudo apt update
    sudo apt upgrade
    sudo apt install lamp-server^
    ```

    Faites attention à l'accent circonflexe en fin de commande : `sudo apt install lamp-server^`. Il indique que `lamp-server` est un méta-paquet, qui installe Apache, MySQL et PHP avec d'autres paquets et dépendances.

    Vérifiez qu'Apache ait été correctement installé (le numéro de version devrait s'afficher), lancez automatiquement Apache après chaque redémarrage, et vérifiez le statut actuel d'Apache (qui devrait être `Active`) :

    ```bash
    apachectl -V
    sudo systemctl enable apache2
    sudo systemctl status apache2
    ```

    Même procédure pour MySQL :

    ```bash
    mysql -V
    sudo systemctl enable mysql
    sudo systemctl status mysql
    ```

    Vérifiez que PHP ait correctement été installé (un numéro de version devrait s'afficher) :

    ```bash
    php -v
    ```

    Et pour finir, un peu de ménage :

    ```bash
    sudo apt autoremove && sudo apt clean
    ```

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/d9c1baa7-68ec-47c7-83dd-c338c5e55751" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ip.svg" alt="Adresse IP statique du serveur Ubuntu" width="150px"></img> </center>

## Adresse IP statique

Il est recommandé d'attribuer une adresse IP statique au serveur. Dans le cadre de ce tutoriel, nous allons attribuer l'adresse IP statique `192.168.1.100`. Bien sûr, toute autre adresse disponible fera l'affaire. Assurez-vous simplement d'adapter les commandes correspondantes en conséquence dans les instructions fournies ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Interface réseau

    Tout d'abord, découvrons le nom de l'interface réseau du serveur :

    ```bash
    ip link show
    ```

    Une liste de toutes les interfaces réseau devrait s'afficher:

    * La première entrée est probablement nommée `lo`, l'interface de bouclage
    * L'interface Ethernet devrait être nommée `enpXsY`, par exemple `enp0s1` ou `enp1s3`
    * L'interface WiFi devrait être nommée `wlpXsY`, par exemple `wlp0s1` ou `wlp2s3`

    Dans le cadre de ce tutoriel, supposons que l'interface réseau du serveur soit nommée `enp0s3` (ajustez en conséquence).

    ### Passerelle par défaut

    Il est temps de découvrir le nom de la passerelle par défaut (en anglais, « default gateway ») :

    ```bash
    ip route show
    ```

    Le terminal devrait afficher une ligne contenant l'adresse IP de la passerelle par défaut, du type `default via 192.168.1.1 dev enp0s3 proto dhcp`. Pour ce tutoriel, supposons que cette adresse est `192.168.1.1` (ajustez en conséquence).

    ### Sauvegarde

    Sauvegardez le fichier de configuration réseau existant. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    ### Configuration réseau

    Ouvrez le fichier de configuration réseau :

    ```bash
    sudo vi /etc/netplan/00-installer-config.yaml
    ```

    Le contenu du fichier devrait ressembler à ceci :

    ```bash
    network:
      ethernets:
        enp0s3:
          dhcp4: true
      version: 2
    ```

    Le champ `dhcp4 : true` nous indique que le serveur se voit attribuer une adresse IP dynamique. Modifiez le contenu du fichier:

    * spécifiez l'interface réseau correct: dans notre exemple `enp0s3` (ajustez en conséquence)
    * attribuez une adresse IP statique au serveur: dans notre exemple `192.168.1.100` (ajustez en conséquence)
    * spécifiez la passerelle par défaut: dans notre exemple `192.168.1.1` (ajustez en conséquence)
    * sélectionnez un fournisseur DNS: dans notre exemple [UncensoredDNS](https://blog.uncensoreddns.org/) (sélectionnez le fournisseur DNS de votre choix)

    Faites attention à l'indentation correcte de chaque ligne dans le fichier de configuration réseau. Et assurez-vous d'ajuster tous les paramètres en fonction de votre propre configuration. Par exemple :

    * votre interface Ethernet pourrait s'appeler `enp0s1` au lieu de `enp0s3`
    * ou vous utiliser peut-etre une interface WiFi, disons `wlp0s1` : dans ce cas, vous devez remplacer la ligne `ethernets:` par `wifis` et la ligne `enp0s3` par `wlp0s1`
    * votre passerelle par défaut pourrait être nommée `192.168.0.1` au lieu de `192.168.1.1`
    * Vous pouvez également choisir une autre adresse IP statique ou un autre fournisseur de DNS

    ```bash
    network:
      ethernets:
        enp0s3:
          dhcp4: false
          addresses: [192.168.1.100/24]
          routes:
            - to: default
              via: 192.168.1.1
          nameservers:
            addresses: [89.233.43.71, 91.239.100.100]
      version: 2
    ```

    Sauvegardez et fermez le fichier en appuyant sur `ESC` et en tapant `:wq`. Enfin, appliquez et vérifiez tous les changements :

    ```bash
    sudo netplan apply
    ip add
    ```


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ef1b78f7-bab2-4ace-8de0-ddb1c9715338" frameborder="0" allowfullscreen></iframe>
    </center>

??? question "Pourquoi le serveur a-t-il besoin d'une adresse IP statique ?"

    Par défaut, le [protocole DHCP](https://fr.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) attribue une adresse IP unique à chaque appareil de votre réseau domestique, y compris votre serveur. Après chaque redémarrage ou perte de connexion réseau, il est possible que votre serveur se voie attribuer une nouvelle adresse IP. Ce qui, à son tour, nécessiterait de reconfigurer divers réglages. Vous pouvez éviter cela en attribuant une adresse IP statique à votre serveur.

    L'adresse IP statique que vous pouvez attribuer à votre serveur dépend de la configuration de votre réseau. Vérifiez les réglages de vos autres appareils ou de votre routeur pour savoir quelles adresses IP sont utilisées à travers votre réseau domestique. Des plages IP couramment utilisées sont `192.168.0.xx` ou `192.168.1.xx`.

    Il existe deux façons d'attribuer une adresse IP statique à votre serveur :

    * **Configuration réseau** : modifiez directement la configuration réseau sur le serveur, comme indiqué ci-dessus.

    * **Réservation DHCP** : vous pouvez également configurer une réservation DHCP sur votre routeur, qui attribuera ainsi toujours la même adresse IP au serveur. Reportez-vous au manuel du routeur pour plus d'informations.


??? question "Existe-t-il d'autres fournisseurs de DNS respectueux de la vie privée ?"

    <center>

    | Fournisseurs DNS | Pays | DNS #1 | DNS #2 |Politique de confidentialité |
    | ------ | ------ | ------  |------ |------ |
    | [Digitalcourage](https://digitalcourage.de/en) | Allemagne | 5.9.164.112 | -- |[Politique de confidentialité](https://digitalcourage.de/datenschutz-bei-digitalcourage) |
    | [Dismail](https://dismail.de/info.html#dns/) | Allemagne |80.241.218.68 | 159.69.114.157 |[Politique de confidentialité](https://dismail.de/datenschutz.html#dns) |
    | [DNS Watch](https://dns.watch/) | Allemagne | 84.200.69.80 | 84.200.70.40 | -- |
    | [FDN](https://www.fdn.fr/actions/dns/) | France | 80.67.169.12 | 80.67.169.40 | -- |
    | [OpenNIC](https://servers.opennic.org/) | Divers | Divers | Divers | Divers |

    </center>



<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Connexion à distance au serveur Ubuntu" width="150px"></img> </center>

## Connexion à distance

Il est possible de se connecter à distance au *serveur* depuis un autre ordinateur, appelé *client*. De cette façon, le serveur peut rester *sans affichage* (en anglais, « headless ») — il n'est pas nécessaire de le brancher à un écran, un clavier, une souris ou tout autre périphérique. La connexion à distance est protégée par le protocole [« Secure Shell (SSH) »](https://fr.wikipedia.org/wiki/Secure_Shell), qui impose une authentification et un chiffrement puissants. Voici en gros comment cela fonctionne, vous trouverez des instructions détaillées un peu plus bas.

<div align="center">
<img src="../../assets/img/ssh_connection.png" alt="Serveur Ubuntu SSH" width="500px"></img>
</div>

1. Le *client* établit la connexion avec le *serveur*.
2. Le *serveur* envoie sa clé publique au *client*.
3. La clé publique du *serveur* est enregistrée dans le fichier d'hôtes connus (en anglais, « known hosts ») du *client*.
4. Le *client* et le *serveur* conviennent du chiffrement à utiliser pour leur communication et établissent la connexion.


??? tip "Montrez-moi le guide étape par étape"

    ### Configuration du serveur

    Commençons par le *serveur*. Exécutez les commandes suivantes pour installer OpenSSH, pour automatiquement lancer le logiciel après chaque démarrage, et pour vérifier son statut actuel (qui devrait être `Active`) :

    ```bash
    sudo apt install openssh-server
    sudo systemctl enable ssh
    sudo systemctl status ssh
    ```

    Créez un compte administrateur avec des privilèges pour se connecter à distance au serveur. Dans le cadre de ce tutoriel, nous appellerons cet administrateur `gofossadmin`. Tout autre nom fera l'affaire, assurez-vous simplement d'ajuster les commandes en conséquence. Lorsque vous y êtes invité, saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) :

    ```bash
    sudo adduser gofossadmin
    sudo usermod -a -G sudo gofossadmin
    ```

    Connectez-vous au nouveau compte administrateur et testez ses pouvoirs `sudo` en mettant à jour le système :

    ```bash
    su - gofossadmin
    sudo apt update
    ```

    ### Configuration du client

    Maintenant, configurons le *client*. Dans le cadre de ce tutoriel, nous supposons que l'ordinateur client tourne sous une distribution GNU/Linux, par exemple Ubuntu. Si votre client tourne sous Windows ou macOS, utilisez les outils système disponibles ou installez un client SSH tel que [PuTTY](https://www.putty.org/).

    Ouvrez un terminal sur votre ordinateur client Linux avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Activités` en haut à gauche et cherchez `Terminal`. Créez maintenant le même compte administrateur `gofossadmin` que sur le serveur (ajustez le nom en fonction de votre propre configuration). Saisissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) lorsque vous y êtes invité :

    ```bash
    sudo adduser gofossadmin
    ```

    Connectez-vous au nouveau compte administrateur, créez un répertoire caché où seront stockées les clés SSL et attribuez lui les droits d'accès nécessaires :

    ```bash
    su - gofossadmin
    mkdir /home/gofossadmin/.ssh
    chmod 755 /home/gofossadmin/.ssh
    ```

    Générez une paire de clés publique/privée qui sécurisera la connexion à distance à votre serveur. Exécutez les commandes ci-dessous, suivez les instructions à l'écran et, lorsque vous y êtes invité, fournissez un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) :

    ```bash
    cd /home/gofossadmin/.ssh
    ssh-keygen -t RSA -b 4096
    ```

    Le résultat devrait ressembler à ceci :

    ```bash
    Generating public/private RSA key pair.
    Enter file in which to save the key (/home/gofossadmin/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase): *******
    Enter same passphrase again: ********
    Your identification has been saved in /home/gofossadmin/.ssh/id_rsa.
    Your public key has been saved in /home/gofossadmin/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:erdBxn9i/ORLIJ4596DvfUIIPOyfFnSMQ4SieLIbSuxI gofossadmin@gofossclient
    The key's randomart image is:
    +---[RSA 4096]----+
    |   .+.+=     o   |
    |       ..oo.+ o  |
    | .S+..     %..   |
    |         o+o. o  |
    |        So*o o = |
    |   ..oo.+     A +|
    | ..+ooo       .oo|
    |  .o...+  .E..   |
    |         .. o==.%|
    +----[SHA256]-----+
    ```

    Transférez la clé publique du client vers le serveur. Veillez à remplacer le nom de l'administrateur et l'adresse IP en fonction de votre propre configuration :

    ```bash
    ssh-copy-id gofossadmin@192.168.1.100
    ```

    Voilà, c'est fait ! À partir de maintenant, vous pouvez vous connecter à distance à votre serveur à partir de l'ordinateur client. Ouvrez simplement un terminal, basculez sur le compte administrateur et connectez-vous au serveur en utilisant le mot de passe correct. N'oubliez pas d'ajuster le nom de l'administrateur et l'adresse IP selon vos besoins :

    ```bash
    su - gofossadmin
    ssh gofossadmin@192.168.1.100
    ```

??? tip "Montrez-moi une vidéo récapitulative (3min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/f201ed55-5b1c-49b8-9698-73940c05aab2" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance pour le serveur Ubuntu" width="150px"></img> </center>

## Assistance

Pour davantage de précisions ou en cas de questions, consultez la [documentation d'Ubuntu](https://help.ubuntu.com/), les [tutoriels d'Ubuntu](https://ubuntu.com/tutorials), le [wiki d'Ubuntu](https://wiki.ubuntu.com/) ou demandez de l'aide à la [communauté d'Ubuntu](https://forum.ubuntu-fr.org/).

<br>

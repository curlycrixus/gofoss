---
template: main.html
title: So verschlüsselt Ihr Eure Nachrichten
description: Was ist Transportschichtverschlüsselung? Was ist eine durchgängige Verschlüsselung? Welche Verschlüsselungsalgorithmen gibt es? Was sind öffentliche und private Schlüssel?
---

# Führt vertrauliche Gespräche

In diesem Kapitel erfahrt Ihr, wie Ihr Eure [Nachrichten und Anrufe verschlüsseln](https://gofoss.net/de/encrypted-messages/) könnt. Darüber hinaus wird in diesem Kapitel beschrieben, wie Ihr schrittweise Eure [E-Mails zu einem datenschutzfreundlichen Anbieter umsiedelt](https://gofoss.net/de/encrypted-emails/).

<center>
<html>
<embed src="../../assets/echarts/email_stats.html" style="width: 100%; height:420px">
</html>
</center>


## Verschlüsselte Chats und verschlüsselte E-Mails

Wahrscheinlich ist Euch bereits bekannt, dass die meisten Nachrichte-Apps und E-Mail-Anbieter:

* [private Nachrichten an Werbekunden verkaufen](https://www.wsj.com/articles/yahoo-bucking-industry-scans-emails-for-data-to-sell-advertisers-1535466959)
* [an Massenüberwachungsprogrammen teilnehmen](https://www.nytimes.com/2016/10/06/technology/yahoo-email-tech-companies-government-investigations.html)
* [schwerwiegende Datenpannen erleiden](https://en.wikipedia.org/wiki/Yahoo!_data_breaches)
* [das Verhalten ihrer Benutzer überwachen](https://thenextweb.com/news/google-tracks-nearly-all-your-online-and-some-of-your-offline-purchases-see-it-for-yourself-here)

Dennoch vertrauen die meisten NutzerInnen weiterhin etablierten Anbietern. Hauptgrund? Ein Anbieterwechsel schreckt viele ab. Es wird oft argumentiert, dass die derzeitigen Apps doch "kostenlos", jedem bekannt und darüber hinaus an alle Konten und Abonnements gebunden seien. Dabei ist ein Anbieterwechsel eigentlich gar nicht so schwer.


## Transportschichtverschlüsselung vs. durchgängige Verschlüsselung

Heutzutage bieten mehrere Apps Verschlüsselung an, um Eure Kommunikation zu schützen:

Die **Transportschichtverschlüsselung (TLS)** stellt sicher, dass Eure Nachrichten während der Übertragung im Internet oder zwischen Mobilfunkmasten nicht entschlüsselt werden können. Zwischengeschaltete Dienste können jedoch auf Eure Daten zugreifen, während sie diese weiterleiten oder verarbeiten. Dazu gehören z.B. Messenger-Dienste, soziale Netzwerke, Suchmaschinen, Banken und so weiter.

<div align="center">
<img src="../../assets/img/tls.png" alt="Transportschichtverschlüsselung" width="550px"></img>
</div>

Die **durchgängige Verschlüsselung** schützt Eure Nachrichten während der gesamten Übertragung. Niemand außer dem Endempfänger kann sie entschlüsseln. Nicht einmal zwischengeschaltete Dienste, die die Daten weitergeben, wie z. B. ein [durchgängig verschlüsselter Nachrichten-Dienst](https://gofoss.net/de/encrypted-messages/) oder [E-Mail-Anbieter](https://gofoss.net/de/encrypted-emails/).

<div align="center">
<img src="../../assets/img/e2ee.png" alt="Durchgängige Verschlüsselung" width="550px"></img>
</div>

??? tip "Hier erfahrt Ihr, wie Verschlüsselung helfen kann Überwachung zu vermeiden"

    <center>

    <iframe src="https://archive.org/embed/tips-to-help-avoid-online-surveillance" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Quelle: [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>

<br>

---
template: main.html
title: So richtet Ihr eine sichere Domäne ein
description: Selbst hosten. DuckDNS. Let's Encrypt. Wie installiert man OpenVPN? Was ist Pi-hole? Was ist ein Apache Virtual Host? Was ist ein Reverse Proxy?
---

# Verbindet Euch sicher und von überall aus mit Eurem Server

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/pihole.png" alt="Pi-hole installieren" width="450px"></img>
</div>

Nachdem Ihr Euren [Ubuntu-Server](https://gofoss.net/de/ubuntu-server/) erfolgreich eingerichtet und die [grundlegenden](https://gofoss.net/de/server-hardening-basics/) sowie [erweiterten](https://gofoss.net/de/server-hardening-advanced/) Sicherheitsmaßnahmen konfiguriert habt, erfahrt Euch dieses Kapitel, wie Ihr:

* mittels DuckDNS eine (Sub-)Domäne auf Eure Heim-IP-Adresse verweist
* mittels Let's Encrypt Euren Datenverkehr verschlüsselt
* mittels Pi-hole auf lokale Dienste zugreift, einen datenschutzfreundlichen DNS-Anbieter nutzt und Werbung blockiert
* mittels eines Reverse Proxys Euren Server vom Internet abschirmt
* mittels OpenVPN sicher auf Euren Server zugreift, von überall auf der Welt

## Eine Domäne mit DuckDNS einrichten

<div align="center">
<img src="../../assets/img/duckdns.png" alt="DuckDNS" width="500px"></img>
</div>

Man merkt sich Internet-Adressen leichter als irgendwelche IP-Adressen. Statt `18.192.76.182` in den Browser einzugeben, ziehen es die meisten Nutzer vor [www.gofoss.net](https://gofoss.net/de/) aufzurufen. Damit das auch mit Eurem Server funktioniert, benötigt Ihr einen eindeutigen Namen – den sogenannte *Domänennamen* – der auf Eure private IP-Adresse verweist. Der Domänenname ist zudem zur Verschlüsselung des Datenverkehrs erforderlich, wie im nächsten Abschnitt erläutert wird.

[DuckDNS](https://www.duckdns.org/) ermöglicht es, kostenlos eine (Sub-)Domäne zu registrieren und diese dynamisch auf Eure Heim-IP-Adresse zu verweisen. Diese Methode funktioniert also selbst, wenn sich Eure IP-Adresse häufig ändern sollte. Besucht dazu einfach die DuckDNS-Webseite und registriert eine (Sub-)Domäne Eurer Wahl. Sobald die Registrierung bestätigt ist, erhaltet Ihr ein einmaliges Token.

Für die Zwecke dieses Tutorials haben wir die (Sub-)Domäne `gofoss.duckdns.org` registriert und das Token `ebaa3bd3-177c-4230-8d91-a7a946b5a51e` erhalten. Damit können wir später über Adressen wie `https://myservice.gofoss.duckdns.org` auf Server-Dienste zugreifen.

<br>

<center> <img src="../../assets/img/separator_letsencrypt.svg" alt="Let's Encrypt Zertifikate" width="150px"></img> </center>

## Datenverkehr mit Let's Encrypt verschlüsseln

Sämtlicher Datenverkehr sollte über HTTPS verschlüsselt werden. Die Adressleiste Eures Browsers sollte immer eine sichere Verbindung anzeigen – ganz egal, ob Ihr im Internet surft oder auf selbst gehostete Dienste zugreift. Damit das auch mit Eurem Server funktioniert, werden SSL-Zertifikate benötigt. Die könnt Ihr entweder kaufen, oder kostenlos von [Let's Encrypt](https://letsencrypt.org/de/) erhalten. Let's Encrypt ist eine vertrauenswürdige [Zertifizierungsstelle](https://de.wikipedia.org/wiki/Zertifizierungsstelle). Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Dehydrated installieren

    Bevor Let's Encrypt Euch ein Zertifikat ausstellt, wird erst mal überprüft, [ob Ihr tatsächlich Eigentümer der Domäne seid](https://letsencrypt.org/de/docs/challenge-types/). Ein schickes Programm namens [Dehydrated](https://github.com/dehydrated-io/dehydrated/) macht dies möglich. Meldet Euch auf Eurem Server an und ladet Dehydrated herunter:

    ```bash
    sudo apt install git
    git clone https://github.com/dehydrated-io/dehydrated.git
    ```

    Wechselt in das Verzeichnis `dehydrated` und erstellt eine erste Konfigurationsdatei:

    ```bash
    cd dehydrated
    vi domains.txt
    ```

    Fügt die folgende Zeile ein, und gebt dabei unbedingt Euren eigenen Domänennamen an:

    ```bash
    *.gofoss.duckdns.org > gofoss.duckdns.org
    ```

    Speichert und schließt die Datei (`:wq!`). Erstellt anschließend eine zweite Konfigurationsdatei:

    ```bash
    vi config
    ```

    Fügt die folgenden Zeilen ein. Vergesst nicht, Eure eigene E-Mail-Adresse anzugeben:

    ```bash
    CHALLENGETYPE="dns-01"
    BASEDIR=/etc/dehydrated
    HOOK="${BASEDIR}/hook.sh"
    CONTACT_EMAIL=gofoss@gofoss.net
    ```

    Speichert und schließt die Datei (`:wq!`). Erstellt anschließend eine dritte Konfigurationsdatei:

    ```bash
    vi hook.sh
    ```

    Fügt die folgenden Zeilen ein. Gebt dabei unbedingt Euren eigenen Domänennamen sowie Euer eigenes DuckDNS-Token an:

    ```bash
    DOMAIN="gofoss.duckdns.org"                         # gebt Eure Domäne hier an
    TOKEN="ebaa3bd3-177c-4230-8d91-a7a946b5a51e"        # gebt Euer Token hier an

    case "$1" in
        "deploy_challenge")
            curl "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=$4"
            echo
            ;;
        "clean_challenge")
            curl "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&txt=removed&clear=true"
            echo
            ;;
        "deploy_cert")
            ;;
        "unchanged_cert")
            ;;
        "startup_hook")
            ;;
        "exit_hook")
            ;;
        *)
            echo Unknown hook "${1}"
            exit 0
            ;;
    esac
    ```

    Speichert und schließt die Datei (`:wq!`).

    **Hinweis:** Die obigen Anweisungen müssen angepasst werden, falls Ihr eine andere Domäne als DuckDNS verwendet. Lest in diesem Fall bitte die [Dokumentation von Dehydrated](https://github.com/dehydrated-io/dehydrated/wiki/) oder fragt in der [Let's Encrypt Gemeinschaft](https://community.letsencrypt.org/) nach, um die sogenannte *Hook* für die DNS-basierte Überprüfung richtig einzurichten.

    Die folgenden Befehle erstellen ein `certs` Verzeichnis, in dem alle SSL-Zertifikate gespeichert werden. Die Dateien `dehydrated` und `hook.sh` werden ausführbar gemacht. Das `dehydrated` Verzeichnis wird nach `/etc/dehydrated` verschoben. Schließlich wird `gofossadmin` zum Eigentümer des Verzeichnisses `/etc/dehydrated`. Stellen sicher, dass Ihr den Namen des Administrators an Eure eigenen Einstellungen anpasst:

    ```bash
    mkdir certs
    chmod a+x dehydrated
    chmod a+x hook.sh
    cd ..
    sudo mv dehydrated /etc/dehydrated
    sudo chown -R gofossadmin:gofossadmin /etc/dehydrated
    ```

    Überprüft die Verzeichnisstruktur:

    ```bash
    sudo ls -al /etc/dehydrated
    ```

    Die Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    drwxr-xr-x 2 gofossadmin gofossadmin   4096 Jan 01 00:00 .
    drwxr-xr-x 4 gofossadmin gofossadmin   4096 Jan 01 00:00 ..
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 certs
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 config
    -rwxr-xr-x 1 gofossadmin gofossadmin 700000 Jan 01 00:00 dehydrated
    -rw-r--r-- 1 gofossadmin gofossadmin    200 Jan 01 00:00 docs
    -rw-r--r-- 1 gofossadmin gofossadmin     20 Jan 01 00:00 domains.txt
    -rwxr-xr-x 1 gofossadmin gofossadmin    700 Jan 01 00:00 hook.sh
    ```


    ### Zertifikate erstellen

    Meldet Euch bei Let's Encrypt an:

    ```bash
    bash /etc/dehydrated/dehydrated --register --accept-terms
    ```

    Der obige Befehl erstellt das Verzeichnis `/etc/dehydrated/accounts`, das Eure Registrierungsinformationen enthält. Das Terminal sollte nun in etwa folgendes anzeigen:

    ```bash
    # INFO: Using main config file /etc/dehydrated/config
    + Generating account key...
    + Registering account key with ACME server...
    + Done!
    ```

    Erstellt nun die SSL-Zertifikate:

    ```bash
    bash /etc/dehydrated/dehydrated -c
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    + Creating chain cache directory /etc/dehydrated/chains
    Processing gofoss.duckdns.org
    + Creating new directory /etc/dehydrated/certs/gofoss.duckdns.org ...
    + Signing domains...
    + Generating private key...
    + Generating signing request...
    + Requesting new certificate order from CA...
    + Received 1 authorizations URLs from the CA
    + Handling authorization for gofoss.duckdns.org
    + 1 pending challenge(s)
    + Deploying challenge tokens...
    OK
    + Responding to challenge for gofoss.duckdns.org authorization...
    + Challenge is valid!
    + Cleaning challenge tokens...
    OK
    + Requesting certificate...
    + Checking certificate...
    + Done!
    + Creating fullchain.pem...
    + Done!
    ```

    Das war's! Es wurden zwei SSL-Schlüssel erstellt, um den Datenverkehr Eurer selbst gehosteten Dienste zu verschlüsseln. Wir erklären später, wie diese Schlüssel zum Einsatz kommen.

    <center>

    | Datei | Pfad |
    | ------ | ------ |
    | Öffentlicher Schlüssel | /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem |
    | Privater Schlüssel | /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem |

    </center>


    ### Zertifikate automatisch verlängern

    Die Zertifikate von Let's Encrypt laufen nach 90 Tagen ab und können 30 Tage zuvor erneuert werden. Es empfiehlt sich, dies zu automatisieren. Erstellen hierzu eine Log-Datei:

    ```bash
    sudo touch /var/log/dehydrated
    ```

    Erstellt anschließend einen wöchentlichen Cron-Job, der die Gültigkeit des Zertifikats überprüft und es bei Bedarf erneuert:

    ```bash
    sudo vi /etc/cron.weekly/dehydrated
    ```

    Fügt die folgenden Zeilen ein:

    ```bash
    #!/bin/sh

    MYLOG=/var/log/dehydrated
    echo "Checking cert renewals at `date`" >> $MYLOG
    /etc/dehydrated/dehydrated -c >> $MYLOG 2>&1
    ```

    Speichert und schließt die Datei (`:wq!`). Macht das Skript nun ausführbar:

    ```bash
    sudo chmod +x /etc/cron.weekly/dehydrated
    ```

    Testet den Cron-Job:

    ```bash
    sudo bash /etc/cron.weekly/dehydrated
    sudo cat /var/log/dehydrated
    ```

    Die Terminal-Ausgabe sollte in etwa folgendermaßen aussehen:

    ```bash
    Checking cert renewals at Sun 01 Jan 2021 01:29:06 PM CEST
    # INFO: Using main config file /etc/dehydrated/config
    Processing *.gofoss.duckdns.org
     + Checking domain name(s) of existing cert... unchanged.
     + Checking expire date of existing cert...
     + Valid till Mar 12 10:23:18 2021 GMT (Longer than 30 days). Skipping renew!
    ```


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/8ab6fc0e-fee9-4ca4-acc5-aa143c967e41" frameborder="0" allowfullscreen></iframe>
    </center>


??? question "Und was ist mit selbst signierten Zertifikaten?"

    Die Verwendung von selbst signierten Zertifikaten für die HTTPS-Verschlüsselung ist durchaus möglich. Allerdings werden diese von Browsern und Anwendungen oft nicht erkannt oder erzeugen Warnungen und Fehlermeldungen. Auf Android-Telefonen beispielsweise müssen selbst signierte Zertifikate erst importiert und als vertrauenswürdig eingestuft werden. Aus diesen Gründen empfehlen wir, eine Zertifizierungsstelle wie Let's Encrypt zu bevorzugen.


<br>

<center> <img src="../../assets/img/separator_pihole.svg" alt="Pi-hole" width="150px"></img> </center>

##  Pi-hole verwaltet den Datenverkehr und blockiert Werbung

<br>

<div align="center">
<img src="../../assets/img/pihole_interface.png" alt="Pi-hole Benutzeroberfläche" width="500px"></img>
</div>

[Pi-hole](https://pi-hole.net/) ist ein nützliches kleines Programm: Es kann lokale Adressen wie `https://myservice.gofoss.duckdns.org` auflösen, ermöglicht die Auswahl eines Upstream-DNS-Anbieters Eurer Wahl und blockiert Werbung und Tracker. Folgt den nachstehenden Anweisungen, um Pi-hole auf Eurem Server einzurichten.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Pi-hole installieren

    Zum Zeitpunkt der Abfassung dieses Textes war die Version 5.10 die letzte Pi-hole-Version. Das Programm ist relativ ressourcenarm, 50 MB freier Speicherplatz und 512 MB RAM reichen aus, um es unter Ubuntu zu betreiben. Pi-hole benötigt außerdem eine statische IP-Adresse sowie Apache und PHP, die wir [in früheren Kapiteln](https://gofoss.net/de/server-hardening-basics/) konfiguriert haben. Führt das Installationsskript von Pi-hole aus:

    ```bash
    sudo curl -sSL https://install.pi-hole.net | bash
    ```

    Folgt den Anweisungen auf dem Bildschirm:

    <center>

    | Anweisung | Beschreibung |
    | ------ | ------ |
    | This installer will transform your device into a network-wide ad blocker! | Drückt auf `EINGABE`, um fortzufahren. |
    | The Pi-hole is free, but powered by your donations | Drückt auf `EINGABE`, um fortzufahren. |
    | The Pi-hole is a SERVER so it needs a STATIC IP ADDRESS to function properly | Wählt `Yes` und drückt auf `EINGABE`, um fortzufahren. |
    | Choose An Interface | Wählt Eure Netzwerkschnittstelle. Drückt die `TABULATORTASTE` zur Änderung der Auswahl und die `LEERTASTE` um Eure Auswahl zu bestätigen. Drückt auf `EINGABE`, um fortzufahren. |
    | Select Upstream DNS Provider. To use your own, select Custom | Wählt einen DNS-Anbieter Eurer Wahl. In diesem Beispiel verwenden wir [UncensoredDNS](https://blog.uncensoreddns.org/), Ihr könnt natürlich jeglichen anderen DNS-Anbieter auswählen! Wir empfehlen Euch lediglich, einen anderen DNS-Anbieter als Euren Internet-Anbieter, Cloudfare oder Google zu wählen. Weitere Informationen zu DNS-Anbietern findet am Ende dieses Abschnitts. |
    | Pi-hole relies on third party lists in order to block ads. | Drückt auf `EINGABE`, um fortzufahren. Wir erklären etwas später, wie Ihr weitere Blocklisten hinzufügen könnt. |
    | Do you wish to install the web admin interface? | Wählt `On`, falls Ihr eine Administrator-Weboberfläche wünscht (empfohlen) und drückt `EINGABE`, um fortzufahren. |
    | Do you wish to install the web server (lighttpd)? | Wir werden Apache statt lighttpd als Webserver verwenden. Wählt also `Off` und bestätigt mit der `LEERTASTE`. Drückt anschließend auf `EINGABE`, um fortzufahren. |
    | Do you want to log queries? | Wählt `On` (empfohlen) und drückt auf `EINGABE`, um fortzufahren. |
    | Select a privacy mode for FTL | Wählt die gewünschte Datenschutzstufe aus und klickt auf `ENTER`, um fortzufahren: <br><br> • `Show everything`: zeichnet alles auf; liefert die umfangreichsten Statistiken <br> • `Hide domains`: Zeigt und speichert alle Domänen als "versteckt"; deaktiviert die Tabellen mit den Top Domänen und Top Werbungen auf dem Dashboard <br> • `Hide domains and clients`: Zeigt und speichert alle Domänen als "versteckt" und alle Client-Geräte als "0.0.0.0"; deaktiviert alle Tabellen auf dem Dashboard <br> • `Anonymous mode`: Deaktiviert im Grunde alles außer den anonymen Live-Statistiken; es wird kein Verlauf in der Datenbank gespeichert, nichts wird im Abfrageprotokoll angezeigt, es gibt keine Top-Item-Listen; bietet einen verbesserten Datenschutz |
    | Installation Complete! |Sobald die Installation abgeschlossen ist, solltet Ihr Folgendes sehen:<br><br><center><img src="../../assets/img/pihole-installation.png" alt="Pi-hole Installation" width="60%"></img></center> |

    </center>


    ### Weboberfläche

    Als nächstes richten wir einen Apache Virtual Host als Reverse Proxy ein, um auf die Pi-hole-Weboberfläche zuzugreifen. Klingt kompliziert, aber im Wesentlichen ist der Reverse Proxy dazu da, Euren Server noch stärker vom Internet abzuschirmen. Mehr dazu erfahrt Ihr am Ende dieses Abschnitts.

    Legt die richtigen Zugriffsrechte fest:

    ```bash
    sudo chown www-data:www-data -R /var/www/html/admin/
    sudo usermod -aG pihole www-data
    sudo chown -R pihole:pihole /etc/pihole
    ```

    Erstellt eine Apache-Konfigurationsdatei::

    ```bash
    sudo vi /etc/apache2/sites-available/mypihole.gofoss.duckdns.org.conf
    ```

    Fügt den folgenden Inhalt hinzu und passt die Einstellungen an Eure eigene Konfiguration an, z. B. den Domain-Namen (`mypihole.gofoss.duckdns.org`), den Pfad zu den SSL-Schlüsseln, die IP-Adressen und so weiter:

    ```bash
    <VirtualHost *:80>

        ServerName              mypihole.gofoss.duckdns.org
        ServerAlias             www.mypihole.gofoss.duckdns.org
        Redirect permanent /    https://mypihole.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              mypihole.gofoss.duckdns.org
        ServerAlias             www.mypihole.gofoss.duckdns.org
        ServerSignature         Off

        SSLEngine               On
        SSLProxyEngine          On
        SSLProxyCheckPeerCN     Off
        SSLCertificateFile      /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
        SSLCertificateKeyFile   /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
        DocumentRoot            /var/www/html/admin

        <Location />
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        <Directory /var/www/html/admin/>
            Options +FollowSymlinks
            AllowOverride All
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/mypihole.gofoss.duckdns.org-error.log
        CustomLog ${APACHE_LOG_DIR}/mypihole.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Speichert und schließt die Datei (`:wq!`).

    Beachtet, dass die SSL-Verschlüsselung mit der Anweisung `SSLEngine On` aktiviert und das zuvor erstellte SSL-Zertifikat `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` sowie der private SSL-Schlüssel `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem` verwendet wurden.

    Aktiviert als nächstes den Apache Virtual Host (passt die Domäne entsprechend an) und ladet Apache neu:

    ```bash
    sudo a2ensite mypihole.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Startet Pi-hole neu und prüft, ob alles korrekt läuft. Die Ausgabe sollte "Active" anzeigen:

    ```bash
    sudo systemctl restart pihole-FTL
    sudo systemctl status pihole-FTL
    ```


    ### Pi-hole konfigurieren

    Ändert das Standardpasswort für die Pi-hole-Weboberfläche. [Gebt ein sicheres, individuelles Passwort an](https://gofoss.net/de/passwords/):

    ```bash
    sudo pihole -a -p
    ```

    Ruft `http://192.168.1.100/admin/` auf und meldet Euch mit den neuen Zugangsdaten an. Passt dabei die IP-Adresse des Servers an Eure eigenen Einstellungen an. Die Weboberfläche sollte auf Eurem Bildschirm erscheinen. Sie ermöglicht verschiedentliche Einstellungen, [die auf Pi-holes Webseite näher beschrieben werden](https://github.com/pi-hole/AdminLTE/):

    <center>

    | Menü-Eintrag | Beschreibung |
    | ------ | ------ |
    | Gravity |Ruft den Eintrag `Tools ‣ Update Gravity` auf, um die Blocklisten zu aktualisieren. Ein wenig Geduld ist gefragt, das kein ein paar Sekunden dauern. |
    | Dashboard |Zeigt Statistiken an: wie viele Domänen besucht oder blockiert wurden, wie viele Domänen sich auf der Blockliste befinden, usw. |
    | Queries |Detaillierte Informationen zu Anfragen. |
    | Blocklists |Ruft den Eintrag `Group management ‣ Adlists` auf, um weitere Blocklisten hinzuzufügen und zusätzliche Werbung und Schadprogramme zu filtern. Weitere Informationen zu Pi-hole-Blocklisten findet Ihr am Ende dieses Abschnitts. |
    | Settings |Von hier aus kann Pi-hole verwaltet und konfiguriert werden: DNS-Server, Datenschutz, usw. |
    | Local DNS |Pi-hole ist in der Lage, lokale Adressen aufzulösen. Ruft dazu den Menü-Eintrag `Local DNS ‣ DNS Records` auf und fügt die folgende Domäne/IP-Kombination hinzu (passt den Eintrag entsprechend Eurer eigenen Einstellungen an): <br><br>`Domain`: `mypihole.gofoss.duckdns.org` <br> `IP Address`: `192.168.1.100` |

    </center>


??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/09477516-5382-44f7-bcb5-c846375e723e" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Übergangslösung für Ubuntu 22.04"

    Zum Zeitpunkt des Verfassens dieser Zeilen wird [Ubuntu 22.04 offiziell nicht von Pi-hole 5.10 unterstützt](https://docs.pi-hole.net/main/prerequisites/). Aus diesem Grund funktioniert das übliche Pi-hole-Installationsskript nicht korrekt. Als [vorübergehende Abhilfe](https://github.com/pi-hole/pi-hole/issues/4693) könnt Ihr stattdessen die folgenden Befehle ausführen:

    ```bash
    sudo bash
    curl -sSL https://install.pi-hole.net | PIHOLE_SKIP_OS_CHECK=true bash
    ```

    Vergesst nicht, die Bash-Shell zu beenden, sobald die Installation erfolgreich abgeschlossen ist. Gebt dazu einfach folgenden Befehl ein, sobald Pi-hole installiert ist:

    ```
    exit
    ```


??? question "Hier erfahrt Ihr mehr zu DNS und Datenschutz"

    <center><img src="../../assets/img/dns.jpg" alt="DNS" width="70%"></img></center>

        Im Internet hat jeder Computer seine eigene [IP-Adresse](https://de.wikipedia.org/wiki/IP-Adresse). Da diese IP-Adressen nicht leicht zu merken sind, wurde das *Domain Name System* ([DNS](https://de.wikipedia.org/wiki/Domain_Name_System)) erfunden. Es funktioniert ein bisschen wie bei den ehemaligen Telefonzentralen: Jedes Mal, wenn Ihr die Adresse einer Webseite eingebt, sucht ein sogenannter DNS-Auflöser nach der entsprechenden IP und verbindet Euer Gerät mit dem passenden Rechner/Server. DNS spielt also eine Schlüsselrolle beim Surfen im Internet. Da jedoch alles automatisch abläuft, vergessen die Nutzer oft, dass DNS-Abfragen ein Datenschutz-Risiko darstellen:

    <center>

    | Risiken | Beschreibung |
    | ------ | ------ |
    |Aufzeichnung |DNS-Auflöser zeichnen jede Webseite auf, die Ihr besucht. Je nachdem, welchen DNS-Auflöser Ihr nutzt — oder vielmehr, welcher Euch auferlegt wurde — erfassen [Google](https://de.wikipedia.org/wiki/Google_Public_DNS), [Cloudflare](https://www.cloudflare.com/de-de/), Euer Internet- bzw. Telekom-Anbieter oder andere Drittanbieter eine Liste aller Webseiten, die Ihr besucht. Ob Euer Datenverkehr über HTTPS verschlüsselt ist oder nicht, spielt hierbei keine Rolle. |
    |Keine Verschlüsselung |DNS-Anfragen sind meist unverschlüsselt. Selbst wenn Ihr Eurem DNS-Auflöser blind vertrauen solltet, könnt Ihr theoretisch von Dritten abgehört oder manipuliert werden ([Spoofing-Angriff](https://de.wikipedia.org/wiki/Spoofing)). |
    |Zensur |Internet-Anbieter können ebenfalls Eure Onlineaktivitäten zensieren, indem sie DNS-Anfragen überwachen. |

    </center>

    Mit Pi-hole könnt Ihr selbst wählen, welchem DNS-Anbieter Ihr beim Surfen im Internet vertrauen wollt. Hier einige datenschutzfreundlichere DNS-Anbieter:

    <center>

    | DNS-Anbieter | Land | DNS #1 | DNS #2 |Datenschutz |
    | ------ | ------ | ------  |------ |------ |
    | [Digitalcourage](https://digitalcourage.de/support/zensurfreier-dns-server) | Deutschland | 5.9.164.112 | -- |[Datenschutz](https://digitalcourage.de/datenschutz-bei-digitalcourage) |
    | [UncensoredDNS](https://blog.uncensoreddns.org/) | Dänemark |89.233.43.71 | 91.239.100.100 |[Datenschutz](https://blog.uncensoreddns.org/faq/) |
    | [Dismail](https://dismail.de/info.html#dns/) | Deutschland |80.241.218.68 | 159.69.114.157 |[Datenschutz](https://dismail.de/datenschutz.html#dns) |
    | [DNS Watch](https://dns.watch/) | Deutschland | 84.200.69.80 | 84.200.70.40 | -- |
    | [FDN](https://www.fdn.fr/actions/dns/) | Frankreich | 80.67.169.12 | 80.67.169.40 | -- |
    | [OpenNIC](https://servers.opennic.org/) | Verschiedentlich | Verschiedentlich | Verschiedentlich | Verschiedentlich |

    </center>


??? question "Hier erfahrt Ihr mehr zu Werbeblockern"

    Pi-hole filtert den Internetverkehr und blockiert Werbung oder Tracker auf Euren Geräten, ohne dass Ihr zusätzliche Software benötigt. Über die Pi-hole-Weboberfläche könnt Ihr Blocklisten hinzufügen, um zusätzliche Werbung und Schadprogramme zu filtern. Beachtet jedoch, dass Pi-Hole keine Werbung in YouTube-Videos blockiert (dafür benötigt Ihr [uBlock Origin](https://gofoss.net/de/firefox/)).

    <center>

    | Blocklisten | Beschreibung |
    | ------ | ------ |
    |Standardlisten |• https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts <br>• https://mirror1.malwaredomains.com/files/justdomains <br>• http://sysctl.org/cameleon/hosts <br>• https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt |
    |Werbung |• https://adaway.org/hosts.txt <br>• https://v.firebog.net/hosts/AdguardDNS.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt <br>• https://v.firebog.net/hosts/Easylist.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/SpotifyAds/hosts <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts |
    |Tracking & Telemetrie |• https://v.firebog.net/hosts/Airelle-trc.txt <br>• https://v.firebog.net/hosts/Easyprivacy.txt <br>• https://v.firebog.net/hosts/Prigent-Ads.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/tyzbit/hosts |
    |Böswillige Webseiten |• https://v.firebog.net/hosts/Airelle-hrsk.txt <br>• https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt <br>• https://mirror1.malwaredomains.com/files/justdomains <br>• https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt <br>• https://www.malwaredomainlist.com/hostslist/hosts.txt <br>• https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt <br>• https://v.firebog.net/hosts/Prigent-Malware.txt <br>• https://v.firebog.net/hosts/Prigent-Phishing.txt <br>• https://v.firebog.net/hosts/Shalla-mal.txt <br>• https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts |

    </center>


??? question "Hier erfahrt Ihr mehr zu Reverse Proxies"

    Ein [Reverse Proxy](https://de.wikipedia.org/wiki/Reverse_Proxy) ist eine Anwendung, die zwischen einem auf Eurem Server laufenden Dienst und dem Internet sitzt. Alle auf unserer Webseite vorgestellten Dienste, einschließlich Pi-hole, werden als Apache Virtual Hosts hinter einem Reverse Proxy eingerichtet. Dies ermöglicht es:

    * zu verhindern, dass Euer Server direkt dem Internet ausgesetzt ist
    * mehrere Dienste nebeneinander auf Eurem Server laufen zu lassen
    * die Anzahl der offenen Ports zu minimieren (in der Regel auf die Ports 80 und 443 beschränkt)
    * Dienste über benutzerdefinierte, lokale Adressen zu erreichen, statt über irgendwelche IP-Adressen oder Portnummern
    * SSL-Zertifikate einfach zu verwalten
    * verschiedene Sicherheitsrichtlinien, wie HTTP-Header, Benutzerauthentifizierung, Zugriffsbeschränkungen usw. einzurichten

    Bei Ubuntu befinden sich die Konfigurationsdateien für Apache Virtual Hosts im Verzeichnis `etc/apache2/sites-available`. Die Struktur dieser Dateien sieht mehr oder weniger wie folgt aus:

    ```bash
    <VirtualHost *:80>

        ServerName              example.com                         # Adresse des Dienstes
        ServerAlias             www.example.com                     # Adresse des Dienstes, einschließlich www-Subdomäne
        Redirect permanent /    https://example.com/                # Umleitung des gesamten nicht verschlüsselten http:// Datenverkehrs auf verschlüsselten https:// Datenverkehr

    </VirtualHost>

    <VirtualHost *:443>

        ServerName              example.com                         # Adresse des Dienstes
        ServerAlias             www.example.com                     # Adresse des Dienstes, einschließlich www-Subdomäne
        ServerSignature         Off                                 # Serverinformationen verbergen

        SSLEngine               On                                  # SSL/TLS-Protokoll verwenden
        SSLProxyEngine          On                                  # SSL/TLS-Protokoll für den Proxy verwenden
        SSLCertificateFile      /path/to/fullchain.pem              # Speicherort der SSL-Zertifikatsdatei im PEM-Format
        SSLCertificateKeyFile   /path/to/privkey.pem                # Speicherort des PEM-kodierten privaten SSL-Schlüssels

        DocumentRoot            /var/www/example                    # Verzeichnis, aus dem Apache Dateien bereitstellt

        <Location />                                                # Beschränkung des Server-Zugriffs auf das Heimnetzwerk (LAN) und VPN
            Order deny,allow
            Deny from all
            Allow from 127.0.0.1
            Allow from 192.168.1.0/24
            Allow from 10.8.0.1/24
        </Location>

        <Directory /var/www/example>
            Options -Indexes +FollowSymLinks                        # Verzeichniseinträge verhindern und symbolischen Links folgen
            AllowOverride All                                       # Direktiven aus der .htaccess-Datei, die Konfigurationsdirektiven außer Kraft setzen können
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/example.com-error.log            # Log-Dateien
        CustomLog ${APACHE_LOG_DIR}/example.com-access.log combined # Log-Dateien

    </VirtualHost>
    ```

    Der virtuelle Host wird mit dem Befehl `sudo a2ensite example.com` aktiviert. Mit dem Befehl `sudo apachectl configtest` könnt Ihr vorab prüfen, ob Syntaxfehler vorhanden sind.


??? question "Hier erfahrt Ihr mehr zu Pi-hole-Terminalbefehlen"

    Ein Großteil der Einstellungen für Pi-hole kann über die Weboberfläche abgewickelt werden. Alternativ könnt Ihr Pi-hole auch direkt auf dem Server über das Terminal konfigurieren. Die vollständige Befehlsliste findet Ihr auf der [Pi-hole-Webseite](https://docs.pi-hole.net/core/pihole-command/):

    <center>

    | Befehle | Beschreibung |
    | ------ | ------ |
    | pihole status | Zeigt den Status von Pi-hole an. |
    | pihole -t tail log | Zeigt die Echtzeit-Protokollierung an. |
    | pihole -c | Zeigt Statistiken an. |
    | pihole -w -l | Zeigt die Domänen auf der Positivliste an. |
    | pihole -w example.com | Fügt example.com zur Positivliste hinzu. |
    | pihole -w -d example.com | Entfernt example.com von der Positivliste. |
    | pihole -b -l | Zeigt die Domänen auf der Negativliste an. |
    | pihole -b example.com | Fügt example.com zur Negativliste hinzu. |
    | pihole -b -d example.com | Entfernt example.com von der Negativliste. |
    | pihole -up | Aktualisiert Pi-hole. |
    | pihole -l off | Aktiviert die Protokollierung von Anfragen. |
    | pihole -l on | Deaktiviert die Protokollierung von Anfragen. |
    | pihole enable | Aktiviert Pi-hole. |
    | pihole disable | Deaktiviert Pi-hole. |
    | pihole disable 10m | Deaktiviert Pi-hole für 10 Minuten. |
    | pihole disable 60s | Deaktiviert Pi-hole für 60 Sekunden. |
    | pihole uninstall | Deinstalliert Pi-hole. |

    </center>


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="OpenVPN installieren" width="150px"></img> </center>

## Sicherer Fernzugriff mit OpenVPN

Bislang ist der Server nur von Eurem Heimnetzwerk aus zugänglich, da er durch eine Firewall vom Internet abgeschirmt ist. Um einen Fernzugriff zu ermöglichen könntet Ihr mehrere "Löcher" in die Firewall stanzen und so für jeden einzelnen Dienst Ports von Eurem Router an den Server weiterzuleiten. Damit wären jedoch mehrere Ports dem Internet ausgesetzt, was wiederum Angreifern eine größere Angriffsfläche bietet, um in Euer Netzwerk einzudringen.

Ein virtuelles privates Netzwerk ([VPN](https://de.wikipedia.org/wiki/Virtual_Private_Network)) ermöglicht es, sich relativ risikofrei von jedem Ort der Welt aus mit Eurem Server zu verbinden. Es wird nur ein einziger Port offengelegt, und es werden nur vorab genehmigte Verbindungen zugelassen, für die sowohl ein Zertifikat als auch ein Passwort erforderlich sind. Untenstehend findet Ihr eine ausführliche Anleitung.


??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### OpenVPN installieren

    [OpenVPN](https://openvpn.net/) ist quelloffen und verwendet OpenSSL, TLS und zahlreiche weitere Sicherheitsmechanismen. Es kann mit einem [einfachen Installationsskript](https://github.com/Nyr/openvpn-install/) eingerichtet werden:

    ```bash
    wget https://git.io/vpn -O openvpn-install.sh
    sudo bash openvpn-install.sh
    ```

    Folgt den Anweisungen auf dem Bildschirm:

    <center>

    | Anweisung | Beschreibung |
    | ------ | ------ |
    | What is the public IPv4 address or hostname? | Gebt Eure öffentliche IPv4-Adresse an, die Ihr z.B. auf [whatsmyip.org](https://www.whatsmyip.org/) ermitteln könnt. Zum Zwecke dieses Tutorials nehmen wir an, dass Eure öffentliche IP-Adresse `88.888.88.88` ist (passt dies entsprechen an).  |
    | Which protocol do you want for OpenVPN connections? | Wählt das empfohlene UDP-Protokoll aus. |
    | What port do you want OpenVPN listening to? | Wählt einen Port. Im Rahmen dieses Tutorials verwenden wir den Standardport `1194`. |
    | Which DNS do you want to use with the VPN? | Da Pi-hole als DNS-Server verwendet wird, wählt die Option `Current system resolvers`. |
    | Finally, tell me your name for the client certificate. | Gebt Eurem ersten VPN-Client-Zertifikat einen Namen, z.B. `computer_vpn` (passt den Namen entsprechend an). |

    </center>

    Sobald die Installation abgeschlossen ist, sollte das Terminal in etwa so aussehen:

    ```bash
    Welcome to this OpenVPN road warrior installer!

    I need to ask you a few questions before starting setup.
    You can use the default options and just press enter if you are ok with them.

    This server is behind NAT. What is the public IPv4 address or hostname?
    Public IPv4 address / hostname [88.888.88.88]: 88.888.88.88

    What IPv6 address should the OpenVPN server use?
        1) 1a00:a0a:10a:10a0:a00:00ff:faf0:a011
        2) 1a00:a0a:10a:10a0:a00:00bb:faf0:a022
    IPv6 address [1]: 1

    Which protocol do you want for OpenVPN connections?
    1) UDP (recommended)
    2) TCP
    Protocol [1]: 1

    What port do you want OpenVPN listening to?
    Port [1194]: 1194

    Which DNS do you want to use with the VPN?
    1) Current system resolvers
    2) 1.1.1.1
    3) Google
    4) OpenDNS
    5) NTT
    6) AdGuard
    DNS [1]: 1

    Finally, tell me a name for the client certificate.
    Client name [client]: computer_vpn

    We are ready to set up your OpenVPN server now.

    [etc.]

    Finished!

    The client configuration is available in: /root/computer_vpn.ovpn
    New clients can be added by running this script again.
    ```


    ### Zertifikate

    Ein erstes VPN-Client-Zertifikat, `computer_vpn.ovpn`, wurde während der Installation von OpenVPN erstellt. Ihr braucht ein Zertifikat pro Gerät, das sich mit dem Server verbinden soll: Desktop-Rechner, Laptops, Tabletts, Telefone usw. Erstellt so viele Zertifikate wie nötig, indem Ihr das Installationsskript erneut ausführt.

    Für die Zwecke dieses Tutorials erstellen wir ein zweites Zertifikat mit dem Namen `phone_vpn.ovpn`. Natürlich könnt Ihr jeden beliebigen Namen für Eure Zertifikate wählen. Stellt nur sicher, dass Ihr die Befehle entsprechend anpasst:

    ```bash
    sudo bash openvpn-install.sh
    ```

    Das Terminal sollte in etwa folgendes anzeigen:

    ```bash
    Looks like OpenVPN is already installed.

    What do you want to do?
    1) Add a new user
    2) Revoke an existing user
    3) Remove OpenVPN
    4) Exit
    Select an option: 1

    Tell me a name for the client certificate.
    Client name: phone_vpn

    [...]

    Write out database with 1 new entries
    Data Base Updated

    Client phone_vpn added, configuration is available at: /root/phone_vpn.ovpn
    ```

    Verschiebt schließlich das/die Zertifikat(e) in den `gofossadmin`-Home-Ordner und legt die richtigen Berechtigungen fest:

    ```bash
    cd
    sudo mv /root/computer_vpn.ovpn .
    sudo mv /root/phone_vpn.ovpn .
    sudo chmod 755 *.ovpn
    sudo chown gofossadmin:gofossadmin *.ovpn
    ```


    ### OpenVPN konfigurieren

    Öffnet den Port 1194 (UDP) bzw. den Port, den Ihr bei der Installation von OpenVPN angegeben habt:

    ```bash
    sudo ufw allow 1194/udp
    ```

    Vergewissert Euch, dass der Port erfolgreich zu den Firewall-Regeln hinzugefügt wurde:

    ```bash
    sudo ufw status numbered
    ```

    Überprüft, ob die virtuelle Schnittstelle `tun0` funktioniert, und ermittelt den Namen des Standardsubnetzes:

    ```bash
    ip ad | grep tun0
    ```

    Ermittelt die IP-Adresse des OpenVPN-Servers:

    ```bash
    ip route | grep tun0
    ```

    Vergewissert Euch, dass der Datenverkehr über den VPN-Tunnel geleitet wird:

    ```bash
    sudo apt install traceroute
    traceroute 10.8.0.1
    ```

    Als nächstes werden die Client-Geräte, die sich über VPN verbinden wollen, dazu aufgefordert Pi-hole als primären DNS-Server zu verwenden. Bearbeitet dazu die OpenVPN-Konfigurationsdatei:

    ```bash
    sudo vi /etc/openvpn/server/server.conf
    ```

    Kommentiert alle vorhandenen `dhcp-option`-Einstellungen aus (setzt dazu ein Hashtag `#` an den Anfang der jeweiligen Zeile) und ersetzt sie durch die Schnittstelle `tun0`:

    ```bash
    #push "dhcp-option DNS XX.XX.XXX.XXX"
    #push "dhcp-option DNS XX.XX.XXX.XXX"
    push "dhcp-option DNS 10.8.0.1"
    ```

    Startet den OpenVPN-Server neu:

    ```bash
    sudo systemctl restart openvpn-server@server
    ```

    Ruft die Pi-hole-Weboberfläche auf, in unserem Beispiel über `http://192.168.1.100/admin` (passt die Adresse entsprechend an). Ruft anschließend den Menü-Eintrag `Settings ‣ DNS ‣ Interface settings` auf und wählt die Option `Permit all origins`. Klickt anschließend auf `Save`.

    !!! warning "Router-Einstellungen"

        Überprüft die Einstellungen Eures Routers und stellt sicher, dass:

        * der von OpenVPN verwendete Port korrekt weitergeleitet wird (in unserem Beispiel Port `1194`, passt dies entsprechend an). Weitere Informationen findet Ihr im Handbuch des Routers
        * der Port `53` geschlossen ist. mit den Werkzeugen der [Gibson Research Corporation](https://www.grc.com/x/ne.dll?bh0bkyd2) nach offenen Ports suchen: Wählt `Proceed ‣ All service ports` und stellt sicher, dass Port `53` nicht als `open` erscheint


    ### Clients

    Nur Geräte mit gültigen Zertifikaten können eine VPN-Verbindung aufbauen und sicher auf selbst gehostete Dienste zugreifen. Nachfolgend erfahrt Ihr, wie Ihr die zuvor erstellten Zertifikate vom Server auf Eure Geräte übertragt.

    === "Desktop-Geräte (Ubuntu/Linux)"

        Wir gehen davon aus, dass auf Eurem Client-Gerät Ubuntu/Linux läuft und eine [SSH-Anmeldung](https://gofoss.net/de/ubuntu-server/) möglich ist. Öffnet ein Terminal auf dem Client-Gerät, wechselt zum Administrator-Benutzer (passt den Namen entsprechend an) und ruft alle Zertifikate vom Server ab:

        ```bash
        su - gofossadmin
        scp -v -P 2222 gofossadmin@192.168.1.100:/home/gofossadmin/*.ovpn .
        ```

        Konfiguriert schließlich die Netzwerkverbindung des Client-Geräts:

        <center>

        | Schritt | Anweisungen |
        | :------: | ------ |
        | 1 |Klickt auf das Wifi-Symbol in der oberen Leiste. |
        | 2 |Klickt auf `Einstellungen`. |
        | 3 |Ruft den Menü-Eintrag `Netzwerk` auf. |
        | 4 |Klickt auf das `+`-Symbol in der Rubrik `VPN`. |
        | 5 |Wählt `Aus Datei importieren`. |
        | 6 |Sucht nach dem Zertifikat (in unserem Beispiel `computer_vpn.ovpn`) und klickt auf `Öffnen`. |
        | 7 |Klickt auf das Wifi-Symbol in der oberen Leiste. |
        | 8 |Aktiviert die VPN-Verbindung `computer_vpn`. |

        </center>

        Das war's! Euer Desktop-Gerät kann sich von jedem Ort der Welt aus sicher über VPN mit dem Server verbinden.


    === "Mobile Geräte (Android)"

        Öffnet F-Droid auf Eurem mobilen Gerät und installiert [OpenVPN für Android](https://f-droid.org/de/packages/de.blinkt.openvpn/).

        Verbindet das mobile Gerät über USB mit dem Desktop-Gerät, mit dem Ihr alle Zertifikate vom Server abgerufen habt (siehe oben). Kopiert das passende Zertifikat auf das mobile Gerät (in unserem Beispiel heißt das Zertifikat `phone_vpn.ovpn`, passt den Namen entsprechend an).

        Öffnt schließlich die OpenVPN-App auf Eurem mobilen Gerät und konfiguriert sie wie folgt:

        <center>

        | Schritt | Anweisung |
        | :------: | ------ |
        | 1 |Tippt auf das `+`-Symbol und dann `Importieren`. |
        | 2 |Sucht nach dem Zertifikat (in unserem Beispiel `phone_vpn.ovpn`) und importiert es. |

        </center>

        Das war's! Euer mobiles Gerät kann sich von jedem Ort der Welt aus sicher über VPN mit dem Server verbinden.


<br>

<center> <img src="../../assets/img/separator_tasks.svg" alt="Abschließende Überprüfungen" width="150px"></img> </center>

## Abschließende Überprüfungen

Nachdem Ihr alles eingerichtet habt, sollte der Datenverkehr wie folgt abgewickelt werden:

* Jedes Mal, wenn eines Eurer Geräte eine Adresse aufruft, verbindet es sich erst mal sicher über VPN mit Eurem Server
* Enthält die Adresse Werbung oder Tracker, blockiert Pi-hole diese Elemente
* Verweist die Adresse auf einen selbst gehosteten Dienst, leitet Pi-hole die Anfrage auf dem Server weiter
* Verweist die Adresse auf eine externe Webseite oder einen externen Dienst, leitet Pi-hole die Anfrage an einen vorgelagerten DNS-Server Eurer Wahl weiter (z.B. Digitalcourage, UncensoredDNS, usw.)
* Der gesamte Datenverkehr wird über HTTPS verschlüsselt

Vergewissert Euch, dass alles reibungslos läuft:

* Ruft [ads-blocker.com](https://ads-blocker.com/testing/) oder [cnn.com](https://edition.cnn.com/) auf und überprüft, ob Werbung blockiert wird. Ihr könnt auch das Pi-hole-Dashboard unter `https://mypihole.gofoss.duckdns.org` auf blockierte Abfragen überprüfen (passt die URL entsprechend an)
* Ruft einen lokalen Dienst wie `https://mypihole.gofoss.duckdns.org` auf und überprüft, ob die Adresse korrekt aufgelöst wird (passt die URL entsprechend an)
* Ruft [dnsleaktest.com](https://www.dnsleaktest.com/) oder [bash.ws](https://bash.ws/dnsleak) auf und stellt sicher, dass Ihr den richtigen Upstream-DNS-Auflöser verwendet


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr in der [Pi-hole Dokumentation](https://docs.pi-hole.net/), [Let's Encrypt Dokumentation](https://letsencrypt.org/de/docs/) oder [OpenVPN Dokumentation](https://openvpn.net/vpn-server-resources/). Ihr könnt auch gerne die [Pi-hole Gemeinschaft](https://discourse.pi-hole.net/), [Let's Encrypt Gemeinschaft](https://community.letsencrypt.org/) oder [OpenVPN Gemeinschaft](https://openvpn.net/community/) um Hilfe bitten.


</br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/google_announcement.png" alt="Google Ankündigung" width="250"></img>
</div>

</br>
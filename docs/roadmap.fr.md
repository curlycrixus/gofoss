---
template: main.html
title: Un aperçu de l'avenir
description: La feuille de route et le journal des modifications de gofoss.net. Un aperçu de l'avenir. Un regard dans le rétroviseur. Nouvelles fonctionnalités. Mises à niveau. Mises à jour du contenu. Corrections de problèmes.
---

# Feuille de route et journal des modifications de gofoss.net

<center> <img src="../../assets/img/roadmap.png" alt="Feuille de route" width="500px"></img> </center>

Nous continuons à ajouter du contenu et des fonctionnalités, à créer des versions locales et à maintenir le site. Les mises à jour mineures sont dédiées aux corrections et la maintenance, alors que les mises à jour majeures, plus espacées, introduisent de nouvelles fonctionnalités et du contenu.

##Feuille de route

* Ajouter une version espagnole ([#50](https://gitlab.com/curlycrixus/gofoss/-/issues/50))
* Ajouter une section de nouvelles ou d'annonces ([#68](https://gitlab.com/curlycrixus/gofoss/-/issues/68))
* Mettre à jour [Material pour MkDocs](https://squidfunk.github.io/mkdocs-material/upgrading/)
* Mettre à jour d'E-Charts [E-Charts](https://echarts.apache.org/en/index.html)
* Mettre à jour et clarifier les instructions pour les contributeurs et les gestionnaires du site ([#9](https://gitlab.com/curlycrixus/gofoss/-/issues/9), [#10](https://gitlab.com/curlycrixus/gofoss/-/issues/10))
* Ajouter Session au chapitre sur les messageries chiffrées ([#29](https://gitlab.com/curlycrixus/gofoss/-/issues/29))
* Ajouter Delta Chat au chapitre sur les messageries chiffrées ([#67](https://gitlab.com/curlycrixus/gofoss/-/issues/67))
* Passer en revue les instructions iOS, macOS & Windows pour le chapitre sur les messageries chiffrées ([#65](https://gitlab.com/curlycrixus/gofoss/-/issues/65))
* Ajouter GPG au chapitre sur les courriels chiffrés ([#62](https://gitlab.com/curlycrixus/gofoss/-/issues/62))
* Ajouter une note sur la confidentialité de Protonmail au chapitre sur les courriels chiffrés ([#66](https://gitlab.com/curlycrixus/gofoss/-/issues/66))
* Ajouter Bitwarden au chapitre sur les mots de passe ([#30](https://gitlab.com/curlycrixus/gofoss/-/issues/30))
* Remplacer Yubikey dans le chapitre sur les mots de passe ([#63](https://gitlab.com/curlycrixus/gofoss/-/issues/63))
* Ajouter RaivoOTP au chapitre sur les mots de passe ([#71](https://gitlab.com/curlycrixus/gofoss/-/issues/71))
* Ajouter PinePhone au chapitre sur les téléphones dégooglés ([#41](https://gitlab.com/curlycrixus/gofoss/-/issues/41))
* Ajouter GrapheneOS au chapitre sur les téléphones dégooglés ([#48](https://gitlab.com/curlycrixus/gofoss/-/issues/48))
* Ajouter /e/ au chapitre sur les téléphones dégooglés ([#53](https://gitlab.com/curlycrixus/gofoss/-/issues/53))
* Passer en revue la liste des applis FOSS ([#64](https://gitlab.com/curlycrixus/gofoss/-/issues/64))
* Mettre à jour les instructions pour Ubuntu LTS 22.04 ([#69](https://gitlab.com/curlycrixus/gofoss/-/issues/69))
* Ajouter Linux Mint & Pop OS! au chapitre sur les ordinateurs Linux ([#49](https://gitlab.com/curlycrixus/gofoss/-/issues/49))
* Mettre à jour le chapitre sur la sécurité des serveurs ([#70](https://gitlab.com/curlycrixus/gofoss/-/issues/70))
* Mettre à jour les vidéos des tâches cron ([#17](https://gitlab.com/curlycrixus/gofoss/-/issues/17))
* Ajouter Nextcloud au chapitre sur les services en nuage ([#51](https://gitlab.com/curlycrixus/gofoss/-/issues/51))
* Améliorer la lisibilité et l'intelligibilité du site ([#15](https://gitlab.com/curlycrixus/gofoss/-/issues/15))
* Corriger les liens défectueux
* Enlever l'extension mkdocs-static-i18n ([#5](https://gitlab.com/curlycrixus/gofoss/-/issues/5))
* Corriger la recherche multi-langue ([#4](https://gitlab.com/curlycrixus/gofoss/-/issues/4))


##Journal des modifications

###Janvier 2022

* Migré vers [https://gofoss.net](https://gofoss.net/fr/) ([#80](https://gitlab.com/curlycrixus/gofoss/-/issues/80))
* Publié le code source sur Gitlab ([#57](https://gitlab.com/curlycrixus/gofoss/-/issues/57))
* Changé la licence vers AGPLv3 ([#72](https://gitlab.com/curlycrixus/gofoss/-/issues/72))
* Remanié la mise en page : page d'accueil, polices, icônes, fichiers png, etc. ([#7](https://gitlab.com/curlycrixus/gofoss/-/issues/7), [#14](https://gitlab.com/curlycrixus/gofoss/-/issues/14), [#79](https://gitlab.com/curlycrixus/gofoss/-/issues/79))
* Mis à jour Material pour MkDocs ([#34](https://gitlab.com/curlycrixus/gofoss/-/issues/34))
* Mis à jour d'E-Charts ([#14](https://gitlab.com/curlycrixus/gofoss/-/issues/14))
* Migré les liens Reddit & Twitter vers Teddit & Nitter ([#13](https://gitlab.com/curlycrixus/gofoss/-/issues/13))
* Ajouter un thème sombre ([#3](https://gitlab.com/curlycrixus/gofoss/-/issues/3))
* Ajouté la prise en charge multi-langue & les versions locales : anglais, français & allemand ([#2](https://gitlab.com/curlycrixus/gofoss/-/issues/2))
* Ajouté un fichier README ([#36](https://gitlab.com/curlycrixus/gofoss/-/issues/36))
* Ajouté des lignes directrices pour les contribution ([#8](https://gitlab.com/curlycrixus/gofoss/-/issues/8), [#73](https://gitlab.com/curlycrixus/gofoss/-/issues/73), [#74](https://gitlab.com/curlycrixus/gofoss/-/issues/74), [#77](https://gitlab.com/curlycrixus/gofoss/-/issues/77))
* Ajouté des modèles pour les « Issue & Merge » ([#45](https://gitlab.com/curlycrixus/gofoss/-/issues/45))
* Ajouté un aperçu de navigateurs au chapitre sur Firefox ([#12](https://gitlab.com/curlycrixus/gofoss/-/issues/12), [#22](https://gitlab.com/curlycrixus/gofoss/-/issues/22))
* Ajouté des fournisseurs VPN : RiseupVPN, CalyxVPN ([#26](https://gitlab.com/curlycrixus/gofoss/-/issues/26))
* Ajouté des messageries chiffrées : Element, Jami, Briar ([#24](https://gitlab.com/curlycrixus/gofoss/-/issues/24), [#27](https://gitlab.com/curlycrixus/gofoss/-/issues/27), [#33](https://gitlab.com/curlycrixus/gofoss/-/issues/33), [#42](https://gitlab.com/curlycrixus/gofoss/-/issues/42))
* Ajouté FreeFileSync au chapitre sur les sauvegardes serveur ([#21](https://gitlab.com/curlycrixus/gofoss/-/issues/21))
* Ajouté des fournisseurs de services en nuage: Chatons, Digitalcourage, Picasoft ([#19](https://gitlab.com/curlycrixus/gofoss/-/issues/19), [#25](https://gitlab.com/curlycrixus/gofoss/-/issues/25), [#31](https://gitlab.com/curlycrixus/gofoss/-/issues/31))
* Ajouté un chapitre sur le Fediverse: Mastodon, PeerTube, PixelFed, Friendica, Lemmy, Funkwhale, etc. ([#40](https://gitlab.com/curlycrixus/gofoss/-/issues/40))
* Ajouté Electronmail à la section sur Protonmail ([#28](https://gitlab.com/curlycrixus/gofoss/-/issues/28))
* Ajouté PhotoPrism au chapitre Piwigo ([#59](https://gitlab.com/curlycrixus/gofoss/-/issues/59))
* Ajouté Xiaomi Mi A2 au chapitre sur CalyxOS ([#61](https://gitlab.com/curlycrixus/gofoss/-/issues/61))
* Mis à jour les permissions dans le chapitre sur la sécurité des serveurs ([#11](https://gitlab.com/curlycrixus/gofoss/-/issues/11))
* Mis à jour le chapitre sur le domaine sécurisé ([#18](https://gitlab.com/curlycrixus/gofoss/-/issues/18), [#44](https://gitlab.com/curlycrixus/gofoss/-/issues/44))
* Mis à jour la feuille de route et le journal des modifications ([#58](https://gitlab.com/curlycrixus/gofoss/-/issues/58), [#38](https://gitlab.com/curlycrixus/gofoss/-/issues/38), [#75](https://gitlab.com/curlycrixus/gofoss/-/issues/75))
* Mis à jour la liste des applis FOSS ([#23](https://gitlab.com/curlycrixus/gofoss/-/issues/23), [#47](https://gitlab.com/curlycrixus/gofoss/-/issues/47))
* Mis à jour la liste des applis Ubuntu ([#39](https://gitlab.com/curlycrixus/gofoss/-/issues/39), [#43](https://gitlab.com/curlycrixus/gofoss/-/issues/43), [#46](https://gitlab.com/curlycrixus/gofoss/-/issues/46), [#56](https://gitlab.com/curlycrixus/gofoss/-/issues/56))
* Mis à jour les remerciements ([#37](https://gitlab.com/curlycrixus/gofoss/-/issues/37))
* Corrigé les tâches cron pour dehydrated & ClamAV ([#16](https://gitlab.com/curlycrixus/gofoss/-/issues/16))
* Corrigé le DNS de Digitalcourage ([#20](https://gitlab.com/curlycrixus/gofoss/-/issues/20))
* Corrigé les liens défectueux et les services caduques ([#6](https://gitlab.com/curlycrixus/gofoss/-/issues/6))
* Corrigé les en-têtes des tableaux ([#35](https://gitlab.com/curlycrixus/gofoss/-/issues/35))


###Mai 2021

* Migré de Docsify vers Material pour MkDocs
* Ajouté des vidéos
* Ajouté des liens vers les réseaux sociaux ([#13](https://gitlab.com/curlycrixus/gofoss/-/issues/13))
* Revu la mise en page : page d'accueil, navigation, tableaux, couleurs, polices, sections, etc.
* Mis à jour d'E-Charts
* Introduit un nouveau système pour les utilisateurs débutants, intermédiaires et avancés ([#54](https://gitlab.com/curlycrixus/gofoss/-/issues/54))
* Mis à jour de tous les chapitres du site
* Ajouté Arkenfox à la section user.js
* Ajouté plus de détails sur le chiffrement de Protonmail ([#52](https://gitlab.com/curlycrixus/gofoss/-/issues/52))
* Mis à jour la liste des applis Ubuntu
* Ajouté un chapitre sur les domaines sécurisés
* Mis à jour les remerciements
* Ajouté une feuille de route
* Supprimé les liens vers les CDNs
* Supprimé NordVPN ([#32](https://gitlab.com/curlycrixus/gofoss/-/issues/32))


###Décembre 2020

* Revue de l'identité visuelle
* Mis à jour d'E-Charts
* Mis à jour du contenu du site


###Août 2020

* Publié la version initiale de [https://gofoss.today](https://gofoss2020.netlify.app/#/)


<br>
---
template: main.html
title: Comment utiliser un VPN
description: Comment fonctionne un VPN ? Comment utiliser un VPN ? VPN est-il illégal ? VPN peut-il être tracé ? VPN est-il gratuit ? VPN gratuit. Signification de VPN.
---

# Comment utiliser un VPN <br> (pour cacher votre emplacement et plus encore)

!!! level "Dernière mise à jour: mars 2022. Destiné aux utilisateurs expérimentés. Certaines compétences techniques sont requises."

<div align="center">
<img src="../../assets/img/vpn_connection.png" alt="VPN" width="500px"></img>
</div>

Le terme [réseau privé virtuel](https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9_virtuel) (en anglais, « Virtual Private Network » ou VPN) fait souvent référence à une forme de chiffrement de la couche transport. Cela présente toutes sortes d'avantages, comme par exemple de masquer votre emplacement pour accéder à des sites géobloqués. Lorsque vous vous connectez à un VPN, le trafic émis ou reçu par votre appareil passe par une sorte de tunnel chiffré. Si quelqu'un vous surveille, par exemple votre fournisseur d'accès à Internet ou un attaquant, il pourra uniquement constater que vous transmettez des données. Il ne connaîtra ni leur destination ni leur contenu. Cependant, d'autres acteurs avec lesquels vous interagissez en ligne, comme les moteurs de recherche, les banques ou les fournisseurs de messagerie électronique, sont en mesure de consulter, de stocker et de modifier vos données. Il en va de même pour votre fournisseur VPN.

Choisissez un fournisseur VPN digne de confiance afin de protéger votre activité en ligne. Vous trouverez de nombreux comparatifs et conseils en ligne. Pour obtenir de l'aide, vous pouvez aussi faire appel à la [communauté VPN sur Reddit](https://teddit.net/r/VPN/). Les fournisseurs présentes ci-dessous ont une bonne réputation en termes de situation géographique, de collecte de données, de paiement, de niveau de sécurité, de disponibilité, de prix et d'éthique.

??? warning "Un VPN va-t-il cacher mon emplacement ? Est-ce qu'un VPN peut être tracé ?"

    Même si vous chiffrez votre trafic avec un VPN (ou [Tor](https://gofoss.net/fr/tor/)), cela ne signifie pas nécessairement que vous êtes anonyme ou en sécurité. Par exemple, alors que certains sites web expliquent comment masquer votre localisation à l'aide d'un VPN, votre appareil peut toujours être exposé à la relevé d'empreintes de votre navigateur, aux [« attaques de l'homme du milieu »](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu), aux attaques par corrélation ou à d'autres piratages. Notez par ailleurs que [nous expliquerons dans un chapitre ultérieur comment configurer OpenVPN pour communiquer avec votre serveur auto-hébergé](https://gofoss.net/fr/secure-domain/). Cette configuration n'est pas destinée à protéger votre identité en ligne.

<br>

<center> <img src="../../assets/img/separator_protonvpn.svg" alt="Proton VPN" width="150px"></img> </center>

## ProtonVPN

[ProtonVPN](https://protonvpn.com/fr/) affirme être le seul fournisseur VPN gratuit sans publicité et sans limite de vitesse. Il offre un large éventail de fonctionnalités, telles que le « kill switch », la protection contre les fuites DNS, le « tunneling » divisé ou le chiffrement du trafic via deux sites.

ProtonVPN est basé en Suisse, en dehors des pays de la liste des [« quatorze yeux »](https://fr.wikipedia.org/wiki/Five_Eyes). L'entreprise affirme être un [fournisseur VPN sans collecte de données](https://protonvpn.com/privacy-policy), à l'exception de de l'horodatage de la dernière connexion réussie, qui sert à protéger les utilisateurs contre des attaques.

Le [logiciel client de ProtonVPN est open source](https://github.com/ProtonVPN/), tout le monde peut donc en examiner le code. L'application Android [ne contient aucun traqueur et nécessite 7 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/ch.protonvpn.android/latest/). ProtonVPN semble utiliser des [serveurs dédiés](https://protonvpn.com/vpn-servers/).

Il existe une version gratuite du logiciel, avec toutefois certaines limitations : 1 seule connexion VPN, des seveurs disponibles dans seulement 3 pays et une vitesse limitée. L'accès à des fonctionnalités plus poussées nécessite un compte payant, qui démarre à environ 4 €/mois. ProtonVPN accepte les paiements anonymes.


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Mullvad Mozilla VPN" width="150px"></img> </center>

## Mozilla / Mullvad

[Mullvad](https://mullvad.net/fr/) affirme être un fournisseur VPN rapide, fiable et facile à utiliser. Mullvad fournit également [la solution VPN de Mozilla](https://www.mozilla.org/fr/products/vpn/).

Mullvad est basé en Suède, qui fait partie des pays de la liste des [« quatorze yeux »](https://fr.wikipedia.org/wiki/Five_Eyes). L'entreprise affirme être un [fournisseur de VPN sans collecte de données](https://mullvad.net/en/help/no-logging-data-policy/).

Le [logiciel client de Mullvad est open source](https://github.com/mullvad/), tout le monde peut donc en examiner le code. L'application Android [ne contient pas de traqueurs et nécessite 4 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/net.mullvad.mullvadvpn/latest/). Mullvad [opère des serveurs dans le monde entier](https://mullvad.net/fr/servers/), dont la plupart sont loués.

Au moment d'écrire ces lignes, les abonnements démarre à environ 5 €/mois. Mullvad accepte les paiements anonymes et ne nécessite pas d'email pour s'inscrire.


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="RiseupVPN" width="150px"></img> </center>

## RiseupVPN

[RiseupVPN](https://riseup.net/fr/vpn) affirme offrir aider ses utilisatrices et utilisateurs à contourner la censure, à anonymiser leur géolocalisation et à chiffrer leur trafic.

RiseUp est un collectif géré par des bénévoles, créé en 1999. L'organisation, dont le siège est à Seattle, est situé dans un des pays de la liste des [« quatorze yeux »](https://fr.wikipedia.org/wiki/Five_Eyes). RiseUp affirme être un [fournisseur de VPN sans collecte de données](https://riseup.net/fr/privacy-policy).

Les clients de RiseupVPN sont basés sur le logiciel open source [Bitmask](https://bitmask.net/). L'application Android [ne contient aucun traqueur et nécessite 8 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/se.leap.riseupvpn/latest/).

Au moment d'écrire ces lignes, RiseupVPN était gratuit. Le service est entièrement financé par des dons.


<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="CalyxVPN" width="150px"></img> </center>

## CalyxVPN

Le [CalyxVPN](https://calyx.net/) est mis à disposition par l'Institut Calyx, une organisation à but non lucratif fondée en 2010. Elle est situé dans un des pays de la liste des [« quatorze yeux »](https://fr.wikipedia.org/wiki/Five_Eyes).

La [politique de confidentialité](https://calyxinstitute.org/legal/privacy-policy/) de l'Institut Calyx affirme que les adresses IP peuvent être enregistrées, mais sont supprimées régulièrement.

Les clients de CalyxVPN sont basés sur le logiciel open source [Bitmask](https://bitmask.net/). L'application Android [ne contient aucun traqueur et nécessite 8 autorisations](https://reports.exodus-privacy.eu.org/fr/reports/org.calyxinstitute.vpn/latest/).

Au moment d'écrire ces lignes, CalyxVPN était gratuit.


<br>

---
template: main.html
title: Tutanota vs Protonmail vs Gmail | Easy Email Encryption
description: Looking for email privacy? Don't want to host your own email server? We are comparing privacy email providers Protonmail vs Tutanota (incl. Protonmail Bridge).
---

# Get The Best Email Encryption Software

!!! level "Last updated: May 2022. For beginners & intermediate users. Some tech skills may be required."

<div align="center">
<img src="../../assets/img/https5.png" alt="Email encryption" width="250px"></img>
</div>

Choose a secure email service. While you are scouting for the best privacy email provider, carefully assess aspects such as available features, email encryption technologies, or server locations — privacy legislation changes from country to country. This chapter provides an (incomplete) overview of popular, privacy respecting email providers: Tutanota vs Protonmail (and more). It also explains how to use a PGP encryption tool for emails.


<br>


<center> <img src="../../assets/img/separator_protonmail.svg" alt="Encrypted email meaning" width="150px"></img> </center>

## What is Protonmail used for?

[Protonmail](https://protonmail.com/) claims to be the world's largest secure email service, protected by Swiss privacy laws. It's amongst others funded by US investors (Charles River Ventures) and the European Union. While Protonmail's apps are [open source](https://github.com/ProtonMail/), the server-side is not.

At the time of writing, the basic single user account offered 500 MB storage for free. For 4 to 24 EUR/month, you get access to more users and storage, as well as a plethora of features: calendar, contact and email imports, bitcoin payments, [VPN](https://gofoss.net/vpn/), and more.

??? warning "Some words of advice on encryption"

    <center>

    | Emails | Encryption |
    | ------ | ------ |
    | **Sent between Protonmail users** |Message body and attachments are end-to-end encrypted. Subject lines and recipient/sender addresses are not. |
    | **Sent from Protonmail users to other providers** |Message body and attachments are only end-to-end encrypted if the user selects the `Encrypt for Outside` option. Otherwise, only TLS encryption is applied if the receiving mail server supports it (which also means that the receiving provider can read the message). In any case, subject lines and recipient/sender addresses are not end-to-end encrypted.|
    | **Received by Protonmail users from other providers** | Message body and attachments are only encrypted with TLS, if the sender's mail server supports it. Subject lines and recipient/sender addresses are not end-to-end encrypted. |

    </center>


### Protonmail clients (incl. Protonmail bridge)

Besides [webmail](https://account.protonmail.com/login/) access, Protonmail offers mobile apps for Android and iOS. On desktop environments, Protonmail works with [Thunderbird](https://www.thunderbird.net/en-GB/) via a so-called Bridge application. This feature is however only available to paid accounts. Alternatively, [ElectronMail](https://github.com/vladimiry/ElectronMail) is a free and open source desktop client for Protonmail. Mind however that ElectronMail is an unofficial app. More detailed instructions below.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

         Simply download the Protonmail app from [Google's Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=en&gl=US) or [Aurora Store](https://auroraoss.com/). It [contains 0 trackers and requires 14 permissions](https://reports.exodus-privacy.eu.org/en/reports/ch.protonmail.android/latest/). By comparison: for Gmail it's 1 tracker and 55 permissions; for Outlook it's 13 trackers and 49 permissions; and for Hotmail it's 4 trackers and 31 permissions.


=== "iOS"

    ??? tip "Show me the step-by-step guide iOS"

         Simply download the Protonmail app from the [App Store](https://apps.apple.com/us/app/protonmail-encrypted-email/id979659905/).


=== "Windows"

    ??? tip "Show me the step-by-step guide for ElectronMail on Windows (no paid account needed)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download ElectronMail |Download and run the [ElectronMail installer for Windows](https://github.com/vladimiry/ElectronMail/releases). |
        | Create a master password |Open ElectronMail and provide a [strong, unique master password](https://gofoss.net/passwords/) to protect your emails. |
        | Login |Provide your Protonmail credentials, including two-factor authentication if activated. |
        | Domain |Choose a domain from the list. There is even an `Onion` option to use Tor. Then click on `Close`. |

        </center>


    ??? tip "Show me the step-by-step guide for Thunderbird on Windows (paid accounts only)"

        ### Install Thunderbird on Windows

        Navigate to [Thunderbird's download page](https://www.thunderbird.net/en-US/) and click on the `Free Download` button. Once the installer is downloaded, click on the `Run` button and follow the installation wizard.

        ### Install Protonmail Bridge on Windows

        Thunderbird integrates nicely with Protonmail, making sure emails stay encrypted when they enter and leave your computer. This is handled by the so-called Bridge application, a software available to paid users only. [Download Protonmail Bridge for Windows](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/). Once the installer is downloaded, click on the "Run" button and follow the installation wizard.

        ### Configure Protonmail Bridge on Windows

        Open the freshly installed Protonmail Bridge application and follow the setup wizard:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Log into your Protonmail account. |
        | 2 | Click on your account name and then the `Mailbox configuration` button. |
        | 3 | A window with the title `Protonmail Bridge Mailbox Configuration` should pop up. It displays IMAP and SMTP settings, including a password, needed later on to configure Thunderbird. |

        </center>

        ### Configure Thunderbird on Windows

        Now launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Email Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Your name | Enter the name you want others to see. |
        | Email address | Enter your Protonmail email address. |
        | Password | Copy and paste the password from the `Protonmail Bridge Mailbox Configuration` window (do not enter your Protonmail password, it won't work). |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Manual config | Click on the `Manual config` button, and fill out the IMAP and SMTP settings provided in the `Protonmail Bridge Mailbox Configuration` window (for Authentication, select `Normal password`). |
        | Re-test | Click on the `Re-test` button to verify your connection settings. |
        | Advanced config | Click on the `Advanced config` button. A new window appears. Just click on the `OK` button, do not modify any settings in this window. |
        | Add Security Exception | Click on the `Confirm Security Exception` button in the pop-up window. This confirms that your computer (127.0.0.1) can run the Bridge app. You might have to confirm a second security exception later on, once you send your first email. |

        </center>

    ??? tip "Show me the 3-minute summary video for Thunderbird (paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Courtesy of Protonmail. Instructions should similarly apply to macOS or Linux.*

        </center>


=== "macOS"

    ??? tip "Show me the step-by-step guide for ElectronMail on macOS (no paid account needed)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download ElectronMail |Download the [ElectronMail disk image](https://github.com/vladimiry/ElectronMail/releases), open it and drag the ElectronMail icon on top of the Application folder. For easy access, open the Applications folder and drag the ElectronMail icon to your dock. |
        | Create a master password |Open ElectronMail and provide a [strong, unique master password](https://gofoss.net/passwords/) to protect your emails. |
        | Login |Provide your Protonmail credentials, including two-factor authentication if activated. |
        | Domain |Choose a domain from the list. There is even an `Onion` option to use Tor. Then click on `Close`. |

        </center>


    ??? tip "Show me the step-by-step guide for Thunderbird on macOS (paid accounts only)"

        ### Install Thunderbird on macOS

        Navigate to [Thunderbird's download page](https://www.thunderbird.net/en-US/) and click on the `Free Download` button. Once the installer is downloaded, it should open by itself and mount a new volume containing the Thunderbird application. If not, open the downloaded Thunderbird .dmg file and drag the appearing Thunderbird icon on top of the Application folder. For easy access, open the Applications folder and drag the Thunderbird icon to your dock.

        ### Install Protonmail Bridge on macOS

        Thunderbird integrates nicely with Protonmail, making sure emails stay encrypted when they enter and leave your computer. This is handled by the so-called Bridge application, available to paid users only. [Download Protonmail Bridge for macOS](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/). Once the installer is downloaded, it should start by itself and mount a new volume containing the Protonmail application. If not, open the downloaded Protonmail Bridge `.dmg` file and drag the Protonmail icon on top of the Application folder. For easy access, open the Applications folder and drag the Protonmail Bridge icon to your dock.

        ### Configure Protonmail Bridge on macOS

        Open the freshly installed Protonmail Bridge application and follow the setup wizard:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Log into your Protonmail account. |
        | 2 | Click on your account name and then the `Mailbox configuration` button. |
        | 3 | A window with the title `Protonmail Bridge Mailbox Configuration` should pop up. It displays IMAP and SMTP settings, including a password, needed later on to configure Thunderbird. |

        </center>

        ### Configure Thunderbird on macOS

        Now launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Email Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Your name | Enter the name you want others to see. |
        | Email address | Enter your Protonmail email address. |
        | Password | Copy and paste the password from the `Protonmail Bridge Mailbox Configuration` window (do not enter your Protonmail password, it won't work). |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Manual config | Click on the `Manual config` button, and fill out the IMAP and SMTP settings provided in the `Protonmail Bridge Mailbox Configuration` window (for Authentication, select `Normal password`). |
        | Re-test | Click on the `Re-test` button to verify your connection settings. |
        | Advanced config | Click on the `Advanced config` button. A new window appears. Just click on the `OK` button, do not modify any settings in this window. |
        | Add Security Exception | Click on the `Confirm Security Exception` button in the pop-up window. This confirms that your computer (127.0.0.1) can run the Bridge app. You might have to confirm a second security exception later on, once you send your first email. |

        </center>

    ??? tip "Show me the 3-minute summary video for Thunderbird (paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Courtesy of Protonmail. Instructions should similarly apply to macOS or Linux.*

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for ElectronMail on Ubuntu Linux (no paid account needed)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Download ElectronMail |Download the latest [ElectronMail .deb package](https://github.com/vladimiry/ElectronMail/releases). The file should be named something like `electron-mail-X-XX-X-linux-amd64.deb`. For the purpose of this tutorial, let's suppose the file was downloaded to the folder `/home/gofoss/Downloads`. Make sure to adjust these file paths according to your own setup. Now open the terminal with the `Ctrl+Alt+T` shortcut or click on the `Applications` button on the top left and search for `Terminal`. Finally, run the following commands:<br><br> `cd /home/gofoss/Downloads` <br>`sudo dpkg -i electron-mail-X-XX-X-linux-amd64.deb` |
        | Create a master password |Open ElectronMail and provide a [strong, unique master password](https://gofoss.net/passwords/) to protect your emails. |
        | Login |Provide your Protonmail credentials, including two-factor authentication if activated. |
        | Domain |Choose a domain from the list. There is even an `Onion` option to use Tor. Then click on `Close`. |

        </center>


    ??? tip "Show me the step-by-step guide for Thunderbird on Ubuntu Linux (paid accounts only)"

        ### Install Thunderbird on Linux

        If you run a Linux distribution such as [Ubuntu](https://gofoss.net/ubuntu/), open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. Run the following command to install Thunderbird:

        ```bash
        sudo apt install thunderbird
        ```

        ### Install Protonmail Bridge Linux

        Thunderbird integrates nicely with Protonmail, making sure emails stay encrypted when they enter and leave your computer. This is handled by the so-called Bridge application, available to paid users only. [Download Protonmail Bridge Linux](https://protonmail.com/support/knowledge-base/protonmail-bridge-install/). The file should be called something similar to `protonmail-bridge_X.X.X-X_amd64.deb`. Let's assume it has been downloaded to the folder `/home/gofoss/Downloads`. Open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. Then run the following commands (don't forget to adjust the filename and download folder path accordingly):

        ```bash
        sudo apt install gdebi
        cd /home/gofoss/Downloads
        sudo gdebi protonmail-bridge_X.X.X-X_amd64.deb
        ```

        ### Configure Protonmail Bridge Linux

        Open the Bridge application with the terminal command `protonmail-bridge`, or click on the `Applications` button on the top left, and search for `ProtonMail Bridge`. Follow the setup wizard:

        <center>

        | Steps | Description |
        | :------: | ------ |
        | 1 | Log into your Protonmail account. |
        | 2 | Click on your account name and then the `Mailbox configuration` button. |
        | 3 | A window with the title `Protonmail Bridge Mailbox Configuration` should pop up. It displays the Protonmail server settings, including IMAP, SMTP and a password needed later on to configure Thunderbird. |

        </center>

        ### Configure Thunderbird on Linux

        Now launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Email Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Your name | Enter the name you want others to see. |
        | Email address | Enter your Protonmail email address. |
        | Password | Copy and paste the password from the `Protonmail Bridge Mailbox Configuration` window (do not enter your Protonmail password, it won't work). |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Manual config | Click on the `Manual config` button, and fill out the IMAP and Protonmail SMTP settings provided in the `Protonmail Bridge Mailbox Configuration` window (for Authentication, select `Normal password`). |
        | Re-test | Click on the `Re-test` button to verify your connection settings. |
        | Advanced config | Click on the `Advanced config` button. A new window appears. Just click on the `OK` button, do not modify any settings in this window. |
        | Add Security Exception | Click on the `Confirm Security Exception` button in the pop-up window. This confirms that your computer (127.0.0.1) can run the Bridge app. You might have to confirm a second security exception later on, once you send your first email. |

        </center>


    ??? tip "Show me the 3-minute summary video for Thunderbird (paid accounts only)"

        <center>

        <iframe width="720" height="405" src="https://invidious.fdn.fr/embed/NUpByACqo7g" frameborder="0" allowfullscreen></iframe>

        *Courtesy of Protonmail. Instructions should similarly apply to macOS or Linux.*

        </center>


<br>


<center> <img src="../../assets/img/separator_tutanota.svg" alt="Is Tutanota safe" width="150px"></img> </center>

## Tutanota review

[Tutanota](https://tutanota.com/) is a freemium hosted secure email service, registered in Germany. Everything is end-to-end encrypted. Tutanota uses its own encryption standard, and does not support PGP. While Tutanota's apps are [open source](https://github.com/tutao/tutanota/), the server-side is not.


At the time of writing, the basic account offered 1 GB storage for free. For approximately 1 to 6 EUR/month, you get access to more users and storage, as well as a plethora of features: custom domains, unlimited search, multiple calendars, inbox rules, whitelabel, calendar sharing, etc. Email imports and anonymous payment are currently not supported.


### Tutanota clients

Besides [webmail](https://mail.tutanota.com/?r=/login/) access on the Tutanota login page, Tutanota offers mobile apps for Android and iOS. For desktop environments, Tutanota developed its own dedicated client. More detailed instructions below.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

         Simply download the Tutanota app from [Google's Play Store](https://play.google.com/store/apps/details?id=de.tutao.tutanota&hl=en&gl=US) or [Aurora Store](https://auroraoss.com/). Tutanota is also available on [F-Droid](https://f-droid.org/en/packages/de.tutao.tutanota/). Alternatively, visit Tutanota's [download page](https://tutanota.com/#download) or [Github repository](https://github.com/tutao/tutanota/releases/) to download and install the `.apk` file. The app [contains 0 trackers and requires 9 permissions](https://reports.exodus-privacy.eu.org/en/reports/de.tutao.tutanota/latest/). By comparison: for Gmail it's 1 tracker and 55 permissions; for Outlook it's 13 trackers and 49 permissions; and for Hotmail it's 4 trackers and 31 permissions.

=== "iOS"

    ??? tip "Show me the step-by-step guide for iOS"

         Simply download the Tutanota app from the [App Store](https://apps.apple.com/us/app/tutanota/id922429609).


=== "Windows"

    ??? tip "Show me the step-by-step guide for Windows"

        Simply [download the installer](https://mail.tutanota.com/desktop/tutanota-desktop-win.exe), then click on the `Run` button and follow the installation wizard.


=== "macOS"

    ??? tip "Show me the step-by-step guide for macOS"

        Simply [download the installer](https://mail.tutanota.com/desktop/tutanota-desktop-mac.dmg), which should open by itself and mount a new volume containing the Tutanota application. If not, open the downloaded Tutanota `.dmg` file and drag the appearing Tutanota icon on top of the Application folder. For easy access, open the Applications folder and drag the Tutanota icon to your dock.


=== "Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Linux (Ubuntu)"

        Simply [download the installer](https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage), which should be called something like `tutanota-desktop-linux.AppImage`. Let's assume it was downloaded to the folder `/home/gofoss/Downloads`. Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Applications` button on the top left and search for `Terminal`. Then run the following commands (don't forget to adjust the filename and download folder path accordingly):

        ```bash
        cd /home/gofoss/Downloads
        chmod +x tutanota-desktop-linux.AppImage
        ```

    ??? tip "Show me how to pin Tutanota to the Ubuntu dock"

        It's not straight forward, but Tutanota's launcher can be added to Ubuntu's application menu and pinned to the dock. Open the terminal with the `CTRL + ALT + T` shortcut, or click on the `Applications` button on the top left and search for `Terminal`. Run the following command:

        ```bash
        sudo gedit /usr/share/applications/tutanota.desktop
        ```

        Paste the following content into the newly created file. Make sure to point the `Exec` path towards the folder containing the downloaded AppImage:

        ```
        #!/usr/bin/env xdg-open
        [Desktop Entry]
        Version=1.0
        Type=Application
        Terminal=false
        Exec=/home/gofoss/Downloads/tutanota-desktop-linux.AppImage
        Name=Tutanota
        ```

        Make the file executable:

        ```bash
        sudo chmod +x /usr/share/applications/tutanota.desktop
        ```

        Log off and back into your Ubuntu session. You should now be able to launch Tutanota from the application menu, and pin it to the dock.


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Email encryption tools" width="150px"></img> </center>

## Other providers

=== "Disroot"

    | Info | Description |
    | :-----: | ----- |
    |Website |[disroot. org](https://disroot.org/en/services/email) |
    |Pricing | Basic account is free (1 GB storage); extra storage for 0.15 EUR per GB per month. |
    |Features |Platform providing online services based on principles of freedom, privacy, federation and decentralization. Located in the Netherlands. Accepts bitcoin and faircoin. Full disk encryption & email encryption. Mobile app. |
    |Anti-features| Can potentially decrypt user data, as emails are reportedly stored in plain text. |


=== "Mailbox"

    | Info | Description |
    | :-----: | ----- |
    |Website |[mailbox. org](https://mailbox.org/en/) |
    |Pricing | 1 EUR/month, 2 GB storage. |
    |Features |German open source email provider, with servers located in Berlin. Offers security features such as encryption at rest, PGP, DANE, SPF and DKIM, as well as two-factor authentication, full text search, calendars, address books and task lists, CalDAV and CardDAV synchronisation. |
    |Anti-features| No mobile client, need for third party clients. |


=== "Posteo"

    | Info | Description |
    | :-----: | ----- |
    |Website |[posteo.de](https://posteo.de/en/) |
    |Pricing | 1 EUR/month, 2 GB storage. |
    |Features |German open source email provider, self-financed, encryption at rest, [two-factor authentication](https://gofoss.net/passwords/), calendars and address books, CalDAV and CardDAV synchronisation. |
    |Anti-features| No spam folder, no trial or free version. |


=== "Kolab Now"

    | Info | Description |
    | :-----: | ----- |
    |Website |[kolabnow.com](https://kolabnow.com/) |
    |Pricing | 5 USD/month, 2 GB storage. |
    |Features |Swiss open source email provider, text search and tagging, filters, address books, calendars, CalDAV and CardDAV synchronisation. |
    |Anti-features| No built-in end-to-end encryption, not encryption at rest. |


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="pgp vs gpg" width="150px"></img> </center>

## Transitioning towards encrypted emails

=== "Forward emails"

    The transition to a new email account can take some time, similar to [changing messanging apps](https://gofoss.net/encrypted-messages/). You'll probably want to keep your old accounts alive for a while to make sure you don't miss out on anything. Just forward any incoming message to the new account. For more instructions on how to forward emails refer to the documentation pages of [Gmail](https://support.google.com/mail/answer/10957?hl=en/), [Outlook](https://support.microsoft.com/en-us/office/create-reply-to-or-forward-email-messages-in-outlook-on-the-web-ecafbc06-e812-4b9e-a7af-5074a9c7abd0?redirectsourcepath=%252fen-us%252farticle%252fforward-email-from-office-365-to-another-email-account-1ed4ee1e-74f8-4f53-a174-86b748ff6a0e%252f), [iCloud](https://www.idownloadblog.com/2019/01/28/set-up-icloud-email-forwarding/), [Yahoo](https://www.wikihow.com/Forward-Yahoo-Mail) and so on.


=== "Update subscriptions"

    Use the transition period to scan your old email accounts for any active subscriptions and update your new personal email address!


=== "Inform contacts"

    Don't forget to communicate the new email address to your personal and professional contacts, bank, insurance, tax office, and so on. You might also want to set up an auto-reply message on your old account to keep folks informed about the change of address.


=== "Terminate old account"

    Over time, less and less emails will land in your old inbox. Eventually, it will become inactive. That's when you should consider terminating your old email account.


<br>

<center> <img src="../../assets/img/separator_protonmail.svg" alt="Warning unprotected private key file" width="150px"></img> </center>

## How to use PGP Encryption for Emails

Don't know anyone using Protonmail or Tutanota? Or simply don't like those service providers? Encrypt your emails the old-fashioned way, with [OpenPGP](https://www.openpgp.org/)! This encryption protocol is free, open source and [compatible with a large variety of clients](https://www.openpgp.org/software/). In the section below, we'll explain how to set up OpenPGP on your phone or computer, how to use a PGP key generator, how to backup your PGP keys, and how to encrypt and decode your emails.

=== "Android"

    ??? tip "Show me the step-by-step guide for Android"

        ### Install K-9 Mail & OpenKeychain

        <center>

        | Step | Description |
        | ----- | ----- |
        | K-9 Mail | K-9 Mail is one of several [Android email clients](https://www.openpgp.org/software/) which supports OpenPGP. Simply install it from the [Play Store](https://play.google.com/store/apps/details?id=com.fsck.k9) or [F-Droid](https://f-droid.org/packages/com.fsck.k9/). |
        | OpenKeychain | OpenKeychain is a free and open source app which integrates with K-9 Mail to provide end-to-end encryption capabilities. Simply install it from the [Play Store](https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain) or [F-Droid](https://f-droid.org/app/org.sufficientlysecure.keychain). |

        </center>


        ### Manage PGP keys with OpenKeychain

        What is a PGP key? To be able to send or read encrypted emails, you need a unique key pair for your email address:

        * **Public key**: people use your public key to encrypt emails they send to you. You can share your public key with anyone.
        * **Private key**: it's used to decode encrypted emails other people send to you. Keep your private key to yourself, never share it with anyone, and don't keep an unprotected private key file!


        <center>

        | Step | Description |
        | ----- | ----- |
        | Import existing PGP keys |• If there is already a key pair for your email address, don't generate a new one <br>• Launch OpenKeychain <br>• Tap on `Menu ‣ Manage my keys ‣ Import key from file` <br>• If required, enter the backup code and/or key password |
        | Generate new PGP keys |• If no key pair exists for your email address, create a new one <br>• Launch OpenKeychain <br>• Tap on `Menu ‣ Manage my keys ‣ Create my key` <br>• Associate a name & email address <br>• Tap on `Menu ‣ Change key configuration` <br>• Provide a [strong, unique password](https://gofoss.net/passwords/) <br>• Uncheck `Publish on keyservers` <br>• Tap on `Create key` |
        | Back up PGP key pair |• If you loose your keys, you loose access to all your emails <br>• If you just created a new key pair, make sure to store a backup <br>• Launch OpenKeychain <br>• Tap on your key <br>• Select `Menu ‣ Backup key` <br> • Provide the key password <br>• [Save the 45-character backup code](https://gofoss.net/passwords/), it's required to restore the keys! <br> • Also save the backup file to your phone's storage or better, [somewhere safe](https://gofoss.net/backups/) |
        | Share public keys |Before you can exchange encrypted emails with your contacts, you need to share your respective public keys with each other. Below some common methods to share public keys. <br><br> **Send your public key to your contacts**: <br>• Launch OpenKeychain <br>• Tap on your key <br>• Tap on the `Share` symbol & send your key <br>• Your contacts can import your key with their preferred app <br><br> **Upload your public key to a keyserver**: <br>• Launch OpenKeychain <br>• Tap on your key <br>• Tap on `Menu ‣ Advanced ‣ Share ‣ Publish on keyserver` <br>• Your contacts can now download the PGP public key from the PGP keyservers <br>• Optionally, add the download link & key fingerprint to your email signature <br><br> **Import your contact's public keys**: <br>• Ask your contacts to send you their public key by email, messenger, etc. <br>• Launch OpenKeychain <br>• Tap on `Menu ‣ Manage my keys ‣ Import key from file` <br><br> **Import your contact's public keys from a keyserver**: <br>• Launch OpenKeychain <br>• Tap on `+ ‣ Key search` <br>• Search for your contact's email address, name or fingerprint <br>• Tap on `Import` |

        </center>


        ### Encrypt emails with K-9 Mail

        <center>

        | Step | Description |
        | ----- | ----- |
        | Set up account & encryption |• Open the K-9 Mail app <br>• Add your account: provide your email address & password <br>• Configure IMAP/POP3/SMTP settings, if not detected automatically <br>• Select `Menu ‣ Settings ‣ Account ‣ End-to-end-encryption ‣ Enable OpenPGP support` <br>• Select `Menu ‣ Settings ‣ Account ‣ End-to-end-encryption ‣ Configure end-to-end key` <br>• Select your key |
        | Encrypt emails |• Open the K-9 Mail app <br>• From the Inbox view, tap on the `Pen` icon <br>• Compose your message & enter your contact's email address <br>• If you previously imported the public key(s) of your contact(s), a `Padlock` icon should show on the top of the composition screen <br>• When you tap on it, it should turn green, indicating that encryption is enabled <br>• Tap on `Send` <br><br>*Caution*: the email subject is transmitted unencrypted! |
        | Decode emails |• K-9 Mail/OpenKeychain automatically decodes messages which use your public key encryption <br>• This requires the password of your private key <br>• A `Padlock` symbol should show on the top of the decoded message |

        </center>


        ### Try it out!

        Edward is a program developed by the Free Software Foundation to test email encryption. Here is how it works:

        * First, you share your public key with Edward
        * Edward uses your public key to send you an encrypted email
        * Only you are able to decode this email, using your private key
        * Next, you retrieve Edward's public key to send an encrypted and signed email
        * Edward is the only one able to decode your message, using its private key
        * Edward will reply, confirming that your previous email was both encrypted and signed

        <center>

        | Step | Description |
        | ----- | ----- |
        | Send public key to Edward |• Launch OpenKeychain <br>• Tap on your key <br>• Tap on the `Share` symbol <br>• Select K-9 Mail & compose an email to `edward-en@fsf.org` <br>• Add a subject and a short message <br>• Tap on `Menu` & make sure encryption is `Disabled` <br>• Hit `Send` |
        | Decode Edward's message |• Open K-9 Mail & wait for Edward to reply <br>• Edward's email answer should be encrypted using your public key <br>• Enter your private key's password to decode the mail <br>• Make sure an orange `Padlock` symbol shows on the top of the message  |
        | Import Edward's public key |• Tap on the orange `Padlock` symbol <br>• Tap on `Search key` <br>• Tap on `Import` <br>•  The `Padlock` symbol should have turned green |
        | Send Edward encrypted & signed email |• Tap on `Reply` <br>• Compose a short response to `edward-en@fsf.org` <br>• Tap on `Menu` & make sure encryption is `Enabled` <br>• Hit `Send` |
        | Decode Edward's message |• Wait for Edward to reply <br>• Make sure the green `Padlock` symbol still shows <br>• Edward's message should confirm that it could decode your message and verify your signature |

        </center>


=== "Windows, macOS & Linux (Ubuntu)"

    ??? tip "Show me the step-by-step guide for Windows, macOS & Linux (Ubuntu)"

        ### Install Thunderbird 78 (or newer)

        <center>

        | OS | Description |
        | ----- | ----- |
        | Windows | Navigate to [Thunderbird's download page](https://www.thunderbird.net/en-US/) and click on the `Free Download` button. Once the installer is downloaded, click on the `Run` button and follow the installation wizard. |
        | macOS | Navigate to [Thunderbird's download page](https://www.thunderbird.net/en-US/) and click on the `Free Download` button. Once the installer is downloaded, it should open by itself and mount a new volume containing the Thunderbird application. If not, open the downloaded Thunderbird `.dmg` file and drag the appearing Thunderbird icon on top of the Application folder. For easy access, open the Applications folder and drag the Thunderbird icon to your dock. |
        | Linux (Ubuntu) | If you run a Linux distribution such as [Ubuntu](https://gofoss.net/ubuntu/), open the terminal with the shortcut `CTRL + ALT + T`, or click on the `Applications` button on the top left and search for `Terminal`. Run the following command to install Thunderbird: `sudo apt install thunderbird` |

        </center>


        ### Configure Thunderbird

        Launch Thunderbird, navigate to `Menu ‣ New ‣ Existing Mail Account` and follow the setup wizard:

        <center>

        | Setting | Description |
        | ------ | ------ |
        | Name | Enter the name you want others to see. |
        | Email address | Enter your email address. |
        | Password | Enter your email password. |
        | Remember password | Check the `Remember Password` box to avoid re-entering the password each time you fire up Thunderbird. |
        | Automatic vs. manual configuration | Once you've filled in your credentials, hit the `Continue` button. Thunderbird will try to automatically configure IMAP/POP3/SMTP settings. If that's unsuccessful, configure those settings manually (refer to your email provider). |

        </center>


        ### Manage PGP keys with Thunderbird

        Public key vs private key – to be able to send or read encrypted emails, you need a unique key pair for your email address:

        * **Public key**: people use your public key to encrypt emails they send to you. You can share your public key with anyone.
        * **Private key**: it's used to decode encrypted emails other people send to you. Keep your private key to yourself, never share it with anyone. It is required that your private key files are not accessible by others!

        <center>

        | Step | Description |
        | ----- | ----- |
        | Import existing PGP keys |**Import backup key**: <br>• If there is already a key pair for your email address, don't generate a new one <br>• Launch Thunderbird <br>• Go to `Menu ‣ Account Settings ‣ End-To-End Encryption ‣ Add Key` <br>• Select `Import an existing OpenPGP Key` & hit `Continue` <br>• Click on `Select File to Import` & navigate to the key file <br>• If required, enter the backup code and/or key password <br><br> **Import encrypted backup key**: <br>• Some backups are encrypted (e.g. OpenKeychain) <br>• They can't be directly imported into Thunderbird <br>• Open a terminal <br>• Decrypt PGP file:<br>`gpg --decrypt backup_YYYY-MM-DD.sec.pgp | gpg --import` <br>• If required, enter the backup code and/or key password <br>• Display the list of keys:<br>`gpg --list-keys` <br>• Note down the `UID` of the key to import <br>• Store the key in the right format (replace `UID` accordingly): <br>`gpg --export-secret-keys UID > decrypted_backup_key.asc` <br>• If required, enter the key password <br>• Launch Thunderbird <br>• Go to `Menu ‣ Account Settings ‣ End-To-End Encryption ‣ Add Key` <br>• Select `Import an existing OpenPGP Key` & hit `Continue` <br>• Click on `Select File to Import` & navigate to the `.asc` file <br>• If required, enter the password for opening PGP file |
        | Generate PGP key |• If no key pair exists for your email address, create a new one <br>• Launch Thunderbird <br>• Go to `Menu ‣ Account Settings ‣ End-To-End Encryption ‣ Add Key` <br>• Select `Create a new OpenPGP Key` & hit `Continue` <br>• Select the relevant email address <br>• Set expiration time between 1-3 years (can be extended at any time) <br>• Choose `Key type: RSA` & `Key size: 4096`  <br>• Click on `Generate key ‣ Confirm` |
        | Back up PGP key pair |• If you loose your keys, you loose access to all your emails <br>• If you just created a new key pair, make sure to store a backup <br><br>**Backup the private key:** <br>• Launch Thunderbird <br>• Go to `Menu ‣ Tools ‣ OpenPGP Key Manager` <br>• Click on the relevant key <br>• Select `File ‣ Backup Secret Key(s) To File` <br>• Provide a [strong, unique backup code](https://gofoss.net/passwords/) <br>• Keep the backup code somewhere safe, it's required to restore the private key! <br>• Save the `.asc` backup file of your private key to your computer's storage or better, [somewhere safe](https://gofoss.net/backups/) <br><br>**GPG export public key:** <br>• Launch Thunderbird <br>• Go to `Menu ‣ Tools ‣ OpenPGP Key Manager` <br>• Right-click on the relevant key <br>• Select `Export Key(s) To File` <br>• Save the `.asc` backup file of your public key to your computer's storage or better, [somewhere safe](https://gofoss.net/backups/)|
        | Share public keys |Before you can exchange encrypted emails with your contacts, you need to share your respective public keys with each other. Below some common methods to share public keys. <br><br> **Send your public key to your contacts**: <br>• Launch Thunderbird <br>• Go to `Menu ‣ Tools ‣ OpenPGP Key Manager` <br>• Right-click on the relevant key <br>• Select `Send Public Key(s) By Email` <br>• Your contacts can import your key with their preferred app <br><br> **Upload your public key to a keyserver**: <br>• Launch Thunderbird <br>• Go to `Menu ‣ Tools ‣ OpenPGP Key Manager` <br>• Right-click on the relevant key <br>• Select `Export Key(s) To File` <br>• Browse to the [OpenPGP Key Repository](https://keys.openpgp.org/upload) <br>• Select the exported public key file & click on `Upload` <br>• Your contacts can now download the public key from the keyserver <br>• Optionally, add the download link & key fingerprint to your email signature <br><br> **Import your contact's public keys**: <br>• Ask your contacts to send you their public key by email, messenger, etc. <br>• Launch Thunderbird <br>• If you received a public key in an email, click on the `OpenPGP` button to import it <br>• If you downloaded a public key file to your computer, go to `Menu ‣ Tools ‣ OpenPGP Key Manager` and click on `File ‣ Import Public Key(s) From File` <br><br> **Import your contact's public keys from a keyserver**: <br>• Launch Thunderbird <br>• Go to `Menu ‣ Tools ‣ OpenPGP Key Manager` <br>• Click on `Keyserver` <br>• Search for your contact's email address, name or fingerprint <br>• Click on `OK` |

        </center>


        ### Encrypt emails with Thunderbird

        <center>

        | Step | Description |
        | ----- | ----- |
        | Set up encryption |• Launch Thunderbird <br>• Go to `Menu ‣ Account Settings ‣ End-To-End Encryption` <br>• Make sure the right key is associated with your email address |
        | Encrypt emails |• Launch Thunderbird <br>• From the Inbox view, click on the `Write` button <br>• Compose your message & enter your contact's email address <br>• Click on the drop-down icon next to the `Security` button <br>• Select `Require Encryption` <br>• An `OpenPGP` icon should be displayed in the window footer <br>• Click on the `Security` button <br>• If you previously imported the public key(s) of your contact(s), it should show `OK` next to your contact's email address <br>• Click on `Send` when ready |
        | Decode emails |• Thunderbird automatically decodes messages which have been encrypted using your public key <br>• This requires the password of your private key <br>• An `OpenPGP Padlock` symbol with a green check mark should show on the top of the decoded message |

        </center>


        ### Try it out!

        Edward is a program developed by the Free Software Foundation to test email encryption. Here is how it works:

        * First, you share your public key with Edward
        * Edward uses your public key to send you an encrypted email
        * Only you are able to decode this email, using your private key encryption
        * Next, you ask Edward for its public key
        * Use Edward's public key to send an encrypted and signed email
        * Edward is the only one able to decode your message, using its private key
        * Edward will reply, confirming that your previous email was both encrypted and signed

        <center>

        | Step | Description |
        | ----- | ----- |
        | Send public key to Edward |• Launch Thunderbird <br>• Go to `Menu ‣ Tools ‣ OpenPGP Key Manager` <br>• Right-click on the relevant key <br>• Select `Send Public Key(s) By Email` <br>• Address the email to `edward-en@fsf.org` <br>• Add a subject and a short message <br>• Click on the drop-down icon next to the `Security` button <br>• Make sure `Do Not Encrypt` is selected <br>• Hit `Send` |
        | Decode Edward's message |• Wait for Edward to reply <br>• Edward's email answer should be encrypted using your public key <br>• Make sure an `OpenPGP Padlock` symbol with a green check mark shows on the top of the message |
        | Import Edward's public key |• In Edward's reply, click on the email address `edward-en@fsf.org` <br>• Select `Discover OpenPGP Key` <br>• Select `Accepted (unverified)` <br>•  Click `OK` |
        | Send Edward encrypted & signed email |• Click on `Reply` <br>• Compose a short response to `edward-en@fsf.org` <br>• Click on the drop-down icon next to the PGP `Security` button <br>• Make sure `Require Encryption` is selected <br>• Click on the `Security` button <br>• It should show `OK` next to Edward's email address <br>• Hit `Send` |
        | Decode Edward's message |• Wait for Edward to reply <br>• Make sure the `OpenPGP Padlock` symbol with the green check mark still shows <br>• Edward's message should confirm that it could decode your message and verify pgp signature |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="gpg vs pgp" width="150px"></img> </center>


## Support

For further details or questions, refer to:

* [Protonmail's support](https://protonmail.com/support/) or [Thunderbird's documentation](https://support.mozilla.org/en-US/products/thunderbird). Also feel free to ask the [Protonmail](https://teddit.net/r/ProtonMail/) or [Thunderbird](https://support.mozilla.org/en-US/questions/new/thunderbird) communities for help.

* [Tutanota's documentation](https://tutanota.com/howto/#install-desktop/) or [ask the Tutanota community](https://teddit.net/r/tutanota/).

* [K9 Mail's documentation](https://docs.k9mail.app).


<br>

<div align="center">
<img align="center" src="https://imgs.xkcd.com/comics/public_key.png" width="600px" alt="Open pgp file">
</div>

<br>
---
template: main.html
title: Voici pourquoi vous devriez sauvegarder vos fichiers
description: Comment créer des sauvegardes ? Méthode de sauvegarde 3-2-1 ? Comment installer FreeFileSync? Qu'est-ce que la synchronisation miroir? La synchronisation bidirectionnelle?
---

# Stratégie de sauvegarde 3-2-1

!!! level "Destiné aux débutants. Aucune compétence technique particulière n'est requise."

<div align="center">
<img src="../../assets/img/cloud_storage_2.png" alt="Stratégie de sauvegarde 3-2-1" width="550px"></img>
</div>

Vous pensez que créer des sauvegardes est ennuyeux? Rassurez-vous, la perte de vos données ou [mots de passe](https://gofoss.net/fr/passwords/) est pire. Conserver des copies vous rendra résilient à la perte, au vol, à la corruption ou à la suppression accidentelle de vos données. Crééz une stratégie de sauvegarde 3-2-1 régulière pour conserver au moins trois copies de vos données :

* *Fichier original*: stocké sur votre appareil primaire, tel que votre ordinateur, ordinateur portable, téléphone, appareil photo, etc...
* *Copie locale*: sauvegardée à intervalles réguliers. Conservez cette sauvegarde sur place, par exemple chez vous ou sur votre lieu de travail. Si un incident affecte votre appareil principal, les données pourront être rapidement restaurées. Pour plus de sécurité, pensez également à chiffrer cette sauvegarde, par exemple avec [VeraCrypt](https://gofoss.net/fr/encrypted-files/).
* *Copie distante*: sauvegardée à intervalles réguliers. Conservez cette sauvegarde hors de chez vous, par exemple [dans le nuage](https://gofoss.net/fr/cloud-storage/) ou sur un autre site. En cas d'incident qui affecterait à la fois votre appareil principal et votre sauvegarde locale, les données pourront encore être récupérées. Pensez aux risques de vol, d'incendie, d'inondation, etc. Pour plus de sécurité, pensez également à chiffrer cette sauvegarde, par exemple avec [Cryptomator](https://gofoss.net/fr/encrypted-files/).

Si vous [hébergez votre propre serveur](https://gofoss.net/fr/ubuntu-server/), nous expliquerons dans un chapitre dédié [comment planifier des sauvegardes de votre serveur](https://gofoss.net/fr/server-backups/).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Backup" width="150px"></img> </center>

## FreeFileSync

[FreeFileSync](https://freefilesync.org/) est un logiciel libre et [open-source](https://github.com/hkneptune/FreeFileSync/) de synchronisation de dossiers. Il crée et gère des sauvegardes de vos fichiers en détectant les différences entre les dossiers source et cible. Des instructions plus détaillées sont disponibles ci-dessous.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installez FreeFileSync | Téléchargez le dernier [installateur de FreeFileSync pour Windows](https://freefilesync.org/download.php), double-cliquez sur le fichier `.exe` et suivez l'assistant d'installation. |
        | Sélectionnez les dossiers | Démarrez FreeFileSync et sélectionnez les deux dossiers à comparer: <br>• `dossier source` : dans le panneau de gauche, sélectionnez le dossier contenant les données que vous voulez sauvegarder <br>• `dossier cible` : dans le panneau de droite, sélectionnez le dossier dans lequel les données doivent être sauvegardées |
        | Comparez les dossiers | Cliquez sur le bouton `Comparer` pour examiner les différences entre les deux dossiers, en fonction de l'heure de modification des fichiers et de leur taille. Vous pouvez également configurer une comparaison plus lente, mais plus détaillée, basée sur le contenu des fichiers. Une fois la comparaison terminée, vous pouvez visualiser dans la colonne centrale quels fichiers sont identiques, quels fichiers sont différents, et quels fichiers ne sont disponibles que dans un seul des deux dossiers. |
        | Synchronisez les dossiers | Il y a principalement deux façons pour synchroniser les dossiers source et cible. Choisissez soigneusement celle qui correspond à vos besoins: <br><br>*Synchronisation miroir* : <br><br>• procédez comme décrit ci-dessus pour comparer les deux dossiers <br>• sélectionnez l'option de synchronisation `Miroir -->` <br>• la colonne centrale devrait indiquer une seule direction de synchronisation, du dossier source (gauche) vers le dossier cible (droite) <br>• la colonne centrale indique quels fichiers seront mis à jour dans le dossier cible, quels fichiers seront ajoutés au dossier cible, et quels fichiers seront effacés du dossier cible <br>• vous pouvez exclure des fichiers ou des sous-dossiers entiers du processus de synchronisation en utilisant le menu de clic droit <br>• une fois que vous avez sélectionné tous les fichiers qui doivent être synchronisés, cliquez sur le bouton `Synchronisation`. FreeFileSync va copier les fichiers du dossier source vers le dossier cible, et effacer les fichiers du dossier cible, jusqu'à ce que les deux dossiers soient des copies exactes <br><br>*Synchronisation bidirectionnelle* : <br><br>• procédez comme décrit ci-dessus pour comparer les deux dossiers <br>• sélectionnez l'option de synchronisation `<- Deux sens ->` <br>• Contrairement à l'option "miroir", la synchronisation bidirectionnelle détecte les différences entre les deux dossiers dans les deux directions. La colonne centrale devrait donc indiquer deux directions de synchronisation, à la fois du dossier source (gauche) vers le dossier cible (droite), et vice versa <br>• la colonne centrale indique quels fichiers seront mis à jour dans les dossiers source et/ou cible, quels fichiers seront ajoutés aux dossiers source et/ou cible, et quels fichiers seront supprimés des dossiers source et/ou cible. Des conflits de versions entre les fichiers seront également affichées <br>• vous pouvez changer la direction de synchronisation ou exclure des fichiers ou des sous-dossiers entiers du processus de synchronisation en utilisant le menu de clic droit <br>• Une fois que vous avez défini comment tous les fichiers doivent être synchronisés, cliquez sur le bouton `Synchronisation`. FreeFileSync va copier ou effacer les fichiers du dossier source et du dossier cible jusqu'à ce qu'ils soient des copies exactes. |

        </center>


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installez FreeFileSync | Téléchargez le dernier [installateur de FreeFileSync pour macOS](https://freefilesync.org/download.php), qui devrait se lancer tout seul et monter un nouveau volume contenant l'application FreeFileSync. Si ce n'est pas le cas, ouvrez le fichier `.dmg` téléchargé et faites glisser l'icône FreeFileSync qui apparaît dans le dossier `Application`. Pour un accès facilité, ouvrez le dossier `Application` et faites glisser l'icône FreeFileSync sur le dock. |
        | Sélectionnez les dossiers | Démarrez FreeFileSync et sélectionnez les deux dossiers à comparer: <br>• `dossier source` : dans le panneau de gauche, sélectionnez le dossier contenant les données que vous voulez sauvegarder <br>• `dossier cible` : dans le panneau de droite, sélectionnez le dossier dans lequel les données doivent être sauvegardées |
        | Comparez les dossiers | Cliquez sur le bouton `Comparer` pour examiner les différences entre les deux dossiers, en fonction de l'heure de modification des fichiers et de leur taille. Vous pouvez également configurer une comparaison plus lente, mais plus détaillée, basée sur le contenu des fichiers. Une fois la comparaison terminée, vous pouvez visualiser dans la colonne centrale quels fichiers sont identiques, quels fichiers sont différents, et quels fichiers ne sont disponibles que dans un seul des deux dossiers. |
        | Synchronisez les dossiers | Il y a principalement deux façons pour synchroniser les dossiers source et cible. Choisissez soigneusement celle qui correspond à vos besoins: <br><br>*Synchronisation miroir* : <br><br>• procédez comme décrit ci-dessus pour comparer les deux dossiers <br>• sélectionnez l'option de synchronisation `Miroir -->` <br>• la colonne centrale devrait indiquer une seule direction de synchronisation, du dossier source (gauche) vers le dossier cible (droite) <br>• la colonne centrale indique quels fichiers seront mis à jour dans le dossier cible, quels fichiers seront ajoutés au dossier cible, et quels fichiers seront effacés du dossier cible <br>• vous pouvez exclure des fichiers ou des sous-dossiers entiers du processus de synchronisation en utilisant le menu de clic droit <br>• une fois que vous avez sélectionné tous les fichiers qui doivent être synchronisés, cliquez sur le bouton `Synchronisation`. FreeFileSync va copier les fichiers du dossier source vers le dossier cible, et effacer les fichiers du dossier cible, jusqu'à ce que les deux dossiers soient des copies exactes <br><br>*Synchronisation bidirectionnelle* : <br><br>• procédez comme décrit ci-dessus pour comparer les deux dossiers <br>• sélectionnez l'option de synchronisation `<- Deux sens ->` <br>• Contrairement à l'option "miroir", la synchronisation bidirectionnelle détecte les différences entre les deux dossiers dans les deux directions. La colonne centrale devrait donc indiquer deux directions de synchronisation, à la fois du dossier source (gauche) vers le dossier cible (droite), et vice versa <br>• a colonne centrale indique quels fichiers seront mis à jour dans les dossiers source et/ou cible, quels fichiers seront ajoutés aux dossiers source et/ou cible, et quels fichiers seront supprimés des dossiers source et/ou cible. Des conflits de versions entre les fichiers seront également affichées <br>• vous pouvez changer la direction de synchronisation ou exclude des fichiers ou des sous-dossiers entiers du processus de synchronisation en utilisant le menu de clic droit <br>• Une fois que vous avez défini comment tous les fichiers doivent être synchronisés, cliquez sur le bouton `Synchronisation`. FreeFileSync va copier ou effacer les fichiers du dossier source et du dossier cible jusqu'à ce qu'ils soient des copies exactes. |

        </center>


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        <center>

        | Instructions | Description |
        | ------ | ------ |
        | Installez FreeFileSync | Téléchargez le dernier [installateur de FreeFileSync pour Linux](https://freefilesync.org/download.php), qui devrait s'appeler `FreeFileSync_XX.YY_Linux.tar.gz`. Supposons par exemple que le fichier a été téléchargé dans le dossier `/home/gofoss/Downloads`. Décompressez et installez les fichiers avec les commandes suivantes (ajustez en conséquence):  <br><br>`cd /home/gofoss/Downloads` <br>`tar xfv FreeFileSync_XX.YY_Linux.tar.gz` <br>`./FreeFileSync_XX.YY_Install.run` <br><br> Suivez les instructions à l'écran: acceptez les termes de la licence, sélectionnez le dossier d'installation et installez le logiciel |
        | Sélectionnez les dossiers | Démarrez FreeFileSync et sélectionnez les deux dossiers à comparer: <br>• `dossier source` : dans le panneau de gauche, sélectionnez le dossier contenant les données que vous voulez sauvegarder <br>• `dossier cible` : dans le panneau de droite, sélectionnez le dossier dans lequel les données doivent être sauvegardées |
        | Comparez les dossiers | Cliquez sur le bouton `Comparer` pour examiner les différences entre les deux dossiers, en fonction de l'heure de modification des fichiers et de leur taille. Vous pouvez également configurer une comparaison plus lente, mais plus détaillée, basée sur le contenu des fichiers. Une fois la comparaison terminée, vous pouvez visualiser dans la colonne centrale quels fichiers sont identiques, quels fichiers sont différents, et quels fichiers ne sont disponibles que dans un seul des deux dossiers. |
        | Synchronisez les dossiers | Il y a principalement deux façons pour synchroniser les dossiers source et cible. Choisissez soigneusement celle qui correspond à vos besoins: <br><br>*Synchronisation miroir* : <br><br>• procédez comme décrit ci-dessus pour comparer les deux dossiers <br>• sélectionnez l'option de synchronisation `Miroir -->` <br>• la colonne centrale devrait indiquer une seule direction de synchronisation, du dossier source (gauche) vers le dossier cible (droite) <br>• la colonne centrale indique quels fichiers seront mis à jour dans le dossier cible, quels fichiers seront ajoutés au dossier cible, et quels fichiers seront effacés du dossier cible <br>• vous pouvez exclure des fichiers ou des sous-dossiers entiers du processus de synchronisation en utilisant le menu de clic droit <br>• une fois que vous avez sélectionné tous les fichiers qui doivent être synchronisés, cliquez sur le bouton `Synchronisation`. FreeFileSync va copier les fichiers du dossier source vers le dossier cible, et effacer les fichiers du dossier cible, jusqu'à ce que les deux dossiers soient des copies exactes <br><br>*Synchronisation bidirectionnelle* : <br><br>• procédez comme décrit ci-dessus pour comparer les deux dossiers <br>• sélectionnez l'option de synchronisation `<- Deux sens ->` <br>• Contrairement à l'option "miroir", la synchronisation bidirectionnelle détecte les différences entre les deux dossiers dans les deux directions. La colonne centrale devrait donc indiquer deux directions de synchronisation, à la fois du dossier source (gauche) vers le dossier cible (droite), et vice versa <br>• a colonne centrale indique quels fichiers seront mis à jour dans les dossiers source et/ou cible, quels fichiers seront ajoutés aux dossiers source et/ou cible, et quels fichiers seront supprimés des dossiers source et/ou cible. Des conflits de versions entre les fichiers seront également affichées <br>• vous pouvez changer la direction de synchronisation ou exclude des fichiers ou des sous-dossiers entiers du processus de synchronisation en utilisant le menu de clic droit <br>• Une fois que vous avez défini comment tous les fichiers doivent être synchronisés, cliquez sur le bouton `Synchronisation`. FreeFileSync va copier ou effacer les fichiers du dossier source et du dossier cible jusqu'à ce qu'ils soient des copies exactes. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance FreeFileSync" width="150px"></img> </center>

## Assistance

Pour plus de détails ou en cas de questions, référez-vous à la [documentation de FreeFileSync](https://freefilesync.org/manual.php), aux [tutoriels de FreeFileSync](https://freefilesync.org/tutorials.php) ou demandez de l'aide à la [communauté de FreeFileSync](https://freefilesync.org/forum/).


<br>

---
template: main.html
title: So verschlüsset Ihr Eure Nachrichten
description: Schützt Eure Nachrichten mit durchgängiger Verschlüsselung. Wählt datenschutzfreundliche Nachrichten-Apps wie Signal, Element, Jami, oder Briar.
---

# Verschlüsselt Eure Nachrichten (Perfect Forward Secrecy, Zero Knowledge Encryption & mehr)

!!! level "Letzte Aktualisierung: März 2022. Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/email_2.png" alt="Verschlüsselte Nachrichten-Apps" width="400px"></img>
</div>

Wählt vertrauenswürdige Nachrichten-Apps. Prüft sorgfältig welche Optionen verfügbar sind, welche Verschlüsselungstechnologien genutzt werden oder wo die Server stehen — Datenschutzgesetze sind von Land zu Land unterschiedlich. Dieses Kapitel bietet einen (kurzen) Überblick über beliebte und datenschutzfreundliche Nachrichten-Apps.

??? tip "Hier ein Vergleich mehrerer datenschutzfreundlicher Nachrichten-Apps"

    <center>

    | | Signal | Element | Jami | Briar |
    | ------ | :------: | :------: | :------: | :------: |
    | Gründungsjahr | 2014 | 2016 | 2005/2017^1^ | 2018 |
    | Gerichtsbarkeit | US | UK | Kanada |N/A^2^ |
    | Finanzierung | Verschiedene Stiftungen^3^ | Matrix Stiftung, New Vector Limited | Free Software Foundation^4^ |Spenden |
    | Architektur | Zentralisiert | Dezentralisiert | Peer-to-Peer |Peer-to-Peer |
    | Offener Quellcode | [Ja](https://github.com/signalapp/)^17^ | [Ja](https://github.com/vector-im/) | [Ja](https://git.jami.net/savoirfairelinux) |[Ja](https://code.briarproject.org/briar/briar/) |
    | Protokoll | Signal | Matrix | SIP/OpenDHT |Bramble |
    | Durchgängige Verschlüsselung | <span style="color:green">✔</span> (standardmäßig)^5^ | <span style="color:green">✔</span> (nicht standardmäßig)^6^ | <span style="color:green">✔</span> (standardmäßig)^7^ |<span style="color:green">✔</span> (standardmäßig)^8^ |
    | Perfect Forward Secrecy | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:orange">?</span>^10^ |
    | Sicherheitsaudit | [2016](https://eprint.iacr.org/2016/1013.pdf), [2017](https://hal.inria.fr/hal-01575923/file/KobeissiBhargavanBlanchetEuroSP17.pdf) | <span style="color:red">✗</span>^11^ | <span style="color:red">✗</span> |[2017](https://briarproject.org/raw/BRP-01-report.pdf) |
    | Keine Aufzeichnung | <span style="color:green">✔</span> | <span style="color:orange">?</span>^12^ | <span style="color:red">✗</span>^13^ |<span style="color:green">✔</span> |
    | DSGVO-Konformität | <span style="color:red">✗</span>^14^ | <span style="color:orange">?</span>^15^ | <span style="color:orange">?</span> | <span style="color:orange">?</span> |
    | Anonyme Anmeldung | <span style="color:red">✗</span>^16^ | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:green">✔</span> |
    | Sofortnachrichten | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:green">✔</span> |
    | Video-/Sprachanrufe | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |<span style="color:red">✗</span> |
    | Windows Client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | macOS Client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Linux Client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |
    | Android Client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> |
    | iOS Client | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:green">✔</span> | <span style="color:red">✗</span> |

    </center>

    1. *Jami baut auf GNU/Ring und SFLphone auf. Die beiden Technologien gibt es seit 2015 bzw. 2005.*
    2. *Briar basiert auf einer Peer-to-Peer-Architektur, bei der die NutzerInnen direkt eine Verbindung herstellen, ohne auf Server angewiesen zu sein.*
    3. *Signal Stiftung, Freedom of the Press Stiftung, Shuttleworth Stiftung, Knight Stiftung, Open Technology Fund.*
    4. *Im November 2016 wurde Jami Teil des GNU-Projekts, das von der Free Software Foundation finanziert wird.*
    5. *Signal, AES-256, Curve25519, HMAC-SHA256.*
    6. *Olm/Megaolm, AES-256, Curve25519, HMAC-SHA256.*
    7. *TLS 1.3 mit RSA Schlüsseln & X.509 Zertifikaten.*
    8. *Bramble, AES-256.*
    9. *Signal/Axolotl, AES-256.*
    10. *Briar bietet zwar Perfect Forward Secrecy. Es ist aber zu beachten, dass die Abstreitbarkeit bei der Kommunikation von einer Person zur anderen, z. B. bei Gruppenchats, nicht gegeben ist.*
    11. *Zum Zeitpunkt der Abfassung dieses Textes gab es noch keine vollständige Sicherheitsüberprüfung von Element. Im Jahr 2016 hat NCC Group Security Services [den Verschlüsselungsalgorithmus von Matrix überprüft](https://www.nccgroup.com/globalassets/our-research/us/public-reports/2016/november/ncc_group_olm_cryptogrpahic_review_2016_11_01.pdf).*
    12. *Das hängt vom Matrix-Server ab. BenutzerInnen können zwischen mehreren unabhängigen Servern wählen, von denen einige IP-Adressen protokollieren, andere wiederum nicht. Denkt daran, dass der Administrator des Matrix-Servers auf fast alle Daten der BenutzerInnen zugreifen kann (z. B. ID, Benutzername, Passwörter, E-Mail, Telefonnummer, Geräteinformationen, Nutzungsmuster, IP-Adresse usw.), auch wenn die Nachrichteninhalte verschlüsselt bleiben.*
    13. *IP-Adressen können eingesehen werden, wenn sie nicht sorgfältig über [Tor](https://gofoss.net/de/tor/) oder [VPN](https://gofoss.net/de/vpn/) abgesichert werden.*
    14. *Laut Signal ist dies ein ["voranschreitender Prozess"](https://support.signal.org/hc/en-us/articles/360007059412-Signal-and-the-General-Data-Protection-Regulation-GDPR-).*
    15. *Das hängt vom Matrix-Server ab, daher solltet Ihr die Datenschutzbestimmungen genau prüfen. Denkt daran, dass der Administrator des Matrix-Servers auf fast alle Daten der BenutzerInnen zugreifen kann (z. B. ID, Benutzername, Passwörter, E-Mail, Telefonnummer, Geräteinformationen, Nutzungsmuster, IP-Adresse usw.), auch wenn die Nachrichteninhalte verschlüsselt bleiben.*
    16. *Eine Telefonnummer ist zur Anmeldung erforderlich.*
    17. *Enthält proprietäre Bibliotheken. Serverseitiger Quellcode wurde in der Vergangenheit [unregelmäßig veröffentlicht](https://github.com/signalapp/Signal-Android/issues/11101).*


??? question "Und was ist mit XMPP?"

    [XMPP](https://xmpp.org/) ist großartig! Wir haben uns jedoch aus mehreren Gründen entschieden, XMPP-Clients in diesem Leitfaden nicht zu behandeln:

    * *Durchgängige Verschlüsselung ist standardmäßig nicht aktiviert*: XMPP kann zwar theoretisch Eure Kommunikation verschlüsseln, standardmäßig ist dies allerdings nicht der Fall. Aufgrund der Fragmentierung der verwendeten Server und Clients kann es durchaus vorkommen, dass Eure Kommunikation manchmal verschlüsselt ist, und manchmal nicht. Darüber hinaus unterstützen Gruppenchats oft keine Verschlüsselung. Standardmäßig sollten XMPP-BenutzerInnen also sicherheitshalber davon ausgehen, dass ihre Chats unverschlüsselt bleiben.

    * *XMPP erhebt Metadaten*: XMPP verfolgt das "Null-Wissen"-Prinzip genauso wenig wie Matrix. Es fallen daher Metadaten an. Ihr vertraut somit einen Teil Eurer Daten den XMPP-Serveradministratoren an. Selbst wenn die Kommunikation verschlüsselt wird, sind diese Administratoren theoretisch in der Lage, eine Kopie der gesamten (verschlüsselten) Kommunikation zu speichern, IP-Adressen zu protokollieren oder auf unverschlüsselte Daten wie Benutzernamen, Passwörter, E-Mail-Adressen, Telefonnummern, Mediendateien, Geräteinformationen, Kontaktlisten, Nutzungsprofile, Gruppenmitgliedschaften usw. zuzugreifen. Selbst wenn Ihr Euren eigenen XMPP-Server hostet, können Administratoren von anderen XMPP-Servern, die an einer Kommunikation teilnehmen, auf diese Metadaten zugreifen.

    Nach dieser Feststellung sei jedoch gesagt, dass es eine sehr aktive XMPP-Gemeinschaft sowie mehrere Open-Source-Clients gibt. Diese basieren auf der dezentralen XMPP-Architektur, bei der BenutzerInnen zwischen mehreren unabhängigen Servern wählen oder sogar eigene Instanzen hosten können:

    * [Gajim](https://gajim.org/de/), eine App für Sofortnachrichten und Videogespräche, die auf allen Desktop-Umgebungen verfügbar ist
    * [Conversations](https://conversations.im/), eine Android-App für Sofortnachrichten und Videogespräche



<br>

<center> <img src="../../assets/img/separator_signal.svg" alt="Signal" width="150px"></img> </center>

## Signal

[Signal](https://signal.org/de/) ist eine schnelle und einfach zu handhabende Nachrichten-, Sprach- und Videoanruf-App, die auf allen Desktop- sowie Mobilplattformen verfügbar ist. Sie ist frei, [quelloffen](https://github.com/signalapp/) und basiert auf einer zentralisierten Architektur, die von der Signal Foundation betrieben wird. Die App bietet durchgängige Verschlüsselung für Text, Sprache, Videos, Bilder und Dateien. Signal kann also nicht auf den Inhalt von Nachrichten zugreifen. Zudem verfolgt Signal das "Null-Wissen"-Prinzip: es erlangt keinen Einblick in die sozialen Graphen oder Profile seiner NutzerInnen. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        ### Installation und Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Signal installieren |Ladet die Signal-App herunter und installiert sie: <br>• [Google Play Store](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=de_DE)<br>• [Aurora Store](https://auroraoss.com/de/)<br>• [Signals Webseite](https://signal.org/android/apk/). |
        |Tracker frei |Die Signal-App [enthält 0 Tracker und benötigt 67 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/org.thoughtcrime.securesms/latest/). Zum Vergleich:<br>• TikTok: 16 Tracker, 76 Berechtigungen<br>• Snapchat: 2 Tracker, 44 Berechtigungen<br>• WhatsApp: 1 Tracker, 57 Berechtigungen |
        |Kein GCM |Androids Signal-App greift auf Websockets zurück, und kann somit Push-Benachrichtigungen ohne Googles Cloud Messaging (GCM) verarbeiten. Dies ist besonders dann praktisch, wenn Ihr Euer [Smartphone entgoogeln](https://gofoss.net/de/intro-free-your-phone/) wollt. |
        |Registrierung |• Öffnet die Signal-App und folgt den Anweisungen auf dem Bildschirm <br>• Gebt Eure Telefonnummer an und tippt auf `Registrieren` <br>• Ihr erhaltet dann einen Verifizierungscode per SMS, der automatisch erkannt werden sollte <br>• Falls das nicht klappt, könnt Ihr Euch auch anrufen lassen |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Texteingabefeld` und verfasst eine Nachricht <br>• Tippt auf die blaue Schaltfläche `Senden` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Einzel-Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |SMS/MMS | • Die Android Signal-App kann ebenfalls unverschlüsselte SMS oder MMS über Euren Mobilfunkanbieter versenden <br>• Begebt Euch hierzu ins Menü `Einstellungen ‣ Unterhaltungen ‣ SMS und MMS ‣ Als Standard-SMS-App verwenden`<br>• Nun könnt Ihr in einer beliebigen Unterhaltung lange auf die Schaltfläche `Senden` drücken <br>• Wählt anschließend die graue Schaltfläche, um eine unverschlüsselte SMS-Nachricht zu versenden |
        |Einzel-Sprachanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Telefon` Symbol im oberen Menü, um einen Sprachanruf zu tätigen |
        |Einzel-Videoanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Kamera` Symbol im oberen Menü, um einen Videoanruf zu tätigen |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chat beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt `Neue Gruppe` <br>• Fügt bestehende Kontakte hinzu oder gebt neue Nummern an <br>• Tippt auf die Schaltfläche `Weiter` <br>• Wählt einen Gruppennamen <br>• Tippt auf die Schaltfläche `Erstellen` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Gruppen-Chat <br>• Tippt auf den Gruppennamen <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |SMS/MMS |• Die Android Signal-App kann ebenfalls unverschlüsselte MMS-Gruppen anlegen <br>• Nachrichten werden über Euren Mobilfunkanbieter gesendet <br>• Die `Senden`-Schaltfläche erscheint grau statt blau |
        |Gruppen-Sprach- oder Videoanruf tätigen |• Öffnet Signal <br>• Erstellt einen neuen oder öffnet einen bestehenden Gruppen-Chat <br>• Tippt auf das `Kamera`-Symbol im oberen Menü <br>• Wählt `Anruf starten` or `Anruf beitreten` <br>• Für einen Gruppen-Sprachanruf könnt Ihr einfach die Kamera abschalten |

        ### Authentifizierung, Signal-PIN & Datensicherung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Authentifizierung |• Stellt sicher, dass Ihr mit den richtigen Leuten sprecht <br>• Signal kann Gesprächspartner anhand eines digitalen Fingerabdrucks authentifizieren <br>• Öffnet hierzu die Signal-App  <br>• Öffnet einen bestehenden Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Wählt `Sicherheitsnummer anzeigen` <br>• Vergleicht die angezeigte Zahlenfolge mit der auf dem Telefon Eures Kontakts <br>• Falls die Zahlenfolgen übereinstimmen, tippt auf die Schaltfläche `Verifiziert` <br>• Ihr könnt auch einfach den auf dem Telefon Eures Kontakts angezeigten QR-Code scannen <br>• Nach erfolgreicher Verifizierung wird unter dem Namen Eures Kontakts ein Häkchen angezeigt <br>• Sollte sich die Sicherheitsnummer mal ändern, z.B. wenn Ihr oder Eure Kontakte Telefon wechselt oder Signal neu installiert, werdet Ihr dazu eingeladen den Authentifizierungsprozess zu wiederholen |
        |Signal-PIN |• Mithilfe der Signal-PIN könnt Ihr bei Verlust oder Tausch Eures Telefons sowohl Profil als auch Einstellungen und Kontakte schnell wiederherstellen <br>• Und zwar ohne Euren Sozial-Graph an Signal preiszugeben <br>• Öffnet hierzu den Menüeintrag `Einstellungen ‣ Konto ‣ PIN ändern`, um eine PIN-Nummer festzulegen und zu verwalten <br>• Legt eine [sichere, individuelle PIN-Nummer](https://gofoss.net/de/passwords/) fest und bewahrt sie sicher auf <br>• Signal kann Eure PIN-Nummer nicht zurücksetzen. Solltet Ihr Eure PIN-Nummer also vergessen haben, könnt Ihr Eure Konto-Informationen teilweise nicht wiederherstellen. Es kann auch sein, dass Ihr bis zu 7 Tage warten müsst, bevor Ihr ein neues Konto anlegen könnt |
        |Chat-Sicherungskopien |• Eure Signal-Nachrichten sind verschlüsselt und lediglich auf Euren Geräten gespeichert <br>• Solltet Ihr Euer Telefon verlieren oder Nachrichten versehentlich löscht, sind diese für immer verloren <br>• Um Nachrichten wiederherstellen oder auf ein anderes Gerät übertragen zu können, müsst Ihr Sicherungskopien anlegen <br><br>*Sicherungskopien anlegen*<br>• Wählt `Einstellungen ‣ Unterhaltungen ‣ Unterhaltungen sichern ‣ Einschalten` <br>• Wählt einen Ordner, in dem Eure Sicherungskopien abgelegt werden sollen <br>• [Bewahrt das angezeigte 30-stellige Kennwort sicher auf](https://gofoss.net/de/passwords/), es wird zum Wiederherstellen der Sicherungskopien benötigt <br>• Tippt auf `Sicherungen aktivieren` <br>• Tippt auf `Datensicherung erstellen` <br>• Verschiebt die so erzeugten Sicherungskopien auf einen [externen Datenträger](https://gofoss.net/de/backups/) <br><br>*Sicherungskopien wiederherstellen*• Verschiebt die Sicherungskopien vom externen Datenträger auf Euer Telefon <br>• Installiert Signal (erneut) <br>• Wählt `Konto übertragen oder wiederherstellen` <br>• Wählt `Backup wiederherstellen` <br>• Gebt das 30-stellige Kennwort an <br>• Gebt Eure Telefonnummer zur Registrierung an |
        |Medien-Sicherungskopien |• Fotos, Videos, Audiodateien usw. sind verschlüsselt und lediglich auf Euren Geräten gespeichert <br>• Solltet Ihr Euer Telefon verlieren oder Mediendateien versehentlich löscht, sind diese für immer verloren <br>• [Legt Sicherungskopien an](https://gofoss.net/de/backups/)! <br>• Wählt `Einstellungen ‣ Daten und Speicher ‣ Speicher verwalten ‣ Speicherinhalte überprüfen` <br>• Wählt die Dateien aus, die Ihr sichern wollt <br>• Tippt auf die Schaltflächen `Speichern` und `Ja`, um die (unverschlüsselten) Mediendateien außerhalb von Signal abzuspeichern <br>• Verschiebt die so erzeugten Sicherungskopien auf einen [externen Datenträger](https://gofoss.net/de/backups/) |


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        ### Installation und Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Installation |Installiert Signal vom [App Store](https://apps.apple.com/de/app/signal-private-messenger/id874139669/). |
        |Registrierung |• Öffnet die Signal-App und folgt den Anweisungen auf dem Bildschirm <br>• Gebt Eure Telefonnummer an und tippt auf `Registrieren` <br>• Ihr erhaltet dann einen Verifizierungscode per SMS, der automatisch erkannt werden sollte <br>• Falls das nicht klappt, könnt Ihr Euch auch anrufen lassen |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Texteingabefeld` und verfasst eine Nachricht <br>• Tippt auf die blaue Schaltfläche `Senden` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Einzel-Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Einzel-Sprachanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Telefon` Symbol im oberen Menü, um einen Sprachanruf zu tätigen |
        |Einzel-Videoanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Kamera` Symbol im oberen Menü, um einen Videoanruf zu tätigen |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chat beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt `Neue Gruppe` <br>• Fügt bestehende Kontakte hinzu oder gebt neue Nummern an <br>• Tippt auf die Schaltfläche `Weiter` <br>• Wählt einen Gruppennamen <br>• Tippt auf die Schaltfläche `Erstellen` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Gruppen-Chat <br>• Tippt auf den Gruppennamen <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Gruppen-Sprach- oder Videoanruf tätigen |• Öffnet Signal <br>• Erstellt einen neuen oder öffnet einen bestehenden Gruppen-Chat <br>• Tippt auf das `Kamera`-Symbol im oberen Menü <br>• Wählt `Anruf starten` or `Anruf beitreten` <br>• Für einen Gruppen-Sprachanruf könnt Ihr einfach die Kamera abschalten |

        ### Authentifizierung, Signal-PIN & Datensicherung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Authentifizierung |• Stellt sicher, dass Ihr mit den richtigen Leuten sprecht <br>• Signal kann Gesprächspartner anhand eines digitalen Fingerabdrucks authentifizieren <br>• Öffnet hierzu die Signal-App  <br>• Öffnet einen bestehenden Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Wählt `Sicherheitsnummer anzeigen` <br>• Vergleicht die angezeigte Zahlenfolge mit der auf dem Telefon Eures Kontakts <br>• Falls die Zahlenfolgen übereinstimmen, tippt auf die Schaltfläche `Verifiziert` <br>• Ihr könnt auch einfach den auf dem Telefon Eures Kontakts angezeigten QR-Code scannen <br>• Nach erfolgreicher Verifizierung wird unter dem Namen Eures Kontakts ein Häkchen angezeigt <br>• Sollte sich die Sicherheitsnummer mal ändern, z.B. wenn Ihr oder Eure Kontakte Telefon wechselt oder Signal neu installiert, werdet Ihr dazu eingeladen den Authentifizierungsprozess zu wiederholen |
        |Signal-PIN |• Mithilfe der Signal-PIN könnt Ihr bei Verlust oder Tausch Eures Telefons sowohl Profil als auch Einstellungen und Kontakte schnell wiederherstellen <br>• Und zwar ohne Euren Sozial-Graph an Signal preiszugeben <br>• Öffnet hierzu den Menüeintrag `Einstellungen ‣ Konto ‣ PIN ändern`, um eine PIN-Nummer festzulegen und zu verwalten <br>• Legt eine [sichere, individuelle PIN-Nummer](https://gofoss.net/de/passwords/) fest und bewahrt sie sicher auf <br>• Signal kann Eure PIN-Nummer nicht zurücksetzen. Solltet Ihr Eure PIN-Nummer also vergessen haben, könnt Ihr Eure Konto-Informationen teilweise nicht wiederherstellen. Es kann auch sein, dass Ihr bis zu 7 Tage warten müsst, bevor Ihr ein neues Konto anlegen könnt |
        |Medien-Sicherungskopien |• Fotos, Videos, Audiodateien usw. sind verschlüsselt und lediglich auf Euren Geräten gespeichert <br>• Solltet Ihr Euer Telefon verlieren oder Mediendateien versehentlich löscht, sind diese für immer verloren <br>• [Legt Sicherungskopien an](https://gofoss.net/de/backups/)! <br>• Öffnet eine Unterhaltung <br>• Tippt auf den Kontaktnamen im oberen Menü, um die Chat-Einstellungen anzuzeigen <br>• Wählt `Alle Medien` <br>• Wählt die Dateien aus, die Ihr sichern wollt <br>• Tippt auf die Schaltfläche `Teilen` und wählt `Bild speichern` oder `Video speichern`, um die (unverschlüsselten) Mediendateien außerhalb von Signal abzuspeichern <br>• Verschiebt die so erzeugten Sicherungskopien auf einen [externen Datenträger](https://gofoss.net/de/backups/) |


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        ### Installation & Kopplung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Installation |• Vergewissert Euch, dass Signal auf Eurem Telefon installiert und funktionsfähig ist <br>• Ladet Signals Electron-basierten [Windows Desktop-Client](https://signal.org/download/windows/) herunter <br>• Klickt auf die Schaltfläche `Ausführen` <br>• Folgt dem Installationsassistenten |
        |Kopplung |• Öffnet Signal auf Eurem Telefon <br>• Wählt `Einstellungen ‣ Gekoppelte Geräte` <br>• Tippt auf die `+`-Schaltfläche (Android) oder `Neue Geräte koppeln`-Schaltfläche (iPhone) <br>• Scannt mit der Signal-App Eures Telefons den QR-Code, der auf Eurem Windows-Computer angezeigt wird <br>• Wählt einen Namen für das gekoppelte Gerät aus <br>• Wählt `Fertigstellen` |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Beginnt eine neue Unterhaltung, oder wählt eine bestehende aus <br>• Verfasst Eure Nachricht <br>• Klickt auf `EINGABE`, um Eure Nachricht zu senden |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Einzel-Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Einzel-Sprachanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Telefon` Symbol im oberen Menü, um einen Sprachanruf zu tätigen |
        |Einzel-Videoanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Kamera` Symbol im oberen Menü, um einen Videoanruf zu tätigen <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Präsentation starten` und gebt an, ob Ihr den gesamten Bildschirm oder ein bestimmtes Fenster mit den anderen Teilnehmern teilen wollt |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chat beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt `Neue Gruppe` <br>• Fügt bestehende Kontakte hinzu oder gebt neue Nummern an <br>• Tippt auf die Schaltfläche `Weiter` <br>• Wählt einen Gruppennamen <br>• Tippt auf die Schaltfläche `Erstellen` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Gruppen-Chat <br>• Tippt auf den Gruppennamen <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Gruppen-Sprach- oder Videoanruf tätigen |• Öffnet Signal <br>• Erstellt einen neuen oder öffnet einen bestehenden Gruppen-Chat <br>• Tippt auf das `Kamera`-Symbol im oberen Menü <br>• Wählt `Anruf starten` or `Anruf beitreten` <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Präsentation starten` und gebt an, ob Ihr den gesamten Bildschirm oder ein bestimmtes Fenster mit den anderen Teilnehmern teilen wollt <br>• Für einen Gruppen-Sprachanruf könnt Ihr einfach die Kamera abschalten |

        ### Authentifizierung & Datensicherung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Authentifizierung |• Stellt sicher, dass Ihr mit den richtigen Leuten sprecht <br>• Signal kann Gesprächspartner anhand eines digitalen Fingerabdrucks authentifizieren <br>• Öffnet hierzu die Signal-App  <br>• Öffnet einen bestehenden Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Wählt `Sicherheitsnummer anzeigen` <br>• Vergleicht die angezeigte Zahlenfolge mit der auf dem Telefon Eures Kontakts <br>• Falls die Zahlenfolgen übereinstimmen, tippt auf die Schaltfläche `Verifiziert` <br>• Nach erfolgreicher Verifizierung wird unter dem Namen Eures Kontakts ein Häkchen angezeigt <br>• Sollte sich die Sicherheitsnummer mal ändern, z.B. wenn Ihr oder Eure Kontakte Telefon wechselt oder Signal neu installiert, werdet Ihr dazu eingeladen den Authentifizierungsprozess zu wiederholen |
        |Medien-Sicherungskopien |• Fotos, Videos, Audiodateien usw. sind verschlüsselt und lediglich auf Euren Geräten gespeichert <br>• Solltet Ihr Euer Telefon verlieren oder Mediendateien versehentlich löscht, sind diese für immer verloren <br>• [Legt Sicherungskopien an](https://gofoss.net/de/backups/)! <br>• Öffnet eine Unterhaltung <br>• Tippt auf den Kontaktnamen im oberen Menü, um die Chat-Einstellungen anzuzeigen <br>• Wählt `Neueste Medien anzeigen` <br>• Wählt die Dateien aus, die Ihr sichern wollt <br>• Tippt auf die Schaltfläche `Speichern`, um die (unverschlüsselten) Mediendateien außerhalb von Signal abzuspeichern <br>• Verschiebt die so erzeugten Sicherungskopien auf einen [externen Datenträger](https://gofoss.net/de/backups/) |


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        ### Installation & Kopplung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Installation |• Vergewissert Euch, dass Signal auf Eurem Telefon installiert und funktionsfähig ist <br>• Ladet Signals Electron-basierten [macOS Desktop-Client](https://signal.org/download/macos/) herunter <br>• Dieser sollte sich von selbst öffnen und ein neues Laufwerk einbinden, das die Signal-Anwendung enthält <br>• Sollte dies nicht der Fall sein, öffnet die heruntergeladene `.dmg`-Datei und verschiebt das erscheinende Signal-Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff öffnet den Anwendungsordner und verschiebt das Signal-Symbol in das Andockmenü |
        |Kopplung |• Öffnet Signal auf Eurem Telefon <br>• Wählt `Einstellungen ‣ Gekoppelte Geräte` <br>• Tippt auf die `+`-Schaltfläche (Android) oder `Neue Geräte koppeln`-Schaltfläche (iPhone) <br>• Scannt mit der Signal-App Eures Telefons den QR-Code, der auf Eurem macOS angezeigt wird <br>• Wählt einen Namen für das gekoppelte Gerät aus <br>• Wählt `Fertigstellen` |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Beginnt eine neue Unterhaltung, oder wählt eine bestehende aus <br>• Verfasst Eure Nachricht <br>• Klickt auf `EINGABE`, um Eure Nachricht zu senden |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Einzel-Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Einzel-Sprachanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Telefon` Symbol im oberen Menü, um einen Sprachanruf zu tätigen |
        |Einzel-Videoanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Kamera` Symbol im oberen Menü, um einen Videoanruf zu tätigen <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Präsentation starten` und gebt an, ob Ihr den gesamten Bildschirm oder ein bestimmtes Fenster mit den anderen Teilnehmern teilen wollt |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chat beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt `Neue Gruppe` <br>• Fügt bestehende Kontakte hinzu oder gebt neue Nummern an <br>• Tippt auf die Schaltfläche `Weiter` <br>• Wählt einen Gruppennamen <br>• Tippt auf die Schaltfläche `Erstellen` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Gruppen-Chat <br>• Tippt auf den Gruppennamen <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Gruppen-Sprach- oder Videoanruf tätigen |• Öffnet Signal <br>• Erstellt einen neuen oder öffnet einen bestehenden Gruppen-Chat <br>• Tippt auf das `Kamera`-Symbol im oberen Menü <br>• Wählt `Anruf starten` or `Anruf beitreten` <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Präsentation starten` und gebt an, ob Ihr den gesamten Bildschirm oder ein bestimmtes Fenster mit den anderen Teilnehmern teilen wollt <br>• Für einen Gruppen-Sprachanruf könnt Ihr einfach die Kamera abschalten |

        ### Authentifizierung & Datensicherung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Authentifizierung |• Stellt sicher, dass Ihr mit den richtigen Leuten sprecht <br>• Signal kann Gesprächspartner anhand eines digitalen Fingerabdrucks authentifizieren <br>• Öffnet hierzu die Signal-App <br>• Öffnet einen bestehenden Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Wählt `Sicherheitsnummer anzeigen` <br>• Vergleicht die angezeigte Zahlenfolge mit der auf dem Telefon Eures Kontakts <br>• Falls die Zahlenfolgen übereinstimmen, tippt auf die Schaltfläche `Verifiziert` <br>• Nach erfolgreicher Verifizierung wird unter dem Namen Eures Kontakts ein Häkchen angezeigt <br>• Sollte sich die Sicherheitsnummer mal ändern, z.B. wenn Ihr oder Eure Kontakte Telefon wechselt oder Signal neu installiert, werdet Ihr dazu eingeladen den Authentifizierungsprozess zu wiederholen |
        |Medien-Sicherungskopien |• Fotos, Videos, Audiodateien usw. sind verschlüsselt und lediglich auf Euren Geräten gespeichert <br>• Solltet Ihr Euer Telefon verlieren oder Mediendateien versehentlich löscht, sind diese für immer verloren <br>• [Legt Sicherungskopien an](https://gofoss.net/de/backups/)! <br>• Öffnet eine Unterhaltung <br>• Tippt auf den Kontaktnamen im oberen Menü, um die Chat-Einstellungen anzuzeigen <br>• Wählt `Neueste Medien anzeigen` <br>• Wählt die Dateien aus, die Ihr sichern wollt <br>• Tippt auf die Schaltfläche `Speichern`, um die (unverschlüsselten) Mediendateien außerhalb von Signal abzuspeichern <br>• Verschiebt die so erzeugten Sicherungskopien auf einen [externen Datenträger](https://gofoss.net/de/backups/) |


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        ### Installation & Kopplung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Installation |• Vergewissert Euch, dass Signal auf Eurem Telefon installiert und funktionsfähig ist <br>• Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` oben links und sucht nach `Terminal` <br>• Installiert den Signierschlüssel: <br>`wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg` <br>`cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null` <br>• Fügt Signal zur Paketquellen-Liste hinzu: <br> `echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list` <br>• Aktualisiert die Paketquellen und installiert Signal: <br>`sudo apt update && sudo apt install signal-desktop`|
        |Kopplung |• Öffnet Signal auf Eurem Telefon <br>• Wählt `Einstellungen ‣ Gekoppelte Geräte` <br>• Tippt auf die `+`-Schaltfläche (Android) oder `Neue Geräte koppeln`-Schaltfläche (iPhone) <br>• Scannt mit der Signal-App Eures Telefons den QR-Code, der auf Eurem Linux-Gerät angezeigt wird <br>• Wählt einen Namen für das gekoppelte Gerät aus <br>• Wählt `Fertigstellen` |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Beginnt eine neue Unterhaltung, oder wählt eine bestehende aus <br>• Verfasst Eure Nachricht <br>• Klickt auf `EINGABE`, um Eure Nachricht zu senden |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Einzel-Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Einzel-Sprachanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Telefon` Symbol im oberen Menü, um einen Sprachanruf zu tätigen |
        |Einzel-Videoanruf tätigen |• Öffnet Signal <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt einen Kontakt aus oder gebt eine Nummer an <br>• Tippt auf das `Kamera` Symbol im oberen Menü, um einen Videoanruf zu tätigen <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Präsentation starten` und gebt an, ob Ihr den gesamten Bildschirm oder ein bestimmtes Fenster mit den anderen Teilnehmern teilen wollt |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chat beginnen |• Öffnet die Signal-App <br>• Tippt auf die Schaltfläche `Verfassen` <br>• Wählt `Neue Gruppe` <br>• Fügt bestehende Kontakte hinzu oder gebt neue Nummern an <br>• Tippt auf die Schaltfläche `Weiter` <br>• Wählt einen Gruppennamen <br>• Tippt auf die Schaltfläche `Erstellen` |
        |Verschwindende Nachrichten |• Signal ermöglicht das Setzen einer Frist, nach deren Ablauf die Nachrichten von Euren Geräten verschwinden <br>• Öffnet hierzu einen Gruppen-Chat <br>• Tippt auf den Gruppennamen <br>• Aktiviert die Option `Verschwindende Nachrichten` <br>• Legt eine Frist von bis zu einer Woche fest <br>• Sobald die Option aktiv ist, wird ein Timer-Symbol neben jeder verschwindenden Nachricht sichtbar |
        |Gruppen-Sprach- oder Videoanruf tätigen |• Öffnet Signal <br>• Erstellt einen neuen oder öffnet einen bestehenden Gruppen-Chat <br>• Tippt auf das `Kamera`-Symbol im oberen Menü <br>• Wählt `Anruf starten` or `Anruf beitreten` <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Präsentation starten` und gebt an, ob Ihr den gesamten Bildschirm oder ein bestimmtes Fenster mit den anderen Teilnehmern teilen wollt <br>• Für einen Gruppen-Sprachanruf könnt Ihr einfach die Kamera abschalten |

        ### Authentifizierung & Datensicherung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Authentifizierung |• Stellt sicher, dass Ihr mit den richtigen Leuten sprecht <br>• Signal kann Gesprächspartner anhand eines digitalen Fingerabdrucks authentifizieren <br>• Öffnet hierzu die Signal-App  <br>• Öffnet einen bestehenden Chat <br>• Tippt auf den Namen Eures Kontakts <br>• Wählt `Sicherheitsnummer anzeigen` <br>• Vergleicht die angezeigte Zahlenfolge mit der auf dem Telefon Eures Kontakts <br>• Falls die Zahlenfolgen übereinstimmen, tippt auf die Schaltfläche `Verifiziert` <br>• Nach erfolgreicher Verifizierung wird unter dem Namen Eures Kontakts ein Häkchen angezeigt <br>• Sollte sich die Sicherheitsnummer mal ändern, z.B. wenn Ihr oder Eure Kontakte Telefon wechselt oder Signal neu installiert, werdet Ihr dazu eingeladen den Authentifizierungsprozess zu wiederholen |
        |Medien-Sicherungskopien |• Fotos, Videos, Audiodateien usw. sind verschlüsselt und lediglich auf Euren Geräten gespeichert <br>• Solltet Ihr Euer Telefon verlieren oder Mediendateien versehentlich löscht, sind diese für immer verloren <br>• [Legt Sicherungskopien an](https://gofoss.net/de/backups/)! <br>• Öffnet eine Unterhaltung <br>• Tippt auf den Kontaktnamen im oberen Menü, um die Chat-Einstellungen anzuzeigen <br>• Wählt `Neueste Medien anzeigen` <br>• Wählt die Dateien aus, die Ihr sichern wollt <br>• Tippt auf die Schaltfläche `Speichern`, um die (unverschlüsselten) Mediendateien außerhalb von Signal abzuspeichern <br>• Verschiebt die so erzeugten Sicherungskopien auf einen [externen Datenträger](https://gofoss.net/de/backups/) |

<div style="margin-top:-20px">
</div>

??? warning "Die Kommunikation über Signal ist zwar verschlüsselt, aber nicht anonym"

    Signal trifft mehrere Maßnahmen, um das "Null-Wissen"-Prinzip zu gewährleisten und so wenig Metadaten wie möglich zu erheben: [Private-Contact-Discovery](https://signal.org/blog/private-contact-discovery/), [Sealed-Sender](https://signal.org/blog/sealed-sender/), [Signal-PIN](https://signal.org/blog/signal-pins/), [Secure Value Recovery](https://signal.org/blog/secure-value-recovery/) usw. Beachtet dennoch, dass Signal trotz dieser Datenschutzmaßnahmen nicht anonym ist, und z.B. auf Eure Telefonnummer Zugriff hat.


<br>

<center> <img src="../../assets/img/separator_element.svg" alt="Element" width="150px"></img> </center>

## Element

[Element](https://element.io/) ist eine [quelloffene](https://github.com/vector-im/) Nachrichten-, Sprach- und Videoanruf-App, die auf allen Desktop- sowie Mobilplattformen verfügbar ist. Element basiert auf dem [Matrix-Protokoll.](https://matrix.org/) und stützt sich auf eine dezentrale Architektur. Für die Registrierung ist keine Telefonnummer erforderlich. BenutzerInnen können sich bei mehreren unabhängigen öffentlichen Servern registrieren oder sogar eine eigene Server-Instanz hosten. Element unterstützt ebenfalls durchgängige Verschlüsselung, Nachrichteninhalte können somit nicht von Dritten eingesehen werden. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        ### Installation

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Element installieren |Ladet die Element-App herunter und installiert sie: <br>• [Google Play Store](https://play.google.com/store/apps/details?id=im.vector.app&hl=de_DE)<br>• [Aurora Store](https://auroraoss.com/de/)<br>• [F-Droid](https://f-droid.org/de/packages/im.vector.app/) |
        |Tracker frei |Die Element-App [enthält 0 Tracker und benötigt 37 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/im.vector.app/latest/). Zum Vergleich:<br>• TikTok: 16 Tracker, 76 Berechtigungen<br>• Snapchat: 2 Tracker, 44 Berechtigungen<br>• WhatsApp: 1 Tracker, 57 Berechtigungen |
        |Kein GCM |F-Droids Version der Element-App kann Push-Benachrichtigungen ohne Googles Cloud Messaging (GCM) verarbeiten. Dies ist besonders dann praktisch, wenn Ihr Euer [Smartphone entgoogeln](https://gofoss.net/de/intro-free-your-phone/) wollt. |

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Jeder Matrix-Benutzer erhält eine eindeutige Kennung, die einer [E-Mail-Adresse](https://gofoss.net/de/encrypted-emails/) ziemlich ähnlich sieht: `@benutzername:servername.net`. Ihr könnt jederzeit eine neue Matrix-Kennung bei einem beliebigen Anbieter Eurer Wahl erstellen, oder sogar Eure eigene Matrix-Instanz hosten. |
        |Element öffnen |Öffnet die Element-App und klickt auf `Beginne`. |
        |Server wählen |Wählt den Standard-Server [matrix.org](https://matrix.org) oder einen öffentlichen Server Eurer Wahl. Eine Auswahl findet Ihr hier: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Registrieren |Klickt auf `Registrieren` und gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Diese Zugangsdaten sind für die Anmeldung bei Element erforderlich. |
        |Datenschutzerklärung |Lest Euch aufmerksam die Datenschutzerklärung des Servers durch und klickt auf `Akzeptieren`. |
        |E-Mail-Verifizierung |Gebt eine bestehende oder eine [Wegwerf-Email-Adresse](https://gofoss.net/de/cloud-providers/#other-cloud-services) an, um Eure Zugangsdaten zu verifizieren und ggf. wiederherzustellen. Öffnet hierzu den Link in der Bestätigungs-Email und schließt damit die Registrierung ab. |

        ### Sicherheitsschlüssel

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |*Dieser Schritt ist lediglich nach der ersten Anmeldung erforderlich*. <br> Element verwendet durchgängige Verschlüsselung. Um sicherzustellen, dass Ihr jederzeit und von jedem Gerät aus Zugriff auf Eure verschlüsselten Nachrichten habt, müsst Ihr einen sogenannten Sicherheitsschlüssel generieren. |
        |Einstellungen |Ruft den Menü-Eintrag `Einstellungen ‣ Sicherheit und Privatsphäre ‣ Sicheres Backup ‣ Backup einrichten` auf. |
        |Sicherheitsphrase |• Wählt `Eine Sicherheitsphrase benutzen` und gebt eine [sichere, individuelle Sicherheitsphrase](https://gofoss.net/de/passwords/) ein <br>• Diese Sicherheitsphrase schützt Euren Sicherheitsschlüssel <br>• *Verwendet auf keinen Fall Euer Element-Kennwort als Sicherheitsphrase!* <br>• Tippt auf `Fortsetzen`, bestätigt Eure Sicherheitsphrase und tippt erneut auf `Fortsetzen`. |
        |Sicherheitsschlüssel |Element generiert nun einen Sicherheitsschlüssel und legt eine verschlüsselte Sicherungskopie auf dem Matrix Server ab. |
        |Sicherungskopie |Ihr solltet ebenfalls die Sicherheitsphrase sowie den Sicherheitsschlüssel an einem sicheren Ort aufbewahren, z.B. in einem [Passwortmanager](https://gofoss.net/de/passwords/)! Dort solltet Ihr drei Dinge abspeichern: <br>• Euer Kennwort, das zur Anmeldung bei Element nötig ist <br>• die Sicherheitsphrase, die den Sicherheitsschlüssel schützt <br>• den Sicherheitsschlüssel, der zum Zugriff auf Eure verschlüsselten Nachrichten nötig ist |
        |Wiederherstellung |Sollte Euer Android-Gerät nicht mehr auf Eure Nachrichten zugreifen können, liegt das wahrscheinlich daran, dass es nicht über den richtigen Sicherheitsschlüssel verfügt. Ruft in diesem Fall den Menü-Eintrag `Einstellungen ‣ Sicherheit und Privatsphäre ‣ Verwaltung der Kryptoschlüssel ‣ Wiederherstellung verschlüsselter Nachrichten` auf, klickt auf `Von Sicherung wiederherstellen` und gebt Eure Sicherheitsphrase ein. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Tippt auf die `+`-Schaltfläche <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Tippt auf `ERSTELLEN` <br>• Beginnt zu chatten |
        |Sprachnachricht senden |• Öffnet die Element-App <br>• Öffnet einen bestehenden Chat, oder beginnt einen neuen <br>• Die `Sprachnachricht`-Schaltfläche befindet sich neben dem Texteingabefeld <br>• Haltet die `Sprachnachricht`-Schaltfläche gedrückt, um Eure Nachricht aufzuzeichnen. Lasst los, um die Sprachnachricht zu versenden. Oder aber wischt nach links, um den Vorgang abzubrechen <br>• Um längere Sprachnachrichten aufzunehmen, haltet die `Sprachnachricht`-Schaltfläche gedrückt und wischt nach oben. Damit könnt Ihr die Sprachnachricht noch einmal abhören, bevor Ihr auf `Senden` drückt. Oder bevor Ihr auf den `Papierkorb` drückt, um die Nachricht zu löschen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Tippt auf die `+`-Schaltfläche <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Tippt auf `ERSTELLEN` <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Tippt auf die `+`-Schaltfläche <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Tippt auf `ERSTELLEN` <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Öffentlichen Räumen beitreten |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Tippt auf die `#+`-Schaltfläche <br>• Stöbert durch das Raumverzeichnis <br>• Oder tippt auf die Lupe und sucht nach Räumen, die Euch interessieren <br>• Ihr könnt auch andere Server auswählen, um dort nach weiteren Räumen zu suchen <br>• Tippt auf `BETRETEN`, um einer Gemeinschaft beizutreten |
        |Neue Räume erstellen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Tippt auf die `#+`-Schaltfläche <br>• Tippt auf `NEUEN RAUM ERSTELLEN` <br>• Wählt einen Raumnamen <br>• Gebt an, ob der Raum privater oder öffentlicher Natur ist (in letzterem Fall muss eine Adresse angegeben werden) <br>• Aktiviert oder deaktiviert die Verschlüsselung <br>• Optional könnt Ihr auch NutzerInnen anderer Matrix-Server blockieren <br>• Tippt auf `ERSTELLEN` <br>• Tippt auf die `Leute hinzufügen`-Schaltfläche und ladet andere Personen anhand ihrer Benutzernamen oder E-Mail-Adressen ein <br>• Optional können sehr detaillierte Raumeinstellungen definiert werden, z.B. URL-Vorschau, Raumzugang, Nachrichtenverlauf, Nutzerrechte, usw. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Gruppen-Sprachanruf |
        |Gruppen-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Gruppen-Videoanruf |

        ### Benachrichtigungen

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Globale Benachrichtigungs-Einstellungen |• Öffnet die Element-App <br>• Ruft den Menü-Eintrag `Einstellungen ‣ Benachrichtigungen` aus und konfiguriert globale Benachrichtigungen nach Euren Wünschen <br>• Aktiviert oder deaktiviert Benachrichtigungen, die das Benutzerkonto oder die Sitzung betreffen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Nachrichten die Euch erwähnen, für Einzel- oder Gruppenchats, für Einladungen usw. <br>• Konfiguriert LED-Farben, Vibrationsfunktionen sowie Klingeltöne |
        |Einstellungen für Direktnachrichten-Benachrichtigungen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Drückt länger auf einen bestehenden Chat <br>• Aktiviert oder deaktiviert Benachrichtigungen für sämtliche Nachrichten, oder lediglich für solche die Euch namentlich erwähnen |
        |Einstellungen für Raumbenachrichtigungen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Drückt länger auf einen bestehenden Raum <br>• Aktiviert oder deaktiviert Benachrichtigungen für sämtliche Nachrichten, oder lediglich für solche die Euch namentlich erwähnen |

        ### Authentifizierung und Verifizierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Unter bestimmten Umständen überprüft Element Eure Identität. Zum Beispiel, wenn Ihr Euch von einem neuen Gerät anmeldet oder eine neue Sitzung eröffnet. Oder wenn Ihr Euch über mehrere Geräte gleichzeitig anmeldet. In solchen Fällen zeigt Element eine Mitteilung an: `Überprüfe diese Anmeldung` oder `Neue Anmeldung. Warst du das?`. Ihr verfügt über mehrere Möglichkeiten, um Eure Identität nachzuweisen. |
        |Sicherheitsphrase- oder Schlüssel |Die einfachste Lösung besteht darin, Eure Sicherheitsphrase oder Euren Sicherheitsschlüssel anzugeben. |
        |Andere Geräte |Ihr könnt auch ein anderes Gerät verwenden, das bereits angemeldet ist. Klickt hierzu auf die Verifizierungsanfrage, die auf diesem Gerät angezeigt wird. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |
        |Erfolgreiche Verifizierung |Sobald Eure Identität erfolgreich überprüft wurde erhaltet Ihr vollen Zugriff auf Eure Nachrichten und werden anderen Gesprächspartner über als vertrauenswürdig ausgewiesen. |
        |Aktive Sitzungen |Ihr könnt alle aktiven und verifizierten Sitzungen unter dem Menü-Eintrag `Einstellungen ‣ Sicherheit und Privatsphäre ‣ Aktive Sitzungen` einsehen. |
        |Benutzerverifizierung |Für zusätzliche Sicherheit könnt Ihr ebenfalls die Identität anderer Gesprächspartner überprüfen. Öffnet hierzu einen (verschlüsselten) Chat, ruft den Menü-Eintrag `Einstellungen ‣ Personen` auf, wählt den entsprechenden Gesprächspartner und klickt auf `Verifizieren ‣ Verifizierung beginnen`. Wartet, bis der Gesprächspartner die Verifizierungsanfrage akzeptiert. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |


=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        ### Installation

        Ladet die Element-App vom [App Store](https://apps.apple.com/de/app/vector/id1083446067/) herunter und installiert sie.

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Jeder Matrix-Benutzer erhält eine eindeutige Kennung, die einer E-Mail-Adresse ziemlich ähnlich sieht: `@benutzername:servername.net`. Ihr könnt jederzeit eine neue Matrix-Kennung bei einem beliebigen Anbieter Eurer Wahl erstellen, oder sogar Eure eigene Matrix-Instanz hosten. |
        |Element öffnen |Öffnet die Element-App und klickt auf `Beginne`. |
        |Server wählen |Wählt den Standard-Server [matrix.org](https://matrix.org) oder einen öffentlichen Server Eurer Wahl. Eine Auswahl findet Ihr hier: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Registrieren |Klickt auf `Registrieren` und gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Diese Zugangsdaten sind für die Anmeldung bei Element erforderlich. |
        |Datenschutzerklärung |Lest Euch aufmerksam die Datenschutzerklärung des Servers durch und klickt auf `Akzeptieren`. |
        |E-Mail-Verifizierung |Gebt eine bestehende oder eine [Wegwerf-Email-Adresse](https://gofoss.net/de/cloud-providers/#other-cloud-services) an, um Eure Zugangsdaten zu verifizieren und ggf. wiederherzustellen. Öffnet hierzu den Link in der Bestätigungs-Email und schließt damit die Registrierung ab. |

        ### Sicherheitsschlüssel

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |*Dieser Schritt ist lediglich nach der ersten Anmeldung erforderlich*. <br> Element verwendet durchgängige Verschlüsselung. Um sicherzustellen, dass Ihr jederzeit und von jedem Gerät aus Zugriff auf Eure verschlüsselten Nachrichten habt, müsst Ihr einen sogenannten Sicherheitsschlüssel generieren. |
        |Einstellungen |Ruft den Menü-Eintrag `Einstellungen ‣ Sicherheit und Privatsphäre ‣ Sicheres Backup ‣ Backup einrichten` auf. |
        |Sicherheitsphrase |• Wählt `Eine Sicherheitsphrase benutzen` und gebt eine [sichere, individuelle Sicherheitsphrase](https://gofoss.net/de/passwords/) ein <br>• Diese Sicherheitsphrase schützt Euren Sicherheitsschlüssel <br>• *Verwendet auf keinen Fall Euer Element-Kennwort als Sicherheitsphrase!* <br>• Tippt auf `Fortsetzen`, bestätigt Eure Sicherheitsphrase und tippt erneut auf `Fortsetzen`. |
        |Sicherheitsschlüssel |Element generiert nun einen Sicherheitsschlüssel und legt eine verschlüsselte Sicherungskopie auf dem Matrix Server ab. |
        |Sicherungskopie |Ihr solltet ebenfalls die Sicherheitsphrase sowie den Sicherheitsschlüssel an einem sicheren Ort aufbewahren, z.B. in einem [Passwortmanager](https://gofoss.net/de/passwords/)! Dort solltet Ihr drei Dinge abspeichern: <br>• Euer Kennwort, das zur Anmeldung bei Element nötig ist <br>• die Sicherheitsphrase, die den Sicherheitsschlüssel schützt <br>• den Sicherheitsschlüssel, der zum Zugriff auf Eure verschlüsselten Nachrichten nötig ist |
        |Wiederherstellung |Sollte Euer Android-Gerät nicht mehr auf Eure Nachrichten zugreifen können, liegt das wahrscheinlich daran, dass es nicht über den richtigen Sicherheitsschlüssel verfügt. Ruft in diesem Fall den Menü-Eintrag `Einstellungen ‣ Sicherheit und Privatsphäre ‣ Verwaltung der Kryptoschlüssel ‣ Wiederherstellung verschlüsselter Nachrichten` auf, klickt auf `Von Sicherung wiederherstellen` und gebt Eure Sicherheitsphrase ein. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Tippt auf die `+`-Schaltfläche <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Tippt auf `ERSTELLEN` <br>• Beginnt zu chatten |
        |Sprachnachricht senden |• Öffnet die Element-App <br>• Öffnet einen bestehenden Chat, oder beginnt einen neuen <br>• Die `Sprachnachricht`-Schaltfläche befindet sich neben dem Texteingabefeld <br>• Haltet die `Sprachnachricht`-Schaltfläche gedrückt, um Eure Nachricht aufzuzeichnen. Lasst los, um die Sprachnachricht zu versenden. Oder aber wischt nach links, um den Vorgang abzubrechen <br>• Um längere Sprachnachrichten aufzunehmen, haltet die `Sprachnachricht`-Schaltfläche gedrückt und wischt nach oben. Damit könnt Ihr die Sprachnachricht noch einmal abhören, bevor Ihr auf `Senden` drückt. Oder bevor Ihr auf den `Papierkorb` drückt, um die Nachricht zu löschen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Tippt auf die `+`-Schaltfläche <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Tippt auf `ERSTELLEN` <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Tippt auf die `+`-Schaltfläche <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Tippt auf `ERSTELLEN` <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Öffentlichen Räumen beitreten |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Tippt auf die `#+`-Schaltfläche <br>• Stöbert durch das Raumverzeichnis <br>• Oder tippt auf die Lupe und sucht nach Räumen, die Euch interessieren <br>• Ihr könnt auch andere Server auswählen, um dort nach weiteren Räumen zu suchen <br>• Tippt auf `BETRETEN`, um einer Gemeinschaft beizutreten |
        |Neue Räume erstellen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Tippt auf die `#+`-Schaltfläche <br>• Tippt auf `NEUEN RAUM ERSTELLEN` <br>• Wählt einen Raumnamen <br>• Gebt an, ob der Raum privater oder öffentlicher Natur ist (in letzterem Fall muss eine Adresse angegeben werden) <br>• Aktiviert oder deaktiviert die Verschlüsselung <br>• Optional könnt Ihr auch NutzerInnen anderer Matrix-Server blockieren <br>• Tippt auf `ERSTELLEN` <br>• Tippt auf die `Leute hinzufügen`-Schaltfläche und ladet andere Personen anhand ihrer Benutzernamen oder E-Mail-Adressen ein <br>• Optional können sehr detaillierte Raumeinstellungen definiert werden, z.B. URL-Vorschau, Raumzugang, Nachrichtenverlauf, Nutzerrechte, usw. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Gruppen-Sprachanruf |
        |Gruppen-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Gruppen-Videoanruf |

        ### Benachrichtigungen

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Globale Benachrichtigungs-Einstellungen |• Öffnet die Element-App <br>• Ruft den Menü-Eintrag `Einstellungen ‣ Benachrichtigungen` aus und konfiguriert globale Benachrichtigungen nach Euren Wünschen <br>• Aktiviert oder deaktiviert Benachrichtigungen, die das Benutzerkonto oder die Sitzung betreffen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Nachrichten die Euch erwähnen, für Einzel- oder Gruppenchats, für Einladungen usw. <br>• Konfiguriert LED-Farben, Vibrationsfunktionen sowie Klingeltöne |
        |Einstellungen für Direktnachrichten-Benachrichtigungen |• Öffnet die Element-App <br>• Wählt die Rubrik `Direktnachrichten` <br>• Drückt länger auf einen bestehenden Chat <br>• Aktiviert oder deaktiviert Benachrichtigungen für sämtliche Nachrichten, oder lediglich für solche die Euch namentlich erwähnen |
        |Einstellungen für Raumbenachrichtigungen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Drückt länger auf einen bestehenden Raum <br>• Aktiviert oder deaktiviert Benachrichtigungen für sämtliche Nachrichten, oder lediglich für solche die Euch namentlich erwähnen |

        ### Authentifizierung und Verifizierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Unter bestimmten Umständen überprüft Element Eure Identität. Zum Beispiel, wenn Ihr Euch von einem neuen Gerät anmeldet oder eine neue Sitzung eröffnet. Oder wenn Ihr Euch über mehrere Geräte gleichzeitig anmeldet. In solchen Fällen zeigt Element eine Mitteilung an: `Überprüfe diese Anmeldung` oder `Neue Anmeldung. Warst du das?`. Ihr verfügt über mehrere Möglichkeiten, um Eure Identität nachzuweisen. |
        |Sicherheitsphrase- oder Schlüssel |Die einfachste Lösung besteht darin, Eure Sicherheitsphrase oder Euren Sicherheitsschlüssel anzugeben. |
        |Andere Geräte |Ihr könnt auch ein anderes Gerät verwenden, das bereits angemeldet ist. Klickt hierzu auf die Verifizierungsanfrage, die auf diesem Gerät angezeigt wird. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |
        |Erfolgreiche Verifizierung |Sobald Eure Identität erfolgreich überprüft wurde erhaltet Ihr vollen Zugriff auf Eure Nachrichten und werden anderen Gesprächspartner über als vertrauenswürdig ausgewiesen. |
        |Aktive Sitzungen |Ihr könnt alle aktiven und verifizierten Sitzungen unter dem Menü-Eintrag `Einstellungen ‣ Sicherheit und Privatsphäre ‣ Aktive Sitzungen` einsehen. |
        |Benutzerverifizierung |Für zusätzliche Sicherheit könnt Ihr ebenfalls die Identität anderer Gesprächspartner überprüfen. Öffnet hierzu einen (verschlüsselten) Chat, ruft den Menü-Eintrag `Einstellungen ‣ Personen` auf, wählt den entsprechenden Gesprächspartner und klickt auf `Verifizieren ‣ Verifizierung beginnen`. Wartet, bis der Gesprächspartner die Verifizierungsanfrage akzeptiert. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        ### Installation

        Ladet den [Element-Desktop-Client für Windows](https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe) herunter und folgt dem Installationsassistenten.

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Jeder Matrix-Benutzer erhält eine eindeutige Kennung, die einer E-Mail-Adresse ziemlich ähnlich sieht: `@benutzername:servername.net`. Ihr könnt jederzeit eine neue Matrix-Kennung bei einem beliebigen Anbieter Eurer Wahl erstellen, oder sogar Eure eigene Matrix-Instanz hosten. |
        |Element öffnen |Öffnet die Element-App und klickt auf `Konto erstellen`. |
        |Server wählen |Wählt den Standard-Server [matrix.org](https://matrix.org) oder einen öffentlichen Server Eurer Wahl. Eine Auswahl findet Ihr hier: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Registrieren |Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Diese Zugangsdaten sind für die Anmeldung bei Element erforderlich. |
        |E-Mail-Verifizierung |Gebt eine bestehende oder eine [Wegwerf-Email-Adresse](https://gofoss.net/de/cloud-providers/#other-cloud-services) an, um Eure Zugangsdaten zu verifizieren und ggf. wiederherzustellen. Klickt auf `Registrieren`, lest Euch aufmerksam die Datenschutzerklärung durch und akzeptiert diese. Öffnet schließlich den Link in der Bestätigungs-Email und schließt die Registrierung ab. |

        ### Sicherheitsschlüssel

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |*Dieser Schritt ist lediglich nach der ersten Anmeldung erforderlich*. <br> Element verwendet durchgängige Verschlüsselung. Um sicherzustellen, dass Ihr jederzeit und von jedem Gerät aus Zugriff auf Eure verschlüsselten Nachrichten habt, müsst Ihr einen sogenannten Sicherheitsschlüssel generieren. |
        |Einstellungen |Nachdem Ihr Euch angemeldet habt, ruft den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Sicheres Backup ‣ Backup einrichten` auf. |
        |Sicherheitsphrase |• Wählt `Eine Sicherheitsphrase benutzen` und gebt eine [sichere, individuelle Sicherheitsphrase](https://gofoss.net/de/passwords/) ein <br>• Diese Sicherheitsphrase schützt Euren Sicherheitsschlüssel <br>• *Verwendet auf keinen Fall Euer Element-Kennwort als Sicherheitsphrase!* <br>• Klickt auf `Fortsetzen`, bestätigt Eure Sicherheitsphrase und klickt erneut auf `Fortsetzen`. |
        |Sicherheitsschlüssel |Element generiert nun einen Sicherheitsschlüssel und legt eine verschlüsselte Sicherungskopie auf dem Matrix Server ab. |
        |Sicherungskopie |Ihr solltet ebenfalls die Sicherheitsphrase sowie den Sicherheitsschlüssel an einem sicheren Ort aufbewahren, z.B. in einem [Passwortmanager](https://gofoss.net/de/passwords/)! Dort solltet Ihr drei Dinge abspeichern: <br>• Euer Kennwort, das zur Anmeldung bei Element nötig ist <br>• die Sicherheitsphrase, die den Sicherheitsschlüssel schützt <br>• den Sicherheitsschlüssel, der zum Zugriff auf Eure verschlüsselten Nachrichten nötig ist |
        |Wiederherstellung |Sollte Euer Windows-Gerät nicht mehr auf Eure Nachrichten zugreifen können, liegt das wahrscheinlich daran, dass es nicht über den richtigen Sicherheitsschlüssel verfügt. Ruft in diesem Fall den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Verschlüsselung ‣ Sicheres Backup` auf, klickt auf `Von Sicherung wiederherstellen` und gebt Eure Sicherheitsphrase ein. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Beginnt zu chatten |
        |Sprachnachricht senden |• Öffnet die Element-App <br>• Öffnet einen bestehenden Chat, oder beginnt einen neuen <br>• Die `Sprachnachricht`-Schaltfläche befindet sich neben dem Texteingabefeld <br>• Klickt auf die Schaltfläche, um Eure Nachricht aufzuzeichnen <br>• Wenn Ihr bereit seid, klick auf die `Senden`-Schaltfläche <br>• Oder klickt auf die `Papierkorb`-Schaltfläche, um die Sprachnachricht zu löschen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik  <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik  <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Öffentlichen Räumen beitreten |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Wählt `Öffentliche Räume erkunden` <br>• Stöbert durch das Raumverzeichnis <br>• Oder sucht nach Räumen, die Euch interessieren <br>• Ihr könnt auch andere Server auswählen, um dort nach weiteren Räumen zu suchen <br>• Klickt auf `Beitreten`, um einer Gemeinschaft beizutreten |
        |Neue Räume erstellen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Klickt auf `Neuer Raum` <br>• Wählt einen Raumnamen <br>• Gebt an, ob der Raum privater oder öffentlicher Natur ist (in letzterem Fall muss eine Adresse angegeben werden) <br>• Aktiviert oder deaktiviert die Verschlüsselung <br>• Optional könnt Ihr auch NutzerInnen anderer Matrix-Server blockieren <br>• Klickt auf `Raum erstellen` <br>• Klickt auf die `In diesen Raum einladen`-Schaltfläche und ladet andere Personen anhand ihrer Benutzernamen oder E-Mail-Adressen ein <br>• Optional können sehr detaillierte Raumeinstellungen definiert werden, z.B. URL-Vorschau, Raumzugang, Nachrichtenverlauf, Nutzerrechte, usw. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Gruppen-Sprachanruf |
        |Gruppen-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Gruppen-Videoanruf |

        ### Benachrichtigungen

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Globale Benachrichtigungs-Einstellungen |• Öffnet die Element-App <br>• Ruft den Menü-Eintrag `Benutzermenü ‣ Benachrichtigungen` aus und konfiguriert globale Benachrichtigungen nach Euren Wünschen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Benutzerkonto  <br>• Aktiviert oder deaktiviert Desktop-Benachrichtigungen <br>• Aktiviert oder deaktiviert E-Mail-Benachrichtigungen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einzel- oder Gruppenchats <br>• Aktiviert oder deaktiviert Benachrichtigungen für Nachrichten, die Euren Namen oder Schlüsselwörter enthalten <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einladungen usw. |
        |Einstellungen für Direktnachrichten-Benachrichtigungen |• Öffnet die Element-App <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Personen` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |
        |Einstellungen für Raumbenachrichtigungen |• Öffnet die Element-App <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Räume` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |

        ### Authentifizierung und Verifizierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Unter bestimmten Umständen überprüft Element Eure Identität. Zum Beispiel, wenn Ihr Euch von einem neuen Gerät anmeldet oder eine neue Sitzung eröffnet. Oder wenn Ihr Euch über mehrere Geräte gleichzeitig anmeldet. In solchen Fällen zeigt Element eine Mitteilung an: `Überprüfe diese Anmeldung` oder `Neue Anmeldung. Warst du das?`. Ihr verfügt über mehrere Möglichkeiten, um Eure Identität nachzuweisen. |
        |Sicherheitsphrase- oder Schlüssel |Die einfachste Lösung besteht darin, Eure Sicherheitsphrase oder Euren Sicherheitsschlüssel anzugeben. |
        |Andere Geräte |Ihr könnt auch ein anderes Gerät verwenden, das bereits angemeldet ist. Klickt hierzu auf die Verifizierungsanfrage, die auf diesem Gerät angezeigt wird. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |
        |Erfolgreiche Verifizierung |Sobald Eure Identität erfolgreich überprüft wurde erhaltet Ihr vollen Zugriff auf Eure Nachrichten und werden anderen Gesprächspartner über als vertrauenswürdig ausgewiesen. |
        |Aktive Sitzungen |Ihr könnt alle aktiven und verifizierten Sitzungen unter dem Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz` einsehen. |
        |Benutzerverifizierung |Für zusätzliche Sicherheit könnt Ihr ebenfalls die Identität anderer Gesprächspartner überprüfen. Öffnet hierzu einen (verschlüsselten) Chat, ruft den Menü-Eintrag `Rauminfo ‣ Personen` auf, wählt den entsprechenden Gesprächspartner und klickt auf `Verifizieren ‣ Verifizierung starten`. Wartet, bis der Gesprächspartner die Verifizierungsanfrage akzeptiert. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        ### Installation

        Ladet den [Element-Desktop-Client für macOS](https://packages.riot.im/desktop/install/macos/Element.dmg) herunter, öffnet die `.dmg`-Datei und verschiebt das Element-Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das Signal-Symbol in das Andockmenü.

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Jeder Matrix-Benutzer erhält eine eindeutige Kennung, die einer E-Mail-Adresse ziemlich ähnlich sieht: `@benutzername:servername.net`. Ihr könnt jederzeit eine neue Matrix-Kennung bei einem beliebigen Anbieter Eurer Wahl erstellen, oder sogar Eure eigene Matrix-Instanz hosten. |
        |Element öffnen |Öffnet die Element-App und klickt auf `Konto erstellen`. |
        |Server wählen |Wählt den Standard-Server [matrix.org](https://matrix.org) oder einen öffentlichen Server Eurer Wahl. Eine Auswahl findet Ihr hier: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Registrieren |Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Diese Zugangsdaten sind für die Anmeldung bei Element erforderlich. |
        |E-Mail-Verifizierung |Gebt eine bestehende oder eine [Wegwerf-Email-Adresse](https://gofoss.net/de/cloud-providers/#other-cloud-services) an, um Eure Zugangsdaten zu verifizieren und ggf. wiederherzustellen. Klickt auf `Registrieren`, lest Euch aufmerksam die Datenschutzerklärung durch und akzeptiert diese. Öffnet schließlich den Link in der Bestätigungs-Email und schließt die Registrierung ab. |

        ### Sicherheitsschlüssel

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |*Dieser Schritt ist lediglich nach der ersten Anmeldung erforderlich*. <br> Element verwendet durchgängige Verschlüsselung. Um sicherzustellen, dass Ihr jederzeit und von jedem Gerät aus Zugriff auf Eure verschlüsselten Nachrichten habt, müsst Ihr einen sogenannten Sicherheitsschlüssel generieren. |
        |Einstellungen |Nachdem Ihr Euch angemeldet habt, ruft den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Sicheres Backup ‣ Backup einrichten` auf. |
        |Sicherheitsphrase |• Wählt `Eine Sicherheitsphrase benutzen` und gebt eine [sichere, individuelle Sicherheitsphrase](https://gofoss.net/de/passwords/) ein <br>• Diese Sicherheitsphrase schützt Euren Sicherheitsschlüssel <br>• *Verwendet auf keinen Fall Euer Element-Kennwort als Sicherheitsphrase!* <br>• Klickt auf `Fortsetzen`, bestätigt Eure Sicherheitsphrase und klickt erneut auf `Fortsetzen`. |
        |Sicherheitsschlüssel |Element generiert nun einen Sicherheitsschlüssel und legt eine verschlüsselte Sicherungskopie auf dem Matrix Server ab. |
        |Sicherungskopie |Ihr solltet ebenfalls die Sicherheitsphrase sowie den Sicherheitsschlüssel an einem sicheren Ort aufbewahren, z.B. in einem [Passwortmanager](https://gofoss.net/de/passwords/)! Dort solltet Ihr drei Dinge abspeichern: <br>• Euer Kennwort, das zur Anmeldung bei Element nötig ist <br>• die Sicherheitsphrase, die den Sicherheitsschlüssel schützt <br>• den Sicherheitsschlüssel, der zum Zugriff auf Eure verschlüsselten Nachrichten nötig ist |
        |Wiederherstellung |Sollte Euer Windows-Gerät nicht mehr auf Eure Nachrichten zugreifen können, liegt das wahrscheinlich daran, dass es nicht über den richtigen Sicherheitsschlüssel verfügt. Ruft in diesem Fall den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Verschlüsselung ‣ Sicheres Backup` auf, klickt auf `Von Sicherung wiederherstellen` und gebt Eure Sicherheitsphrase ein. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Beginnt zu chatten |
        |Sprachnachricht senden |• Öffnet die Element-App <br>• Öffnet einen bestehenden Chat, oder beginnt einen neuen <br>• Die `Sprachnachricht`-Schaltfläche befindet sich neben dem Texteingabefeld <br>• Klickt auf die Schaltfläche, um Eure Nachricht aufzuzeichnen <br>• Wenn Ihr bereit seid, klick auf die `Senden`-Schaltfläche <br>• Oder klickt auf die `Papierkorb`-Schaltfläche, um die Sprachnachricht zu löschen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik  <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik  <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Öffentlichen Räumen beitreten |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Wählt `Öffentliche Räume erkunden` <br>• Stöbert durch das Raumverzeichnis <br>• Oder sucht nach Räumen, die Euch interessieren <br>• Ihr könnt auch andere Server auswählen, um dort nach weiteren Räumen zu suchen <br>• Klickt auf `Beitreten`, um einer Gemeinschaft beizutreten |
        |Neue Räume erstellen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Klickt auf `Neuer Raum` <br>• Wählt einen Raumnamen <br>• Gebt an, ob der Raum privater oder öffentlicher Natur ist (in letzterem Fall muss eine Adresse angegeben werden) <br>• Aktiviert oder deaktiviert die Verschlüsselung <br>• Optional könnt Ihr auch NutzerInnen anderer Matrix-Server blockieren <br>• Klickt auf `Raum erstellen` <br>• Klickt auf die `In diesen Raum einladen`-Schaltfläche und ladet andere Personen anhand ihrer Benutzernamen oder E-Mail-Adressen ein <br>• Optional können sehr detaillierte Raumeinstellungen definiert werden, z.B. URL-Vorschau, Raumzugang, Nachrichtenverlauf, Nutzerrechte, usw. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Gruppen-Sprachanruf |
        |Gruppen-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Gruppen-Videoanruf |

        ### Benachrichtigungen

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Globale Benachrichtigungs-Einstellungen |• Öffnet die Element-App <br>• Ruft den Menü-Eintrag `Benutzermenü ‣ Benachrichtigungen` aus und konfiguriert globale Benachrichtigungen nach Euren Wünschen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Benutzerkonto  <br>• Aktiviert oder deaktiviert Desktop-Benachrichtigungen <br>• Aktiviert oder deaktiviert E-Mail-Benachrichtigungen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einzel- oder Gruppenchats <br>• Aktiviert oder deaktiviert Benachrichtigungen für Nachrichten, die Euren Namen oder Schlüsselwörter enthalten <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einladungen usw. |
        |Einstellungen für Direktnachrichten-Benachrichtigungen |• Öffnet die Element-App <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Personen` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |
        |Einstellungen für Raumbenachrichtigungen |• Öffnet die Element-App <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Räume` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |

        ### Authentifizierung und Verifizierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Unter bestimmten Umständen überprüft Element Eure Identität. Zum Beispiel, wenn Ihr Euch von einem neuen Gerät anmeldet oder eine neue Sitzung eröffnet. Oder wenn Ihr Euch über mehrere Geräte gleichzeitig anmeldet. In solchen Fällen zeigt Element eine Mitteilung an: `Überprüfe diese Anmeldung` oder `Neue Anmeldung. Warst du das?`. Ihr verfügt über mehrere Möglichkeiten, um Eure Identität nachzuweisen. |
        |Sicherheitsphrase- oder Schlüssel |Die einfachste Lösung besteht darin, Eure Sicherheitsphrase oder Euren Sicherheitsschlüssel anzugeben. |
        |Andere Geräte |Ihr könnt auch ein anderes Gerät verwenden, das bereits angemeldet ist. Klickt hierzu auf die Verifizierungsanfrage, die auf diesem Gerät angezeigt wird. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |
        |Erfolgreiche Verifizierung |Sobald Eure Identität erfolgreich überprüft wurde erhaltet Ihr vollen Zugriff auf Eure Nachrichten und werden anderen Gesprächspartner über als vertrauenswürdig ausgewiesen. |
        |Aktive Sitzungen |Ihr könnt alle aktiven und verifizierten Sitzungen unter dem Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz` einsehen. |
        |Benutzerverifizierung |Für zusätzliche Sicherheit könnt Ihr ebenfalls die Identität anderer Gesprächspartner überprüfen. Öffnet hierzu einen (verschlüsselten) Chat, ruft den Menü-Eintrag `Rauminfo ‣ Personen` auf, wählt den entsprechenden Gesprächspartner und klickt auf `Verifizieren ‣ Verifizierung starten`. Wartet, bis der Gesprächspartner die Verifizierungsanfrage akzeptiert. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        ### Installation

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Terminal öffnen |Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, nutzt die Tastenkombination `Strg+Alt+T` oder klickt auf die `Aktivitäten`-Schaltfläche oben links und sucht nach `Terminal`. |
        |Sicherer Zugriff auf Paketquellen |`sudo apt install -y wget apt-transport-https` |
        |Signierschlüssel hinzufügen |`sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg` |
        |Paketquelle zur Liste hinzufügen |`echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list` |
        |Lokalen apt-Cache aktualisieren |`sudo apt update` |
        |Element-Client installieren |`sudo apt install element-desktop`|

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Jeder Matrix-Benutzer erhält eine eindeutige Kennung, die einer E-Mail-Adresse ziemlich ähnlich sieht: `@benutzername:servername.net`. Ihr könnt jederzeit eine neue Matrix-Kennung bei einem beliebigen Anbieter Eurer Wahl erstellen, oder sogar Eure eigene Matrix-Instanz hosten. |
        |Element öffnen |Öffnet die Element-App und klickt auf `Konto erstellen`. |
        |Server wählen |Wählt den Standard-Server [matrix.org](https://matrix.org) oder einen öffentlichen Server Eurer Wahl. Eine Auswahl findet Ihr hier: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Registrieren |Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Diese Zugangsdaten sind für die Anmeldung bei Element erforderlich. |
        |E-Mail-Verifizierung |Gebt eine bestehende oder eine [Wegwerf-Email-Adresse](https://gofoss.net/de/cloud-providers/#other-cloud-services) an, um Eure Zugangsdaten zu verifizieren und ggf. wiederherzustellen. Klickt auf `Registrieren`, lest Euch aufmerksam die Datenschutzerklärung durch und akzeptiert diese. Öffnet schließlich den Link in der Bestätigungs-Email und schließt die Registrierung ab. |

        ### Sicherheitsschlüssel

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |*Dieser Schritt ist lediglich nach der ersten Anmeldung erforderlich*. <br> Element verwendet durchgängige Verschlüsselung. Um sicherzustellen, dass Ihr jederzeit und von jedem Gerät aus Zugriff auf Eure verschlüsselten Nachrichten habt, müsst Ihr einen sogenannten Sicherheitsschlüssel generieren. |
        |Einstellungen |Nachdem Ihr Euch angemeldet habt, ruft den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Sicheres Backup ‣ Backup einrichten` auf. |
        |Sicherheitsphrase |• Wählt `Eine Sicherheitsphrase benutzen` und gebt eine [sichere, individuelle Sicherheitsphrase](https://gofoss.net/de/passwords/) ein <br>• Diese Sicherheitsphrase schützt Euren Sicherheitsschlüssel <br>• *Verwendet auf keinen Fall Euer Element-Kennwort als Sicherheitsphrase!* <br>• Klickt auf `Fortsetzen`, bestätigt Eure Sicherheitsphrase und klickt erneut auf `Fortsetzen`. |
        |Sicherheitsschlüssel |Element generiert nun einen Sicherheitsschlüssel und legt eine verschlüsselte Sicherungskopie auf dem Matrix Server ab. |
        |Sicherungskopie |Ihr solltet ebenfalls die Sicherheitsphrase sowie den Sicherheitsschlüssel an einem sicheren Ort aufbewahren, z.B. in einem [Passwortmanager](https://gofoss.net/de/passwords/)! Dort solltet Ihr drei Dinge abspeichern: <br>• Euer Kennwort, das zur Anmeldung bei Element nötig ist <br>• die Sicherheitsphrase, die den Sicherheitsschlüssel schützt <br>• den Sicherheitsschlüssel, der zum Zugriff auf Eure verschlüsselten Nachrichten nötig ist |
        |Wiederherstellung |Sollte Euer Windows-Gerät nicht mehr auf Eure Nachrichten zugreifen können, liegt das wahrscheinlich daran, dass es nicht über den richtigen Sicherheitsschlüssel verfügt. Ruft in diesem Fall den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Verschlüsselung ‣ Sicheres Backup` auf, klickt auf `Von Sicherung wiederherstellen` und gebt Eure Sicherheitsphrase ein. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Beginnt zu chatten |
        |Sprachnachricht senden |• Öffnet die Element-App <br>• Öffnet einen bestehenden Chat, oder beginnt einen neuen <br>• Die `Sprachnachricht`-Schaltfläche befindet sich neben dem Texteingabefeld <br>• Klickt auf die Schaltfläche, um Eure Nachricht aufzuzeichnen <br>• Wenn Ihr bereit seid, klick auf die `Senden`-Schaltfläche <br>• Oder klickt auf die `Papierkorb`-Schaltfläche, um die Sprachnachricht zu löschen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik  <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik  <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Öffentlichen Räumen beitreten |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Wählt `Öffentliche Räume erkunden` <br>• Stöbert durch das Raumverzeichnis <br>• Oder sucht nach Räumen, die Euch interessieren <br>• Ihr könnt auch andere Server auswählen, um dort nach weiteren Räumen zu suchen <br>• Klickt auf `Beitreten`, um einer Gemeinschaft beizutreten |
        |Neue Räume erstellen |• Öffnet die Element-App <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Klickt auf `Neuer Raum` <br>• Wählt einen Raumnamen <br>• Gebt an, ob der Raum privater oder öffentlicher Natur ist (in letzterem Fall muss eine Adresse angegeben werden) <br>• Aktiviert oder deaktiviert die Verschlüsselung <br>• Optional könnt Ihr auch NutzerInnen anderer Matrix-Server blockieren <br>• Klickt auf `Raum erstellen` <br>• Klickt auf die `In diesen Raum einladen`-Schaltfläche und ladet andere Personen anhand ihrer Benutzernamen oder E-Mail-Adressen ein <br>• Optional können sehr detaillierte Raumeinstellungen definiert werden, z.B. URL-Vorschau, Raumzugang, Nachrichtenverlauf, Nutzerrechte, usw. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Element-App <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Gruppen-Sprachanruf |
        |Gruppen-Videoanruf tätigen |• Öffnet die Element-App <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Gruppen-Videoanruf |

        ### Benachrichtigungen

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Globale Benachrichtigungs-Einstellungen |• Öffnet die Element-App <br>• Ruft den Menü-Eintrag `Benutzermenü ‣ Benachrichtigungen` aus und konfiguriert globale Benachrichtigungen nach Euren Wünschen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Benutzerkonto  <br>• Aktiviert oder deaktiviert Desktop-Benachrichtigungen <br>• Aktiviert oder deaktiviert E-Mail-Benachrichtigungen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einzel- oder Gruppenchats <br>• Aktiviert oder deaktiviert Benachrichtigungen für Nachrichten, die Euren Namen oder Schlüsselwörter enthalten <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einladungen usw. |
        |Einstellungen für Direktnachrichten-Benachrichtigungen |• Öffnet die Element-App <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Personen` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |
        |Einstellungen für Raumbenachrichtigungen |• Öffnet die Element-App <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Räume` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |

        ### Authentifizierung und Verifizierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Unter bestimmten Umständen überprüft Element Eure Identität. Zum Beispiel, wenn Ihr Euch von einem neuen Gerät anmeldet oder eine neue Sitzung eröffnet. Oder wenn Ihr Euch über mehrere Geräte gleichzeitig anmeldet. In solchen Fällen zeigt Element eine Mitteilung an: `Überprüfe diese Anmeldung` oder `Neue Anmeldung. Warst du das?`. Ihr verfügt über mehrere Möglichkeiten, um Eure Identität nachzuweisen. |
        |Sicherheitsphrase- oder Schlüssel |Die einfachste Lösung besteht darin, Eure Sicherheitsphrase oder Euren Sicherheitsschlüssel anzugeben. |
        |Andere Geräte |Ihr könnt auch ein anderes Gerät verwenden, das bereits angemeldet ist. Klickt hierzu auf die Verifizierungsanfrage, die auf diesem Gerät angezeigt wird. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |
        |Erfolgreiche Verifizierung |Sobald Eure Identität erfolgreich überprüft wurde erhaltet Ihr vollen Zugriff auf Eure Nachrichten und werden anderen Gesprächspartner über als vertrauenswürdig ausgewiesen. |
        |Aktive Sitzungen |Ihr könnt alle aktiven und verifizierten Sitzungen unter dem Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz` einsehen. |
        |Benutzerverifizierung |Für zusätzliche Sicherheit könnt Ihr ebenfalls die Identität anderer Gesprächspartner überprüfen. Öffnet hierzu einen (verschlüsselten) Chat, ruft den Menü-Eintrag `Rauminfo ‣ Personen` auf, wählt den entsprechenden Gesprächspartner und klickt auf `Verifizieren ‣ Verifizierung starten`. Wartet, bis der Gesprächspartner die Verifizierungsanfrage akzeptiert. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |



=== "Browser"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Jeder Matrix-Benutzer erhält eine eindeutige Kennung, die einer E-Mail-Adresse ziemlich ähnlich sieht: `@benutzername:servername.net`. Ihr könnt jederzeit eine neue Matrix-Kennung bei einem beliebigen Anbieter Eurer Wahl erstellen, oder sogar Eure eigene Matrix-Instanz hosten. |
        |Element öffnen |Öffnet Euren Browser und begebt Euch auf [https://app.element.io](https://app.element.io/). Erlaubt auf Aufforderung den Zugriff auf den dauerhaften Speicher. Dies ermöglicht es Element, Schlüssel, Nachrichten usw. in der Browsersitzung zu speichern. Klickt dann auf `Konto erstellen`. |
        |Server wählen |Wählt den Standard-Server [matrix.org](https://matrix.org) oder einen öffentlichen Server Eurer Wahl. Eine Auswahl findet Ihr hier: <br>• [hello-matrix.net](https://www.hello-matrix.net/public_servers.php) <br>• [anchel.nl](https://www.anchel.nl/matrix-publiclist/) <br>• [Tatsumoto](https://tatsumoto-ren.github.io/blog/list-of-matrix-servers.html) |
        |Registrieren |Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Diese Zugangsdaten sind für die Anmeldung bei Element erforderlich. |
        |E-Mail-Verifizierung |Gebt eine bestehende oder eine [Wegwerf-Email-Adresse](https://gofoss.net/de/cloud-providers/#other-cloud-services) an, um Eure Zugangsdaten zu verifizieren und ggf. wiederherzustellen. Klickt auf `Registrieren`, lest Euch aufmerksam die Datenschutzerklärung durch und akzeptiert diese. Öffnet schließlich den Link in der Bestätigungs-Email und schließt die Registrierung ab. |

        ### Sicherheitsschlüssel

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |*Dieser Schritt ist lediglich nach der ersten Anmeldung erforderlich*. <br> Element verwendet durchgängige Verschlüsselung. Um sicherzustellen, dass Ihr jederzeit und von jedem Gerät aus Zugriff auf Eure verschlüsselten Nachrichten habt, müsst Ihr einen sogenannten Sicherheitsschlüssel generieren. |
        |Einstellungen |Nachdem Ihr Euch angemeldet habt, ruft den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Sicheres Backup ‣ Backup einrichten` auf. |
        |Sicherheitsphrase |• Wählt `Eine Sicherheitsphrase benutzen` und gebt eine [sichere, individuelle Sicherheitsphrase](https://gofoss.net/de/passwords/) ein <br>• Diese Sicherheitsphrase schützt Euren Sicherheitsschlüssel <br>• *Verwendet auf keinen Fall Euer Element-Kennwort als Sicherheitsphrase!* <br>• Klickt auf `Fortsetzen`, bestätigt Eure Sicherheitsphrase und klickt erneut auf `Fortsetzen`. |
        |Sicherheitsschlüssel |Element generiert nun einen Sicherheitsschlüssel und legt eine verschlüsselte Sicherungskopie auf dem Matrix Server ab. |
        |Sicherungskopie |Ihr solltet ebenfalls die Sicherheitsphrase sowie den Sicherheitsschlüssel an einem sicheren Ort aufbewahren, z.B. in einem [Passwortmanager](https://gofoss.net/de/passwords/)! Dort solltet Ihr drei Dinge abspeichern: <br>• Euer Kennwort, das zur Anmeldung bei Element nötig ist <br>• die Sicherheitsphrase, die den Sicherheitsschlüssel schützt <br>• den Sicherheitsschlüssel, der zum Zugriff auf Eure verschlüsselten Nachrichten nötig ist |
        |Wiederherstellung |Sollte Euer Windows-Gerät nicht mehr auf Eure Nachrichten zugreifen können, liegt das wahrscheinlich daran, dass es nicht über den richtigen Sicherheitsschlüssel verfügt. Ruft in diesem Fall den Menü-Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz ‣ Verschlüsselung ‣ Sicheres Backup` auf, klickt auf `Von Sicherung wiederherstellen` und gebt Eure Sicherheitsphrase ein. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Beginnt zu chatten |
        |Sprachnachricht senden |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Öffnet einen bestehenden Chat, oder beginnt einen neuen <br>• Die `Sprachnachricht`-Schaltfläche befindet sich neben dem Texteingabefeld <br>• Klickt auf die Schaltfläche, um Eure Nachricht aufzuzeichnen <br>• Wenn Ihr bereit seid, klick auf die `Senden`-Schaltfläche <br>• Oder klickt auf die `Papierkorb`-Schaltfläche, um die Sprachnachricht zu löschen |
        |Einzel-Sprachanruf tätigen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Klickt auf die `+`-Schaltfläche neben der `Personen`-Rubrik <br>• Sucht nach Kontakten anhand ihres Benutzernamens oder ihrer E-Mail-Adresse <br>• Klickt auf `Los` <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Öffentlichen Räumen beitreten |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Wählt `Öffentliche Räume erkunden` <br>• Stöbert durch das Raumverzeichnis <br>• Oder sucht nach Räumen, die Euch interessieren <br>• Ihr könnt auch andere Server auswählen, um dort nach weiteren Räumen zu suchen <br>• Klickt auf `Beitreten`, um einer Gemeinschaft beizutreten |
        |Neue Räume erstellen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Klickt auf die `+`-Schaltfläche neben der `Räume`-Rubrik <br>• Klickt auf `Neuer Raum` <br>• Wählt einen Raumnamen <br>• Gebt an, ob der Raum privater oder öffentlicher Natur ist (in letzterem Fall muss eine Adresse angegeben werden) <br>• Aktiviert oder deaktiviert die Verschlüsselung <br>• Optional könnt Ihr auch NutzerInnen anderer Matrix-Server blockieren <br>• Klickt auf `Raum erstellen` <br>• Klickt auf die `In diesen Raum einladen`-Schaltfläche und ladet andere Personen anhand ihrer Benutzernamen oder E-Mail-Adressen ein <br>• Optional können sehr detaillierte Raumeinstellungen definiert werden, z.B. URL-Vorschau, Raumzugang, Nachrichtenverlauf, Nutzerrechte, usw. |
        |Gruppen-Sprachanruf tätigen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Gruppen-Sprachanruf |
        |Gruppen-Videoanruf tätigen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Wählt die Rubrik `Räume` <br>• Wählt einen bestehenden Raum, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Gruppen-Videoanruf |

        ### Benachrichtigungen

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Globale Benachrichtigungs-Einstellungen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Ruft den Menü-Eintrag `Benutzermenü ‣ Benachrichtigungen` aus und konfiguriert globale Benachrichtigungen nach Euren Wünschen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Benutzerkonto  <br>• Aktiviert oder deaktiviert Benachrichtigungen für die bestehende Sitzung <br>• Aktiviert oder deaktiviert E-Mail-Benachrichtigungen <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einzel- oder Gruppenchats <br>• Aktiviert oder deaktiviert Benachrichtigungen für Nachrichten, die Euren Namen oder Schlüsselwörter enthalten <br>• Aktiviert oder deaktiviert Benachrichtigungen für Einladungen usw. |
        |Einstellungen für Direktnachrichten-Benachrichtigungen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Personen` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |
        |Einstellungen für Raumbenachrichtigungen |• Öffnet den Browser und meldet Euch bei [https://app.element.io](https://app.element.io/) an <br>• Bewegt den Mauszeiger über eine Chatsitzung in der Rubrik `Räume` und klickt auf das `Glocken`-Symbol, um die Benachrichtigungs-Einstellungen aufzurufen <br>• Aktiviert oder deaktiviert Benachrichtigungen für alle Nachrichten, oder lediglich für solche, die Euren Namen oder Schlüsselwörter enthalten |

        ### Authentifizierung und Verifizierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Vorbemerkungen |Unter bestimmten Umständen überprüft Element Eure Identität. Zum Beispiel, wenn Ihr Euch von einem neuen Gerät anmeldet oder eine neue Sitzung eröffnet. Oder wenn Ihr Euch über mehrere Geräte gleichzeitig anmeldet. In solchen Fällen zeigt Element eine Mitteilung an: `Überprüfe diese Anmeldung` oder `Neue Anmeldung. Warst du das?`. Ihr verfügt über mehrere Möglichkeiten, um Eure Identität nachzuweisen. |
        |Sicherheitsphrase- oder Schlüssel |Die einfachste Lösung besteht darin, Eure Sicherheitsphrase oder Euren Sicherheitsschlüssel anzugeben. |
        |Andere Geräte |Ihr könnt auch ein anderes Gerät verwenden, das bereits angemeldet ist. Klickt hierzu auf die Verifizierungsanfrage, die auf diesem Gerät angezeigt wird. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |
        |Erfolgreiche Verifizierung |Sobald Eure Identität erfolgreich überprüft wurde erhaltet Ihr vollen Zugriff auf Eure Nachrichten und werden anderen Gesprächspartner über als vertrauenswürdig ausgewiesen. |
        |Aktive Sitzungen |Ihr könnt alle aktiven und verifizierten Sitzungen unter dem Eintrag `Benutzermenü ‣ Sicherheit und Datenschutz` einsehen. |
        |Benutzerverifizierung |Für zusätzliche Sicherheit könnt Ihr ebenfalls die Identität anderer Gesprächspartner überprüfen. Öffnet hierzu einen (verschlüsselten) Chat, ruft den Menü-Eintrag `Rauminfo ‣ Personen` auf, wählt den entsprechenden Gesprächspartner und klickt auf `Verifizieren ‣ Verifizierung starten`. Wartet, bis der Gesprächspartner die Verifizierungsanfrage akzeptiert. Scannt dann entweder den QR-Code, oder vergleicht einen Emoji-Code. |


<div style="margin-top:-20px">
</div>

??? warning "Die Kommunikation über Element ist zwar verschlüsselt, erfordert jedoch Vertrauen"

    Im Gegensatz zu Signal verfolgt Element nicht das "Null-Wissen"-Prinzip und [erhebt Metadaten](https://gitlab.com/libremonde-org/papers/research/privacy-matrix.org/-/blob/master/part1/README.md/). Dies ist auch in [Elements Datenschutzerklärung](https://element.io/privacy) nachzulesen: *[...] We might profile metadata pertaining to the configuration and management of hosted homeservers so that we can improve our products and services*.

    Tatsächlich werden die Matrix-Server von Administratoren verwaltet. Wenn Ihr einen öffentlichen Matrix-Server nutzt, vertraut Ihr den Administratoren daher einen Teil Eurer Daten an. Selbst wenn die Kommunikation über Element verschlüsselt bleibt, sind Administratoren potenziell dazu in der Lage, eine Kopie der (verschlüsselten) Nachrichten auf unbestimmte Zeit aufzubewahren, IP-Adressen zu protokollieren oder auf (unverschlüsselte) Daten wie Benutzernamen, E-Mail-Adressen, Mediendateien, Geräteinformationen, Kontaktlisten, Nutzungsprofile, Gruppenmitgliedschaften usw. zuzugreifen.

    Selbst wenn Ihr Euren eigenen Matrix-Server hostet können Administratoren anderer an der Kommunikation teilnehmenden Server auf die Metadaten zugreifen. Der [Matrix-Projektleiter](https://teddit.net/r/privacy/comments/da219t/im_project_lead_for_matrixorg_the_open_protocol/f20r6vp/) formuliert das so: *"[...] if you invite a user to your chatroom who's on a server that you don't trust, then the history will go to that server. If the room is end-to-end encrypted then that server won't be able to see the messages, but it will be able to see the metadata of who was talking to who and when (but not what). [...]"*.


<br>

<center> <img src="../../assets/img/separator_jami.svg" alt="Jami" width="150px"></img> </center>

## Jami

[Jami](https://jami.net/) ist eine [quelloffene](https://git.jami.net/savoirfairelinux/) Nachrichten-, Sprach- und Videoanruf-App, die auf allen Desktop- sowie Mobilplattformen verfügbar ist. Jami basiert auf einer Peer-to-Peer-Architektur, bei der sich NutzerInnen direkt miteinander verbinden, ohne sich auf Server zu verlassen. Die gesamte Kommunikation ist durchgängig verschlüsselt, mit *Perfect Forward Secrecy*. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Android"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

        ### Installation

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Installation |Ladet die Jami-App herunter und installiert sie: <br>• [Google Play Store](https://play.google.com/store/apps/details?id=cx.ring&hl=de_DE)<br>• [Aurora Store](https://auroraoss.com/de/)<br>• [F-Droid](https://f-droid.org/de/packages/cx.ring/) |
        |Tracker frei |Die Jami-App [enthält 0 Tracker und benötigt 21 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/cx.ring/latest/). Zum Vergleich:<br>• TikTok: 16 Tracker, 76 Berechtigungen<br>• Snapchat: 2 Tracker, 44 Berechtigungen<br>• WhatsApp: 1 Tracker, 57 Berechtigungen |
        |Push-Benachrichtigungen |Zwar ist Jamis F-Droid-Version Google-frei, aber [nur Jamis Google Play Store Version unterstützt Push-Benachrichtigungen](https://git.jami.net/savoirfairelinux/jami-client-android/-/issues/781), wobei Googles Cloud Messaging (GCM) verwendet wird. |

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Benutzerkonto anlegen |Öffnet die Jami-App und klickt auf `Jami-Konto erstellen`. Gebt einen Benutzernamen ein. |
        |Passwort festlegen |Optional könnt Ihr ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) festlegen, um Euer Konto zu verschlüsseln. Bewahrt dieses Passwort an einem sicheren Ort auf, es kann nicht neu erstellt werden. |
        |Profil anlegen |Optional könnt Ihr einen für andere sichtbaren Benutzernamen sowie ein Profilbild festlegen. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Jami-App <br>• Tippt auf `Ein Gespräch starten` <br>• Sucht nach Kontakten anhand ihres Benutzernamens <br>• Tippt auf `Kontakt hinzufügen`. Der Gesprächspartner erhält nun eine Einladung <br>• Sobald die Einladung angenommen wurde, könnt Ihr mit dem Chat beginnen |
        |Sprachnachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Sprachdatei aufnehmen` befindet sich im Menü neben dem Texteingabefeld <br>• Tippt darauf, um mit der Aufnahme einer Sprachnachricht zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Teilen`-Schaltfläche tippen und die Sprachnachricht mit einem Jami-Kontakt teilen |
        |Videonachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Videodatei aufnehmen` befindet sich im Menü neben dem Texteingabefeld < <br>• Tippt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Teilen`-Schaltfläche tippen und die Videonachricht mit einem Jami-Kontakt teilen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Bildschirm teilen` |
        |Dateien versenden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Datei senden` befindet sich im Menü neben dem Texteingabefeld <br>• Wählt eine Datei zum Senden |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chats beginnen |Zum Zeitpunkt der Abfassung dieses Textes [arbeitet Jami noch an der Implementierung von Gruppen-Chats](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Diese sogenannten "Swarms" sind vollkommen dezentrale, Peer-to-Peer Textnachrichten mit einer theoretisch unbegrenzten Anzahl an Teilnehmern. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf <br>• Sobald der Anruf beginnt, tippt auf die Schaltfläche `Teilnehmer hinzufügen` |
        |Gruppen-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Sobald der Anruf beginnt, tippt auf die Schaltfläche `Teilnehmer hinzufügen` <br>• Bildschirmfreigabe: Klickt während eines Gruppen-Videoanrufs auf die Schaltfläche `Bildschirm teilen` |

        ### Sicherungskopien

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Sicherungskopien anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden solltet Ihr regelmäßig [Sicherungskopien Eures Jami-Kontos anlegen](https://gofoss.net/de/backups/). Ruft hierzu das Menü `Einstellungen ‣ Konto ‣ Konto ‣ Konto sichern` auf. Falls Ihr während des Registrierungsvorgangs ein Passwort festgelegt habt, solltet Ihr dieses nun eingeben. Eure Sicherungskopie wird als `.gz`-Archivdatei gespeichert. |
        |Sicherungskopien wiederherstellen |Falls Ihr Jami neu installiert, könnt Ihr auf `Von Sicherung verbinden` tippen. Ruft dann die Archivdatei auf, die Ihr als Sicherungskopie angelegt habt. Falls erforderlich, gebt Euer Passwort ein und klickt auf `Von Sicherung verbinden`. |



=== "iOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für iOS"

        ### Installation

        Ladet die Jami-App vom [App Store](https://apps.apple.com/de/app/ring-a-gnu-package/id1306951055) herunter und installiert sie.

        ### Registrierung

        | Anweisungen | Beschreibung |Eure
        | ------ | ------ |
        |Benutzerkonto anlegen |Öffnet die Jami-App und klickt auf `Jami-Konto erstellen`. Gebt einen Benutzernamen ein. |
        |Passwort festlegen |Optional könnt Ihr ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) festlegen, um Euer Konto zu verschlüsseln. Bewahrt dieses Passwort an einem sicheren Ort auf, es kann nicht neu erstellt werden. |
        |Profil anlegen |Optional könnt Ihr einen für andere sichtbaren Benutzernamen sowie ein Profilbild festlegen. |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Jami-App <br>• Tippt auf `Ein Gespräch starten` <br>• Sucht nach Kontakten anhand ihres Benutzernamens <br>• Tippt auf `Kontakt hinzufügen`. Der Gesprächspartner erhält nun eine Einladung <br>• Sobald die Einladung angenommen wurde, könnt Ihr mit dem Chat beginnen |
        |Sprachnachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Sprachdatei aufnehmen` befindet sich im Menü neben dem Texteingabefeld <br>• Tippt darauf, um mit der Aufnahme einer Sprachnachricht zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Teilen`-Schaltfläche tippen und die Sprachnachricht mit einem Jami-Kontakt teilen |
        |Videonachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Videodatei aufnehmen` befindet sich im Menü neben dem Texteingabefeld < <br>• Tippt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Teilen`-Schaltfläche tippen und die Videonachricht mit einem Jami-Kontakt teilen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Bildschirm teilen` |
        |Dateien versenden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Datei senden` befindet sich im Menü neben dem Texteingabefeld <br>• Wählt eine Datei zum Senden |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chats beginnen |Zum Zeitpunkt der Abfassung dieses Textes [arbeitet Jami noch an der Implementierung von Gruppen-Chats](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Diese sogenannten "Swarms" sind vollkommen dezentrale, Peer-to-Peer Textnachrichten mit einer theoretisch unbegrenzten Anzahl an Teilnehmern. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf <br>• Sobald der Anruf beginnt, tippt auf die Schaltfläche `Teilnehmer hinzufügen` |
        |Gruppen-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Tippt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Sobald der Anruf beginnt, tippt auf die Schaltfläche `Teilnehmer hinzufügen` <br>• Bildschirmfreigabe: Klickt während eines Gruppen-Videoanrufs auf die Schaltfläche `Bildschirm teilen` |

        ### Sicherungskopien

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Sicherungskopien anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden solltet Ihr regelmäßig [Sicherungskopien Eures Jami-Kontos anlegen](https://gofoss.net/de/backups/). Ruft hierzu das Menü `Einstellungen ‣ Konto ‣ Konto ‣ Konto sichern` auf. Falls Ihr während des Registrierungsvorgangs ein Passwort festgelegt habt, solltet Ihr dieses nun eingeben. Eure Sicherungskopie wird als `.gz`-Archivdatei gespeichert. |
        |Sicherungskopien wiederherstellen |Falls Ihr Jami neu installiert, könnt Ihr auf `Von Sicherung verbinden` tippen. Ruft dann die Archivdatei auf, die Ihr als Sicherungskopie angelegt habt. Falls erforderlich, gebt Euer Passwort ein und klickt auf `Von Sicherung verbinden`. |


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        ### Installation

        Ladet den [Jami-Desktop-Client für Windows](https://jami.net/download-jami-windows/) herunter und folgt dem Installationsassistenten.

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Benutzerkonto anlegen |Öffnet die Jami-App und klickt auf `Jami-Konto erstellen`. Gebt einen Benutzernamen ein. |
        |Passwort festlegen |Optional könnt Ihr ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) festlegen, um Euer Konto zu verschlüsseln. Bewahrt dieses Passwort an einem sicheren Ort auf, es kann nicht neu erstellt werden. |
        |Profil anlegen |Optional könnt Ihr einen für andere sichtbaren Benutzernamen sowie ein Profilbild festlegen. |
        |Sicherheitskopie anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden könnt Ihr während des Registrierungsvorgangs eine [Sicherungskopie Eurer Benutzerinformationen anlegen](https://gofoss.net/de/backups/). |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Jami-App <br>• Sucht nach Kontakten anhand ihres Benutzernamens <br>• Klickt auf `Zur Unterhaltung hinzufügen`. Der Gesprächspartner erhält nun eine Einladung <br>• Sobald die Einladung angenommen wurde, könnt Ihr mit dem Chat beginnen |
        |Sprachnachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Audio-Nachricht hinterlassen` befindet sich im Menü neben dem Texteingabefeld <br>• Klickt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Senden`-Schaltfläche klicken <br>• Oder den Aufnahmedialog verlassen, um den Vorgang abzubrechen |
        |Videonachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Video-Nachricht hinterlassen` befindet sich im Menü neben dem Texteingabefeld < <br>• Klickt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Senden`-Schaltfläche klicken <br>• Oder den Aufnahmedialog verlassen, um den Vorgang abzubrechen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Bildschirm teilen` |
        |Dateien versenden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Datei senden` befindet sich im Menü neben dem Texteingabefeld <br>• Wählt eine Datei zum Senden <br>• Alternativ könnt Ihr einfach eine Datei per Drag & Drop in das Jami-Fenster verschieben |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chats beginnen |Zum Zeitpunkt der Abfassung dieses Textes [arbeitet Jami noch an der Implementierung von Gruppen-Chats](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Diese sogenannten "Swarms" sind vollkommen dezentrale, Peer-to-Peer Textnachrichten mit einer theoretisch unbegrenzten Anzahl an Teilnehmern. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf <br>• Sobald der Anruf beginnt, klickt auf `Menu ‣ Teilnehmer hinzufügen` |
        |Gruppen-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Sobald der Anruf beginnt, klickt auf `Menu ‣ Teilnehmer hinzufügen` <br>• Bildschirmfreigabe: Klickt während eines Gruppen-Videoanrufs auf die Schaltfläche `Bildschirm teilen` |

        ### Sicherungskopien

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Sicherungskopien anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden solltet Ihr regelmäßig [Sicherungskopien Eures Jami-Kontos anlegen](https://gofoss.net/de/backups/). Ruft hierzu das Menü `Einstellungen ‣ Konto ‣ Konto sichern` auf. Falls Ihr während des Registrierungsvorgangs ein Passwort festgelegt habt, solltet Ihr dieses nun eingeben. Eure Sicherungskopie wird als `.gz`-Archivdatei gespeichert. |
        |Sicherungskopien wiederherstellen |Falls Ihr Jami neu installiert, könnt Ihr auf `Ein Konto aus einer Sicherung wiederherstellen` klicken. Ruft dann die Archivdatei auf, die Ihr als Sicherungskopie angelegt habt. Falls erforderlich, gebt Euer Passwort ein und klickt auf `Ein Konto aus einer Sicherung wiederherstellen`. |


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        ### Installation

        Ladet den [Jami-Desktop-Client für macOS](https://jami.net/download-jami-macos/) herunter, öffnet die `.dmg`-Datei und verschiebt das Jami-Symbol in den Anwendungsordner. Für einen noch bequemeren Zugriff, öffnet den Anwendungsordner und verschiebt das Jami-Symbol in das Andockmenü.

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Benutzerkonto anlegen |Öffnet die Jami-App und klickt auf `Jami-Konto erstellen`. Gebt einen Benutzernamen ein. |
        |Passwort festlegen |Optional könnt Ihr ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) festlegen, um Euer Konto zu verschlüsseln. Bewahrt dieses Passwort an einem sicheren Ort auf, es kann nicht neu erstellt werden. |
        |Profil anlegen |Optional könnt Ihr einen für andere sichtbaren Benutzernamen sowie ein Profilbild festlegen. |
        |Sicherheitskopie anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden könnt Ihr während des Registrierungsvorgangs eine [Sicherungskopie Eurer Benutzerinformationen anlegen](https://gofoss.net/de/backups/). |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Jami-App <br>• Sucht nach Kontakten anhand ihres Benutzernamens <br>• Klickt auf `Zur Unterhaltung hinzufügen`. Der Gesprächspartner erhält nun eine Einladung <br>• Sobald die Einladung angenommen wurde, könnt Ihr mit dem Chat beginnen |
        |Sprachnachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Audio-Nachricht hinterlassen` befindet sich im Menü neben dem Texteingabefeld <br>• Klickt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Senden`-Schaltfläche klicken <br>• Oder den Aufnahmedialog verlassen, um den Vorgang abzubrechen |
        |Videonachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Video-Nachricht hinterlassen` befindet sich im Menü neben dem Texteingabefeld < <br>• Klickt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Senden`-Schaltfläche klicken <br>• Oder den Aufnahmedialog verlassen, um den Vorgang abzubrechen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Bildschirm teilen` |
        |Dateien versenden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Datei senden` befindet sich im Menü neben dem Texteingabefeld <br>• Wählt eine Datei zum Senden <br>• Alternativ könnt Ihr einfach eine Datei per Drag & Drop in das Jami-Fenster verschieben |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chats beginnen |Zum Zeitpunkt der Abfassung dieses Textes [arbeitet Jami noch an der Implementierung von Gruppen-Chats](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Diese sogenannten "Swarms" sind vollkommen dezentrale, Peer-to-Peer Textnachrichten mit einer theoretisch unbegrenzten Anzahl an Teilnehmern. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf <br>• Sobald der Anruf beginnt, klickt auf `Menu ‣ Teilnehmer hinzufügen` |
        |Gruppen-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Sobald der Anruf beginnt, klickt auf `Menu ‣ Teilnehmer hinzufügen` <br>• Bildschirmfreigabe: Klickt während eines Gruppen-Videoanrufs auf die Schaltfläche `Bildschirm teilen` |

        ### Sicherungskopien

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Sicherungskopien anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden solltet Ihr regelmäßig [Sicherungskopien Eures Jami-Kontos anlegen](https://gofoss.net/de/backups/). Ruft hierzu das Menü `Einstellungen ‣ Konto ‣ Konto sichern` auf. Falls Ihr während des Registrierungsvorgangs ein Passwort festgelegt habt, solltet Ihr dieses nun eingeben. Eure Sicherungskopie wird als `.gz`-Archivdatei gespeichert. |
        |Sicherungskopien wiederherstellen |Falls Ihr Jami neu installiert, könnt Ihr auf `Ein Konto aus einer Sicherung wiederherstellen` klicken. Ruft dann die Archivdatei auf, die Ihr als Sicherungskopie angelegt habt. Falls erforderlich, gebt Euer Passwort ein und klickt auf `Ein Konto aus einer Sicherung wiederherstellen`. |



=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        ### Installation

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Terminal öffnen |Falls Ihr eine Linux-Distro wie z.B. [Ubuntu](https://gofoss.net/de/ubuntu/) verwendet, nutzt die Tastenkombination `Strg+Alt+T` oder klickt auf die `Aktivitäten`-Schaltfläche oben links und sucht nach `Terminal`. |
        |Abhängigkeiten installieren |`sudo apt install gnupg dirmngr ca-certificates curl --no-install-recommends` |
        |Signierschlüssel hinzufügen |`curl -s https://dl.jami.net/public-key.gpg | sudo tee /usr/share/keyrings/jami-archive-keyring.gpg > /dev/null` |
        |Paketquelle zur Liste hinzufügen |`sudo sh -c "echo 'deb [signed-by=/usr/share/keyrings/jami-archive-keyring.gpg] https://dl.jami.net/nightly/ubuntu_20.04/ jami main' > /etc/apt/sources.list.d/jami.list"` |
        |Lokalen apt-Cache aktualisieren |`sudo apt update` |
        |Jami-Client installieren |`sudo apt install jami`|

        ### Registrierung

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Benutzerkonto anlegen |Öffnet die Jami-App und klickt auf `Jami-Konto erstellen`. Gebt einen Benutzernamen ein. |
        |Passwort festlegen |Optional könnt Ihr ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) festlegen, um Euer Konto zu verschlüsseln. Bewahrt dieses Passwort an einem sicheren Ort auf, es kann nicht neu erstellt werden. |
        |Profil anlegen |Optional könnt Ihr einen für andere sichtbaren Benutzernamen sowie ein Profilbild festlegen. |
        |Sicherheitskopie anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden könnt Ihr während des Registrierungsvorgangs eine [Sicherungskopie Eurer Benutzerinformationen anlegen](https://gofoss.net/de/backups/). |

        ### Einzel-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Einzel-Chats beginnen |• Öffnet die Jami-App <br>• Sucht nach Kontakten anhand ihres Benutzernamens <br>• Klickt auf `Zur Unterhaltung hinzufügen`. Der Gesprächspartner erhält nun eine Einladung <br>• Sobald die Einladung angenommen wurde, könnt Ihr mit dem Chat beginnen |
        |Sprachnachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Audio-Nachricht hinterlassen` befindet sich im Menü neben dem Texteingabefeld <br>• Klickt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Senden`-Schaltfläche klicken <br>• Oder den Aufnahmedialog verlassen, um den Vorgang abzubrechen |
        |Videonachricht senden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Video-Nachricht hinterlassen` befindet sich im Menü neben dem Texteingabefeld < <br>• Klickt darauf, um mit der Aufnahme zu beginnen <br>• Sobald Ihr fertig seid könnt Ihr auf die `Senden`-Schaltfläche klicken <br>• Oder den Aufnahmedialog verlassen, um den Vorgang abzubrechen |
        |Einzel-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf |
        |Einzel-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Bildschirmfreigabe: Klickt während eines Videoanrufs auf die Schaltfläche `Bildschirm teilen` |
        |Dateien versenden |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Die Schaltfläche `Datei senden` befindet sich im Menü neben dem Texteingabefeld <br>• Wählt eine Datei zum Senden <br>• Alternativ könnt Ihr einfach eine Datei per Drag & Drop in das Jami-Fenster verschieben |

        ### Gruppen-Chats, Sprach- oder Videoanrufe

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Gruppen-Chats beginnen |Zum Zeitpunkt der Abfassung dieses Textes [arbeitet Jami noch an der Implementierung von Gruppen-Chats](https://jami.net/swarm-introducing-a-new-generation-of-group-conversations/). Diese sogenannten "Swarms" sind vollkommen dezentrale, Peer-to-Peer Textnachrichten mit einer theoretisch unbegrenzten Anzahl an Teilnehmern. |
        |Gruppen-Sprachanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Telefon`-Symbol im oberen Menü und startet den Sprachanruf <br>• Sobald der Anruf beginnt, klickt auf `Menu ‣ Teilnehmer hinzufügen` |
        |Gruppen-Videoanruf tätigen |• Öffnet die Jami-App <br>• Öffnet einen bestehenden Chat, oder erstellt einen neuen <br>• Klickt auf das `Kamera`-Symbol im oberen Menü und startet den Videoanruf <br>• Sobald der Anruf beginnt, klickt auf `Menu ‣ Teilnehmer hinzufügen` <br>• Bildschirmfreigabe: Klickt während eines Gruppen-Videoanrufs auf die Schaltfläche `Bildschirm teilen` |

        ### Sicherungskopien

        | Anweisungen | Beschreibung |
        | ------ | ------ |
        |Sicherungskopien anlegen |Benutzerinformationen werden lokal auf Euren Geräten gespeichert. Falls Ihr Euer Gerät verliert oder Jami neu installiert, gehen diese Benutzerinformationen für immer verloren. Um dies zu vermeiden solltet Ihr regelmäßig [Sicherungskopien Eures Jami-Kontos anlegen](https://gofoss.net/de/backups/). Ruft hierzu das Menü `Einstellungen ‣ Konto ‣ Konto sichern` auf. Falls Ihr während des Registrierungsvorgangs ein Passwort festgelegt habt, solltet Ihr dieses nun eingeben. Eure Sicherungskopie wird als `.gz`-Archivdatei gespeichert. |
        |Sicherungskopien wiederherstellen |Falls Ihr Jami neu installiert, könnt Ihr auf `Ein Konto aus einer Sicherung wiederherstellen` klicken. Ruft dann die Archivdatei auf, die Ihr als Sicherungskopie angelegt habt. Falls erforderlich, gebt Euer Passwort ein und klickt auf `Ein Konto aus einer Sicherung wiederherstellen`. |



<br>

<center> <img src="../../assets/img/separator_briar.svg" alt="Briar" width="150px"></img> </center>

## Briar

[Briar](https://briarproject.org/) ist eine [quelloffene](https://code.briarproject.org/briar/briar/tree/master/) Nachrichten-App für Android. Sie richtet sich insbesondere an Personen mit einem erhöhten Bedrohungsmodell wie Aktivisten oder Journalisten. Briar ist unabhängig von Google Cloud Messaging (GCM) und basiert auf einer Peer-to-Peer-Architektur, bei der sich die NutzerInnen direkt miteinander verbinden, ohne sich auf Server zu verlassen.

Die gesamte Kommunikation wird durchgängig verschlüsselt und über das [Tor-Netwerk](https://gofoss.net/de/tor/) gesendet. Briar unterstützt *Perfect Forward Secrecy*. Sämtliche Daten werden auf den lokalen Geräten gespeichert, es ist keine Registrierung per Telefonnummer oder E-Mail erforderlich. Die Anwendung ist auch ohne Internetverbindung funktionsfähig, und zwar über WLAN oder Bluetooth. Es bleibt jedoch zu beachten, dass die BenutzerInnen gleichzeitig online sein müssen, um miteinander zu chatten. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Android"

    ### Installation

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Installation |Ladet die Briar-App herunter und installiert sie: <br><br> • [Google Play Store](https://play.google.com/store/apps/details?id=org.briarproject.briar.android&hl=de_DE) <br> • [F-Droid](https://f-droid.org/de/packages/org.briarproject.briar.android/) <br> • [Aurora Store](https://auroraoss.com/de/) |
    |Tracker frei |Die Briar-App [enthält 0 Tracker und benötigt 11 Berechtigungen](https://reports.exodus-privacy.eu.org/de/reports/org.briarproject.briar.android/latest/). Zum Vergleich:<br>• TikTok: 16 Tracker, 76 Berechtigungen<br>• Snapchat: 2 Tracker, 44 Berechtigungen<br>• WhatsApp: 1 Tracker, 57 Berechtigungen |
    |Kein GCM |Briar kann Push-Benachrichtigungen ohne Googles Cloud Messaging (GCM) verarbeiten. Dies ist besonders dann praktisch, wenn Ihr Euer [Smartphone entgoogeln](https://gofoss.net/de/intro-free-your-phone/) wollt. |

    ### Registrierung

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |BenutzerInnen erstellen |Öffnet die Briar-App und gebt einen Benutzernamen an. |
    |Passwort festlegen |Gebt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Bewahrt dieses Passwort sicher auf: solltet Ihr es vergessen, kann Euer Konto nicht wiederhergestellt werden! |
    |Hintergrundverbindungen aktivieren |Tippt auf die Schaltflächen, um die Batterieoptimierung zu deaktivieren und Briar im Hintergrund laufen zu lassen. |
    |Konto erstellen |Tippt schließlich auf `Konto anlegen`. |

    ### Einzel-Chats

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Persönliches Treffen |• Trefft Euch persönlich mit Euren Kontakten und öffnet die Briar-App <br>• Tippt auf die `+`-Schaltfläche und wählt `Kontakt in der Nähe hinzufügen` <br>• Tippt auf `Weiter` <br>• Ihr müsst nun beide gegenseitig die jeweiligen QR-Codes auf Euren Bildschirmen scannen <br>• Auf diese Weise ist Eure Kommunikation von Anfang an authentifiziert <br>• Ihr könnt nun fröhlich drauf los chatten |
    |Vorgestellt werden |• Solltet Ihr Eure Kontakte nicht persönlich treffen können, lasst Euch von einem gemeinsamen, vertrauenswürdigen Freund empfehlen <br>• Dieser Freund muss Briar auf seinem/ihrem Telefon öffnen <br>• Dann sollte er/sie auf Euren Namen in der Kontaktliste tippen <br>• Anschließend sollte er/sie `Menü ‣ Kontaktempfehlung abgeben` wählen <br>• Er/sie sollte nun die Kontaktperson auswählen, der Ihr vorgestellt werden möchtet <br>• Schließlich sollte er/sie auf `Kontaktempfehlung abgeben` tippen <br>• Ihr solltet dann dazu aufgefordert werden, die Kontaktempfehlung zu akzeptieren <br>• Sobald beide Parteien die Empfehlung akzeptiert haben, könnt Ihr anfangen zu chatten |
    |Kontakte auf Distanz hinzufügen |• Solltet Ihr Eure Kontakte nicht persönlich treffen können, könnt Ihr sie auch auf Distanz hinzufügen <br>• Tippt auf die `+`-Schaltfläche und wählt `Kontakt in der Ferne hinzufügen` <br>• Teilt den angezeigten Link mit Eurem Kontakt über einen sicheren Kanal (z.B. verschlüsselte E-Mail, Signal, usw.) <br>• Bittet Euren Kontakt ebenfalls, Euch seinen/ihren Link mitzuteilen <br> Stellt sicher, dass der Link authentisch ist, bevor Ihr ihn in die App eingebt (z.B. übers Telefon, einen zuvor authentifizierten Chat oder ein persönliches Treffen) <br>• Tippt auf `Weiter` sobald beide Links hinzugefügt wurden und wählt einen Nutzernamen für Euren neuen Kontakt <br>• Sobald Euer Kontakt die Einladung annimmt, könnt Ihr anfangen zu chatten |

    ### Gruppen-Chats, Foren, Blogs und RSS-Feeds

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Gruppen-Chats beginnen |• Öffnet die Briar-App <br>• Tippt auf `Menü ‣ Private Gruppen` <br>• Tippt auf die `+`-Schaltfläche und erstellt eine neue Gruppe <br>• Wählt einen Gruppennamen, tippt auf `Gruppe erstellen` und ladet Eure Kontakte ein <br>• Ihr könnt nun fröhlich drauf los chatten |
    |Foren starten |• Ein Forum ist eine öffentliche Unterhaltung <br>• Im Gegensatz zu privaten Gruppen-Chats kann jeder Teilnehmer weitere Kontakte einladen <br>• Öffnet die Briar-App <br>• Tippt auf `Menü ‣ Foren` <br>• Tippt auf die `+`-Schaltfläche und erstellt ein neues Forum <br>• Wählt einen Forennamen und tippt auf `Forum erstellen` <br>• Öffnet das Forum und tippt auf das `Teilen`-Symbol, um weitere Kontakte einzuladen |
    |Blogs starten |• Ein Blog ermöglicht es, Neuigkeiten mit Euren Kontakten zu teilen <br>• Öffnet die Briar-App <br>• Tippt auf `Menü ‣ Blogs` <br>• Tippt auf das `Stift`-Symbol um einen neuen Beitrag zu verfassen, und tippt dann auf `Veröffentlichen` |
    |RSS-Feeds lesen |• Mit Briar könnt Ihr ebenfalls Blogs oder Nachrichtenseiten verfolgen! <br>• Öffnet die Briar-App <br>• Tippt auf `Menü ‣ Blogs` <br>• Tippt auf `Menü ‣ RSS-Feeds ‣ RSS-Feed importieren` <br>• Gebt eine URL ein und tippt auf `Importieren` |

    ### Sicherungskopien und Bildschirmsperre

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    |Sicherungskopien |• Aus Sicherheitsgründen wird Euer Benutzerkonto dauerhaft gelöscht, wenn Ihr den Zugang zu Euren Geräten verliert, Euer Passwort vergesst oder Briar deinstalliert <br>• Dies schließt Eure Identitäten, Kontakte und Nachrichten ein <br>• Ihr müsst Eure Kontakte nach einer Neuinstallation also erneut in Briar hinzufügen und verifizieren <br>• Außerdem gibt es keine Möglichkeit, Euer Benutzerkonto auf ein anderes Gerät zu übertragen |
    |Bildschirmsperre |• Briar kann gesperrt werden, ohne den Benutzer abzumelden <br>• Tippt hierzu auf `Menü ‣ Einstellungen ‣ Sicherheit` <br>• Aktiviert die `App-Sperre` <br>• Briar wird nun automatisch gesperrt, wenn die App nicht genutzt wird (z.B. nach 5 Minuten) oder wenn Ihr auf `Menü ‣ App sperren` tippt <br>• Um Briar zu entsperren müsst Ihr dieselbe PIN, dasselbe Muster oder Passwort eingeben, mit dem Ihr normalerweise Euer Gerät entsperrt |



??? warning "Die Kommunikation über Briar ist zwar verschlüsselt, aber nicht anonym"

    Briar ergreift mehrere Maßnahmen, um die sogenannte "Unverknüpfbarkeit" zu erreichen. [Briar kann allerdings keine Anonymität garantieren](https://code.briarproject.org/briar/briar/-/wikis/FAQ#does-briar-provide-anonymity). Zwar kann niemand herausfinden, wer Eure Kontakte sind, aber Eure Kontakte können sehr wohl herausfinden, wer Ihr seid. Zur Kontaktaufnahme gibt Briar nämlich Eure Bluetooth-Adresse sowie die aktuellen IPv4- und IPv6-Adressen der Wi-Fi-Schnittstelle weiter.


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Details oder Antworten finden Ihr in:

* [Signals Dokumentation](https://support.signal.org/hc/de/). Ihr könnt auch gerne [Signals Gemeinschaft](https://community.signalusers.org/) um Hilfe bitten.
* [Elements FAQ](https://element.io/help). Ihr könnt auch gerne [Elements Gemeinschaft](https://teddit.net/r/elementchat/) um Hilfe bitten.
* [Jamis FAQ](https://jami.net/help/). Ihr könnt auch gerne [Jamis Gemeinschaft](https://forum.jami.net/) um Hilfe bitten.
* [Briars Benutzerhandbuch](https://briarproject.org/manual/de/) oder der [Matrix-Chat](https://matrix.to/#/#freenode_#briar:matrix.org).


<br>

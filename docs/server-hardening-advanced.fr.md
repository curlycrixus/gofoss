---
template: main.html
title: Sécurisation avancée de votre serveur
description: Auto-hébergement (avancé). Sécurisez votre serveur. Qu'est-ce qu'un pare-feu ? Qu'est-ce que NTP ? Logiciels malveillants sur Linux. ClamAV. Qu'est-ce qu'un core dump ?
---

# Sécurisation avancée de votre serveur

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises"

!!! warning "Avertissement"

    **Aucun système n'est sûr**. Ceci est le [second chapitre sur la sécurité de serveur](https://gofoss.net/fr/server-hardening-basics/). Bien qu'il présente des mesures plus avancées visant à protéger votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/) contre la majorité des menaces immédiates, n'importe quel pirate ou organisme compétent disposant de ressources suffisantes trouvera vraisemblablement un moyen pour accéder à votre système.


## Limiter les privilèges « sudo »

Il est recommandé de limiter l'accès privilégié aux utilisateurs d'un groupe spécifique, comme indiqué ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Créez un groupe appelé `sudousers`, et ajoutez-y les deux utilisateurs `gofossroot` et `gofossadmin` (ajustez ces noms d'utilisateurs en fonction de votre propre configuration) :


    ```bash
    sudo groupadd sudousers
    sudo usermod -a -G sudousers gofossroot
    sudo usermod -a -G sudousers gofossadmin
    ```

    Sauvegardez le fichier de configuration `sudoers`. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/sudoers /etc/sudoers-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/sudoers
    ```

    Ajoutez la ligne suivante, si elle n'est pas déjà incluse :

    ```bash
    %sudousers   ALL=(ALL:ALL) ALL
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Enfin, limitez l'accès privilégié au groupe `sudousers` :

    ```bash
    sudo dpkg-statoverride --update --add root sudousers 4750 /bin/su
    ```

    Vérifiez les permissions avec la commande suivante, le terminal devrait afficher `sudousers` :

    ```bash
    ls -lh /bin/su
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/102e7dac-ef23-4ac4-a4ca-e179dbabddd7" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_userinfo.svg" alt="Processus protégés" width="150px"></img> </center>

## Protéger les processus

Le répertoire `/proc` contient des informations potentiellement sensibles sur les processus tournant sur votre serveur. Par défaut, tous les utilisateurs ou utilisatrices peuvent accéder à ce répertoire. Suivez les instructions ci-dessous pour limiter l'accès des usagers ou programmes non autorisés aux données sensibles.

??? tip "Montrez-moi le guide étape par étape"

    Sauvegardez le fichier de configuration `fstab`. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --preserve /etc/fstab /etc/fstab-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/fstab
    ```

    Occultez les informations sur les processus en insérant la ligne suivante à la fin du fichier :

    ```bash
    proc    /proc   proc    defaults,hidepid=2  0   0
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Enfin, remontez le répertoire `/proc` :

    ```bash
    sudo mount -o remount,hidepid=2 /proc
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/b8252b3f-93b0-4f64-a016-af4c436f60e7" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Sécurité de mots de passe" width="150px"></img> </center>

## Renforcer la sécurité des mots de passe

Par défaut, les utilisateurs et utilisatrices peuvent librement choisir leurs mots de passe. Ci-dessous, nous préciserons comment renforcer la sécurité des mots de passe en définissant :

* un nombre maximum de jours pendant lesquels les mots de passe peuvent être utilisés
* un nombre minimum de jours avant que les mots de passe puissent être modifiés
* une longueur minimale pour les mots de passe
* le nombre de caractères majuscules et minuscules ainsi que de chiffres à utiliser

??? tip "Montrez-moi le guide étape par étape"

    ### Configurer les paramètres du compte

    Sauvegardez le fichier de configuration `login.defs`. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --preserve /etc/login.defs /etc/login.defs-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/login.defs
    ```

    Modifiez ou ajoutez les paramètres suivants :

    ```bash
    PASS_MAX_DAYS   365
    PASS_MIN_DAYS   1
    PASS_WARN_AGE   30
    SHA_CRYPT_MIN_ROUNDS 1000000
    SHA_CRYPT_MAX_ROUNDS 1000000
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Appliquons les mêmes paramètres aux administrateurs `gofossroot` et `gofossadmin`, qui ont été créés avant que les règles de mot de passe n'aient été renforcées (ajustez le nom des administrateurs en fonction de votre propre configuration) :

    ```bash
    sudo chage -m 1 -M 365 -W 30 gofossroot
    sudo chage -m 1 -M 365 -W 30 gofossadmin
    ```

    Assurez-vous que tous les changements aient été correctement appliqués :

    ```bash
    sudo chage -l gofossroot
    sudo chage -l gofossadmin
    ```

    Voici quelques informations supplémentaires sur ces paramètres :

    <center>

    | Paramètres | Description |
    | ------ | ------ |
    | `PASS_MAX_DAYS` |Nombre maximal de jours pendant lesquels un mot de passe peut être utilisé. |
    | `PASS_MIN_DAYS` |Nombre minimum de jours autorisés entre les changements de mot de passe. |
    | `PASS_WARN_AGE` |Nombre de jours d'avertissement avant l'expiration d'un mot de passe. |
    | `SHA_CRYPT_MIN_ROUNDS & SHA_CRYPT_MAX_ROUNDS` |Par défaut, Linux stocke le mot de passe de l'administrateur en utilisant un seul hachage SHA-512. Pour renforcer le mot de passe, utilisez plusieurs tours de hachage. |

    </center>


    ### Configurer PAM

    [« Linux Pluggable Authentication Modules (PAM) « »](https://en.wikipedia.org/wiki/Linux_PAM) est un outil puissant pour gérer l'authentification des utilisateurs. Installez le module PAM suivant pour activer la vérification de la force des mots de passe :

    ```bash
    sudo apt install libpam-cracklib
    ```

    Sauvegardez le fichier de configuration `pam.d'. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/pam.d/common-password /etc/pam.d/common-password-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/pam.d/common-password
    ```

    Modifiez ou ajoutez les paramètres suivants :

    ```bash
    password requisite pam_cracklib.so retry=3 minlen=12 ucredit=-1 lcredit=-1 dcredit=-1 difok=3 gecoschec
    ```

    Toujours dans ce même fichier, ajoutez l'option `shadow` ainsi qu'un nombre accru de tours (en anglais, « `rounds` ») dans la ligne suivante :

    ```bash
    password        [success=1 default=ignore]      pam_unix.so obscure use_authtok try_first_pass sha512 shadow rounds=1000000
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Voici quelques informations supplémentaires sur ces paramètres :

    <center>

    | Paramètres | Description |
    | ------ | ------ |
    | `retry = 3` |Interpeller les utilisateurs 3 fois avant de renvoyer une erreur. |
    | `minlen = 10` |Longueur minimale du mot de passe. |
    | `ucredit = -1` |Le mot de passe doit comporter au moins une lettre majuscule. |
    | `lcredit = -1` |Le mot de passe doit comporter au moins une lettre minuscule. |
    | `dcredit = -1` |Le mot de passe doit comporter au moins un chiffre. |
    | `difok = 3` |Au moins 3 caractères du nouveau mot de passe ne peuvent pas avoir été utilisés dans l'ancien mot de passe. |
    | `gecoschec` |Les mots de passe contenant le nom du compte utilisateur ne sont pas autorisés. |
    | `shadow rounds=1000000` |Par défaut, Linux stocke le mot de passe de l'administrateur en utilisant un seul hachage SHA-512. Pour renforcer le mot de passe, utilisez plusieurs tours de hachage. |

    </center>


??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a893ced7-b4a8-4e68-83a8-8dc195c2150c" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Sécurité du réseau" width="150px"></img> </center>

## Sécuriser le réseau

La commande `sysctl` permet de paramétrer avec précision le noyau (en anglais, « kernel ») et peut notamment servir à améliorer la sécurité du réseau. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Sauvegardez le fichier de configuration `sysctl`. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/sysctl.conf /etc/sysctl.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/sysctl.conf
    ```

    Supprimez le contenu du fichier en tapant `:%d`. Puis saisissez ou copiez/collez le contenu suivant :

    ```bash
    # IP Spoofing protection
    net.ipv4.conf.all.rp_filter = 1
    net.ipv4.conf.default.rp_filter = 1

    # Ignore ICMP broadcast requests
    net.ipv4.icmp_echo_ignore_broadcasts = 1

    # Disable source packet routing
    net.ipv4.conf.all.accept_source_route = 0
    net.ipv6.conf.all.accept_source_route = 0
    net.ipv4.conf.default.accept_source_route = 0
    net.ipv6.conf.default.accept_source_route = 0

    # Block SYN attacks
    net.ipv4.tcp_syncookies = 1
    net.ipv4.tcp_max_syn_backlog = 2048
    net.ipv4.tcp_synack_retries = 2
    net.ipv4.tcp_syn_retries = 5

    # Log Martians
    net.ipv4.conf.all.log_martians = 1
    net.ipv4.icmp_ignore_bogus_error_responses = 1

    # Ignore ICMP redirects
    net.ipv4.conf.all.send_redirects = 0
    net.ipv4.conf.default.send_redirects = 0
    net.ipv4.conf.all.accept_redirects = 0
    net.ipv6.conf.all.accept_redirects = 0
    net.ipv4.conf.default.accept_redirects = 0
    net.ipv6.conf.default.accept_redirects = 0

    # Ignore Directed pings
    net.ipv4.icmp_echo_ignore_all = 1
    ```

    Sauvegardez et fermez le fichier (`:wq!`), puis chargez les nouveaux paramètres :

    ```bash
    sudo sysctl -p
    ```

    Voici quelques informations supplémentaires sur ces mesures de sécurité :

    <center>

    |Mesure |Description |
    | ------ | ------ |
    | `Enable IP spoofing protection` |Protégez-vous contre l'[usurpation d'adresse IP](https://fr.wikipedia.org/wiki/Usurpation_d%27adresse_IP), lors de laquelle des pirates déguisent leur adresse IP, généralement lors d'[attaques par déni de service (en anglais, Denial of Service ou DoS)](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service). |
    | `Ignore ICMP broadcast requests` |Évitez que votre serveur ne participe à une [attaque par rebond](https://fr.wikipedia.org/wiki/Attaque_par_rebond). |
    | `Ignore ICMP redirects` |Protégez-vous contre les faux paquets de redirection ICMP utilisés par des pirates pour provoquer une [attaque par déni de service (en anglais, Denial of Service ou DoS)](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service). |
    | `Disable source packet routing` |Protégez-vous contre les paquets routés à la source, une méthode utilisée par les pirates pour masquer leur identité et leur localisation. |
    | `Block SYN attacks` |Protégez-vous contre les attaques SYN, une forme d'[attaque par déni de service (en anglais, Denial of Service ou DoS)](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service). |
    | `Log martians` |Protégez-vous contre les paquets martiens. Il s'agit de paquets usurpés dont l'adresse source est manifestement fausse. Ils peuvent être utilisés lors d'[attaques du type déni de service (en anglais, Denial of Service ou DoS)](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service). |
    | `Ignore directed pings` |La commande « ping » peut être utilisée pour vérifier l'état de la connexion avec votre serveur. Mais elle peut également être utilisée à mauvais escient pour effectuer des attaques. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/06be3f02-f2c2-4998-b355-64d5fcb9c608" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_antivirus.svg" alt="Antivirus et anti-maliciel" width="150px"></img> </center>

## Utiliser des antivirus et anti-maliciels

Linux Malware Detect ([LMD](https://www.rfxn.com/projects/linux-malware-detect/)) et [ClamAV](https://www.clamav.net/) sont des solutions open source de détection de virus et de logiciels malveillants (« maliciels »). Suivez les instructions ci-dessous pour les installer et configurer sur votre serveur.

??? tip "Montrez-moi le guide étape par étape"

    ### Détection de maliciels (LMD)

    Installez LMD en utilisant les commandes suivantes :

    ```bash
    sudo apt install wget
    sudo wget http://www.rfxn.com/downloads/maldetect-current.tar.gz
    sudo tar -xvf maldetect-current.tar.gz
    cd maldetect-1.6.4
    sudo ./install.sh
    ```

    Sauvegardez le fichier de configuration `maldet`. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /usr/local/maldetect/conf.maldet /usr/local/maldetect/conf.maldet-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /usr/local/maldetect/conf.maldet
    ```

    Modifiez ou ajoutez les paramètres suivants (ajustez le nom de l'administrateur `gofossadmin` en fonction de votre propre configuration) :

    ```bash

    # envoi des résultats d'inspection des maliciels à l'administrateur système local gofossadmin (ajuster en conséquence) vers /var/mail/gofossadmin

    email_alert="1"
    email_addr="gofossadmin@localhost"

    # passage en quarantaine et alerte en cas de détection d'un maliciel
    quarantine_hits="1"

    # ne pas supprimer automatiquement les injections de maliciels basés sur des chaînes de caractères (en cas de faux positifs, éviter de supprimer les fichiers)
    quarantine_clean="0"

    # permettre la suspension des utilisateurs touchés par des maliciels
    quarantine_suspend_user="1"

    #utiliser le moteur d'analyse de ClamAVs (analyses plus rapides)
    scan_clamscan="1"

    #analyse des fichiers appartenant à l'administrateur système
    scan_ignore_root="0"
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Mettez à jour LMD et téléchargez les définitions récentes de maliciels :

    ```bash
    sudo maldet --update-ver
    sudo maldet --update
    ```


    ### Antivirus (ClamAV)

    Installez ClamAV avec la commande suivante :

    ```bash
    sudo apt install clamav
    ```

    Vérifiez que le logiciel a bien été installé :

    ```bash
    clamscan --version
    ```

    Mettez à jour la base de données des signatures de virus :

    ```bash
    sudo systemctl stop clamav-freshclam
    sudo freshclam
    sudo systemctl start clamav-freshclam
    ```

    Autorisez ClamAV à se lancer automatiquement après chaque redémarrage :

    ```bash
    sudo systemctl enable clamav-freshclam
    ```


    ### Vérifier LMD et ClamAV

    Nous utiliserons un maliciel factice fourni par le groupe d'experts européens en sécurité informatique ([EICAR](https://www.eicar.org/?page_id=3950/)) pour tester l'analyse de maliciels et l'antivirus. Téléchargez les fichiers comme suit :

    ```bash
    cd /var/www/html
    sudo wget http://www.eicar.org/download/eicar.com
    sudo wget http://www.eicar.org/download/eicar.com.txt
    sudo wget http://www.eicar.org/download/eicar_com.zip
    sudo wget http://www.eicar.org/download/eicarcom2.zip
    ```

    Utilisez ClamAV pour détecter de manière récursive la présence de virus dans le répertoire `/var/www/html` et ses sous-répertoires :

    ```bash
    sudo clamscan --recursive --infected /var/www/html
    ```

    Le terminal devrait afficher quelque chose comme :

    ```bash
    /var/www/html/eicar_com.zip: Win.Test.EICAR_HDB-1 FOUND
    /var/www/html/eicar.com: Win.Test.EICAR_HDB-1 FOUND
    /var/www/html/eicarcom2.zip: Win.Test.EICAR_HDB-1 FOUND
    /var/www/html/eicar.com.txt: Win.Test.EICAR_HDB-1 FOUND

    ----------- SCAN SUMMARY -----------
    Known viruses: 6880642
    Engine version: 0.102.2
    Scanned directories: 1
    Scanned files: 5
    Infected files: 4
    Data scanned: 0.01 MB
    Data read: 0.01 MB (ratio 1.00:1)
    Time: 54.632 sec (0 m 54 s)
    ```

    Utilisez ensuite LMD pour détecter la présence de maliciels dans le même répertoire :

    ```bash
    sudo maldet --scan-all /var/www/html
    ```

    L'affichage du terminal devrait contenir un identifiant du format `200510-1954.2127`. Vous pouvez accéder à un rapport détaillé avec la commande suivante (ajustez l'identifiant en conséquence) :

    ```bash
    sudo maldet --report 200510-1954.2127
    ```

    La liste de l'ensemble des rapports d'analyse est consultable avec la commande suivante :

    ```bash
    sudo maldet -e list
    ```

    Pour finir, supprimez tous les fichiers mis en quarantaine :

    ```bash
    sudo ls /usr/local/maldetect/quarantine/
    sudo rm -rf /usr/local/maldetect/quarantine/*
    ```


    ### Programmer des détections de maliciels avec LMD

    Le détecteur de maliciels LMD est configuré pour exécuter une tâche cron quotidienne (`/etc/cron.daily/maldet`). Cette tâche met à jour le registre des maliciels et analyse tous les répertoires de l'utilisateur. Ce qui devrait suffire pour la plupart des utilisations. Afin de limiter la charge du serveur, vous pouvez désactiver la détection en temps réel et ainsi éviter que LMD ne soit lancé toutes les 10 minutes :

    ```bash
    sudo rm /etc/cron.d/maldet_pub
    sudo maldet -k
    ```


    ### Programmer des détections de virus avec ClamAV

    La base de données des signatures de virus est mise à jour par le service `freshclam`, par défaut toutes les heures. Vous pouvez modifier cette fréquence dans le fichier `/etc/clamav/freshclam.conf`, puis redémarrer le service avec `sudo service clamav-freshclam restart`.

    Pour planifier la détection de virus, créez un fichier journal vide :

    ```bash
    sudo touch /var/log/clamav/clamscan.log
    ```

    Créez une tâche cron pour rechercher les virus tous les jours :

    ```bash
    sudo vi /etc/cron.daily/clamav
    ```

    Ajoutez le contenu suivant :

    ```bash
    #!/bin/sh

    MYLOG=/var/log/clamav/clamscan.log
    echo "Scanning for viruses at `date`" >> $MYLOG
    clamscan --recursive --infected --max-filesize=100M --max-scansize=100M --exclude=/boot / >> $MYLOG 2>&1
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Maintenant, rendez le script exécutable :

    ```bash
    sudo chmod +x /etc/cron.daily/clamav
    ```

    Notez que nous avons utilisé les paramètres suivants pour exécuter `clamscan` dans le script, vous pouvez les adapter à vos propres besoins :

    <center>

    |Paramètre |Description |
    | ------ | ------ |
    | `--recursive` | Détecter les virus de manière récursive dans les répertoires et sous-répertoires. |
    | `--infected` | Imprimer uniquement les fichiers infectés dans le rapport d'analyse, plutôt que tous les fichiers analysés. |
    | `--max-filesize=100M`, `--max-scansize=100M` | Analyser des fichiers d'une taille maximale de 100 Mo. Par défaut, ClamAV n'analyse pas les fichiers de plus de 20 Mo. |
    | `--exclude=/boot` | Dans notre exemple, nous excluons le répertoire `/boot` de l'analyse. Assurez-vous d'adapter ce paramètre à votre propre configuration, l'option peut également être utilisée de façon répétée pour exclure plusieurs répertoires. |
    | `/` | Répertoire dans lequel ClamAV est censé détecter des virus. Dans notre exemple, nous avons analysé l'ensemble du dossier racine `/`. Adaptez le répertoire cible en fonction de vos propres besoins. |

    </center>

    Testez la tâche cron. Soyez patient, selon la taille des répertoires cibles, l'analyse peut prendre un certain temps :

    ```bash
    sudo bash /etc/cron.daily/clamav
    sudo cat /var/log/clamav/clamscan.log
    ```

    L'affichage du terminal devrait ressembler à ceci :

    ```bash
    ----------- SCAN SUMMARY -----------
    Infected files: 0
    Time: 54.052 sec (0 m 54 s)
    Start Date: 2021:05:22 21:05:29
    End Date:   2021:05:22 21:06:23
    ```


??? tip "Montrez-moi une vidéo récapitulative (4min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/a9b938db-0d32-4c07-8520-40fb98b07e52" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Permissions" width="150px"></img> </center>

## Gérer les permissions

Vous trouverez ci-dessous plus de détail sur la limitation d'accès non autorisé aux fichiers systèmes.

??? tip "Montrez-moi le guide étape par étape"

    ```bash
    sudo chmod 600 /etc/crontab
    sudo chmod 600 /etc/ssh/sshd_config
    sudo chmod 700 /etc/cron.d
    sudo chmod 700 /etc/cron.daily
    sudo chmod 700 /etc/cron.hourly
    sudo chmod 700 /etc/cron.weekly
    sudo chmod 700 /etc/cron.monthly
    sudo chmod 750 /home/gofossroot
    sudo chmod 750 /home/gofossadmin
    ```


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/50edc3be-5fc6-43c2-a283-166ebdc54b78" frameborder="0" allowfullscreen></iframe>
    </center>


??? info "Dites-m'en plus sur les permissions de Linux"

    [umask](https://fr.wikipedia.org/wiki/Umask) restreint l'accès des utilisateurs aux fichiers ou répertoires nouvellement créés. Il existe **trois classes d'utilisateurs et d'utilisatrices** :

    <center>

    |Classe |Description |
    | ------ | ------ |
    | « User » |Propriétaire du fichier ou du répertoire. |
    | « Group » |Utilisateurs et utilisatrices appartenant au groupe du fichier ou du répertoire. |
    | « Others » |Utilisatrices et utilisateurs qui ne sont ni propriétaires ni membres du groupe du fichier ou du répertoire. |

    </center>

    Et il y a **trois restrictions d'accès** :

    <center>

    |Restriction d'accès |Description |Valeur décimale |
    | ------ | ------ | :------: |
    | « Read » ( r ) |Lire ou visualiser le fichier ou le répertoire. | 4 |
    | « Write » ( w ) |Écrire, éditer ou modifier le fichier ou le répertoire. | 2 |
    | « Execute » ( x ) |Exécuter le fichier ou le répertoire. | 1 |

    </center>

    `umask` restreint l'accès aux fichiers ou répertoires nouvellement créés en additionnant ces trois valeurs décimales :

    <center>

    |umask |Valeur |Restrictions |
    | :------: | :------: | ------ |
    | --- | 0 = 0 + 0 + 0 |Peut lire, écrire et exécuter. |
    | --x | 1 = 0 + 0 + 1 |Peut lire et écrire, mais pas exécuter. |
    | -w- | 2 = 0 + 2 + 0 |Peut lire et exécuter, mais pas écrire. |
    | -wx | 3 = 0 + 2 + 1 |Peut lire, mais pas écrire ou exécuter. |
    | r-- | 4 = 4 + 0 + 0 |Peut écrire et exécuter, mais pas lire. |
    | r-x | 5 = 4 + 0 + 1 |Peut écrire, mais pas lire ou exécuter. |
    | rw- | 6 = 4 + 2 + 0 |Peut exécuter, mais pas lire ou écrire. |
    | rwx | 7 = 4 + 2 + 1 |Ne peut ni lire, ni écrire, ni exécuter. |

    </center>

    `umask` consiste en trois valeurs, une pour chaque classe d'utilisateurs. Par exemple, en changeant `umask` de `022` à `027`, on empêche les « Autres (en anglais, Others) » de lire, d'écrire ou d'exécuter des fichiers et des répertoires :

    <center>

    |Umask |Restrictions pour les utilisateurs |Restrictions pour les groupes |Restrictions pour les autres |Permissions résultantes |
    | :------: | :------: | :------: | :------: | ------ |
    | `022` | --- | -w- | -w- |• Le propriétaire du fichier ou du répertoire peut le lire, le modifier et l'exécuter. <br>• Les membres du groupe du fichier ou du répertoire peuvent le lire et l'exécuter, mais pas le modifier. <br>• Les utilisateurs qui ne sont ni propriétaires ni membres du groupe du fichier ou du répertoire peuvent le lire et l'exécuter, mais pas le modifier. <br>• En d'autres termes, le propriétaire du fichier ou du répertoire dispose d'un accès complet, le groupe et les autres ne peuvent que le lire et l'exécuter. |
    | `027` | --- | -w- | rwx |• Le propriétaire du fichier ou du répertoire peut le lire, le modifier et l'exécuter. <br>• Les membres du groupe du fichier ou du répertoire peuvent le lire et l'exécuter, mais pas le modifier. <br>• Les utilisateurs qui ne sont ni propriétaires ni membres du groupe du fichier ou du répertoire ne peuvent pas le lire, le modifier ou l'exécuter. <br>• En d'autres termes, le propriétaire du fichier ou du répertoire dispose d'un accès complet, le groupe ne peut que le lire et l'exécuter, et les autres n'y ont pas accès. |

    </center>

    Notez que sur notre site, la commande `chmod` est souvent utilisée pour définir les permissions des fichiers ou des répertoires. Contrairement à `umask`, qui définit les *restrictions* par défaut pour les fichiers ou répertoires *nouvellement créés*, la commande `chmod` définit les *permissions* pour les fichiers ou répertoires *existants*. Ainsi, les valeurs entre `umask` et `chmod` en quelque sorte inversées.


<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="Vidage système" width="150px"></img> </center>

## Désactiver les vidages système

Un vidage système (en anglais, « core dump ») est un fichier contenant la mémoire d'un programme lorsque celui-ci plante. C'est utile pour le débogage, mais peut faire fuiter des données sensibles. Il est recommandé de désactiver ces vidages système si vous ne faites pas de débogage. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Sauvegardez les fichiers de configuration :

    ```bash
    sudo cp --archive /etc/security/limits.conf /etc/security/limits.conf-COPY-$(date +"%Y%m%d%H%M%S")

    sudo cp --archive /etc/sysctl.conf /etc/sysctl.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le premier fichier de configuration :

    ```bash
    sudo vi /etc/security/limits.conf
    ```

    Insérez les lignes suivantes à la fin :

    ```bash
    * soft core 0
    * hard core 0
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Ouvrez le second fichier de configuration :

    ```bash
    sudo vi /etc/sysctl.conf
    ```

    Insérez les lignes suivantes à la fin :

    ```bash
    # Disable coredumps
    fs.suid_dumpable=0
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Pour finir, appliquez les changements :

    ```bash
    sudo sysctl -p
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/14ad0c9b-d605-4b2b-813b-2a59ddb1487a" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_cpuinfo.svg" alt="Modules noyaux" width="150px"></img> </center>

## Désactiver les modules noyaux inutilisés

Le noyau Linux est le cœur du système d'exploitation. Il contrôle le matériel périphérique, la mémoire, les tâches et les processus, et ainsi de suite. Les modules noyaux sont des bouts de code qui étendent les fonctionnalités du noyau, par exemple pour accéder à du nouveau matériel. Cela dit, les modules noyaux peuvent également être détournés pour charger des maliciels ou obtenir un accès non autorisé au serveur. Pour éviter cela, interdisez le chargement de modules noyaux non utilisés, comme indiqué ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Sauvegardez le fichier de configuration :

    ```bash
    sudo cp --archive /etc/modprobe.d/blacklist.conf /etc/modprobe.d/blacklist.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/modprobe.d/blacklist.conf
    ```

    Insérez les lignes suivantes à la fin :

    ```bash
    # Instruct modprobe to force inactive modules to always fail loading
    install cramfs /bin/false
    install freevxfs /bin/false
    install hfs /bin/false
    install hfsplus /bin/false
    install jffs2 /bin/false
    install udf /bin/false
    ```

    Sauvegardez et fermez le fichier (`:wq!`).


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/66cc82c1-f44c-41ee-8ae2-8e06066a5bae" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/incident.png" width="550px" alt="Ubuntu server security"></img>
</div>

<br>
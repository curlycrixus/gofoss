---
template: main.html
title: FDroid & Aurora Store | Top Mobile Tracker Free App Lists
description: Alternative app store | Find an Instagram alternative, secure messaging, Twitter alternative, open source pdf editor, Reddit alternative, private browser, etc.
---

# Top 75 Tracker Free FOSS Apps <br> (FDroid & Aurora Store)

!!! level "Last updated: May 2022. For intermediate users. Some tech skills required."

<div align="center">
<img src="../../assets/img/mobile_3.png" alt="Best apps on FDroid" width="500px"></img>
</div>


## Alternative app store

<center>

| App store | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/fdroid.svg" alt="F-Droid review" width="50px"></img> | [F-Droid](https://f-droid.org/) | If you are the proud owner of an Android phone (including [CalyxOS](https://gofoss.net/calyxos/) or [LineageOS](https://gofoss.net/lineageos/)), have a look at F-Droid! It's a Google Play Store alternative exclusively hosting free and open-source applications. Installing and keeping F-Droid up to date should be pretty straight forward. More detailed instructions below. |
| <img src="../../assets/img/aurora.svg" alt="Aurora store apk" width="50px"></img> | [Aurora](https://f-droid.org/en/packages/com.aurora.store/) | Not all useful Android apps are FOSS, tracker-free or available on F-Droid. That doesn't necessarily mean they are bad for your privacy. It also doesn't mean you have to use Google's Play Store to get them. Install the Aurora store apk instead, an alternative app store where you can download, update or search for apps without a Google account. |
| <img src="../../assets/img/fossdroid.svg" alt="Download Android marketplace" width="50px"></img> | [Fossdroid](https://fossdroid.com/) |Android market place similar to F-Droid, promoting free and open source apps on the Android platform. |

</center>


??? tip "Show me the step-by-step guide for F-Droid"

    <center>

    | Steps | Instructions |
    | ------ | ------ |
    | F-Droid Download |Open up your phone's browser, navigate to [F-Droid's website](https://f-droid.org/) and download the `.apk` file. |
    | Allow installation from unknown sources |Open the downloaded `.apk` file. Your phone should prompt something like *"For your security, your phone is not allowed to install unknown apps from this source"*. To temporarily allow installation from unknown sources, click on `Settings ‣ Allow from this source`. |
    | Install F-Droid app | Leave the settings page and click on `Install`. |
    | Forbid installation from unknown sources |Browse to your phone's `Settings` to disable installation from unknown sources. Depending on your phone, this option might be located in `Settings ‣ Apps & notifications ‣ Advanced ‣ Special app access ‣ Install unknown apps`. Select your browser or file manager and disable the option `Allow from this source`.|
    | Launch F-Droid |After the first launch, F-Droid will update the software repository. That's the list of all the software packages available on F-Droid. This update can take a while, be patient. |
    | Enable automatic updates |Navigate to F-Droid's `Settings` page and enable the options `Automatically install updates` as well as `Show available updates`. |
    | Manually update FDroid repo |Back on F-Droid's home screen, swipe down to refresh all repositories.|

    </center>


??? question "Can I install F-Droid or FOSS apps on my iPhone?"

    Owners of iOS devices are out of luck. Apple's walled garden makes it [practically incompatible with mobile FOSS apps](https://www.fsf.org/blogs/community/why-free-software-and-apples-iphone-dont-mix/). Under the pretext of security, Apple's use of [copyright lock](https://www.law.cornell.edu/uscode/text/17/1201) and [access control](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32001L0029:EN:HTML) forecloses any tinkering, jail-breaking, repairing or diagnosis on iOS devices. Apple — not the user — gets to decide what content can be accessed, what apps can be installed and when the device needs to be thrown away.


<br>

<center> <img src="../../assets/img/separator_privacysettings.svg" alt="Best FDroid apps" width="150px"></img> </center>

## FOSS browser

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/firefox.svg" alt="Firefox" width="50px"></img> | [Firefox](https://www.mozilla.org/en-US/firefox/browsers/mobile/) | Fast, secure and private browser. *Caution*: [3 trackers (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/en/reports/org.mozilla.firefox/latest/). |
| <img src="../../assets/img/tor.svg" alt="Tor browser" width="50px"></img> | [Tor browser](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=en_US/) |Tor browser for Android. Blocks trackers, defends against surveillance, resists fingerprinting, encrypts internet traffic. *Caution*: [3 trackers (Adjust, LeanPlum, Google Analytics)](https://reports.exodus-privacy.eu.org/en/reports/org.torproject.torbrowser/latest/). |
| <img src="../../assets/img/fossbrowser.svg" alt="FOSS browser" width="50px"></img> | [FOSS browser](https://f-droid.org/en/packages/de.baumann.browser/) | Simple and light weight browser. Tracker free. |
| <img src="../../assets/img/fennec.svg" alt="Fennec" width="50px"></img> | [Fennec F-Droid](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/) |Privacy-focused version of Firefox. It's focused on removing any proprietary bits found in official Mozilla's builds. *Remark*: Exodus Privacy reports [2 trackers (Adjust, LeanPlum)](https://reports.exodus-privacy.eu.org/en/reports/org.mozilla.fennec_fdroid/latest/). However, according to the development team, this is a [false positive](https://forum.f-droid.org/t/welcome-a-new-fennec-f-droid/11113): the tracker libraries were replaced with "stubs", meaning that they have been replaced with code that does nothing. |
| <img src="../../assets/img/mull.svg" alt="Mull browser" width="50px"></img> | [Mull](https://f-droid.org/packages/us.spotco.fennec_dos/) | Hardened fork of Firefox for Android, developed by the DivestOS team and based on the Tor uplift and arkenfox-user.js projects. Mull comes with tracking protection (per-site data isolation) and fingerprinting protection, supports the content filter uBlock Origin, and receives regular security updates. *Remark*: Exodus Privacy reports [2 trackers (Adjust, Mozilla Telemetry)](https://reports.exodus-privacy.eu.org/en/reports/us.spotco.fennec_dos/latest/). However, according to the development team, this is a [false positive](https://forum.f-droid.org/t/classyshark3exydus-found-five-trackers-inside-tor-browser/13453/20): the tracker libraries were replaced with "stubs", meaning that they have been replaced with code that does nothing. |
| <img src="../../assets/img/bromite.svg" alt="Bromite" width="50px"></img> | [Bromite](https://www.bromite.org/) |Chromium browser, including ad blocking and enhanced privacy. Not available on F-Droid. |
| <img src="../../assets/img/webapps.svg" alt="Webapps" width="50px"></img> | [Webapps](https://f-droid.org/en/packages/com.tobykurien.webapps/) | Turns your favourite mobile websites into secure apps. Tracker free. |

</center>



<br>

<center> <img src="../../assets/img/separator_signal.svg" alt="Messengers" width="150px"></img> </center>

## Secure messaging

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/signal.svg" alt="Signal" width="50px"></img> | [Signal](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en_US/) |Fast, simple and secure messaging. [End-to-end encrypted](https://gofoss.net/encrypted-messages/) text, voice, videos, documents, pictures. Open source, no ads. Tracker free. *Caution*: requires your phone number. |
| <img src="../../assets/img/element.svg" alt="Element" width="50px"></img> | [Element](https://f-droid.org/en/packages/im.vector.app/) | Open source client for the matrix protocol. Matrix is an network for secure, [end-to-end encrypted](https://gofoss.net/encrypted-messages/), decentralised communication. Can be run off [self-hosted servers](https://gofoss.net/ubuntu-server/). Tracker free. |
| <img src="../../assets/img/briar.svg" alt="Briar" width="50px"></img> | [Briar](https://f-droid.org/packages/org.briarproject.briar.android/) | Open source messenger. Peer-to-peer (does not rely on central servers), [end-to-end encrypted](https://gofoss.net/encrypted-messages/), minimal exposure of information. Uses Tor. Tracker free. |
| <img src="../../assets/img/jami.svg" alt="Jami" width="50px"></img> | [Jami](https://f-droid.org/en/packages/cx.ring/) | Open source messenger. Peer-to-peer (does not rely on central servers), [end-to-end encrypted](https://gofoss.net/encrypted-messages/). Tracker free. |
| <img src="../../assets/img/conversations.svg" alt="Conversations" width="50px"></img> | [Conversations](https://f-droid.org/packages/eu.siacs.conversations/) | Open source client for the XMPP protocol. Secure and decentralised communication. Can be run off self-hosted servers. Tracker free. |
| <img src="../../assets/img/jitsi.svg" alt="Jitsi" width="50px"></img> | [Jitsi](https://f-droid.org/en/packages/org.jitsi.meet/) | Video & audio conferencing solution. Open source and encrypted. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Social media" width="150px"></img> </center>

## Alternative social media

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/tusky.svg" alt="Tusky" width="50px"></img> | [Tusky](https://f-droid.org/en/packages/com.keylesspalace.tusky/) | Lightweight client for Mastodon, a free and open-source social network. Twitter alternative or Facebook alternative. Part of the [Fediverse](https://gofoss.net/fediverse). Tracker free. |
| <img src="../../assets/img/pixelfed.svg" alt="Pixeldroid" width="50px"></img> | [Pixeldroid](https://f-droid.org/en/packages/org.pixeldroid.app/) | Client for Pixelfed, a free and open-source Instagram alternative. Part of the [Fediverse](https://gofoss.net/fediverse). Tracker free. |
| <img src="../../assets/img/lemmur.svg" alt="Lemmur" width="50px"></img> | [Lemmur](https://f-droid.org/en/packages/com.krawieck.lemmur/) | Client for Lemmy, a free and open-source Reddit alternative. Part of the [Fediverse](https://gofoss.net/fediverse). Tracker free. |
| <img src="../../assets/img/friendica.svg" alt="Friendica" width="50px"></img> | [Friendica](https://f-droid.org/en/packages/de.wikilab.android.friendica01/) | Client for Friendica, a free and open-source Facebook alternative. Part of the [Fediverse](https://gofoss.net/fediverse). Tracker free. |
| <img src="../../assets/img/simplemusic.svg" alt="SoundCloud alternative" width="50px"></img> | [Funkwhale](https://f-droid.org/en/packages/audio.funkwhale.ffa/) | Client for Funkwhale, a free and open-source Spotify alternative or YouTube alternative. Part of the [Fediverse](https://gofoss.net/fediverse). Tracker free. |
| <img src="../../assets/img/redreader.svg" alt="RedReader" width="50px"></img> | [RedReader](https://f-droid.org/en/packages/org.quantumbadger.redreader/) | FOSS client for reddit.com. No ads. Tracker free. |
| <img src="../../assets/img/infinity.svg" alt="Infinity" width="50px"></img> | [Infinity for Reddit](https://f-droid.org/en/packages/ml.docilealligator.infinityforreddit/) | FOSS client for reddit.com. No ads. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_simpleemail.svg" alt="Email" width="150px"></img> </center>

## Email encryption

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/protonmail.svg" alt="Protonmail" width="50px"></img> | [Protonmail](https://play.google.com/store/apps/details?id=ch.protonmail.android&hl=en_US/) |The world's largest secure email service, developed by CERN and MIT scientists. Open source and protected by Swiss privacy law. Tracker free. |
| <img src="../../assets/img/tutanota.svg" alt="Tutanota" width="50px"></img> | [Tutanota](https://f-droid.org/en/packages/de.tutao.tutanota/) | Open source email application. Dark theme, encryption, instant push notifications, auto-sync, full-text search, swipe gestures, etc. Tracker free. |
| <img src="../../assets/img/k9mail.svg" alt="K-9 mail" width="50px"></img> | [K-9 mail](https://f-droid.org/en/packages/com.fsck.k9/) |Open source email application. Supports POP3 and IMAP, but only supports push mail for IMAP. Tracker free. |
| <img src="../../assets/img/libremmail.svg" alt="Librem mail" width="50px"></img> | [Librem mail](https://f-droid.org/en/packages/one.librem.mail/) | Open source email application. Encryption, multiple accounts, fork of K-9 Mail. Tracker free. |
| <img src="../../assets/img/simpleemail.svg" alt="Simple email" width="50px"></img> | [Simple email](https://f-droid.org/en/packages/org.dystopia.email/) | Open source email application. Privacy friendly, encryption, multiple accounts, two way sync, offline storage, dark theme, search on server, simple design. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_gimp.svg" alt="News" width="150px"></img> </center>

## RSS & online radio

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/feeder.svg" alt="Feeder" width="50px"></img> | [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/) | Open source RSS feed reader. No need for an account, offline reading, background synchronisation, notifications. Tracker free. |
| <img src="../../assets/img/flym.svg" alt="Flym" width="50px"></img> | [Flym](https://f-droid.org/packages/net.frju.flym/) |Open source RSS reader. Tracker free. |
| <img src="../../assets/img/antennapod.svg" alt="AntennaPod" width="50px"></img> | [AntennaPod](https://f-droid.org/en/packages/de.danoeh.antennapod/) |Advanced podcast manager and player. Provides instant access to millions of free and paid podcasts, from independent podcasters to large publishing houses such as the BBC, NPR and CNN. Tracker free. |
| <img src="../../assets/img/radiodroid.svg" alt="RadioDroid" width="50px"></img> | [RadioDroid](https://f-droid.org/en/packages/net.programmierecke.radiodroid2/) |Listen to online radio stations. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_forward.svg" alt="Maps" width="150px"></img> </center>

## Maps & travel

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/osmand.svg" alt="Osmand" width="50px"></img> | [Osmand](https://f-droid.org/en/packages/net.osmand.plus/) |Open source app for online & offline maps and navigation. Tracker free. |
| <img src="../../assets/img/maps.svg" alt="Organic Maps" width="50px"></img> | [Organic maps](https://f-droid.org/en/packages/app.organicmaps/) |Open source app for offline maps and navigation. Maps.me fork. Tracker free. |
| <img src="../../assets/img/transportr.svg" alt="Transportr" width="50px"></img> | [Transportr](https://f-droid.org/packages/de.grobox.liberario/) |Open source app for public transport time schedules in Europe and overseas. *Caution*: [1 tracker (mapbox)](https://reports.exodus-privacy.eu.org/en/reports/de.grobox.liberario/latest/)|
| <img src="../../assets/img/money.svg" alt="SettleUp" width="50px"></img> | [Split it easy](https://f-droid.org/en/packages/com.nishantboro.splititeasy/) |Open source app to manage group expenses. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Multimedia" width="150px"></img> </center>

## Multimedia software

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/opencamera.svg" alt="Open camera" width="50px"></img> | [Open camera](https://f-droid.org/en/packages/net.sourceforge.opencamera/) |Feature rich camera app, including auto-stabilisation, multi-zoom touch, flash, face detection, timer, burst mode, silenceable shutter, and many more. Tracker free. |
| <img src="../../assets/img/simplegallery.svg" alt="Simple gallery" width="50px"></img> | [Simple gallery](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/) |Highly customizable offline gallery, requires no Internet access to protect your privacy. Organise and edit photos, recover deleted files, protect and hide files and view a huge variety of different photo and video formats including RAW, SVG and much more. Tracker free. |
| <img src="../../assets/img/simplemusic.svg" alt="Simple music player" width="50px"></img> | [Simple music player](https://f-droid.org/en/packages/com.simplemobiletools.musicplayer/) |Music player, easily controllable from the status bar, the home screen widget or by hardware buttons on your headset. Fully open source, contains no ads or unnecessary permissions. Fully customizable colors. Tracker free. |
| <img src="../../assets/img/newpipe.svg" alt="Newpipe" width="50px"></img> | [Newpipe](https://f-droid.org/packages/org.schabi.newpipe/) | Lightweight YouTube app, without proprietary API or Google's play services. Also supports PeerTube. Tracker free. |
| <img src="../../assets/img/peertube.svg" alt="Thorium" width="50px"></img> | [Thorium](https://f-droid.org/en/packages/net.schueller.peertube/) |PeerTube is a decentralised video hosting network, based on FOSS. Tracker free. |
| <img src="../../assets/img/totem.svg" alt="Free tube" width="50px"></img> | [Free tube](https://github.com/FreeTubeApp/FreeTube/) |Open source YouTube app for privacy. Not available on F-Droid. |

</center>


<br>

<center> <img src="../../assets/img/separator_simplecalendar.svg" alt="Office tools" width="150px"></img> </center>

## Productivity apps

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/documentviewer.svg" alt="Free pdf editor Android" width="50px"></img> | [Document viewer](https://f-droid.org/en/packages/org.sufficientlysecure.viewer/) |View various file formats, including pdf, djvu, epub, xps and comic books (cbz, fb2). Tracker free. |
| <img src="../../assets/img/libreofficeviewer.svg" alt="Libre Office viewer" width="50px"></img> | [Libre Office viewer](https://f-droid.org/en/packages/org.documentfoundation.libreoffice/) |View docx, doc, xlsx, xls, pptx, ppt, odt, ods and odp files. Tracker free. |
| <img src="../../assets/img/simplenotes.svg" alt="Simple notes" width="50px"></img> | [Simple notes](https://f-droid.org/en/packages/com.simplemobiletools.notes.pro/) |Fully open source offline text editor for Android, without ads or unnecessary permissions. Customizable colors and widget. Tracker free. |
| <img src="../../assets/img/editor.svg" alt="Notepad" width="50px"></img> | [Notepad](https://f-droid.org/packages/com.farmerbb.notepad/) |Simple open source text editor for Android. Tracker free. |
| <img src="../../assets/img/carnet.svg" alt="Text editor Android" width="50px"></img> | [Carnet](https://f-droid.org/packages/com.spisoft.quicknote/) |Powerful open source note taking app, with sync capabilities (incl. NextCloud) and online editor. Tracker free. |
| <img src="../../assets/img/markor.svg" alt="Markor" width="50px"></img> | [Markor](https://f-droid.org/packages/net.gsantner.markor/) |Open source text editor with markdown support. Tracker free. |
| <img src="../../assets/img/joplin.svg" alt="Best text editor for Android" width="50px"></img> | [Joplin](https://play.google.com/store/apps/details?id=net.cozic.joplin) |Open source, encrypted note taking app, with sync capabilities (incl. NextCloud). Tracker free. |
| <img src="../../assets/img/standardnotes.svg" alt="Standard notes" width="50px"></img> | [Standard notes](https://f-droid.org/en/packages/com.standardnotes/) |Free, open source and completely encrypted notes app. Tracker free. |
| <img src="../../assets/img/simplecalendar.svg" alt="Simple calendar" width="50px"></img> | [Simple calendar](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/) |Fully customizable offline calendar to organise single or recurring events, birthdays, anniversaries, business meetings, appointments, etc. Daily, weekly and monthly views available. Fully open source, no ads or unnecessary permissions. Customizable colors. If you [host your own server](https://gofoss.net/intro-free-your-cloud/), this app can be used to [manage and synchronise calendars](https://gofoss.net/contacts-calendars-tasks/). Tracker free. |
| <img src="../../assets/img/doodle.svg" alt="Etar" width="50px"></img> | [Etar](https://f-droid.org/en/packages/ws.xsoh.etar/) |Material designed open source calendar. Works with online calendars. Free, open source and without ads. Tracker free. |
| <img src="../../assets/img/simplecontacts.svg" alt="Simple contacts" width="50px"></img> | [Simple contacts](https://f-droid.org/en/packages/com.simplemobiletools.contacts.pro/) |Simple app for creating or managing contacts. Fully open source, no ads or unnecessary permissions. Customizable colors. Contacts can be stored locally on devices only, or synchronised with the cloud. If you [host your own server](https://gofoss.net/intro-free-your-cloud/), this app can be used to [manage and synchronise contacts](https://gofoss.net/contacts-calendars-tasks/). Tracker free. |
| <img src="../../assets/img/opencontacts.svg" alt="Open contacts" width="50px"></img> | [Open contacts](https://f-droid.org/en/packages/opencontacts.open.com.opencontacts/) |Open source contact app. Tracker free. |
| <img src="../../assets/img/opentasks.svg" alt="Open tasks" width="50px"></img> | [OpenTasks](https://f-droid.org/en/packages/org.dmfs.tasks/) |Open source task manager. If you [host your own server](https://gofoss.net/intro-free-your-cloud/), this app can be used to [manage and synchronise tasks](https://gofoss.net/contacts-calendars-tasks/). Tracker free. |
| <img src="../../assets/img/tasks.svg" alt="Tasks.org" width="50px"></img> | [Tasks.org](https://f-droid.org/en/packages/org.tasks/) |Open source task manager. If you [host your own server](https://gofoss.net/intro-free-your-cloud/), this app can be used to [manage and synchronise tasks](https://gofoss.net/contacts-calendars-tasks/). *Caution*: [1 tracker (OpenTelemetry).](https://reports.exodus-privacy.eu.org/en/reports/org.tasks/latest/) |
| <img src="../../assets/img/davx5.svg" alt="Davx5" width="50px"></img> | [Davx5](https://f-droid.org/en/packages/at.bitfire.davdroid/) | Open source client to synchronise contacts, calendars and task lists. Can be used with a [self-hosted server](https://gofoss.net/contacts-calendars-tasks/) or a trusted hoster. Tracker free. |
| <img src="../../assets/img/simplecalculator.svg" alt="Simple calculator" width="50px"></img> | [Simple calculator](https://f-droid.org/en/packages/com.simplemobiletools.calculator/) |Fully open source calculator, without ads or unnecessary permissions. Customizable colors. Tracker free. |
| <img src="../../assets/img/simpleclock.svg" alt="Simple clock" width="50px"></img> | [Simple clock](https://f-droid.org/en/packages/com.simplemobiletools.clock/) |Clock, alarm, stopwatch, timer. Fully open source, no ads or unnecessary permissions. Customizable colors. Tracker free. |
| <img src="../../assets/img/alarm.svg" alt="Simple alarm clock" width="50px"></img> | [Simple alarm clock](https://play.google.com/store/apps/details?id=com.better.alarm/) |Open source alarm clock with powerful features and clean interface. Tracker free. |
| <img src="../../assets/img/quickdic.svg" alt="Quick dic" width="50px"></img> | [Quick dic](https://f-droid.org/en/packages/de.reimardoeffinger.quickdic/) |Offline dictionary. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_letsencrypt.svg" alt="Open Source Password Manager" width="150px"></img> </center>

## Password manager

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/https.svg" alt="Keepass DX" width="50px"></img> | [Keepass DX](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/) |Secure and open source password manager. Tracker free. |
| <img src="../../assets/img/andotp.svg" alt="andOTP" width="50px"></img> | [andOTP](https://f-droid.org/en/packages/org.shadowice.flocke.andotp/) |Free and open-source application for two-factor authentication. Tracker free. |
| <img src="../../assets/img/freeotp.svg" alt="Free OTP" width="50px"></img> | [Free OTP](https://f-droid.org/en/packages/org.liberty.android.freeotpplus/) | Open source two-factor authenticator. Tracker free. |
| <img src="../../assets/img/aegis.svg" alt="Aegis" width="50px"></img> | [Aegis](https://f-droid.org/en/packages/com.beemdevelopment.aegis/) |Free, secure and open source two-factor authenticator. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_simplefilemanager.svg" alt="File storage" width="150px"></img> </center>

## File storage

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/simplefilemanager.svg" alt="Simple file manager" width="50px"></img> | [Simple file manager](https://f-droid.org/en/packages/com.simplemobiletools.filemanager.pro/) |Browse and edit files. Open source, no ads, customizable colors. Tracker free. |
| <img src="../../assets/img/droidfs.svg" alt="DroidFS" width="50px"></img> | [DroidFS](https://f-droid.org/en/packages/sushi.hardcore.droidfs/) |Store and access your files securely. Open source tool to store files in encrypted volumes. Tracker free. |
| <img src="../../assets/img/seafile.svg" alt="Seafile" width="50px"></img> | [Seafile](https://f-droid.org/en/packages/com.seafile.seadroid2/) |If you [host your own server](https://gofoss.net/intro-free-your-cloud/), this app can be used to [manage and synchronise cloud files](https://gofoss.net/cloud-storage/). Tracker free. |
| <img src="../../assets/img/letsencrypt.svg" alt="Cryptomator" width="50px"></img> | [Cryptomator](https://cryptomator.org/android/) |Encrypt your data before it's uploaded to the cloud. Paid and open source app. |

</center>


<br>

<center> <img src="../../assets/img/separator_openvpn.svg" alt="VPN" width="150px"></img> </center>

## VPN

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/openvpn.svg" alt="Open VPN" width="50px"></img> | [Open VPN](https://f-droid.org/packages/de.blinkt.openvpn/) |If you [host your own server](https://gofoss.net/intro-free-your-cloud/), this app can be used to establish a VPN connection. Tracker free. |
| <img src="../../assets/img/protonvpn.svg" alt="Proton VPN" width="50px"></img> | [Proton VPN](https://f-droid.org/en/packages/ch.protonvpn.android/) |Secure and (partially) free VPN. Claims not to log user activity. Features encryption, Swiss privacy laws, DNS leak protection, kill switch and more. Tracker free. |
| <img src="../../assets/img/mullvad.svg" alt="Mullvad VPN" width="50px"></img> | [Mullvad VPN](https://f-droid.org/en/packages/net.mullvad.mullvadvpn/) |Secure VPN, claims not to log user activity. Subscriptions start at around €5/month. Mullvad accepts anonymous payments and requires no email to sign up. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_games.svg" alt="Keyboards" width="150px"></img> </center>

## Open source keyboard

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/florisboard.svg" alt="Florisboard" width="50px"></img> | [Florisboard](https://f-droid.org/packages/dev.patrickgold.florisboard/) |Open source keyboard, multi-language support, gesture support, night mode, theme support, privacy friendly. Tracker free. |
| <img src="../../assets/img/anysoft.svg" alt="Anysoft keyboard" width="50px"></img> | [Anysoft keyboard](https://f-droid.org/packages/com.menny.android.anysoftkeyboard/) |Open source keyboard, multi-language support, voice input, gesture support, night mode, theme support, privacy friendly. Tracker free. |
| <img src="../../assets/img/simplekeyboard.svg" alt="Simple keyboard" width="50px"></img> | [Simple keyboard](https://f-droid.org/packages/rkr.simplekeyboard.inputmethod/) |Open source keyboard. Tracker free. |
| <img src="../../assets/img/openboard.svg" alt="Open board" width="50px"></img> | [Open board](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/) |Open source keyboard. Tracker free. |
| <img src="../../assets/img/hackerskeyboard.svg" alt="Hacker's keyboard" width="50px"></img> | [Hacker's keyboard](https://f-droid.org/en/packages/org.pocketworkstation.pckeyboard/) |Open source keyboard with separate number keys, punctuation in the usual places, and arrow keys. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_aurora.svg" alt="Other" width="150px"></img> </center>

## Other apps

<center>

| FOSS application | |Description |
| :------: | ------ | ------ |
| <img src="../../assets/img/exodus.svg" alt="Exodus" width="50px"></img> | [Exodus](https://f-droid.org/en/packages/org.eu.exodus_privacy.exodusprivacy/) |Find out which trackers and permissions are embedded in your apps. Tracker free. |
| <img src="../../assets/img/zimlauncher.svg" alt="Zim launcher" width="50px"></img> | [Zim launcher](https://f-droid.org/packages/org.zimmob.zimlx/) |Free, open source launcher app, without ads. Tracker free. |
| <img src="../../assets/img/lawnchair.svg" alt="Lawnchair" width="50px"></img> | [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah) |Free and open source launcher. Customise icon size, labels, rows, columns, icon packs, notifications, and much more. Tracker free. |

</center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="F-Droid best apps" width="150px"></img> </center>

## Support

For further details or questions, refer to:

* [F-Droid's documentation](https://f-droid.org/en/docs/)
* [F-Droid's community](https://forum.f-droid.org/)
* [The Reddit community](https://teddit.net/r/fossdroid/)


<br>

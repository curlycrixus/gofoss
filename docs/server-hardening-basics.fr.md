---
template: main.html
title: Sécurisation basique de votre serveur
description: Auto-hébergement (basique). Sécurisez votre serveur. Qu'est-ce qu'un pare-feu ? Qu'est-ce que NTP ? Logiciels malveillants sur Linux. ClamAV. Qu'est-ce qu'un core dump ?
---

# Sécurisation basique de votre serveur

!!! level "Dernière mise à jour: mai 2022. Destiné aux utilisateurs chevronnés. De solides compétences techniques sont requises"

!!! warning "Avertissement"

    **Aucun système n'est sûr**. Ceci est le premier de [deux chapitres sur la sécurité de serveur](https://gofoss.net/fr/server-hardening-advanced/). Bien qu'il présente des mesures basiques visant à protéger votre [serveur Ubuntu](https://gofoss.net/fr/ubuntu-server/) contre la majorité des menaces immédiates, n'importe quel pirate ou organisme compétent disposant de ressources suffisantes trouvera vraisemblablement un moyen pour accéder à votre système.


## Le pare-feu

<div align="center">
<img src="../../assets/img/firewall.png" alt="Serveur sécurisé" width="500px"></img>
</div>

Un serveur interagit en permanence avec des appareils situés à l'intérieur et à l'extérieur de son réseau. Il doit être protégé par un système de sécurité appelé *pare-feu* (en anglais, « firewall »), qui contrôle tout trafic entrant et sortant. Le « Uncomplicted Firewall » ([ufw](https://help.ubuntu.com/community/UFW/)) est un choix de pare-feu populaire pour les serveurs Ubuntu. Vous trouverez plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Installation

    Installez le « Uncomplicated Firewall », lancez-le automatiquement après chaque redémarrage, et vérifiez qu'il fonctionne (le statut doit être `Active`) :

    ```bash
    sudo apt install ufw
    sudo ufw enable
    sudo systemctl status ufw
    ```

    ### Règles du pare-feu

    Refusez tout trafic entrant et sortant par défaut :

    ```bash
    sudo ufw default deny outgoing
    sudo ufw default deny incoming
    ```

    Assurez-vous que le pare-feu n'autorise que le trafic indispensable. Pour ce faire, ouvrez les points de communication appropriés, également appelés [ports](https://fr.wikipedia.org/wiki/Port_(logiciel)). Dans le cadre de ce tutoriel, nous n'en ouvrirons que quelques-uns. Adaptez ces règles en fonction de vos propres besoins :

    ```bash
    sudo ufw allow 80,443/tcp
    sudo ufw allow 22/tcp
    sudo ufw allow 123/udp
    sudo ufw allow in from any to any port 53
    sudo ufw allow out 80,443/tcp
    sudo ufw allow out 22/tcp
    sudo ufw allow out 123/udp
    sudo ufw allow out from any to any port 53
    ```

    Voici quelques informations supplémentaires sur ces ports :

    <center>

    | Port | Description |
    | ------ | ------ |
    | `80` | Requêtes HTTP : ce port est attribué au protocole de communication Internet le plus utilisé, le protocole de transfert hypertexte (HTTP). Votre serveur peut l'utiliser pour envoyer et recevoir des données sur la toile. |
    | `443` | Requêtes HTTPS : port standard pour tout trafic HTTP sécurisé (HTTPS). Votre serveur peut l'utiliser pour envoyer et recevoir du trafic chiffré. |
    | `22` | Requêtes SSH : ce port est généralement utilisé pour exécuter le protocole « Secure Shell (SSH) ». Votre serveur peut l'utiliser pour les connexions à distance. |
    | `123` | Synchronisation NTP : ce port permet de synchroniser l'heure entre plusieurs ordinateurs. |
    | `53` | Requêtes DNS : ce port est utilisé pour la résolution des noms de domaine. Votre serveur peut l'utiliser pour traduire les noms de domaine en adresses IP. |

    </center>


    Pour finir, vérifiez si les règles du pare-feu sont correctement configurées :

    ```bash
    sudo ufw status numbered
    ```

    Voici à quoi devraient ressembler les règles :

    ```bash
    Status: active

            To                  Action          From
            --                  ------          ----
    [ 1]    80,443/tcp          ALLOW IN        Anywhere
    [ 2]    22/tcp              ALLOW IN        Anywhere
    [ 3]    123/udp             ALLOW IN        Anywhere
    [ 4]    53                  ALLOW IN        Anywhere
    [ 5]    80,443/tcp          ALLOW OUT       Anywhere
    [ 6]    22/tcp              ALLOW OUT       Anywhere
    [ 7]    123/udp             ALLOW OUT       Anywhere
    [ 8]    53                  ALLOW OUT       Anywhere
    [ 9]    80,443/tcp (v6)     ALLOW IN        Anywhere (v6)
    [10]    22/tcp  (v6)        ALLOW IN        Anywhere (v6)
    [11]    123/udp  (v6)       ALLOW IN        Anywhere (v6)
    [12]    53  (v6)            ALLOW IN        Anywhere (v6)
    [13]    80,443/tcp  (v6)    ALLOW OUT       Anywhere (v6)
    [14]    22/tcp  (v6)        ALLOW OUT       Anywhere (v6)
    [15]    123/udp  (v6)       ALLOW OUT       Anywhere (v6)
    [16]    53  (v6)            ALLOW OUT       Anywhere (v6)
    ```

    ### Réglages du routeur

    Vous pouvez également vérifier les réglages de votre routeur et vous assurer que tous les ports inutilisés sont désactivés. Consultez le manuel du routeur pour plus d'informations.


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/16198a6e-e223-441d-9c49-e94726f5ed2a" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ntp.svg" alt="Heure du serveur NTP" width="150px"></img> </center>

## Heure du serveur

De nombreux protocoles de sécurité reposent sur l'heure du système. Une heure incorrecte peut avoir des répercussions négatives en matière de sécurité. Le protocole de temps réseau (en anglais, « Network Time Protocol » ou [NTP](https://fr.wikipedia.org/wiki/Network_Time_Protocol)) permet de s'assurer que l'heure du serveur reste synchronisée avec des ordinateurs de référence. Aussi appelés serveurs NTP, ces ordinateurs sont organisés en couches hiérarchiques, ou strates. Vous trouverez ci-dessous plus de renseignements pour configurer le protocole NTP sur votre serveur.

<div align="center">
<img src="../../assets/img/ntp.png" alt="Heure du serveur NTP" width="500px"></img>
</div>

<center>

| Couche | Description |
| ------ | ------ |
|`Strate 0`| Horloges matérielles, par exemple des horloges atomiques, des GPS ou des horloges radio-pilotées. |
|`Strate 1`| Ordinateurs avec une connexion directe aux horloges matérielles. |
|`Strate 2`| Ordinateurs synchronisés en réseau avec les serveurs de la strate 1. |
|`Clients NTP`| Ordinateurs demandant périodiquement l'heure aux serveurs NTP. |

</center>


??? tip "Montrez-moi le guide étape par étape"

    ### Configuration

    Sauvegardez le fichier de configuration :

    ```bash
    sudo cp --archive /etc/systemd/timesyncd.conf /etc/systemd/timesyncd.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier :

    ```bash
    sudo vi /etc/systemd/timesyncd.conf
    ```

    Modifiez-en le contenu pour accéder aux serveurs NTP publics et open-source exploités par le projet [pool.ntp.org](https://www.ntppool.org/fr/) ou [Ubuntu](https://ubuntu.com/server/docs/network-ntp) :

    ```bash
    [Time]
    NTP=0.pool.ntp.org 1.pool.ntp.org
    FallbackNTP=ntp.ubuntu.com
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Puis, redémarrez le service `systemd-timesyncd` et assurez-vous qu'il fonctionne (le statut devrait être `Active`) :

    ```bash
    sudo systemctl restart systemd-timesyncd
    sudo systemctl status systemd-timesyncd
    ```

    ### Fuseau horaire

    Configurons à présent le [fuseau horaire](https://fr.wikipedia.org/wiki/Tz_database) du serveur :

    ```bash
    timedatectl | grep Time
    ```

    En supposant que vous habitiez près de Budapest, le terminal devrait afficher quelque chose semblable à :

    ```bash
    Time zone: Europe/Budapest (CEST, +0200)
    ```

    Si ce n'est pas le cas, configurez le fuseau horaire correct (ajustez celui-ci en fonction de votre propre localisation) :

    ```bash
    sudo timedatectl set-timezone Europe/Budapest
    ```


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/80abd77b-e973-4333-bb9f-33cd1dc73731" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="SSH sécurisé" width="150px"></img> </center>

## Sécuriser le protocole SSH

Nous avons expliqué dans un chapitre précédent comment [se connecter à distance au serveur depuis un autre ordinateur](https://gofoss.net/fr/ubuntu-server/). Vous trouverez ci-dessous des instructions pour sécuriser davantage cette connexion à distance :

<center>

| Mesure de sécurité | Description |
| ------ | ------ |
|Port SSH |Changez le port SSH de 22 à une autre valeur. Dans ce tutoriel, nous choisirons le port `2222` ; toute autre valeur peut convenir, assurez-vous toutefois d'ajuster les commandes correspondantes en conséquence. |
|Restrictions de connexion |Limitez le nombre de tentatives de connexion. |
|Restrictions d'accès |Autorisez un seul utilisateur à se connecter. Dans notre cas, il s'agit de l'administrateur `gofossadmin` (adaptez le nom de l'administrateur en conséquence). |
|Enregistrement |Gardez la trace de chaque connexion. |
|Restrictions « Root » |Bloquez l'accès SSH distant au compte « root ». |
|Authentification |Utilisez des clés d'authentification protégées par mot de passe plutôt qu'une authentification par mot de passe. |
|Déconnexion |Activez la déconnexion automatique après 5 minutes d'inactivité. |
|Clés de chiffrement |Supprimez les clés de chiffrement courtes pour une sécurité accrue. |
|Notice légale |Ajoutez une notice légale pour avertir les utilisatrices et utilisateurs non habilités. |

</center>

??? tip "Montrez-moi le guide étape par étape"

    ### Port SSH & réglages de sécurité

    Ouvrez le nouveau port SSH 2222 (ajustez en conséquence) :

    ```bash
    sudo ufw allow 2222/tcp
    sudo ufw allow out 2222/tcp
    ```

    Sauvegardez le fichier de configuration SSH. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --preserve /etc/ssh/sshd_config /etc/ssh/sshd_config.$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration SSH :


    ```bash
    sudo vi /etc/ssh/sshd_config
    ```

    Supprimez le contenu du fichier en tapant `:%d`. Puis saisissez ou copiez/collez le contenu suivant, tout en veillant à ajuster les paramètres si nécessaire (par exemple, le port SSH ou le nom d'utilisateur de l'administrateur) :

    ```bash
    # algorithmes HostKey supportés par ordre de préférence :

    HostKey /etc/ssh/ssh_host_ed25519_key
    HostKey /etc/ssh/ssh_host_rsa_key
    HostKey /etc/ssh/ssh_host_ecdsa_key
    KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256
    Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
    MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com

    # divers paramètres de sécurité :

    Port                            2222        # change le port SSH standard 22 (ajuster en conséquence)
    LogLevel                        VERBOSE     # enregistre l'empreinte de la clé de l'utilisateur lors de la connexion afin d'avoir une trace claire de la clé utilisée pour se connecter
    Protocol                        2           # utilise uniquement le nouveau protocole, plus sûr
    PermitUserEnvironment           no          # empêche les utilisateurs de définir des variables d'environnement
    PermitRootLogin                 no          # bloque la connexion de l'utilisateur root
    PubkeyAuthentication            yes         # autorise l'authentification avec une clé publique
    PasswordAuthentication          no          # empêche l'authentification avec un mot de passe
    PermitEmptyPasswords            no          # empêche la connexion si le mot de passe de l'utilisatrice ou de l'utilisateur est vide
    MaxAuthTries                    3           # nombre maximum de tentatives de connexion
    MaxSessions                     2           # nombre maximum de sessions ouvertes
    X11Forwarding                   no          # désactive le transfert X11
    IgnoreRhosts                    yes         # ignore .rhosts et .shosts
    UseDNS                          no          # vérifie que le nom d'hôte correspond à l'IP
    ClientAliveCountMax             0           # nombre maximum de messages « client vivant » envoyés sans réponse
    ClientAliveInterval             300         # délai en secondes avant une demande de réponse
    AllowUsers                      gofossadmin # autorise uniquement l'utilisateur gofossadmin à se connecter à distance (ajustez en conséquence)

    # désactiver le transfert de port :

    AllowAgentForwarding            no
    AllowTcpForwarding              no
    AllowStreamLocalForwarding      no
    GatewayPorts                    no
    PermitTunnel                    no

    # journal des accès aux fichiers sftp (lecture/écriture/etc.) :

    Subsystem sftp  /usr/lib/openssh/sftp-server

    # autres paramètres de sécurité :

    Compression                     no
    PrintMotd                       no
    TCPKeepAlive                    no
    ChallengeResponseAuthentication no
    UsePAM                          yes
    AcceptEnv LANG LC_*
    ```

    Sauvegardez et fermez le fichier de configuration SSH (`:wq!`). Redémarrez le serveur SSH et vérifiez que le nouveau port SSH est défini comme 2222 (ou le port que vous avez choisi) :

    ```bash
    sudo systemctl restart sshd
    sudo ss -tlpn | grep ssh
    ```

    ### Déconnexion de sessions inactives

    Assurez-vous que toute connexion SSH distante est désactivée après 5 minutes d'inactivité :

    ```bash
    echo 'TMOUT=300' >> .bashrc
    tail .bashrc
    ```

    ### Suppression des clés de chiffrement courtes

    SSH utilise l'algorithme [Diffie-Hellman](https://fr.wikipedia.org/wiki/%C3%89change_de_cl%C3%A9s_Diffie-Hellman) pour établir une connexion sécurisée. Pour des raisons de sécurité, il est recommandé d'utiliser des clés d'au moins 3072 bits.

    Sauvegardez le fichier de configuration SSH. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/ssh/moduli /etc/ssh/moduli-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Supprimez les clés plus courtes que 3072 bits :

    ```bash
    sudo awk '$5 >= 3071' /etc/ssh/moduli | sudo tee /etc/ssh/moduli.tmp
    sudo mv /etc/ssh/moduli.tmp /etc/ssh/moduli
    ```

    ### Notice légale

    Un message d'avertissement devrait être affiché à tout utilisateur essayant de se connecter à distance au serveur. Ouvrez le fichier suivant :

    ```bash
    sudo vi /etc/issue.net
    ```

    Ajoutez une notice légale, comme celle-ci par exemple :

    ```bash
    Ce système est réservé aux utilisateurs autorisés.

    Les personnes qui utilisent ce système informatique sans autorisation, ou au-delà de leur autorisation, sont susceptibles de voir toutes leurs activités sur ce système surveillées et enregistrées par le personnel du système.

    Les activités des utilisateurs autorisés peuvent également être contrôlées dans le cadre de la surveillance des personnes qui utilisent ce système de manière inappropriée ou dans le cadre de la maintenance du système.

    Toute personne utilisant ce système consent expressément à une telle surveillance et est informée que si cette surveillance révèle des preuves possibles d'une activité criminelle, le personnel du système peut fournir ces preuves aux autorités policières.
    ```

    ### Vérification & clôture du port 22

    Et voilà ! À partir de maintenant, vous pouvez utiliser en toute sécurité votre ordinateur client pour vous connecter à distance au serveur. Ouvrez simplement un terminal, basculez vers le compte administrateur et connectez-vous au serveur en utilisant votre mot de passe. N'oubliez pas d'ajuster le nom d'utilisateur, l'adresse IP et le port SSH selon votre configuration :

    ```bash
    su - gofossadmin
    ssh -p 2222 gofossadmin@192.168.1.100
    ```

    Essayez-le plusieurs fois. Si tout fonctionne comme prévu, fermez le port 22 inutilisé :

    ```bash
    sudo ufw delete allow 22/tcp
    sudo ufw delete allow out 22/tcp
    ```

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/841da26c-a57a-4be1-be4b-d36d4965dfd0" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_mysql.svg" alt="Securiser MySQL" width="150px"></img> </center>

## Securiser MySQL

Les bases de données MySQL servent de « back-end » à de nombreux services web. Pour protéger les informations conservées par MySQL contre tout accès non autorisé, suivez les instructions ci-dessous.


??? tip "Montrez-moi le guide étape par étape"

    ### Renforcer MySQL

    Lancez MySQL:

    ```bash
    sudo mysql
    ```

    Changez les paramètres d'authentification pour l'utilisateur root. Assurez-vous de remplacer la chaîne `StrongPassword` par un [mot de passe fort et unique](https://gofoss.net/fr/passwords/) :

    ```bash
    ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by 'StrongPassword';
    ```

    Quittez MySQL:

    ```bash
    exit
    ```

    Exécutez maintenant le script de renforcement de MySQL :

    ```bash
    sudo mysql_secure_installation
    ```

    Suivez les instructions affichées à l'écran :

    * pas besoin de changer le mot de passe root, qui vient d'être défini
    * supprimez l'utilisateur anonyme
    * interdisez à l'utilisateur « root » de se connecter à distance
    * supprimez la base de données de test
    * rechargez la table des privilèges


    ### Empêcher tout accès non autorisé

    Sauvegardez le fichier de configuration MySQL. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
    ```

    Désactivez l'accès distant à MySQL et empêchez l'accès non autorisé aux fichiers locaux en ajoutant ou en modifiant les deux lignes suivantes :

    ```bash
    bind-address = 127.0.0.1
    local-infile = 0
    ```

    Sauvegardez et fermez le fichier de configuration MySQL (`:wq!`).


    ### Autorisations des utilisateurs

    Il est recommandé de créer un utilisateur dédié avec des privilèges limités pour chaque application nécessitant d'accéder à MySQL. Par exemple, le [stockage en nuage](https://gofoss.net/fr/cloud-storage/) ou la [photothèque](https://gofoss.net/fr/photo-gallery/) que nous mettrons en place ultérieurement auront chacun leur propre utilisateur MySQL. De cette façon, les applications restent isolées et ne peuvent accéder qu'aux bases de données nécessaires à leur fonctionnement.


??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/9ac7ab65-47f6-459b-ae08-ca98ad2bfd73" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Securiser Apache" width="150px"></img> </center>

## Sécuriser Apache

Apache est le serveur web le plus utilisé au monde. Vous trouverez ci-dessous des instructions pour le configurer correctement et renforcer sa sécurité.

??? tip "Montrez-moi le guide étape par étape"

    ### Activer les modules de sécurité

    Installez les deux [modules Apache](https://en.wikipedia.org/wiki/List_of_Apache_modules) suivants:

    ```bash
    sudo apt install libapache2-mod-security2 libapache2-mod-evasive
    ```

    Activez les modules de sécurité, ainsi que d'autres modules requis pour ce tutoriel, et redémarrez Apache :

    ```bash
    sudo a2enmod security2
    sudo a2enmod evasive
    sudo a2enmod rewrite
    sudo a2enmod headers
    sudo a2enmod ssl
    sudo a2enmod proxy
    sudo a2enmod proxy_html
    sudo a2enmod proxy_http
    sudo a2enmod proxy_wstunnel
    sudo a2enmod xml2enc
    sudo a2enmod expires
    sudo systemctl restart apache2
    ```

    Voici quelques informations supplémentaires sur ces modules :

    <center>

    | Module | Description |
    | ------ | ------ |
    | `Security2` | Protège votre serveur contre de nombreuses attaques, telles que l'[injection SQL](https://fr.wikipedia.org/wiki/Injection_SQL), le [détournement de session](https://en.wikipedia.org/wiki/Session_hijacking), le [« cross-site scripting »](https://fr.wikipedia.org/wiki/Cross-site_scripting), les [agents utilisateurs malveillants](https://fr.wikipedia.org/wiki/User_agent), etc. |
    | `Evasive` | Fournit des actions d'évitement en cas d'[attaque par déni de service](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service) (en anglais, « Denial of Service » ou DoS) ou par déni de service distribué (en anglais, « Distributed Denial of Service » ou DDoS), ou d'[attaque par force brute](https://fr.wikipedia.org/wiki/Attaque_par_force_brute). |
    | `Rewrite` | Permet de réécrire les URL pour activer les redirections, par exemple de `http://` vers `https://`. |
    | `Headers` | Permet de contrôler et de modifier les requêtes HTTP et les en-têtes de réponse. |
    | `SSL` | Active le chiffrement via [SSL et TLS](https://fr.wikipedia.org/wiki/Transport_Layer_Security). |
    | `Proxy, proxy_html, proxy_http` | Crée un proxy/une passerelle pour votre serveur, garantit que les liens fonctionnent pour les utilisateurs en dehors du proxy et transmet les requêtes HTTP et HTTPS. |
    | `Xml2enc` | Fournit un support amélioré pour l'internationalisation. |
    | `Expires` | Améliore le temps de chargement des pages en déterminant la durée de stockage d'une image par le navigateur. |

    </center>

    ### Cacher des informations sensibles

    Par défaut, le serveur envoie des [en-têtes HTTP](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields) contenant des informations sur la version d'Apache, les modules, le système d'exploitation, etc. Ces données peuvent être utilisées pour exploiter des vulnérabilités. Créez un fichier de configuration Apache personnalisé pour masquer ces informations sensibles :

    ```bash
    sudo vi /etc/apache2/conf-available/custom.conf
    ```

    Ajoutez le contenu suivant :

    ```bash
    ServerTokens        Prod
    ServerSignature     Off
    TraceEnable         Off
    Options all -Indexes
    Header always unset X-Powered-By
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Appliquez la configuration personnalisée, vérifiez qu'il n'y a pas d'erreurs de syntaxe (le terminal devrait afficher `OK`) et redémarrez Apache :

    ```bash
    sudo a2enconf custom.conf
    sudo apachectl configtest
    sudo systemctl restart apache2
    ```

    Voici quelques informations supplémentaires sur ces paramètres :

    <center>

    | Paramètre | Description |
    | ------ | ------ |
    | `ServerTokens Prod` |Ne renvoie que « Apache » dans l'en-tête du serveur. |
    | `ServerSignature Off` |Masque la version du serveur sur les pages générées par Apache. |
    | `TraceEnable Off` |Désactive la méthode HTTP TRACE, qui peut exposer votre serveur à des attaques de type [cross-site scripting](https://fr.wikipedia.org/wiki/Cross-site_scripting). |
    | `Options all -Indexes` |Désactive l'affichage de répertoire. |
    | `Header always unset X-Powered-By` |Cache l'information « X-Powered-By ... » et évite d'afficher le logiciel utilisé. |

    </center>


    ### Configurer ModSecurity

    [ModSecurity](https://github.com/SpiderLabs/ModSecurity) est un pare-feu applicatif à code source ouvert. Ce logiciel peut protéger votre serveur contre diverses attaques. Pour cela, commencez par ouvrir le fichier de configuration :

    ```bash
    sudo mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
    sudo vi /etc/modsecurity/modsecurity.conf
    ```

    Modifiez/ajoutez les paramètres suivants :

    ```bash
    SecRuleEngine                   On
    SecResponseBodyAccess           Off
    SecRequestBodyLimit             8388608
    SecRequestBodyNoFilesLimit      131072
    SecRequestBodyInMemoryLimit     262144
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Voici quelques informations supplémentaires sur ces paramètres :

    <center>

    | Paramètre | Description |
    | ------ | ------ |
    | `SecRuleEngine On` |Active ModSecurity en utilisant les règles de base par défaut. |
    | `SecResponseBodyAccess Off` |Désactive la mise en mémoire tampon des requêtes pour utiliser moins de RAM et de CPU. |
    | `SecRequestBodyLimit 8388608` |Définit la taille maximale pour la mise en mémoire tampon des requêtes à 8 Mo. |
    | `SecRequestBodyNoFilesLimit 131072` |Définit la taille maximale pour la mise en mémoire tampon des données à 128 Ko. |
    | `SecRequestBodyInMemoryLimit 262144` |Définit la taille maximale de la requête stockée dans la mémoire vive à 256 Ko. |

    </center>


    Activez le dernier « OWASP ModSecurity Core Rule Set (CRS) », un ensemble de règles génériques de détection d'attaques [maintenu sur GitHub](https://github.com/SpiderLabs/owasp-modsecurity-crs/) :

    ```bash
    sudo rm -rf /usr/share/modsecurity-crs
    sudo apt install git
    sudo git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git /usr/share/modsecurity-crs
    cd /usr/share/modsecurity-crs
    sudo mv crs-setup.conf.example crs-setup.conf
    ```

    Sauvegardez le fichier de configuration ModSecurity. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/apache2/mods-enabled/security2.conf /etc/apache2/mods-enabled/security2.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/apache2/mods-enabled/security2.conf
    ```

    Supprimez son contenu avec la commande `:%d` et ajoutez ou copiez/collez les lignes suivantes :

    ```bash
    <IfModule security2_module>
        SecDataDir /var/cache/modsecurity
        IncludeOptional /etc/modsecurity/*.conf
        IncludeOptional /usr/share/modsecurity-crs/*.conf
        IncludeOptional /usr/share/modsecurity-crs/rules/*.conf
    </IfModule>
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Vérifiez qu'il n'y a pas d'erreurs de syntaxe (le terminal devrait afficher `OK`) et redémarrez Apache pour appliquer la nouvelle configuration :

    ```bash
    sudo apachectl configtest
    sudo systemctl restart apache2
    ```

    Sauvegardez le fichier de configuration ModEvasive. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/apache2/mods-enabled/evasive.conf /etc/apache2/mods-enabled/evasive.conf-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Configurez ModEvasive pour protéger votre serveur contre les [attaques par déni de service](https://fr.wikipedia.org/wiki/Attaque_par_d%C3%A9ni_de_service) (en anglais, « Denial of Service » ou DoS) ou par déni de service distribué (en anglais, « Distributed Denial of Service » ou DDoS), et contre les [attaques par force brute](https://fr.wikipedia.org/wiki/Attaque_par_force_brute).

    Ouvrez le fichier de configuration :

    ```bash
    sudo vi /etc/apache2/mods-enabled/evasive.conf
    ```

    Modifiez ou ajoutez les paramètres suivants :

    ```bash
    <IfModule mod_evasive20.c>
        DOSPageCount        5
        DOSSiteCount        50
        DOSPageInterval     1
        DOSSiteInterval     1
        DOSBlockingPeriod   600
        DOSLogDir           "/var/log/mod_evasive"
    </IfModule>
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Les commandes suivantes créent un répertoire pour les fichiers journaux, vérifient l'absence d'erreurs de syntaxe (le terminal devrait afficher `OK`), redémarrent Apache et vérifient si le statut est `Active` :


    ```bash
    sudo mkdir /var/log/mod_evasive
    sudo chown -R www-data: /var/log/mod_evasive
    sudo apachectl configtest
    sudo systemctl restart apache2
    sudo systemctl status apache2
    ```

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/9b1b3559-e391-45aa-95be-23a5c0ef28ce" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_php.svg" alt="Securiser PHP" width="150px"></img> </center>

## Securiser PHP

PHP est un langage de programmation couramment utilisé pour les services web. Configurez-le correctement pour renforcer la sécurité de votre serveur. Plus de détails ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    ### Version PHP

    [PHP supporte différentes versions](https://www.php.net/supported-versions.php). Au moment d'écrire ces lignes, PHP 8.1 est inclus par défaut dans les dépôts d'Ubuntu 22.04. Vérifiez quelle version est installée sur votre système :

    ```bash
    php -v
    ```

    ### Modules PHP

    Installez certains modules PHP nécessaires pour ce tutoriel, en appliquant le même numéro de version que ci-dessus :

    ```bash
    sudo apt install php8.1-common php8.1-mbstring php8.1-xmlrpc php8.1-gd php8.1-xml php8.1-intl php8.1-mysql php8.1-cli php8.1-ldap php8.1-zip php8.1-curl php8.1-pgsql php8.1-opcache php8.1-sqlite3
    ```

    ### Paramètres de sécurité de PHP

    Les fichiers de configuration `php.ini` contiennent tous les paramètres de sécurité de PHP. Tout d'abord, déterminons où sont stockés ces fichiers :

    ```bash
    sudo find / -name php.ini
    ```

    Selon la version de PHP installée, la commande devrait retourner quelque chose comme :

    ```bash
    /etc/php/8.1/apache2/php.ini
    /etc/php/8.1/cli/php.ini
    ```

    Le fichier `/etc/php/8.1/apache2/php.ini` est utilisé par le serveur Apache, tandis que le fichier `/etc/php/8.1/cli/php.ini` est utilisé par le programme PHP CLI.

    Sauvegardez les deux fichiers de configuration. Si quelque chose devait mal tourner, vous serez toujours en mesure de récupérer les paramètres initiaux :

    ```bash
    sudo cp --archive /etc/php/8.1/apache2/php.ini /etc/php/8.1/apache2/php.ini-COPY-$(date +"%Y%m%d%H%M%S")
    sudo cp --archive /etc/php/8.1/cli/php.ini /etc/php/8.1/cli/php.ini-COPY-$(date +"%Y%m%d%H%M%S")
    ```

    Ouvrez le premier fichier de configuration :

    ```bash
    sudo vi /etc/php/8.1/apache2/php.ini
    ```

    Modifiez ou ajoutez les paramètres suivants :

    ```bash
    expose_php          =   Off
    allow_url_fopen     =   Off
    allow_url_include   =   Off
    display_errors      =   Off
    mail.add_x_header   =   Off
    disable_functions   =   show_source,system,passthru,phpinfo,proc_open,allow_url_fopen,curl_exec
    max_execution_time  =   90
    max_input_time      =   90
    memory_limit        =   1024M
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Ouvrez le second fichier de configuration :

    ```bash
    sudo vi /etc/php/8.1/cli/php.ini
    ```

    Modifiez ou ajoutez les paramètres suivants :

    ```bash
    expose_php = Off
    allow_url_fopen = Off
    ```

    Sauvegardez et fermez le fichier (`:wq!`).

    Voici quelques informations supplémentaires sur ces paramètres :

    <center>

    | Paramètre | Description |
    | ------ | ------ |
    | `expose_php = Off` |Cache la version de PHP dans les en-têtes. |
    | `allow_url_fopen = Off` |Désactive l'exécution de code PHP à distance pour réduire les [vulnérabilités par injection de code](https://fr.wikipedia.org/wiki/Injection_de_code). |
    | `allow_url_include = Off` |Désactive l'exécution de code PHP à distance pour réduire les [vulnérabilités par injection de code](https://fr.wikipedia.org/wiki/Injection_de_code). |
    | `display_errors = Off` |Désactive l'affichage d'erreur. |
    | `mail.add_x_header = Off` |Supprime l'en-tête PHP des courriels. |
    | `disable_functions = show_source, system, passthru, phpinfo, proc_open, allow_url_fopen, curl_exec` |Désactive les fonctions PHP potentiellement dangereuses. Les trois fonctions suivantes n'ont pas été désactivées, car cela [perturbe Pihole v5.2](https://gofoss.net/fr/secure-domain/) : `exec`, `shell_exec`, `popen`. |
    | `max_execution_time = 90` |Définit la durée maximale d'exécution d'un script (en secondes). |
    | `max_input_time = 90` |Définit la durée maximale pendant laquelle un script peut analyser des données (en secondes). |
    | `memory_limit = 1024M` |Définit la quantité maximale de mémoire allouée à un script. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/5ba7da80-c09e-4432-a231-a9823f5cd9a2" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_davx5.svg" alt="Mises à jour de sécurité" width="150px"></img> </center>

## Mises à jour de sécurité

Le serveur doit être mis à jour régulièrement pour réduire les failles de sécurité. Activez les mises à jour automatiques des principaux correctifs de sécurité, comme indiqué ci-dessous.

??? tip "Montrez-moi le guide étape par étape"

    Activez les mises à jour automatiques :

    ```bash
    sudo apt install unattended-upgrades
    ```

    Créez un fichier de configuration :

    ```bash
    sudo vi /etc/apt/apt.conf.d/51myunattended-upgrades
    ```

    Ajoutez le contenu suivant pour configurer les mises à jour non surveillées :

    ```bash
    // Active le script de mise à jour (0=désactiver)
    APT::Periodic::Enable "1";

    // Exécute "apt-get update" automatiquement tous les n-jours (0=désactiver)
    APT::Periodic::Update-Package-Lists "1";

    // Exécute "apt-get upgrade --download-only" tous les n-jours (0=désactiver)
    APT::Periodic::Download-Upgradeable-Packages "1";

    // Exécute "apt-get autoclean" tous les n-jours (0=désactiver)
    APT::Periodic::AutocleanInterval "7";

    // Envoi d'un rapport à « root »
    //     0:  pas de rapport        (ou chaîne de caractères nulle)
    //     1:  rapport d'activité    (toute chaîne de caractères)
    //     2:  + résultats des commandes     (supprime -qq, supprime 2>/dev/null, ajoute -d)
    //     3:  + tracage traçage
    APT::Periodic::Verbose "2";
    APT::Periodic::Unattended-Upgrade "1";

    // Mise à jour automatique des paquets
    Unattended-Upgrade::Origins-Pattern {
        "o=Ubuntu,a=stable";
        "o=Ubuntu,a=stable-updates";
        "origin=Ubuntu,codename=${distro_codename},label=Ubuntu-Security";
    };

    // Spécifiez vos propres paquets à ne pas mettre automatiquement à jour ici
    Unattended-Upgrade::Package-Blacklist {
    };

    // Exécute dpkg --force-confold --configure -a si un état dpkg impropre est détecté pour s'assurer que les mises à jour sont installées même si le système a été interrompu lors d'une exécution précédente
    Unattended-Upgrade::AutoFixInterruptedDpkg "true";

    // Effectue les mises à jour lorsque la machine est en marche, car votre serveur ne sera pas arrêté souvent
    Unattended-Upgrade::InstallOnShutdown "false";

    // Envoie un courriel à l'adresse indiquée avec des informations sur les paquets mis à jour
    Unattended-Upgrade::Mail "root";

    // Toujours envoyer un courriel
    Unattended-Upgrade::MailOnlyOnError "false";

    // Supprime toutes les dépendances inutilisées une fois la mise à jour terminée
    Unattended-Upgrade::Remove-Unused-Dependencies "true";

    // Supprime toutes les nouvelles dépendances inutilisées après la fin de la mise à jour
    Unattended-Upgrade::Remove-New-Unused-Dependencies "true";

    // NE PAS redémarrer automatiquement SANS CONFIRMATION si le fichier /var/run/reboot-required est trouvé après la mise à jour
    Unattended-Upgrade::Automatic-Reboot "false";

    // NE PAS redémarrer automatiquement, même si des utilisateurs sont connectés
    Unattended-Upgrade::Automatic-Reboot-WithUsers "false";
    ```

    Sauvegardez et fermez le fichier (`:wq!`). Effectuez un essai pour vous assurer que tout fonctionne correctement :

    ```bash
    sudo unattended-upgrade -d --dry-run
    ```

??? tip "Montrez-moi une vidéo récapitulative (1min)"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/8a8ad992-8a36-433c-b2b4-17908993a86b" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/server_problem.png" width="550px" alt="Sécurité du serveur"></img>
</div>

<br>
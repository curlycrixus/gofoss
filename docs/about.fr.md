---
template: main.html
title: gofoss.net. Liberté numérique pour tout.e.s
description: La liberté numérique pour tou·tes. Guide libre et ouvert de la vie numérique privée, de la propriété des données et des technologies durables.
---

# Nous croyons en un Internet libre, ouvert et respectueux de la vie privée

<center> <img src="../../assets/img/about.png" alt="gofoss.net" width="400px"></img> </center>

[gofoss.net](https://gofoss.net/fr/) est un guide libre et ouvert sur la vie privée en ligne, la propriété des données et les technologies durables. Ce projet à but non lucratif a été lancé en 2020 par une [petite équipe de bénévoles](https://gofoss.net/fr/team/) – pas de publicité, pas de traçage, pas de contenu sponsorisé, pas d'affiliés.

Nous regrettons qu'un grand nombre d'entreprises de la tech génère du profit aux dépens de la vie privée des internautes. Nous sommes de plus en plus inquiets face aux intrusions répétées des pouvoirs publics dans la sphère privée des citoyennes. Et nous voyons un besoin urgent d'agir pour réduire l'impact environnemental de la technologie.

[La liberté et la vie privée sont des droits humains fondamentaux](https://gofoss.net/fr/nothing-to-hide/). Nous croyons que chacun devrait pouvoir utiliser des appareils numériques sécurisés et respectueux de la vie privée. Naviguer sur Internet, utiliser les réseaux sociaux, partager des médias ou travailler de façon collaborative — nous devrions pouvoir faire tout cela sans que personne n'enregistre, ne monétise ni ne censure les données nous concernant.

<br>
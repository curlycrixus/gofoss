---
template: main.html
title: Comment utiliser le navigateur Tor
description: Comment utiliser le navigateur Tor ? L'utilisation de Tor est-elle illégale ? Tor peut-il être tracé ? Qu'en est-il du navigateur Tor pour Android ?
---

# Restez anonyme en ligne avec Tor

!!! level "Dernière mise à jour: mars 2022. Destiné aux débutants. Aucune compétence technique particulière n'est requise."

<div align="center">
<img src="../../assets/img/tor_connection.png" alt="Navigateur Tor" width="500px"></img>
</div>

Vous avez besoin de rester anonyme en ligne et de dissimuler votre identité face aux fournisseurs d'accès à internet, aux sites web, aux annonceurs ou aux agences de surveillance ? Utilisez le [navigateur Tor](https://www.torproject.org/fr/) !

## Comment fonctionne Tor?

Tor a été lancé en 2002, est basé sur une version modifiée de [Firefox ESR](https://gofoss.net/fr/firefox/) et utilisé par des millions de personnes dans le monde, notamment des journalistes et des militants. Le navigateur Tor achemine le trafic web à travers un réseau mondial composé de milliers d'ordinateurs (appelés « relais »). Étant donné que tout est chiffré et qu'aucun enregistrement n'est conservé, personne n'a la moindre idée de l'origine, de la destination ou du contenu du trafic. La seule chose qu'un site web visité sera en mesure de voir est l'adresse IP du nœud de sortie, c'est-à-dire du dernier relais du réseau. Vous trouverez des instructions plus détaillées ci-dessous.

=== "Windows"

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

         Téléchargez et exécutez le [programme d'installation de Tor pour Windows](https://www.torproject.org/fr/download/).


=== "macOS"

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        Téléchargez le [programme d'installation de Tor pour OS X](https://www.torproject.org/fr/download/). Ouvrez le fichier `.dmg` et faites glisser l'icône du navigateur Tor vers le dossier d'applications. Pour y accéder facilement, ouvrez le dossier d'applications et faites glisser l'icône du navigateur Tor vers le dock.


=== "Linux (Ubuntu)"

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), ouvrez le terminal avec le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche et cherchez `Terminal`. Saisissez la commande suivante pour installer Tor :

        ```bash
        sudo apt install torbrowser-launcher
        ```

        Vérifiez si Tor a été correctement installé en vérifiant le numéro de version :

        ```bash
        tor --version
        ```

        Voilà, vous avez installé Tor ! Pour lancer le navigateur, cliquez sur `Activités` et recherchez `Tor Browser`, ou exécutez la commande `torbrowser-launcher` dans le terminal.


=== "Android"

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        Téléchargez le navigateur Tor depuis le [Google App Store](https://play.google.com/store/apps/details?id=org.torproject.torbrowser&hl=fr/). Vous pouvez également télécharger ou mettre à jour Tor sans devoir utiliser de compte Google. Pour cela, visitez la [page de téléchargement de Tor](https://www.torproject.org/fr/download/) depuis votre appareil Android, tapez sur l'icône `Android` et téléchargez le fichier `.apk`. Tor Browser est également disponible sur [F-Droid](https://support.torproject.org/fr/tormobile/tormobile-7/) ou [Aurora Store](https://auroraoss.com/), deux magasins d'applications alternatifs. Plus d'informations à ce sujet dans [un prochain chapitre](https://gofoss.net/fr/foss-apps/) !


=== "iOS"

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        Téléchargez l'Onion Browser sur l'[App Store](https://apps.apple.com/fr/app/onion-browser/id519296448).

        **Attention** : veuillez noter qu'Apple exige l'utilisation de « Webkit » par les navigateurs sur iOS, ce qui empêche l'Onion Browser d'avoir le même niveau de confidentialité que le navigateur Tor.


<div style="margin-top:-20px">
</div>

??? warning "Tor est génial, mais loin d'être parfait"

    Bien que le navigateur Tor présente de nombreux avantages, il comporte également quelques inconvénients. La navigation est plus lente qu'avec une connexion Internet normale. Certains sites importants bloquent les utilisateurs de Tor. Dans certains pays, Tor est illégal ou bloqué par le gouvernement. De plus, comme pour [VPN](https://gofoss.net/fr/vpn/), utiliser Tor ne signifie pas nécessairement être anonyme ou en sécurité. Votre appareil peut toujours être exposé à des [« attaques de l'homme du milieu »](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu), des attaques par corrélation ou des piratages similaires.


<br>

<center> <img src="../../assets/img/separator_tor.svg" alt="Vérification de l'anonymat de Tor" width="150px"></img> </center>

## Vérification de l'anonymat

Une fois Tor installé, allez sur les sites suivants et vérifiez si des informations identifiantes, comme votre véritable adresse IP, sont divulguées :

* [torproject.org](https://check.torproject.org/)
* [ipleak.net](https://ipleak.net/)
* [coveryourtracks.eff.org](https://coveryourtracks.eff.org/)
* [amiunique.org](https://amiunique.org/)
* [browserleaks.com](https://browserleaks.com/)

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Assistance Tor" width="150px"></img> </center>

## Assistance

Pour davantage de précisions, consultez la [documentation de Tor](https://support.torproject.org/fr/) ou demandez de l'aide à la [communauté Tor sur Reddit](https://teddit.net/r/TOR/).

<div align="center">
<img src="https://imgs.xkcd.com/comics/incognito_mode.png" alt="Navigateur Tor"></img>
</div>

<br>

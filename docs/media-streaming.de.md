---
template: main.html
title: So hostet Ihr Eure Multimediadateien selbst
description: Cloud-Dienste selbst hosten. Wie installiert man Jellyfin? Was ist Jellyfin? Ist Jellyfin besser als Plex? Ist Jellyfin sicher?
---

# Jellyfin, eine selbst gehostete Medien-Streaming-Lösung

!!! level "Letzte Aktualisierung: Mai 2022. Für fortgeschrittene BenutzerInnen. Solide technische Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/jellyfin.png" alt="Jellyfin" width="550px"></img>
</center>

<br>

[Jellyfin](https://jellyfin.org/) ist eine Medien-Streaming-Lösung, die Ihr selbst auf Eurem [Ubuntu-Server](https://gofoss.net/de/ubuntu-server/) hosten könnt. Streamt Filme, Fernsehsendungen und Musik direkt auf Eure Geräte! Jellyfin ist frei, quelloffen, schnell und benutzerfreundlich. Und es unterstützt Mehrbenutzer, Live-TV, Metadaten-Management, Plugins, verschiedene Clients und mehr.

<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Jellyfin Installation" width="150px"></img> </center>

## Installation

Jellyfin ist vergleichsweise einfach zu installieren. Untenstehend findet Ihr eine ausführliche Anleitung für Eure selbst gehostete Medien-Streaming-Lösung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Meldet Euch auf dem Server an und führt die folgenden Befehle aus, um die Paketquelle hinzuzufügen und Jellyfin zu installieren:

    ```bash
    sudo apt install apt-transport-https
    sudo apt-get install software-properties-common
    sudo add-apt-repository universe
    curl -fsSL https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/debian-jellyfin.gpg
    sudo echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/ubuntu $( lsb_release -c -s ) main" | sudo tee /etc/apt/sources.list.d/jellyfin.list
    sudo apt update
    sudo apt install jellyfin
    ```


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/65b649cf-9b0b-4448-8823-0162e51fd9f0" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Übergangslösung für Ubuntu 22.04"

    Zum Zeitpunkt des Verfassens dieser Zeilen kann Jellyfin nicht wie oben beschrieben aus den Ubuntu 22.04-Paketquellen installiert werden. Das liegt vor allem daran, dass Jellyfin 10.7.7 von `libssl1` abhängt, welches [nicht mehr von Ubuntu 22.04 unterstützt wird](https://github.com/jellyfin/jellyfin/pull/7648). Als Übergangslösung könnt Ihr allerdings [Jellyfin 10.8.0 Beta 3](https://github.com/jellyfin/jellyfin/releases/tag/v10.8.0-beta3) installieren.

    Aktiviert die Universe-Paketquellen:

    ```bash
    sudo add-apt-repository universe
    sudo apt update
    ```

    Ladet die Jellyfin `.deb`-Pakete herunter:

    ```bash
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/10.8.0-beta3/web/jellyfin-web_10.8.0~beta3_all.deb
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/10.8.0-beta3/server/jellyfin-server_10.8.0~beta3_amd64.deb
    wget https://repo.jellyfin.org/releases/server/ubuntu/stable-pre/ffmpeg/jellyfin-ffmpeg5_5.0.1-4-jammy_amd64.deb
    ```

    Installiert die erforderlichen Abhängigkeiten:

    ```bash
    sudo apt install at libsqlite3-0 libfontconfig1 libfreetype6
    ```

    Installiert die heruntergeladenen `.deb`-Pakete:


    ```bash
    sudo dpkg -i jellyfin-*.deb
    ```

    Behebt fehlende Abhängigkeiten:

    ```bash
    sudo apt -f install
    ```

    Startet Jellyfin neu und vergewissert Euch, dass der Status `Active` ist:

    ```bash
    sudo systemctl restart jellyfin
    sudo systemctl status jellyfin
    ```


<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="Jellyfin Konfiguration" width="150px"></img> </center>

## Konfiguration

Nach der erfolgreichen Installation von Jellyfin sind einige Einstellungen vorzunehmen: Autostart beim Booten, Benutzerkonten, Spracheinstellungen und so weiter. Untenstehend findet Ihr eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    ### Autostart

    Stellt sicher, dass Jellyfin bei jedem Server-Neustart automatisch ausgeführt wird:

    ```bash
    sudo systemctl daemon-reload
    sudo systemctl enable jellyfin
    ```

    Startet den Server neu:

    ```bash
    sudo reboot
    ```

    Vergewissert Euch, dass Jellyin läuft (der Status sollte "Active" sein):

    ```bash
    sudo systemctl status jellyfin
    ```

    ### Ersteinrichtung

    Wir werden auf Jellyfins Weboberfläche zugreifen, um die Ersteinrichtung durchzuführen. Dazu muss vorübergehend der Port `8096` in der Firewall geöffnet werden:

    ```bash
    sudo ufw allow 8096/tcp
    ```

    Ruft nun `http://192.168.1.100:8096` auf (passt die IP-Adresse entsprechend an) und folgt dem Einrichtungsassistenten:

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Bevorzugte Anzeigesprache |Wählt Eure bevorzugte Sprache, z.B. Deutsch. |
    | Benutzername |Gebt einen Benutzernamen für den Jellyfin-Administrator an. Für die Zwecke dieses Tutorials nennen wir den Administratoren `jellyfinadmin`. Ihr könnt selbstverständlich jeden beliebigen Namen wählen, stellt allerdings sicher, dass Ihr die Befehle entsprechend anpasst. |
    | Passwort |Wählt ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) für den Jellyfin-Administrator. |
    | Medienbibliotheken hinzufügen |Ihr könnt Musik, Filme oder Fernsehsendungen zu Jellyfin hinzufügen, während der Ersteinrichtung oder zu einem späteren Zeitpunkt über das Dashboard. Die Medien sollten dabei direkt auf dem Server gespeichert sein, oder auf einem mit dem Server verbundenen Laufwerk, oder aber auf einem im Betriebssystem eingehängten Netzwerkspeichergerät. |
    | Bevorzugte Metadatensprache auswählen |Wählt Eure bevorzugte Sprache, z.B. Deutsch. |
    | Fernverbindungen zum Jellyfin-Server zulassen |Nein. |

    </center>

    Sobald die Ersteinrichtung abgeschlossen ist, schließt den Port `8096`:

    ```
    sudo ufw delete allow 8096/tcp
    ```


??? tip "Hier geht's zum 2-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/006c7186-00ea-4c0e-b804-6a7b8629f3e0" frameborder="0" allowfullscreen></iframe>
    </center>

<br>

<center> <img src="../../assets/img/separator_apache.svg" alt="Jellyfin Weboberfläche" width="150px"></img> </center>

## Weboberfläche

Als nächstes richten wir einen Apache Virtual Host als Reverse Proxy ein, um auf die Jellyfin-Weboberfläche zuzugreifen. Untenstehend eine ausführliche Anleitung.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Erstellt eine Apache-Konfigurationsdatei:

    ```bash
    sudo vi /etc/apache2/sites-available/mymedia.gofoss.duckdns.org.conf
    ```

    Fügt den folgenden Inhalt hinzu und passt die Einstellungen an Eure eigene Konfiguration an, z. B. den Domain-Namen (`mymedia.gofoss.duckdns.org`), den Pfad zu den SSL-Schlüsseln, die IP-Adressen und so weiter:

    ```bash
    <VirtualHost *:80>

      ServerName mymedia.gofoss.duckdns.org
      ServerAlias www.mymedia.gofoss.duckdns.org
      Redirect permanent / https://mymedia.gofoss.duckdns.org/

    </VirtualHost>

    <VirtualHost *:443>

      ServerName  mymedia.gofoss.duckdns.org
      ServerAlias www.mymedia.gofoss.duckdns.org
      ServerSignature Off

      SecRuleEngine Off
      SSLEngine On
      SSLCertificateFile  /etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem
      SSLCertificateKeyFile /etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem
      DocumentRoot /var/www

      <Location />
        Order deny,allow
        Deny from all
        Allow from 127.0.0.1
        Allow from 192.168.1.0/24
        Allow from 10.8.0.1/24
      </Location>

      ProxyPreserveHost On
      ProxyPass "/socket" "ws://127.0.0.1:8096/socket"
      ProxyPassReverse "/socket" "ws://127.0.0.1:8096/socket"
      ProxyPass "/" "http://127.0.0.1:8096/"
      ProxyPassReverse "/" "http://127.0.0.1:8096/"

      ErrorLog ${APACHE_LOG_DIR}/mymedia.gofoss.duckdns.org-error.log
      CustomLog ${APACHE_LOG_DIR}/mymedia.gofoss.duckdns.org-access.log combined

    </VirtualHost>
    ```

    Speichert und schließt die Datei (`:wq!`).

    Beachtet, dass die SSL-Verschlüsselung für Jellyfin mit der Anweisung `SSLEngine On` aktiviert und das zuvor erstellte SSL-Zertifikat `/etc/dehydrated/certs/gofoss.duckdns.org/fullchain.pem` sowie der private SSL-Schlüssel `/etc/dehydrated/certs/gofoss.duckdns.org/privkey.pem` verwendet wurden.

    Beachtet ebenfalls, dass ModSecurity in der Apache-Konfigurationsdatei mit der Anweisung `SecRuleEngine Off` deaktiviert wurde, da Jellyfin und ModSecurity nicht gut miteinander harmonieren.

    Aktiviert als nächstes den Apache Virtual Host und ladet Apache neu:

    ```bash
    sudo a2ensite mymedia.gofoss.duckdns.org.conf
    sudo systemctl reload apache2
    ```

    Konfiguriert Pi-Hole, um die lokale Adresse von Jellyfin nutzen zu können. Ruft dazu `https://mypihole.gofoss.duckdns.org` auf und meldet Euch über die Pi-Hole-Weboberfläche an (passt die URL entsprechend an). Öffnet den Menüeintrag `Locale DNS Records` und fügt die folgende Domain/IP-Kombination hinzu (passt wiederum die URL sowie IP entsprechend an):

    ```bash
    DOMAIN:      mymedia.gofoss.duckdns.org
    IP ADDRESS:  192.168.1.100
    ```

    Ruft [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) auf und meldet Euch als `jellyfinadmin` an (passt den Benutzernamen entsprechend an).


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/252127f0-8931-4a5e-a987-7f6502e55322" frameborder="0" allowfullscreen></iframe>
    </center>



<br>

<center> <img src="../../assets/img/separator_fdroid.svg" alt="Jellyfin Clients" width="150px"></img> </center>

## Clients

Jellyfin unterstützt verschiedene Clients für Desktop-, TV- oder mobile Geräte. Wählt Euren bevorzugten Client und folgt den Installationsanweisungen auf [Jellyfins Webseite](https://jellyfin.org/clients/).


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Jellyfin BenutzerInnen anlegen" width="150px"></img> </center>

## BenutzerInnen anlegen

Jellyfin unterscheidet zwischen [zwei Nutzertypen](https://jellyfin.org/docs/general/server/users/index.html):

* **Administratoren** haben Vollzugriff und können Medien und Nutzer hinzufügen, bearbeiten und entfernen. Darüber hinaus können sie Jellyfin warten und aktualisieren. In unserem Beispiel wurde der Administrator `jellyfinadmin` während des Installationsprozesses angelegt. Weitere Administratoren können hinzugefügt werden.

* **BenutzerInnen** haben nur begrenzten Zugang zu Jellyfin. Nutzereigenschaften sind individuell konfigurierbar.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Anweisungen | Beschreibung |
    | ------ | ------ |
    | Anmelden | Ruft [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) auf und meldet Euch als Administrator an, z.B. `jellyfinadmin` (passt den Admin-Namen entsprechend an). |
    | Einstellungen | Sucht den Eintrag `Menü ‣ Übersicht ‣ Benutzer ‣ Benutzer hinzufügen` auf. |
    | BenutzerInnen hinzufügen | Gebt einen Benutzernamen sowie ein [sicheres, individuelles Passwort](https://gofoss.net/de/passwords/) an. Klickt anschließend auf `Speichern`. Legt zugleich fest, auf welche Bibliotheken die BenutzerInnen zugreifen können. |
    | Nutzerrechte festlegen | Optional können weitere Einstellungen festgelegt werden, wie z.B.:<br>• Administratoren-Rechte <br>• Live TV Zugriff <br>• Transkodierung <br>• Eingeschränkte Internetgeschwindigkeit <br>• Erlaubnis, Medien zu löschen <br>• Fernsteuerung <br>• Kontosperrung <br>• Zugriff auf Bibliotheken und Geräte <br>• Kindersicherung <br>• Pin-Code|

    </center>


??? tip "Hier geht's zum 1-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/ba6374ef-d524-43e7-97eb-d1bc81d6f6cb" frameborder="0" allowfullscreen></iframe>
    </center>


??? warning "Administratoren & BenutzerInnen benötigen einen VPN-Zugang"

    BenutzerInnen müssen über [VPN](https://gofoss.net/de/secure-domain/) mit dem Server verbunden sein, um auf Jellyfin zugreifen zu können.


<br>

<center> <img src="../../assets/img/separator_openshot.svg" alt="Jellyfin Untertitel" width="150px"></img> </center>

## Untertitel

Jellyfin unterstützt sowohl [eingebettete als auch externe Untertitel](https://jellyfin.org/docs/general/server/media/external-files.html). Mit dem *Open Subtitle Plugin* können Untertitel automatisch heruntergeladen werden. Wie man externe Untertitel zu Jellyfin hinzufügt, wird im Folgenden beschrieben.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    <center>

    | Instructions | Description |
    | ------ | ------ |
    | Anmelden | Ruft [https://mymedia.gofoss.duckdns.org](https://mymedia.gofoss.duckdns.org/) auf und meldet Euch als Administrator an, z.B. `jellyfinadmin` (passt den Admin-Namen entsprechend an). |
    | Plugin installieren | Sucht den Eintrag `Menü ‣ Übersicht ‣ Plugins ‣ Katalog ‣ Open Subtitles` auf und klickt auf `Installieren`. |
    | Neustarten | Sucht den Eintrag `Menü ‣ Übersicht` auf und klickt auf `Neustarten`. |
    | Plugin konfigurieren | Das *Open Subtitle Plugin* benötigt grundsätzlich kein Konto, um Untertitel herunterladen. Falls dennoch erwünscht, könnt Ihr optional auf [https://www.opensubtitles.org](https://www.opensubtitles.org/en/search/subs) ein Konto einrichten. Ruft in diesem Fall anschließend den Eintrag `Menü ‣ Übersicht ‣ Plugins ‣ Open Subtitles` auf und gebt Eure Open Subtitle-Benutzerdaten an. |
    | Einstellungen | Sucht den Eintrag `Menü ‣ Bibliotheken ‣ Filme oder Serien ‣ Bibliothek verwalten` auf und legt weitere Einstellungen im Abschnitt `Untertitel Downloads` fest:<br><br>• Herunterzuladende Sprachen <br>• Untertitel Downloader <br>• Download je nach Dateiname oder Ton überspringen <br>• Speicherort für Untertitel |
    | Untertitel herunterladen | Jellyfin lädt von selbst regelmäßig fehlende Untertitel herunter. Um manuell einen Download zu starten, ruft den Eintrag `Menü ‣ Übersicht ‣ Geplante Aufgaben` auf und klickt auf `Fehlende Untertitel herunterladen`. |

    </center>


??? tip "Hier geht's zum 3-minütigen Zusammenfassungsvideo"

    <center>
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://libre.video/videos/embed/3d82e2cb-9bd7-41b4-866b-a1db979b584a" frameborder="0" allowfullscreen></iframe>
    </center>


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="Jellyfin Upgrade" width="150px"></img> </center>

## Upgrade

Upgrades auf aktuelle Jellyfin-Versionen werden automatisch durch den Paketmanager des Ubuntu-Servers durchgeführt. Um ein manuelles Update vorzunehmen könnt Ihr den untenstehenden Anweisungen folgen.

??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

    Meldet Euch auf dem Server an und führt den folgenden Befehl aus:

    ```bash
    sudo apt update && sudo apt upgrade
    ```

    Startet anschließend Jellyfin neu und vergewissert Euch, dass alles richtig läuft (der Status sollte "Active" sein):

    ```bash
    sudo systemctl restart jellyfin
    sudo systemctl status jellyfin
    ```

<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="Jellyfin Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten findet Ihr in der [Jellyfin Dokumentation](https://jellyfin.org/docs/). Oder bittet die [Jellyfin-Gemeinschaft](https://jellyfin.org/docs/general/getting-help.html) um Hilfe.


<br>
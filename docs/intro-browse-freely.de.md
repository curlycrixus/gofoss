---
template: main.html
title: Surft datenschutzfreundlich im Internet
description: Firefox vs. Chrome. Ist Firefox sicherer als Google? Wie kann ich Firefox installieren? Was ist mit Firefox auf Android? Was ist ein gehärteter Firefox?
---

# Das Netz ist unendlich weit

<center>
<html>
<embed src="../../assets/echarts/browser_stats.html" style="width: 100%; height:520px">
</html>
</center>

In diesem Kapitel erklären wir Euch, wie Ihr auf datenschutzfreundlichere Weise im Internet surfen könnt:

* mit [Firefox](https://gofoss.net/de/firefox/)
* mit den [Tor Browser](https://gofoss.net/de/tor/)
* mit einem [VPN](https://gofoss.net/de/vpn/)

Der Browser ist das Tor zum Internet. Er ist so allgegenwärtig geworden, dass wir seine weit reichende Bedeutung übersehen.

Die meisten von uns verlassen sich ganz unbekauf Google Chrome, um im Internet zu surfen. Andere auf Microsofts Edge oder Apples Safari. All diese Browser sind mangelhaft in Bezug auf Datenschutz. Sie lassen sich unendlich viele Möglichkeiten einfallen, um unsere Daten zu sammeln: Seitenbesuche, Inhaltsansichten, Suchverlauf, Benutzernamen und Kennwörter, Standorte, Sprachaufzeichnungen, Einkäufe und so weiter.

Es gibt einige Datenschutz-Maßnahmen für Chrome, Edge und Safari. Aber letztlich sind diese Browser darauf ausgelegt, das Online-Verhalten der NutzerInnen zu erfassen und in Profit umzuwandeln.

<center>
<a href="https://contrachrome.com/comic/279/"><img align="center" src="../../assets/img/contrachrome_de.png" alt="Contra Chrome"></img></a>
</center>

<br>

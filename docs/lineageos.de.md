---
template: main.html
title: So installiert Ihr LineageOS für microG
description: Degooglet Eure Android-Telefone. Wie installiert man LineageOS? Ist LineageOS legal? Was ist microG? Ist microG legal? LineageOS vs. CalyxOS? LineageOS vs. GrapheneOS?
---

# LineageOS für microG, ein beliebtes mobiles Betriebssystem

!!! level "Letzte Aktualisierung: März 2022. Für erfahrene BenutzerInnen. Technische Kenntnisse erforderlich."

<center>
<img align="center" src="../../assets/img/lineageosmicrog.png" alt="LineageOS für microG" width ="600px"></img>
</center>


Solltet Ihr kein Pixel-Handy besitzen, könnte [LineageOS für microG](https://lineage.microg.org) sich als Alternative zu [CalyxOS](https://gofoss.net/de/calyxos/) eignen. Das Projekt ist ein inoffizieller Fork der freien und quelloffenen Android-Distribution [LineageOS](https://lineageos.org/). Es baut auf [microG](https://microg.org/) auf, das einige von Googles proprietären Bibliotheken durch freien und quelloffenen Code ersetzt. LineageOS für microG unterstützt derzeit Hunderte von Telefonmodellen. Stellt sicher, dass *Euer Handymodell* in der [LineageOS Geräteliste](https://wiki.lineageos.org/devices/) aufgeführt ist.


!!! warning "Ein paar Anmerkungen zu Kompatibilität und Sicherheit"

    Bitte seid vorsichtig, wenn Ihr LineageOS für microG auf Euer Telefon aufspielt. Dadurch erlischt die Garantie Eures Geräts, das im schlimmsten Fall sogar unbrauchbar werden kann. Ihr solltet Euch daher des Risikos für eventuelle Schäden oder Datenverluste bewusst sein.

    Bevor Ihr auf LineageOS für microG umsteigt, bedenkt bitte das einige Apps [nicht voll funktionsfähig](https://github.com/microg/GmsCore/wiki/Implementation-Status) sind — auch wenn sonst alles in allem hervorragend funktioniert. Darunter befinden sich ein paar Apps von Google, wie z. B. Android Wear, Google Fit, Google Cast oder Android Auto. Zum Glück gibt es dafür großartige [FOSS-Alternativen](https://gofoss.net/de/foss-apps/). Bedenkt schließlich auch, dass die Verwendung von kostenpflichtigen Apps ohne Googles Play Store ein bisschen aufwendiger sein kann.

    Die Installation von LineageOS für microG erfordert das Entsperren des sogenannten Bootloaders. Damit wird die Sicherheit Eures Telefons herabgesetzt. [Ein Angreifer mit physischem Zugriff auf Euer Telefon könnte möglicherweise Software installieren (Passwort-Logger usw.), ohne dieses vorher zu entschlüsseln](https://en.wikipedia.org/wiki/Evil_Maid_attack). Zudem ist LineageOS für microG in der Vergangenheit mit Sicherheitsupdates in Verzug geraten. Wir empfehlen, regelmäßig auf Updates zu prügen. Proprietäre Komponenten wie Bootloader oder Firmware werden für bestimmte Handys gar nicht mehr aktualisiert. Bewertet diese potenziellen Sicherheitslücken im Hinblick auf Euer [Bedrohungsmodell](https://ssd.eff.org/en/module/your-security-plan/).


<br>

<center> <img src="../../assets/img/separator_backups.svg" alt="LineageOS Sicherungskopie" width="150px"></img> </center>

## Sicherungskopien

Während des Installationsvorgangs werden alle Daten auf Eurem Gerät gelöscht. Geht kein Risiko ein, [legt Sicherheitskopien an](https://gofoss.net/de/backups/)!

<br>

<center> <img src="../../assets/img/separator_bug.svg" alt="LineageOS debugging" width="150px"></img> </center>

## USB-Debugging

USB-Debugging muss aktiviert sein, damit die Android Debug Bridge (ADB) Euer Telefon mit dem Computer verbinden kann. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet das Menü `Einstellungen ► Über das Telefon`. |
        | 2 | Tippt siebenmal auf `Build-Nummer`, um die Entwickleroptionen zu aktivieren. |
        | 3 | Öffnet das Menü `Einstellungen ► System ► Erweiterte Einstellungen ► Entwickleroptionen`. |
        | 4 | Scrollt nach unten, und aktiviert das Kontrollkästchen `Android-Debugging` oder `USB-Debugging`. |
        | 5 | Schließt Euer Android-Gerät an den Windows Computer an. |
        | 6 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

        ### Auf Eurem Windows Computer

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | [Ladet ADB für Windows herunter](https://dl.google.com/android/repository/platform-tools-latest-windows.zip/). |
        | 2 | Entpackt den Inhalt der `.zip`-Datei. |
        | 3 | Öffnet den Windows Explorer und sucht den Ordner, der die extrahierten ADB-Dateien enthält. |
        | 4 | Führt einen Rechtsklick innerhalb des Ordners aus und wählt den Eintrag `Eingabeaufforderung hier öffnen` oder `PowerShell-Fenster hier öffnen`. |
        | 5 | Führt den folgenden Befehl aus, um den ADB-Daemon zu starten:<br> `adb devices` |
        | 6 | Ein Dialogfeld sollte auf Eurem Android-Telefon erscheinen und Euch dazu auffordern, USB-Debugging zuzulassen. Aktiviert das Kontrollkästchen `Immer erlauben` und klickt auf `OK`. |



=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet das Menü `Einstellungen ► Über das Telefon`. |
        | 2 | Tippt siebenmal auf `Build-Nummer`, um die Entwickleroptionen zu aktivieren. |
        | 3 | Öffnet das Menü `Einstellungen ► System ► Erweiterte Einstellungen ► Entwickleroptionen`. |
        | 4 | Scrollt nach unten, und aktiviert das Kontrollkästchen `Android-Debugging` oder `USB-Debugging`. |
        | 5 | Schließt Euer Android-Gerät an das macOS Gerät an. |
        | 6 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

        ### Auf Eurem macOS Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | [Ladet ADB für macOS herunter](https://dl.google.com/android/repository/platform-tools-latest-darwin.zip/). |
        | 2 | Entpackt den Inhalt der `.zip`-Datei. |
        | 3 | Öffnet das Terminal und sucht den Ordner, der die extrahierten ADB-Dateien enthält. |
        | 4 | Führt den folgenden Befehl aus, um den ADB-Dämon zu starten:<br> `adb devices` |
        | 5 | Ein Dialogfeld sollte auf Eurem Android-Telefon erscheinen und Euch dazu auffordern, USB-Debugging zuzulassen. Aktiviert das Kontrollkästchen `Immer erlauben` und klickt auf `OK`. |


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet das Menü `Einstellungen ► Über das Telefon`. |
        | 2 | Tippt siebenmal auf `Build-Nummer`, um die Entwickleroptionen zu aktivieren. |
        | 3 | Öffnet das Menü `Einstellungen ► System ► Erweiterte Einstellungen ► Entwickleroptionen`. |
        | 4 | Scrollt nach unten, und aktiviert das Kontrollkästchen `Android-Debugging` oder `USB-Debugging`. |
        | 5 | Schließt Euer Android-Gerät an das Linux (Ubuntu) Gerät an. |
        | 6 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

        ### Auf Eurem Linux (Ubuntu) Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`. |
        | 2 | Führt die folgenden Befehle aus, um die Systempakete zu aktualisieren:<br> `sudo apt update` <br> `sudo apt upgrade` |
        | 3 | Führt die folgenden Befehle aus, um ADB Fastboot zu installieren:<br> `sudo apt install android-tools-adb` <br> `sudo apt install android-tools-fastboot`|
        | 4 | Führt den folgenden Befehl aus, um den ADB-Server zu starten (falls dieser noch nicht automatisch gestartet wurde):<br> `sudo adb start-server`|
        | 5 | Führt den folgenden Befehl aus, um den ADB-Dämon zu starten:<br> `adb devices` |
        | 6 | Ein Dialogfeld sollte auf Eurem Android-Telefon erscheinen und Euch dazu auffordern, USB-Debugging zuzulassen. Aktiviert das Kontrollkästchen `Immer erlauben` und klickt auf `OK`. |


<br>

<center> <img src="../../assets/img/separator_ssh.svg" alt="LineageOS Bootloader" width="150px"></img> </center>

## Bootloader entsperren

Der Bootloader ist Teil des Codes, der beim Einschalten Eures Telefons als erstes ausgeführt wird. Viele Hersteller sperren den Bootloader, um jegliche Veränderung am Telefon zu unterbinden. Der Bootloader muss daher entsperrt werden, um LineageOS für microG zu installieren. *Dies ist der heikelste Teil dieser Anleitung*, da der Prozess von Telefon zu Telefon unterschiedlich ist und ein wenig Nachforschung erfordert:

* Schaut Euch das [LineageOS Wiki](https://wiki.lineageos.org/devices/) an. Sucht nach *Eurem Handymodell*, begebt Euch in die Rubrik `Installationsanweisungen` und lest das Kapitel `Bootloader entsperren`.
* Fall Ihr zusätzliche Unterstützung benötigt, fragt einfach die [LineageOS-Gemeinschaft](https://lineageos.org/community/) oder die [LineageOS-Reddit-Gemeinschaft](https://teddit.net/r/LineageOS/).


<br>

<center> <img src="../../assets/img/separator_twrp.svg" alt="LineageOS TWRP" width="150px"></img> </center>

## Custom Recovery

Android-Telefone werden mit einer von Google bereitgestellten Wiederherstellungssoftware ausgeliefert, die auch *Stock Recovery* genannt wird. Damit können Werkseinstellungen wiederhergestellt oder das Betriebssystem aktualisiert werden. Um LineageOS für microG zu installieren, muss diese Stock Recovery durch eine sogenannte *Custom Recovery* ersetzt werden.

Das [Team Win Recovery Project](https://twrp.me/) (TWRP) ist ein beliebtes Custom Recovery. TWRP ermöglicht nicht nur die Installation von LineageOS für microG, sondern auch die Erstellung von Sicherungskopien sowie die Änderung von Systemeinstellungen. Untenstehend findet Ihr eine ausführliche Anleitung.


=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Schließt Euer Android-Gerät an den Windows Computer an. |
        | 2 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

        ### Auf Eurem Windows Computer

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Ruft die [TWRP Webseite](https://twrp.me/Devices/) auf, wählt das richtige Telefonmodell und ladet die entsprechende TWRP-Version herunter. Ihm Rahmen dieses Tutorials nehmen wir an, dass die Datei unter `/Downloads/twrp-x.x.x.img` heruntergeladen wurde. |
        | 2 | Öffnet den Windows Explorer und sucht den Ordner, der die heruntergeladene `img`-Datei enthält. |
        | 3 | Führt einen Rechtsklick innerhalb des Ordners aus und wählt den Eintrag `Eingabeaufforderung hier öffnen` oder `PowerShell-Fenster hier öffnen`. |
        | 4 | Führt den folgenden Befehl aus, um das Telefon im *Bootloader*-Modus zu starten:<br> `adb reboot bootloader` <br><br> Alternativ könnt Ihr das Telefon im *Bootloader*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke RUNTER` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Wort `FASTBOOT` auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://wiki.lineageos.org/devices). |
        | 5 | Führt den folgenden Befehl aus, um sicherzustellen, dass der Computer das Telefon erkennt:<br> `fastboot devices`|
        | 6 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br> `$ 0077fxe89p12 fastboot`|
        | 7 | Führt die folgenden Befehle aus, um den Ordner mit der Custom Recovery Datei aufzurufen (passt den Befehl entsprechend an) und die Custom Recovery auf Euer Telefon zu flashen: <br>`cd /Downloads` <br>`fastboot flash recovery twrp-x.x.x.img` |
        | 8 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 9 | Führt den folgenden Befehl aus, um direkt in TWRP zu booten:<br> `fastboot boot recovery twrp-x.x.x.img`|


=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Schließt Euer Android-Gerät an das macOS Gerät an. |
        | 2 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

        ### Auf Eurem macOS Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Ruft die [TWRP Webseite](https://twrp.me/Devices/) auf, wählt das richtige Telefonmodell und ladet die entsprechende TWRP-Version herunter. Ihm Rahmen dieses Tutorials nehmen wir an, dass die Datei unter `/Downloads/twrp-x.x.x.img` heruntergeladen wurde. |
        | 2 | Öffnet das Terminal. |
        | 3 | Führt den folgenden Befehl aus, um das Telefon im *Bootloader*-Modus zu starten:<br> `adb reboot bootloader` <br><br> Alternativ könnt Ihr das Telefon im *Bootloader*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke RUNTER` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Wort `FASTBOOT` auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://wiki.lineageos.org/devices). |
        | 4 | Führt den folgenden Befehl aus, um sicherzustellen, dass der Computer das Telefon erkennt:<br> `sudo fastboot devices`|
        | 5 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br> `$ 0077fxe89p12 fastboot`|
        | 6 | Führt die folgenden Befehle aus, um den Ordner mit der Custom Recovery Datei aufzurufen (passt den Befehl entsprechend an) und die Custom Recovery auf Euer Telefon zu flashen: <br>`cd /Downloads` <br>`sudo fastboot flash recovery twrp-x.x.x.img` |
        | 7 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 8 | Führt den folgenden Befehl aus, um direkt in TWRP zu booten<br> `sudo fastboot boot recovery twrp-x.x.x.img`|


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Schließt Euer Android-Gerät an das Linux (Ubuntu) Gerät an. |
        | 2 | Ändert den USB-Modus auf `Dateiübertragung (MTP)`. |

        ### Auf Eurem Linux (Ubuntu) Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Ruft die [TWRP Webseite](https://twrp.me/Devices/) auf, wählt das richtige Telefonmodell und ladet die entsprechende TWRP-Version herunter. Ihm Rahmen dieses Tutorials nehmen wir an, dass die Datei unter `/home/gofoss/Downloads/twrp-x.x.x.img` heruntergeladen wurde. |
        | 2 | Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`. |
        | 3 | Führt den folgenden Befehl aus, um das Telefon im *Bootloader*-Modus zu starten:<br> `adb reboot bootloader` <br><br> Alternativ könnt Ihr das Telefon im *Bootloader*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke RUNTER` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Wort `FASTBOOT` auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://wiki.lineageos.org/devices). |
        | 4 | Führt den folgenden Befehl aus, um sicherzustellen, dass der Computer das Telefon erkennt:<br> `sudo fastboot devices` |
        | 5 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br> `$ 0077fxe89p12 fastboot`|
        | 6 | Führt die folgenden Befehle aus, um den Ordner mit der Custom Recovery Datei aufzurufen (passt den Befehl entsprechend an) und die Custom Recovery auf Euer Telefon zu flashen: <br>`cd /home/gofoss/Downloads` <br>`sudo fastboot flash recovery twrp-x.x.x.img` |
        | 7 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br> `$ sending ‘recovery’ (8450 KB)…` <br>`$ OKAY [ 0.730s]` <br>`$ writing ‘recovery’…` <br>`$ OKAY [ 0.528s]` <br>`$ finished. total time: 1.258s`|
        | 8 | Führt den folgenden Befehl aus, um direkt in TWRP zu booten<br> `sudo fastboot boot recovery twrp-x.x.x.img`|



<br>

<center> <img src="../../assets/img/separator_settings.svg" alt="LineageOS Installation" width="150px"></img> </center>

## Installation

LineageOS für microG kann installiert werden, sobald das Telefon erfolgreich in die Custom Recovery (TWRP) gebootet hat. Untenstehend findet Ihr eine ausführliche Anleitung.

=== "Windows"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Windows"

        ### Auf Eurem Windows Computer

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | [Findet auf der LineageOS-Webseite heraus, welcher Codename mit Eurem Telefonmodell verbunden wird](https://wiki.lineageos.org/devices/). Der Codename des Fairphones ist z.B. "FP2", das Nexus 5X nennt sich "bullhead", das Samsung Galaxy S9 "starlte", und so weiter. |
        | 2 | [Sucht auf der LineageOS für microG Webseite nach demselben Codenamen](https://download.lineage.microg.org/). Klickt auf den entsprechenden Ordner und ladet die neueste Version von LineageOS für microG herunter. Im Rahmen dieses Tutorials nehmen wir an, dass die Datei in den Ordner `/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip` heruntergeladen wurde. |
        | 3 | Öffnet den Windows Explorer und sucht den Ordner, der die heruntergeladene `zip`-Datei enthält. |
        | 4 | Führt einen Rechtsklick innerhalb des Ordners aus und wählt den Eintrag `Eingabeaufforderung hier öffnen` oder `PowerShell-Fenster hier öffnen`. |
        | 5 | Führt den folgenden Befehl aus, um das Telefon im *Recovery*-Modus zu starten:<br> `adb reboot recovery`<br><br> Alternativ könnt Ihr das Telefon im *Recovery*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke HOCH` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Logo auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | TWRP Custom Recovery sollte jetzt geladen werden. Wischt über den Schieberegler, um den Hauptbildschirm aufzurufen. |
        | 2 | Tippt auf die `Wipe`-, und dann auf die `Format Data`-Schaltflächen. *Vorsicht!* Dies entfernt die Verschlüsselung und löscht alle Dateien vom Telefon. [Stellt sicher, dass Ihr eine Sicherungskopie Eurer Daten erstellt habt](https://gofoss.net/de/backups/)! |
        | 3 | Kehrt zum vorherigen Menü zurück, und tippt auf die `Wipe`- sowie `Advanced Wipe`-Schaltflächen. |
        | 4 | Wählt die Einträge `Cache`, `System` und `Data` aus und wischt über den Schieberegler am unteren Bildschirmrand. |

        ### Auf Eurem Windows Computer

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Führt den folgenden Befehl aus, um sicherzustellen, dass der Computer das Telefon erkennt:<br> `fastboot devices` |
        | 2 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br><br>`$ 0077fxe89p12 fastboot`|
        | 3 | Führt die folgende Befehle aus, um den Ordner mit der heruntergeladenen LineageOS für microG Datei aufzurufen (passt den Befehl entsprechend an) und die `.zip`-Datei in den internen Speicher Eures Telefons zu laden:<br> `cd /Downloads`<br>`adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Alternativ könnt Ihr die `.zip`-Datei auch manuell mit dem Dateimanager von Eurem Computer auf Euer Telefon kopieren. |
        | 4 | Führt den folgenden Befehl aus, um das Telefon im *Recovery*-Modus zu starten:<br> `adb reboot recovery`<br><br> Alternativ könnt Ihr das Telefon im *Recovery*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke HOCH` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Logo auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Ruft TWRPs Hauptfenster auf und tippt auf die `Installieren`-Schaltfläche. Wählt anschließend die `lineage-XX.X-202XXXXX-microG-CODENAME.zip` Datei aus. |
        | 2 | Wischt über den Schieberegler `Swipe to confirm Flash`, wartet bis LineageOS für microG installiert ist und startet das Telefon neu (diesmal reicht ein "normaler" Neustart, kein Neustart im "Bootloader"- oder "Recovery"-Modus). |
        | 3 | Etwas Geduld, der erste Bootvorgang kann eine Weile dauern. Herzlichen Glückwunsch, LineageOS für microG ist nun erfolgreich auf Eurem Telefon installiert! |



=== "macOS"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für macOS"

        ### Auf Eurem macOS Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | [Findet auf der LineageOS-Webseite heraus, welcher Codename mit Eurem Telefonmodell verbunden wird](https://wiki.lineageos.org/devices/). Der Codename des Fairphones ist z.B. "FP2", das Nexus 5X nennt sich "bullhead", das Samsung Galaxy S9 "starlte", und so weiter. |
        | 2 | [Sucht auf der LineageOS für microG Webseite nach demselben Codenamen](https://download.lineage.microg.org/). Klickt auf den entsprechenden Ordner und ladet die neueste Version von LineageOS für microG herunter. Im Rahmen dieses Tutorials nehmen wir an, dass die Datei in den Ordner `/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip` heruntergeladen wurde. |
        | 3 | Öffnet das Terminal. |
        | 4 | Führt den folgenden Befehl aus, um das Telefon im *Recovery*-Modus zu starten:<br> `adb reboot recovery`<br><br> Alternativ könnt Ihr das Telefon im *Recovery*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke HOCH` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Logo auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | TWRP Custom Recovery sollte jetzt geladen werden. Wischt über den Schieberegler, um den Hauptbildschirm aufzurufen. |
        | 2 | Tippt auf die `Wipe`-, und dann auf die `Format Data`-Schaltflächen. *Vorsicht!* Dies entfernt die Verschlüsselung und löscht alle Dateien vom Telefon. [Stellt sicher, dass Ihr eine Sicherungskopie Eurer Daten erstellt habt](https://gofoss.net/de/backups/)! |
        | 3 | Kehrt zum vorherigen Menü zurück, und tippt auf die `Wipe`- sowie `Advanced Wipe`-Schaltflächen. |
        | 4 | Wählt die Einträge `Cache`, `System` und `Data` aus und wischt über den Schieberegler am unteren Bildschirmrand. |

        ### Auf Eurem macOS Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Führt den folgenden Befehl aus, um sicherzustellen, dass der Computer das Telefon erkennt:<br> `sudo fastboot devices` |
        | 2 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br>`$ 0077fxe89p12 fastboot`|
        | 3 | Führt die folgenden Befehle aus, um den Ordner mit der heruntergeladenen LineageOS für microG Datei aufzurufen (passt den Befehl entsprechend an) und die `.zip`-Datei in den internen Speicher Eures Telefons zu laden:<br> `cd /Downloads`<br>`adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Alternativ könnt Ihr die `.zip`-Datei auch manuell mit dem Dateimanager von Eurem Computer auf Euer Telefon kopieren.|
        | 4 | Führt den folgenden Befehl aus, um das Telefon im *Recovery*-Modus zu starten:<br> `adb reboot recovery`<br><br> Alternativ könnt Ihr das Telefon im *Recovery*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke HOCH` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Logo auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Ruft TWRPs Hauptfenster auf und tippt auf die `Installieren`-Schaltfläche. Wählt anschließend die `lineage-XX.X-202XXXXX-microG-CODENAME.zip` Datei aus. |
        | 2 | Wischt über den Schieberegler `Swipe to confirm Flash`, wartet bis LineageOS für microG installiert ist und startet das Telefon neu (diesmal reicht ein "normaler" Neustart, kein Neustart im "Bootloader"- oder "Recovery"-Modus) |
        | 3 | Etwas Geduld, der erste Bootvorgang kann eine Weile dauern. Herzlichen Glückwunsch, LineageOS für microG ist nun erfolgreich auf Eurem Telefon installiert! |


=== "Linux (Ubuntu)"

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung für Linux (Ubuntu)"

        ### Auf Eurem Linux (Ubuntu) Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | [Findet auf der LineageOS-Webseite heraus, welcher Codename mit Eurem Telefonmodell verbunden wird](https://wiki.lineageos.org/devices/). Der Codename des Fairphones ist z.B. "FP2", das Nexus 5X nennt sich "bullhead", das Samsung Galaxy S9 "starlte", und so weiter. |
        | 2 | [Sucht auf der LineageOS für microG Webseite nachh demselben Codenamen](https://download.lineage.microg.org/). Klickt auf den entsprechenden Ordner und ladet die neueste Version von LineageOS für microG herunter. Im Rahmen dieses Tutorials nehmen wir an, dass die Datei in den Ordner `/home/gofoss/Downloads/lineage-XX.X-202XXXXX-microG-CODENAME.zip` heruntergeladen wurde. |
        | 3 | Öffnet das Terminal mit der Tastenkombination `STRG + ALT + T`, oder klickt auf die Schaltfläche `Aktivitäten` in der Menüleiste und sucht nach `Terminal`. |
        | 4 | Führt den folgenden Befehl aus, um das Telefon im *Recovery*-Modus zu starten:<br> `adb reboot recovery`<br><br> Alternativ könnt Ihr das Telefon im *Recovery*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke HOCH` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Logo auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://www.xda-developers.com/how-to-boot-to-recovery/). |


        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | TWRP Custom Recovery sollte jetzt geladen werden. Wischt über den Schieberegler, um den Hauptbildschirm aufzurufen. |
        | 2 | Tippt auf die `Wipe`-, und dann auf die `Format Data`-Schaltflächen. *Vorsicht!* Dies entfernt die Verschlüsselung und löscht alle Dateien vom Telefon. [Stellt sicher, dass Ihr eine Sicherungskopie Eurer Daten erstellt habt](https://gofoss.net/de/backups/)! |
        | 3 | Kehrt zum vorherigen Menü zurück, und tippt auf die `Wipe`- sowie `Advanced Wipe`-Schaltflächen. |
        | 4 | Wählt die Einträge `Cache`, `System` und `Data` aus und wischt über den Schieberegler am unteren Bildschirmrand. |

        ### Auf Eurem Linux (Ubuntu) Gerät

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Führt den folgenden Befehl aus, um sicherzustellen, dass der Computer das Telefon erkennt:<br> `sudo fastboot devices` |
        | 2 | Das Terminal sollte in etwa folgende Meldung anzeigen:<br>`$ 0077fxe89p12 fastboot`|
        | 3 | ührt die folgende Befehle aus, um den Ordner mit der heruntergeladenen LineageOS für microG Datei aufzurufen (passt den Befehl entsprechend an) und die `.zip`-Datei in den internen Speicher Eures Telefons zu laden:<br> `cd /home/gofoss/Downloads`<br>`adb push lineage-XX.X-202XXXXX-microG-CODENAME.zip /sdcard/`<br><br>Alternativ könnt Ihr die `.zip`-Datei auch manuell mit dem Dateimanager von Eurem Computer auf Euer Telefon kopieren. |
        | 4 | Führt den folgenden Befehl aus, um das Telefon im *Recovery*-Modus zu starten:<br> `adb reboot recovery`<br><br> Alternativ könnt Ihr das Telefon im *Recovery*-Modus starten, indem Ihr es ausschaltet und dann die beide Tasten `Lautstärke HOCH` und `EINSCHALTEN` gedrückt haltet. Lasst die Tasten los, sobald das Logo auf dem Bildschirm erscheint. Die Tastenkombination kann sich von Gerät zu Gerät unterscheiden. Weitere Informationen findet Ihr [hier](https://www.xda-developers.com/how-to-boot-to-recovery/). |

        ### Auf Eurem Android-Handy

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Ruft TWRPs Hauptfenster auf und tippt auf die `Installieren`-Schaltfläche. Wählt anschließend die `lineage-XX.X-202XXXXX-microG-CODENAME.zip` Datei aus. |
        | 2 | Wischt über den Schieberegler `Swipe to confirm Flash`, wartet bis LineageOS für microG installiert ist und startet das Telefon neu (diesmal reicht ein "normaler" Neustart, kein Neustart im "Bootloader"- oder "Recovery"-Modus) |
        | 3 | Etwas Geduld, der erste Bootvorgang kann eine Weile dauern. Herzlichen Glückwunsch, LineageOS für microG ist nun erfolgreich auf Eurem Telefon installiert! |



<br>

<center> <img src="../../assets/img/separator_microg.svg" alt="LineageOS für microG" width="150px"></img> </center>

## microG

LineageOS für microG wird logischerweise mit... [microG](https://microg.org/) ausgeliefert. MicroG ersetzt einige von Googles proprietären Bibliotheken und Anwendungen mit quelloffenen Code. Durch die Aktivierung von microG können Funktionen wie Push-Nachrichten oder Standortbestimmung genutzt werden, ohne das datenhungrige Google Services Framework (GSF) zu installieren. Viele GSF-abhängige Apps können auf diese Weise mit LineageOS für microG verwendet werden. Es gilt jedoch zu beachten, dass diese Apps noch immer in der Lage sind, Google-Dienste zu kontaktieren. Um Datenlecks weitestgehend einzuschränken, empfehlen wir GSF-unabhängige, [freie und quelloffene Apps](https://gofoss.net/de/foss-apps/) zu bevorzugen.


=== "Standortbestimmung"

    Folgt den unten aufgeführten Anweisungen, um Euren Telefon-Standort mit Hilfe einer lokalen Datenbank mit Funkmast-Informationen zu ermitteln und somit völlig unabhängig von Drittanbietern zu sein.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet F-Droid und installiert die App namens `Lokaler GSM-Standort`. |
        | 2 | Ruft die microG-Einstellung-App auf. Wählt den Menüeintrag `Standortdienste` und aktiviert `GSM Standortdienst` sowie `Nominatim`. |
        | 3 | Wählt den Menüeintrag `Selbstkontrolle` und überprüft, ob alles richtig eingerichtet ist. |

        </center>


=== "Push-Nachrichten"

    LineageOS für microG vermeidet die Nutzung von Googles Diensten soweit möglich. Einzige Ausnahme: Viele Apps verlassen sich auf Google Cloud Messaging (GCM), ein von Google entwickeltes proprietäres System, um Push-Nachrichten an Euer Gerät zu senden. microG kann den Zugriff auf Push-Nachrichten ermöglichen, indem es die (eingeschränkte) Nutzung des GCM-Dienstes aktiviert. Untenstehend findet Ihr weitere Details zum Thema.


    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        | 1 | Öffnet die microG-Einstellung-App. |
        | 2 | Wählt den Menüeintrag `Google Geräte-Registrierung` und registriert Euer Gerät. |
        | 3 | Wählt den Menüeintrag `Google Cloud Messaging` und aktiviert Push-Nachrichten. |

        </center>

    ??? question "Kann ich LineageOS für microG vertrauen, obwohl es Google Cloud Messaging verwendet?"

        Die Registrierung Eures Geräts und die Aktivierung von Push-Nachrichten ist optional. Dadurch werden möglicherweise begrenzt Daten an Google übermittelt, wie z. B. eine eindeutige ID. microG stellt jedoch sicher, dass so viele identifizierende Informationen wie möglich entfernt werden. Die Aktivierung von Push-Nachrichten kann Google auch ermöglichen, den Inhalt Eurer Nachrichten (teilweise) zu lesen, je nachdem, wie Apps Google Cloud Messaging verwenden.


=== "F-Droid"

    [F-Droid](https://f-droid.org/de/) ist ein App-Store, der ausschließlich freie und quelloffene Anwendungen anbietet, wie in einem [vorherigen Kapitel über FOSS-Apps](https://gofoss.net/de/foss-apps/) beschrieben. Stellt sicher, dass Ihr die microG-Paketquelle in F-Droid aktiviert. Folgt dazu den untenstehenden Anweisungen.

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        |1 |Startet F-Droid. |
        |2 |Wählt den Menüeintrag `Einstellungen ► Paketquellen`. |
        |3 |Aktiviert die microG-Paketquelle. |

        </center>


=== "Kostenpflichtige Apps"

    Die Einrichtung von kostenpflichtigen Apps ohne Googles Play Store kann ein wenig knifflig sein. Hier ein Lösungsansatz:

    ??? tip "Hier geht's zur Schritt-für-Schritt-Anleitung"

        <center>

        | Schritte | Beschreibung |
        | :------: | ------ |
        |1 |Stöbert durch [Googles Online-Playstore](https://play.google.com/store?hl=de). |
        |2 |Kauft Apps mit einem ausgedienten oder wegwerfbaren Google-Konto. |
        |3 |Meldet Euch bei [Aurora Store](https://f-droid.org/de/packages/com.aurora.store/) mit demselben Google-Konto an.|
        |4 |Ladet die erworbenen Apps herunter. |

        </center>


<br>

<center> <img src="../../assets/img/separator_permissions.svg" alt="LineageOS Unterstützung" width="150px"></img> </center>

## Unterstützung

Weitere Einzelheiten oder Antworten findet Ihr in der [LineageOS für microG Dokumentation](https://lineage.microg.org/) oder bei der [Lineage-Gemeinschaft](https://lineageos.org/community/).

<div align="center">
<img src="https://imgs.xkcd.com/comics/xkcd_phone_2.png" alt="LineageOS"></img>
</div>

<br>

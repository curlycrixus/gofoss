---
template: main.html
title: 75 datenschutzfreundliche Cloud-Dienste
description: Datenschutzfreundliche Cloud-Anbieter. Alternative Cloud-Anbieter. Framasoft, Chatons, Disroot, Riseup, Systemausfall, Picasoft, Digitalcourage.
---

# 75 datenschutzfreundliche Cloud-Dienste

!!! level "Letzte Aktualisierung: April 2022. Anfängerfreundlich. Keine technischen Kenntnisse erforderlich."

<div align="center">
<img src="../../assets/img/cloud_storage_2.png" alt="Alternative Cloud-Anbieter" width="550px"></img>
</div>

## Alternative Cloud-Anbieter

=== "Framasoft"

    <img src="../../assets/img/framasoft.png" alt="Framasoft" width="80px" align="left"></img> [Framasoft](https://framasoft.org/de/) ist ein gemeinnütziger Verein, der 2001 in Frankreich gegründet wurde. Framasoft fördert FOSS, [bietet zahlreiche Online-Dienste](https://degooglisons-internet.org/de/list/) an und [empfiehlt datenschutzfreundliche Software](https://degooglisons-internet.org/de/alternatives/). [Personenbezogene Nutzerdaten, die von Framasoft erfasst werden, unterliegen französischem Recht](https://framasoft.org/de/legals/). Die Framasoft-Server werden von Deutschland aus betrieben.

=== "Kitten"

    <img src="../../assets/img/chatons.png" alt="Kätzchen" width="70px" align="left"></img> [Die Kätzchen (auf französisch "Chatons")](https://www.chatons.org/en/) sind ein französisches Kollektiv alternativer Cloud-Anbieter. Sie fördern transparente, offene, neutrale und nachhaltige [Online-Dienste](https://www.chatons.org/en/search/by-service/). [Personenbezogene Nutzerdaten, die von den Kätzchen erfasst werden, unterliegen französischem Recht](https://www.chatons.org/mentions-legales/). Die Kätzchen-Server werden von Frankreich und Deutschland aus betrieben.


=== "Disroot"

    <img src="../../assets/img/disroot.png" alt="Disroot" width="70px" align="left"></img> [Disroot](https://disroot.org/de) ist ein ehrenamtliches Projekt, das 2015 in den Niederlanden gegründet wurde. Disroot [bietet zahlreiche, meist kostenfreie Online-Dienste an](https://disroot.org/de/#services). Eine eigene [Android app](https://f-droid.org/de/packages/org.disroot.disrootapp/) ermöglicht zudem einen mobilen Zugriff auf die meisten Online-Anwendungen. [Disroot gibt an, DSGVO-konform zu handeln](https://disroot.org/de/privacy_policy). Die Disroot-Server werden von den Niederlanden aus betrieben.


=== "Digitalcourage"

    <img src="../../assets/img/digitalcourage.png" alt="Digitalcourage" width="70px" align="left"></img> [Digitalcourage](https://digitalcourage.de) ist ein deutscher Verein, der 1987 gegründet wurde. Er [bietet zahlreiche, meist kostenfreie Online-Dienste an](https://digitalcourage.de/swarm-support). [Digitalcourage gibt an, DSGVO-konform zu handeln](https://digitalcourage.de/datenschutz-bei-digitalcourage). Die Server werden von dem in Irland ansässigen Unternehmen [freistil.it](https://www.freistil.it/) betrieben.


=== "RiseUp"

    <img src="../../assets/img/riseup.png" alt="RiseUp" width="80px" align="left"></img> [RiseUp](https://riseup.net/de) ist ein ehrenamtliches Kollektiv, das 1999 in Seattle gegründet wurde. Es bietet verschiedene Online-Dienste und Computerressourcen an. [RiseUp veröffentlicht eine transparente Datenschutzrichtlinie](https://riseup.net/de/privacy-policy).

=== "Systemausfall"

    <img src="../../assets/img/systemausfall.png" alt="Systemausfall" width="178px" align="left"></img> [Systemausfall](https://systemausfall.org/) ist ein ehrenamtliches Kollektiv, das 2003 in Deutschland gegründet wurde. Es bietet verschiedene Online-Dienste und Computerressourcen an. [Systemausfall veröffentlicht eine transparente Datenschutzrichtlinie](https://systemausfall.org/wikis/hilfe/Datensicherheit/).

=== "Picasoft"

    <img src="../../assets/img/picasoft.png" alt="Picasoft" width="70px" align="left"></img> [Picasoft](https://picasoft.net/) ist ein gemeinnütziger Verein der Technischen Universität Compiègne, der 2016 in Frankreich gegründet wurde. Picasoft fördert eine freie, integrative und datenschutzfreundliche Lebensart. Der Verein bietet unter anderem zahlreiche Online-Dienste an. [Personenbezogene Nutzerdaten, die von Picasoft erfasst werden, unterliegen französischem Recht](https://picasoft.net/co/cgu.html). Die Picasoft-Server werden von [Frankreich](https://wiki.picasoft.net/doku.php?id=technique:resume) aus betrieben.




## Office-Paket

=== "Text-Editor"

    <img src="../../assets/img/editor.svg" alt="Online-Texteditor" width="50px" align="left"></img> Ersetzt Google Docs, Word365 und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framapad](https://framapad.org/abc/de/) |Gemeinschaftlicher Online-Texteditor, basiert auf [etherpad](https://etherpad.org/)|
    |[Disroot pad](https://pad.disroot.org/) |Gemeinschaftlicher Online-Texteditor, basiert auf [etherpad](https://etherpad.org/) |
    |[Disroot cryptpad](https://cryptpad.disroot.org/pad/) |Gemeinschaftlicher und verschlüsselter Online-Texteditor, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage cryptpad](https://cryptpad.digitalcourage.de/) |Gemeinschaftlicher und verschlüsselter Online-Texteditor, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Riseup pad](https://pad.riseup.net/)|Gemeinschaftlicher Online-Texteditor, basiert auf [etherpad](https://etherpad.org/) |
    |[Picapad](https://pad.picasoft.net/)|Gemeinschaftlicher Online-Texteditor, basiert auf [etherpad](https://etherpad.org/)|
    |[Zaclys cloud docs](https://www.zaclys.com/cloud-2/) |Gemeinschaftlicher Online-Texteditor, basiert auf [nextcloud](https://nextcloud.com/de/) & [onlyoffice](https://www.onlyoffice.com/de/) |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=135&title=&field_software_target_id=All&field_is_shared_value=All) |Zahlreiche gemeinschaftliche Online-Texteditoren, basierend auf [cryptpad](https://cryptpad.fr), [etherpad](https://etherpad.org/) oder [hedgedoc](https://hedgedoc.org/) |


=== "Tabellen-Editor"

    <img src="../../assets/img/spreadsheet.svg" alt="Online-Tabellenkalkulation" width="50px" align="left"></img> Ersetzt Google Spreadsheet, Excel365 und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framacalc](https://framacalc.org/abc/de/) |Gemeinschaftlicher Online-Tabelleneditor, basiert auf [ethercalc](https://github.com/audreyt/ethercalc/) |
    |[Disroot calc](https://calc.disroot.org/) |Gemeinschaftlicher Online-Tabelleneditor, basiert auf [ethercalc](https://github.com/audreyt/ethercalc/) |
    |[Disroot sheet](https://cryptpad.disroot.org/sheet/) |Gemeinschaftlicher und verschlüsselter Online-Tabelleneditor, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage sheet](https://cryptpad.digitalcourage.de/) |Gemeinschaftlicher und verschlüsselter Online-Tabelleneditor, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Zaclys cloud sheets](https://www.zaclys.com/cloud-2/) |Gemeinschaftlicher Online-Tabelleneditor, basiert auf [nextcloud](https://nextcloud.com/de/) & [onlyoffice](https://www.onlyoffice.com/de/) |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=136&title=&field_software_target_id=All&field_is_shared_value=All) |Zahlreiche gemeinschaftliche Online-Tabelleneditoren, basierend auf [ethercalc](https://github.com/audreyt/ethercalc/) |


=== "Folien-Editor"

    <img src="../../assets/img/slide.svg" alt="Online-Folien" width="50px" align="left"></img> Ersetzt Google Slides, Powerpoint365 und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Disroot slides](https://cryptpad.disroot.org/slide/) |Gemeinschaftlicher und verschlüsselter Online-Folieneditor, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage cryptpad](https://cryptpad.digitalcourage.de/) |Gemeinschaftlicher und verschlüsselter Online-Folieneditor, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Zaclys cloud slides](https://www.zaclys.com/cloud-2/) |Gemeinschaftlicher Online-Folieneditor, basiert auf [nextcloud](https://nextcloud.com/de/) & [onlyoffice](https://www.onlyoffice.com/de/) |


=== "Kalender"

    <img src="../../assets/img/simplecalendar.svg" alt="Online-Kalender" width="50px" align="left"></img> Ersetzt Google Calendar, Outlook365 und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framagenda](https://framagenda.org/login/) |Gemeinschaftlicher Online-Kalender, basiert auf [nextcloud](https://nextcloud.com/de/) |
    |[Cryptpad calendar](https://cryptpad.fr/calendar/) |Gemeinschaftlicher und verschlüsselter Online-Kalender, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Zaclys cloud calendar](https://www.zaclys.com/cloud-2/)|Gemeinschaftlicher Online-Kalender, basiert auf [nextcloud](https://nextcloud.com/de/) |


=== "Terminplanung"

    <img src="../../assets/img/doodle.svg" alt="Terminplanung" width="50px" align="left"></img> Ersetzt Doodle und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framadate](https://framadate.org/abc/en/) |Online-Dienst zur Terminplanung und Entscheidungsfindung, basiert auf [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Disroot poll](https://poll.disroot.org/) |Online-Dienst zur Terminplanung und Entscheidungsfindung, basiert auf [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Digitalcourage poll](https://nuudel.digitalcourage.de/) |Online-Dienst zur Terminplanung und Entscheidungsfindung, basiert auf [studs](https://sourcesup.cru.fr/projects/studs/) |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=139&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Online-Dienste zur Terminplanung und Entscheidungsfindung, basierend auf [studs](https://sourcesup.cru.fr/projects/studs/) |


## Datenspeicherung und -synchronisierung

=== "Cloud-Laufwerk"

    <img src="../../assets/img/mysql.svg" alt="Cloud-Laufwerk" width="50px" align="left"></img> Ersetzt Google Drive, Dropbox, iCloud, OneDrive und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Disroot cloud](https://cloud.disroot.org/) |Cloud-Speicherlösung, basiert auf [nextcloud](https://nextcloud.com/de/) |
    |[Systemausfall drive](https://speicher.systemausfall.org/accounts/login/?next=) |Cloud-Speicherlösung, basiert auf [seafile](https://www.seafile.com/en/home/) |
    |[Zaclys cloud](https://www.zaclys.com/cloud-2/) |Cloud-Speicherlösung, basiert auf [nextcloud](https://nextcloud.com/de/) |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=147&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Cloud-Speicherlösungen, basierend auf [nextcloud](https://nextcloud.com/de/) oder [seafile](https://www.seafile.com/en/home/)|
    |[Disroot cryptdrive](https://cryptpad.disroot.org/drive/)|Verschlüsselte Cloud-Speicherlösung, basiert auf [cryptpad](https://cryptpad.fr) |
    |[Digitalcourage cryptdrive](https://cryptpad.digitalcourage.de/drive/)|Verschlüsselte Cloud-Speicherlösung, basiert auf [cryptpad](https://cryptpad.fr) |


=== "Datenaustausch"

    <img src="../../assets/img/davx5.svg" alt="Datenaustausch" width="50px" align="left"></img> Ersetzt WeTransfer und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Disroot upload](https://upload.disroot.org/) |Verschlüsselter Datenaustausch-Dienst, basiert auf [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[Riseup share](https://share.riseup.net/) |Datenaustausch-Dienst, basiert auf [up1](https://github.com/Upload/Up1/)|
    |[Systemausfall share](https://teilen.systemausfall.org/login/)|Verschlüsselter Datenaustausch-Dienst, basiert auf [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[PicaDrop](https://drop.picasoft.net/) |Verschlüsselter Datenaustausch-Dienst, basiert auf [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/)|
    |[Standardnotes filesend](https://filesend.standardnotes.com)|Datenaustausch-Dienst, basiert auf [standardnotes](https://github.com/standardnotes/filesend)|
    |[Zaclys share](https://www.zaclys.com/envoi/) |Datenaustausch-Dienst |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=148&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Datenaustausch-Dienste, basierend auf [lufi](https://framagit.org/fiat-tux/hat-softwares/lufi/), [plik](https://github.com/root-gg/plik/), [file2link](https://framagit.org/kepon/file2link/) oder [firefox send (fork)](https://forge.april.org/Chapril/drop.chapril.org-firefoxsend/) |


## Chats und Mailinglisten

=== "Video- und Sprachanrufe"

    <img src="../../assets/img/jitsi.svg" alt="Jitsi" width="50px" align="left"></img> Ersetzt Skype, Google Hangouts, Zoom und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framatalk](https://framatalk.org/accueil/de/) |Sicherer Video- und Audiokonferenzdienst, basiert auf [jitsi meet](https://meet.jit.si/). Unterstützt [Ende-zu-Ende-verschlüsselte Videoanrufe](https://jitsi.org/e2ee-in-jitsi/) |
    |[Disroot calls](https://calls.disroot.org/) |Sicherer Video- und Audiokonferenzdienst, basiert auf [jitsi meet](https://meet.jit.si/). Unterstützt [Ende-zu-Ende-verschlüsselte Videoanrufe](https://jitsi.org/e2ee-in-jitsi/) |
    |[Senfcall](https://senfcall.de/)|Videokonferenzdienst, basiert auf [bigbluebutton](https://bigbluebutton.org/) |
    |[Picasoft voice](https://framatalk.org/accueil/en/)|Audiokonferenzdienst, basiert auf [mumble](https://www.mumble.info/)|
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=117&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Video- und Audiokonferenzdienste, basierend auf [jitsi meet](https://meet.jit.si/), [nextcloud](https://nextcloud.com/de/) oder [big blue button](https://bigbluebutton.org/) |


=== "Web Chats"

    <img src="../../assets/img/signal.svg" alt="Web chats" width="50px" align="left"></img> Ersetzt Teams, Facebook Groups, Slack und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framateam](https://framateam.org/login/) |Team-Chat-Lösung, basiert auf [mattermost](https://mattermost.com/) |
    |[Disroot web chat](https://webchat.disroot.org/) |Team-Chat-Lösung, basiert auf [xmpp](https://conversejs.org/) |
    |[Systemausfall chat](https://klax.systemausfall.org/) |Team-Chat-Lösung, basiert auf [matrix](https://matrix.org/) |
    |[Picateam](https://team.picasoft.net/login)|Team-Chat-Lösung, basiert auf [mattermost](https://mattermost.com/) |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=118&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Team-Chat-Lösungen, basierend auf [matrix](https://matrix.org/), [mattermost](https://mattermost.com/) oder [rocket chat](https://rocket.chat/) |


=== "Mailing-Listen"

    <img src="../../assets/img/pidgin.svg" alt="Mailing-Listen" width="50px" align="left"></img> Ersetzt Google Groups und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Framalistes](https://framalistes.org/sympa/) |Mailinglisten-Dienst, basiert auf [sympa](https://www.sympa.org/)|
    |[Riseup lists](https://lists.riseup.net/) |Mailinglisten-Dienst, basiert auf [sympa](https://www.sympa.org/)    |
    |[Systemausfall lists](https://wat.systemausfall.org/) |Mailinglisten-Dienst, basiert auf [sympa](https://www.sympa.org/) |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=114&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Mailinglisten-Dienste, basierend auf [sympa](https://www.sympa.org/) |


## Soziale Netzwerke

=== "Soziale Netzwerke"

    <img src="../../assets/img/permissions.svg" alt="Fediverse" width="50px" align="left"></img> Ersetzt Facebook und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Diaspora](https://diasporafoundation.org/) |Dezentrales, kostenfreies und datenschutzfreundliches soziales Netzwerk. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/)|
    |[Friendica](https://friendi.ca/) |Dezentrales, datenschutzfreundliches und interoperables soziales Netzwerk. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[GNU Social](https://gnusocial.network/)|Freies und quelloffenes soziales Netzwerk. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[Minds](https://www.minds.com/) |Freies und quelloffenes, verschlüsseltes und auf Belohnung basiertes soziales Netzwerk |


=== "Mikroblogging"

    <img src="../../assets/img/tusky.svg" alt="Mastodon" width="50px" align="left"></img> Ersetzt Twitter und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Mastodon](https://joinmastodon.org/) |Dezentrale Mikroblogging-Plattform zum Teilen von Textbeiträgen, Bildern, Audio, Video oder Umfragen. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[Nitter](https://nitter.net/)|Freies und quelloffenes Benutzeroberfläche für Twitter. Keine Reklame, kein Tracking, kein Twitter |


=== "Blogging"

    <img src="../../assets/img/apache.svg" alt="Write Freely" width="50px" align="left"></img> Ersetzt Medium, WordPress und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[WriteFreely](https://writefreely.org/)|Dezentrale und quelloffene Blog-Plattform. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |


=== "Veranstaltungen"

    <img src="../../assets/img/simplecalendar.svg" alt="Mobilizon" width="50px" align="left"></img> Ersetzt Facebook Events, Gruppen und dergleichen:

    |FOSS Alternativen |Beschreibung|
    | ------ | ------ |
    |[Mobilizon](https://joinmobilizon.org/de/) |Online-Dienst zur Planung, Suche und Organisation von Veranstaltungen. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |


=== "Foren"

    <img src="../../assets/img/redreader.svg" alt="Lemmy" width="50px" align="left"></img> Ersetzt Reddit und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Lemmy](https://join-lemmy.org/) |Online-Diskussionsforen. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[Aether](https://getaether.net/) |Online-Diskussionsforen. Quelloffen, Peer-to-Peer und vergänglich |
    |[Teddit](https://teddit.net/)|Freie und quelloffene Benutzeroberfläche für Reddit. Keine Reklame, kein Tracking, kein Reddit |
    |[Libreddit](https://libredd.it/)|Freie und quelloffene Benutzeroberfläche für Reddit. Keine Reklame, kein Tracking, kein Reddit |


## Soziale Medien

=== "Videos"

    <img src="../../assets/img/totem.svg" alt="Peertube" width="50px" align="left"></img> Ersetzt YouTube, Vimeo, Dailymotion und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Peertube](https://joinpeertube.org/de/) |Dezentrale, freie und quelloffene Video-Plattform. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[Dtube](https://d.tube/) |Dezentrale, freie und quelloffene Video-Plattform |
    |[Invidious](https://invidious.io/)|Freie und quelloffene Benutzeroberfläche für YouTube. Keine Reklame, kein Tracking, kein Google |


=== "Fotos"

    <img src="../../assets/img/opencamera.svg" alt="Pixelfed" width="50px" align="left"></img> Ersetzt Instagram, Flickr, Google Fotos, Img.ur und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Pixelfed](https://pixelfed.org/) |Freie und ethische Foto-Plattform. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[Bibliogram](https://bibliogram.art/) |Freie und quelloffene Benutzeroberfläche für Instagram. Keine Reklame, kein Tracking, kein Facebook |
    |[Zaclys album](https://www.zaclys.com/album/) |Online-Dienst zur Erstellung und Teilen von privaten und öffentlichen Fotoalben |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=149&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene Lösungen zum Teilen von Fotos, basierend auf [lutim](https://github.com/ldidry/lutim/)|


=== "Musik"

    <img src="../../assets/img/audacity.svg" alt="Funkwhale" width="50px" align="left"></img> Ersetzt Spotify, YouTube, Last.fm und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Funkwhale](https://funkwhale.audio/) |Freie, quelloffene und dezentrale Plattform zum Anhören und Teilen von Musik. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |
    |[Libre.fm](https://libre.fm/)|Freie und quelloffene Plattform zum Anhören und Austauschen von Musik |
    |Internet Radio |Hört Online-Radio, zum Beispiel mit [RadioDroid](https://f-droid.org/de/packages/net.programmierecke.radiodroid2/) |
    |Podcasts |Hört kostenfreie Podcasts, zum Beispiel mit [AntennaPod](https://f-droid.org/de/packages/de.danoeh.antennapod/) |


=== "Bücher"

    <img src="../../assets/img/books.svg" alt="Bookwyrm" width="50px" align="left"></img> Ersetzt Goodreads und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Bookwyrm](https://bookwyrm.social/)|Freie, quelloffene und dezentrale Plattform um über Bücher zu sprechen, Lektüre zu verfolgen und mit Freunden zu teilen. Basiert auf [activitypub](https://activitypub.rocks/), Teil des [Fediverse](https://gofoss.net/de/fediverse/) |


## Andere Cloud-Dienste

=== "Suche"

    <img src="../../assets/img/search.svg" alt="Online-Suche" width="50px" align="left"></img> Ersetzt Google Search, Bing und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[SearX](https://searx.space/) |Freie Metasuchmaschine, die Ergebnisse von über 70 Suchdiensten zusammenfasst. Keine Reklame, kein Tracking, kein Google, kein Microsoft. Wählt Eure bevorzugte Instanz, wie z.B. [searx.be](https://searx.be/) oder [disroot search](https://search.disroot.org/) |
    |[DuckDuckGo](https://duckduckgo.com/) |Datenschutzfreundliche Suchmaschine. Kein Tracking. *Vorsicht*: schaltet Anzeigen aus dem Yahoo-Bing-Suchallianz-Netzwerk sowie aus Partnerschaften mit Amazon und eBay. |


=== "Karten"

    <img src="../../assets/img/maps.svg" alt="Online-Karten" width="50px" align="left"></img> Ersetzt Google Maps und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Openstreetmap](https://www.openstreetmap.org/) |Freie, quelloffene und von der Gemeinschaft gepflegte Online-Weltkarte |
    |[Framacarte](https://framacarte.org/de/) |Freier Online-Dienst zur Erstellung eigener Karten. Basiert auf [umap](https://github.com/umap-project/umap/) |
    |[GoGoCarto](https://gogocarto.fr/projects) |Freier Online-Dienst zur Erstellung eigener Karten |


=== "Übersetzungen"

    <img src="../../assets/img/empathy.svg" alt="Deepl" width="50px" align="left"></img> Ersetzt Google Translations und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[LibreTranslate](https://libretranslate.com/)|Freier und quelloffener Übersetzungsdienst. Keine Reklame, kein Google |
    |[Simply Translate](https://simplytranslate.org/)|Freie und quelloffene Benutzeroberfläche für Google Translate. Keine Reklame, kein Tracking, kein Google |
    |[Deepl](https://www.deepl.com/translator)|Freier Übersetzungsdienst. *Vorsicht*: nicht quelloffen |


=== "Wikipedia"

    <img src="../../assets/img/fossbrowser.svg" alt="Wikipedia" width="50px" align="left"></img> Ersetzt Wikipedia und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Wikiless](https://wikiless.org/)|Freie und quelloffene Benutzeroberfläche für Wikipedia. Keine Reklame, kein Tracking, keine Zensur |


=== "URL-Verkürzer"

    <img src="../../assets/img/fossbrowser.svg" alt="URL-Verkürzer" width="50px" align="left"></img> Ersetzt Bit.ly, Goo.gl und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Let's shorten that URL](https://lstu.fr/)|Freier und quelloffener URL-Verkürzter. Ermöglicht personalisierte Link-Namen |
    |[Kätzchen-Dienste](https://www.chatons.org/en/search/by-service?service_type_target_id=125&title=&field_software_target_id=All&field_is_shared_value=All) |Verschiedene freie und quelloffene URL-Verkürzer, basierend auf [lstu](https://lstu.fr/), [polr](https://github.com/cydrobolt/polr/) oder [yourls](https://github.com/YOURLS/YOURLS/) |


=== "Pastebins"

    <img src="../../assets/img/editor.svg" alt="Hastebin" width="50px" align="left"></img> Ersetzt Pastebin und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | -------------------------------- | ------ |
    |[Hastebin](https://www.toptal.com/developers/hastebin/) |Freier und quelloffener Online-Pastebin-Dienst |
    |[PicaPaste](https://paste.picasoft.net/)|Freier, quelloffener und verschlüsselter Online-Pastebin-Dienst. Basiert auf Privatebin, [mehr Privatebin-Instanzen gibt's hier](https://github.com/PrivateBin/PrivateBin/wiki/PrivateBin-Instances-Directory)|
    |[GhostBin](https://ghostbin.com/) |Freier und quelloffener Online-Pastebin-Dienst |


=== "Online-Bezahlung"

    <img src="../../assets/img/money.svg" alt="Kryptowährung" width="50px" align="left"></img> Ersetzt Paypal, Google Wallet, Apple Pay und dergleichen:

    |FOSS Alternativen |Beschreibung |
    | --------------------------------- | ------ |
    |[LiberaPay](https://de.liberapay.com/)|Quelloffene Spenden-Plattform |
    |[Bitcoin](https://bitcoin.org/de/) |Dezentrale digitale Kryptowährung, in 2009 eingeführt |
    |[Ethereum](https://ethereum.org/de/) |Dezentrale digitale Kryptowährung, in 2015 eingeführt |
    |[Litecoin](https://litecoin.org/de/) |Dezentrale digitale Kryptowährung, in 2011 eingeführt |


=== "E-Mail Weiterleitung"

    <img src="../../assets/img/simpleemail.svg" alt="E-Mail Weiterleitung" width="50px" align="left"></img> E-Mails anonym weiterleiten:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Anon Addy](https://anonaddy.com/) |Quelloffener und anonymer E-Mail Weiterleitungsdienst |
    |[Simple Login](https://simplelogin.io/) |Quelloffener und anonymer E-Mail Weiterleitungsdienst |


=== "Wegwerf-Emails"

    <img src="../../assets/img/simpleemail.svg" alt="Wegwerf-Emails" width="50px" align="left"></img> Wegwerf-Emails nutzen:

    |FOSS Alternativen |Beschreibung |
    | ------ | ------ |
    |[Spam Gourmet](https://www.spamgourmet.com/index.pl) |Kostenloser Online-Dienst zum Erstellen von Wegwerf-E-Mail-Adressen |
    |[Guerrillamail](https://www.guerrillamail.com/) |Kostenloser Online-Dienst zum Erstellen von Wegwerf-E-Mail-Adressen |
    |[Anonbox.net](https://anonbox.net/) |Kostenloser Online-Dienst zum Erstellen von Wegwerf-E-Mail-Adressen |
    |[Jetable.org](https://jetable.org/) |Kostenloser Online-Dienst zum Erstellen von Wegwerf-E-Mail-Adressen |

<br>

---
template: main.html
title: Comment créer des mots de passe forts et uniques
description: Force de mots de passes. Liste de mots Diceware. Authentification multi facteurs. Gestionnaire de mots de passes. Keepass. Est ce que mon mot de passe a été piraté ?
---

# Choisissez des mots de passe sécurisés et une authentification à deux facteurs

!!! level "Dernière mise à jour: mars 2022. Destiné aux débutants. Aucune compétence technique particulière n'est requise."

<center>
<img align="center" src="../../assets/img/password.png" alt="Diceware" width ="450px"></img>
</center>

Avez-vous déjà été victime d'une perte de mot de passe ? Alors ce guide sur la création de mots de passe forts, uniques et simples à retenir pourra vous être utile pour protéger vos comptes, appareils et [fichiers chiffrés](https://gofoss.net/fr/encrypted-files/). Nous aborderons les meilleures pratiques en matière de longueur des mots de passe et passerons en revue les gestionnaires de mots de passe open source tels que Keepass XC, Keepass DX ou Strongbox. Finalement, vous apprendrez également comment activer l'authentification multifactorielle (2FA).

## La méthode du lancer de dés ("Diceware")

[Diceware](https://diceware.fr/) est une méthode populaire pour créer des mots de passe forts et uniques et pourtant simple à se rappeler. Il vous suffit d'avoir un dé, un stylo et une feuille de papier. Si vous choisissez un mot de passe composé d'au moins sept mots, il sera considéré comme virtuellement incassable par les technologies actuelles. Lisez la suite pour en apprendre plus.

??? tip "Montrez-moi le guide étape par étape"

    <center>

    | Étapes | Instructions |
    | :------: | ------ |
    | 1 |Sélectionnez une liste Diceware. Par exemple la [française](https://diceware.fr/), ou bien [celle de Christophe-Marie Duquesne](https://raw.githubusercontent.com/chmduquesne/diceware-fr/master/diceware-fr-5-jets.txt). Il en existe beaucoup d'autres dans divers langages. |
    | 2 |Lancez un dé 5 fois et notez les résultats. |
    | 3 |Regardez le mot correspondant dans la liste et notez le. |
    | 4 | Répétez les étapes précédentes jusqu'à avoir au moins six mots. En fait, sept mots sont recommandés pour obtenir une entropie de 90.3 bits. Selon la [foire aux questions de Diceware](https://theworld.com/~reinhold/dicewarefaq.html#howlong/), avec cette entropie, votre phrase est incassable par les technologies actuelles, mais elle est pourra l'être par de grande organisations vers 2030. Utiliser huit mots la rendra complètement sécurisée jusqu'en 2050. |
    | 5 |La combinaison de ces mots est votre mot de passe sécurisé. N'oubliez pas de les séparer par un espace. |

    </center>

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe src="https://archive.org/embed/how-to-make-a-super-secure-password" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Publié par la [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>


??? question "Est ce que l'un de mes comptes a été piraté ?"

    | Avez vous été piraté⋅e ? | Description |
    | ------ | ------ |
    | [Have I Been Pwned](https://haveibeenpwned.com/) | Moteur de recherche pour vérifier si votre courrier électronique ou votre mot de passe ne figure pas dans une gigantesque liste de données volées. |
    | [Dehashed](https://www.dehashed.com/) | Recherchez dans les adresses IP, les courriels, les noms d'utilisateurs, les noms, les numéros de téléphone etc. pour savoir ce qui est impacté dans des fuites de bases de données et des comptes compromis. |


<br>

<center> <img src="../../assets/img/separator_https.svg" alt="Keepass" width="150px"></img> </center>

## Keepass

[Keepass](https://keepass.info/) est un gestionnaire de mots de passe libre et open source, disponible sur pratiquement tous les appareils. Ce logiciel sauvegarde vos mots de passe dans une base de données chiffrée, qui est elle-même protégée par un mot de passe maître — un mot de passe pour les gouverner tous. Bien entendu, **vous ne devez jamais oublier ce mot de passe maître** !

Nous vous recommandons également de conserver hors-ligne votre base de données de mots de passe. Stockez-la en local sur un de vos appareils et conservez deux copies distantes dans une [sauvegarde](https://gofoss.net/fr/backups/).


### Clients Keepass

=== "Android"

    [Keepass DX](https://www.keepassdx.com/) est un gestionnaire de mots de passe libre, sécurisé [et open source](https://github.com/Kunzisoft/KeePassDX) pour Android. Plus d'informations ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        Téléchargez l'application sur [Google's Play Store](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free&hl=fr_FR/), [F-Droid](https://www.f-droid.org/fr/packages/com.kunzisoft.keepass.libre/) ou [Aurora Store](https://auroraoss.com/). Elle [contient 0 traqueur et demande 6 permissions](https://reports.exodus-privacy.eu.org/fr/reports/com.kunzisoft.keepass.libre/latest/).


=== "iOS"

    A l'heure où nous écrivons ces lignes, il n'existe pas de version gratuite de Keepass DX pour iOS. [Strongbox](https://github.com/strongbox-password-safe/Strongbox/) est un client Keepass sécurisé et open source. Plus d'instructions sont données ci-après.

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        Téléchargez l'application Strongbox sur l'[App Store](https://apps.apple.com/fr/app/strongbox-keepass-pwsafe/id897283731/).


=== "Windows"

    [KeePass XC](https://keepassxc.org/) est un gestionnaire de mots de passe multi-plateformes, communautaire, gratuit et [open source](https://github.com/keepassxreboot/keepassxc/). Plus d'informations ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        [Téléchargez le programme d'installation](https://keepassxc.org/download/#windows/), faites un double clic sur le fichier `.msi` et suivez les indications qui s'affichent à l'écran.


=== "macOS"

    [KeePass XC](https://keepassxc.org/) est un gestionnaire de mots de passe multi-plateformes, communautaire, gratuit et [open source](https://github.com/keepassxreboot/keepassxc/). Plus d'informations ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        [Téléchargez le programme d'installation](https://keepassxc.org/download/#mac/) ; il devrait s'ouvrir automatiquement et monter un nouveau volume contenant l'application Keepass XC. Si cela n'est pas le cas, ouvrez le fichier `.dmg` que vous avez téléchargé et faites glisser en haut du dossier Applications l'icône Keepass XC qui apparaît. Pour pouvoir y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Keepass XC dans le dock.


=== "Linux (Ubuntu)"

    [KeePass XC](https://keepassxc.org/) est un gestionnaire de mots de passe multi-plateformes, communautaire, gratuit et [open source](https://github.com/keepassxreboot/keepassxc/). Plus d'informations ci-dessous.

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        Si vous utilisez une distribution Linux telle qu'[Ubuntu](https://gofoss.net/fr/ubuntu/), ouvrez le terminal en utilisant le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche de votre écran et cherchez `Terminal`. Exécutez les commandes suivantes pour installer KeePassXC :

        ```bash
        sudo add-apt-repository ppa:phoerious/keepassxc
        sudo apt update
        sudo apt install keepassxc
        ```

<div style="margin-top: -20px;">
</div>

??? tip "Montrez-moi une vidéo récapitulative (2min)"

    <center>
    <iframe src="https://archive.org/embed/using-password-managers-to-stay-safe-online" width="600" height="400" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

    *Publié par la [Electronic Frontier Foundation](https://www.eff.org/).*
    </center>

<br>

<center> <img src="../../assets/img/separator_networksecurity.svg" alt="Authentification à deux facteurs" width="150px"></img> </center>

## Authentification à deux facteurs

[L'authentification à deux facteurs (A2F, ou 2FA en anglais)](https://fr.wikipedia.org/wiki/Double_authentification) offre une couche de sécurité supplémentaire. Avec cette méthode, il faut plus qu'un simple mot de passe pour accéder aux services ou à ses comptes. Par exemple, un code de vérification à usage unique envoyé par SMS ou généré par une application ou une clé d'authentification.

S'il est généralement admis que l'authentification à deux facteurs augmente le niveau de sécurité, cette méthode ouvre un espace supplémentaire pour les cyberattaques dont font partie le « phishing » ou [hameçonnage](https://fr.wikipedia.org/wiki/Hame%C3%A7onnage), [le vol d'identité ou SIM swap](https://fr.wikipedia.org/wiki/Swapper#SIM_swapper) ou encore [l'interception de SMS ou « SMS hijacking » (attaques SS7)](https://fr.wikipedia.org/wiki/Signaling_System_7). Elle est par ailleurs moins simple à manier pour l'utilisateur moyen.

En définitive, nous recommandons l'authentification à deux facteurs. Voyez si l'authentification à deux facteurs vous apporte quelque chose en plus, [cela dépend de votre modèle de menace](https://www.eff.org/document/surveillance-self-defense-threat-modeling/). Si vous adoptez cette procédure, n'oubliez pas de conserver dans un endroit sûr les codes de sauvegarde que certains services fournissent. Ils peuvent vous sauver la vie si vous ne pouvez plus accéder à votre téléphone ou au programme d'authentification.


### Clients 2FA

=== "Android"

    [AndOTP](https://github.com/andOTP/andOTP/) est une application d'authentification à deux facteurs gratuite et libre pour Android. Plus d'instructions sont données ci-après.

    ??? tip "Montrez-moi le guide étape par étape pour Android"

        Téléchargez l'application sur [Google's Play Store](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp&hl=en&gl=FR/), [F-Droid](https://f-droid.org/fr/packages/org.shadowice.flocke.andotp/) ou [Aurora Store](https://auroraoss.com/). Elle [contient 0 traqueur et demande 1 permission](https://reports.exodus-privacy.eu.org/fr/reports/org.shadowice.flocke.andotp/latest/).

=== "iOS"

    [Tofu](https://www.tofuauth.com/) est une application d'authentification à deux facteurs gratuite et libre pour iOS. Plus d'instructions sont données ci-après.

    ??? tip "Montrez-moi le guide étape par étape pour iOS"

        Téléchargez l'application sur l'[App Store](https://apps.apple.com/fr/app/tofu-authenticator/id1082229305).


=== "Windows"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) est une application d'authentification libre et multi-plateformes. Elle nécessite l'utilisation d'une clé physique. Plus d'instructions sont données ci-après.

    ??? tip "Montrez-moi le guide étape par étape pour Windows"

        [Téléchargez le programme d'installation](https://www.yubico.com/products/yubico-authenticator/) et suivez les indications qui s'affichent à l'écran.


=== "macOS"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) est une application d'authentification libre et multi-plateformes. Elle nécessite l'utilisation d'une clé physique. Plus d'instructions sont données ci-après.

    ??? tip "Montrez-moi le guide étape par étape pour macOS"

        [Téléchargez le programme d'installation](https://www.yubico.com/products/yubico-authenticator/).  Il devrait s'ouvrir automatiquement et monter un nouveau volume contenant l'application Yubico. Si cela n'est pas le cas, ouvrez le fichier `.dmg` que vous avez téléchargé et faites glisser en haut du dossier Applications l'icône Yubico qui apparaît. Pour pouvoir y accéder facilement, ouvrez le dossier Applications et faites glisser l'icône Yubico dans le dock.


=== "Linux (Ubuntu)"

    [Yubico Authenticator](https://www.yubico.com/products/yubico-authenticator/) est une application d'authentification libre et multi-plateformes. Elle nécessite l'utilisation d'une clé physique. Plus d'instructions sont données ci-après.

    ??? tip "Montrez-moi le guide étape par étape pour Linux (Ubuntu)"

        Ouvrez le terminal en utilisant le raccourci `CTRL + ALT + T`, ou cliquez sur le bouton `Applications` en haut à gauche de votre écran et cherchez `Terminal`. Exécutez les commandes suivantes pour installer Yubico Authenticator :

        ```bash
        sudo add-apt-repository ppa:yubico/stable
        sudo apt update
        sudo apt-get install yubioath-desktop
        ```

<div align="center">
<img src="https://imgs.xkcd.com/comics/password_strength.png" width="550px" alt="Force de mot de passe"></img>
</div>

<br>

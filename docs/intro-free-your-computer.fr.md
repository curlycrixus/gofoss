---
template: main.html
title: Comment installer Linux sur votre ordinateur
description: Qu'est-ce que Linux ? Quel système d'exploitation Linux est le meilleur ? Linux vs Windows ? Linux vs MacOS ? Qu'est-ce qu'Ubuntu ?
---

# Ubuntu & Linux Mint – <br> Meilleurs distros Linux pour les débutants

!!! level "Dernière mise à jour: mars 2022"

<center>
<html>
<embed src="../../assets/echarts/os_stats.html" style="width: 100%; height:520px">
</html>
</center>

## Qu'est-ce que Linux ?

[Linux](https://fr.wikipedia.org/wiki/Linux) est un systèmes d'exploitation à code source ouvert. C'est une formidable alternative pour les utilisateurs qui ne font pas confiance aux GAFAM pour leurs données. Linux peut même se glorifier d'avantages uniques par rapport à ses rivaux fermés : gratuit et open source, c'est un système d'exploitation qui respecte la vie privée, tourne sur de vieux matériels, dispose d'une communauté de support très active et est facile à maintenir. Longtemps relégué à une existence de niche, Linux est arrivé à maturité: Linux est même présent sur Mars, grâce au Perseverance Rover de la Nasa ! Et Linux se décline en de multiples saveurs, comme par exemple [Ubuntu](https://gofoss.net/fr/ubuntu/) et ses dérivés.

## Devriez-vous passer à Linux ?

Un utilisateur standard passe [entre deux et six heures par jour](https://www.bondcap.com/report/it19/) sur son ordinateur. La plupart de ces machines tournent sous Windows. Certaines sous MacOS et quelques unes sous ChromeOS. Par conséquent, les utilisateurs partagent (in)volontairement des quantités de données avec [Microsoft](https://privacy.microsoft.com/fr-fr/privacystatement/), [Apple](https://www.apple.com/legal/privacy/fr-ww/) ou [Google](https://policies.google.com/privacy?hl=fr&gl=FR/). Est-ce que cette situation est inévitable ? Non — la beauté caractéristique des ordinateurs est qu'ils sont, [par construction](https://fr.wikipedia.org/wiki/Turing-complet), conçus pour faire tourner n'importe quel système d'exploitation. Y compris Linux !



??? info "Dites m'en plus sur les préoccupations à propos de Windows, MacOS & ChromeOS"

    <center>

    | Préoccupations| Description |
    | ------ | ------ |
    |Abus de données |Microsoft, Apple et Google collectent, utilisent, transfèrent et dévoilent les données utilisateurs, certaines de manière anonyme, d'autres pas. |
    |Logiciels propriétaires | Windows, MacOS et ChromeOS sont, utilisent ou promeuvent des logiciels propriétaires. Il n'y a aucune possibilité de savoir exactement ce qui se passe à l'intérieur de l'ordinateur d'un utilisateur. |
    |Gestion des droits numériques | Microsoft, Apple et Google utilisent des menottes numériques [« DRM » en anglais] pour restreindre ce que les utilisateurs peuvent faire avec leurs ordinateurs. |
    |Obsolescence programmée | Les politiques de Microsoft, Apple et Google forcent les utilisateurs à renouveler régulièrement des logiciels et du matériel fonctionnels. Aux frais de l'utilisateur et de l'environnement.|
    |Manque d'interopérabilité | Microsoft, Apple et Google conçoivent des services incompatibles avec d'autres pour enfermer leurs utilisateurs. |
    |Vulnérabilités | Bien que maintenus par de très grandes entreprises, Windows, MacOs et ChromeOS présentent des vulnérabilités sévères et sont la cible de logiciels espions ou malveillants et de virus. |

</center>



??? question "Est-ce que Linux est LA solution ?"

    Non. Linux n'est pas pour tout le monde. Tout dépend de vos besoins, votre machine et votre ouverture au changement. Mais un passage à Linux vaut vraiment la peine. [Ubuntu](https://ubuntu.com/) ou [Linux Mint](https://linuxmint.com) sont deux options à recommander aux novices. Elles offrent [un tas de formidables applications](https://gofoss.net/fr/ubuntu-apps/), tout est plus ou moins prêt à l'emploi et les deux distributions sont supportées par de grandes communautés :

    * **Ubuntu**: consultez le [forum d'utilisateurs](https://forum.ubuntu-fr.org/), cet autre [forum d'utilisateurs](https://askubuntu.com/) ou le [wiki](https://wiki.ubuntu.com/)
    * **Linux Mint**: consultez le [forum d'utilisateurs](https://forums.linuxmint.com/viewforum.php?f=63) ou la [page de documentation](https://linuxmint.com/documentation.php)

    Linux se décline en de multiples saveurs, appelées distributions ou "distros". Elles ont chacune leurs spécificités :

    <center>

    | Distribution | Description |
    | ------ | ------ |
    | [Sélectionneur de Distro](https://distrochooser.de/fr/) | Super site pour guider votre choix. |
    | [Debian](https://www.debian.org/index.fr.html) | Une des distributions les plus utilisées, composée entièrement de logiciels libres (avec la possibilité d'ajouter des éléments propriétaires). Traite les vulnérabilités dans la sécurité de manière ouverte et transparente. Offre un super support, par exemple sur ce [forum](https://www.debian-fr.org/) et [wiki](https://wiki.debian.org/fr/FrontPage). |
    | [Fedora](https://getfedora.org/fr/) | Plateforme innovante, libre et open source pour matériels, clouds et containers. |
    | [Arch](https://archlinux.org/) | Distribution Linux légère et flexible qui se veut aussi simple que possible (en anglais, « Keep it simple ou KISS »). Arch Linux n'utilise pas d'interface graphique pour installer ou configurer la distribution. La communauté d'Arch offre un super support, par exemple sur ce [forum](https://forums.archlinux.fr/) et [wiki](https://wiki.archlinux.fr/). |
    | [Qubes OS](https://www.qubes-os.org/) | Système d'exploitation open source construit pour fournir une forte sécurité. Utilisé par Edward Snowden. |
    | [Tails](https://tails.boum.org/index.fr.html) | Système d'exploitation live qui marche sur quasi tous les ordinateurs depuis une clef USB ou un DVD. A pour but de préserver la vie privée et l'anonymat, et d'éviter la censure en forçant les connections internet via le réseau Tor. |
    | [Knoppix](http://www.knopper.net/knoppix/index-en.html) | Système live exécutable sur CD, DVD ou clef USB. |
    | [Pure OS](https://pureos.net/) | OS facile d'utilisation, sécurisé et respectant les libertés pour un usage quotidien. |
    | [MX Linux](https://mxlinux.org/) | Élégant et efficace avec une configuration simple, une haute stabilité, de solides performances et une empreinte moyenne. La communauté Linux MX offre un super support, par exemple sur ce [forum](https://forum.mxlinux.org/viewforum.php?f=13) et [wiki (en anglais)](https://mxlinux.org/wiki/). |
    | [Manjaro](https://manjaro.org/) | Système d'exploitation professionnel, adapté pour remplacer Windows ou MacOS. Contient des éléments propriétaires tels que les codecs multimédias. La communauté Manjaro offre un super support, par exemple sur ce [forum](https://www.manjaro.fr/forum/index.php) et [wiki](https://wiki.manjaro.org/index.php/Main_Page/fr). |
    | [Trisquel](https://trisquel.info/fr) | Système d'exploitation entièrement libre pour particuliers, petites entreprises et établissements éducatifs. Dérivé d'Ubuntu. Entièrement libre, sans logiciels propriétaires. |
    | [Gentoo](https://www.gentoo.org/) | Distribution Linux très flexible. |
    | [Alpine Linux](https://www.alpinelinux.org/) | Distribution Linux légère orientée sécurité, basée sur « musl libc » et « busybox ». |
    | [Parrot](https://parrotsec.org/) | Distribution GNU/Linux libre et open source pensée pour les experts de la sécurité, développeurs et particuliers sensibles à la sécurité. |

    </center>


??? info "J'aimerais juste ajouter un mot..."

    Ce qui est appelé Linux est, en fait GNU/Linux, ou, comme je me suis mis à l'appeler récemment, GNU plus Linux. Linux n'est pas un système d'exploitation.

    Beaucoup d'utilisateurs d'ordinateur font tous les jours tourner une version modifiée du système GNU, sans s'en apercevoir. A la suite d'un concours de circonstances étrange, la version de GNU qui est largement utilisée aujourd’hui est souvent appelée "Linux", et un bon nombre de ses utilisateurs ne sont pas conscients qu'il s'agit en fait du système GNU, développé par le projet GNU. Il y a effectivement un Linux, et ces personnes l'utilisent bien, mais c'est juste une partie du système qu'ils utilisent.

    Linux est un noyau: le programme dans le système qui alloue les ressources de la machine aux autres programmes que vous faites tourner. Le noyau est une pièce essentielle du système d'exploitation, mais inutile isolé : il peut seulement fonctionner dans le contexte d'un système d'exploitation complet. Linux est normalement utilisé en association avec le système d'exploitation GNU ; le système au total est en gros GNU avec Linux ajouté, ou GNU/Linux. Toutes les distributions de soi-disant "Linux" sont en fait des distribution GNU/Linux !

    Allez sur [https://gnu.org](https://www.gnu.org/gnu/incorrect-quotation.html) pour en apprendre plus sur Linux et GNU.

<br>

<div align="center">
<img src="https://imgs.xkcd.com/comics/linux_user_at_best_buy.png" alt="Libérez votre ordinateur"></img>
</div>

<br>
